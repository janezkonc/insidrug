# Find probis
#
# Find the probis includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  PROBIS_INCLUDE_DIR, where to find header, etc.
#  PROBIS_LIBRARY, the libraries needed to use probis.
#  PROBIS_FOUND, If false, do not try to use probis.

# only look in default directories
find_path(
	PROBIS_INCLUDE_DIR 
	NAMES ProBiS.hpp
    PATHS external/probis/include
	DOC "probis include dir"
)

find_library(
	PROBIS_LIBRARY
	NAMES libProBiS_static.a libProBiS.dylib
    PATHS external/probis/lib
	DOC "probis library"
)

set(PROBIS_INCLUDE_DIR ${PROBIS_INCLUDE_DIR} CACHE STRING INTERNAL)
set(PROBIS_LIBRARY ${PROBIS_LIBRARY} CACHE STRING INTERNAL)

MESSAGE( STATUS ":::PROBIS_INCLUDE_DIR:         " "${PROBIS_INCLUDE_DIR}" )
MESSAGE( STATUS ":::PROBIS_LIBRARY:         " "${PROBIS_LIBRARY}" )


IF(PROBIS_LIBRARY)
IF(PROBIS_INCLUDE_DIR OR PROBIS_CXX_FLAGS)

SET(PROBIS_FOUND 1)

ENDIF(PROBIS_INCLUDE_DIR OR PROBIS_CXX_FLAGS)
ENDIF(PROBIS_LIBRARY)


# ==========================================
IF(NOT PROBIS_FOUND)
# make FIND_PACKAGE friendly
IF(NOT PROBIS_FIND_QUIETLY)
IF(PROBIS_FIND_REQUIRED)
 MESSAGE(FATAL_ERROR "PROBIS required, please specify it's location.")
ELSE(PROBIS_FIND_REQUIRED)
 MESSAGE(STATUS       "ERROR: PROBIS was not found.")
ENDIF(PROBIS_FIND_REQUIRED)
ENDIF(NOT PROBIS_FIND_QUIETLY)
ENDIF(NOT PROBIS_FOUND)
