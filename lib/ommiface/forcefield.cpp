#include "forcefield.hpp"
#include "OpenMM.h"
#include "gaff.hpp"
#include "helper/error.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "ommerrors.hpp"
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/asio/ip/host_name.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/regex.hpp>

namespace insilab::ommiface {

std::ostream &operator<<(std::ostream &stream, const ForceField &f) {
  for (const auto &[resn, residue_topology] : f.residue_topology) {
    for (const auto &[name, type] : residue_topology.atom) {
      stream << "atom " << name << " = " << type << std::endl;
    }
    for (const auto &[from, to_yes] : residue_topology.bond) {
      for (const auto &[to, yes] : to_yes) {
        stream << "bond " << from << " " << to << " = " << yes << std::endl;
      }
    }
    for (const auto &from : residue_topology.external_bond) {
      stream << "external bond " << from << std::endl;
    }
  }
  return stream;
}

ForceField::ForceField(const double step_non_bond)
    : step{step_non_bond * OpenMM::NmPerAngstrom} {}

const ForceField::AtomType &ForceField::get_atom_type(const int type) const {
  try {
    return atom_type.at(type);
  } catch (const std::out_of_range &) {
  }
  throw ParameterError("[NOTE] missing atom type " + helper::to_string(type));
}
const ForceField::BondType &ForceField::get_bond_type(const int type1,
                                                      const int type2) const {

  const AtomType &atype1 = atom_type.at(type1);
  dbgmsg(atype1.cl);
  const AtomType &atype2 = atom_type.at(type2);
  dbgmsg(atype2.cl);
  const std::string &aclass1 = atype1.cl;
  const std::string &aclass2 = atype2.cl;
  dbgmsg("add bond type1 = " << type1 << " type2 = " << type2 << " aclass1 = "
                             << aclass1 << " aclass2 = " << aclass2);
  try {
    return bond_type.at(aclass1).at(aclass2);
  } catch (const std::out_of_range &) {
  }
  try {
    return bond_type.at(aclass2).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  // if this fails, we try finding a similar bond parameter
  for (auto &sclass : helper::get_replacement({aclass1, aclass2})) {
    try {
      return bond_type.at(sclass[0]).at(sclass[1]);
    } catch (const std::out_of_range &) {
    }
    try {
      return bond_type.at(sclass[1]).at(sclass[0]);
    } catch (const std::out_of_range &) {
    }
  }
  throw ParameterError("[NOTE] missing bond type " + aclass1 + "-" + aclass2);
}

const ForceField::AngleType &ForceField::get_angle_type(const int type1,
                                                        const int type2,
                                                        const int type3) const {

  const AtomType &atype1 = atom_type.at(type1);
  const AtomType &atype2 = atom_type.at(type2);
  const AtomType &atype3 = atom_type.at(type3);
  const std::string &aclass1 = atype1.cl;
  const std::string &aclass2 = atype2.cl;
  const std::string &aclass3 = atype3.cl;
  dbgmsg("add angle type1 = " << type1 << " type2 = " << type2 << " type3 = "
                              << type3 << " aclass1 = " << aclass1
                              << " aclass2 = " << aclass2
                              << " aclass3 = " << aclass3);
  // See note under bond stretch above regarding the factor of 2 here.
  try {
    return angle_type.at(aclass1).at(aclass2).at(aclass3);
  } catch (const std::out_of_range &) {
  }
  try {
    return angle_type.at(aclass3).at(aclass2).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  // if this fails, we try finding a similar angle parameter
  for (auto &sclass : helper::get_replacement({aclass1, aclass2, aclass3})) {
    try {
      return angle_type.at(sclass[0]).at(sclass[1]).at(sclass[2]);
    } catch (const std::out_of_range &) {
    }
    try {
      return angle_type.at(sclass[2]).at(sclass[1]).at(sclass[0]);
    } catch (const std::out_of_range &) {
    }
  }
  throw ParameterError("[NOTE] missing angle type " + aclass1 + "-" + aclass2 +
                       "-" + aclass3);
}

const ForceField::TorsionTypeVec &
ForceField::get_dihedral_type(const int type1, const int type2, const int type3,
                              const int type4) const {

  const AtomType &atype1 = atom_type.at(type1);
  const AtomType &atype2 = atom_type.at(type2);
  const AtomType &atype3 = atom_type.at(type3);
  const AtomType &atype4 = atom_type.at(type4);
  const std::string &aclass1 = atype1.cl;
  const std::string &aclass2 = atype2.cl;
  const std::string &aclass3 = atype3.cl;
  const std::string &aclass4 = atype4.cl;
  dbgmsg("add dihedral type1 = "
         << type1 << " type2 = " << type2 << " type3 = " << type3 << " type4 = "
         << type4 << " aclass1 = " << aclass1 << " aclass2 = " << aclass2
         << " aclass3 = " << aclass3 << " aclass4 = " << aclass4);
  try {
    return torsion_type.at(aclass1).at(aclass2).at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }
  try {
    return torsion_type.at(aclass4).at(aclass3).at(aclass2).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return torsion_type.at("X").at(aclass2).at(aclass3).at("X");
  } catch (const std::out_of_range &) {
  }
  try {
    return torsion_type.at("X").at(aclass3).at(aclass2).at("X");
  } catch (const std::out_of_range &) {
  }
  // if this fails, we try finding a similar dihedral parameter
  for (auto &sclass :
       helper::get_replacement({aclass1, aclass2, aclass3, aclass4})) {
    try {
      return torsion_type.at(sclass[0]).at(sclass[1]).at(sclass[2]).at(
          sclass[3]);
    } catch (const std::out_of_range &) {
    }
    try {
      return torsion_type.at(sclass[3]).at(sclass[2]).at(sclass[1]).at(
          sclass[0]);
    } catch (const std::out_of_range &) {
    }
    try {
      return torsion_type.at("X").at(sclass[1]).at(sclass[2]).at("X");
    } catch (const std::out_of_range &) {
    }
    try {
      return torsion_type.at("X").at(sclass[2]).at(sclass[1]).at("X");
    } catch (const std::out_of_range &) {
    }
  }
  throw ParameterError("[NOTE] missing dihedral type " + aclass1 + "-" +
                       aclass2 + "-" + aclass3 + "-" + aclass4);
}
const ForceField::TorsionTypeVec &
ForceField::get_improper_type(const int type1, const int type2, const int type3,
                              const int type4) const {

  const AtomType &atype1 = atom_type.at(type1);
  const AtomType &atype2 = atom_type.at(type2);
  const AtomType &atype3 = atom_type.at(type3);
  const AtomType &atype4 = atom_type.at(type4);
  const std::string &aclass1 = atype1.cl;
  const std::string &aclass2 = atype2.cl;
  const std::string &aclass3 = atype3.cl;
  const std::string &aclass4 = atype4.cl;
  dbgmsg("add improper (third atom is central) type1 = "
         << type1 << " type2 = " << type2 << " type3 = " << type3 << " type4 = "
         << type4 << " aclass1 = " << aclass1 << " aclass2 = " << aclass2
         << " aclass3 = " << aclass3 << " aclass4 = " << aclass4);
  try {
    return improper_type.at(aclass1).at(aclass2).at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at(aclass2).at(aclass1).at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at(aclass2).at(aclass4).at(aclass3).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at(aclass4).at(aclass2).at(aclass3).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at(aclass4).at(aclass1).at(aclass3).at(aclass2);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at(aclass1).at(aclass4).at(aclass3).at(aclass2);
  } catch (const std::out_of_range &) {
  }

  try {
    return improper_type.at("X").at(aclass1).at(aclass3).at(aclass2);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at(aclass1).at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at(aclass2).at(aclass3).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at(aclass2).at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at(aclass4).at(aclass3).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at(aclass4).at(aclass3).at(aclass2);
  } catch (const std::out_of_range &) {
  }

  try {
    return improper_type.at("X").at("X").at(aclass3).at(aclass1);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at("X").at(aclass3).at(aclass2);
  } catch (const std::out_of_range &) {
  }
  try {
    return improper_type.at("X").at("X").at(aclass3).at(aclass4);
  } catch (const std::out_of_range &) {
  }

  throw ParameterError("[NOTE] missing improper type " + aclass1 + "-" +
                       aclass2 + "-" + aclass3 + "-" + aclass4);
}

const std::optional<std::reference_wrapper<const ForceField::ResidueTopology>>
ForceField::get_residue_topology(const std::string &resn) const {
  return residue_topology.contains(resn)
             ? std::optional<std::reference_wrapper<
                   const ForceField::ResidueTopology>>{residue_topology
                                                           .at(resn)}
             : std::nullopt;
}

ForceField &ForceField::insert_topology(const molib::Molecule &molecule) {

  std::map<const std::string, const int> atom_name_to_type;
  for (auto &kv : this->atom_type) {
    const int &type = kv.first;
    AtomType &at = kv.second;
    atom_name_to_type.insert({at.cl, type});
  }
  for (const auto &presidue : molecule.get_residues()) {
    const auto &residue = *presidue;
    dbgmsg("checking if residue is already in ffield: " << residue);
    // check if residue not already in the ffield
    if (this->residue_topology.contains(residue.resn()))
      continue;
    ResidueTopology rtop;
    for (const auto &atom : residue) {
      dbgmsg("inserting topology for atom "
             << atom << " with gaff type [" << atom.gaff_type()
             << "], the topology type is "
             << (atom_name_to_type.contains(atom.gaff_type())
                     ? helper::to_string(atom_name_to_type.at(atom.gaff_type()))
                     : "NON-EXISTENT"));
      if (!atom_name_to_type.contains(atom.gaff_type())) { // see issue #113
        throw helper::Error(
            "[WHOOPS] insert topology failed for atom " +
            helper::to_string(atom) +
            " with gaff type (check gaff.dat) = " + atom.gaff_type());
      }
      rtop.atom.insert(
          {atom.atom_name(), atom_name_to_type.at(atom.gaff_type())});
    }
    for (const auto &atom : residue) {
      for (const auto &adj_a : atom) {
        if (atom.atom_number() < adj_a.atom_number())
          rtop.bond[atom.atom_name()][adj_a.atom_name()] = true;
      }
    }

    this->residue_topology.insert({residue.resn(), rtop});
    dbgmsg("inserted topology for residue " << residue.resn());
  }

  dbgmsg("after inserting topology");
  return *this;
}

ForceField &ForceField::parse_gaff_dat_file() {

  dbgmsg("parsing gaff_dat_file");
  boost::smatch m;

  bool non_bonded = false;
  int semaphore = 0;
  int type =
      10000; // start ligand types with 10000 to NOT overlap with receptor
  std::map<const std::string, const int> atom_name_to_type;

  std::stringstream ss0(data::gaff_dat);

  std::string line;
  while (getline(ss0, line)) {
    if (line.empty()) {
      semaphore++;
      continue;
    }
    if (line.starts_with("AMBER")) {
      continue;
    }
    if (line.starts_with("MOD4")) {
      non_bonded = true;
      continue;
    }
    if (line.starts_with("END"))
      break;
    // read atom types
    if (semaphore == 0 &&
        boost::regex_search(line, m,
                            boost::regex("^(\\S{1,2})\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched) {
        AtomType &at = this->atom_type[type];
        at.cl = m[1].str();
        at.mass = stod(m[2].str());
        at.element = "";
        //~ at.polarizability = stod(m[3].str()); // not used ...
        atom_name_to_type.insert({at.cl, type});
        dbgmsg("type = " << type << " class = " << at.cl << " at.element = "
                         << at.element << " at.mass = " << at.mass);
        type++;
      }
    }
    // parse harmonic bonds
    if (semaphore == 1 &&
        boost::regex_search(
            line, m,
            boost::regex("^(\\S{1,2})\\s*-(\\S{1,2})\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched && m[4].matched) {
        const std::string cl1 = m[1].str();
        const std::string cl2 = m[2].str();
        const double k = stod(m[3].str()) * 2 * OpenMM::KJPerKcal *
                         OpenMM::AngstromsPerNm * OpenMM::AngstromsPerNm;
        const double length = stod(m[4].str()) * OpenMM::NmPerAngstrom;
        const bool can_constrain =
            (cl1.at(0) == 'h' || cl2.at(0) == 'h'
                 ? true
                 : false); // X-H bonds can be constrained
        this->bond_type[cl1][cl2] = BondType{length, k, can_constrain};
        dbgmsg("harmonic bond force between "
               << cl1 << " and " << cl2 << " is "
               << " length = " << length << " k = " << k
               << " can_constrain = " << can_constrain);
      }
    }
    // parse harmonic angles
    if (semaphore == 2 &&
        boost::regex_search(line, m,
                            boost::regex("^(\\S{1,2})\\s*-(\\S{1,2})\\s*-(\\S{"
                                         "1,2})\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched && m[4].matched &&
          m[5].matched) {
        const std::string cl1 = m[1].str();
        const std::string cl2 = m[2].str();
        const std::string cl3 = m[3].str();
        const double k = stod(m[4].str()) * 2 * OpenMM::KJPerKcal;
        const double angle = stod(m[5].str()) * OpenMM::RadiansPerDegree;
        this->angle_type[cl1][cl2][cl3] = AngleType{angle, k};
        dbgmsg("harmonic angle force between "
               << cl1 << " and " << cl2 << " and " << cl3 << " is "
               << " angle = " << angle << " k = " << k);
      }
    }
    // parse proper torsions
    if (semaphore == 3 &&
        boost::regex_search(
            line, m,
            boost::regex("^(\\S{1,2})\\s*-(\\S{1,2})\\s*-(\\S{1,2})\\s*-(\\S{1,"
                         "2})\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched && m[4].matched &&
          m[5].matched && m[6].matched && m[7].matched && m[8].matched) {
        const std::string cl1 = m[1].str();
        const std::string cl2 = m[2].str();
        const std::string cl3 = m[3].str();
        const std::string cl4 = m[4].str();
        const double idivf = stod(m[5].str());
        const double k = (1 / idivf) * stod(m[6].str()) * OpenMM::KJPerKcal;
        const double phase = stod(m[7].str()) * OpenMM::RadiansPerDegree;
        const int periodicity = abs(stoi(m[8].str()));
        TorsionTypeVec &dihedral = this->torsion_type[cl1][cl2][cl3][cl4];
        dihedral.push_back(TorsionType{periodicity, phase, k});
        dbgmsg("periodic (proper) torsion force between "
               << cl1 << " and " << cl2 << " and " << cl3 << " and " << cl4
               << " is "
               << " periodicity" << dihedral.size() << " = " << periodicity
               << " phase" << dihedral.size() << " = " << phase << " k"
               << dihedral.size() << " = " << k);
      }
    }
    // parse improper torsions (there is no IDIVF)
    if (semaphore == 4 &&
        boost::regex_search(
            line, m,
            boost::regex("^(\\S{1,2})\\s*-(\\S{1,2})\\s*-(\\S{1,2})\\s*-(\\S{1,"
                         "2})\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched && m[4].matched &&
          m[5].matched && m[6].matched && m[7].matched) {
        const std::string cl1 = m[1].str();
        const std::string cl2 = m[2].str();
        const std::string cl3 = m[3].str();
        const std::string cl4 = m[4].str();
        const double k = stod(m[5].str()) * OpenMM::KJPerKcal;
        const double phase = stod(m[6].str()) * OpenMM::RadiansPerDegree;
        const int periodicity = abs(stoi(m[7].str()));
        TorsionTypeVec &improper = this->improper_type[cl1][cl2][cl3][cl4];
        improper.push_back(TorsionType{periodicity, phase, k});
        dbgmsg(
            "periodic (improper) torsion force between (third atom is central) "
            << cl1 << " and " << cl2 << " and " << cl3 << " and " << cl4
            << " is "
            << " periodicity" << improper.size() << " = " << periodicity
            << " phase" << improper.size() << " = " << phase << " k"
            << improper.size() << " = " << k);
      }
    }
    if (non_bonded &&
        boost::regex_search(
            line, m, boost::regex("^\\s{2}(\\S{1,2})\\s+(\\S+)\\s+(\\S+)"))) {
      if (m[1].matched && m[2].matched && m[3].matched) {
        const int type = atom_name_to_type.at(m[1].str());
        AtomType &at = this->atom_type[type];
        at.charge = 0; // for now set charge to 0
        at.sigma = stod(m[2].str()) * OpenMM::NmPerAngstrom *
                   OpenMM::SigmaPerVdwRadius;
        at.epsilon = stod(m[3].str()) * OpenMM::KJPerKcal;
        dbgmsg("nonbonded force for atom type "
               << type << " charge = " << at.charge << " sigma = " << at.sigma
               << " epsilon = " << at.epsilon);
      }
    }
  }
  // parse nonbonded force
  this->coulomb14scale = 0.833333;
  this->lj14scale = 0.5;
  return *this;
}

template <typename T>
const char *str(rapidxml::xml_document<> &doc, const T &i) {
  return doc.allocate_string(helper::to_string(i).c_str());
}

void ForceField::output_forcefield_file(
    const std::string &fn) { // output XML forcefield & topology file
  using namespace rapidxml;
  xml_document<> doc; // character type defaults to char
  xml_node<> *ff_node = doc.allocate_node(node_element, "ForceField");
  doc.append_node(ff_node);
  // atom types to XML
  xml_node<> *atom_type_node = doc.allocate_node(node_element, "AtomTypes");
  ff_node->append_node(atom_type_node);
  for (auto &kv : this->atom_type) {
    const int &name = kv.first;
    const AtomType &at = kv.second;
    xml_node<> *type_node = doc.allocate_node(node_element, "Type");
    atom_type_node->append_node(type_node);
    type_node->append_attribute(doc.allocate_attribute("name", str(doc, name)));
    type_node->append_attribute(doc.allocate_attribute("class", at.cl.c_str()));
    type_node->append_attribute(
        doc.allocate_attribute("element", at.element.c_str()));
    type_node->append_attribute(
        doc.allocate_attribute("mass", str(doc, at.mass)));
  }
  // residue topology to XML
  xml_node<> *residues_node = doc.allocate_node(node_element, "Residues");
  ff_node->append_node(residues_node);
  for (auto &kv : this->residue_topology) {
    const std::string &name = kv.first;
    const ResidueTopology &rtop = kv.second;
    xml_node<> *residue_node = doc.allocate_node(node_element, "Residue");
    residues_node->append_node(residue_node);
    residue_node->append_attribute(
        doc.allocate_attribute("name", name.c_str()));
    std::map<const std::string, const int> atom_name_to_index;
    int i = 0;
    for (auto &kv2 : rtop.atom) {
      const std::string &name = kv2.first;
      const int &type = kv2.second;
      atom_name_to_index.insert({name, i++});
      xml_node<> *atom_node = doc.allocate_node(node_element, "Atom");
      residue_node->append_node(atom_node);
      atom_node->append_attribute(doc.allocate_attribute("name", name.c_str()));
      atom_node->append_attribute(
          doc.allocate_attribute("type", str(doc, type)));
    }
    for (auto &kv2 : rtop.bond) {
      const int from = atom_name_to_index.at(kv2.first);
      for (auto &kv3 : kv2.second) {
        const int to = atom_name_to_index.at(kv3.first);
        xml_node<> *bond_node = doc.allocate_node(node_element, "Bond");
        residue_node->append_node(bond_node);
        bond_node->append_attribute(
            doc.allocate_attribute("from", str(doc, from)));
        bond_node->append_attribute(doc.allocate_attribute("to", str(doc, to)));
      }
    }
  }
  // harmonic bonds to XML
  xml_node<> *hb_node = doc.allocate_node(node_element, "HarmonicBondForce");
  ff_node->append_node(hb_node);
  for (auto &kv : this->bond_type) {
    const std::string &cl1 = kv.first;
    for (auto &kv2 : kv.second) {
      const std::string &cl2 = kv2.first;
      const BondType &bt = kv2.second;
      xml_node<> *bond_node = doc.allocate_node(node_element, "Bond");
      hb_node->append_node(bond_node);
      bond_node->append_attribute(
          doc.allocate_attribute("class1", cl1.c_str()));
      bond_node->append_attribute(
          doc.allocate_attribute("class2", cl2.c_str()));
      bond_node->append_attribute(
          doc.allocate_attribute("length", str(doc, bt.length)));
      bond_node->append_attribute(doc.allocate_attribute("k", str(doc, bt.k)));
    }
  }
  // harmonic angles to XML
  xml_node<> *ha_node = doc.allocate_node(node_element, "HarmonicAngleForce");
  ff_node->append_node(ha_node);
  for (auto &kv : this->angle_type) {
    const std::string &cl1 = kv.first;
    for (auto &kv2 : kv.second) {
      const std::string &cl2 = kv2.first;
      for (auto &kv3 : kv2.second) {
        const std::string &cl3 = kv3.first;
        const AngleType &at = kv3.second;
        xml_node<> *angle_node = doc.allocate_node(node_element, "Angle");
        ha_node->append_node(angle_node);
        angle_node->append_attribute(
            doc.allocate_attribute("class1", cl1.c_str()));
        angle_node->append_attribute(
            doc.allocate_attribute("class2", cl2.c_str()));
        angle_node->append_attribute(
            doc.allocate_attribute("class3", cl3.c_str()));
        angle_node->append_attribute(
            doc.allocate_attribute("angle", str(doc, at.angle)));
        angle_node->append_attribute(
            doc.allocate_attribute("k", str(doc, at.k)));
      }
    }
  }
  // periodic proper torsions to XML
  xml_node<> *pt_node = doc.allocate_node(node_element, "PeriodicTorsionForce");
  ff_node->append_node(pt_node);
  for (auto &kv : this->torsion_type) {
    const std::string &cl1 = kv.first;
    for (auto &kv2 : kv.second) {
      const std::string &cl2 = kv2.first;
      for (auto &kv3 : kv2.second) {
        const std::string &cl3 = kv3.first;
        for (auto &kv4 : kv3.second) {
          const std::string &cl4 = kv4.first;
          xml_node<> *torsion_node = doc.allocate_node(node_element, "Proper");
          pt_node->append_node(torsion_node);
          torsion_node->append_attribute(
              doc.allocate_attribute("class1", cl1.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class2", cl2.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class3", cl3.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class4", cl4.c_str()));
          int i = 0;
          for (auto &tt : kv4.second) {
            const std::string &num = helper::to_string(++i);
            torsion_node->append_attribute(doc.allocate_attribute(
                str(doc, ("periodicity" + num)), str(doc, tt.periodicity)));
            torsion_node->append_attribute(doc.allocate_attribute(
                str(doc, "phase" + num), str(doc, tt.phase)));
            torsion_node->append_attribute(
                doc.allocate_attribute(str(doc, "k" + num), str(doc, tt.k)));
          }
        }
      }
    }
  }
  // periodic improper torsions to XML
  for (auto &kv : this->improper_type) {
    const std::string &cl1 = kv.first;
    for (auto &kv2 : kv.second) {
      const std::string &cl2 = kv2.first;
      for (auto &kv3 : kv2.second) {
        const std::string &cl3 = kv3.first;
        for (auto &kv4 : kv3.second) {
          const std::string &cl4 = kv4.first;
          xml_node<> *torsion_node =
              doc.allocate_node(node_element, "Improper");
          pt_node->append_node(torsion_node);
          // be careful because in xml file FIRST atom is central and in gaff
          // dat type (and in improper_type array) the THIRD atom is central!
          torsion_node->append_attribute(
              doc.allocate_attribute("class1", cl3.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class2", cl1.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class3", cl2.c_str()));
          torsion_node->append_attribute(
              doc.allocate_attribute("class4", cl4.c_str()));
          int i = 0;
          for (auto &tt : kv4.second) {
            const std::string &num = helper::to_string(++i);
            torsion_node->append_attribute(doc.allocate_attribute(
                str(doc, ("periodicity" + num)), str(doc, tt.periodicity)));
            torsion_node->append_attribute(doc.allocate_attribute(
                str(doc, "phase" + num), str(doc, tt.phase)));
            torsion_node->append_attribute(
                doc.allocate_attribute(str(doc, "k" + num), str(doc, tt.k)));
          }
        }
      }
    }
  }
  // nonbonded force to XML
  xml_node<> *nb_force_node = doc.allocate_node(node_element, "NonbondedForce");
  ff_node->append_node(nb_force_node);
  nb_force_node->append_attribute(
      doc.allocate_attribute("coulomb14scale", str(doc, this->coulomb14scale)));
  nb_force_node->append_attribute(
      doc.allocate_attribute("lj14scale", str(doc, this->lj14scale)));
  for (auto &kv : this->atom_type) {
    const int &type = kv.first;
    const AtomType &at = kv.second;
    xml_node<> *atom_node = doc.allocate_node(node_element, "Atom");
    nb_force_node->append_node(atom_node);
    atom_node->append_attribute(doc.allocate_attribute("type", str(doc, type)));
    atom_node->append_attribute(
        doc.allocate_attribute("charge", str(doc, at.charge)));
    atom_node->append_attribute(
        doc.allocate_attribute("sigma", str(doc, at.sigma)));
    atom_node->append_attribute(
        doc.allocate_attribute("epsilon", str(doc, at.epsilon)));
  }
  // print XML to file
  std::stringstream ss;
  ss << doc;
  inout::file_open_put_stream(fn, ss);
}

ForceField &ForceField::parse_forcefield_file(
    const std::string
        &ff_file) { // read XML forcefield & topology from std::string

  using namespace rapidxml;

  dbgmsg("parsing focefield file ");

  char *c_ff_file = new char[ff_file.size() + 1];
  strcpy(c_ff_file, ff_file.c_str());
  xml_document<> doc;      // character type defaults to char
  doc.parse<0>(c_ff_file); // 0 means default parse flags
  // parse atom types
  for (xml_node<> *node =
           doc.first_node("ForceField")->first_node("AtomTypes")->first_node();
       node; node = node->next_sibling()) {
    const int name = stoi(node->first_attribute("name")->value());
    if (this->atom_type.contains(name))
      throw helper::Error(
          "[WHOOPS] ligands's types (names) overlap with receptor's "
          "(see xml file)");
    AtomType &at = this->atom_type[name];
    at.cl = node->first_attribute("class")->value();
    at.element = node->first_attribute("element")->value();
    at.mass = stod(node->first_attribute("mass")->value());
    dbgmsg("name = " << name << " class = " << at.cl << " at.element = "
                     << at.element << " at.mass = " << at.mass);
  }
  // parse residue topology
  for (xml_node<> *node =
           doc.first_node("ForceField")->first_node("Residues")->first_node();
       node; node = node->next_sibling()) {
    const std::string name = node->first_attribute("name")->value();
    dbgmsg("parsing residue topology name = " << name);
    ResidueTopology &rtop = this->residue_topology[name];

    std::vector<std::string> names;
    dbgmsg("before reading atoms first_node = "
           << node->first_node("Atom")->name());
    for (xml_node<> *node2 = node->first_node("Atom"); node2;
         node2 = node2->next_sibling("Atom")) {
      dbgmsg("parsing residue topology atom name = "
             << node2->first_attribute("name")->value()
             << " type = " << stoi(node2->first_attribute("type")->value()));
      rtop.atom[node2->first_attribute("name")->value()] =
          stoi(node2->first_attribute("type")->value());
      names.push_back(node2->first_attribute("name")->value());
    }
    dbgmsg("after reading atoms");
    for (xml_node<> *node2 = node->first_node("Bond"); node2;
         node2 = node2->next_sibling("Bond")) {
      const std::string atom_name1 =
          names[stoi(node2->first_attribute("from")->value())];
      const std::string atom_name2 =
          names[stoi(node2->first_attribute("to")->value())];
      dbgmsg("parsing residue topology bond atom_name1 = "
             << atom_name1 << " atom_name2 = " << atom_name2);
      rtop.bond[atom_name1][atom_name2] = true;
    }
    for (xml_node<> *node2 = node->first_node("ExternalBond"); node2;
         node2 = node2->next_sibling("ExternalBond")) {
      dbgmsg("parsing residue topology external_bond from = "
             << names[stoi(node2->first_attribute("from")->value())]);
      rtop.external_bond.insert(
          names[stoi(node2->first_attribute("from")->value())]);
    }
    dbgmsg("name = " << node->name());
  }
  // parse harmonic bonds
  for (xml_node<> *node = doc.first_node("ForceField")
                              ->first_node("HarmonicBondForce")
                              ->first_node();
       node; node = node->next_sibling()) {
    const std::string cl1 = node->first_attribute("class1")->value();
    const std::string cl2 = node->first_attribute("class2")->value();
    const bool can_constrain = (cl1.at(0) == 'H' || cl2.at(0) == 'H'
                                    ? true
                                    : false); // X-H bonds can be constrained
    this->bond_type[cl1][cl2] =
        BondType{stod(node->first_attribute("length")->value()),
                 stod(node->first_attribute("k")->value()), can_constrain};
    dbgmsg("harmonic bond force between "
           << cl1 << " and " << cl2 << " is "
           << " length = " << stod(node->first_attribute("length")->value())
           << " k = " << stod(node->first_attribute("k")->value())
           << " can_constrain = " << can_constrain);
  }
  // parse harmonic angles
  for (xml_node<> *node = doc.first_node("ForceField")
                              ->first_node("HarmonicAngleForce")
                              ->first_node();
       node; node = node->next_sibling()) {
    const std::string cl1 = node->first_attribute("class1")->value();
    const std::string cl2 = node->first_attribute("class2")->value();
    const std::string cl3 = node->first_attribute("class3")->value();
    this->angle_type[cl1][cl2][cl3] =
        AngleType{stod(node->first_attribute("angle")->value()),
                  stod(node->first_attribute("k")->value())};
    dbgmsg("harmonic angle force between "
           << cl1 << " and " << cl2 << " and " << cl3 << " is "
           << " angle = " << stod(node->first_attribute("angle")->value())
           << " k = " << stod(node->first_attribute("k")->value()));
  }
  // parse torsions
  xml_node<> *torsions_node =
      doc.first_node("ForceField")->first_node("PeriodicTorsionForce");
  if (torsions_node) {
    for (xml_node<> *node = torsions_node->first_node(); node;
         node = node->next_sibling()) {
      const std::string cl1 =
          (node->first_attribute("class1")->value_size() == 0
               ? "X"
               : node->first_attribute("class1")->value());
      const std::string cl2 =
          (node->first_attribute("class2")->value_size() == 0
               ? "X"
               : node->first_attribute("class2")->value());
      const std::string cl3 =
          (node->first_attribute("class3")->value_size() == 0
               ? "X"
               : node->first_attribute("class3")->value());
      const std::string cl4 =
          (node->first_attribute("class4")->value_size() == 0
               ? "X"
               : node->first_attribute("class4")->value());
      const bool is_proper = (string("Proper").compare(node->name()) == 0);
      dbgmsg("node_name = " << node->name() << " is_proper = " << is_proper);
      xml_attribute<> *attr = node->first_attribute("periodicity1");
      while (attr) {
        const int periodicity = stoi(attr->value());
        attr = attr->next_attribute();
        const double phase = stod(attr->value());
        attr = attr->next_attribute();
        const double k = stod(attr->value());
        attr = attr->next_attribute();
        if (is_proper) {
          TorsionTypeVec &dihedral = this->torsion_type[cl1][cl2][cl3][cl4];
          dihedral.push_back(TorsionType{periodicity, phase, k});
          dbgmsg("periodic torsion (dihedral) force between "
                 << cl1 << " and " << cl2 << " and " << cl3 << " and " << cl4
                 << " is "
                 << " periodicity" << dihedral.size() << " = " << periodicity
                 << " phase" << dihedral.size() << " = " << phase << " k"
                 << dihedral.size() << " = " << k);
        } else {
          TorsionTypeVec &improper = this->improper_type[cl2][cl3][cl1][cl4];
          improper.push_back(TorsionType{periodicity, phase, k});
          dbgmsg("periodic torsion (improper) force between "
                 << cl1 << " and " << cl2 << " and " << cl3 << " and " << cl4
                 << " is "
                 << " periodicity" << improper.size() << " = " << periodicity
                 << " phase" << improper.size() << " = " << phase << " k"
                 << improper.size() << " = " << k
                 << " (here the FIRST atom is central)");
        }
      }
    }
  }
  dbgmsg("before parsing nonbonded force");
  // parse nonbonded force
  xml_node<> *nb_node =
      doc.first_node("ForceField")->first_node("NonbondedForce");
  if (nb_node) {
    this->coulomb14scale =
        stod(nb_node->first_attribute("coulomb14scale")->value());
    this->lj14scale = stod(nb_node->first_attribute("lj14scale")->value());
    for (xml_node<> *node = nb_node->first_node(); node;
         node = node->next_sibling()) {
      const int type = stoi(node->first_attribute("type")->value());
      AtomType &at = this->atom_type[type];
      at.charge = stod(node->first_attribute("charge")->value());
      at.sigma = stod(node->first_attribute("sigma")->value());
      at.epsilon = stod(node->first_attribute("epsilon")->value());
      dbgmsg("nonbonded force for atom type "
             << type << " charge = " << at.charge << " sigma = " << at.sigma
             << " epsilon = " << at.epsilon);
    }
  }
  delete[] c_ff_file;
  return *this;
}

} // namespace insilab::ommiface
