
#pragma once

#include "molib/details/common.hpp"

namespace insilab::molib {
class Atom;
class Molecule;
} // namespace insilab::molib

namespace insilab::ommiface {
class ForceField;

/**
 * Add bonds for standard residues based on the topology in the forcefield in
 * addition to main-chain bonds and disulfide bonds.
 *
 * @note we need this because bonds for proteins etc., are not in PDB!
 *
 * @param atoms a vector of pointers to atoms
 */
void connect_bonds_standard_residues(const std::vector<molib::Atom *> &atoms);

/**
 * Add bonds for standard residues based on the topology in the forcefield in
 * addition to main-chain bonds and disulfide bonds.
 *
 * @note we need this because bonds for proteins etc., are not in PDB!
 *
 * @param cont any molib container that has residues (will be modified)
 */
template <molib::details::HasGetAtoms T> void connect_bonds_standard_residues(const T &cont) {
  connect_bonds_standard_residues(cont.get_atoms());
}

/**
 * Renames some residues (e.g., HIS => HIP) according to Amber forcefield. Adds
 * bonds for standard residues based on the topology in the forcefield in
 * addition to main-chain bonds and disulfide bonds.
 *
 * @note we need this because bonds for proteins etc., are not in PDB!
 *
 * @param atoms a vector of pointers to atoms
 */
void prepare_for_mm(const std::vector<molib::Atom *> &atoms);

/**
 * Renames some residues (e.g., HIS => HIP) according to Amber forcefield. Adds
 * bonds for standard residues based on the topology in the forcefield in
 * addition to main-chain bonds and disulfide bonds.
 *
 * @note we need this because bonds for proteins etc., are not in PDB!
 *
 * @param cont any molib container that has residues (will be modified)
 */
template <molib::details::HasGetAtoms T> void prepare_for_mm(const T &cont) {
  prepare_for_mm(cont.get_atoms());
}

/// Renames residues from Amber back to PDB-style (leaves assigned bonds for
/// standard residues intact)
void undo_mm_specific(const std::vector<molib::Atom *> &atoms);

/// Renames residues from Amber back to PDB-style (leaves assigned bonds for
/// standard residues intact)
template <molib::details::HasGetAtoms T>
void undo_mm_specific(const T &cont) {
  undo_mm_specific(cont.get_atoms());
}

} // namespace insilab::ommiface
