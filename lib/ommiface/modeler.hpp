#pragma once

#include "OpenMM.h"
#include "geom3d/coordinate.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "kbforce/openmmapi/include/KBForce.h"
#include "systemtopology.hpp"
#include "topology.hpp"
#include <map>
#include <set>
#include <string>
#include <vector>

namespace insilab::score {
// class ScoringFunction;
class ObjectiveFunction;
}; // namespace insilab::score

namespace insilab::ommiface {

class Modeler {
public:
  class MinimizationError : public helper::Error {
  public:
    MinimizationError(const std::string &msg) : Error(msg) {}
  };

  struct Options {
    ForceField *ffield;
    const score::ObjectiveFunction &objective;
    const molib::Atom::PVec &flexible_atoms;
    std::string fftype;
    double dist_cutoff;
    double mini_tol;
    int max_iterations;
    int update_freq;
    double position_tolerance;
    double force_tol;
    bool use_constraints;
    double step_size_in_fs;

    double get_dist_cutoff_in_nm() const {
      return dist_cutoff * OpenMM::NmPerAngstrom;
    }
    double get_position_tolerance_in_nm() const {
      return position_tolerance * OpenMM::NmPerAngstrom;
    }
    double get_step_size_in_ps() const {
      return step_size_in_fs * OpenMM::PsPerFs;
    }
  };

private:
  Options __opts;

  geom3d::Point::Vec __positions;
  Topology __topology;
  SystemTopology __system_topology;

  double __get_max_dist_change(const std::vector<OpenMM::Vec3> &,
                               const std::vector<OpenMM::Vec3> &) const;

public:
  Modeler(Options &opts) : __opts(opts) {}

  void mask(const molib::Atom::PVec &atoms);
  void unmask(const molib::Atom::PVec &atoms);
  void unmask_flexible();

  void add_topology(const molib::Atom::PVec &atoms);
  void add_crds(const molib::Atom::PVec &atoms, const geom3d::Point::Vec &crds);
  void add_random_crds(const molib::Atom::PVec &atoms);

  geom3d::Point::Vec get_state(const molib::Atom::PVec &atoms);

  void minimize_state(const molib::Molecule &ligand,
                      const molib::Molecule &receptor
                      //#ifndef NDEBUG
                      //				    ,
                      // molib::ScoringFunction &score #endif
  );
  void minimize_knowledge_based(const molib::Molecule &ligand,
                                const molib::Molecule &receptor
                                //#ifndef NDEBUG
                                //					      ,
                                // molib::ScoringFunction &score #endif
  );
  void minimize_physical();

  void init_openmm_positions();
  void init_openmm();

  void set_max_iterations(const int max_iterations) {
    __opts.max_iterations = max_iterations;
  }

  static std::string print_complex(molib::Molecule &ligand,
                                   molib::Molecule &receptor,
                                   const double energy);
};

} // namespace insilab::ommiface
