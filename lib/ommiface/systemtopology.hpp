#pragma once

#include "OpenMM.h"
#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "kbforce/openmmapi/include/KBForce.h"
#include "molib/molecule.hpp"
#include "topology.hpp"
#include <map>
#include <set>
#include <string>
#include <vector>

namespace insilab::score {
class ObjectiveFunction;
}

namespace insilab::molib {
class Atom;
class Molecule;
} // namespace insilab::molib

namespace insilab::ommiface {

class ForceField;

class SystemTopology {
public:
  enum class Options { torsional = 1, nonbond = 2 };

private:
  OpenMM::System *system;
  OpenMM::Integrator *integrator;
  OpenMM::Context *context;

  OpenMM::HarmonicBondForce *bondStretch;
  OpenMM::HarmonicAngleForce *bondBend;
  OpenMM::PeriodicTorsionForce *bondTorsion;

  ForceField *__ffield;

  int __kbforce_idx;
  std::vector<bool> masked;
  std::vector<double> masses;

  class AtomPoint {
  private:
    const geom3d::Point __crd;
    molib::Atom &__atom;

  public:
    AtomPoint(const geom3d::Point &crd, molib::Atom &atom)
        : __crd(crd), __atom(atom) {}
    const geom3d::Point &crd() const { return __crd; }
    molib::Atom &get_atom() { return __atom; }

    typedef std::vector<std::unique_ptr<AtomPoint>> UPVec;
    typedef std::vector<AtomPoint *> PVec;
    typedef grid::Grid<AtomPoint> Grid;
  };

  struct ForceData {
    int force_idx, idx1, idx2, idx3, idx4;
    double length, angle;
    int periodicity;
    double phase, k;
  };
  std::vector<std::vector<ForceData>> bondStretchData, bondBendData,
      bondTorsionData;

public:
  SystemTopology()
      : system(nullptr), integrator(nullptr), context(nullptr),
        __kbforce_idx(-1) {}
  ~SystemTopology();
  static void loadPlugins();
  void mask(Topology &topology, const molib::Atom::PVec &atoms);
  void unmask(Topology &topology, const molib::Atom::PVec &atoms);

  void mask_forces(const int atom_idx, const std::set<int> &substruct);
  void unmask_forces(const int atom_idx, const std::set<int> &substruct);

  void init_integrator(const double step_size_in_ps);
  void init_particles(Topology &topology);

  void remove_kbforce_if_exists();
  void add_kbforce_if_not_exists(KBPlugin::KBForce *kbforce);

  Topology::Bonds
  update_nonbond_list(Topology &topology,
                      const std::vector<OpenMM::Vec3> &positions,
                      const double dist_cutoff);

  void update_knowledge_based_force(const score::ObjectiveFunction &objective,
                                    const Topology::Bonds &nonbondlist,
                                    Topology &topology,
                                    const std::vector<OpenMM::Vec3> &positions,
                                    const double dist_cutoff);

  void init_physics_based_force(Topology &topology);
  void init_bonded(Topology &topology, const bool use_constraints,
                   const molib::Atom::PVec &flexible_atoms);

  void init_positions(const geom3d::Point::Vec &crds);
  //~ const std::vector<OpenMM::Vec3>& get_positions_in_nm();
  std::vector<OpenMM::Vec3> get_positions_in_nm();
  std::vector<OpenMM::Vec3> get_forces();
  void minimize(const double tolerance, const double max_iterations);
  void set_forcefield(ForceField &ffield) { __ffield = &ffield; }
};

} // namespace insilab::ommiface
