#include "prepare_for_mm.hpp"
#include "amber10.hpp"
#include "forcefield.hpp"
#include "molib/atom.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecule.hpp"

namespace insilab::ommiface {

using namespace molib;

void connect_bonds_standard_residues(const Atom::PVec &atoms) {
  prepare_for_mm(atoms);
  undo_mm_specific(atoms);
}

void prepare_for_mm(const Atom::PVec &atoms) {

  static ForceField ffield{
      ForceField().parse_forcefield_file(data::amber10_dat)};

  Residue::PSet residues;
  for (const auto &patom : atoms)
    residues.insert(&patom->residue());

  const auto get_closest_atom_of = [](const Atom &atom1,
                                      const Atom::PVec &neighbors,
                                      const std::string &atom_name) {
    double min_dist = std::numeric_limits<double>::max();
    Atom *patom2 = nullptr;
    for (const auto &pa : neighbors) {
      if (pa->atom_name() == atom_name) {
        const auto d = atom1.crd().distance(pa->crd());
        if (d < min_dist) {
          min_dist = d;
          patom2 = pa;
        }
      }
    }
    return patom2;
  };

  const Atom::Grid grid(atoms);

  // Rename some residues
  for (auto &presidue : residues) {
    if (presidue->resn() == "HIS")
      presidue->set_resn("HIP");
    for (const auto &atom : *presidue) {
      // not foolproof, sometimes OXT is missing !!!
      if (atom.atom_name() == "OXT") {
        // last residue is renamed to CALA, CGLY,...
        presidue->set_resn("C" + presidue->resn());
        break;
      }
    }
  }

  // Add bonds inside residues to protein atoms according to the topology file.
  for (auto &presidue : residues) {
    const auto rtop = ffield.get_residue_topology(presidue->resn());
    if (!rtop) {
      std::cerr << "[NOTE] cannot find topology for residue "
                << presidue->resn()
                << " (is it a non-standard or hetero residue?)" << std::endl;
      continue;
    }
    for (auto &atom1 : *presidue) {
      if (rtop.value().get().bond.contains(atom1.atom_name())) {
        for (auto &atom2 : *presidue) {
          if (rtop.value()
                  .get()
                  .bond.at(atom1.atom_name())
                  .contains(atom2.atom_name())) {
            atom1.connect(atom2);
          }
        }
      }
    }
  }
  // Add main chain peptide and nucleic bonds
  // Add disulfide bonds
  for (auto &patom1 : atoms) {
    auto &atom1 = *patom1;
    Atom *patom2 = nullptr;
    if (atom1.atom_name() == "SG") {
      patom2 = get_closest_atom_of(atom1, grid.get_neighbors(atom1.crd(), 2.5),
                                   "SG");
    } else if (atom1.atom_name() == "N") {
      patom2 =
          get_closest_atom_of(atom1, grid.get_neighbors(atom1.crd(), 1.5), "C");
    } else if (atom1.atom_name() == "P") {
      patom2 = get_closest_atom_of(atom1, grid.get_neighbors(atom1.crd(), 2.0),
                                   "O3'");
    }
    if (patom2) {
      auto &atom2 = *patom2;
      Residue &residue1 = atom1.residue();
      Residue &residue2 = atom2.residue();
      if (&residue1 != &residue2) {
        atom1.connect(atom2);
        if (atom1.atom_name() == "SG") {
          residue1.set_resn("CYX");
          residue2.set_resn("CYX");
        }
      }
    }
  }

  // we have to reconnect the bond graph from scratch
  Bond::erase_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(atoms));
  Bond::connect_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(atoms));

  // Rename terminal residues
  for (auto &patom : atoms) {
    auto &atom = *patom;
    Residue &residue = atom.residue();
    if (residue.resn().size() == 3) {
      if (atom.atom_name() == "N" && !atom.is_adjacent("C")) {
        // the first residue is renamed to NALA, NGLY,...
        residue.set_resn("N" + residue.resn());
      } else if (atom.atom_name() == "C" && !atom.is_adjacent("N")) {
        // and last residue to CALA, CGLY,... (if not already)
        residue.set_resn("C" + residue.resn());
      }
    }
  }
}

void undo_mm_specific(const Atom::PVec &atoms) {
  Residue::PSet residues;
  for (const auto &patom : atoms)
    residues.insert(&patom->residue());
  for (const auto &presidue : residues) {
    if (presidue->resn().size() == 4)
      // NALA -> ALA, CALA -> ALA
      presidue->set_resn(presidue->resn().substr(1));
    if (presidue->resn() == "HIP")
      presidue->set_resn("HIS");
    if (presidue->resn() == "CYX")
      presidue->set_resn("CYS");
  }
}

} // namespace insilab::ommiface
