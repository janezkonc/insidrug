#pragma once

#include <map>
#include <optional>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {
class Molecule;
} // namespace insilab::molib

namespace insilab::ommiface {

struct ForceField {

  struct AtomType {
    std::string cl, element;
    double mass, charge, sigma, epsilon;
  };
  struct BondType {
    double length, k;
    bool can_constrain;
  };
  struct AngleType {
    double angle, k;
  };
  struct TorsionType {
    int periodicity;
    double phase, k;
  };

  using TorsionTypeVec = std::vector<TorsionType>;
  struct ResidueTopology {
    std::map<std::string, int> atom; // name, type
    std::map<const std::string, std::map<const std::string, bool>>
        bond;                            // from, to
    std::set<std::string> external_bond; // from
  };

  using Atoms = std::map<const int, AtomType>;
  using Bonds =
      std::map<const std::string, std::map<const std::string, BondType>>;
  using Angles = std::map<
      const std::string,
      std::map<const std::string, std::map<const std::string, AngleType>>>;
  using Torsions =
      std::map<const std::string,
               std::map<const std::string,
                        std::map<const std::string,
                                 std::map<const std::string, TorsionTypeVec>>>>;
  using Residues = std::map<const std::string, ResidueTopology>;

  ForceField(const double step_non_bond = 0.01);

  const AtomType &get_atom_type(const int type) const;
  const BondType &get_bond_type(const int type1, const int type2) const;
  const AngleType &get_angle_type(const int type1, const int type2,
                                  const int type3) const;
  const TorsionTypeVec &get_dihedral_type(const int type1, const int type2,
                                          const int type3,
                                          const int type4) const;
  const TorsionTypeVec &get_improper_type(const int type1, const int type2,
                                          const int type3,
                                          const int type4) const;
  const std::optional<std::reference_wrapper<const ResidueTopology>>
  get_residue_topology(const std::string &resn) const;

  const auto get_coulomb14scale() const { return coulomb14scale; }
  const auto get_lj14scale() const { return lj14scale; }
  const auto get_step() const { return step; }

  ForceField &parse_forcefield_file(const std::string &);
  void output_forcefield_file(const std::string &);
  ForceField &parse_gaff_dat_file();

  /**
   * Add non-standard residues to the forcefield topology.
   * 
   * @param molecule a molecule that we wish to add (e.g., a small molecule, cofactor)
   * @return reference to the (modified) forcefield
   */
  ForceField &insert_topology(const molib::Molecule &molecule);

  friend std::ostream &operator<<(std::ostream &stream, const ForceField &f);

private:
  double coulomb14scale;
  double lj14scale;
  double step; // for knowledge-based potential

  Atoms atom_type;
  Bonds bond_type;
  Angles angle_type;
  Torsions torsion_type, improper_type;
  Residues residue_topology;
};

} // namespace insilab::ommiface
