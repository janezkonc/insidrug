#include "modeler.hpp"
#include "forcefield.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "score/objectivefunction.hpp"
#include "score/scoringfunction.hpp"
#include "systemtopology.hpp"
#include "topology.hpp"
#include <stdlib.h>
#include <time.h>

namespace insilab::ommiface {

std::ostream &operator<<(std::ostream &os,
                         const std::vector<OpenMM::Vec3> &positions) {
  os << "SIZE OF POSITIONS = " << positions.size() << std::endl;
  os << "ELEMENTS :" << std::endl;
  for (auto &v : positions)
    os << std::setprecision(8) << std::fixed << v[0] << " " << v[1] << " "
       << v[2] << " length = " << sqrt(v.dot(v)) << std::endl;
  return os;
}

void Modeler::mask(const molib::Atom::PVec &atoms) {
  dbgmsg("Masking atoms " << atoms);
  __system_topology.mask(__topology, atoms);
}

void Modeler::unmask(const molib::Atom::PVec &atoms) {
  dbgmsg("Unmasking atoms " << atoms);
  __system_topology.unmask(__topology, atoms);
}

void Modeler::unmask_flexible() {
  dbgmsg("Unmasking atoms " << __opts.flexible_atoms);
  __system_topology.unmask(__topology, __opts.flexible_atoms);
}

void Modeler::add_topology(const molib::Atom::PVec &atoms) {
  __topology.add_topology(atoms, *__opts.ffield);
  __positions.resize(
      __topology.atoms.size()); // as many positions as there are atoms
}

void Modeler::add_crds(const molib::Atom::PVec &atoms,
                       const geom3d::Point::Vec &crds) {
  for (int i = 0; i < atoms.size(); ++i) {
    int idx = __topology.get_index(*atoms[i]);
    __positions[idx] = crds[i];
  }
}

void Modeler::add_random_crds(const molib::Atom::PVec &atoms) {
  srand(time(NULL));
  for (int i = 0; i < atoms.size(); ++i) {
    int idx = __topology.get_index(*atoms[i]);
    const double x = rand() % 100 + 1;
    const double y = rand() % 100 + 1;
    const double z = rand() % 100 + 1;
    __positions[idx] = geom3d::Point(x, y, z);
  }
}

/**
 * Changes coordinates of atoms
 */

geom3d::Point::Vec Modeler::get_state(const molib::Atom::PVec &atoms) {

  const std::vector<OpenMM::Vec3> &positions_in_nm =
      __system_topology.get_positions_in_nm();
  geom3d::Point::Vec crds;
  for (int i = 0; i < atoms.size(); ++i) {
    int idx = __topology.get_index(*atoms[i]);
    crds.push_back(
        geom3d::Point(positions_in_nm[idx][0] * OpenMM::AngstromsPerNm,
                      positions_in_nm[idx][1] * OpenMM::AngstromsPerNm,
                      positions_in_nm[idx][2] * OpenMM::AngstromsPerNm));
  }

  return crds;
}

void Modeler::minimize_state(const molib::Molecule &ligand,
                             const molib::Molecule &receptor
#ifndef NDEBUG
                             ,
                             molib::ScoringFunction &score
#endif
) {
  if (__opts.fftype == "kb")
    minimize_knowledge_based(ligand, receptor
#ifndef NDEBUG
                             ,
                             score
#endif
    );
  else if (__opts.fftype == "phy")
    minimize_physical();
}

void Modeler::minimize_physical() {
  helper::Benchmark b;
  std::clog << "Doing energy minimization using physical forcefield"
            << std::endl;

  __system_topology.minimize(__opts.mini_tol, __opts.max_iterations);

  std::clog << "Minimization took " << b.duration() << "s" << std::endl;
}

double Modeler::__get_max_dist_change(
    const std::vector<OpenMM::Vec3> &first_positions,
    const std::vector<OpenMM::Vec3> &second_positions) const {
  double max_distance_change = 0;
  for (int i = 0; i < first_positions.size(); ++i) {
    OpenMM::Vec3 dif_pos = first_positions[i] - second_positions[i];
    const double distance_change = dif_pos.dot(dif_pos);
    if (distance_change > max_distance_change)
      max_distance_change = distance_change;
  }
  return sqrt(max_distance_change);
}

void Modeler::minimize_knowledge_based(const molib::Molecule &ligand,
                                       const molib::Molecule &receptor
#ifndef NDEBUG
                                       ,
                                       molib::ScoringFunction &score
#endif
) {
  helper::Benchmark b;
  std::clog << "Doing energy minimization of ligand " << ligand.name()
            << " using knowledge-based forcefield" << std::endl;

  // for knowledge-based forcefield we implement a custom update nonbond
  // function
  int iter = 0;

  const double buffer_distance_in_nm = 2.0 * OpenMM::NmPerAngstrom;

  // do a brief relaxation of bonded forces initially (without non-bonded
  // forces)
  __system_topology
      .remove_kbforce_if_exists(); //...from a previous conformation
  __system_topology.minimize(__opts.mini_tol, 20);

  // get initial positions after brief minimization
  std::vector<OpenMM::Vec3> initial_positions =
      __system_topology.get_positions_in_nm();
  dbgmsg(
      "initial_positions (after brief minimization) = " << initial_positions);

  // check if minimization failed
  if (std::isnan(initial_positions[0][0]))
    throw MinimizationError(
        "[WHOOPS] minimization failed (initial bonded relaxation)");

  // prepare the first nonbonded list
  Topology::Bonds nonbondlist = __system_topology.update_nonbond_list(
      __topology, initial_positions,
      (__opts.get_dist_cutoff_in_nm() + buffer_distance_in_nm) *
          OpenMM::AngstromsPerNm);

  // set the nonbonded force
  __system_topology.update_knowledge_based_force(__opts.objective, nonbondlist,
                                                 __topology, initial_positions,
                                                 __opts.get_dist_cutoff_in_nm());
  std::vector<OpenMM::Vec3> update_positions = initial_positions;

  while (iter < __opts.max_iterations) {

    dbgmsg("starting minimization step = " << iter);
    dbgmsg("initial_positions = " << initial_positions);

#ifndef NDEBUG
    // output frames during minimization
    molib::Molecule minimized_receptor(receptor,
                                       get_state(receptor.get_atoms()));
    molib::Molecule minimized_ligand(ligand, get_state(ligand.get_atoms()));

    minimized_receptor.undo_mm_specific();

    molib::Atom::Grid gridrec(minimized_receptor.get_atoms());

    const double energy = score.non_bonded_energy(minimized_ligand, gridrec);
    inout::output_file(
        Modeler::print_complex(minimized_ligand, minimized_receptor, energy),
        ligand.name() + "_frames" + ".pdb", {}, ios_base::app);
#endif

    __system_topology.minimize(__opts.mini_tol, __opts.update_freq);
    const std::vector<OpenMM::Vec3> &minimized_positions =
        __system_topology.get_positions_in_nm();

    // check if minimization failed
    if (std::isnan(minimized_positions[0][0]))
      throw MinimizationError("[WHOOPS] minimization failed (in loop)");

    dbgmsg("minimized_positions = " << minimized_positions);
#ifndef NDEBUG
    const std::vector<OpenMM::Vec3> &forces = __system_topology.get_forces();
    dbgmsg("forces after minimization = " << forces);
#endif

    // check if positions have converged
    double max_distance_change_prev_step =
        __get_max_dist_change(initial_positions, minimized_positions);
    double max_distance_change_last_update =
        __get_max_dist_change(update_positions, minimized_positions);

    // stop if convergence reached
    if (max_distance_change_prev_step < __opts.get_position_tolerance_in_nm()) {
      dbgmsg(
          "Convergence reached (position_tolerance) - minimization finished");
      break;
    }

    // if the coordinates changed by more than 1.0 A (=2.0 A / 2) from last
    // update, then update the nonbond list
    if (max_distance_change_last_update >= buffer_distance_in_nm / 2) {

      dbgmsg("updating nonbond list at iteration "
             << iter << " maximum distance change is "
             << max_distance_change_last_update
             << " buffer distance in nm = " << buffer_distance_in_nm);

      nonbondlist = __system_topology.update_nonbond_list(
          __topology, minimized_positions,
          (__opts.get_dist_cutoff_in_nm() + buffer_distance_in_nm) *
              OpenMM::AngstromsPerNm);
      update_positions = minimized_positions;
    }

    // update knowledge-based force
    __system_topology.update_knowledge_based_force(
        __opts.objective, nonbondlist, __topology, minimized_positions,
        __opts.get_dist_cutoff_in_nm());
    iter += __opts.update_freq;

    initial_positions = minimized_positions;
    dbgmsg("ending minimization step iter = " << iter);
  }

  // check if forces are OK
  const double ftol = pow(__opts.force_tol, 2);
  const std::vector<OpenMM::Vec3> &forces = __system_topology.get_forces();

  dbgmsg("forces after minimization = " << forces);
  for (auto &v3 : forces) {
    if (v3.dot(v3) > ftol)
      throw MinimizationError("[WHOOPS] forces far from minimum");
  }

  std::clog << "Minimized in " << iter << " iterations, which took "
            << b.duration() << "s" << std::endl;
}

void Modeler::init_openmm_positions() {
#ifndef NDEBUG
  dbgmsg("init_openmm_positions");
  for (auto &point : __positions)
    dbgmsg(point);
#endif
  __system_topology.init_positions(__positions);
}

void Modeler::init_openmm() {
  __system_topology.set_forcefield(*__opts.ffield);
  __system_topology.init_particles(__topology);
  __system_topology.init_bonded(__topology, __opts.use_constraints,
                                __opts.flexible_atoms);
  __system_topology.init_integrator(__opts.get_step_size_in_ps());

  if (__opts.fftype == "kb") {

    // do nothing since force will be initialized when minimization starts

  } else if (__opts.fftype == "phy") {

    __system_topology.init_physics_based_force(__topology);

  } else {
    throw helper::Error("[WHOOPS] unsupported forcefield");
  }
}

std::string Modeler::print_complex(molib::Molecule &ligand,
                                   molib::Molecule &receptor,
                                   const double energy) {
  std::stringstream ss;
  ss << "REMARK   1 MINIMIZED COMPLEX OF " << ligand.name() << " AND "
     << receptor.name() << " WITH SCORE OF " << energy << std::endl;
  ss << "MODEL" << std::endl;
  int reenum = 0;
  for (auto &patom : receptor.get_atoms()) {
    ss << std::setw(66) << std::left << *patom;
    reenum = patom->atom_number();
  }
  ss << "TER" << std::endl;
  for (auto &patom : ligand.get_atoms()) {
    patom->set_atom_number(++reenum);
    ss << std::setw(66) << std::left << *patom;
  }
  //~ ss << get_bonds_in(ligand.get_atoms());
  ss << "ENDMDL" << std::endl;
  return ss.str();
}

} // namespace insilab::ommiface
