#pragma once
#include "helper/error.hpp"

namespace insilab::ommiface {

class ParameterError : public helper::Error {
public:
  ParameterError(const std::string &msg) : Error(msg) {}
};

} // namespace insilab::ommiface
