#pragma once

#include "OpenMM.h"
#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "molib/molecule.hpp"
#include <map>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {
class Atom;
class Molecule;
}; // namespace insilab::molib

namespace insilab::ommiface {

using namespace molib;

class ForceField;

class Topology {
public:
  typedef std::set<std::pair<Atom *, Atom *>> BondedExclusions;
  typedef std::vector<std::pair<Atom *, Atom *>> Bonds;
  typedef std::vector<std::tuple<Atom *, Atom *, Atom *>> Angles;
  typedef std::vector<std::tuple<Atom *, Atom *, Atom *, Atom *>> Dihedrals;
  Atom::PVec atoms;
  Bonds bonds;
  Angles angles;
  Dihedrals dihedrals, impropers;
  BondedExclusions bonded_exclusions;

private:
  std::map<const Atom *, const int> atom_to_type;
  std::map<const Atom *, const int> atom_to_index;

public:
  ~Topology() { dbgmsg("calling destructor of Topology"); }

  Topology &add_topology(const Atom::PVec &atoms, const ForceField &ffield);
  int get_index(const Atom &atom) const { return atom_to_index.at(&atom); }
  int get_type(const Atom &atom) const { return atom_to_type.at(&atom); }
};

std::ostream &operator<<(std::ostream &stream,
                         const Topology::BondedExclusions &bonds);
std::ostream &operator<<(std::ostream &stream, const Topology::Bonds &bonds);
std::ostream &operator<<(std::ostream &stream, const Topology::Angles &angles);
std::ostream &operator<<(std::ostream &stream,
                         const Topology::Dihedrals &dihedrals);

} // namespace insilab::ommiface
