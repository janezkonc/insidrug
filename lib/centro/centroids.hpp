#pragma once

#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
}

namespace insilab::centro {

/**
 * Write centroid file either as a text file (.cen) or as a .json file.
 *
 * @param centroid_file a string, a file name ending with .cen or .json (can be
 * gzipped)
 * @param cen_j a Json array, in which each element has key 'data': a 2-element
 * Json array, in which first element is a Json array of centroid centers
 * (x,y,z), and second element is Json array of centroid radiuses (r); key
 * 'bs_id': binding site identifier; key 'bsite_rest': binding site type
 */
void write_file(const std::string &centroid_file, const nlohmann::json &cen_j);

/**
 * Parse centroids from a text file (.cen) or from a Json file.
 *
 * @param centroid_file a string, a file name ending with .cen or .json (can be
 * gzipped)
 * @return a Json array, in which each element has key 'data': a 2-element Json
 * array, in which first element is a Json array of centroid centers (x,y,z),
 * and second element is Json array of centroid radiuses (r); key 'bs_id':
 * binding site identifier; key 'bsite_rest': binding site type
 */
nlohmann::json parse_file(const std::string &centroid_file);

} // namespace insilab::centro
