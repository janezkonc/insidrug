#include "centroids.hpp"
#include "cluster/greedy.hpp"
#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/benchmark.hpp"
#include "inout/inout.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "path/path.hpp"
#include <exception>
#include <iostream>
#include <numeric>

using namespace insilab::molib;

namespace insilab::centro {

/// Read a centroid from a file - can read only one centroid
nlohmann::json cen_to_json(const std::vector<std::string> &cen_lines_v) {
  std::pair<geom3d::Point::Vec, std::vector<double>> result;
  auto old_format = false;
  for (const auto &line : cen_lines_v) {
    if (line.contains("#") || line.empty())
      continue;
    if (line.contains("bs_id")) {
      std::cerr
          << "[NOTE] Centroid file is in the OLD format. It continues to work"
             "as of May 2024, since the ProBiS-Dock Database uses it, but it "
             "is scheduled to be deprecated in the future"
          << std::endl;
      old_format = true;
      continue;
    }
    std::stringstream ss(line);
    double x, y, z, r;
    if (old_format) {
      int bs_id;
      std::string rest;
      ss >> bs_id >> x >> y >> z >> r >> rest;
    } else {
      ss >> x >> y >> z >> r;
    }
    if (ss.fail())
      continue;
    result.first.push_back({std::stod(helper::dtos(x)),
                            std::stod(helper::dtos(y)),
                            std::stod(helper::dtos(z))});
    result.second.push_back(std::stod(helper::dtos(r)));
  }

  if (result.first.empty() || result.first.size() != result.second.size())
    throw helper::Error("[WHOOPS] Centroids could not be read from .cen file!");
  nlohmann::json result_j = {{
      {"data", result},
      {"bs_id", 0},
      {"bsite_rest", Residue::Type::notassigned},
  }};
  return result_j;
}

std::vector<std::string> json_to_cen(const nlohmann::json &cen_j) {
  std::vector<std::string> result;
  for (const auto &bsite_j : cen_j) {
    result.push_back(">>> " + bsite_j.at("bsite_rest").get<std::string>() +
                     " " +
                     std::to_string(bsite_j.at("bs_id").get<std::size_t>()));
    result.push_back("#x y z r");
    const auto sz = bsite_j.at("data").at(0).size();
    for (auto i{0uz}; i < sz; ++i) {
      const auto crd = bsite_j.at("data").at(0).at(i).get<geom3d::Coordinate>();
      const auto radius = bsite_j.at("data").at(1).at(i).get<double>();
      result.push_back(helper::to_string(crd.simple()) + " " +
                       helper::dtos(radius));
    }
  }
  return result;
}

void write_file(const std::string &centroid_file, const nlohmann::json &cen_j) {
  if (path::has_suffix(centroid_file, ".cen") ||
      path::has_suffix(centroid_file, ".cen.gz")) {
    inout::file_open_put_contents(centroid_file, json_to_cen(cen_j));
  } else if (path::has_suffix(centroid_file, ".json") ||
             path::has_suffix(centroid_file, ".json.gz")) {
    inout::output_file(cen_j, centroid_file);
  }
}

nlohmann::json parse_file(const std::string &centroid_file) {
  if (path::has_suffix(centroid_file, ".cen") ||
      path::has_suffix(centroid_file, ".cen.gz")) {
    return cen_to_json(inout::read_file(centroid_file).first);
  } else if (path::has_suffix(centroid_file, ".json") ||
             path::has_suffix(centroid_file, ".json.gz")) {
    const auto &result =
        nlohmann::json::parse(inout::read_file_to_str(centroid_file).first);
    if (result.empty())
      throw helper::Error(
          "[WHOOPS] Centroids could not be read from .json file!");
    return result;
  }
  throw helper::Error(
      "[WHOOPS] Not a valid filename for centroids input file!");
}

} // namespace insilab::centro
