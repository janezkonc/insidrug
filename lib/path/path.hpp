#pragma once

#include "helper/help.hpp"
#include <filesystem>
#include <string>

namespace insilab::path {

bool file_exists(const std::string &file_path);

template <typename... Args>
std::string join(Args... args) {
  return (std::filesystem::path(args) / ...).string();
}

std::string get_stem(const std::string &str);

template <typename T>
std::string insert_to_filename(const std::string &filename, T value) {
  std::string fn(filename);
  return fn.insert(fn.find_first_of("."),
                   std::string("_" + helper::to_string(value)));
}

template <typename T, typename... Targs>
std::string insert_to_filename(const std::string &filename, T value,
                               Targs... args) {
  std::string fn(insert_to_filename(filename, value));
  return insert_to_filename(fn, args...);
}

bool has_suffix(const std::string &filename, const std::string &suffix);

std::vector<std::string>
files_matching_pattern(const std::string &, const std::string &pattern = "");

} // namespace insilab::path
