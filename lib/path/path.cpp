#include "path.hpp"
#include <iostream>
#include <regex>
#include <stdio.h>
#include <string>

namespace insilab::path {

using namespace helper;

bool file_exists(const std::string &file_path) {
  std::filesystem::path p(file_path);
  return std::filesystem::exists(p);
}

std::string get_stem(const std::string &str) {
  std::filesystem::path p(str);
  return p.stem().string();
}

/**
 * Determine if filename has suffix. Suffix may be composed of multiple
 * sub-suffixes separated with dots, eg., .srf.gz, .all.pdb.gz, etc., for
 * example, suffix = .srf.gz and filename = janez.konc.srf.gz have the same
 * suffix.
 * @return true if filename has suffix
 */
bool has_suffix(const std::string &filename, const std::string &suffix) {

  std::string sfx = "";
  std::string stem = filename;
  for (int i = 0; i < std::count(suffix.begin(), suffix.end(), '.'); ++i) {
    sfx = std::filesystem::path(stem).extension().string() + sfx;
    stem = std::filesystem::path(stem).stem();
    if (sfx == suffix)
      return true;
  }
  return false;
}

std::vector<std::string> files_matching_pattern(const std::string &file_or_dir,
                                                const std::string &pattern) {

  std::vector<std::string> fn;
  std::filesystem::path p(file_or_dir);

  if (std::filesystem::is_directory(p)) {

    std::vector<std::filesystem::path> files;
    std::copy(std::filesystem::directory_iterator(p),
              std::filesystem::directory_iterator(),
              std::back_inserter(files)); // get all files in directoy...

    for (auto &p : files) {
      fn.push_back(p.string());
    }

  } else {
    throw helper::Error("[WHOOPS] cannot open directory " + p.string());
  }

  std::vector<std::string> filenames;
  for (const std::string &s : fn) {
    if (std::regex_search(s, std::regex(pattern))) {
      filenames.push_back(s);
    }
  }
  if (filenames.empty())
    throw helper::Error("[WHOOPS] " + file_or_dir +
                        " does not contain any files...");
  return filenames;
}

} // namespace insilab::path
