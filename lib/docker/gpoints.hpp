#pragma once

#include "centro/centroids.hpp"
#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/array1d.hpp"
#include "helper/array3d.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
class Molecules;
} // namespace insilab::molib

namespace insilab::score {
class ScoringFunction;
}

namespace insilab::docker {

class Gpoints {
public:
  struct IJK {
    int i, j, k;
    IJK operator+(const IJK &right) const {
      return IJK{i + right.i, j + right.j, k + right.k};
    }
    IJK operator-(const IJK &right) const {
      return IJK{i - right.i, j - right.j, k - right.k};
    }
    friend std::ostream &operator<<(std::ostream &os, const IJK &ijk);
  };

  struct Gpoint {
    geom3d::Point __crd;
    IJK __ijk;
    helper::Array1d<double> __energy; // ligand idatm type to energy
    geom3d::Point &crd() { return __crd; }
    const geom3d::Point &crd() const { return __crd; }
    double energy(const int l) const { return __energy.data[l]; }
    IJK &ijk() { return __ijk; }
    friend std::ostream &operator<<(std::ostream &os, const Gpoint &gpoint);
  };

  typedef std::vector<Gpoint> GpointVec;
  typedef std::vector<Gpoint *> PGpointVec;

private:
  std::map<int, GpointVec> __gridpoints;
  std::map<int, helper::Array3d<Gpoint *>> __gmap;

public:
  void identify_gridpoints(const double &grid_spacing,
                           const double &radial_check);

  void identify_gridpoints(const molib::Molecule &receptor,
                           const molib::Molecules &template_ligands,
                           const nlohmann::json &centroids,
                           const score::ScoringFunction &score,
                           const std::set<int> &ligand_idatm_types,
                           const double &grid_spacing, const int &dist_cutoff,
                           const double &excluded_radius,
                           const double &max_interatomic_distance,
                           const double &dist_cutoff_probis_template);

  GpointVec &get_gridpoints0() {
    try {
      return __gridpoints.at(0);
    } catch (const std::out_of_range &oor) {
      throw helper::Error("[WHOOPS] no gridpoints0 ?");
    }
  }
  const GpointVec &get_gridpoints0() const {
    try {
      return __gridpoints.at(0);
    } catch (const std::out_of_range &oor) {
      throw helper::Error("[WHOOPS] no gridpoints0 ?");
    }
  }

  std::map<int, GpointVec> &get_gridpoints() { return __gridpoints; }
  const Gpoint &get_center_point() const;

  helper::Array3d<Gpoint *> &get_gmap(const int bsite_id) {
    try {
      return __gmap.at(bsite_id);
    } catch (const std::out_of_range &oor) {
      throw helper::Error("[WHOOPS] cannot get gmap for bsite #" +
                          helper::to_string(bsite_id));
    }
  }

  /// For a given coordinate, find the closest gridpoint (within certain
  /// distance cutoff) and return its (i,j,k) coordinate.
  static IJK crd_to_ijk(const grid::Grid<Gpoint> &, const geom3d::Point &,
                        const double);

  friend std::ostream &operator<<(std::ostream &os, const Gpoints &gpoints);
};

std::ostream &operator<<(std::ostream &os,
                         const docker::Gpoints::GpointVec &points);
std::ostream &operator<<(std::ostream &os,
                         const docker::Gpoints::PGpointVec &points);

} // namespace insilab::docker
