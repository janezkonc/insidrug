#include "gpoints.hpp"
#include "centro/centroids.hpp"
#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/benchmark.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "score/scoringfunction.hpp"
#include <exception>
#include <iostream>

namespace insilab::docker {

using namespace molib;
using namespace score;

std::ostream &operator<<(std::ostream &os, const Gpoints::IJK &ijk) {
  os << ijk.i << " " << ijk.j << " " << ijk.k << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os, const Gpoints::Gpoint &gpoint) {
  os << gpoint.__crd << std::endl;
  os << gpoint.__ijk << std::endl;
  os << gpoint.__energy << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os, const Gpoints::PGpointVec &points) {
  for (auto &ppoint : points) {
    // to add : output of energies
    os << "ATOM      1   U  DIK     1    " << ppoint->crd().pdb() << std::endl;
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const Gpoints::GpointVec &points) {
  for (auto &point : points) {
    // to add : output of energies
    os << "ATOM      1   U  DIK     1    " << point.crd().pdb() << std::endl;
    //			   << std::setw(6) << std::setprecision(2) << 1.0
    //			   << std::setw(6) << std::setprecision(2) << std::fixed
    //<<
    // point.energy(22)
    //<< std::endl; // Car energy
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const Gpoints &gpoints) {
  for (auto &kv : gpoints.__gridpoints) {
    const int bs_id = kv.first;
    os << "MODEL" << std::setw(9) << std::right << bs_id << std::endl;
    os << kv.second;
    os << "ENDMDL" << std::endl;
  }
  return os;
}

const Gpoints::Gpoint &Gpoints::get_center_point() const {
  geom3d::Point center(0, 0, 0);
  double min_d = std::numeric_limits<double>::max();
  const Gpoints::Gpoint *center_point = nullptr;
  for (auto &p : get_gridpoints0()) {
    const double d = p.crd().distance(center);
    if (d < min_d) {
      min_d = d;
      center_point = &p;
    }
  }
  if (!center_point)
    throw helper::Error("[WHOOPS] central point cannot be found");
  return *center_point;
}

void Gpoints::identify_gridpoints(
    const Molecule &receptor, const Molecules &template_ligands,
    const nlohmann::json &centroids, const ScoringFunction &score,
    const std::set<int> &ligand_idatm_types, const double &grid_spacing,
    const int &dist_cutoff, const double &excluded_radius,
    const double &max_interatomic_distance,
    const double &dist_cutoff_probis_template) {

  helper::Benchmark b;

  const Atom::Grid gridrec(receptor.get_atoms());
  const Atom::Grid gridprobis(template_ligands.get_atoms());

  if (centroids.empty())
    throw helper::Error("[WHOOPS] Centroids are empty!");

  for (const auto &d : centroids) {
    const auto bs_id = d.at("bs_id").get<std::size_t>();
    const auto &cp = d.at("data");
    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();

    dbgmsg("calculating grid for binding type " << bsite_rest);
    dbgmsg("binding site id = " << bs_id);

    geom3d::Coordinate min(std::numeric_limits<double>::max(),
                           std::numeric_limits<double>::max(),
                           std::numeric_limits<double>::max());
    geom3d::Coordinate max(std::numeric_limits<double>::lowest(),
                           std::numeric_limits<double>::lowest(),
                           std::numeric_limits<double>::lowest());

    // find the minimium and maximum coordinates of this binding site
    for (auto i{0uz}; i < crds.size(); ++i) {
      const auto &centroid = crds[i];
      const auto &radial_check = radiuses[i];

      geom3d::Coordinate min2 = centroid - ceil(radial_check);
      geom3d::Coordinate max2 = centroid + ceil(radial_check);

      if (min2.x() < min.x())
        min.set_x(min2.x());
      if (min2.y() < min.y())
        min.set_y(min2.y());
      if (min2.z() < min.z())
        min.set_z(min2.z());
      if (max2.x() > max.x())
        max.set_x(max2.x());
      if (max2.y() > max.y())
        max.set_y(max2.y());
      if (max2.z() > max.z())
        max.set_z(max2.z());
    }
    dbgmsg("min point = " << min.pdb());
    dbgmsg("max point = " << max.pdb());
    const int total_gridpoints = 3 * ceil((max.x() - min.x()) / grid_spacing) *
                                 ceil((max.y() - min.y()) / grid_spacing) *
                                 ceil((max.z() - min.z()) / grid_spacing);
    std::clog << "approximately " << total_gridpoints
              << " gridpoints to evaluate\n\n";
    int points_kept = 0;
    int gridpoint_counter = 0;
    const double r = grid_spacing / 2;
    const double max_d = min.distance(max); // distance between min and max
    const int last_column = ceil(max_d / r);
    const int last_row = ceil(max_d / (sqrt(3) * r));
    const int last_layer = ceil(max_d / (2 * r * sqrt(6) / 3));

    // initialize mapping between gridpoints and discretized 3D space
    __gmap[bs_id].init(last_column + 1, last_row + 1, last_layer + 1);
    dbgmsg("gmap szi = " << __gmap[bs_id].szi << " szj = " << __gmap[bs_id].szj
                         << " szk = " << __gmap[bs_id].szk);
    geom3d::Coordinate eval;
    for (int column = 0; column <= last_column; column++) {
      int even_column = (column % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
      for (int row = 0; row <= last_row; row++) {
        int even_row = (row % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
        for (int layer = 0; layer <= last_layer; layer++) {
          int even_layer = (layer % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
          if ((even_column == 0 && even_row == 0) ||
              (even_column == 1 && even_row == 1)) {
            if (even_layer == 1) {
              eval.set_x(min.x() + column * r);
              eval.set_y(min.y() + sqrt(3) * r * row);
              eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
            } else {
              eval.set_x(min.x() + r + column * r);
              eval.set_y(min.y() + r / sqrt(3) + sqrt(3) * r * row);
              eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
            }

            // mark that this point is not accessible
            //~ assert(column < __gmap.szi);
            //~ assert(row < __gmap.szj);
            //~ assert(layer < __gmap.szk);
            __gmap[bs_id].data[column][row][layer] = nullptr;

            // if the point is within the radial_check of ANY of the
            // centroids...
            bool is_within_r_of_c = false;

            for (auto i{0uz}; i < crds.size(); ++i) {
              const auto &centroid = crds[i];
              const auto &radial_check = radiuses[i];
              if (eval.distance(centroid) <= radial_check) {
                is_within_r_of_c = true;
                break;
              }
            }
            if (is_within_r_of_c) {

              bool okay_min = true;
              bool okay_max = true;

              double closest = std::numeric_limits<double>::max();

              for (molib::Atom *a : gridrec.get_neighbors(eval, dist_cutoff)) {
                molib::Atom &atom = *a;
                //~ dbgmsg("before getting atom radius");
                const double vdW = atom.radius();
                //~ dbgmsg("vdW = " << vdW);
                const double eval_dist = excluded_radius + 0.9 * vdW;
                const double distance = atom.crd().distance(eval);
                if (distance <= eval_dist) {
                  okay_min = false;
                  dbgmsg("distance = " << distance
                                       << " eval_dist = " << eval_dist);
                  break;
                } else {
                  okay_min = true;
                  if (distance < closest)
                    closest = distance;
                }
              }

              // points inside the receptor are accepted nonetheless if they
              // are near template probis ligand atoms. grid thus stretches ~
              // 2 A into the receptor atoms...

              bool bumped = false;

              if (!okay_min && gridprobis.has_neighbor_within(
                                   eval, dist_cutoff_probis_template)) {
                okay_min = true;
                bumped = true;
              }

              if (closest > max_interatomic_distance)
                okay_max = false;

              if (okay_min && okay_max) {
                dbgmsg("before adding to __gridpoints");

                __gridpoints[bs_id].push_back(
                    Gpoint{eval, IJK{column, row, layer},
                           score.compute_energy(eval, ligand_idatm_types,
                                                gridrec, bumped)});

                dbgmsg("really out");

                points_kept++;
              }
            }
          }
          const int mod = gridpoint_counter % 10000;
          if (mod == 0) {
            dbgmsg("Processing gridpoint "
                   << gridpoint_counter << " of approximately "
                   << total_gridpoints << " (took " << b.duration() << "s)");
            b.reset();
          }
          gridpoint_counter++;
        }
        dbgmsg("column = " << column);
      }
    }
    // the last ones that did not get to the next mod==0
    std::clog << points_kept << " points kept out of " << gridpoint_counter
              << " total gridpoints\n";
  }

  // initialize gmap data here, because push_back can invalidate pointers...
  dbgmsg("initializing gmap");
  for (auto &kv : __gridpoints) {
    const int bs_id = kv.first;
    dbgmsg("binding site = " << bs_id
                             << " number of points = " << kv.second.size());
    for (auto &gpoint : kv.second) {
      __gmap[bs_id].data[gpoint.ijk().i][gpoint.ijk().j][gpoint.ijk().k] =
          &gpoint;
    }
  }
}

void Gpoints::identify_gridpoints(const double &grid_spacing,
                                  const double &radial_check) {

  helper::Benchmark b;

  // find the minimium and maximum coordinates
  geom3d::Point center(0, 0, 0);

  geom3d::Coordinate min = center - ceil(radial_check);
  geom3d::Coordinate max = center + ceil(radial_check);
  dbgmsg("min point = " << min.pdb());
  dbgmsg("max point = " << max.pdb());
  const int total_gridpoints = 3 * ceil((max.x() - min.x()) / grid_spacing) *
                               ceil((max.y() - min.y()) / grid_spacing) *
                               ceil((max.z() - min.z()) / grid_spacing);
  std::clog << "approximately " << total_gridpoints
            << " gridpoints to evaluate\n\n";
  int points_kept = 0;
  int gridpoint_counter = 0;
  const double r = grid_spacing / 2;
  const double max_d = min.distance(max); // distance between min and max
  const int last_column = ceil(max_d / r);
  const int last_row = ceil(max_d / (sqrt(3) * r));
  const int last_layer = ceil(max_d / (2 * r * sqrt(6) / 3));

  geom3d::Coordinate eval;
  for (int column = 0; column <= last_column; column++) {
    int even_column = (column % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
    for (int row = 0; row <= last_row; row++) {
      int even_row = (row % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
      for (int layer = 0; layer <= last_layer; layer++) {
        int even_layer = (layer % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
        if ((even_column == 0 && even_row == 0) ||
            (even_column == 1 && even_row == 1)) {
          if (even_layer == 1) {
            eval.set_x(min.x() + column * r);
            eval.set_y(min.y() + sqrt(3) * r * row);
            eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
          } else {
            eval.set_x(min.x() + r + column * r);
            eval.set_y(min.y() + r / sqrt(3) + sqrt(3) * r * row);
            eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
          }

          // if the point is within the radial_check of ANY of the centroids...
          if (eval.distance(center) <= radial_check) {
            __gridpoints[0].push_back(Gpoint{eval, IJK{column, row, layer}});
            dbgmsg("lastcolumn = " << last_column << " lastrow = " << last_row
                                   << " lastlayer = " << last_layer);
            dbgmsg("column = " << column << " row = " << row
                               << " layer = " << layer);
            dbgmsg("gridpoint0 = " << __gridpoints[0].back().ijk());
            points_kept++;
          }
        }
        const int mod = gridpoint_counter % 10000;
        if (mod == 0) {
          dbgmsg("Processing gridpoint "
                 << gridpoint_counter << " of approximately "
                 << total_gridpoints << " (took " << b.duration() << "s)");
          b.reset();
        }
        gridpoint_counter++;
      }
      dbgmsg("column = " << column);
    }
  }

  // center the ijk coordinates of grid points around the center point (0,0,0)
  Gpoint cp = this->get_center_point(); // here we copy by value intentionally
  dbgmsg("center point = " << cp.ijk());
  for (auto &point : this->get_gridpoints0()) {
    dbgmsg("point = " << point.ijk() << " address = " << &point);
    point.ijk() = point.ijk() - cp.ijk();
    dbgmsg("centered point = " << point.ijk());
  }

  // the last ones that did not get to the next mod==0
  std::clog << points_kept << " points kept out of " << gridpoint_counter
            << " total gridpoints\n";
}

Gpoints::IJK Gpoints::crd_to_ijk(const grid::Grid<Gpoints::Gpoint> &grid,
                                 const geom3d::Point &crd, const double tol) {
  const auto closest = grid.get_closest_including_self(crd, tol);
  if (closest.empty())
    throw helper::Error("[WHOOPS] A fragment coordinate is outside of grid!");
  return closest[0]->ijk();
}

}; // namespace insilab::docker
