#include "conformations.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/quaternion.hpp"
#include "glib/algorithms/mcqd.hpp"
#include "gpoints.hpp"
#include "grid/grid.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/geometry.hpp"
#include "molib/molecule.hpp"
#include <exception>
#include <iostream>

using namespace insilab::grid;

namespace insilab::docker {

std::ostream &operator<<(std::ostream &os,
                         const Conformations::Conformation &conf) {
  for (auto &atom : conf.get_atoms()) {
    os << atom.ijk() << std::endl;
    os << atom.crd() << std::endl;
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const Conformations &conformations) {
  for (auto &conf : conformations.get_conformations()) {
    os << "#CONFORMATION" << std::endl;
    os << conf;
  }
  return os;
}

Conformations::Conformations(const molib::Molecule &seed,
                             const double conf_spin, const int num_univec,
                             const double rmsd_tol, const double grid_spacing,
                             const double max_frag_radius)
    : __seed(seed) {

  try {

    // zero centered centroid to get conformations of each seed
    Gpoints gpoints;
    gpoints.identify_gridpoints(grid_spacing, max_frag_radius);

    const double conf_spin_in_radians = geom3d::radians(conf_spin / 2);

    helper::Benchmark b;

    // get uniform sphere points, i.e., unit vectors
    geom3d::Point::Vec unit_vectors = geom3d::uniform_sphere(num_univec);

    // get center point
    const geom3d::Point center =
        molib::algorithms::compute_geometric_center(seed);

    // get atom which is maximum distance from center
    const auto &another =
        molib::algorithms::find_most_distant_atom(seed, center);

    // calculate vector between the two atoms
    const geom3d::Point bondvec = another.crd() - center;

    geom3d::Point::Vec seed_crds = seed.get_crds();

    // center the seed points at origin
    for (auto &crd : seed_crds)
      crd = crd - center;

    // rotate seed on each unit_vector by increments of conf_spin_in_radians
    // degrees (in radians)
    std::vector<geom3d::Point::Vec> confs;
    for (auto &unitvec : unit_vectors) {

      geom3d::Vector3 ortho = geom3d::Coordinate::cross(bondvec, unitvec);
      const double rotangl = geom3d::angle(unitvec, bondvec) / 2;
      dbgmsg("rotangl = " << geom3d::degrees(rotangl)
                          << " seed = " << seed.name());
      const geom3d::Quaternion q0(ortho.norm() * sin(rotangl), cos(rotangl));

      // align seed crds along unit vector
      geom3d::Point::Vec previous;
      for (auto &crd : seed_crds) {
        previous.push_back(q0.rotatedVector(crd));
      }

      confs.push_back(previous);

#ifndef NDEBUG
      std::stringstream ss;
      ss << "MODEL" << std::endl
         << seed_crds << "ENDMDL" << std::endl
         << "MODEL" << std::endl
         << previous << "ATOM      1   U  UNI     2    " << unitvec.pdb()
         << std::endl
         << "ATOM      1   U  BON     3    " << bondvec.pdb() << std::endl
         << "ENDMDL" << std::endl;
      inout::output_file(ss.str(), "unit_" + seed.name() + ".pdb", {},
                         std::ios_base::app);
#endif

      const geom3d::Quaternion q(geom3d::Vector3(unitvec) *
                                     sin(conf_spin_in_radians),
                                 cos(conf_spin_in_radians));

      for (double angle = 2 * conf_spin_in_radians; angle < M_PI;
           angle += conf_spin_in_radians) {
        geom3d::Point::Vec rotated;
        for (auto &crd : previous) {
          rotated.push_back(q.rotatedVector(crd));
        }
        confs.push_back(rotated);
        previous = rotated;
      }
    }
    // pre-compute rmsd between ALL conformations
    __rmsd = helper::Array2d<bool>(confs.size());

    for (int i = 0; i < confs.size(); ++i) {
      dbgmsg("working on conformation " << i << " out of " << confs.size()
                                        << " with rmsd_tol of " << rmsd_tol);
      for (int j = i + 1; j < confs.size(); ++j) {
        if (geom3d::compute_rmsd(confs[i], confs[j]) < rmsd_tol) {
          __rmsd.set(i, j);
          __rmsd.set(j, i);
        }
      }
    }

    dbgmsg("after rmsd calculation");

    // map rotamer conformations to gridpoints
    Gpoints::PGpointVec pgvec;
    for (auto &point : gpoints.get_gridpoints0()) {
      pgvec.push_back(const_cast<Gpoints::Gpoint *>(&point));
    }

    Grid<Gpoints::Gpoint> grid(pgvec);

    dbgmsg("after creating grid");
    dbgmsg("conformation size = " << confs.size());

    // go over all conformations & get the closest gripoint
    for (int i = 0; i < confs.size(); ++i) {

      auto &conf = confs[i];
      Conformation one_conformation; // init size of vec!
      dbgmsg("conformation size = " << conf.size());
      for (auto &crd : conf) {

        auto ijk = Gpoints::crd_to_ijk(grid, crd, grid_spacing * 1.33);

        __conf_map[ijk.i][ijk.j][ijk.k].push_back(i);
        one_conformation.get_atoms().push_back(Conformation::Atom(ijk, crd));
      }
      // save conformation
      __conf_vec.push_back(one_conformation);
    }

    std::clog << "Finding " << __conf_vec.size() << " conformations of seed "
              << seed.name() << " took " << b.duration() << "s" << std::endl;

  } catch (...) {
    dbgmsg("FAILURE: constructor of Conformations failed ... cleaning "
           "resources...");
    throw;
  }
}

} // namespace insilab::docker
