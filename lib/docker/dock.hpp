#pragma once

#include "conformations.hpp"
#include "gpoints.hpp"
#include "molib/molecules.hpp"

namespace insilab::molib {
class Molecule;
};

namespace insilab::docker {

class Dock {
public:
  class DockedConf {
  public:
    typedef std::vector<DockedConf> Vec;
    typedef std::vector<DockedConf *> PVec;

  private:
    Gpoints::Gpoint &__cavpoint;
    Conformations::Conformation &__conf0;
    double __energy;
    int __i;
    geom3d::Point::Vec __real_crds;

  public:
    DockedConf(Gpoints::Gpoint &cavpoint, Conformations::Conformation &conf0,
               double energy, int i)
        : __cavpoint(cavpoint), __conf0(conf0), __energy(energy), __i(i) {}

    geom3d::Point &crd() { return __cavpoint.crd(); }
    const geom3d::Point &crd() const { return __cavpoint.crd(); }

    void set_real_crds() {
      for (auto &catom : __conf0.get_atoms())
        __real_crds.push_back(__cavpoint.crd() + catom.crd());
    }

    geom3d::Point::Vec &get_real_crds() { return __real_crds; }
    const geom3d::Point::Vec &get_real_crds() const { return __real_crds; }

    Gpoints::Gpoint &get_cavpoint() const { return __cavpoint; }

    Conformations::Conformation &get_conf0() const { return __conf0; }
    double get_energy() const { return __energy; }
    int get_i() const { return __i; }
    void set_i(int i) { __i = i; }

    struct CompareRMSDLessThanConstant {
      bool operator()(const DockedConf *x, const DockedConf *y,
                      const double rmsd_tol) const {
        return x->compute_rmsd_ord(*y) < rmsd_tol;
      }
    };

    struct CompareScoreLess {
      bool operator()(const DockedConf *x, const DockedConf *y) const {
        return x->get_energy() < y->get_energy();
      }
    };

    double compute_rmsd_ord(const DockedConf &other) const;
    friend std::ostream &operator<<(std::ostream &os,
                                    const DockedConf &dockedconf);
    friend std::ostream &operator<<(std::ostream &os,
                                    const DockedConf::Vec &dockedconf_vec);
  };

private:
  Gpoints &__gpoints;
  Conformations &__conformations;
  molib::Molecule __seed; // a copy, because we are changing atom crds
  double __rmsd_tol;

  molib::Molecules __docked;

  DockedConf::Vec __dock();
  //		DockedConf::Vec __cluster(const DockedConf::Vec &confs);
  void __cluster_fast(const DockedConf::Vec &conformations,
                      DockedConf::Vec &reps);
  void __set_docked(const DockedConf::Vec &confs);

public:
  Dock(Gpoints &gpoints, Conformations &conformations, molib::Molecule seed,
       const double rmsd_tol = 2.0)
      : __gpoints(gpoints), __conformations(conformations), __seed(seed),
        __rmsd_tol(rmsd_tol) {}

  Dock &run();

  molib::Molecules &get_docked() { return __docked; };
};

} // namespace insilab::docker
