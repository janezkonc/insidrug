#pragma once

#include "geom3d/linear.hpp"
#include "gpoints.hpp"
#include "helper/array2d.hpp"
#include "molib/molecule.hpp"
#include <map>
#include <vector>

namespace insilab::docker {

class Conformations {
public:
  typedef std::map<int, std::map<int, std::map<int, std::vector<int>>>> ConfMap;

  class Conformation {
  public:
    typedef std::vector<Conformation> Vec;

    class Atom {
      Gpoints::IJK __ijk;
      geom3d::Point __crd;

    public:
      Atom(Gpoints::IJK &ijk, geom3d::Point &crd) : __ijk(ijk), __crd(crd) {}
      geom3d::Point &crd() { return __crd; }

      const geom3d::Point &crd() const { return __crd; }
      Gpoints::IJK &ijk() { return __ijk; }

      const Gpoints::IJK &ijk() const { return __ijk; }
      typedef std::vector<Conformation::Atom> Vec;
    };

  private:
    Conformation::Atom::Vec __atoms;

  public:
    Conformation::Atom::Vec &get_atoms() { return __atoms; }
    const Conformation::Atom::Vec &get_atoms() const { return __atoms; }
    friend std::ostream &operator<<(std::ostream &os, const Conformation &conformation);
  };

private:
  const molib::Molecule &__seed;
  Conformation::Vec __conf_vec;

  ConfMap __conf_map;
  helper::Array2d<bool> __rmsd;

public:
  Conformations(const molib::Molecule &seed, const double conf_spin,
                const int num_univec, const double rmsd_tol,
                const double grid_spacing, const double max_frag_radius);

  Conformation::Vec &get_conformations() { return __conf_vec; }
  const Conformation::Vec &get_conformations() const { return __conf_vec; }

  ConfMap &get_confmap() { return __conf_map; }
  std::vector<int> &get_confs_at(const Gpoints::IJK &ijk) {
    return __conf_map[ijk.i][ijk.j][ijk.k];
  }

  bool rmsd_less_than_tol(const int i, const int j) const { return __rmsd.get(i, j); }
  friend std::ostream &operator<<(std::ostream &os, const Conformations &conformations);
};

} // namespace insilab::docker
