#include "dock.hpp"
#include "cluster/greedy.hpp"
#include "conformations.hpp"
#include "geom3d/linear.hpp"
#include "gpoints.hpp"
#include "grid/grid.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "score/scoringfunction.hpp"
#include <exception>
#include <iostream>

namespace insilab::docker {

std::ostream &operator<<(std::ostream &os,
                         const Dock::DockedConf &docked_conf) {
  os << "cavpoint " << docked_conf.__cavpoint << std::endl;
  os << "conf0 " << docked_conf.__conf0 << std::endl;
  os << "energy " << docked_conf.__energy << std::endl;
  os << "i " << docked_conf.__i << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os,
                         const Dock::DockedConf::Vec &docked_conf_vec) {
  for (auto &docked_conf : docked_conf_vec) {
    os << docked_conf;
  }
  return os;
}

double Dock::DockedConf::compute_rmsd_ord(const Dock::DockedConf &other) const {

  return geom3d::compute_rmsd(this->get_real_crds(), other.get_real_crds());
}

Dock &Dock::run() {

  DockedConf::Vec docked = __dock();

  DockedConf::PVec pdocked;
  for (auto &conf : docked) {
    conf.set_real_crds();
    pdocked.push_back(&conf);
  }

  cluster::GreedyCluster<DockedConf, DockedConf::CompareRMSDLessThanConstant,
                         DockedConf::CompareScoreLess>
      greedy;
  DockedConf::PVec preps =
      greedy.compute_uniform_representatives(pdocked, __rmsd_tol);

  DockedConf::Vec reps;
  for (auto &prep : preps)
    reps.push_back(*prep);

  docked.clear(); // clear memory
  __set_docked(reps);
  return *this;
}

Dock::DockedConf::Vec Dock::__dock() {
  helper::Benchmark b;

  DockedConf::Vec accepted;

  auto &conformations = __conformations.get_conformations();
  helper::Array1d<bool> rejected(conformations.size());

  molib::Atom::PVec seed_atoms = __seed.get_atoms();

  // go over all cavity points
  for (auto &[bsite_id, cavity_points] : __gpoints.get_gridpoints()) {
    auto &gmap = __gpoints.get_gmap(bsite_id);
    for (auto &cavpoint : cavity_points) {
      DockedConf::Vec accepted_tmp;
      // reset map of rejected conformations to zero
      rejected.reset();
      for (int c = 0; c < conformations.size(); ++c) {
        Conformations::Conformation &conf = conformations[c];
        // test if c-th conformation clashes with receptor: if yes, reject it
        if (rejected.data[c])
          continue;
        double energy_sum{};
        bool reje{};
        // go over coordinates of the c-th conformation
        dbgmsg("testing conformation " << c);

        Conformations::Conformation::Atom::Vec &atoms = conf.get_atoms();
        for (int i = 0; i < atoms.size(); ++i) {

          Conformations::Conformation::Atom &catom = atoms[i];
          docker::Gpoints::IJK confijk = cavpoint.ijk() + catom.ijk();

          dbgmsg("cavpoint.ijk() = " << cavpoint.ijk());
          dbgmsg("gpoint.ijk() = " << catom.ijk());
          dbgmsg("confijk = " << confijk);
          dbgmsg("gmap.szi = " << gmap.szi << " gmap.szj = " << gmap.szj
                               << " gmap.szk = " << gmap.szk);

          if (confijk.i < 0 || confijk.j < 0 || confijk.k < 0 ||
              confijk.i >= gmap.szi || confijk.j >= gmap.szj ||
              confijk.k >= gmap.szk ||
              gmap.data[confijk.i][confijk.j][confijk.k] == nullptr) {
            // mark as rejected all conformations that have this point
            for (const auto &r : __conformations.get_confs_at(catom.ijk())) {
              rejected.data[r] = true;
              dbgmsg("rejected conformation " << r);
            }
            reje = true;
            break;
          }
          docker::Gpoints::Gpoint *pgpoint =
              gmap.data[confijk.i][confijk.j][confijk.k];
          const molib::Atom &atom = *seed_atoms[i];
          energy_sum += pgpoint->energy(atom.idatm_type());
          dbgmsg("energy of pgpoint at crd = "
                 << pgpoint->crd() << " is "
                 << pgpoint->energy(atom.idatm_type())
                 << " for idatm_type = " << atom.idatm_type());
        }
        // if no clashes were found ...
        if (!reje) {
          accepted_tmp.push_back(
              Dock::DockedConf(cavpoint, conf, energy_sum, c));
        }
      }
      __cluster_fast(accepted_tmp, accepted);
    }
  }
  std::clog << "Fragment docking took " << b.duration()
            << "s, the number of accepted conformations is " << accepted.size()
            << std::endl;

  return accepted;
}

void Dock::__cluster_fast(const DockedConf::Vec &conformations,
                          DockedConf::Vec &reps) {

  std::set<const DockedConf *, DockedConf::CompareScoreLess> confs;
  for (auto &conf : conformations)
    confs.insert(&conf);

  while (!confs.empty()) {
    // accept lowest energy conformation as representative
    reps.push_back(**confs.begin());
    const auto &lowest_point_i = reps.back().get_i();
    confs.erase(confs.begin());
    // delete all conformations within RMSD tolerance of this lowest energy
    // conformation
    std::erase_if(confs, [this, &lowest_point_i](const auto &i) {
      return this->__conformations.rmsd_less_than_tol(lowest_point_i,
                                                      i->get_i());
    });
  }
}

void Dock::__set_docked(const DockedConf::Vec &confs) {

  helper::Benchmark b;

  __docked.set_name(__seed.name()); // molecules(!) name is seed_id

  molib::Atom::PVec seed_atoms = __seed.get_atoms();

  for (const auto &[bsite_id, _unused] : __gpoints.get_gridpoints()) {
    // go over all accepted conformations
    for (auto &conf : confs) {
      dbgmsg(" conformation size = " << conf.get_conf0().get_atoms().size()
                                     << " energy = " << conf.get_energy());
      // correct the seed's new coordinates and ...
      Conformations::Conformation::Atom::Vec &catoms =
          conf.get_conf0().get_atoms();

      for (int i = 0; i < catoms.size(); ++i) {

        molib::Atom &atom = *seed_atoms[i];
        Conformations::Conformation::Atom &catom = catoms[i];

        Gpoints::Gpoint &cavpoint = conf.get_cavpoint();
        atom.set_crd(cavpoint.crd() + catom.crd());
      }
      // save the conformation
      __docked.add(__seed).set_name(
          helper::to_string(conf.get_energy())); // molecule name is energy
      dbgmsg("conformation energy = " << conf.get_energy());
    }
  }
  std::clog << "Conversion of conformations to mols took " << b.duration()
            << "s" << std::endl;
}

} // namespace insilab::docker
