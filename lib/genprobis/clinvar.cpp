#include "clinvar.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include <memory>

namespace insilab::genprobis {

nlohmann::json generate_clinvar(const std::string &submission_summary_file,
                                const std::string &variant_summary_file) {

  nlohmann::json result;
  dbgmsg("before reading clinvar file");

  std::map<int, std::string> variant_summary;

  const auto &variant_summary_contents =
      inout::read_file(variant_summary_file).first;

  for (const auto &line : variant_summary_contents) {
    try {
      const auto &tokens = helper::split(line, "\t");
      const auto VariationId = std::stoi(tokens.at(30));
      variant_summary[VariationId] = line;
    } catch (...) {
    }
  }

  const auto &submission_summary_contents =
      inout::read_file(submission_summary_file).first;

  std::map<int, std::string> submission_summary;

  for (const auto &line : submission_summary_contents) {
    try {
      const auto &tokens = helper::split(line, "\t");
      const auto VariationId = std::stoi(tokens.at(0));
      submission_summary[VariationId] = line;
    } catch (...) {
    }
  }

  for (const auto [VariationId, line] : submission_summary) {
    try {
      const auto &tokens = helper::split(line, "\t");

      const auto VariationId = std::stoi(tokens.at(0));
      const auto &Description = tokens.at(3);

      const auto &variant_line = variant_summary.at(VariationId);
      const auto &variant_tokens = helper::split(variant_line, "\t");

      const auto &Type = variant_tokens.at(1);
      const auto &Name = variant_tokens.at(2);
      const auto &ClinicalSignificance = variant_tokens.at(6);
      const auto &RsId = variant_tokens.at(9);
      const auto &RCVaccessions = variant_tokens.at(11);

      result.push_back({
          {"rs_id", RsId == "-1" ? "-" : "rs" + RsId},
          {"rcv_accessions", helper::split(RCVaccessions, "|")},
          {"type", Type},
          {"name", Name},
          {"clinical_significance", ClinicalSignificance},
          {"description", Description},
      });

    } catch (...) {
    }
  }

  return result;
}

} // namespace insilab::genprobis
