#pragma once

#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
class Molecules;
} // namespace insilab::molib

namespace insilab::genprobis {

nlohmann::json generate_sifts_alphafold(const molib::Molecules &mols);
nlohmann::json generate_sifts(const std::string &sifts_file,
                              const molib::Molecule &receptor);

} // namespace insilab::genprobis
