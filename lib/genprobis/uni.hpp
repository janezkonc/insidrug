#pragma once

#include "nlohmann/json.hpp"


namespace insilab::genprobis {
nlohmann::json generate_uni(const std::string &uniprot_file);

} // namespace insilab::genprobis
