#pragma once

#include "nlohmann/json.hpp"

namespace insilab::genprobis {
nlohmann::json
generate_unipdbseq_mapping(const std::string &separated_uniprot_variants_dir,
                           nlohmann::json result);
} // namespace insilab::genprobis
