#include "mapping.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include <regex>

namespace insilab::genprobis {

nlohmann::json
generate_unipdbseq_mapping(const std::string &separated_uniprot_variants_dir,
                           nlohmann::json result) {
  std::map<std::string, std::map<int, std::vector<nlohmann::json>>>
      uniprot_id_pos_to_variants;
  for (const auto &s : result) {
    if (s.contains("uniprot_id")) {
      const auto &uniprot_id = s.at("uniprot_id").get<std::string>();
      if (uniprot_id_pos_to_variants.contains(uniprot_id))
        continue;
      const auto &uniprot_file =
          path::join(separated_uniprot_variants_dir, uniprot_id + ".json.gz");
      for (const auto &u :
           nlohmann::json::parse(inout::read_file_to_str(uniprot_file).first)) {
        const auto uniprot_resi = std::stoi(
            std::regex_replace(u.at("change").get<std::string>(),
                               std::regex("[^0-9]*([0-9]+).*"), "$1"));
        dbgmsg("uniprot_resi = " << uniprot_resi);
        uniprot_id_pos_to_variants[uniprot_id][uniprot_resi].push_back(u);
      }
    }
  }

  for (auto &s : result) {
    if (!s.contains("uniprot_id"))
      continue;
    dbgmsg("uniprot_resi = " << s.at("uniprot_resi"));
    const auto &uniprot_id = s.at("uniprot_id").get<std::string>();
    const auto &uniprot_resi = s.at("uniprot_resi").get<int>();

    // if SNPs exist for this uniprot position..
    if (!uniprot_id_pos_to_variants.at(uniprot_id).contains(uniprot_resi))
      continue;
    for (const auto &u :
         uniprot_id_pos_to_variants.at(uniprot_id).at(uniprot_resi)) {
      if (!s.contains("variants"))
        s["variants"] = nlohmann::json::array();
      s["variants"].push_back(u);
    }
  }
  dbgmsg("mapping = " << result);
  return result;
}

} // namespace insilab::genprobis
