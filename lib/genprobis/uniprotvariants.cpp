#include "uniprotvariants.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include <filesystem>

namespace insilab::genprobis {

namespace fs = std::filesystem;

void generate_uniprot_variants(const std::string &uniprot_variants_dir,
                               const std::string &out_dir,
                               const nlohmann::json &clinvar_j,
                               const nlohmann::json &pharmgkb_j) {

  // read file names in uniprot variants directory
  const auto &uniprot_variants_files =
      path::files_matching_pattern(uniprot_variants_dir, ".txt.gz$");

  std::map<std::string, nlohmann::json> rcv_id_clinvar;
  for (const auto &c : clinvar_j) {
    const auto &rcv_ids =
        c.at("rcv_accessions").get<std::vector<std::string>>();
    for (const auto &rcv_id : rcv_ids)
      rcv_id_clinvar[rcv_id] = c;
  }

  std::map<std::string, nlohmann::json> rs_id_pharmgkb;
  for (const auto &c : pharmgkb_j) {
    const auto &rs_id = c.at("rs_id").get<std::string>();
    auto copy_c = c;
    copy_c.erase("rs_id");
    rs_id_pharmgkb[rs_id] = copy_c;
  }

  // read humsavar file, add to homo sapiens information
  std::map<std::string, std::string> humsavar;
  const auto humsavar_v =
      inout::read_file(path::join(uniprot_variants_dir, "humsavar.txt")).first;
  for (const auto &line : humsavar_v) {
    const auto &lvec = helper::split(helper::squeeze(line), " ", true);
    if (lvec.size() < 7)
      continue;
    const auto &ft_id = lvec.at(2);
    const auto &variant_category = lvec.at(4);
    humsavar[ft_id] = variant_category;
    // Variant category is a shorthand for clinical significance:
    // LP/P = likely pathogenic or pathogenic
    // LB/B = likely benign or benign
    // US   = uncertain significance
  }

  for (const auto &species_file : uniprot_variants_files) {
    std::clog << "Reading " << species_file << std::endl;
    std::map<std::string, nlohmann::json> result;

    const auto &file_name = fs::path(species_file).filename().string();
    const auto pos = file_name.find("_variation.txt.gz");
    auto organism = file_name.substr(0, pos);
    std::replace(organism.begin(), organism.end(), '_', ' ');
  
    const auto &uniprot_variants_raw = inout::read_file(species_file).first;

    for (const auto &line : uniprot_variants_raw) {
      std::string uniprot_id, source_id;
      nlohmann::json obj;

      const auto &lvec = helper::split(line, "\t", true);
      if (lvec.size() != 14)
        continue;
      obj["gene_name"] = lvec.at(0);
      obj["uniprot_id"] = uniprot_id = lvec.at(1);
      obj["change"] = lvec.at(2);
      obj["source_id"] = source_id = lvec.at(3); // RSID, COSMIC, RCV, VAR_...
      obj["consequence"] = lvec.at(4);
      obj["clinical_significance"] = lvec.at(5);
      obj["phenotype_disease"] = lvec.at(6);
      obj["phenotype_disease_source"] = lvec.at(7);
      obj["cytogenetic_band"] = lvec.at(8);
      obj["chromosome_coordinate"] = lvec.at(9);
      obj["ensembl_gene_id"] = lvec.at(10);
      obj["ensembl_transcript_id"] = lvec.at(11);
      obj["ensembl_translation_id"] = lvec.at(12);
      obj["evidence"] = lvec.at(13);
      obj["organism"] = organism;
      if (rcv_id_clinvar.contains(source_id))
        obj["clinvar"] = rcv_id_clinvar.at(source_id);
      if (rs_id_pharmgkb.contains(source_id))
        obj["pharmgkb"] = rs_id_pharmgkb.at(source_id);
      if (humsavar.contains(source_id))
        obj["variant_category_humsavar"] = humsavar.at(source_id);

      if (!result.contains(uniprot_id))
        result[uniprot_id] = nlohmann::json::array();

      result.at(uniprot_id).push_back(obj);
    }
    // write to disk, one file for each UniProt ID
    for (const auto &[uniprot_id, contents_j] : result) {
      const auto &out_file = path::join(out_dir, uniprot_id + ".json.gz");
      inout::output_file(contents_j, out_file);
    }
  }
}

} // namespace insilab::genprobis
