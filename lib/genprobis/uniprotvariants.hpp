#pragma once

#include "nlohmann/json.hpp"

namespace insilab::genprobis {
void generate_uniprot_variants(const std::string &uniprot_variants_dir,
                               const std::string &out_dir,
                               const nlohmann::json &clinvar,
                               const nlohmann::json &pharmgkb);
} // namespace insilab::genprobis
