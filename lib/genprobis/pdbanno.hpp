#pragma once
#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
}

namespace insilab::genprobis {
nlohmann::json generate_pdb_sequence_annotation(const molib::Molecule &receptor,
                                                nlohmann::json result);
} // namespace insilab::genprobis
