#include "pdbanno.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "path/path.hpp"
#include <queue>

namespace insilab::genprobis {

nlohmann::json generate_pdb_sequence_annotation(const molib::Molecule &receptor,
                                                nlohmann::json result) {

  // assign missing residue numbers to sifts, because they are null
  std::map<std::string, std::queue<std::pair<int, char>>> missing;
  for (const auto &m : receptor.get_missing()) {

    const auto &missing_chain_id = get<0>(m);
    const int resi = get<2>(m);
    const char ins_code = get<3>(m);

    dbgmsg("missing_chain_id = " << missing_chain_id << " chain_id = "
                                 << chain_id << "missing resi = " << resi
                                 << " missing ins_code = " << ins_code);

    missing[missing_chain_id].push({resi, ins_code});
  }

  for (auto &s : result) {
    if (s.contains("pdb_missing") && s.at("pdb_missing").get<bool>()) {
      const auto &chain_id = s.at("pdb_chain_id").get<std::string>();
      if (missing.at(chain_id).empty()) {
        std::cerr
            << "Check PDB and SIFTS for differences in missing residues..."
            << std::endl;
        continue;
      }
      dbgmsg("assigning residue id "
             << missing.front() << " to missing residue " << s.at("pdb_resi"));
      const auto &[resi, ins_code] = missing.at(chain_id).front();
      s["pdb_resi"] = resi;
      s["pdb_ins_code"] = ins_code;
      missing.at(chain_id).pop();
    }
  }

  return result;
}

} // namespace insilab::genprobis
