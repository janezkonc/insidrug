#pragma once

#include "nlohmann/json.hpp"

namespace insilab::genprobis {
nlohmann::json generate_pharmgkb(const std::string &pharmgkb_file);
} // namespace insilab::genprobis
