#include "uni.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"

namespace insilab::genprobis {

nlohmann::json generate_uni(const std::string &uniprot_file) {

  const auto &contents = inout::read_file(uniprot_file).first;

  nlohmann::json uni;

  for (auto ln_num{0uz}; const auto &line : contents) {
    if (++ln_num == 1)
      continue;
    const auto &s = helper::split(line, "\t", true);
    if (s.size() != 3)
      throw helper::Error("[WHOOPS] wrong format of uniprot extra file!");

    uni.push_back({
        {"uniprot_id", s.at(0)},
        {"gene_names", helper::split(s.at(1))},
        {"organism", s.at(2)},
    });
  }
  return uni;
}

} // namespace insilab::genprobis
