#pragma once

#include "nlohmann/json.hpp"

namespace insilab::genprobis {
nlohmann::json generate_clinvar(const std::string &submission_summary_file,
                                const std::string &variant_summary_file);
} // namespace insilab::genprobis
