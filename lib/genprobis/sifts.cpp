#include "sifts.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"
#include "pdbanno.hpp"
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include <regex>

namespace insilab::genprobis {

using namespace molib;

/**
 * Generate a SIFTS-like mapping PDB <=> UniProt for AlphaFold proteins which
 * are divided into overlapping chunks.
 *
 */
nlohmann::json generate_sifts_alphafold(const Molecules &mols) {

  const auto &uniprot_id = std::regex_replace(
      mols.first().name(), std::regex(R"(.*AF-([^-]+)-.*)"), "$1");
  std::vector<std::string> sequence;
  nlohmann::json result;
  for (auto i{1u}; const auto &molecule : mols) {
    const auto &chunk_id = std::regex_replace(
        molecule.name(), std::regex{R"(.*(AF.*).pdb.gz)"}, "$1");
    // std::cout << molecule.name() << " " << chunk_id << std::endl;

    // find the longest overlap with sequence of the previous chunk
    auto sz_overlap{0u};
    for (std::vector<std::string> overlap;
         const auto &presidue : molecule.get_residues()) {
      overlap.push_back(presidue->resn());
      if (overlap.size() > sequence.size())
        continue;
      if (std::equal(overlap.begin(), overlap.end(),
                     sequence.end() - overlap.size()))
        sz_overlap = overlap.size();
    }
    // std::cout << "sz_overlap = " << sz_overlap << std::endl;
    i = i - sz_overlap;
    for (const auto &presidue : molecule.get_residues()) {
      result.push_back({
          {"chunk_id", chunk_id},
          {"pdb_chain_id", presidue->chain().chain_id()},
          {"pdb_resi", presidue->resi()},
          {"pdb_resn", presidue->resn()},
          {"pdb_missing", false},
          {"uniprot_id", uniprot_id},
          {"uniprot_resi", i++},
          {"uniprot_resn", presidue->resn()},
      });
      sequence.push_back(presidue->resn());
    }
  }
  return result;
}

nlohmann::json generate_sifts(const std::string &sifts_file,
                              const Molecule &receptor) {

  using namespace rapidxml;

  dbgmsg("before reading sifts");

  nlohmann::json result;

  dbgmsg(sifts_file);
  const auto &sifts_contents = inout::read_file_to_str(sifts_file).first;

  dbgmsg(sifts_contents);

  char *c_sifts_contents = new char[sifts_contents.size() + 1];
  strcpy(c_sifts_contents, sifts_contents.c_str());

  try {
    xml_document<> doc;             // character type defaults to char
    doc.parse<0>(c_sifts_contents); // 0 means default parse flags

    int counter = 0;

    // parse sifts file
    for (xml_node<> *entity = doc.first_node("entry")->first_node("entity");
         entity; entity = entity->next_sibling("entity")) {

      const std::string type = entity->first_attribute("type")->value();

      dbgmsg("type = " << type << " chain_id = " << chain_id);

      if (type != "protein")
        continue;

      for (xml_node<> *segment = entity->first_node("segment"); segment;
           segment = segment->next_sibling("segment")) {
        dbgmsg("reading segment");

        for (xml_node<> *listResidue = segment->first_node("listResidue");
             listResidue;
             listResidue = listResidue->next_sibling("listResidue")) {
          dbgmsg("reading listResidue");

          for (xml_node<> *residue = listResidue->first_node("residue");
               residue; residue = residue->next_sibling("residue")) {

            dbgmsg("reading residue");

            std::string pdb_resi, pdb_resn, pdb_chain_id, uniprot_resi,
                uniprot_resn, uniprot_id;

            bool has_pdb = false, has_uniprot = false;

            for (xml_node<> *crossRefDb = residue->first_node("crossRefDb");
                 crossRefDb;
                 crossRefDb = crossRefDb->next_sibling("crossRefDb")) {

              const std::string dbSource =
                  crossRefDb->first_attribute("dbSource")->value();

              if (dbSource == "PDB") {
                pdb_resi = crossRefDb->first_attribute("dbResNum")->value();
                pdb_resn = crossRefDb->first_attribute("dbResName")->value();
                pdb_chain_id =
                    crossRefDb->first_attribute("dbChainId")->value();
                has_pdb = true;

              } else if (dbSource == "UniProt") {
                uniprot_resi = crossRefDb->first_attribute("dbResNum")->value();
                uniprot_resn =
                    crossRefDb->first_attribute("dbResName")->value();
                uniprot_id =
                    crossRefDb->first_attribute("dbAccessionId")->value();
                has_uniprot = true;
              }
            }

            // consider only correct chain id
            if (has_pdb || has_uniprot) {

              bool missing = false;
              for (xml_node<> *residueDetail =
                       residue->first_node("residueDetail");
                   residueDetail; residueDetail = residueDetail->next_sibling(
                                      "residueDetail")) {

                dbgmsg("value of residueDetail = " << residueDetail->value());

                if (std::string("Not_Observed")
                        .compare(residueDetail->value()) == 0) {
                  missing = true;
                  break;
                }
              }
              nlohmann::json sifts_obj;
              sifts_obj["pdb_chain_id"] = pdb_chain_id;

              // std::cout << "pdb_resi = " << pdb_resi
              //           << " uniprot_resi = " << uniprot_resi
              //           << " missing = " << missing << std::endl;

              if (!pdb_resi.empty() && pdb_resi != "null") {
                sifts_obj["pdb_resi"] = std::stoi(pdb_resi);
                const auto &ins_code = std::regex_replace(
                    pdb_resi, std::regex{R"(\d+([a-zA-Z]*))"}, "$1");
                sifts_obj["pdb_ins_code"] = ins_code.empty() ? ' ' : ins_code.at(0);
              }
              if (!pdb_resn.empty())
                sifts_obj["pdb_resn"] = pdb_resn;

              sifts_obj["pdb_missing"] = missing;

              if (!uniprot_resi.empty())
                sifts_obj["uniprot_resi"] = std::stoi(uniprot_resi);
              if (!uniprot_id.empty())
                sifts_obj["uniprot_id"] = uniprot_id;
              if (!uniprot_resn.empty())
                sifts_obj["uniprot_resn"] =
                    helper::get_three_letter(uniprot_resn.at(0));

              result.push_back(sifts_obj);
            }
          }
        }
      }
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    delete[] c_sifts_contents;
    throw e;
  }

  delete[] c_sifts_contents;
  dbgmsg("after reading sifts result = " + helper::to_string(result));

  // add missing residues IDs from a PDB file
  return genprobis::generate_pdb_sequence_annotation(receptor, result);
}

} // namespace insilab::genprobis
