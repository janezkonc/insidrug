#include "pharmgkb.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"

namespace insilab::genprobis {

nlohmann::json generate_pharmgkb(const std::string &clinvar_file) {

  std::vector<std::string> pharmgkb = inout::read_file(clinvar_file).first;

  nlohmann::json result;

  for (const auto &line : pharmgkb) {
    const auto &lvec = helper::split(line, "\t", true);

    if (lvec.size() != 5)
      continue;
    const auto &rs_id = lvec.at(0);
    const auto &pharmgkb_id = lvec.at(3);

    if (rs_id.compare(0, 2, "rs") != 0)
      continue;
    result.push_back({
        {"rs_id", rs_id},
        {"pharmgkb_id", pharmgkb_id},
    });
  }

  return result;
}

} // namespace insilab::genprobis
