#include "dockedconformation.hpp"
#include "molib/molecule.hpp"

namespace insilab::linker {

std::ostream &operator<<(std::ostream &os, const DockedConformation &conf) {
  os << "start link ++++++++++++++++++++++++++++++" << std::endl;
  os << "ENERGY = " << conf.get_energy() << std::endl;
  os << "LIGAND = " << conf.get_ligand() << std::endl;
  os << "RECEPTOR = " << conf.get_receptor() << std::endl;
  os << "end link --------------------------------" << std::endl;
  return os;
}
}; // namespace insilab::linker
