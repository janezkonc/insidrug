#pragma once

#include "glib/graph.hpp"
#include "helper/it.hpp"
#include "segment.hpp"
#include <functional>
#include <tuple>

namespace insilab::linker {
class State;
class Segment;

class Seed : public helper::template_vector_container<Seed> {
  Segment &__seg;

public:
  using Graph = std::vector<std::unique_ptr<Seed>>;

  Seed(Segment &seg) : __seg(seg) {}
  Segment &get_segment() const { return __seg; }
  friend std::ostream &operator<<(std::ostream &stream, const Seed &s);
  static Graph create_graph(const Segment::Graph &segment_graph);
};
}; // namespace insilab::linker
