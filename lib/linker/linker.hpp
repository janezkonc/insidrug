#pragma once

#include "dockedconformation.hpp"
#include "grid/grid.hpp"
#include "helper/array2d.hpp"
#include "helper/debug.hpp"
#include "molib/atom.hpp"
#include "molib/details/internal.hpp"
#include "ommiface/modeler.hpp"
#include "partial.hpp"
#include "seed.hpp"
#include "segment.hpp"
#include <functional>
#include <mutex>
#include <tuple>

namespace insilab::geom3d {
class Quaternion;
} // namespace insilab::geom3d

namespace insilab::molib {
class Atom;
class Molecule;
class Molecules;
} // namespace insilab::molib

namespace insilab::molib::algorithms {
class Fragment;
}

namespace insilab::ommiface {
class Modeler;
}

namespace insilab::score {
class ScoringFunction;
}

namespace insilab::linker {

using namespace molib;
using namespace score;

class State;

class Linker {
public:
  struct Options {

    details::Internal ic;

    const Molecule &receptor, &ligand;
    const std::vector<algorithms::Fragment> &fragments;
    const std::vector<std::pair<int, Molecules>> &top_seeds;
    const Atom::Grid gridrec;
    ScoringFunction &score;

    ommiface::Modeler::Options modeler_opts;

    const bool iterative;

    const double dist_cutoff, spin_degrees, tol_seed_dist, lower_tol_seed_dist,
        upper_tol_seed_dist, clash_coeff, docked_clus_rad, max_allow_energy;

    const int max_possible_conf, link_iter, n_cliques, max_steps, k_clique_size,
        max_iterations_final;
  };

private:
  class GenericLinker {
  protected:
    using AtomToSegment = std::map<const Atom *, Segment *>;
    using SegStateMap = std::map<const Segment *, State *>;
    static const int MAX_ENERGY = std::numeric_limits<int>::max();
    using PriorityQueue = std::multiset<Partial, Partial::comp>;

    class ConnectionError : public helper::Error {
      const std::set<State::CPair> __fs;

    public:
      ConnectionError(const std::string &msg, const std::set<State::CPair> fs)
          : Error(msg), __fs(fs) {}
      const std::set<State::CPair> &get_failed_state_pairs() const {
        return __fs;
      }
    };

    mutable std::mutex __mtx;

    Options __opts;

    Segment::Graph __segment_graph;
    Seed::Graph __seed_graph;
    std::set<State::CPair> __blacklist;

    double __distance(const State &start, const State &goal) const;
    State::PVec
    __compute_neighbors(const State &curr_state, Segment &next,
                        std::vector<std::unique_ptr<State>> &states);
    bool __clashes_receptor(const State &) const;
    bool __clashes_ligand(const State &current, const Partial &conformation,
                          const State &prev) const;
    std::vector<geom3d::Point> __rotate(const geom3d::Quaternion &q,
                                        const geom3d::Point &p1,
                                        const std::vector<geom3d::Point> &crds);

    helper::Array2d<bool>
    __find_compatible_state_pairs(const Seed::Graph &seed_graph, const int sz);
    std::vector<std::vector<State::PVec>>
    __grow_possibles(const std::map<State *, State::PSet> &pos);

    bool __has_blacklisted(const State::PVec &conformation,
                           const std::set<State::CPair> &blacklist);
    bool __check_distances_to_seeds(const State &curr_state,
                                    const Segment &adjacent,
                                    const SegStateMap &docked_seeds);

    std::pair<State *, Segment *>
    __find_good_neighbor(const Partial &curr_conformation,
                         const SegStateMap &docked_seeds);
    State *__is_seed(const Segment &seg, const SegStateMap &docked_seeds);

    DockedConformation __minimize(DockedConformation &docked);

    void
    __create_states(const Segment::Graph &segment_graph,
                    const std::vector<std::pair<int, Molecules>> &top_seeds);

    void __init_openmm(ommiface::Modeler &modeler);

    virtual DockedConformation
    __a_star(const int segment_graph_size, const Partial &start_conformation,
             std::vector<std::unique_ptr<State>> &states, int iter) = 0;
    virtual Partial::Vec
    __generate_rigid_conformations(const Seed::Graph &seed_graph) = 0;
    virtual DockedConformation __reconstruct(const Partial &conformation) = 0;

  public:
    GenericLinker(Linker::Options &opts) : __opts(opts) {}

    Partial::Vec init_conformations();
    DockedConformation compute_conformation(const Partial &partial);
  };

  class StaticLinker : public GenericLinker {
    DockedConformation __a_star(const int segment_graph_size,
                                const Partial &start_conformation,
                                std::vector<std::unique_ptr<State>> &states,
                                int iter);
    Partial::Vec __generate_rigid_conformations(const Seed::Graph &seed_graph);
    DockedConformation __reconstruct(const Partial &conformation);

  public:
    using GenericLinker::GenericLinker;
  };

  class IterativeLinker : public GenericLinker {
    DockedConformation __a_star(const int segment_graph_size,
                                const Partial &start_conformation,
                                std::vector<std::unique_ptr<State>> &states,
                                int iter);
    Partial::Vec __generate_rigid_conformations(const Seed::Graph &seed_graph);
    DockedConformation __reconstruct(const Partial &conformation);

  public:
    using GenericLinker::GenericLinker;
  };

  std::unique_ptr<GenericLinker> l;

public:
  Linker(Options &opts);
  Linker(Linker &&rhs) = default;

  Partial::Vec compute_partial();
  DockedConformation link_minimize(const Partial &p);
};

} // namespace insilab::linker
