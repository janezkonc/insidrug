#include "partial.hpp"
#include "glib/algorithms/mcqd.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "molib/atom.hpp"
#include "molib/bond.hpp"
#include "molib/molecule.hpp"
#include "score/scoringfunction.hpp"
#include "state.hpp"
#include <algorithm>
#include <map>
#include <queue>

namespace insilab::linker {

using namespace molib;

std::ostream &operator<<(std::ostream &os, const Partial &le) {
  os << "start link ++++++++++++++++++++++++++++++" << std::endl;
  os << "ENERGY = " << le.get_energy() << std::endl;
  for (const auto &pstate : le.get_states())
    os << *pstate << std::endl;
  os << "end link --------------------------------" << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os, const Partial::Vec &vec_le) {
  for (const auto &le : vec_le) {
    os << le << std::endl;
  }
  return os;
}

double Partial::compute_rmsd_ord(const Partial &other) const {
  double sum_squared = 0;
  int sz = 0;
  std::set<Segment::Id> segid1;
  for (auto &pstate1 : this->get_states()) {
    segid1.insert(pstate1->get_segment().get_id());
  }
  for (auto &pstate2 : other.get_states()) {
    if (!segid1.count(pstate2->get_segment().get_id()))
      throw helper::Error("[WHOOPS] cannot compute rmsd between two partial "
                          "conformations with different compositions");
  }
  for (auto &pstate1 : this->get_states()) {
    for (auto &pstate2 : other.get_states()) {
      if (pstate1->get_segment().get_id() == pstate2->get_segment().get_id()) {
        if (pstate1->get_id() == pstate2->get_id()) {
          sz += pstate1->get_crds().size();
        } else {
          auto &crds1 = pstate1->get_crds();
          auto &crds2 = pstate2->get_crds();
          for (int i = 0; i < crds1.size(); ++i) {
            sum_squared += crds1[i].distance_sq(crds2[i]);
          }
          sz += crds1.size();
        }
      }
    }
  }
  return sqrt(sum_squared / sz);
}

void Partial::set_geometric_center() {
  __crd = geom3d::compute_geometric_center(this->get_ligand_crds());
}

void Partial::sort(Partial::Vec &v) {
  std::sort(v.begin(), v.end(), Partial::comp());
}

geom3d::Point::Vec Partial::get_ligand_crds() const {
  geom3d::Point::Vec crds;
  for (auto &pstate : __states) {
    for (auto &crd : pstate->get_crds()) {
      crds.push_back(crd);
    }
  }
  return crds;
}

void Partial::set_ligand_crds(const geom3d::Point::Vec &crds) {
  int i = 0;
  for (auto &pstate : __states) {
    for (auto &crd : pstate->get_crds()) {
      crd = crds[i++];
    }
  }
}

molib::Atom::PVec Partial::get_ligand_atoms() {
  molib::Atom::PVec atoms;
  for (auto &pstate : __states) {
    for (auto &patom : pstate->get_segment().get_atoms()) {
      atoms.push_back(patom);
    }
  }
  return atoms;
}

} // namespace insilab::linker
