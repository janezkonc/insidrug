#pragma once

#include "molib/molecule.hpp"
#include <memory>

namespace insilab::linker {

class DockedConformation {
public:
  typedef std::vector<DockedConformation> Vec;

private:
  std::unique_ptr<molib::Molecule> __ligand;
  std::unique_ptr<molib::Molecule> __receptor;
  double __energy;

public:
  DockedConformation() : __ligand(nullptr), __receptor(nullptr), __energy(0) {}
  DockedConformation(molib::Molecule ligand, molib::Molecule receptor,
                     double energy)
      : __ligand(std::make_unique<molib::Molecule>(ligand)),
        __receptor(std::make_unique<molib::Molecule>(receptor)),
        __energy(energy) {}

  bool empty() const { return __ligand == nullptr; }

  molib::Molecule &get_ligand() { return *__ligand; }
  const molib::Molecule &get_ligand() const { return *__ligand; }
  molib::Molecule &get_receptor() { return *__receptor; }
  const molib::Molecule &get_receptor() const { return *__receptor; }
  double get_energy() { return __energy; }
  const double get_energy() const { return __energy; }

  friend std::ostream &operator<<(std::ostream &os,
                                  const DockedConformation &conf);
};

} // namespace insilab::linker
