#pragma once

#include "geom3d/coordinate.hpp"
#include <functional>
#include <set>
#include <tuple>
#include <vector>

namespace insilab::linker {

class Segment;
class State {
public:
  struct comp {
    bool operator()(State *const i, State *const j) const {
      return i->get_id() < j->get_id();
    }
  };

  using PVec = std::vector<State *>;
  using PSet = std::set<State *, State::comp>;
  using CPair = std::pair<const State *, const State *>;
  using Id = int;

private:
  const Segment &__segment;
  geom3d::Point::Vec __crds;
  double __energy;
  Id __id;
#ifndef NDEBUG
  int __no;
#endif
public:
  State(const Segment &segment, const geom3d::Point::Vec crds,
        const double energy = 0)
      : __segment(segment), __crds(crds), __energy(energy) {}
  void set_energy(const double energy) { __energy = energy; }
  double get_energy() const { return __energy; }
  const Segment &get_segment() const { return __segment; }
  const geom3d::Point::Vec &get_crds() const { return __crds; }
  geom3d::Point::Vec &get_crds() { return __crds; }
  const geom3d::Point &get_crd(const int i) const { return __crds[i]; }
  bool clashes(const State &other, const double clash_coeff)
      const; // clashes between this and other state
  // std::string pdb() const;
  void set_id(Id id) { __id = id; }
  const Id get_id() const { return __id; }
#ifndef NDEBUG
  void set_no(int no) { __no = no; }
  const int get_no() const { return __no; }
#endif
  friend std::ostream &operator<<(std::ostream &stream, const State &s);
  friend std::ostream &operator<<(std::ostream &stream, const State::PVec &sv);
};

State::PVec operator-(const State::PSet &left, const State::PSet &right);

} // namespace insilab::linker
