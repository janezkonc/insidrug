#include "segment.hpp"
#include "glib/algorithms/path.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "molib/algorithms/fragmenter.hpp"
#include "molib/molecule.hpp"

namespace insilab::linker {

using namespace molib;

std::ostream &operator<<(std::ostream &stream, const Segment &s) {
  stream << "Segment(id : " << s.__id << " seed_id : " << s.__seed_id
         << ") : atom numbers = ";
  for (auto &pa : s.__atoms)
    stream << pa->atom_number() << " ";
  return stream;
}

Segment::Segment(const Atom::PVec atoms, const int &seed_id,
                 const Segment::Id idx)
    : __atoms(atoms), __seed_id(seed_id), __id(idx),
      __join_atom(atoms.size(), false), __common_atom(atoms.size(), false) {

  for (int i = 0; i < atoms.size(); ++i)
    __amap[atoms[i]] = i;
}

//~ const Atom& Segment::adjacent_in_segment(const Atom &atom,
//~ const Atom &forbidden) const {
//~ for (auto &adj : atom) {
//~ if (&adj != &forbidden && has_atom(adj))
//~ return adj;
//~ }
//~ throw helper::Error("[WHOOPS] couldn't find adjacent in segment");
//~ }
//~
const int Segment::adjacent_in_segment(const Atom &atom,
                                       const Atom &forbidden) const {
  for (auto &adj : atom) {
    if (&adj != &forbidden && has_atom(adj))
      return get_idx(adj);
  }
  throw helper::Error("[WHOOPS] couldn't find adjacent in segment");
}

void Segment::set_bond(const Segment &other, Atom &a1, Atom &a2) {
  __bond.insert({&other, Bond(&a1, &a2, get_idx(a1), get_idx(a2))});
}

Segment::Graph
Segment::create_graph(const molib::algorithms::Fragment::Vec &fragments) {
  dbgmsg("Create segment graph ...");
  std::vector<std::unique_ptr<Segment>> vertices;
  int idx = 0;
  // make vertices (segments) of a graph
  for (const auto &fragment : fragments) {
    dbgmsg(fragment.get_all());
    const auto &all = fragment.get_all();
    const auto fragatoms = Atom::PVec(all.begin(), all.end());
    vertices.push_back(std::unique_ptr<Segment>(
        new Segment(fragatoms, fragment.get_seed_id(), idx++)));
  }
  // connect segments
  for (int i = 0; i < vertices.size(); ++i) {
    Segment &s1 = *vertices[i];
    for (int j = i + 1; j < vertices.size(); ++j) {
      Segment &s2 = *vertices[j];
      Atom::PVec inter;
      std::ranges::set_intersection(s1.get_atoms(), s2.get_atoms(),
                                    std::back_inserter(inter));
      dbgmsg(s1.get_atoms().size()
             << " " << s2.get_atoms().size() << " " << inter.size());
      if (inter.size() == 1) {

        auto &atom = **inter.begin();
        s1.set_common_atom(atom);
        s2.set_common_atom(atom);
        dbgmsg("intersection size is one for segments " << s1 << " and " << s2);

      } else if (inter.size() == 2) {
        s1.add(&s2);
        s2.add(&s1);
        auto &atom1 = **inter.begin();
        auto &atom2 = **inter.rbegin();

        s1.set_common_atom(atom1);
        s2.set_common_atom(atom1);
        s1.set_common_atom(atom2);
        s2.set_common_atom(atom2);

        // determine which atom of bond is in s1 and which in s2
        int num_bonds = 0;
        for (auto &adj : atom1) {
          if (s1.has_atom(adj))
            num_bonds++;
        }
        if (num_bonds == 1) {
          dbgmsg("atom " << atom1 << " belongs to segment " << s2);
          dbgmsg("atom " << atom2 << " belongs to segment " << s1);
          s1.set_bond(s2, atom2, atom1);
          s2.set_bond(s1, atom1, atom2);

          s1.set_join_atom(atom2);
          s2.set_join_atom(atom1);
        } else {
          dbgmsg("atom " << atom1 << " belongs to segment " << s1);
          dbgmsg("atom " << atom2 << " belongs to segment " << s2);
          s1.set_bond(s2, atom1, atom2);
          s2.set_bond(s1, atom2, atom1);

          s1.set_join_atom(atom1);
          s2.set_join_atom(atom2);
        }
      }
    }
  }

  const Segment::Paths paths = __find_paths(vertices);
  __init_max_linker_length(paths);
  __set_branching_rules(paths);

  return vertices;
}

/**
 * Find ALL paths between ALL seed segments (even non-adjacent)
 * and seeds and leafs
 */
Segment::Paths
Segment::__find_paths(const std::vector<std::unique_ptr<Segment>> &segments) {
  Segment::Paths paths;
  dbgmsg("find all paths in a graph");
  Segment::PSet seeds, leafs;
  for (auto &pseg : segments) {
    auto &seg = *pseg;
    if (seg.is_seed())
      seeds.insert(&seg);
    if (!seg.is_seed() && seg.is_leaf())
      leafs.insert(&seg);
  }
  for (auto &pseg1 : seeds) {
    for (auto &pseg2 : leafs) {
      dbgmsg("finding path between seed segment "
             << *pseg1 << " and leaf segment " << *pseg2);
      Segment::Path path = glib::algorithms::find_path(*pseg1, *pseg2);
      paths.insert({{pseg1, pseg2}, path});
    }
  }
  for (auto &pseg1 : seeds) {
    for (auto &pseg2 : seeds) {
      if (pseg1->get_id() < pseg2->get_id()) {
        dbgmsg("finding path between seed segment "
               << *pseg1 << " and seed segment " << *pseg2);
        Segment::Path path = glib::algorithms::find_path(*pseg1, *pseg2);
        paths.insert({{pseg1, pseg2}, path});
      }
    }
  }
  return paths;
}

bool Segment::__link_adjacent(const Segment::Path &path) {
  /* Returns true if path has no seed segments along the way
   *
   */
#ifndef NDEBUG
  for (auto it = path.begin(); it != path.end(); ++it) {
    dbgmsg(**it << " is seed = " << boolalpha << (*it)->is_seed());
  }
#endif
  for (auto it = path.begin() + 1; it != path.end() - 1; ++it) {
    dbgmsg(**it);
    if ((*it)->is_seed())
      return false;
  }
  return true;
}

void Segment::__init_max_linker_length(const Segment::Paths &paths) {
  for (auto &kv : paths) {
    auto &seg_pair = kv.first;
    Segment::Path path(kv.second.begin(), kv.second.end());
    __compute_max_linker_length(path);
  }
}

void Segment::__compute_max_linker_length(Segment::Path &path) {
  for (int j = 0; j < path.size() - 1; j++) {

    double d = 0.0;
    int i = j;

    Atom *front_atom = &path[i]->get_bond(*path[i + 1]).atom1();

    for (; i < path.size() - 2; i += 2) {
      const Bond &b1 = path[i]->get_bond(*path[i + 1]);
      const Bond &b2 = path[i + 1]->get_bond(*path[i + 2]);
      path[j]->set_max_linker_length(*path[i + 1], d + b1.length());
      path[i + 1]->set_max_linker_length(*path[j], d + b1.length());

      d += front_atom->crd().distance(b2.atom2().crd());

      front_atom = &b2.atom2();

      path[j]->set_max_linker_length(*path[i + 2], d);
      path[i + 2]->set_max_linker_length(*path[j], d);

      dbgmsg("max_linker_length between "
             << *path[j] << " and " << *path[i + 1] << " = "
             << path[j]->get_max_linker_length(*path[i + 1]));
      dbgmsg("max_linker_length between "
             << *path[j] << " and " << *path[i + 2] << " = "
             << path[j]->get_max_linker_length(*path[i + 2]));
    }
    if (i < path.size() - 1) {
      const Bond &b = path[i]->get_bond(*path[i + 1]);

      d += front_atom->crd().distance(b.atom2().crd());

      path[j]->set_max_linker_length(*path[i + 1], d);
      path[i + 1]->set_max_linker_length(*path[j], d);
      dbgmsg("last max_linker_length between "
             << *path[j] << " and " << *path[i + 1] << " = "
             << path[j]->get_max_linker_length(*path[i + 1]));
    }
    dbgmsg("TOTAL max_linker_length between "
           << *path[j] << " and " << *path[path.size() - 1] << " = "
           << path[j]->get_max_linker_length(*path[path.size() - 1]));
  }
}

void Segment::__set_branching_rules(const Segment::Paths &paths) {
  for (auto &kv : paths) {
    const Segment::Path &path = kv.second;
#ifndef NDEBUG
    dbgmsg("valid_path = ");
    for (auto &seg : path)
      dbgmsg(*seg);
#endif
    Segment &start = *path.back();
    Segment &goal = *path.front();
    Segment &start_next = **(path.end() - 2);
    Segment &goal_next = **(path.begin() + 1);
    const bool is_link_adjacent = __link_adjacent(path);
    for (auto it = path.begin(); it != path.end(); ++it) {
      Segment &current = **it;
      if (&current != &goal && goal.is_seed()) {
        Segment &next = **(it - 1);
        current.set_next(goal, next);
        goal.set_next(current, goal_next);
        if (is_link_adjacent)
          current.set_adjacent_seed_segments(goal);
        dbgmsg("current = " << current << " goal.is_seed() = " << boolalpha
                            << goal.is_seed() << " next = " << next);
      }
      if (&current != &start && start.is_seed()) {
        Segment &prev = **(it + 1);
        current.set_next(start, prev);
        start.set_next(current, start_next);
        if (is_link_adjacent)
          current.set_adjacent_seed_segments(start);
        dbgmsg("current = " << current << " start.is_seed() = " << boolalpha
                            << start.is_seed() << " prev = " << prev);
      }
    }
  }
}

} // namespace insilab::linker
