#include "state.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "molib/molecule.hpp"
#include "segment.hpp"

namespace insilab::linker {

using namespace molib;

State::PVec operator-(const State::PSet &left, const State::PSet &right) {
  State::PVec ret;
  for (auto &state : left)
    if (!right.count(state))
      ret.push_back(state);
  return ret;
}

// std::string State::pdb() const {
//   std::stringstream ss;
//   for (int i = 0; i < get_segment().get_atoms().size(); ++i) {
//     Atom a(get_segment().get_atom(i));
//     a.set_br(&get_segment().get_atom(i).residue());
//     a.set_crd(get_crd(i));
//     ss << a;
//   }
//   return ss.str();
// }

bool State::clashes(const State &other, const double clash_coeff)
    const { // clashes between this and other state
  const int other_size = other.get_crds().size();
  for (int i = 0; i < __crds.size(); ++i) {
    const geom3d::Point &crd1 = get_crd(i);
    const Atom &a1 = __segment.get_atom(i);
    ;
    if (__segment.is_common_atom(i)) {
      dbgmsg("common state atom = " << a1.atom_number()
                                    << " is not checked for clashes");
      continue;
    }
    const double vdw1 = a1.radius();
    for (int j = 0; j < other_size; ++j) {
      const geom3d::Point &crd2 = other.get_crd(j);
      const Atom &a2 = other.get_segment().get_atom(j);
      if (other.get_segment().is_common_atom(j)) {
        dbgmsg("common state atom = " << a2.atom_number()
                                      << " is not checked for clashes");
        continue;
      }
      const double vdw2 = a2.radius();
      if (crd1.distance_sq(crd2) < pow(clash_coeff * (vdw1 + vdw2), 2))
        return true;
    }
  }
  return false;
}

std::ostream &operator<<(std::ostream &stream, const State &s) {
  //~ stream << "State(address = " << &s <<", segment = " <<
  // s.__segment.get_seed_id() << ") "
  stream << "State(id = " << s.get_id()
         << ", segment = " << s.__segment.get_seed_id() << ") "
         << " energy = " << std::setprecision(4) << std::fixed << s.__energy
         << " atom_crd =  ";
  for (int i = 0; i < s.get_crds().size(); ++i) {
    const geom3d::Point &crd = s.get_crd(i);
    const Atom &a = s.__segment.get_atom(i);
    ;
    stream << a.atom_number() << " -> " << crd << " ";
  }
  return stream;
}

std::ostream &operator<<(std::ostream &stream, const State::PVec &sv) {
  for (auto &state : sv)
    stream << "MEMBER STATE : " << *state << std::endl;
  return stream;
}

} // namespace insilab::linker
