#pragma once

#include "grid/grid.hpp"
#include "seed.hpp"
#include "segment.hpp"

namespace insilab::molib {
class Atom;
}

namespace insilab::linker {

class State;

class Poses {
  class AtomPoint {
    const geom3d::Point &__crd;
    const molib::Atom &__atom;
    State &__state;

  public:
    AtomPoint(const geom3d::Point &crd, const molib::Atom &atom, State &state)
        : __crd(crd), __atom(atom), __state(state) {}
    const geom3d::Point &crd() const { return __crd; }
    State &get_state() { return __state; }
    const molib::Atom &get_atom() const { return __atom; }
    double radius() const;

    using UPVec = std::vector<std::unique_ptr<AtomPoint>>;
    using PVec = std::vector<AtomPoint *>;
    using Grid = insilab::grid::Grid<AtomPoint>;
  };

  std::map<Segment::Id, AtomPoint::UPVec> __atompoints;
  std::map<Segment::Id, AtomPoint::Grid> __grid;

public:
  Poses(const Seed::Graph &seed_graph);
  State::PSet get_join_states(const State &state, Segment &segment2,
                              std::pair<molib::Atom *, molib::Atom *> &jatoms,
                              const double max_linker_length,
                              const double lower_tol_seed_dist,
                              const double upper_tol_seed_dist);
};

} // namespace insilab::linker
