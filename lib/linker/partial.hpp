#pragma once

#include "seed.hpp"
#include "segment.hpp"
#include <functional>
#include <tuple>

namespace insilab::molib {
class Atom;
}

namespace insilab::linker {

class State;

class Partial {
public:
  typedef std::vector<Partial> Vec;
  typedef std::vector<Partial *> PVec;
  struct comp {
    bool operator()(const Partial &i, const Partial &j) const {
      return i.get_energy() < j.get_energy();
    }
  };

private:
  State::PVec __states;
  double __energy;
  geom3d::Point::Vec __crds;
  geom3d::Point __crd; // geom. center
public:
  Partial()
      : __states(State::PVec()), __energy(0.0), __crds(geom3d::Point::Vec()) {}
  Partial(const double energy)
      : __states(State::PVec()), __energy(energy),
        __crds(geom3d::Point::Vec()) {}
  Partial(const State::PVec &states, const double energy,
          const geom3d::Point::Vec &crds = geom3d::Point::Vec())
      : __states(states), __energy(energy), __crds(crds) {}

  void add_state(State &state) { __states.push_back(&state); }

  State::PVec &get_states() { return __states; }
  const State::PVec &get_states() const { return __states; }

  void set_receptor_crds(const geom3d::Point::Vec &crds) { __crds = crds; }
  geom3d::Point::Vec &get_receptor_crds() { return __crds; }
  const geom3d::Point::Vec &get_receptor_crds() const { return __crds; }

  void set_ligand_crds(const geom3d::Point::Vec &crds);
  geom3d::Point::Vec get_ligand_crds() const;

  std::vector<molib::Atom *> get_ligand_atoms();

  void set_energy(const double energy) { __energy = energy; }
  double get_energy() const { return __energy; }

  int size() const { return __states.size(); }
  bool empty() const { return __states.empty(); }

  double compute_rmsd_ord(const Partial &) const;
  void set_geometric_center();

  static void sort(Partial::Vec &v);

  struct CompareRMSDLessThanConstant {
    bool operator()(const Partial *x, const Partial *y,
                    const double rmsd_tol) const {
      return x->compute_rmsd_ord(*y) < rmsd_tol;
    }
  };

  struct CompareScoreLess {
    bool operator()(const Partial *x, const Partial *y) const {
      return x->get_energy() < y->get_energy();
    }
  };

  const geom3d::Point &crd() const { return __crd; }

  friend std::ostream &operator<<(std::ostream &os, const Partial &le);
  friend std::ostream &operator<<(std::ostream &os, const Vec &vec_le);
};

} // namespace insilab::linker
