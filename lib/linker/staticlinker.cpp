#include "cluster/greedy.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/quaternion.hpp"
#include "glib/algorithms/mcqd.hpp"
#include "glib/io/io.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "linker.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "ommiface/modeler.hpp"
#include "poses.hpp"
#include "score/scoringfunction.hpp"
#include <filesystem>
#include <queue>

namespace insilab::linker {

using namespace molib;

DockedConformation Linker::StaticLinker::__a_star(
    const int segment_graph_size, const Partial &start_conformation,
    std::vector<std::unique_ptr<State>> &states, int iter) {

  for (auto &pstate : start_conformation.get_states())
    states.push_back(std::unique_ptr<State>(new State(*pstate)));
  if (start_conformation.empty())
    throw helper::Error(
        "[WHOOPS] at least one docked anchor state is required for linking");
  SegStateMap docked_seeds;
  for (auto &pstate : states)
    docked_seeds.insert({&pstate->get_segment(), &*pstate});
  std::set<State::CPair> failed_state_pairs;
  Partial min_conformation(MAX_ENERGY);
  PriorityQueue
      openset; // openset has conformations sorted from lowest to highest energy
  openset.insert(Partial(State::PVec{&*states[0]}, states[0]->get_energy(),
                         geom3d::Point::Vec()));

  while (!openset.empty()) {
    if (--iter < 0)
      break;
    Partial curr_conformation = *openset.begin();
    openset.erase(openset.begin());
//~ #define MOVIE
#ifdef MOVIE
    DockedConformation docked = __reconstruct(curr_conformation);
    docked.get_receptor().undo_mm_specific();
    inout::output_file(
        ommiface::print_complex(docked.get_ligand(), docked.get_receptor(),
                                docked.get_energy()),
        "movie.pdb", ios_base::app); // output docked molecule conformations
#endif

    dbgmsg("openset.size() = " << openset.size());
    dbgmsg("curr_conformation at step = " << iter << " = " << std::endl
                                          << curr_conformation);
    if (curr_conformation.size() == segment_graph_size) {
      dbgmsg("CANDIDATE for minimum energy conformation at step = "
             << iter << " = " << std::endl
             << curr_conformation);
      if (curr_conformation.get_energy() < min_conformation.get_energy()) {
        min_conformation = curr_conformation;
        dbgmsg("ACCEPTED");
      }
    } else {
      // grow in the direction of (a) first "free" seed state (b) if such
      // path does not exist, then grow in the first possible direction
      std::pair<State *, Segment *> ret =
          __find_good_neighbor(curr_conformation, docked_seeds);
      State &curr_state = *ret.first;
      Segment &adj = *ret.second;
      State *adj_is_seed = __is_seed(adj, docked_seeds);
      // check seed distances here
      dbgmsg("adj_is_seed " << (adj_is_seed ? "true" : "false"));
      dbgmsg("check_distances_to_seeds = "
             << std::boolalpha
             << (adj_is_seed ||
                 __check_distances_to_seeds(curr_state, adj, docked_seeds)));
      if (adj_is_seed ||
          __check_distances_to_seeds(curr_state, adj, docked_seeds)) {
        auto ret2 =
            (adj_is_seed ? State::PVec{adj_is_seed}
                         : __compute_neighbors(curr_state, adj, states));
        for (auto &pneighbor : ret2) {
          dbgmsg("CHECKING NEIGHBOR : " << *pneighbor);
          dbgmsg("clashes_receptor = " << std::boolalpha
                                       << __clashes_receptor(*pneighbor));
          dbgmsg("clashes_ligand = " << std::boolalpha
                                     << __clashes_ligand(*pneighbor,
                                                         curr_conformation,
                                                         curr_state));
          //~ if (!__clashes_receptor(*pneighbor)
          //~ && !__clashes_ligand(*pneighbor, curr_conformation, curr_state)) {
          if (!__clashes_ligand(
                  *pneighbor, curr_conformation,
                  curr_state)) { // don't check for clashes with receptor

            Partial next_conformation(curr_conformation);
            next_conformation.add_state(*pneighbor);

            // compute energy with original (static) receptor structure, use
            // expensive non_bonded_energy calculation only for linkers
            const double energy =
                curr_conformation.get_energy() +
                (pneighbor->get_energy() == 0
                     ? __opts.score.non_bonded_energy(
                           pneighbor->get_segment().get_atoms(),
                           pneighbor->get_crds(), __opts.gridrec)
                     : pneighbor->get_energy());

            next_conformation.set_energy(energy);
#ifndef NDEBUG
            // TEST if energy determined in a different way is the same
            double ene = 0.0, ene2 = 0.0;
            for (auto &pstate : next_conformation.get_states()) {
              if (pstate->get_energy() == 0) {
                ene += __opts.score.non_bonded_energy(
                    pstate->get_segment().get_atoms(), pstate->get_crds(),
                    __opts.gridrec);
              } else {
                ene += pstate->get_energy();
                ene2 += pstate->get_energy();
              }
            }
            dbgmsg("ene = " << ene << " ene2 = " << ene2
                            << " energy = " << energy);
#endif

            dbgmsg("accepting state " << *pneighbor);
            openset.insert(next_conformation);
          }
        }
      }
    }
  }
  dbgmsg("ASTAR FINISHED");
  if (min_conformation.get_energy() != MAX_ENERGY) {
    dbgmsg("SUCCESS minimum energy conformation at step = "
           << iter << " = " << std::endl
           << min_conformation);
  } else {
    dbgmsg("FAILED to connect start conformation : " << start_conformation);
    throw ConnectionError("[WHOOPS] could not connect this conformation",
                          failed_state_pairs);
  }

#ifndef NDEBUG
  DockedConformation mini = __reconstruct(min_conformation);
  dbgmsg("mini ligand = " << std::endl << mini.get_ligand());
  dbgmsg("ENERGY OF MINIMIZED LIGAND (reconstructed) = "
         << __opts.score.non_bonded_energy(mini.get_ligand(), __opts.gridrec));
  dbgmsg("ENERGY OF MINIMIZED LIGAND (calculated) = "
         << __opts.score.non_bonded_energy(min_conformation.get_ligand_atoms(),
                                           min_conformation.get_ligand_crds(),
                                           __opts.gridrec));
  dbgmsg("ENERGY OF MINIMIZED LIGAND (DockedConformation) = "
         << min_conformation.get_energy());
  for (auto &pstate : min_conformation.get_states()) {
    dbgmsg("seed " << pstate->get_segment().get_seed_id() << " original E = "
                   << pstate->get_energy() << " calculated E = "
                   << __opts.score.non_bonded_energy(
                          pstate->get_segment().get_atoms(), pstate->get_crds(),
                          __opts.gridrec));
  }

#endif
  return __reconstruct(min_conformation);
}

Partial::Vec Linker::StaticLinker::__generate_rigid_conformations(
    const Seed::Graph &seed_graph) {

  helper::Benchmark b;
  std::clog << "Generating rigid conformations of states..." << std::endl;

  State::PVec states;
  State::Id id = 0;
  for (auto &seed : seed_graph) {
#ifndef NDEBUG
    int state_no = 1;
#endif
    for (auto &pstate : seed->get_segment().get_states()) {
      states.push_back(&*pstate);
      pstate->set_id(id++);
      dbgmsg("back id = " << states.back()->get_id()
                          << " pstate id = " << pstate->get_id());
#ifndef NDEBUG
      pstate->set_no(state_no++);
#endif
      dbgmsg(*pstate);
    }
  }

  //~ helper::memusage("before find compatible state pairs");

  // init adjacency matrix (1 when states are compatible, 0 when not)
  helper::Array2d<bool> conn =
      __find_compatible_state_pairs(seed_graph, states.size());

  dbgmsg("dimensions of conn = " << conn.get_szi() << " " << conn.get_szj());
  dbgmsg("conn = " << conn);
  //~ helper::memusage("before max.clique.search");

  Partial::Vec possibles_w_energy;
  {

    std::vector<double> energies;
    for (int i = 0; i < states.size(); ++i) {
      energies.push_back(states[i]->get_energy());
    }

    dbgmsg(
        "largest possible clique (seed_graph.size()) = " << seed_graph.size());
    dbgmsg("currently searched for clique (__opts.k_clique_size) = "
           << __opts.k_clique_size);

    // max clique size is by default set to a big number 1000, so that it is
    // always greater then seed graph size, and therefore ALWAYS set to seed
    // graph size. you can set it to lower values as well, e.g. 3, if you want
    // to search for partial cliques
    const auto kcq_size = __opts.k_clique_size > seed_graph.size()
                              ? seed_graph.size()
                              : __opts.k_clique_size;

    // ////// FOR GENERATING DOCKING GRAPHS ONLY //////////////////////
    // namespace fs = std::filesystem;
    // static std::mutex local_mutex{};
    // {
    //   std::scoped_lock lock(local_mutex);
    //   const auto filename = __opts.ligand.name() + "_" +
    //                         __opts.receptor.name() + "_clq" +
    //                         std::to_string(kcq_size) + ".weighted_graph";
    //   auto filename_unique{filename}; // take care files are not overwritten
    //   auto version{0};
    //   while (
    //       fs::exists(fs::temp_directory_path() / fs::path(filename_unique)))
    //       {
    //     filename_unique = filename + "." + std::to_string(++version);
    //   }
    //   glib::io::write_dimacs_graph(fs::temp_directory_path() /
    //                                    fs::path(filename_unique),
    //                                conn, energies);
    // }
    // ////////////////////////////////////////////////////////////
    /* basic algorithm is more efficient here, see comments in issue #138 */
    const auto &[k_cliques, k_energies] =
        glib::algorithms::find_n_highest_weight_k_cliques<
            glib::algorithms::KCQWStrategy,
            glib::algorithms::LowestWeightPolicy>(
            conn, energies, kcq_size, __opts.n_cliques, __opts.max_steps);
    if (k_cliques.empty())
      throw helper::Error("[NOTE] No cliques found for ligand " +
                          __opts.ligand.name() + "!");
    //~ helper::memusage("after max.clique.search");

    std::clog << "Found " << k_cliques.size() << " maximum cliques, which took "
              << b.duration() << "s" << std::endl;

    if (k_cliques.empty())
      throw helper::Error(
          "[WHOOPS] Couldn't find any possible conformations for ligand " +
          __opts.ligand.name() + "!");

    for (auto i{0uz}; i < k_cliques.size(); ++i) {
      const auto &qmax = k_cliques[i];
      const auto &energy = k_energies[i];
      State::PVec conf;
      for (const auto &j : qmax) {
        conf.push_back(states[j]);
      }
      conf.shrink_to_fit();
      possibles_w_energy.push_back(Partial(conf, energy));

#ifndef NDEBUG
      std::stringstream ss;
      ss << "clique is composed of ";
      std::multimap<int, int> compo;

      for (auto &i : qmax) {
        ss << "seed_" << states[i]->get_segment().get_seed_id() << " state_"
           << states[i]->get_id() << " ene_" << states[i]->get_energy() << " ";
        compo.insert(std::make_pair(states[i]->get_segment().get_seed_id(),
                                    states[i]->get_id()));
      }
      ss << "ordered by seed_id : ";
      for (auto &kv : compo) {
        ss << kv.first << ":" << kv.second << " ";
      }
      ss << "total ene = " << energy;
      //				dbgmsg(ss.str());
      std::clog << ss.str() << std::endl;
#endif
    }

    //~ helper::memusage("after possibles_w_energy");
  }
  //~ helper::memusage("after forced destructor of k_cliques");

  dbgmsg("number of possibles_w_energy = " << possibles_w_energy.size());

  // cluster rigid conformations and take only cluster representatives for
  // further linking

  Partial::PVec ppossibles_w_energy;
  for (auto &pos : possibles_w_energy) {
    pos.set_geometric_center();
    ppossibles_w_energy.push_back(&pos);
  }

  cluster::GreedyCluster<Partial, Partial::CompareRMSDLessThanConstant,
                         Partial::CompareScoreLess>
      greedy;
  Partial::PVec preps = greedy.compute_uniform_representatives(
      ppossibles_w_energy, __opts.docked_clus_rad);

  Partial::Vec clustered_possibles_w_energy;
  for (const auto &prep : preps)
    clustered_possibles_w_energy.push_back(*prep);

  //~ helper::memusage("after greedy");

  possibles_w_energy.clear();

  //~ helper::memusage("after clearing possibles_w_energy");

  dbgmsg("number of clustered_possibles_w_energy = "
         << clustered_possibles_w_energy.size());

  if (__opts.max_possible_conf != -1 &&
      clustered_possibles_w_energy.size() > __opts.max_possible_conf) {
    clustered_possibles_w_energy.resize(__opts.max_possible_conf);
    dbgmsg("number of possible conformations > max_possible_conf, "
           << "resizing to= " << __opts.max_possible_conf << " conformations");
  }

  dbgmsg("RIGID CONFORMATIONS FOR LIGAND " << __opts.ligand.name() << " : "
                                           << std::endl
                                           << clustered_possibles_w_energy);

  std::clog << "Generated " << clustered_possibles_w_energy.size()
            << " possible conformations for ligand " << __opts.ligand.name()
            << ", which took " << b.duration() << " s" << std::endl;
  return clustered_possibles_w_energy;
}

DockedConformation
Linker::StaticLinker::__reconstruct(const Partial &conformation) {
  helper::Benchmark b;
  std::clog << "Reconstructing docked ligands..." << std::endl;
  int conf_number = 0;

  // ligand
  for (auto &pstate :
       conformation.get_states()) { // convert ligand to new coordinates
    State &state = *pstate;
    const Segment &segment = state.get_segment();
    Atom::PSet overlap;
    // deal with atoms that overlap between states
    for (auto &adjacent : segment) {
      const Bond &b = segment.get_bond(adjacent);
      overlap.insert(&b.atom2());
    }
    for (int i = 0; i < state.get_segment().get_atoms().size(); ++i) {
      Atom &atom = const_cast<Atom &>(
          state.get_segment().get_atom(i)); // ugly, correct this
      if (!overlap.count(&atom)) {
        const geom3d::Coordinate &crd = state.get_crd(i);
        atom.set_crd(crd);
      }
    }
  }

  Molecule ligand(__opts.ligand);
  ligand.set_name(__opts.ligand.name() + "_" +
                  helper::to_string(++conf_number));

  std::clog << "Reconstruction of molecules took " << b.duration() << "s"
            << std::endl;
  return DockedConformation(ligand, __opts.receptor, conformation.get_energy());
}

} // namespace insilab::linker
