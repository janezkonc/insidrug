#include "geom3d/quaternion.hpp"
#include "glib/algorithms/match.hpp"
#include "glib/algorithms/ring.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "linker.hpp"
#include "molib/details/internal.hpp"
#include "molib/molecules.hpp"
#include "ommiface/modeler.hpp"
#include "ommiface/prepare_for_mm.hpp"
#include "poses.hpp"
#include "score/scoringfunction.hpp"
#include <queue>

namespace insilab::linker {

using namespace molib;

/**
 * Each state is a mapping of docked seed atoms to ligands's
 * segment atoms.
 */
void Linker::GenericLinker::__create_states(
    const Segment::Graph &segment_graph,
    const std::vector<std::pair<int, Molecules>> &top_seeds) {
  for (const auto &[seed_id, seed_mols] : top_seeds) {
    dbgmsg("seed id = " << seed_id);
    // loop over segments with this name, seeds coordinates will not change
    for (auto &segment : segment_graph) {
      if (segment->get_seed_id() != seed_id)
        continue;
      dbgmsg("segment seed id = " << segment->get_seed_id());
      // create a graph out of the seed "nm" of "to be" ligand molecule
      if (seed_mols.empty())
        throw helper::Error("[WHOOPS] Seed could not be docked (seed_id = " +
                            std::to_string(seed_id) + ")");
      const auto &gs = seed_mols.first().get_atoms();
      dbgmsg("gs = " << std::endl << gs);
      const auto &gd = segment->get_atoms();
      dbgmsg("gd = " << std::endl << gd);
      // map seed molecules atom numbers to ligand molecule
      const auto m = glib::algorithms::match(
          gd, gs, glib::algorithms::compute_shortest_path_matrix(gd),
          [](const auto &a1, const auto &a2) {
            return a1.idatm_type_unmask() == a2.idatm_type_unmask();
          });
      dbgmsg("__create_states : m.size() = " << m.size());
      // consider ONLY THE FIRST mapping of seed to seed (the symmetry has
      // been taken care of in the rigid docking itself)...
      if (m.empty())
        throw helper::Error("[WHOOPS] could not find mapping of docked seed to "
                            "segment with same name ?");
      auto &mv = *m.begin();
      // create a state
      auto &vertices1 = mv.first;
      auto &vertices2 = mv.second;
      // for every docked rigid fragment
      for (auto &seed_molecule : seed_mols) {
        geom3d::Point::Vec crds(vertices2.size());
        const Atom::PVec &seed_atoms = seed_molecule.get_atoms();
        for (int i = 0; i < vertices2.size(); ++i) {
          crds[vertices1[i]] = seed_atoms.at(vertices2[i])->crd();
          dbgmsg("adding matched vertex pair "
                 << vertices1[i] << "," << vertices2[i]
                 << " new coordinates of atom "
                 << segment.get_atom(vertices1[i]).atom_number() << " are "
                 << crds[vertices1[i]]);
        }
        const double energy = stod(seed_molecule.name());
        if (energy < __opts.max_allow_energy) {
          dbgmsg("adding docked state " << segment.get_seed_id()
                                        << " with energy of " << energy);
          // ONLY COPY SEGMENT COORDS NOT SEED
          segment->add_state(std::make_unique<State>(*segment, crds, energy));
        }
      }
    }
  }
}

/**
 * Initialize OpenMM: add ligand coordinates (random) since otherwise
 * there are zeroes (at iterative minimization, when masking out parts
 * of ligand) in the openmm positions array which openmm does not like
 *
 */

void Linker::GenericLinker::__init_openmm(ommiface::Modeler &modeler) {

  modeler.add_topology(__opts.receptor.get_atoms());
  modeler.add_topology(__opts.ligand.get_atoms());

  modeler.init_openmm();

  modeler.add_random_crds(__opts.ligand.get_atoms());
}

/**
 * Minimization of each docked ligand conformation
 * with full ligand and receptor flexibility. Flexible region
 * can be limited to residues around the ligand.
 *
 */

DockedConformation
Linker::GenericLinker::__minimize(DockedConformation &docked_conformation) {

  dbgmsg("max_iterations_final " << __opts.max_iterations_final);

  if (__opts.max_iterations_final == 0)
    return std::move(docked_conformation);

  DockedConformation minimized_conformation;

  try {

    ommiface::Modeler modeler(__opts.modeler_opts);

    // init openmm
    __init_openmm(modeler);

    // minimize
    modeler.add_crds(__opts.receptor.get_atoms(),
                     docked_conformation.get_receptor().get_crds());
    modeler.add_crds(__opts.ligand.get_atoms(),
                     docked_conformation.get_ligand().get_crds());

    modeler.init_openmm_positions();

    // mask whole receptor
    modeler.mask(__opts.receptor.get_atoms());

    // unmask flexible receptor atoms and ligand atoms
    modeler.unmask_flexible();

    modeler.set_max_iterations(
        __opts.max_iterations_final); // until max iter final
    modeler.minimize_state(__opts.ligand, __opts.receptor
                           // #ifndef NDEBUG
                           //					       ,
                           //__opts.score #endif
    );

    // init with minimized coordinates
    Molecule minimized_receptor(__opts.receptor,
                                modeler.get_state(__opts.receptor.get_atoms()));
    Molecule minimized_ligand(__opts.ligand,
                              modeler.get_state(__opts.ligand.get_atoms()));

    ommiface::undo_mm_specific(minimized_receptor);

    Atom::Grid gridrec(minimized_receptor.get_atoms());

    dbgmsg("score after minimization "
           << __opts.score.non_bonded_energy(minimized_ligand, gridrec));
    dbgmsg("ligand after minimization " << minimized_ligand);

    minimized_conformation = DockedConformation(
        minimized_ligand, minimized_receptor,
        __opts.score.non_bonded_energy(minimized_ligand, gridrec));

  } catch (const std::exception &e) {
    std::cerr << "[NOTE] failed to minimize one conformation of ligand "
              << __opts.ligand.name() << " due to : " << e.what()
              << " (skipping...)" << std::endl;
    throw e; // rethrow
  }

  return minimized_conformation;
}

Partial::Vec Linker::GenericLinker::init_conformations() {
  std::clog << "Starting connection of seeds for ligand "
            << __opts.ligand.name() << std::endl;

  // A graph of segments is constructed in which each rigid segment is
  // a vertex & segments that are part of seeds are identified.
  __segment_graph = Segment::create_graph(__opts.fragments);
  dbgmsg("segment_graph = " << __segment_graph);
  if (!glib::algorithms::find_cycles(__segment_graph).empty()) {
    throw helper::Error(
        "[WHOOPS] cyclic molecules are currently not supported");
  }

  dbgmsg("segment graph for ligand " << __opts.ligand.name() << " = "
                                     << __segment_graph);
  __create_states(__segment_graph, __opts.top_seeds);

  __seed_graph = Seed::create_graph(__segment_graph);
  dbgmsg("seed graph for ligand " << __opts.ligand.name() << " = "
                                  << __seed_graph);

  return __generate_rigid_conformations(__seed_graph);
}

/**
 * Link the conformations using the A-STAR algorithm
 *
 */
DockedConformation
Linker::GenericLinker::compute_conformation(const Partial &partial) {

  DockedConformation docked_conformation;

  try {

    auto &conformation = partial.get_states();

    if (!__has_blacklisted(conformation, __blacklist)) {

      Partial grow_link_energy(conformation, partial.get_energy());
      dbgmsg("connecting ligand " << __opts.ligand.name() << std::endl
                                  << "possible starting conformation is "
                                  << std::endl
                                  << grow_link_energy);

      std::vector<std::unique_ptr<State>> states;
      docked_conformation = __a_star(__segment_graph.size(), grow_link_energy,
                                     states, __opts.link_iter);
    }

    docked_conformation = __minimize(docked_conformation);

    std::clog << "Succesfully linked a conformation for ligand "
              << __opts.ligand.name() << std::endl;

  } catch (const ConnectionError &e) {

    std::lock_guard<std::mutex> guard(__mtx);

    std::cerr << "[NOTE] could not reconnect one conformation of ligand "
              << __opts.ligand.name() << " due to " << e.what() << std::endl;

    dbgmsg("exception : " << e.what());
    for (auto &failed_pair : e.get_failed_state_pairs())
      __blacklist.insert(failed_pair);

    throw e;

  } catch (const std::exception &e) {
    std::cerr
        << "[NOTE] a general exception happened during reconnection of ligand "
        << __opts.ligand.name() << std::endl;
    throw e;
  }

  return docked_conformation;
}

bool Linker::GenericLinker::__clashes_receptor(const State &current) const {
  for (int i = 0; i < current.get_crds().size(); ++i) {
    const Atom &a = current.get_segment().get_atom(i);
    const geom3d::Coordinate &c = current.get_crd(i);
    dbgmsg("in clashes_receptor test coordinate = " << c);
    if (__opts.gridrec.clashes(Atom({
                                   .crd = c,
                                   .idatm_type = a.idatm_type(),
                               }),
                               __opts.clash_coeff))
      return true;
  }
  return false;
}

bool Linker::GenericLinker::__clashes_ligand(const State &current,
                                             const Partial &conformation,
                                             const State &prev) const {

  // clashes between current segment and previous segments
  for (auto &pstate : conformation.get_states()) {
    dbgmsg("clashes_ligand test for state " << current << " and state "
                                            << *pstate);
    if (current.clashes(*pstate, __opts.clash_coeff))
      return true;
  }
  return false;
}

double Linker::GenericLinker::__distance(const State &start,
                                         const State &goal) const {
  const Segment &start_segment = start.get_segment();
  const Segment &goal_segment = goal.get_segment();
  const int start_atom_idx =
      start_segment.get_bond(start_segment.get_next(goal_segment)).idx1();
  const int goal_atom_idx =
      goal_segment.get_bond(goal_segment.get_next(start_segment)).idx1();
  const geom3d::Coordinate &first_crd = start.get_crd(start_atom_idx);
  const geom3d::Coordinate &last_crd = goal.get_crd(goal_atom_idx);
  return first_crd.distance(last_crd);
}

geom3d::Point::Vec
Linker::GenericLinker::__rotate(const geom3d::Quaternion &q,
                                const geom3d::Point &p1,
                                const geom3d::Point::Vec &crds) {

  geom3d::Point::Vec rotated;
  for (auto &crd : crds) {
    rotated.push_back(q.rotatedVector(crd - p1) + p1);
  }
  return rotated;
}

State::PVec Linker::GenericLinker::__compute_neighbors(
    const State &curr_state, Segment &next,
    std::vector<std::unique_ptr<State>> &states) {
  const int ini_sz = states.size();
  dbgmsg("in compute_neighbors for state = " << curr_state);
  const Segment &current = curr_state.get_segment();
  dbgmsg("compute_neighbors : current segment is " << current);
  dbgmsg("compute_neighbors : next segment is " << next);
  const Bond &btorsion = current.get_bond(next);
  const int idx3 = btorsion.idx2();
  const Atom *a3 = &btorsion.atom2();
  const int idx2 = btorsion.idx1();
  const Atom *a2 = &btorsion.atom1();
  const int idx1 = current.adjacent_in_segment(
      *a2, *a3); // segments overlap on 1 rotatable bond
  const Atom *a1 = &current.get_atom(idx1);
  const int idx4 = next.adjacent_in_segment(*a3, *a2);
  const Atom *a4 = &next.get_atom(idx4);

  dbgmsg("a4 = " << *a4 << " coordinates not set yet!");
  geom3d::Coordinate crd3(curr_state.get_crd(idx3));
  dbgmsg("a3 = " << *a3 << " crd3 = " << curr_state.get_crd(idx3));
  geom3d::Coordinate crd2(curr_state.get_crd(idx2));
  dbgmsg("a2 = " << *a2 << " crd2 = " << curr_state.get_crd(idx2));
  geom3d::Coordinate crd1(curr_state.get_crd(idx1));
  dbgmsg("a1 = " << *a1 << " crd1 = " << curr_state.get_crd(idx1));

  const double saved_dihedral = __opts.ic.get_dihedral(*a1, *a2, *a3, *a4);
  __opts.ic.set_dihedral(*a1, *a2, *a3, *a4, 0.0);

  states.push_back(std::unique_ptr<State>(
      new State(next, __opts.ic.cartesian(*a1, *a2, *a3, crd1, crd2, crd3,
                                          next.get_atoms()))));

  __opts.ic.set_dihedral(*a1, *a2, *a3, *a4, saved_dihedral);

#ifndef NDEBUG
  State &initial = *states.back();
#endif
  dbgmsg("this is initial state = " << initial);
  dbgmsg("a4 = " << *a4
                 << " newly determined crd4 = " << initial.get_crd(idx4));
  dbgmsg("rotate next segment on vector = "
         << crd2 << " - " << crd3 << " by "
         << geom3d::degrees(__opts.spin_degrees) << " degree increments");
  const geom3d::Quaternion q(geom3d::Vector3(crd3 - crd2).norm() *
                                 sin(__opts.spin_degrees),
                             cos(__opts.spin_degrees));
  for (double angle = __opts.spin_degrees; angle < M_PI;
       angle += __opts.spin_degrees) {
    State &previous_rotated = *states.back();
    states.push_back(std::unique_ptr<State>(
        new State(next, __rotate(q, crd2, previous_rotated.get_crds()))));
    dbgmsg("rotated state at angle = " << geom3d::degrees(angle) << " is "
                                       << *states.back());
  }

  State::PVec ret;
  for (int i = ini_sz; i < states.size(); ++i)
    ret.push_back(&*states[i]);

  return ret;
}

bool Linker::GenericLinker::__check_distances_to_seeds(
    const State &curr_state, const Segment &adjacent,
    const SegStateMap &docked_seeds) {

  const Segment &current = curr_state.get_segment();
  dbgmsg("curr_state= " << curr_state);
  dbgmsg("adjacent= " << adjacent);
  for (auto &adjacent_seed_segment : adjacent.get_adjacent_seed_segments()) {
    auto it = docked_seeds.find(adjacent_seed_segment);
    if (it != docked_seeds.end() && it->first != &current) {

      const double distance = __distance(curr_state, *it->second);
      const double mll = current.get_max_linker_length(*it->first);

      dbgmsg("distance = " << distance);
      dbgmsg("mll = " << mll);
      dbgmsg("is distance OK ? " << boolalpha
                                 << (distance < mll + __opts.tol_seed_dist &&
                                     distance > mll - __opts.tol_seed_dist));
      dbgmsg("curr_state= " << curr_state);
      dbgmsg("adjacent seed state= " << *it->second);

      if (distance > mll + __opts.tol_seed_dist ||
          distance < mll - __opts.tol_seed_dist) {
        dbgmsg("returning false");
        return false;
      }
    }
  }
  dbgmsg("returning true");
  return true;
}

State *Linker::GenericLinker::__is_seed(const Segment &seg,
                                        const SegStateMap &docked_seeds) {
  auto it = docked_seeds.find(&seg);
  return (it == docked_seeds.end() ? nullptr : it->second);
}

std::pair<State *, Segment *>
Linker::GenericLinker::__find_good_neighbor(const Partial &curr_conformation,
                                            const SegStateMap &docked_seeds) {

  Segment::PSet done_segments, free_seeds;
  for (auto &pcurr_state : curr_conformation.get_states()) {
    done_segments.insert(const_cast<Segment *>(&pcurr_state->get_segment()));
  }
  for (auto &kv : docked_seeds) {
    const Segment &docked_segment = *kv.first;
    if (!done_segments.count(const_cast<Segment *>(&docked_segment))) {
      free_seeds.insert(const_cast<Segment *>(&docked_segment));
    }
  }
  std::pair<State *, Segment *> good;
  for (auto &pcurr_state : curr_conformation.get_states()) {
    State &curr_state = *pcurr_state;
    const Segment &curr_segment = curr_state.get_segment();
    for (auto &pgoal : free_seeds) {
      if (curr_segment.has_next(*pgoal)) {
        Segment &next = curr_segment.get_next(*pgoal);
        if (!done_segments.count(&next)) {
          return {&curr_state, &next};
        }
      }
    }
  }
  for (auto &pcurr_state : curr_conformation.get_states()) {
    State &curr_state = *pcurr_state;
    for (auto &adj : curr_state.get_segment()) {
      if (!done_segments.count(&adj)) { // don't go back
        return {&curr_state, &adj};
      }
    }
  }
  throw helper::Error(
      "[WHOOPS] Could not find a pair of 'good' neighbor states");
}

/**
 * Find all pairs of compatible states at correct distances for
 * the multi-seed molecules
 */
helper::Array2d<bool> Linker::GenericLinker::__find_compatible_state_pairs(
    const Seed::Graph &seed_graph, const int sz) {

  helper::Benchmark b;
  std::clog << "Finding compatible state pairs..." << std::endl;

  int idx = 0;

  helper::Array2d<bool> conn(sz);
  Poses poses(seed_graph);
  for (int u = 0; u < seed_graph.size(); ++u) {
    Segment &segment1 = seed_graph[u]->get_segment();
    for (int v = u + 1; v < seed_graph.size(); ++v) {
      Segment &segment2 = seed_graph[v]->get_segment();
      const double max_linker_length = segment1.get_max_linker_length(segment2);
      Atom::Pair jatoms{
          &segment1.get_bond(segment1.get_next(segment2)).atom1(),
          &segment2.get_bond(segment2.get_next(segment1)).atom1()};
#ifndef NDEBUG
      const Bond &excluded =
          (segment1.is_adjacent(segment2) ? segment1.get_bond(segment2)
                                          : Bond());
      dbgmsg("finding compatible state pairs for segments "
             << segment1 << " and " << segment2
             << " with maximum linker length " << max_linker_length << " and "
             << " join atom1 = " << jatoms.first->atom_number()
             << " join atom2 = " << jatoms.second->atom_number()
             << " excluded = " << excluded);
#endif
      for (auto &pstate1 : segment1.get_states()) {
        State &state1 = *pstate1;
        State::PSet join_states = poses.get_join_states(
            state1, segment2, jatoms, max_linker_length,
            __opts.lower_tol_seed_dist, __opts.upper_tol_seed_dist);

        const int i = state1.get_id();
        dbgmsg("comparing state1(" << i << ") = " << state1);
        for (auto &pstate2 : join_states) {
          dbgmsg("with state2(" << pstate2->get_id() << ") = " << *pstate2);
          if (!state1.clashes(*pstate2, __opts.clash_coeff)) {
            const int j = pstate2->get_id();
            conn.set(i, j);
            conn.set(j, i);
            dbgmsg("compatible states " << i << " and " << j);
            ++idx;
          }
        }
      }
    }
  }

  std::clog << "Found " << idx << " compatible state pairs which took "
            << b.duration() << "s" << std::endl;
  return conn;
}

bool Linker::GenericLinker::__has_blacklisted(
    const State::PVec &conformation, const std::set<State::CPair> &blacklist) {

  std::lock_guard<std::mutex> guard(__mtx);

  for (int i = 0; i < conformation.size(); ++i) {
    for (int j = i + 1; j < conformation.size(); ++j) {
      if (blacklist.count({conformation[i], conformation[j]}) ||
          blacklist.count({conformation[j], conformation[i]}))
        return true;
    }
  }
  return false;
}

} // namespace insilab::linker
