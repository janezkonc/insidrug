#include "cluster/greedy.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/quaternion.hpp"
#include "glib/algorithms/mcqd.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "linker.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "ommiface/modeler.hpp"
#include "poses.hpp"
#include "score/scoringfunction.hpp"
#include <queue>

namespace insilab::linker {

using namespace molib;

/**
 * Iterative minimization starts from each seed in the docked molecule.
 * It takes top N percent of each seed's docked conformations, and then
 * reconstructs the whole molecule from that seed.
 *
 */

DockedConformation Linker::IterativeLinker::__a_star(
    const int segment_graph_size, const Partial &start_conformation,
    std::vector<std::unique_ptr<State>> &states, int iter) {

  ommiface::Modeler modeler(__opts.modeler_opts);

  // init openmm
  __init_openmm(modeler);

  for (auto &pstate : start_conformation.get_states())
    states.push_back(std::unique_ptr<State>(new State(*pstate)));
  if (start_conformation.empty())
    throw helper::Error(
        "[WHOOPS] at least one docked anchor state is required for linking");
  SegStateMap docked_seeds;
  for (auto &pstate : states)
    docked_seeds.insert({&pstate->get_segment(), &*pstate});
  std::set<State::CPair> failed_state_pairs;
  Partial min_conformation(MAX_ENERGY);
  PriorityQueue
      openset; // openset has conformations sorted from lowest to highest energy
  openset.insert(Partial(State::PVec{&*states[0]}, states[0]->get_energy(),
                         __opts.receptor.get_crds()));

  while (!openset.empty()) {
    if (--iter < 0)
      break;
    Partial curr_conformation = *openset.begin();
    openset.erase(openset.begin());
    dbgmsg("openset.size() = " << openset.size());
    dbgmsg("curr_conformation at step = " << iter << " = " << std::endl
                                          << curr_conformation << std::endl
                                          << to_pdb(curr_conformation));
    if (curr_conformation.size() == segment_graph_size) {
      dbgmsg("CANDIDATE for minimum energy conformation at step = "
             << iter << " = " << std::endl
             << curr_conformation);
      if (curr_conformation.get_energy() < min_conformation.get_energy()) {
        min_conformation = curr_conformation;
        dbgmsg("ACCEPTED");
      }
    } else {
      // grow in the direction of (a) first "free" seed state (b) if such
      // path does not exist, then grow in the first possible direction
      std::pair<State *, Segment *> ret =
          __find_good_neighbor(curr_conformation, docked_seeds);
      State &curr_state = *ret.first;
      Segment &adj = *ret.second;
      State *adj_is_seed = __is_seed(adj, docked_seeds);
      // check seed distances here
      dbgmsg("adj_is_seed " << (adj_is_seed ? "true" : "false"));
      dbgmsg("check_distances_to_seeds = "
             << boolalpha
             << (adj_is_seed ||
                 __check_distances_to_seeds(curr_state, adj, docked_seeds)));
      if (adj_is_seed ||
          __check_distances_to_seeds(curr_state, adj, docked_seeds)) {
        auto ret2 =
            (adj_is_seed ? State::PVec{adj_is_seed}
                         : __compute_neighbors(curr_state, adj, states));
        for (auto &pneighbor : ret2) {
          dbgmsg("CHECKING NEIGHBOR : " << *pneighbor);
          dbgmsg("clashes_ligand = " << boolalpha
                                     << __clashes_ligand(*pneighbor,
                                                         curr_conformation,
                                                         curr_state));
          if (!__clashes_ligand(
                  *pneighbor, curr_conformation,
                  curr_state)) { // don't check for clashes with receptor

            Partial next_conformation(curr_conformation);
            next_conformation.add_state(*pneighbor);

            // prepare for minimization receptor and ligand coordinates
            modeler.add_crds(__opts.receptor.get_atoms(),
                             next_conformation.get_receptor_crds());
            modeler.add_crds(next_conformation.get_ligand_atoms(),
                             next_conformation.get_ligand_crds());
            modeler.init_openmm_positions();
            modeler.mask(__opts.ligand.get_atoms());
            modeler.unmask(next_conformation.get_ligand_atoms());

#ifndef NDEBUG
            dbgmsg("initial coordinates:");
            for (auto &point : next_conformation.get_ligand_crds())
              dbgmsg(point);
#endif

            // minimize ...
            modeler.minimize_state(__opts.ligand, __opts.receptor
#ifndef NDEBUG
                                   ,
                                   __opts.score
#endif
            );

#ifndef NDEBUG
            dbgmsg("minimized coordinates:");
            for (auto &point :
                 modeler.get_state(next_conformation.get_ligand_atoms()))
              dbgmsg(point);
#endif

            // init with minimized coordinates
            Molecule minimized_receptor(
                __opts.receptor,
                modeler.get_state(__opts.receptor.get_atoms()));
            next_conformation.set_receptor_crds(minimized_receptor.get_crds());
            next_conformation.set_ligand_crds(
                modeler.get_state(next_conformation.get_ligand_atoms()));

            // compute energy after minimization
            Atom::Grid gridrec(minimized_receptor.get_atoms());

            const double energy = __opts.score.non_bonded_energy(
                next_conformation.get_ligand_atoms(),
                next_conformation.get_ligand_crds(), gridrec);

            next_conformation.set_energy(energy);
            dbgmsg("accepting state " << *pneighbor);
            openset.insert(next_conformation);
          }
        }
      }
    }
  }
  dbgmsg("ASTAR FINISHED");
  if (min_conformation.get_energy() != MAX_ENERGY) {
    dbgmsg("SUCCESS minimum energy conformation at step = "
           << iter << " = " << std::endl
           << min_conformation);
  } else {
    dbgmsg("FAILED to connect start conformation : " << start_conformation);
    throw ConnectionError("[WHOOPS] could not connect this conformation",
                          failed_state_pairs);
  }
  return __reconstruct(min_conformation);
}

/**
 * For iterative create top percent states of each seed. Here, partial
 * contains only one state (or any desired number up to seed_graph.size())
 *
 */

Partial::Vec Linker::IterativeLinker::__generate_rigid_conformations(
    const Seed::Graph &seed_graph) {
  helper::Benchmark b;
  std::clog << "Generating rigid conformations of states..." << std::endl;

  State::PVec states;
  State::Id id = 0;
  for (auto &seed : seed_graph)
    for (auto &pstate : seed->get_segment().get_states()) {
      states.push_back(&*pstate);
      pstate->set_id(id++);
      dbgmsg(*pstate);
    }

  //~ helper::memusage("before find compatible state pairs");

  // init adjacency matrix (1 when states are compatible, 0 when not)
  helper::Array2d<bool> conn =
      __find_compatible_state_pairs(seed_graph, states.size());

  dbgmsg("dimensions of conn = " << conn.get_szi() << " " << conn.get_szj());
  dbgmsg("conn = " << conn);

  //~ helper::memusage("before max.clique.search");

  std::clog << "Finding compatible state pairs took " << b.duration() << "s"
            << std::endl;

  Partial::Vec possibles_w_energy;
  {
    // find all maximum cliques with the number of seed segments of
    // __opts.k_clique_size

    // max clique size is by default set to a big number 1000, so that it is
    // always greater then seed graph size, and therefore ALWAYS set to seed
    // graph size. ESPECIALLY for ITERATIVE, you can set it to lower values as
    // well, e.g. 1, so you get only one "seed" state which is then extended
    // iteratively
    const auto kcq_size = __opts.k_clique_size > seed_graph.size()
                              ? seed_graph.size()
                              : __opts.k_clique_size;
    //			const std::vector<std::vector<unsigned short int>>
    //&qmaxes = m.mcq(kcq_size);
    const auto qmaxes =
        glib::algorithms::find_n_k_cliques(conn, kcq_size, __opts.n_cliques);
    if (qmaxes.empty())
      throw helper::Error("[NOTE] No cliques found for ligand " +
                          __opts.ligand.name() + "!");

    //
    //~ helper::memusage("after max.clique.search");

    std::clog << "Found " << qmaxes.size() << " maximum cliques, which took "
              << b.duration() << "s" << std::endl;

    if (qmaxes.empty())
      throw helper::Error(
          "[WHOOPS] couldn't find any possible conformations for ligand " +
          __opts.ligand.name());

    for (const auto &qmax : qmaxes) {
      const double energy = 0.0;
      State::PVec conf;

      for (auto &j : qmax) {
        conf.push_back(states[j]);
      }

      conf.shrink_to_fit();
      possibles_w_energy.push_back(Partial(conf, energy));
    }

    //~ helper::memusage("after possibles_w_energy");
  }
  //~ helper::memusage("after forced destructor of qmaxes");

  //~ helper::memusage("after sort of possibles_w_energy");

  dbgmsg("number of possibles_w_energy = " << possibles_w_energy.size());
  std::clog << "Generated " << possibles_w_energy.size()
            << " possible top percent docked seeds that will serve as starting "
               "points for reconstruction of ligand "
            << __opts.ligand.name() << ", which took " << b.duration() << "s"
            << std::endl;
  return possibles_w_energy;
}

DockedConformation
Linker::IterativeLinker::__reconstruct(const Partial &conformation) {
  helper::Benchmark b;
  ;
  std::clog << "Reconstructing docked ligands..." << std::endl;
  int conf_number = 0;

  // ligand
  for (auto &pstate :
       conformation.get_states()) { // convert ligand to new coordinates
    State &state = *pstate;
    const Segment &segment = state.get_segment();
    Atom::PSet overlap;
    // deal with atoms that overlap between states
    for (auto &adjacent : segment) {
      const Bond &b = segment.get_bond(adjacent);
      overlap.insert(&b.atom2());
    }
    for (int i = 0; i < state.get_segment().get_atoms().size(); ++i) {
      Atom &atom = const_cast<Atom &>(
          state.get_segment().get_atom(i)); // ugly, correct this
      if (!overlap.count(&atom)) {
        const geom3d::Coordinate &crd = state.get_crd(i);
        atom.set_crd(crd);
      }
    }
  }

  Molecule ligand(__opts.ligand);
  ligand.set_name(__opts.ligand.name() + "_" +
                  helper::to_string(++conf_number));

  // receptor
  Molecule receptor(__opts.receptor);
  const geom3d::Point::Vec &crds = conformation.get_receptor_crds();
  int i = 0;
  for (auto &patom : receptor.get_atoms()) {
    patom->set_crd(crds[i++]);
  }
  std::clog << "Reconstruction of molecules took " << b.duration() << "s"
            << std::endl;
  return DockedConformation(ligand, receptor, conformation.get_energy());
}

} // namespace insilab::linker
