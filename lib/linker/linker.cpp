#include "linker.hpp"

namespace insilab::linker {

Linker::Linker(Options &opts) {

  if (opts.iterative) {
    l = std::unique_ptr<IterativeLinker>(new IterativeLinker(opts));
  } else {
    l = std::unique_ptr<StaticLinker>(new StaticLinker(opts));
  }
}

Partial::Vec Linker::compute_partial() { return l->init_conformations(); }

DockedConformation Linker::link_minimize(const Partial &p) {
  return l->compute_conformation(p);
}

} // namespace insilab::linker
