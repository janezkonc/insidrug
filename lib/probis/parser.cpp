#include "parser.hpp"
#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "path/path.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::probis {

Surface Parser::parse() {

  std::lock_guard<std::mutex> guard(__mtx);
  const auto raw_file = __p->read_file();
  return __p->parse(raw_file);
}

Parser::Parser(const std::string &surface_file, Parser::Options hm,
               const int num_occur) {
  if (path::has_suffix(surface_file, ".srf") ||
      path::has_suffix(surface_file, ".srf.gz"))
    __p = std::make_unique<SrfParser>(surface_file, hm, num_occur);
  else if (path::has_suffix(surface_file, ".srfx") ||
           path::has_suffix(surface_file, ".srfx.gz"))
    __p = std::make_unique<SrfXParser>(surface_file, hm, num_occur);
  else
    throw helper::Error("[WHOOPS] could not determine type of input file " +
                        surface_file);
}

} // namespace insilab::probis
