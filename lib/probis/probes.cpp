#include "probes.hpp"

namespace insilab::probis {

std::ostream &operator<<(std::ostream &out, const Probes &p) {
  for (auto &probe : p.__probes) {
    out << "P> " << probe.id() << " " << probe.crd() << " " << probe.surf_id()
        << std::endl;
  }
  return out;
}

Probe &Probes::add_probe(const Probe::Params &p) {
  __probes.push_back(Probe(p));
  return __probes.back();
}

} // namespace insilab::probis
