#include "descriptor.hpp"
#include "details/mnsp.hpp"
#include "details/rules.hpp"
#include "helper/benchmark.hpp"
#include "insilab/score.hpp"
#include "molib/algorithms/search_replace.hpp"
#include "molib/algorithms/surface.hpp"
#include "molib/molecule.hpp"
#include "ommiface/prepare_for_mm.hpp"
#include <cassert>
#include <regex>

namespace insilab::probis {

using namespace molib;

std::vector<std::vector<Descriptor>>
generate_descriptors(const Molecule &molecule,
                     const helper::rename_rules &functional_groups) {

  helper::Benchmark<std::chrono::milliseconds> b;

  // return all surfaces (indexes) to which belong any of matched atoms
  const auto surface_indexes = [](const auto &matched_atoms,
                                  const auto &surfaces) {
    std::vector<std::size_t> result;
    for (auto i{0uz}; i < surfaces.size(); ++i) {
      for (const auto &patom : matched_atoms) {
        if (surfaces[i].contains(patom)) {
          result.push_back(i);
          break;
        }
      }
    }
    return result;
  };

  auto molecule_copy{molecule}; // make a copy!

  ommiface::connect_bonds_standard_residues(molecule_copy);

  const auto &[surface_atoms, surface_probes] =
      algorithms::compute_surface_atoms(molecule_copy);

  std::vector<Atom::CPSet> surfaces;
  std::ranges::transform(
      surface_atoms, std::back_inserter(surfaces), [](const auto &atoms) {
        return Atom::CPSet(std::begin(atoms), std::end(atoms));
      });
  assert(!surfaces.empty());

  std::vector<std::vector<Descriptor>> descs_all_surfaces(surfaces.size());

  algorithms::replace_substructure(
      molecule_copy, functional_groups,
      // custom apply_rule function
      [&descs_all_surfaces, &surfaces,
       &surface_indexes](const auto &assigned_rules) {
        for (const auto &[matched_atoms, rule] : assigned_rules) {
          assert(!matched_atoms.empty());
          const auto &rule_v = helper::split(rule, "=");
          assert(rule_v.size() == 2);
          const auto &mnsp = rule_v[1];
          for (const auto i : surface_indexes(matched_atoms, surfaces)) {
            geom3d::Point::Vec crds;
            std::ranges::transform(
                matched_atoms, std::back_inserter(crds),
                [](const auto &patom) { return patom->crd(); });
            descs_all_surfaces[i].emplace_back(Descriptor::Params{
                .id = descs_all_surfaces[i].size(),
                .crd = geom3d::compute_geometric_center(crds),
                .mnsp = details::to_mnsp(mnsp),
                .atom_number = matched_atoms.at(0)->atom_number(),
            });
          }
        }
      },
      // custom compare_atoms functions
      [](const Atom &atom, const Atom &pseudo_atom) {
        const auto &s_v = helper::split(pseudo_atom.get_smiles_label(), ",");
        assert(s_v.size() == 1 || s_v.size() == 2);
        const auto &atom_name = s_v[0];
        const auto &resn = s_v.size() == 2 ? s_v[1] : "";
        const auto residues_match =
            resn.empty() || atom.residue().resn() == resn;
        const auto atoms_match =
            std::regex_search(atom.atom_name(), std::regex(atom_name));
        return atoms_match && residues_match;
      });

  std::clog << "Computing descriptors took " << b.duration()
            << " ms and resulted in "
            << (descs_all_surfaces.empty()
                    ? "no descriptors found"
                    : std::to_string(descs_all_surfaces.at(0).size()) +
                          " descriptors on the largest surface")
            << ".\n";

  return descs_all_surfaces;
}

std::ostream &operator<<(std::ostream &out, const Descriptor &desc) {
  out << "D> " << desc.id() << " " << desc.crd() << " " << desc.mnsp() << " "
      << desc.atom_number();
  return out;
}

std::pair<std::vector<Descriptor>, std::vector<Descriptor>>
compute_descriptor_product_graph_vertices(
    const std::array<const Descriptor *, 4> &desc1_v,
    const std::array<const Descriptor *, 4> &desc2_v) {
  std::pair<std::vector<Descriptor>, std::vector<Descriptor>> result;
  for (const auto &pdesc1 : desc1_v) {
    for (const auto &pdesc2 : desc2_v) {
      if (*pdesc1 == *pdesc2) {
        result.first.push_back(*pdesc1);
        result.second.push_back(*pdesc2);
      }
    }
  }
  return result;
}

// std::ostream &operator<<(std::ostream &out, const Descriptor &desc) {
//   out << std::setw(6) << std::left << "HETATM" << std::setw(5) << std::right
//       << desc.id() << std::setw(1) << " " << std::setw(4) << std::left
//       << details::from_mnsp_to_atom_name(desc.mnsp()) << std::setw(1) << " "
//       << std::setw(3) << std::right << "XXX" << std::setw(1) << " "
//       << std::setw(1) << "X" << std::setw(4) << std::right << 1 <<
//       std::setw(1)
//       << " " << std::setw(27) << desc.crd().pdb() << std::setw(6)
//       << std::setprecision(2) << std::fixed << std::right << 1.0 <<
//       std::setw(6)
//       << std::setprecision(2) << std::fixed << std::right << 1.00
//       << std::setw(12) << std::right
//       << details::from_mnsp_to_atom_name(desc.mnsp()) << std::setw(2)
//       << std::right << " ";
//   return out;
// }

std::ostream &operator<<(std::ostream &out,
                         const std::vector<Descriptor> &descs) {
  for (const auto &desc : descs) {
    out << desc << std::endl;
  }
  return out;
}

std::ostream &
operator<<(std::ostream &out,
           const std::vector<std::vector<Descriptor>> &descs_all_surfaces) {
  for (auto i{0uz}; i < descs_all_surfaces.size(); ++i) {
    out << "SURFACE " << i << std::endl;
    for (const auto &desc : descs_all_surfaces[i]) {
      out << desc << std::endl;
    }
  }
  return out;
}

// void Descriptors::mark_backbone() {
//   // mark descriptors that belong to backbone (peptide) atoms
//   for (auto &d : __descriptors) {
//     if (d->atom->atom_name() == "N" || d->atom->atom_name() == "O") {
//       d->set_bb(true);
//     }
//   }
// }

} // namespace insilab::probis
