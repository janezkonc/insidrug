#pragma once

#include "grid/grid.hpp"
#include "helper/benchmark.hpp"
#include "helper/it.hpp"
#include <array>
#include <map>
#include <set>
#include <vector>

namespace insilab::probis {

template <typename T>
struct Product : public helper::template_vector_container<Product<T>> {
  const std::pair<const T, const T> similar_entities;
  Product(const std::pair<const T, const T> se) : similar_entities(se){};
};

/**
 * For a product graph vertex (a pair of vertices v1, v2 in the original
 * graphs), we find all neighboring product graph vertices so that dist(v1,v1')
 * < cutoff_product and dist(v2, v2') < cutoff_product.
 *
 * @param v1 the first component of the product graph vertex
 * @param v2 the second component of the product graph vertex
 * @param similar_vertices all product graph vertices from which we will select
 * the ones close to the vertex (v1, v2)
 * @param cutoff_product a distance in Angstroms around both compound vertices
 * that is considered
 * @return a pair of vectors, where the first vector contains vertices of the
 * first graph and the second contains the corresponding vertices of the second
 * graph
 */
template <typename T>
std::pair<std::vector<T>, std::vector<T>>
find_product_graph_vertices_near_central_vertex(
    const T &v1, const T &v2,
    const std::pair<std::vector<T>, std::vector<T>> &similar_vertices,
    const double cutoff_product) {

  const auto &[vertices1, vertices2] = similar_vertices;
  const typename T::Grid grid1(vertices1), grid2(vertices2);
  const auto similar_vertices_s = [](const auto &vertices1,
                                     const auto &vertices2) {
    std::set<std::pair<const T *, const T *>> result;
    for (auto i{0uz}; i < vertices1.size(); ++i) {
      const auto &v1 = vertices1[i];
      const auto &v2 = vertices2[i];
      result.insert({&v1, &v2});
    }
    return result;
  }(vertices1, vertices2);

  // generate locally constrained product graph(s)
  const auto &neighb1 =
      grid1.get_neighbors_including_self(v1.crd(), cutoff_product);
  const auto &neighb2 =
      grid2.get_neighbors_including_self(v2.crd(), cutoff_product);

  std::pair<std::vector<T>, std::vector<T>> result;
  for (const auto &nv1 : neighb1) {
    for (const auto &nv2 : neighb2) {
      if (!similar_vertices_s.contains({nv1, nv2}))
        continue;
      result.first.push_back(*nv1);
      result.second.push_back(*nv2);
    }
  }
  return result;
}

/**
 * Compute locally constrained product graphs. Similar vertex pairs that were
 * found are now considered as product graph vertices. Two such vertices are
 * connected by an edge if the difference of the distance between their
 * coordinates abs(dist1(T1, T2) - dist2(T1', T2')) is less than cutoff_product
 * Angstroms.
 *
 * @param similar_entities a pair of vectors, the first being vertex from
 * the first graph and the second the corresponding vertex from the second
 * graph
 * @param cutoff_product the distance in Angstroms around a pair of similar
 * vertices to consider as a local product graph
 * @param eps_product if the difference between distance of the pair of vertices
 * in the first graph and the distance between the pair of vertices in the
 * second graph is smaller than EPS, then connect the two product graph vertices
 * @return vector of graphs (one for each vertex pair) in which
 * vertex pairs (vertices) are connected if they satisfy distance
 * criterion
 */
template <typename T>
std::vector<std::vector<std::unique_ptr<Product<T>>>> compute_product_graphs(
    const std::pair<std::vector<T>, std::vector<T>> &similar_vertices,
    const double cutoff_product = 12.0, const double eps_product = 2.0) {

  helper::Benchmark<std::chrono::milliseconds> b;
  // generate locally constrained product graph(s)
  const auto &[vertices1, vertices2] = similar_vertices;
  std::vector<std::vector<std::unique_ptr<Product<T>>>> result;
  for (auto i{0uz}; i < vertices1.size(); ++i) {
    const auto &product_graph_vertices =
        find_product_graph_vertices_near_central_vertex(
            vertices1[i], vertices2[i], similar_vertices, cutoff_product);
    // generate product graph by connecting product graph vertices
    result.push_back(std::move(
        compute_product_graph<T>(product_graph_vertices, eps_product)));
  }
  // some statistics to display
  auto pg_sz{0uz}, max_sz{0uz}, max_edges_sz{0uz};
  for (const auto &product_graph : result) {
    pg_sz += product_graph.size();
    max_sz = std::max(max_sz, product_graph.size());
    auto edges_sz{0uz};
    for (const auto &pvertex : product_graph)
      edges_sz += pvertex->size();
    max_edges_sz = std::max(max_edges_sz, edges_sz / 2);
  }
  std::clog << "Computing product graphs took " << b.duration()
            << " ms and resulted in " << result.size()
            << " product graphs, each with " << pg_sz / result.size()
            << " vertices on average, maximum " << max_sz
            << " vertices, maximum " << max_edges_sz << " edges.\n";

  return result;
}

/**
 * Compute a product graph. Similar vertex pairs that were found are now
 * considered as product graph vertices. Two such vertices are connected by an
 * edge if the difference of the distance between their coordinates,
 * abs(dist1(v1, v2) - dist2(v1', v2')) is less than N Angstroms.
 *
 * @param similar_entities a pair of vectors, the first being vertices from
 * the first graph and the second the corresponding vertices from the second
 * graph
 * @param eps_product if the absolute difference between distance of the pair of
 * vertices in the first graph and the distance between the pair of vertices in
 * the second graph is smaller than EPS, then connect the two product graph
 * vertices
 * @return a product graph in which vertex pairs (compound vertices) are
 * connected if they satisfy the distance criterion
 */
template <typename T>
std::vector<std::unique_ptr<Product<T>>> compute_product_graph(
    const std::pair<std::vector<T>, std::vector<T>> &similar_entities,
    const double eps_product) {
  const auto &[entities1, entities2] = similar_entities;
  assert(entities1.size() == entities2.size());
  // create vertices for a product graph (yes, again)
  std::vector<std::unique_ptr<Product<T>>> result;
  for (auto i{0uz}; i < entities1.size(); ++i) {
    const auto &entity1 = entities1[i];
    const auto &entity2 = entities2[i];
    result.push_back(std::make_unique<Product<T>>(std::pair{entity1, entity2}));
  }
  // create edges for a product graph
  for (auto i{0uz}; i < result.size(); ++i) {
    const auto &pvertex1 = result[i];
    const auto &[entity11, entity12] = pvertex1->similar_entities;
    for (auto j{i + 1}; j < result.size(); ++j) {
      const auto &pvertex2 = result[j];
      const auto &[entity21, entity22] = pvertex2->similar_entities;
      if (helper::approx_equal(entity11.crd().distance(entity21.crd()),
                               entity12.crd().distance(entity22.crd()),
                               eps_product)) {
        pvertex1->add(&*pvertex2);
        pvertex2->add(&*pvertex1);
      }
    }
  }
  return result;
}

} // namespace insilab::probis
