#pragma once

#include "descriptor.hpp"
#include "helper/debug.hpp"
#include "helper/parser_base.hpp"
#include "probes.hpp"
#include "surface.hpp"
#include <set>
#include <string>
#include <tuple>
#include <vector>

namespace insilab::probis {

class Descriptors;
class Probes;

class Parser {
public:
  enum Options { first_surface = 1, all_surfaces = 2 };

private:
  class SrfParser : public helper::ParserBase<Surface> {
  public:
    using ParserBase<Surface>::ParserBase;

    /**
     * Parse a legacy .srf file (old ProBiS format) that contains atoms as well
     * as descriptors and surface probes that define the (multiple) surfaces an
     * atom can belong to.
     *
     * @return a tuple that contains the molecule, descriptors and probes
     * objects
     */
    Surface parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };

  class SrfXParser : public helper::ParserBase<Surface> {
  public:
    using ParserBase<Surface>::ParserBase;

    /**
     * Parse a .srfx file (new scalable ProBiS format) that contains atoms as
     * well as descriptors and surface probes that define the (multiple)
     * surfaces an atom can belong to. The new format enables big molecules with
     * tens of thousands of atoms, different models, insertion codes, alternate
     * locations.
     *
     * @return a tuple that contains the molecule, descriptors and probes
     * objects
     */
    Surface parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };

  std::unique_ptr<helper::ParserBase<Surface>> __p;
  mutable std::mutex __mtx;

public:
  Parser(const std::string &srf_file, Options hm = Options::first_surface,
         const int num_occur = -1);

  /**
   * Thread-safe reading of ProBiS surface file in various formats.
   *
   * @return Surface object which is a tuple of molecule, probes, descriptors.
   */
  Surface parse();
};

} // namespace insilab::probis
