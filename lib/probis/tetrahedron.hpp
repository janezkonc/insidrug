#pragma once

#include "grid/grid.hpp"
#include <array>
#include <map>
#include <set>
#include <vector>

namespace insilab::probis {

class Descriptor;

struct Tetrahedron {
  using Grid = grid::Grid<Tetrahedron>;
  const std::array<const Descriptor *, 4> descriptor;
  const std::array<std::array<double, 4>, 4> distance;
  geom3d::Point crd() const;
};

/**
 * Find all the tetrahedrons made from descriptors in the given protein.  we
 * expect descriptors are already filtered (bb have been filtered out). By
 * setting disregard_visited = true, we get redundant tetrahedrons, i.e., all
 * permutations of the vertices of each tetrahedron
 */
std::vector<Tetrahedron>
compute_tetrahedrons(const std::vector<Descriptor> &desc_v,
                     const double cutoff_min, const double cutoff_max);

std::map<std::pair<unsigned, int>, std::set<const Tetrahedron *>>
binarize_tetrahedrons(const std::vector<Tetrahedron> &tetra_v,
                      const double cutoff_min, const double cutoff_max,
                      const double eps);

/**
 * Do the geometric & physico-chemical comparison of two tetrahedrons.
 */
bool compare_tetrahedrons(const Tetrahedron &lhs, const Tetrahedron &rhs,
                          const double eps);

/**
 * Find similar tetrahedron pairs (first from protein1 and second from
 * protein2). Similarity is established based on MNSPs and sum of all distances
 * and if two tetrahedons are similar this means they can be superimposed in 3D.
 */
std::pair<std::vector<Tetrahedron>, std::vector<Tetrahedron>>
find_similar_tetrahedrons(
    const std::vector<Tetrahedron> &thedrons1,
    const std::map<std::pair<unsigned, int>, std::set<const Tetrahedron *>>
        &thedrons2,
    const double cutoff_min, const double eps);

/**
 * Compute product graph vertices, compound vertices, each being a pair of
 * similar tetrahedrons, the first tetrahedron from the first molecular graph
 * and the second from the second molecular graph. Each tetrahedron is composed
 * of four descriptors.
 *
 * @param desc1_v a vector of descriptors from the first molecule
 * @param desc2_v a vector of descriptors from the second molecule
 * @param cutoff_min minimum length (in Angstroms) of a tetrahedron side
 * @param cutoff_max maximum length (in Angstroms) of a tetrahedron side
 * @param eps when comparing two tetrahedrons, if the difference between all of
 * their corresponding sides is below this threshold (Angstroms), we consider
 * them similar
 * @return a pair of vectors, the first containing tetrahedrons from the first
 * molecule and the second from the second molecule, where the tetrahedrons at
 * i-th position in both vectors are similar
 *
 */
std::pair<std::vector<Tetrahedron>, std::vector<Tetrahedron>>
compute_tetrahedron_product_graph_vertices(
    const std::vector<Descriptor> &desc1_v,
    const std::vector<Descriptor> &desc2_v, const double cutoff_min = 4.0,
    const double cutoff_max = 6.0, const double eps = 0.5);

} // namespace insilab::probis
