#pragma once

#include "probe.hpp"
#include <memory>

namespace insilab::probis {

class Probes {
  std::vector<Probe> __probes;

public:
  const auto &get_probes() const { return __probes; }
  Probe &add_probe(const Probe::Params &p);
  friend std::ostream &operator<<(std::ostream &out, const Probes &p);
};

} // namespace insilab::probis
