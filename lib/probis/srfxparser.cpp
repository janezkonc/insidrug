#include "descriptor.hpp"
#include "details/mnsp.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/molecule.hpp"
#include "parser.hpp"
#include "probes.hpp"
#include <string>
#include <vector>

namespace insilab::probis {

using namespace molib;

std::vector<std::string> Parser::SrfXParser::read_file() {
  return inout::read_file(__file).first;
}

Surface Parser::SrfXParser::parse(const std::vector<std::string> &srf_raw) {

  try {
    int surface_id = -1;
    Molecule molecule({.name = __file});
    std::vector<Descriptor> descriptors;
    Probes probes;
    Atom::PVec atoms;

    for (const std::string &line : srf_raw) {
      if (line.starts_with("A>")) { // reading atoms

        const auto tokens = helper::split(helper::squeeze(line));
        assert(tokens.size() == 15);

        // an atom can belong to multiple surfaces, split and convert to int...
        const auto surfaces = helper::split<int>(
            tokens[14], ",", false,
            [](const std::string &s) -> int { return std::stoi(s); });

        // depending on that check if we want to read all srufaces or only the
        // largest one...
        if ((__hm & all_surfaces) ||
            std::count(surfaces.begin(), surfaces.end(), surface_id)) {

          Atom &a =
              molecule.add(Assembly())
                  .add(Model({.model_id = tokens[10] == "."
                                              ? 1
                                              : std::stoi(tokens[10])}))
                  .add(Chain({.chain_id = tokens[9]}))
                  .add(Segment(
                      {.seg_id = tokens[8] == "."
                                     ? tokens[9]
                                     : tokens[8]})) // use chain_id if unset
                  .add(Residue({
                      .resn = tokens[5],
                      .resi = std::stoi(tokens[6]),
                      .ins_code = tokens[7] == "." ? ' ' : tokens[7].at(0),
                      .rest = algorithms::determine_protein_nucleic(tokens[5]),
                  }))
                  .add(
                      Atom({.atom_number = std::stoi(tokens[1]),
                            .atom_name = tokens[2],
                            .crd = geom3d::Point(std::stod(tokens[11]),
                                                 std::stod(tokens[12]),
                                                 std::stod(tokens[13])),
                            .alt_loc = tokens[3] == "." ? ' ' : tokens[3].at(0),
                            .element = tokens[4] == "." ? "" : tokens[4]}));
        }
      } else if (line.starts_with("D>")) { // reading descriptors

        const auto tokens = helper::split(helper::squeeze(line));
        assert(tokens.size() == 7);

        descriptors.emplace_back(Descriptor::Params{
            .id = std::stoul(tokens[1]),
            .crd = geom3d::Point(stod(tokens[2]), std::stod(tokens[3]),
                                 std::stod(tokens[4])),
            .mnsp = details::to_mnsp(tokens[5]),
            .atom_number = std::stoi(tokens[6]) // this is atom_number
        });
      } else if (line.starts_with("P>")) { // reading probes

        const auto tokens = helper::split(helper::squeeze(line));
        assert(tokens.size() == 6);

        probes.add_probe({
            .id = std::stoi(tokens[1]), // probe id
            .crd = geom3d::Point(stod(tokens[2]), std::stod(tokens[3]),
                                 std::stod(tokens[4])),
            .surf_id = std::stoi(tokens[5]) // surface color
        });

      } else if (line.starts_with("MOL>")) {
        surface_id = std::stoi(line.substr(4));
      }
    }
    return {molecule, descriptors, probes};
  } catch (const std::exception &e) {
    throw helper::Error(
        "[NOTE] An error occured during reading of srfx file (" + get_file() +
        "). Errmsg: " + helper::to_string(e.what()));
  }
}

} // namespace insilab::probis
