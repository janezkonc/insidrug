#pragma once

#include "molib/molecule.hpp"

namespace insilab::probis {

// using namespace molib;

class Descriptor;
class Probes;

class Surface {
public:
  typedef class Parser Parser;

private:
  const molib::Molecule __molecule;
  const std::vector<Descriptor> __descriptors;
  const Probes __probes;

public:
  Surface(const molib::Molecule &molecule,
          const std::vector<Descriptor> &descriptors, const Probes &probes)
      : __molecule(molecule), __descriptors(descriptors), __probes(probes) {}

  const auto &molecule() { return __molecule; }
  const auto &descriptors() { return __descriptors; }
  const auto &probes() { return __probes; }
};

} // namespace insilab::probis
