#include "descriptor.hpp"
#include "details/mnsp.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/element_type.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "parser.hpp"
#include "probes.hpp"
#include <string>
#include <vector>

namespace insilab::probis {

using namespace molib;

std::vector<std::string> Parser::SrfParser::read_file() {
  return inout::read_file(__file).first;
}

Surface Parser::SrfParser::parse(const std::vector<std::string> &srf_raw) {

  try {
    int surface_id = -1;
    Molecule molecule({.name = __file});
    std::vector<Descriptor> descriptors;
    Probes probes;
    Atom::PVec atoms;

    for (const std::string &line : srf_raw) {
      if (line.starts_with("A>")) { // reading atoms

        // an atom can belong to multiple surfaces, split and convert to int...
        const auto surfaces = helper::split<int>(
            helper::squeeze(line.substr(58)), " ", false,
            [](const std::string &s) -> int { return std::stoi(s); });

        // depending on that check if we want to read all srufaces or only the
        // largest one...
        if ((__hm & all_surfaces) ||
            std::count(surfaces.begin(), surfaces.end(), surface_id)) {
          const auto atom_name =
              helper::trim(line.substr(39, 6));
          const auto rest =
              algorithms::determine_protein_nucleic(line.substr(45, 3));
          const auto chain_id = helper::trim(line.substr(53, 2));

          Atom &a =
              molecule.add(Assembly())
                  .add(Model())
                  .add(Chain({.chain_id = chain_id}))
                  .add(Segment({.seg_id = chain_id})) // use chain_id if unset
                  .add(Residue({
                      .resn = line.substr(45, 3),
                      .resi = std::stoi(line.substr(48, 5)),
                      .rest = rest,
                  }))
                  .add(
                      Atom({.atom_number = std::stoi(line.substr(2, 5)),
                            .atom_name = atom_name,
                            .crd = geom3d::Point(std::stod(line.substr(12, 8)),
                                                 std::stod(line.substr(20, 8)),
                                                 std::stod(line.substr(28, 8))),
                            .element = algorithms::determine_element(atom_name,
                                                                     rest)}));
        }
      } else if (line.starts_with("D>")) { // reading descriptors

        descriptors.emplace_back(Descriptor::Params{
            .id = std::stoul(line.substr(2, 5)),
            .crd = geom3d::Point(std::stod(line.substr(12, 8)),
                                 std::stod(line.substr(20, 8)),
                                 std::stod(line.substr(28, 8))),
            .mnsp = details::to_mnsp_legacy(std::stoi(line.substr(36, 3))),
            .atom_number = std::stoi(line.substr(
                42, 5)) // in legacy format, this is the position of atom in
                        // receptor.get_atoms(), not atom_number
        });
      } else if (line.starts_with("P>")) { // reading probes

        probes.add_probe({
            .id = std::stoi(line.substr(2, 5)), // probe id
            .crd = geom3d::Point(std::stod(line.substr(12, 8)),
                                 std::stod(line.substr(20, 8)),
                                 std::stod(line.substr(28, 8))),
            .surf_id = std::stoi(line.substr(36, 5)) // surface color
        });

      } else if (line.starts_with("MOL>")) {
        surface_id = std::stoi(line.substr(4));
      }
    }
    return {molecule, descriptors, probes};
  } catch (const std::exception &e) {
    throw helper::Error("[NOTE] An error occured during reading of srf file (" +
                        get_file() +
                        "). Errmsg: " + helper::to_string(e.what()));
  }
}

} // namespace insilab::probis
