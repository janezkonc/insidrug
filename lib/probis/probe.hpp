#pragma once

#include "geom3d/linear.hpp"
#include "grid/grid.hpp"

namespace insilab::probis {

class Probe {
public:
  typedef grid::Grid<Probe> Grid;
  struct Params {
    int id;
    geom3d::Point crd;
    int surf_id;
  };

private:
  Params __p;

public:
  Probe(const Params &p) : __p(p) {}

  auto id() const { return __p.id; }
  auto crd() const { return __p.crd; }
  auto surf_id() const { return __p.surf_id; }
};

} // namespace insilab::probis
