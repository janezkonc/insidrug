#include "tetrahedron.hpp"
#include "descriptor.hpp"
#include "details/mnsp.hpp"
#include "helper/benchmark.hpp"
#include <vector>

namespace insilab::probis {

geom3d::Point Tetrahedron::crd() const {
  return (descriptor[0]->crd() + descriptor[1]->crd() + descriptor[2]->crd() +
          descriptor[3]->crd()) /
         4;
}

std::vector<Tetrahedron>
compute_tetrahedrons(const std::vector<Descriptor> &desc_v,
                     const double cutoff_min, const double cutoff_max) {

  helper::Benchmark<std::chrono::milliseconds> b;

  const auto grid = Descriptor::Grid(desc_v);

  std::vector<Tetrahedron> result;
  for (const auto &d1 : desc_v) {
    const auto &neighbors = grid.get_neighbors(d1.crd(), cutoff_max);
    for (const auto &d2 : neighbors) {
      const auto distance12 = d1.crd().distance(d2->crd());
      if (d1.id() >= d2->id() || distance12 < cutoff_min)
        continue;
      for (const auto &d3 : neighbors) {
        const auto distance13 = d1.crd().distance(d3->crd());
        const auto distance23 = d2->crd().distance(d3->crd());
        if (d2->id() >= d3->id() || distance13 < cutoff_min ||
            distance23 < cutoff_min || distance23 >= cutoff_max)
          continue;
        for (const auto &d4 : neighbors) {
          const auto distance14 = d1.crd().distance(d4->crd());
          const auto distance24 = d2->crd().distance(d4->crd());
          const auto distance34 = d3->crd().distance(d4->crd());
          if (d3->id() >= d4->id() || distance14 < cutoff_min ||
              distance24 < cutoff_min || distance24 >= cutoff_max ||
              distance34 < cutoff_min || distance34 >= cutoff_max)
            continue;
          std::array<std::array<double, 4>, 4> distance;
          distance[0][1] = distance[1][0] = distance12;
          distance[0][2] = distance[2][0] = distance13;
          distance[0][3] = distance[3][0] = distance14;
          distance[1][2] = distance[2][1] = distance23;
          distance[2][3] = distance[3][2] = distance34;
          distance[1][3] = distance[3][1] = distance24;

          result.emplace_back(std::array{&d1,
                                         static_cast<const Descriptor *>(d2),
                                         static_cast<const Descriptor *>(d3),
                                         static_cast<const Descriptor *>(d4)},
                              distance);
        }
      }
    }
  }

  std::clog << "Computing tetrahedrons from descriptors took " << b.duration()
            << " ms and resulted in " << result.size() << " tetrahedrons.\n";

  return result;
}

std::map<std::pair<unsigned, int>, std::set<const Tetrahedron *>>
binarize_tetrahedrons(const std::vector<Tetrahedron> &tetra_v,
                      const double cutoff_min, const double cutoff_max,
                      const double eps) {

  helper::Benchmark<std::chrono::milliseconds> b;

  std::map<std::pair<unsigned, int>, std::set<const Tetrahedron *>> result;
  const double cutoff_min_sum = 6 * cutoff_min;
  const double cutoff_max_sum = 6 * cutoff_max;
  const double eps_sum = 6 * eps;
  const int max_idx = std::round((cutoff_max_sum - cutoff_min_sum) / eps_sum);

  for (const auto &t : tetra_v) {
    const auto &d1 = *t.descriptor[0];
    const auto &d2 = *t.descriptor[1];
    const auto &d3 = *t.descriptor[2];
    const auto &d4 = *t.descriptor[3];

    const auto dist12 = t.distance[0][1];
    const auto dist13 = t.distance[0][2];
    const auto dist14 = t.distance[0][3];
    const auto dist23 = t.distance[1][2];
    const auto dist34 = t.distance[2][3];
    const auto dist42 = t.distance[1][3];

    const auto d = dist12 + dist13 + dist14 + dist23 + dist34 + dist42;

    const auto hsh_mnsp =
        details::compute_mnsp_hash(d1.mnsp(), d2.mnsp(), d3.mnsp(), d4.mnsp());

    const int idx = std::round((d - cutoff_min_sum) / eps_sum);
    const int idx_low = idx == 0 ? 0 : idx - 1;
    const int idx_high = idx == max_idx ? max_idx : idx + 1;

    result[{hsh_mnsp, idx}].insert(&t);
    result[{hsh_mnsp, idx_low}].insert(&t);
    result[{hsh_mnsp, idx_high}].insert(&t);
  }
  auto bin_sz{0uz};
  for (const auto &[hsh, bin] : result) {
    bin_sz += bin.size();
  }
  std::clog << "Binarize tetrahedrons took " << b.duration()
            << " ms and resulted in " << result.size() << " bins, each with "
            << bin_sz / result.size() << " elements on average.\n";
  return result;
}

bool compare_tetrahedrons(const Tetrahedron &lhs, const Tetrahedron &rhs,
                          const double eps) {
  static const auto &permutations = std::vector{std::vector{0, 1, 2, 3},
                                                {0, 1, 3, 2},
                                                {0, 2, 1, 3},
                                                {0, 2, 3, 1},
                                                {0, 3, 1, 2},
                                                {0, 3, 2, 1},
                                                {1, 0, 2, 3},
                                                {1, 0, 3, 2},
                                                {1, 2, 0, 3},
                                                {1, 2, 3, 0},
                                                {1, 3, 0, 2},
                                                {1, 3, 2, 0},
                                                {2, 0, 1, 3},
                                                {2, 0, 3, 1},
                                                {2, 1, 0, 3},
                                                {2, 1, 3, 0},
                                                {2, 3, 0, 1},
                                                {2, 3, 1, 0},
                                                {3, 0, 1, 2},
                                                {3, 0, 2, 1},
                                                {3, 1, 0, 2},
                                                {3, 1, 2, 0},
                                                {3, 2, 0, 1},
                                                {3, 2, 1, 0}};

  const auto compare = [&eps](const Tetrahedron &lhs, const Tetrahedron &rhs,
                              const std::vector<int> &p) {
    const auto &d1 = *lhs.descriptor[p[0]];
    const auto &d2 = *lhs.descriptor[p[1]];
    const auto &d3 = *lhs.descriptor[p[2]];
    const auto &d4 = *lhs.descriptor[p[3]];

    const auto dist12 = lhs.distance[p[0]][p[1]];
    const auto dist13 = lhs.distance[p[0]][p[2]];
    const auto dist14 = lhs.distance[p[0]][p[3]];
    const auto dist23 = lhs.distance[p[1]][p[2]];
    const auto dist34 = lhs.distance[p[2]][p[3]];
    const auto dist42 = lhs.distance[p[1]][p[3]];

    const auto &e1 = *rhs.descriptor[0];
    const auto &e2 = *rhs.descriptor[1];
    const auto &e3 = *rhs.descriptor[2];
    const auto &e4 = *rhs.descriptor[3];

    const auto edist12 = rhs.distance[0][1];
    const auto edist13 = rhs.distance[0][2];
    const auto edist14 = rhs.distance[0][3];
    const auto edist23 = rhs.distance[1][2];
    const auto edist34 = rhs.distance[2][3];
    const auto edist42 = rhs.distance[1][3];

    return d1 == e1 && d2 == e2 && d3 == e3 && d4 == e4 &&
           helper::approx_equal(dist12, edist12, eps) &&
           helper::approx_equal(dist13, edist13, eps) &&
           helper::approx_equal(dist14, edist14, eps) &&
           helper::approx_equal(dist23, edist23, eps) &&
           helper::approx_equal(dist34, edist34, eps) &&
           helper::approx_equal(dist42, edist42, eps);
  };

  for (const auto &p : permutations) {
    if (compare(lhs, rhs, p))
      return true;
  }
  return false;
}

std::pair<std::vector<Tetrahedron>, std::vector<Tetrahedron>>
find_similar_tetrahedrons(
    const std::vector<Tetrahedron> &thedrons1,
    const std::map<std::pair<unsigned, int>, std::set<const Tetrahedron *>>
        &thedrons2,
    const double cutoff_min, const double eps) {

  helper::Benchmark<std::chrono::milliseconds> b;

  const double cutoff_min_sum = 6 * cutoff_min;
  const double eps_sum = 6 * eps;

  std::pair<std::vector<Tetrahedron>, std::vector<Tetrahedron>> result;
  for (const auto &th1 : thedrons1) {

    const auto &d1 = *th1.descriptor[0];
    const auto &d2 = *th1.descriptor[1];
    const auto &d3 = *th1.descriptor[2];
    const auto &d4 = *th1.descriptor[3];

    const auto dist12 = th1.distance[0][1];
    const auto dist13 = th1.distance[0][2];
    const auto dist14 = th1.distance[0][3];
    const auto dist23 = th1.distance[1][2];
    const auto dist34 = th1.distance[2][3];
    const auto dist42 = th1.distance[1][3];

    const auto d = dist12 + dist13 + dist14 + dist23 + dist34 + dist42;

    const auto hsh_mnsp =
        details::compute_mnsp_hash(d1.mnsp(), d2.mnsp(), d3.mnsp(), d4.mnsp());

    const int idx = std::round((d - cutoff_min_sum) / eps_sum);

    const auto tetra_hash1 = std::pair{hsh_mnsp, idx};

    if (!thedrons2.contains(tetra_hash1))
      continue;
    for (const auto &pth2 : thedrons2.at(tetra_hash1)) {
      // tetrahedrons in th1 and th2 all have the same hash code, so they
      // deserve closer inspection
      if (compare_tetrahedrons(th1, *pth2, eps)) {
        result.first.push_back(th1);
        result.second.push_back(*pth2);
      }
    }
  }
  std::clog << "Finding similar tetrahedrons took " << b.duration()
            << " ms and resulted in " << result.first.size()
            << " similar tetrahedrons found.\n";

  return result;
}

std::pair<std::vector<Tetrahedron>, std::vector<Tetrahedron>>
compute_tetrahedron_product_graph_vertices(const std::vector<Descriptor> &desc1_v,
                               const std::vector<Descriptor> &desc2_v,
                               const double cutoff_min, const double cutoff_max,
                               const double eps) {

  helper::Benchmark<std::chrono::milliseconds> b;

  const auto &thedrons1 =
      probis::compute_tetrahedrons(desc1_v, cutoff_min, cutoff_max);
  const auto &thedrons2 =
      probis::compute_tetrahedrons(desc2_v, cutoff_min, cutoff_max);

  const auto &tetrahedron2_bins =
      probis::binarize_tetrahedrons(thedrons2, cutoff_min, cutoff_max, eps);

  const auto &result = probis::find_similar_tetrahedrons(
      thedrons1, tetrahedron2_bins, cutoff_min, eps);

  std::clog << "Computing product graph vertices took " << b.duration()
            << " ms and resulted in " << result.first.size()
            << " similar tetrahedrons found.\n";
  return result;
}

} // namespace insilab::probis
