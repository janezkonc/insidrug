#pragma once

#include "details/rules.hpp"
#include "geom3d/coordinate.hpp"
#include "grid/grid.hpp"
#include <array>
#include <memory>

namespace insilab::molib {
class Molecule;
} // namespace insilab::molib

namespace insilab::probis::details {
enum class mnsp_t;
}

namespace insilab::probis {

class Descriptor {
public:
  using Grid = grid::Grid<Descriptor>;
  struct Params {
    std::size_t id;
    geom3d::Point crd;
    details::mnsp_t mnsp;
    int atom_number;
  };

private:
  Params __p;

public:
  Descriptor(const Params &p) : __p(p) {}
  auto id() const { return __p.id; }
  auto crd() const { return __p.crd; }
  auto mnsp() const { return __p.mnsp; }
  auto atom_number() const { return __p.atom_number; }

  bool operator==(const Descriptor &other) const {
    return this->__p.mnsp == other.__p.mnsp;
  }

  friend std::ostream &operator<<(std::ostream &out, const Descriptor &desc);
  friend std::ostream &operator<<(std::ostream &out,
                                  const std::vector<Descriptor> &descs);
  friend std::ostream &
  operator<<(std::ostream &out,
             const std::vector<std::vector<Descriptor>> &descs_all_surfaces);
};

struct desc_comparator {
  bool operator()(const auto &lhs, const auto &rhs) const {
    return lhs.first.id() < rhs.first.id() ||
           (lhs.first.id() == rhs.first.id() &&
            lhs.second.id() < rhs.second.id());
  }
};

/**
 * Calculate descriptors that represent functional groups of molecules, such as
 * proteins or nucleic acids. They are points in space each assigned a
 * physical-chemical property of the underlying functional group that needs to
 * be close to the solvent accessible surface of the molecule.
 *
 * @note don't return pointers to atoms in e.g. descriptors, because these will
 * be pointing to a copy and thus will be stale!
 *
 * @param molecule input molecule: a protein, DNA, RNA, etc.
 * @param functional_groups rename rules that define groups of connected atoms
 * that have a common physico-chemical character, which are to be replaced by
 * descriptors
 * @return a vector holding vectors, one for each surface (sorted from largest
 * to smallest surface), of descriptors, where each Descriptor object has a
 * coordinate, physical-chemical property (mnsp), and atom_number of the first
 * (sometimes the only) atom on which this descriptor is based
 */
std::vector<std::vector<Descriptor>> generate_descriptors(
    const molib::Molecule &molecule,
    const helper::rename_rules &functional_groups = details::descriptor_rules);

/**
 * A refinement of the above procedure, descriptor product graph vertices are
 * compound vertices, each being a pair of similar descriptors. It is intended
 * that each descriptor in the pair is from the same similar
 * tetrahedron-tetrahedron pair. The intention is shown by use of array of 4 as
 * arguments, but this is not required and could be changed to i.e., vector
 * later.
 *
 * @param desc1_v an array of pointers to descriptors from the first molecule
 * @param desc2_v an array of pointers to descriptors from the second molecule
 * @return a pair of vectors, the first containing pointers to descriptors from
 * the first molecule and the second from the second molecule, where the
 * descriptors at i-th position in both vectors are similar
 *
 */
std::pair<std::vector<Descriptor>, std::vector<Descriptor>>
compute_descriptor_product_graph_vertices(
    const std::array<const Descriptor *, 4> &desc1_v,
    const std::array<const Descriptor *, 4> &desc2_v);

} // namespace insilab::probis
