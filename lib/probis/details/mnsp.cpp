#include "mnsp.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"

namespace insilab::probis::details {

unsigned to_hash(const details::mnsp_t mnsp) {
  switch (mnsp) {
  case details::mnsp_t::aliphatic:
    return 8u;
  case details::mnsp_t::pi:
    return 1u;
  case details::mnsp_t::acceptor:
    return 4096u;
  case details::mnsp_t::donor:
    return 512u;
  case details::mnsp_t::acceptor | details::mnsp_t::donor:
    return 64u;
  }
  throw helper::Error("[WHOOPS] Unknown mnsp!");
}

mnsp_t to_mnsp_legacy(const int &mnsp) {
  if (mnsp == 1)
    return mnsp_t::aliphatic;
  else if (mnsp == 2)
    return mnsp_t::pi;
  else if (mnsp == 4)
    return mnsp_t::acceptor;
  else if (mnsp == 8)
    return mnsp_t::donor;
  else if (mnsp == 12)
    return mnsp_t::acceptor | mnsp_t::donor;
  throw helper::Error("[WHOOPS] Cannot convert legacy int to descriptor: [" +
                      helper::to_string(mnsp) + "] !");
}

mnsp_t to_mnsp(const std::string &mnsp) {
  if (mnsp == "acceptor")
    return mnsp_t::acceptor;
  else if (mnsp == "donor")
    return mnsp_t::donor;
  else if (mnsp == "acceptor_donor")
    return mnsp_t::acceptor | mnsp_t::donor;
  else if (mnsp == "aliphatic")
    return mnsp_t::aliphatic;
  else if (mnsp == "pi")
    return mnsp_t::pi;
  throw helper::Error("[WHOOPS] Cannot convert string to descriptor: [" + mnsp +
                      "] !");
}

std::string from_mnsp(const mnsp_t &mnsp) {
  constexpr auto acdo = mnsp_t::acceptor | mnsp_t::donor;
  switch (mnsp) {
  case mnsp_t::acceptor:
    return "acceptor";
  case mnsp_t::donor:
    return "donor";
  case acdo:
    return "acceptor_donor";
  case mnsp_t::aliphatic:
    return "aliphatic";
  case mnsp_t::pi:
    return "pi";
  }
  throw helper::Error("[WHOOPS] Cannot convert descriptor to string!");
}

std::string from_mnsp_to_atom_name(const mnsp_t &mnsp) {
  constexpr auto acdo = mnsp_t::acceptor | mnsp_t::donor;
  switch (mnsp) {
  case mnsp_t::acceptor:
    return "C";
  case mnsp_t::donor:
    return "N";
  case acdo:
    return "O";
  case mnsp_t::aliphatic:
    return "S";
  case mnsp_t::pi:
    return "P";
  }
  throw helper::Error("[WHOOPS] Cannot convert descriptor to atom name!");
}

std::ostream &operator<<(std::ostream &stream, const mnsp_t &m) {
  stream << from_mnsp(m);
  return stream;
}

} // namespace insilab::probis::details
