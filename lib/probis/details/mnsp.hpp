#pragma once

#include <string>

namespace insilab::probis::details {

// Physical-chemical properties for amino acid functional groups
enum class mnsp_t {
  aliphatic = 1,
  pi = 2,
  acceptor = 4,
  donor = 8,
};

constexpr mnsp_t operator|(const mnsp_t &lhs, const mnsp_t &rhs) {
  return static_cast<mnsp_t>(static_cast<std::underlying_type_t<mnsp_t>>(lhs) |
                             static_cast<std::underlying_type_t<mnsp_t>>(rhs));
}

constexpr mnsp_t &operator|=(mnsp_t &lhs, const mnsp_t &rhs) {
  lhs = lhs | rhs;
  return lhs;
}

constexpr bool operator&(const mnsp_t &lhs, const mnsp_t &rhs) {
  return (static_cast<std::underlying_type_t<mnsp_t>>(lhs) &
          static_cast<std::underlying_type_t<mnsp_t>>(rhs)) ==
         static_cast<std::underlying_type_t<mnsp_t>>(rhs);
}

/// Return a hashing number, so that if you have e.g. 4 mnsp descriptors, and
/// you sum up the four hashes, each such sum is unique.
unsigned to_hash(const details::mnsp_t mnsp);

/// Compute a hash code from a list of mnsps given as parameters of this
/// function. The hash code is a sum of their individual hashes.
template <typename... Args> auto compute_mnsp_hash(Args &&...args) {
  return (details::to_hash(args) + ...);
}

/// Convert legacy (ProBiS's v1) int => enum mnsp_t
mnsp_t to_mnsp_legacy(const int &mnsp);

/// Convert string => mnsp_t
mnsp_t to_mnsp(const std::string &mnsp);

/// Convert mnsp_t => string
std::string from_mnsp(const mnsp_t &mnsp);

/// Convert mnsp_t => atom_name (string) (just for testing)
std::string from_mnsp_to_atom_name(const mnsp_t &mnsp);

/// Write mnsp_t to output stream, e.g., acceptor_donor
std::ostream &operator<<(std::ostream &stream, const mnsp_t &m);

} // namespace insilab::probis::details