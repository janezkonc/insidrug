#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <fstream>
#include <helper/benchmark.hpp>
#include <helper/debug.hpp>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::cluster {

/**
 * Calculate density-based clusters for a set of vertices with defined pairwise
 * distances using OPTICS algorithm.
 *
 * @param pairwise_distances a map, in which a key is a pair of vertex indices
 * (i,j) starting with zero and a value is distance (d) between the two vertices
 * (needed: (i,j) -> d; NOT needed: (j,i) -> d); distances must be comparable
 * (operator<); can be a sparse map, i.e., not all distances need to be
 * defined
 * @param scores a vector, in which each value at i-th position is a score of a
 * vertex with index i; each vertex must have a score; scores must be comparable
 * (operator<)
 * @param eps the maximum distance (radius) to consider, i.e., kind of cluster
 * radius
 * @param eps_extract the maximum distance (radius) to consider, i.e., kind of
 * cluster radius during extraction of clusters (eps_extract must be less than
 * eps)
 * @param min_pts the number of points required to form a cluster
 * @param max_num_clus maximum number of clusteres to report
 * @param do_sort sort clusters according to scores so that those with highest
 * scores will be first
 * @return a vector, in which each value is a vector of indices representing one
 * cluster of vertices; each first vertex (at position 0) in each inner vector
 * has the highest score within this cluster, i.e., is a representative
 */
std::vector<std::vector<std::size_t>> compute_density_based_clusters(
    const std::map<std::pair<std::size_t, std::size_t>, double>
        &pairwise_distances,
    const std::vector<double> &scores, const double eps,
    const double eps_extract, const std::size_t min_pts,
    const std::size_t max_num_clus = std::numeric_limits<std::size_t>::max(),
    const bool do_sort = false);

} // namespace insilab::cluster
