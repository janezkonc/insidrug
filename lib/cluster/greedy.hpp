#pragma once

#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"

namespace insilab::cluster {

using namespace grid;

// default for no rmsd and no score (e.g., comparing points representatives)
template <typename T> struct GenericCompareRMSDLessThanConstant {
  constexpr bool operator()(const T *, const T *, const double) const {
    return true;
  }
};

template <typename T> struct GenericCompareScoreLess {
  constexpr bool operator()(const T *a, const T *b) const { return a < b; }
};

template <typename T,
          typename CompareRMSDLessThanConstant =
              GenericCompareRMSDLessThanConstant<T>,
          typename CompareScoreLess = GenericCompareScoreLess<T>>
class GreedyCluster {

public:
  class Wrap {
    T *__vtx;
    bool __erased;

  public:
    Wrap(T *vtx) : __vtx(vtx), __erased(false) {}
    bool get_erased() const { return __erased; }
    void set_erased() { __erased = true; }
    T *operator()() { return __vtx; }
    geom3d::Point &crd() { return __vtx->crd(); }
    const geom3d::Point &crd() const { return __vtx->crd(); }
  };

  std::vector<T *>
  compute_uniform_representatives(const std::vector<T *> &initial,
                                  const double clus_rad) const {

    helper::Benchmark<std::chrono::milliseconds> b;
    std::clog << "Starting Greedy clustering ..." << std::endl;

    std::set<T *, CompareScoreLess> confs(initial.cbegin(), initial.cend());
    std::vector<Wrap> wrapped(confs.begin(), confs.end());

    dbgmsg("number of conformations to cluster = " << confs.size());

    Grid<const Wrap> grid(wrapped); // grid of docked conformations
    std::vector<T *> reps;

    for (auto &w : wrapped) {
      auto pconf = w();
      if (!w.get_erased()) {
        // accept lowest energy conformation as representative
        reps.push_back(pconf);
        const T *lowest_point = reps.back();
        w.set_erased();

        // delete all conformations within RMSD tolerance of this lowest energy
        // conformation
        for (auto &cwneighb :
             grid.get_neighbors(lowest_point->crd(), clus_rad)) {
          auto wneighb = const_cast<Wrap *>(cwneighb);
          if (!wneighb->get_erased() &&
              CompareRMSDLessThanConstant()((*wneighb)(), lowest_point,
                                            clus_rad)) {
            wneighb->set_erased();
          }
        }
      }
    }

    std::clog << "Clustering " << initial.size()
              << " accepted conformations resulted in " << reps.size()
              << " cluster representatives and took " << b.duration() << " ms"
              << std::endl;

    return reps;
  }
};

} // namespace insilab::cluster
