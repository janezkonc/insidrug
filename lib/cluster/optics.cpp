#include "optics.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <fstream>
#include <helper/benchmark.hpp>
#include <helper/debug.hpp>
#include <helper/error.hpp>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::cluster {

struct OpticsInternals {
  // constants defined during construction
  const std::map<std::pair<std::size_t, std::size_t>, double>
      &pairwise_distances;
  const std::vector<double> &scores;
  const double eps;
  const double eps_extract;
  const std::size_t min_pts;
  const std::size_t max_num_clus;
  const bool do_sort;
  // other member variables
  std::size_t num_vertices{0};
  std::vector<double> reachability_distances;
  std::vector<double> core_distances;
  std::vector<bool> processed;
  std::vector<long int> cluster_ids;
  std::vector<std::vector<std::size_t>> neighbors;
  std::vector<std::size_t> ordered_file;

  double get_distance(const std::size_t i, const std::size_t j) {
    if (const auto it = pairwise_distances.find({i, j});
        it != pairwise_distances.end())
      return it->second;
    if (const auto it = pairwise_distances.find({j, i});
        it != pairwise_distances.end()) {
      return it->second;
    }
    return std::numeric_limits<double>::max();
  }

  void set_core_distances(const auto i) {
    if (neighbors[i].size() < min_pts) {
      core_distances[i] = std::numeric_limits<double>::max();
    } else {
      core_distances[i] = get_distance(i, neighbors[i][min_pts - 1]);
    }
  }

  void expand_cluster_order(const auto i) {
    processed[i] = true;
    reachability_distances[i] =
        std::numeric_limits<double>::max(); // reachability distance is already
                                            // largest value
    set_core_distances(i);
    ordered_file.push_back(i);
    // std::priority_queue sorted by reachability_distances from the core object
    // (last element has the smallest reachability_distances)
    std::vector<std::size_t> order_seeds;
    if (core_distances[i] !=
        std::numeric_limits<double>::max()) { // is i a core object?
      update(order_seeds, i);
      while (!order_seeds.empty()) {
        std::pop_heap(order_seeds.begin(), order_seeds.end(),
                      [this](const auto &k, const auto &l) {
                        return !(reachability_distances[k] <
                                 reachability_distances[l]);
                      });
        const auto &j = order_seeds.back(); // top of heap
        order_seeds.pop_back();             // pop from heap
        processed[j] = true;
        set_core_distances(j);
        ordered_file.push_back(j);
        if (core_distances[j] !=
            std::numeric_limits<double>::max()) { // is j a core object?
          // if yes, insert further candidates into the order_seeds
          update(order_seeds, j);
        }
      }
    }
  }

  void update(std::vector<std::size_t> &order_seeds, const auto i) {
    // i is center object
    const double c_dist = core_distances[i];
    for (const auto j : neighbors[i]) {
      if (processed[j])
        continue;
      double new_r_dist = std::max(c_dist, get_distance(i, j));
      if (reachability_distances[j] == std::numeric_limits<double>::max()) {
        reachability_distances[j] = new_r_dist;
        order_seeds.push_back(j);
        std::push_heap(order_seeds.begin(), order_seeds.end(),
                       [this](const auto &k, const auto &l) {
                         return !(reachability_distances[k] <
                                  reachability_distances[l]);
                       });
      } else { // j already in order_seeds
        if (new_r_dist < reachability_distances[j]) {
          reachability_distances[j] =
              new_r_dist; // j is pointed to from the order_seeds
          std::make_heap(order_seeds.begin(), order_seeds.end(),
                         [this](const auto &k, const auto &l) {
                           return !(reachability_distances[k] <
                                    reachability_distances[l]);
                         });
        }
      }
    }
  }

  auto extract_dbscan() {
    helper::Benchmark<std::chrono::milliseconds> b;

    std::vector<std::vector<std::size_t>> result(
        num_vertices); // clustered points
    auto cluster_id = 0l;

    for (const auto &i : ordered_file) {
      if (!(reachability_distances[i] < eps_extract)) {
        if (core_distances[i] < eps_extract) {
          cluster_ids[i] = ++cluster_id;
        } else {
          cluster_ids[i] = -1; // noise
        }
      } else {
        cluster_ids[i] = cluster_id;
      }
    }

    // assign all yet unclustered points to clusters
    for (const auto &i : ordered_file) {
      if (cluster_ids[i] != -1)
        continue;
      // pass 1 : try to assign a noise point to a nearest cluster
      std::vector<std::size_t> clustered_neighbors;
      for (const auto &j : neighbors[i])
        if (cluster_ids[j] != -1)
          clustered_neighbors.push_back(j);

      if (!clustered_neighbors.empty()) {
        const auto nearest = std::min_element(
            clustered_neighbors.cbegin(), clustered_neighbors.cend(),
            [this, &i](const auto &k, const auto &l) {
              return get_distance(i, k) < get_distance(i, l);
            });
        if (get_distance(i, *nearest) < eps_extract) {
          cluster_ids[i] = cluster_ids[*nearest];
          dbgmsg("pass 1 : assigned point "
                 << i << " to cluster " << cluster_ids[i]
                 << " because it was close (distance="
                 << get_distance(i, *nearest) << ") to point " << *nearest);
          continue;
        }
      }
      // pass 2 : find points that are close and assign them to a new cluster
      std::vector<std::size_t> close_neighbors;
      close_neighbors.push_back(i);
      ++cluster_id;
      for (const auto &j : neighbors[i])
        if (get_distance(i, j) < eps_extract)
          close_neighbors.push_back(j);
      for (const auto &n : close_neighbors) {
        cluster_ids[n] = cluster_id;
        dbgmsg("pass 2 : assigned point " << n << " to a new cluster "
                                          << cluster_ids[n]);
      }
    }
    if (do_sort) {
      // sort according to scores
      std::sort(ordered_file.begin(), ordered_file.end(),
                [this](const auto &i, const auto &j) {
                  return scores[j] < scores[i];
                });
    }
    // renumber clusters so that the best cluster is first
    std::map<std::size_t, std::size_t> added_clus;
    for (auto recluster_id = 0uz; const auto &i : ordered_file) {
      const auto cluster_id = cluster_ids[i];
      if (added_clus.size() < max_num_clus || added_clus.contains(cluster_id)) {
        if (!added_clus.contains(cluster_id))
          added_clus.insert({cluster_id, recluster_id++});
        result[added_clus[cluster_id]].push_back(i);
      }
    }
    // erase empty clusters (from the end)
    std::erase_if(result, [](const auto &v) { return v.empty(); });
    // make cluster representatives (vertex with highest score) the first vertex
    // in each cluster vector
    for (auto &cluster : result) {
      std::sort(cluster.begin(), cluster.end(),
                [this](const auto &i, const auto &j) {
                  return scores[j] < scores[i];
                });
    }
    std::clog << "Extract DBSCAN took " << b.duration() << "ms" << std::endl;
    return result;
  }

  void cluster() {

    helper::Benchmark<std::chrono::milliseconds> b;
    std::clog << "Starting density-based clustering ..." << std::endl;

    // determine number of vertices from size of score since each vertex must
    // have its own score (pairwise_distances cannot be used, because some
    // keys (pairs of vertex indexes) may be missing)
    num_vertices = scores.size();
    reachability_distances.resize(num_vertices);
    core_distances.resize(num_vertices);
    processed.resize(num_vertices);
    cluster_ids.resize(num_vertices);
    neighbors.resize(num_vertices);

    std::vector<std::set<std::size_t>> temp_neighbors(num_vertices);
    for (const auto &[key, distance] : pairwise_distances) {
      const auto &[i, j] = key;
      if (!(i < temp_neighbors.size() && j < temp_neighbors.size()))
        throw helper::Error("[WHOOPS] There are more vertices than scores, "
                            "check your input!");
      reachability_distances[i] = core_distances[i] =
          reachability_distances[j] = core_distances[j] =
              std::numeric_limits<double>::max();
      cluster_ids[i] = cluster_ids[j] = 0;
      processed[i] = processed[j] = false;
      if (distance < eps) {
        temp_neighbors[i].insert(j);
        temp_neighbors[j].insert(i);
      }
    }
    for (auto i = 0uz; const auto &s : temp_neighbors) {
      neighbors[i].assign(std::cbegin(s), std::cend(s));
      std::sort(neighbors[i].begin(), neighbors[i].end(),
                [this, &i](const auto &k, const auto &l) {
                  return get_distance(i, k) < get_distance(i, l);
                });
      ++i;
    }
    for (auto i = 0uz; i < num_vertices; ++i) {
      if (!processed[i])
        expand_cluster_order(i);
    }
    std::clog << "Clustering took " << b.duration() << "ms" << std::endl;
  }
};

std::vector<std::vector<std::size_t>> compute_density_based_clusters(
    const std::map<std::pair<std::size_t, std::size_t>, double>
        &pairwise_distances,
    const std::vector<double> &scores, const double eps,
    const double eps_extract, const std::size_t min_pts,
    const std::size_t max_num_clus, const bool do_sort) {

  if (!(eps_extract < eps)) {
    throw helper::Error(
        "[WHOOPS] Epsilon for extract_DBSCAN must be smaller than "
        "epsilon for the OPTICS algorithm!");
  }
  OpticsInternals o{pairwise_distances, scores, eps, eps_extract, min_pts,
                    max_num_clus,       do_sort};
  o.cluster();
  const auto result = o.extract_dbscan();
  return result;
}

} // namespace insilab::cluster
