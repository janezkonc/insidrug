#pragma once

#include "CLI11/CLI11.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include <any>
#include <boost/algorithm/string/join.hpp>
#include <iterator>
#include <map>
#include <memory>
#include <sstream>
#include <type_traits>
#include <typeinfo>
#include <vector>

namespace insilab::program {

template <typename T> struct is_vector : public std::false_type {};

template <typename T, typename A>
struct is_vector<std::vector<T, A>> : public std::true_type {};

/**
 * A 'facade' class for the CLI11.
 *
 */
class ArgumentParser {

  std::map<std::string, std::any> options;
  std::shared_ptr<CLI::App> app;
  std::map<std::string, bool> flags;

  auto __add_dashes(const std::vector<std::string> &arg) {
    std::vector<std::string> arg_dash;
    std::transform(arg.begin(), arg.end(), std::back_inserter(arg_dash),
                   [](const auto &s) {
                     return (s.size() == 1 ? std::string("-" + s)
                                           : std::string("--" + s));
                   });
    return arg_dash;
  }

public:
  ArgumentParser(const std::string &description = "")
      : app(std::make_shared<CLI::App>(description)) {}

  /**
   * Add an option that allows giving a configuration file from which options
   * can be read.
   *
   * @param arg name of configuration option
   * @param default_val default name of configuration file
   * @param help description of configuration option
   * @param required option required (true/false)
   */
  ArgumentParser &add_config(const std::vector<std::string> &arg,
                             const std::string &help = "Read an ini file",
                             const std::string &default_val = "config.ini",
                             bool required = false) {
    auto arg_dash = __add_dashes(arg);
    app->set_config(boost::algorithm::join(arg_dash, ","), default_val, help,
                    required);
    return *this;
  }

  /**
   * Add an option group that can be used to define exclusive / mutually needed
   * lists of options.
   *
   * @param name name of option group to be shown in help
   * @param options a vector of option names to be grouped together
   * @param description description of option group to be shown in help
   */
  ArgumentParser &add_option_group(const std::string &name,
                                   const std::vector<std::string> &options,
                                   const std::string &description = "") {
    auto group = app->add_option_group(name, description);
    const auto with_dashes = __add_dashes(options);
    for (const auto &opt_name : with_dashes) {
      auto opt = app->get_option(opt_name);
      group->add_option(opt);
    }
    return *this;
  }

  /**
   * Options in the first option group cannot be given together with options in
   * the second option group.
   *
   * @param name1 name of the first option group
   * @param name2 name of the second option group
   */
  ArgumentParser &set_excluded_option_group(const std::string &name1,
                                            const std::string &name2) {
    auto group1 = app->get_option_group(name1);
    auto group2 = app->get_option_group(name2);
    group1->excludes(group2);
    return *this;
  }

  /**
   * Set the number n of required options in each option group, so that exactly
   * n options should be given on the command line.
   *
   * @param name name of option group
   * @param n number of required options in group (-1 = ALL options)
   */
  ArgumentParser &set_required_option_group(const std::string &name,
                                            const int n = -1) {
    auto group = app->get_option_group(name);
    if (n == -1) {
      group->require_option(group->get_options().size());
    } else {
      group->require_option(n);
    }
    return *this;
  }

  /**
   * Set options that need other options without using option groups. All
   * options in the list should be given together or none should be given.
   *
   * @param options a list of option names
   */
  ArgumentParser &set_needed(const std::vector<std::string> &options) {
    const auto with_dashes = __add_dashes(options);
    for (const auto &opt_name1 : with_dashes) {
      auto opt1 = app->get_option(opt_name1);
      for (const auto &opt_name2 : with_dashes) {
        auto opt2 = app->get_option(opt_name2);
        opt1->needs(opt2);
      }
    }
    return *this;
  }

  /**
   * Set options that exclude other options without using option groups. If an
   * option in the first list is given, an option in the second list cannot
   * be given and the other way round.
   *
   * @param options1 a first list of option names
   * @param options2 a second list of option names
   */
  ArgumentParser &set_excluded(const std::vector<std::string> &options1,
                               const std::vector<std::string> &options2) {
    const auto with_dashes1 = __add_dashes(options1);
    const auto with_dashes2 = __add_dashes(options2);
    for (const auto &opt_name1 : with_dashes1) {
      auto opt1 = app->get_option(opt_name1);
      for (const auto &opt_name2 : with_dashes2) {
        auto opt2 = app->get_option(opt_name2);
        opt1->excludes(opt2);
      }
    }
    return *this;
  }

  /**
   * Add a new option.
   *
   * @param arg a vector of option aliases, e.g. {"n", "number"}
   * @param help description of option
   * @param default_val default value
   * @param required if true, argument is required
   * @param allowed_values a vector of discrete values for option
   * @param allowed_range a range [min, max]
   */
  template <typename T>
  ArgumentParser &
  add_argument(const std::vector<std::string> &arg,
               const std::string &help = std::string(),
               const T &default_val = T(), bool required = false,
               const std::vector<T> &allowed_values = std::vector<T>(),
               const std::pair<T, T> &allowed_range = std::pair<T, T>()) {

    const auto opt_name =
        *std::max_element(/* longest option name */
                          arg.begin(), arg.end(),
                          [](const std::string &a, const std::string &b) {
                            return a.size() < b.size();
                          });
    auto arg_dash = __add_dashes(arg);
    if constexpr (std::is_same<T, bool>()) {
      options[opt_name] = false;
      app->add_flag(boost::algorithm::join(arg_dash, ","), flags[opt_name],
                    help);
    } else {
      auto opt = app->add_option<std::any, T>(
          boost::algorithm::join(arg_dash, ","), options[opt_name], help, true);
      if (required)
        opt->required();
      if constexpr (!is_vector<T>()) { // CLI::IsMember doesn't compile for
                                       // T=vector
        opt->default_val(default_val);
        if (allowed_values != std::vector<T>())
          opt->check(CLI::IsMember(allowed_values));
      } else {
        // empty vector cannot be a default value as CLI will complain
        opt->expected(0, -1); // allow an empty container (if you write
                              // --vector_arg without any parameters on cmdline)
        if (default_val != T()) {
          opt->default_val(default_val);
        } else {
          opt->default_str("{}"); // allow an empty vector from config file
        }
      }
      if constexpr (std::is_same<T, int>() || std::is_same<T, double>()) {
        if (allowed_range != std::pair<T, T>())
          opt->check(CLI::Range(allowed_range.first, allowed_range.second));
      }
    }
    return *this;
  }

  ArgumentParser &parse(int argc, char *argv[]) {
    try {
      app->parse(argc, argv);
    } catch (const CLI::ParseError &e) {
      app->exit(e);
      throw helper::Error("");
    }
    return *this;
  }

  /**
   * Get option by name.
   *
   * @param opt_name name of option
   * @return option converted to type T
   */
  template <typename T> const T get(const std::string &opt_name) const {
    try {
      if constexpr (std::is_same<T, bool>()) {
        return flags.at(opt_name);
      } else if constexpr (is_vector<T>()) {
        if (!options.at(opt_name).has_value()) {
          return T{}; // simulate a default value (an empty vector) if vector
                      // argument with empty vector as default value was not
                      // given on command line
        }
      }
      return std::any_cast<T>(options.at(opt_name));
    } catch (const std::out_of_range &) {
      throw helper::Error("[WHOOPS] argument " + opt_name + " does not exist");
    } catch (const std::bad_any_cast &e) {
      throw helper::Error("[WHOOPS] bad cast of argument " + opt_name + " " +
                          e.what());
    }
  }

  /**
   * Check if option was given on the command line.
   *
   * @param opt_name name of option
   * @return true or false
   */
  bool is_set(const std::string &opt_name) const {
    try {
      return app->count("--" + opt_name);
    } catch (const CLI::OptionNotFound &e) {
      throw helper::Error("[WHOOPS] couldn't check if option is set " +
                          helper::to_string(e.what()));
    }
  }

  /**
   * Print configuration file to a std::string (useful when trying to run the
   * application with same parameters).
   *
   * @param default_also include values for default parameters
   * @param write_description include description of parameters
   * @return a std::string with all parameters/values listed
   */
  auto print_config(bool default_also = false,
                    bool write_description = false) const {
    return app->config_to_str(default_also, write_description);
  }
};

} // namespace insilab::program
