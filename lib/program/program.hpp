#pragma once

#include "argumentparser.hpp"
#include <memory>

namespace insilab::program {

using FParseArgs = ArgumentParser(int, char *[]);
using FRun = int(const ArgumentParser &);
int create(int argc, char *argv[], const FParseArgs &pa, const FRun &r) {
  try {
    auto args = pa(argc, argv);
    return r(args);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
}

} // namespace insilab::program
