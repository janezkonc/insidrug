#include "inout.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/gzip.hpp"
#include "helper/help.hpp"
#include "path/path.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/stream.hpp> // for stream
#include <boost/regex.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <mutex>
#include <numeric>
#include <regex>

namespace insilab::inout {

static std::mutex m{};

namespace fs = std::filesystem;

/**
 * Creates directories with subdirectories from path that may include a file as
 * the end of path.
 *
 * @param dir_path one/two/three/file.txt will result in created directory
 * hierarchy one/two/three
 */
void __mkdir(const std::string &dir_path) {
  fs::path dir(dir_path);
  dir.remove_filename();
  if (!dir.string().empty()) {
    if (!fs::exists(dir)) {
      if (!fs::create_directories(dir)) {
        throw helper::Error("[WHOOPS] cannot create directory " + dir.string());
      }
    }
  }
}

auto __read_txt_file(const std::string &filename,
                     const std::streampos &pos_in_file, const int num_occur,
                     const std::string &pattern) {

  std::vector<std::string> s;
  std::ifstream in(filename);
  in.seekg(pos_in_file);
  std::string line;
  int i = 0;
  const auto patt = std::regex(pattern);
  std::streampos pos = in.tellg();
  while (std::getline(in, line)) {
    std::smatch sm;
    if (num_occur != -1 && std::regex_search(line, sm, patt) &&
        ++i == num_occur) {
      const auto prefix =
          sm.prefix().str(); // extract line until first match of pattern
      // when a pattern is $, the size of match is actually 0, so to continue on
      // the next position, increase pos by 1
      pos += prefix.size() + (sm[0].str().size() == 0 ? !in.eof() : 0);
      s.push_back(prefix);
      break;
    }
    pos = in.tellg();
    s.push_back(line);
  }
  return std::make_pair(s, pos);
}

auto __read_gz_file(const std::string &filename,
                    const std::streampos &pos_in_file, const int num_occur,
                    const std::string &pattern) {

  std::vector<std::string> s;
  std::ifstream file(filename, std::ios_base::in | std::ios_base::binary);
  boost::iostreams::filtering_istream in;
  in.push(boost::iostreams::gzip_decompressor());
  in.push(file);
  std::string line;
  // since gzip stream doesn't support seeking, rewind to previous position
  in.ignore(pos_in_file);
  std::streampos pos = pos_in_file;
  int i = 0;
  const auto patt = std::regex(pattern);
  while (std::getline(in, line)) {
    std::smatch sm;
    if (num_occur != -1 && std::regex_search(line, sm, patt) &&
        ++i == num_occur) {
      const auto prefix =
          sm.prefix().str(); // extract line until first match of pattern
      // when a pattern is $, the size of match is actually 0, so to continue on
      // the next position, increase pos by 1
      pos += prefix.size() + (sm[0].str().size() == 0 ? !in.eof() : 0);
      s.push_back(prefix);
      break;
    }
    pos += line.size() + !in.eof();
    s.push_back(line);
  }
  boost::iostreams::close(in);
  return std::make_pair(s, pos);
}

std::pair<std::vector<std::string>, std::streampos>
read_file(const std::string &filename, const std::streampos &pos_in_file,
          FileNotFound w, const int num_occur, const std::string &pattern) {

  std::ifstream f(filename);
  if (!f.is_open() && w == FileNotFound::panic)
    throw helper::Error("[WHOOPS] cannot open file " + filename + "\n");
  f.close();
  try {
    return path::has_suffix(filename, ".gz")
               ? __read_gz_file(filename, pos_in_file, num_occur, pattern)
               : __read_txt_file(filename, pos_in_file, num_occur, pattern);
  } catch (const std::exception &e) {
    if (w == inout::FileNotFound::panic)
      throw helper::Error("[WHOOPS] an error occured during reading file " +
                          filename + " with errmsg " + e.what());
  }
  return {std::vector<std::string>(), pos_in_file};
}

std::pair<std::string, std::streampos>
read_file_to_str(const std::string &filename, const std::streampos &pos_in_file,
                 FileNotFound w, const int num_occur,
                 const std::string &pattern) {
  const auto &[lines, new_pos] =
      inout::read_file(filename, pos_in_file, w, num_occur, pattern);
  std::vector<std::string> lines_with_newline;
  std::transform(std::begin(lines), std::end(lines),
                 std::back_inserter(lines_with_newline), [](const auto &line) {
                   return line.empty() ? "" : line + '\n';
                 });
  return {std::accumulate(std::begin(lines_with_newline),
                          std::end(lines_with_newline), std::string{}),
          new_pos};
}

void file_open_put_contents(const std::string &filename,
                            const std::vector<std::string> &v,
                            std::ios_base::openmode mode) {
  std::stringstream ss;
  for (auto &s : v)
    ss << s << std::endl;
  file_open_put_stream(filename, ss, mode);
}

void file_open_put_stream(const std::string &filename,
                          const std::stringstream &ss,
                          std::ios_base::openmode mode) {
  std::lock_guard lock(m);

  __mkdir(filename);
  std::ofstream output_file(filename, mode);

  if (!output_file.is_open())
    throw helper::Error("[WHOOPS] cannot open output file: " +
                        filename); // how to emulate $! ?
  bool gzipped = path::has_suffix(filename, ".gz");
  if (gzipped) {
    output_file << helper::Gzip::compress(ss.str());
  } else {
    output_file << ss.str();
  }
}

void create_empty_file(const std::string &filename) {
  inout::output_file("", filename);
}

} // namespace insilab::inout
