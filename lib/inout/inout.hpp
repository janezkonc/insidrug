#pragma once

#include <algorithm>
#include <fstream>
#include <functional>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

// Reading and writing to files. Writing is thread-safe using static mutex,
// while reading is not. If multiple threads are reading a file at the same
// time, this file should not be modified during reading.
namespace insilab::inout {

enum class FileNotFound { panic = 0, no_panic = 1 };

/**
 * Read a file (may be gzipped) from current position (default 0) until N-th
 * occurrence of the given pattern is found. The pattern itself is not included
 * in the results. The line in which the pattern was found is included up until
 * the pattern.
 * 
 * @param filename a string, file name
 * @param pos_in_file initial position in file where reading will start
 * @param num_occur -1 ... read file in one chunk, >0 ... read up until N-th
 * occurrence of a pattern
 * @param pattern a string on which the reading stops when found
 * @return a pair, the first element is a vector with lines that were read from
 * the file, the second element is the new position in file after n-th
 * occurrence of pattern was found or EOF reached
 */
std::pair<std::vector<std::string>, std::streampos>
read_file(const std::string &filename, const std::streampos &pos_in_file = 0,
          FileNotFound = FileNotFound::panic, const int num_occur = -1,
          const std::string &pattern = ""); // throws Error

/**
 * Read a text file and return a string with its contents.
 *
 * @param filename a string, file name
 * @param pos_in_file initial position in file where reading will start
 * @param w what to do if reading fails (default is to throw)
 * @param num_occur -1 ... read file in one chunk, >0 ... read up until N-th
 * occurrence of a pattern
 * @param pattern a string on which the reading stops when found
 * @return a pair, the first element is a string with contents of the file, the
 * second element is the new position in file after n-th occurrence of pattern
 * was found or EOF reached
 */
std::pair<std::string, std::streampos>
read_file_to_str(const std::string &filename,
                 const std::streampos &pos_in_file = 0,
                 FileNotFound w = FileNotFound::panic, const int num_occur = -1,
                 const std::string &pattern = "");

void file_open_put_contents(const std::string &filename,
                            const std::vector<std::string> &v,
                            std::ios_base::openmode = std::ios_base::out);

void file_open_put_stream(const std::string &filename,
                          const std::stringstream &ss,
                          std::ios_base::openmode = std::ios_base::out);

template <typename S> struct out_manipulator : public std::function<S &(S &)> {
  template <typename T>
  out_manipulator(T &&t)
      : std::function<S &(S &)>([=](S &i) -> S & { return i << t; }) {}
  template <typename T>
  out_manipulator(T *t)
      : std::function<S &(S &)>([=](S &i) -> S & { return i << t; }) {
  } // for g++
  template <typename U> friend U &operator<<(U &u, out_manipulator &a) {
    return static_cast<U &>(a(u));
  }
};

/**
 * Write (almost) anything to a file.
 *
 * @param anything a template variable, which is written to a file using
 * its operator<<
 * @param filename a string, name of output file
 * @param manips a vector of output manipulators defining the formatting of
 * outputted stream, e.g., molib::IOmanip::mol2
 * @param mode an open mode for file (out - overwrite, app - append)
 */
template <class T>
void output_file(
    const T &anything, const std::string &filename,
    const std::vector<inout::out_manipulator<std::ostream>> &manips =
        std::vector<inout::out_manipulator<std::ostream>>(),
    std::ios_base::openmode mode = std::ios_base::out) {
  std::stringstream ss;
  for (auto m : manips)
    ss << m;
  ss << anything;
  inout::file_open_put_stream(filename, ss, mode);
}

/**
 * Create an empty file or overwrite existing file with empty one.
 *
 * @param filename a string, name of file
 */
void create_empty_file(const std::string &filename);

} // namespace insilab::inout
