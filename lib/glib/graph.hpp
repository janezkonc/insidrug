#pragma once

#include "helper/array2d.hpp"
#include "helper/it.hpp"
#include <algorithm>
#include <concepts>
#include <memory>
#include <queue>
#include <ranges>
#include <set>

namespace insilab::glib {

// Index of a vertex in a Graph
using node_idx_t = std::size_t;
using signed_node_idx_t = std::make_signed_t<node_idx_t>;

// Weight type
using weight_t = double; // int for testing

// Deduce vertex type from a graph-like container
template <typename Container>
using Vertex = std::remove_cvref_t<
    decltype(*std::declval<typename Container::value_type>())>;

// Check if vertex is iterable on its neighbor vertices
template <typename T>
concept NeighborIterable =
    std::derived_from<T, helper::template_vector_container<T>>;

// Allow containers like std::vector, std::set, std::initializer_list of Vertex*
// or std::unique_ptr<Vertex>; each vertex must be iterable on its neighbor
// vertices
template <typename Container>
concept GraphLike =
    NeighborIterable<Vertex<Container>> && std::ranges::range<Container> &&
    (std::is_pointer_v<typename Container::value_type> ||
     std::is_same_v<
         typename Container::value_type,
         std::unique_ptr<typename Container::value_type::element_type>>);

// Check if a vertex has weight
template <typename T>
concept HasWeight = requires(T vertex) { vertex.weight(); };

template <typename Container>
using SubGraphs = std::set<std::set<Vertex<Container> *>>;

using Matches =
    std::vector<std::pair<std::vector<node_idx_t>, std::vector<node_idx_t>>>;

/**
 * Generate an adjacency matrix for any graph-like container.
 *
 * If a vertex in graph is connected to a vertex outside of graph
 * (this could happen if, for example, the input graph is subgraph of a
 * larger graph), such a connection is not added to the adjacency
 * matrix.
 *
 * @param graph a vector, set, or any iterable container that satisfies range
 * concept (has begin and end), in which each element is a pointer or unique_ptr
 * to a vertex; each vertex, in turn, is iterable container, with connected
 * vertices as elements
 * @return a 2D array in which each connected pair of vertices is set to true
 */
template <GraphLike T>
helper::Array2d<bool> generate_adjacency_matrix(const T &graph) {
  auto result{helper::Array2d<bool>(graph.size())};
  std::map<const Vertex<T> *, node_idx_t> idx;
  for (node_idx_t i{0}; const auto &v : graph)
    idx[&*v] = i++;
  for (const auto &v : graph) {
    const node_idx_t i = idx[&*v];
    for (const auto &adj_v : *v) {
      if (!idx.contains(&adj_v))
        continue;
      const node_idx_t j = idx[&adj_v];
      result.set(i, j);
      result.set(j, i);
    }
  }
  return result;
}

/**
 * Print a graph (graph-like container) in DIMACS format to a string.
 *
 * If a graph is weighted (vertices have weigths), these are printed as well.
 *
 * @param graph a vector, set, or any iterable container that satisfies range
 * concept (has begin and end), in which each element is a pointer or unique_ptr
 * to a vertex; each vertex, in turn, is iterable container, with connected
 * vertices as elements
 * @return a string with graph in DIMACS format
 *
 */
template <GraphLike T> std::string print_graph(const T &graph) {
  std::map<const Vertex<T> *, node_idx_t> idx;
  for (node_idx_t i{0}; auto &vertex : graph)
    idx[&*vertex] = i++;
  auto num_edges{0uz};
  for (const auto &v : graph) {
    for (const auto &adj_v : v) {
      if (!idx.contains(&adj_v))
        continue;
      ++num_edges;
    }
  }
  assert(num_edges % 2 == 0);
  num_edges /= 2; // every edge is counted two times
  std::stringstream ss;
  ss << "p " << graph.size() << " " << num_edges << std::endl;
  for (const auto &v : graph) {
    const node_idx_t i = idx[&*v];
    for (const auto &adj_v : v) {
      if (!idx.contains(&adj_v))
        continue;
      const node_idx_t j = idx[&adj_v];
      ss << "e " << i + 1 << " " << j + 1 << std::endl;
    }
  }
  if constexpr (HasWeight<Vertex<T>>) {
    for (const auto &v : graph) {
      ss << "w " << idx[&*v] << " " << v->weight() << std::endl;
    }
  }
  return ss.str();
}

} // namespace insilab::glib
