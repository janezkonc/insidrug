#pragma once

#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include <string>
#include <vector>

namespace insilab::glib::io {

/**
 * Write to a file an undirected (optionally weighted) graph in DIMACS format.
 *
 * @note vertex numbers in the file written will start from 1
 *
 * @param filename name of the output file to which the graph will be written
 * @param conn adjacency matrix of the graph
 * @param weights (optional) a vector of weights, one weight (double) for each
 * vertex of the graph
 */
void write_dimacs_graph(const std::string &filename,
                        const helper::Array2d<bool> &conn,
                        const std::vector<glib::weight_t> &weights = {});

/**
 * Read from a DIMACS-formatted file an undirected (optionally weighted) graph.
 *
 * @note vertex numbers in the file that is being read must start from 1
 *
 * @param filename name of the file from which to read the graph
 * @return a pair, in which the first element is adjacency matrix, and the
 * second element is the vector of weights, one for each vertex (if present in
 * file)
 *
 */
std::pair<helper::Array2d<bool>, std::vector<glib::weight_t>>
read_dimacs_graph(const std::string &filename);

} // namespace insilab::glib::io
