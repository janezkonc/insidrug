#include "io.hpp"
#include "helper/error.hpp"
#include "inout/inout.hpp"
#include <iostream>
#include <numeric>
#include <sstream>
#include <utility>

namespace insilab::glib::io {

void write_dimacs_graph(const std::string &filename,
                        const helper::Array2d<bool> &conn,
                        const std::vector<glib::weight_t> &weights) {
  std::stringstream ss;
  int nodes = conn.get_szi();
  int edges = 0;
  for (int i = 0; i < conn.get_szi(); ++i) {
    ss << "n " << i + 1 << " " << weights[i] << std::endl;
  }
  for (int i = 0; i < conn.get_szi(); i++) {
    for (int j = i + 1; j < conn.get_szj(); j++) {
      if (conn.get(i, j)) {
        ss << "e " << i + 1 << " " << j + 1 << std::endl;
        ++edges;
      }
    }
  }
  std::stringstream result;
  result << "p edge " << nodes << " " << edges << std::endl << ss.str();

  inout::output_file(result.str(), filename);
}

std::pair<helper::Array2d<bool>, std::vector<glib::weight_t>>
read_dimacs_graph(const std::string &filename) {

  helper::Array2d<bool> conn;
  std::vector<glib::weight_t> weights;

  const std::vector<std::string> graph = inout::read_file(filename).first;

  for (const auto &line : graph) {
    std::stringstream ss(line);
    char ch;
    if (line[0] == 'p') {
      std::string keyword;
      int nodes, edges;
      ss >> ch >> keyword >> nodes >> edges;
      conn = helper::Array2d<bool>(nodes);
    } else if (line[0] == 'n') {
      int node;
      glib::weight_t weight;
      ss >> ch >> node >> weight;
      weights.push_back(weight);
    } else if (line[0] == 'e') {
      assert(!conn.empty());
      int node1, node2;
      ss >> ch >> node1 >> node2;
      node1 -= 1;
      node2 -= 1;
      if (node1 < 0 || node2 < 0)
        throw helper::Error("[WHOOPS] Wrong format of graph file: vertex "
                            "numbers must start from one not zero!");
      conn.set(node1, node2);
      conn.set(node2, node1);
    }
  }
  return {std::move(conn), std::move(weights)};
}

} // namespace insilab::glib::io
