#pragma once
#include "glib/graph.hpp"
#include "mcqd.hpp"
#include "product.hpp"

namespace insilab::glib::algorithms {

/**
 * Find all occurrences of graph g2 within graph g1. Graphs are compared on
 * their topology (edges) and optionally on their vertices (if vertices are
 * labeled and a comparison function is provided).
 *
 * @param g1 a graph (graph-like container) in which graph g2 will be searched
 * (g1 should be of equal size or larger than g2)
 * @param g2 a graph that is searched for within graph g1
 * @param distance_matrix1 shortest distances matrix for the first graph
 * (this is optimization if g1 is compared to many different g2 graphs)
 * @param comp_vertex a function taking two vertices as parameters and returning
 * true if vertices are equal (default function always returns true, so all
 * vertices are equal, meaning the two graphs will be treated as unlabeled
 * graphs)
 *
 * @return a vector of pairs, each pair corresponding to a different
 * vertex-to-vertex match of the two graphs; the first element of a pair is a
 * vector of indices of vertices of the first graph and the second element is a
 * vector of indices of vertices of the second graph, where each two vertices at
 * the same position, are equal
 */
template <GraphLike T, GraphLike P>
auto match(
    const T &g1, const P &g2,
    const helper::Array2d<node_idx_t> &distance_matrix1,
    const auto comp_vertex = [](const Vertex<T> &v1, const Vertex<P> &v2) {
      return true;
    }) {

  // helper::Benchmark<std::chrono::milliseconds> b;

  Matches result;

  try {
    const auto [vertices, adjacency_matrix] =
        generate_product_graph_topological(g1, g2, comp_vertex,
                                           distance_matrix1);
    const auto qmaxes =
        algorithms::find_n_k_cliques(adjacency_matrix, g2.size());

    for (const auto &qmax : qmaxes) {
      result.push_back({});
      auto &match = result.back();
      for (const auto &i : qmax) {
        match.first.push_back(vertices[i].first);
        match.second.push_back(vertices[i].second);
      }
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  // std::clog << "Matching a pair of graphs took " << b.duration() << " ms.\n";

  return result;
}

/// Returns true if two graphs (graph-like containers) are isomorphic.
template <GraphLike T, GraphLike P>
bool isomorphic(
    const T &g1, const P &g2,
    const auto comp_vertex = [](const Vertex<T> &v1, const Vertex<P> &v2) {
      return true;
    }) {
  if (g1.size() != g2.size())
    return false;
  const auto &m =
      match(g1, g2, algorithms::compute_shortest_path_matrix(g1), comp_vertex);
  return m.size() > 0 && m[0].first.size() == g2.size();
}

} // namespace insilab::glib::algorithms
