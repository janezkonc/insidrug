#pragma once

#include "glib/graph.hpp"

#include <algorithm>
#include <limits>
#include <queue>

namespace insilab::glib::algorithms {

/**
 * Compute shortest path distance between each pair of vertices in a graph
 * (number of edges between them) using BFS (Breadth-First Search) algorithm.
 * This is more efficient when graph is edge-sparse.
 *
 * @param graph a vector, set, or any iterable container that satisfies range
 * concept (has begin and end), in which each element is a pointer or unique_ptr
 * to a vertex; each vertex, in turn, is iterable container, with connected
 * vertices as elements
 * @param max_dist limit the maximum length of a path that is calculated, so
 * that dist < max_dist
 * @return a 2D array with shortest distances between vertices
 */
template <GraphLike T>
auto compute_shortest_path_matrix_sparse_graph(
    const T &graph, const glib::node_idx_t max_dist =
                        std::numeric_limits<glib::node_idx_t>::max()) {

  // helper::Benchmark<std::chrono::milliseconds> b;

  using vertex_t = Vertex<T>;
  std::map<const vertex_t *, node_idx_t> idx;
  for (node_idx_t i{0}; const auto &v : graph)
    idx[&*v] = i++;
  helper::Array2d<node_idx_t> distance_matrix(graph.size());
  for (const auto &u : graph) {
    const auto pu = &*u;
    std::queue<std::pair<const vertex_t *, node_idx_t>>
        fifo_q; // {vertex, distance}
    std::set<const vertex_t *> visited;
    fifo_q.push({pu, 0});
    while (!fifo_q.empty()) {
      const auto [v, dist] = fifo_q.front();
      const auto pv = &*v;
      fifo_q.pop();
      visited.insert(pv);
      if (dist >= max_dist)
        continue;
      const auto idx_u = idx[pu];
      const auto idx_v = idx[pv];
      auto &dist_uv = distance_matrix[idx_u, idx_v];
      if (dist_uv == 0 || dist < dist_uv) {
        dist_uv = dist;
      }
      for (const auto &w : *v) {
        if (!visited.contains(&w)) {
          visited.insert(&w);
          fifo_q.push({&w, dist + 1});
        }
      }
    }
  }
  for (node_idx_t i = 0; i < distance_matrix.get_szi(); ++i) {
    for (node_idx_t j = 0; j < distance_matrix.get_szi(); ++j) {
      if (i != j && distance_matrix[i, j] == 0)
        distance_matrix[i, j] = std::numeric_limits<node_idx_t>::max();
    }
  }

  // std::clog << "Computing shortest path matrix for sparse graph took "
  //           << b.duration() << " ms.\n";
  return distance_matrix;
}

/**
 * Compute shortest path distance between each pair of vertices in a graph
 * (number of edges between them) using Floyd-Warshal algorithm.
 *
 * @param graph a vector, set, or any iterable container that satisfies range
 * concept (has begin and end), in which each element is a pointer or
 * unique_ptr to a vertex; each vertex, in turn, is iterable container, with
 * connected vertices as elements
 * @return a 2D array with shortest distances between vertices
 */
auto compute_shortest_path_matrix(const GraphLike auto &graph) {

  // helper::Benchmark<std::chrono::milliseconds> b;

  helper::Array2d<node_idx_t> distance_matrix(graph.size());
  const auto &bond_matrix = generate_adjacency_matrix(graph);

  for (node_idx_t i = 0; i < bond_matrix.get_szi(); ++i) {
    for (node_idx_t j = 0; j < bond_matrix.get_szj(); ++j) {
      if (!bond_matrix.get(i, j) && i != j) {
        distance_matrix[i, j] = std::numeric_limits<node_idx_t>::max();
      } else if (bond_matrix.get(i, j)) {
        distance_matrix[i, j] = 1;
      }
    }
  }
  for (node_idx_t k = 0; k < distance_matrix.get_szi(); ++k) {
    for (node_idx_t i = 0; i < distance_matrix.get_szi(); ++i) {
      for (node_idx_t j = 0; j < distance_matrix.get_szi(); ++j) {
        if (distance_matrix[i, k] == std::numeric_limits<node_idx_t>::max() ||
            distance_matrix[k, j] == std::numeric_limits<node_idx_t>::max())
          continue;
        if (distance_matrix[i, j] >
            distance_matrix[i, k] + distance_matrix[k, j]) {
          distance_matrix[i, j] = distance_matrix[i, k] + distance_matrix[k, j];
        }
      }
    }
  }
  // std::clog << "Computing shortest path matrix (Floyd-Warshall) "
  //           << b.duration() << " ms.\n";
  return distance_matrix;
}

/**
 * Find a path (any path, not necessarily the shortest one) between two
 * vertices of a graph.
 *
 * @param start first vertex
 * @param end last vertex
 * @return a vector of pointers to vertices representing a path between start
 * and end vertex; if a path cannot be found, return an empty path
 */
template <NeighborIterable Vertex>
std::vector<Vertex *> find_path(const Vertex &start, const Vertex &goal) {

  const auto reconstruct_path =
      [](const Vertex &goal,
         const std::map<const Vertex *, const Vertex *> &came_from) {
        std::vector<Vertex *> path;
        path.push_back(const_cast<Vertex *>(&goal));
        while (came_from.contains(path.back())) {
          path.push_back(const_cast<Vertex *>(came_from.at(path.back())));
        }
        return path;
      };

  std::queue<const Vertex *> openset;
  openset.push(&start);
  std::set<const Vertex *> closedset;
  std::map<const Vertex *, const Vertex *> came_from;
  while (!openset.empty()) {
    const Vertex &curr = *openset.front();
    openset.pop();
    closedset.insert(&curr);
    if (&curr == &goal) {
      return reconstruct_path(curr, came_from);
    }
    for (const auto &adj_v : curr) {
      if (!closedset.contains(&adj_v)) {
        came_from[&adj_v] = &curr;
        openset.push(&adj_v);
      }
    }
  }
  return {};
}

} // namespace insilab::glib::algorithms
