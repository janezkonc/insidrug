#pragma once

#include "geom3d/linear.hpp"
#include <algorithm>
#include <functional>
#include <memory>
#include <queue>
#include <set>

namespace insilab::glib::algorithms {

class FdLayout {

  geom3d::Point::Graph __g;
  geom3d::Point::Vec __v, __a; // velocity, acceleration

  double __dist;
  int __nsteps;
  int __nupdate;
  double __tstep;
  double __l;
  double __k;

  void __update_edges();
  void __accelerate();
  void __integrate();

public:
  FdLayout(const geom3d::Point::Vec &vertices, const double dist,
           const int nsteps = 1000, const int nupdate = 100,
           const double tstep = 0.01, const double l = 1.0,
           const double k = 1.0)
      : __g(geom3d::Point::create_graph(vertices, dist)), __v(vertices.size()),
        __a(vertices.size()), __dist(dist), __nsteps(nsteps),
        __nupdate(nupdate), __tstep(tstep), __l(l), __k(k) {}
  geom3d::Point::Vec run();
};

} // namespace insilab::glib
