#pragma once

#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include "helper/benchmark.hpp"
#include "path.hpp"

namespace insilab::glib::algorithms {

/**
 * Generate a product graph, whose vertices are pairs of compatible vertices
 * (vi, vj), where v1 is from g1 and v2 from g2. An edge between two vertices
 * (v1i, v2i) and (v1j, v2j) is drawn if distance(v1i, v1j) is equal (or within
 * tolerance) to distance(v2i, v2j). This enables comparison of the two graphs
 * using clique algorithms.
 *
 * @param g1 first graph (graph-like container)
 * @param g2 second graph
 * @param comp_vertex a function taking two vertices as parameters and returning
 * true if vertices are equal
 * @param distance_matrix1 shortest distances matrix for the first graph
 * (this optimization results in higher performance if g1 is compared to many
 * different g2 graphs)
 * @param max_allow_path tolerance for the equality of distances, so that d(v1i,
 * v1j) is within max_allow_path of d(v2i, v2j); if 0, then d(v1i, v1j) ==
 * d(v2i, v2j)
 * @return a pair, in which first element is vertices of the product graph, and
 * second element is adjacency matrix
 */
auto generate_product_graph_topological(
    const GraphLike auto &g1, const GraphLike auto &g2, const auto comp_vertex,
    const helper::Array2d<node_idx_t> &distance_matrix1,
    const node_idx_t max_allow_path = 0) {

  // helper::Benchmark<std::chrono::milliseconds> b;

  std::vector<std::pair<node_idx_t, node_idx_t>> vertices;

  // generate vertices of product graph (these are pairs of equal vertices)
  for (node_idx_t i{0}; const auto &v1 : g1) {
    for (node_idx_t j{0}; const auto &v2 : g2) {
      if (comp_vertex(*v1, *v2)) {
        vertices.emplace_back(i, j);
      }
      ++j;
    }
    ++i;
  }

  const auto &distance_matrix2 = compute_shortest_path_matrix(g2);
  helper::Array2d<bool> adjacency_matrix(vertices.size());

  for (auto i{0uz}; i < vertices.size(); ++i) {
    for (auto j = i + 1; j < vertices.size(); ++j) {
      const auto &[v1i, v2i] = vertices[i];
      const auto &[v1j, v2j] = vertices[j];

      if (v1i != v1j && v2i != v2j) {

        if (std::abs(static_cast<int>(distance_matrix1[v1i, v1j] -
                                      distance_matrix2[v2i, v2j])) <=
            max_allow_path) {
          adjacency_matrix.set(i, j);
          adjacency_matrix.set(j, i);
        }
      }
    }
  }

  // std::clog << "Generating topological product graph took " << b.duration()
  //           << " ms.\n";

  return std::pair{std::move(vertices), std::move(adjacency_matrix)};
}

/**
 * Generate a product graph, whose vertices are pairs of compatible vertices
 * (vi, vj), where v1 is from g1 and v2 from g2. An edge between two vertices
 * (v1i, v2i) and (v1j, v2j) is drawn if distance(v1i, v1j) is equal (or within
 * tolerance) to distance(v2i, v2j). This enables comparison of the two graphs
 * using clique algorithms.
 *
 * @note this overload is necessary due to optimization. Before we had only one
 * generate_product_graph... function having: const auto &distance_matrix1 =
 * default_is_empty ? compute_shortest... : default_distance_matrix1; but this
 * somehow turned off optimization??, seemingly it was copying Array2d instead
 * of just assigning a reference. I found this in fixing issue #121.
 *
 * @param g1 first graph (graph-like container)
 * @param g2 second graph
 * @param comp_vertex a function taking two vertices as parameters and returning
 * true if vertices are equal
 * @param max_allow_path tolerance for the equality of distances, so that d(v1i,
 * v1j) is within max_allow_path of d(v2i, v2j); if 0, then d(v1i, v1j) ==
 * d(v2i, v2j)
 * @return a pair, in which first element is vertices of the product graph, and
 * second element is adjacency matrix
 */
auto generate_product_graph_topological(const GraphLike auto &g1,
                                        const GraphLike auto &g2,
                                        const auto comp_vertex,
                                        const node_idx_t max_allow_path = 0) {

  const auto &distance_matrix1 =
      compute_shortest_path_matrix_sparse_graph(g1, g2.size());
  return generate_product_graph_topological(g1, g2, comp_vertex,
                                            distance_matrix1, max_allow_path);
}

} // namespace insilab::glib::algorithms
