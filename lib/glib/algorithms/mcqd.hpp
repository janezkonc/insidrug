#pragma once

#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include "internals/kcq_strategy.hpp"
#include "internals/kcqd_strategy.hpp"
#include "internals/kcqdw_strategy.hpp"
#include "internals/kcqw_strategy.hpp"
#include "internals/mcq_strategy.hpp"
#include "internals/mcqd_strategy.hpp"
#include "internals/mcqdw_exp_strategy.hpp"
#include "internals/mcqdw_strategy.hpp"
#include "internals/mcqw_exp_strategy.hpp"
#include "internals/mcqw_strategy.hpp"
#include "internals/naive_kcqw_strategy.hpp"
#include "internals/naive_mcqw_strategy.hpp"
#include "internals/vertices.hpp"
#include <cassert>
#include <concepts>
#include <vector>

namespace insilab::glib::algorithms {

using internals::KCQDStrategy;
using internals::KCQDWStrategy;
using internals::KCQStrategy;
using internals::KCQWStrategy;
using internals::MCQDStrategy;
using internals::MCQDWExpStrategy;
using internals::MCQDWStrategy;
using internals::MCQStrategy;
using internals::MCQWExpStrategy;
using internals::MCQWStrategy;
using internals::NaiveKCQWStrategy;
using internals::NaiveMCQWStrategy;

using internals::DecreasingDegreesIncreasingWeightsVertexPolicy;
using internals::DecreasingDegreesVertexPolicy;

using internals::HighestWeightPolicy;
using internals::LowestWeightPolicy;

template <template <typename> typename SearchStrategy, typename T>
concept MCQConcept = std::is_same_v<SearchStrategy<T>, MCQStrategy<T>>;

template <template <typename> typename SearchStrategy, typename T>
concept MCQDConcept = std::is_same_v<SearchStrategy<T>, MCQDStrategy<T>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept KCQDWConcept =
    std::is_same_v<SearchStrategy<T, S>, KCQDWStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept NaiveKCQWConcept =
    std::is_same_v<SearchStrategy<T, S>, NaiveKCQWStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept KCQWConcept = std::is_same_v<SearchStrategy<T, S>, KCQWStrategy<T, S>>;

template <template <typename> typename SearchStrategy, typename T>
concept KCQDConcept = std::is_same_v<SearchStrategy<T>, KCQDStrategy<T>>;

template <template <typename> typename SearchStrategy, typename T>
concept KCQConcept = std::is_same_v<SearchStrategy<T>, KCQStrategy<T>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept MCQDWConcept =
    std::is_same_v<SearchStrategy<T, S>, MCQDWStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept NaiveMCQWConcept =
    std::is_same_v<SearchStrategy<T, S>, NaiveMCQWStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept MCQWConcept = std::is_same_v<SearchStrategy<T, S>, MCQWStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept MCQWExpConcept =
    std::is_same_v<SearchStrategy<T, S>, MCQWExpStrategy<T, S>>;

template <template <typename, typename> typename SearchStrategy, typename T,
          typename S>
concept MCQDWExpConcept =
    std::is_same_v<SearchStrategy<T, S>, MCQDWExpStrategy<T, S>>;

/**
 * Find a maximum clique, i.e., a fully connected subgraph with most vertices,
 * in an undirected graph.
 *
 * If you use this algorithm, please cite:
 *
 * Janez Konc and Dusanka Janezic. An improved branch and bound algorithm for
 * the maximum clique problem. MATCH Commun. Math. Comput. Chem., 2007, 58,
 * 569-590.
 *
 * More information at: http://insilab.org/maxclique
 *
 * @tparam SearchStrategy type of search to perform, one of the following
 * (MCQStrategy or MCQDStrategy, the last one uses dynamic upper bounds
 * algorithm)
 * @tparam VertexPolicy a function object that compares two vertices and is
 * used for initial sorting of vertices and for dynamic version of the algorithm
 *
 * @param conn adjacency matrix
 * @param max_steps maximum number of steps if a positive integer value is
 * given, otherwise the number of steps is unlimited
 * @param Tlimit (only relevant for MCQDStrategy) on what percentage of steps to
 * perform computationally expensive re-sorting of vertices (see original paper
 * MATCH, 2007)
 *
 * @return a clique represented by a vector of node indexes
 */
template <template <typename> typename SearchStrategy = MCQDStrategy,
          typename T = DecreasingDegreesVertexPolicy>
  requires MCQConcept<SearchStrategy, T> || MCQDConcept<SearchStrategy, T>
auto find_maximum_clique(const helper::Array2d<bool> &conn,
                         const std::optional<std::size_t> max_steps = {},
                         const double Tlimit = 0.025) {
  if constexpr (MCQConcept<SearchStrategy, T>)
    return SearchStrategy<T>{conn, max_steps}.run();
  else
    return SearchStrategy<T>{conn, max_steps, Tlimit}.run();
}

/**
 * Find a maximum weight clique, i.e., a fully connected subgraph with the
 * highest sum of weights over its vertices, in an undirected vertex-weighted
 * graph.
 *
 * If you use this algorithm, please cite:
 *
 * Janez Konc and Dusanka Janezic. An improved branch and bound algorithm for
 * the maximum clique problem. MATCH Commun. Math. Comput. Chem., 2007, 58,
 * 569-590.
 *
 * More information at: http://insilab.org/maxclique
 *
 * @note the input graph's weights must be ONLY negative or ONLY positive
 * numbers, otherwise this algorithm won't give correct result
 *
 * @tparam SearchStrategy type of search to perform, one of the following
 * (MCQWStrategy or MCQDWStrategy, the last one uses dynamic upper bounds
 * algorithm) Parameters for SearchStrategy are the following:
 * @param conn adjacency matrix
 * @param weights a vector of weights for vertices, represented with floating
 * point values (double)
 * @param max_steps maximum number of steps if a positive integer value is
 * given, otherwise the number of steps is unlimited
 * @param Tlimit (only relevant for MCQDWStrategy) on what percentage of steps
 * to perform computationally expensive re-sorting of vertices (see original
 * paper MATCH, 2007)
 * @tparam VertexPolicy a function object that compares two vertices and is
 * used for initial sorting of vertices and for dynamic version of the algorithm
 * @tparam WeightPolicy a function object that changes the algorithm to search
 * for highest weight cliques or for lowest weight cliques in the input graph
 *
 * @return a pair, in which the first element is a clique, represented by a
 * vector of node indexes, and the second element is the clique's weight
 */
template <template <typename, typename> typename SearchStrategy = MCQDWStrategy,
          typename T = HighestWeightPolicy,
          typename S = DecreasingDegreesVertexPolicy>
  requires MCQWConcept<SearchStrategy, T, S> ||
           MCQWExpConcept<SearchStrategy, T, S> ||
           MCQDWExpConcept<SearchStrategy, T, S> ||
           MCQDWConcept<SearchStrategy, T, S> ||
           NaiveMCQWConcept<SearchStrategy, T, S>
auto find_maximum_weight_clique(const helper::Array2d<bool> &conn,
                                const std::vector<glib::weight_t> &weights,
                                const std::optional<std::size_t> max_steps = {},
                                const double Tlimit = 0.025) {
  if constexpr (MCQWConcept<SearchStrategy, T, S> ||
                MCQWExpConcept<SearchStrategy, T, S> ||
                MCQDWExpConcept<SearchStrategy, T, S> ||
                NaiveMCQWConcept<SearchStrategy, T, S>)
    return SearchStrategy<T, S>{conn, weights, max_steps}.run();
  else
    return SearchStrategy<T, S>{conn, weights, max_steps, Tlimit}.run();
}

/**
 * Find a set of N cliques, each with size equal to k, in an undirected graph.
 *
 * @tparam SearchStrategy type of search to perform, one of the following
 * (KCQStrategy or KCQDStrategy, the last one uses dynamic upper bounds
 * algorithm) Parameters for SearchStrategy are the following:
 * @param conn adjacency matrix
 * @param k cliques found will have size == k (if k is set to the size
 * of a maximum clique, the function returns all maximum cliques in the graph)
 * @param n_cliques how many cliques that satisfy the size == k criterion to
 * output, if value not given, output as many such cliques as there exist
 * @param max_steps maximum number of steps if a positive integer value is
 * given, otherwise the number of steps is unlimited
 * @param Tlimit (only relevant for KCQDStrategy) on what percentage of steps to
 * perform computationally expensive re-sorting of vertices (see original paper
 * MATCH, 2007)
 * @tparam VertexPolicy a function object that compares two vertices and is
 * used for initial sorting of vertices and for dynamic version of the algorithm
 *
 * @return a vector of cliques, each represented as a vector of node indexes
 */
template <template <typename> typename SearchStrategy = KCQDStrategy,
          typename T = DecreasingDegreesVertexPolicy>
  requires KCQConcept<SearchStrategy, T> || KCQDConcept<SearchStrategy, T>
auto find_n_k_cliques(const helper::Array2d<bool> &conn, const node_idx_t k,
                      const std::optional<std::size_t> n_cliques = {},
                      const std::optional<std::size_t> max_steps = {},
                      const double Tlimit = 0.025) {
  if constexpr (KCQConcept<SearchStrategy, T>)
    return SearchStrategy<T>{conn, k, n_cliques, max_steps}.run();
  else
    return SearchStrategy<T>{conn, k, n_cliques, max_steps, Tlimit}.run();
}

/**
 * Find a set of N cliques with highest weights, each with size equal to k, in
 * an undirected vertex-weigthed graph.
 *
 * @note the input graph's weights must be ONLY negative or ONLY positive
 * numbers, otherwise this algorithm won't give correct result
 *
 * @tparam SearchStrategy type of search to perform, one of the following
 * (KCQWStrategy or KCQDWStrategy, the last one uses dynamic upper bounds
 * algorithm) Parameters for SearchStrategy are the following:
 * @param conn adjacency matrix
 * @param weights a vector of weights for vertices, represented with floating
 * point values (double)
 * @param k cliques found will have size == k (if k is set to the known size
 * of a maximum clique, the function returns N highest weighted maximum cliques
 * in the graph)
 * @param n_cliques N highest-weight cliques that satisfy the size == k to
 * output, if value not given output as many such cliques as there exist
 * @param max_steps maximum number of steps if a positive integer value is
 * given, otherwise the number of steps is unlimited
 * @param Tlimit (only relevant for KCQDWStrategy) on what percentage of steps
 * to perform computationally expensive re-sorting of vertices (see original
 * paper MATCH, 2007)
 * @tparam VertexPolicy a function object that compares two vertices and is
 * used for initial sorting of vertices and for dynamic version of the algorithm
 * @tparam WeightPolicy a struct that changes the algorithm to search for
 * highest weight cliques or for lowest weight cliques in the input graph
 *
 * @return a pair, in which the first element is a vector of weighted cliques,
 * each represented by a vector of node indexes, sorted from the highest to the
 * lowest weight clique, and the second element is a vector of corresponding
 * clique weights
 */
template <template <typename, typename> typename SearchStrategy = KCQDWStrategy,
          typename T = HighestWeightPolicy,
          typename S = DecreasingDegreesIncreasingWeightsVertexPolicy>
  requires KCQWConcept<SearchStrategy, T, S> ||
           KCQDWConcept<SearchStrategy, T, S> ||
           NaiveKCQWConcept<SearchStrategy, T, S>
auto find_n_highest_weight_k_cliques(
    const helper::Array2d<bool> &conn,
    const std::vector<glib::weight_t> &weights, const node_idx_t k,
    const std::optional<std::size_t> n_cliques = {},
    const std::optional<std::size_t> max_steps = {},
    const double Tlimit = 0.025) {
  if constexpr (KCQWConcept<SearchStrategy, T, S> ||
                NaiveKCQWConcept<SearchStrategy, T, S>)
    return SearchStrategy<T, S>{conn, weights, k, n_cliques, max_steps}.run();
  else
    return SearchStrategy<T, S>{conn, weights, k, n_cliques, max_steps, Tlimit}
        .run();
}

} // namespace insilab::glib::algorithms
