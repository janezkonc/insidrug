#pragma once

#include "vertices.hpp"
#include "vertices_ops.hpp"

namespace insilab::glib::algorithms::internals {

template <typename SearchStrategy, WeightPolicyConcept WeightPolicy>
auto run_weighted_KCQW(SearchStrategy &search_strategy, const auto &conn,
                       const auto &weights,
                       VertexPolicyConcept auto vertex_policy) {
  Vertices<VertexWeight> V{conn.get_szi()};
  init_vertices(V, conn);
  sort_vertices(V, vertex_policy(V, conn));
  // init_clq_weight_upper_bounds_TEST<WeightPolicy>(V, conn, weights);
  // init_clq_size_upper_bounds(V);
  // init_clq_size_upper_bounds_TEST(V, conn);
  search_strategy.color_sort(V);
  search_strategy.expand(V);
  return search_strategy.get_result();
}

template <typename SearchStrategy, WeightPolicyConcept WeightPolicy>
auto run_weighted_MCQW(SearchStrategy &search_strategy, const auto &conn,
                       const auto &weights,
                       VertexPolicyConcept auto vertex_policy) {
  Vertices<VertexWeight> V{conn.get_szi()};
  init_vertices(V, conn);
  sort_vertices(V, vertex_policy(V, conn));
  // init_clq_weight_upper_bounds_TEST<WeightPolicy>(V, conn, weights);
  search_strategy.color_sort(V);
  search_strategy.expand(V);
  return search_strategy.get_result();
}

template <typename SearchStrategy>
auto run_unweighted(SearchStrategy &search_strategy, const auto &conn,
                    VertexPolicyConcept auto vertex_policy) {
  Vertices<VertexDegree> V{conn.get_szi()};
  init_vertices(V, conn);
  sort_vertices(V, vertex_policy(V, conn));
  init_clq_size_upper_bounds(V, vertex_policy.get_degrees());
  // init_clq_size_upper_bounds_TEST(V, conn);
  // search_strategy.color_sort(V);
  search_strategy.expand(V);
  return search_strategy.get_result();
}

} // namespace insilab::glib::algorithms::internals
