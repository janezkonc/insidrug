#include "clique.hpp"
#include <iostream>

namespace insilab::glib::algorithms::internals {

void Clique::print(const std::vector<glib::weight_t> &weights) {
  std::clog << "CLIQUE SIZE=" << this->vertices.size()
            << " WEIGHT=" << this->weight << " ";
  for (auto i{0uz}; i < this->vertices.size(); ++i) {
    if (!weights.empty()) {
      std::clog << this->vertices[i] << "(" << weights.at(this->vertices[i])
                << ") ";
    } else {
      std::clog << this->vertices[i] << " ";
    }
  }
  std::clog << std::endl;
}

} // namespace insilab::glib::algorithms::internals
