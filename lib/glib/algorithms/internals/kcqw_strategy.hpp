#pragma once

#include "best_cliques.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "maxclique_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <queue>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class KCQWStrategy : public MaxCliqueStrategy {
protected:
  struct comp {
    bool operator()(const Clique &lhs, const Clique &rhs) const {
      return WeightPolicy::greater(lhs.get_weight(), rhs.get_weight());
    }
  };
  using Queue = std::priority_queue<Clique, std::vector<Clique>, comp>;
  const std::vector<glib::weight_t> &__weights;
  BestCliques<Queue> __best_cliques{};
  const std::size_t __K; /* size of cliques that we wish to find */

  friend VertexPolicy;
  VertexPolicy __vertex_policy; // must be declared AFTER __weights

  auto get_lowest_weight(const BestCliques<Queue> &q) {
    return q.full() ? q.top().get_weight()
                    : WeightPolicy::lowest_possible_weight();
  }

public:
  KCQWStrategy(const helper::Array2d<bool> &conn,
               const std::vector<glib::weight_t> &weights, const node_idx_t k,
               const std::optional<std::size_t> n_cliques = {},
               const std::optional<std::size_t> max_steps = {})
      : MaxCliqueStrategy::MaxCliqueStrategy(conn, max_steps),
        __weights(weights), __best_cliques(n_cliques.value_or(
                                std::numeric_limits<std::size_t>::max())),
        __K(k), __vertex_policy(*this) {
    assert(std::ranges::all_of(weights, [](auto w) { return w > 0; }) ||
           std::ranges::all_of(weights, [](auto w) { return w < 0; }));
  }

  auto run() {
    return run_weighted_KCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, __vertex_policy);
  }

  auto get_result() const {
    std::vector<std::vector<node_idx_t>> cliques;
    std::ranges::transform(__best_cliques.get_cliques(),
                           std::back_inserter(cliques), &Clique::get_vertices);
    std::vector<glib::weight_t> weights;
    std::ranges::transform(__best_cliques.get_cliques(),
                           std::back_inserter(weights), &Clique::get_weight);

    return std::pair{cliques, weights};
  }

  template <typename Vertices> void color_sort(Vertices &R) {
    node_idx_t maxno{1};
    assert(this->C.size() >= 3);
    this->C[1].rewind();
    this->C[2].rewind();
    // for each color class, store the maximum weight of its vertices
    std::vector<glib::weight_t> cc_max_weights(
        this->C.size(), WeightPolicy::lowest_possible_weight());
    // partition vertices into color classes (each color class contains vertices
    // that are not connected)
    for (node_idx_t i = 0; i < R.size(); ++i) {
      const node_idx_t pi = R[i].get_vertex_no();
      node_idx_t k{1};
      while (this->cut1(pi, this->C[k]))
        ++k;
      if (k > maxno) {
        assert(k == maxno + 1);
        maxno = k;
        assert(this->C.size() > maxno + 1);
        this->C[maxno + 1].rewind();
        assert(cc_max_weights.size() > maxno + 1);
        assert(cc_max_weights[maxno + 1] ==
               WeightPolicy::lowest_possible_weight());
      }
      // each color class is assigned the maximum weight of any of its vertices
      cc_max_weights[k] = WeightPolicy::max(__weights[pi], cc_max_weights[k]);
      this->C[k].push(pi);
      R[i].set_clq_size_upper_bound(k);
    }
    // determine min_k from weights (color class number below which vertices
    // cannot be used to extend the growing clique)
    //
    // NOTE: THE FOLLOWING IS OBSOLETE AS WeightPolicy::lowest_possible_weight()
    // NOW RETURNS 0 (instead of + or - largest number) see issue #136
    // using weight_policy.min we protect against integer overflows that can
    // occur from the expression 'get_lowest_weight(this->__best_cliques) -
    // this->Q.get_weight()':
    //
    // double d = std::numeric_limits<double>::lowest() - 1; // OK ... negative
    // int i = std::numeric_limits<int>::lowest() - 1; // WRONG ... positive
    // double d = std::numeric_limits<double>::max() + 1; // OK ... postive
    // int i = std::numeric_limits<int>::max() + 1; // WRONG ... negative
    const glib::weight_t diff_weight_q_qmax =
        get_lowest_weight(this->__best_cliques) - this->Q.get_weight();
    node_idx_t min_k{1};
    glib::weight_t cum_weight{cc_max_weights[min_k]};
    while (min_k < maxno &&
           !WeightPolicy::greater(cum_weight, diff_weight_q_qmax)) {
      cum_weight += cc_max_weights[++min_k];
    }
    // also determine min_k from clique size
    // compared to MCQ where min_k = QMAX - Q + 1, here we don't add one in the
    // equation, which is because in the first case, we don't want to find more
    // cliques of size QMAX, while in the second case, we do want cliques of
    // size K
    const node_idx_t min_k_size =
        std::max(static_cast<signed_node_idx_t>(__K - this->Q.size()),
                 static_cast<signed_node_idx_t>(1));
    // select the greater of min_k's (from size or weight) to give better
    // prunning
    min_k = std::max(min_k, min_k_size);
    assert(min_k >= 1);
    // copy vertices that are not going to be used to the beginning of R
    node_idx_t j{};
    for (node_idx_t i = 0; i < R.size(); ++i) {
      if (R[i].get_clq_size_upper_bound() < min_k) {
        R[j++].set_vertex_no(R[i].get_vertex_no());
      }
    }
    if (j > 0)
      R[j - 1].set_clq_size_upper_bound(0);
    // for vertices that are going to be used to extend the growing clique, set
    // their weights and degrees (for pruning the search tree)
    for (node_idx_t k = min_k; k <= maxno; ++k) {
      // if there are so many color classes that a clique could grow beyond K,
      // discard color classes with worst weights
      assert(k - min_k_size + 1 > 0);
      const node_idx_t discard = k - min_k_size + 1;
      assert(discard > 0 && discard <= k);
      std::ranges::partial_sort(
          cc_max_weights.begin() + 1, cc_max_weights.begin() + discard,
          cc_max_weights.begin() + k + 1, typename WeightPolicy::less{});
      const auto cum_weight_k =
          std::accumulate(cc_max_weights.begin() + discard,
                          cc_max_weights.begin() + k + 1, glib::weight_t{});
      // The above lines work like this: let's say we want to get cliques with
      // size of 3 and we have 4 color classes, each with its own minimum
      // weight. We know clique that we want will only have 3 vertices (we are
      // not interested in 4-vertex cliques at all), so we discard the color
      // class with the worst (the highest) minimum weight. Now we end up with
      // sum of weights from 3 best color classes (with best weights) that will
      // give the tightest bound for 3-cliques in expand's condition. This
      // should be beneficial even if k is the size of the maximum clique,
      // because the number of color classes may still be greater than k in this
      // case.
      for (node_idx_t i = 0; i < this->C[k].size(); ++i) {
        assert(j < R.size());
        auto &v = R[j++];
        v.set_clq_weight_upper_bound(cum_weight_k);
        v.set_vertex_no(this->C[k].at(i));
        v.set_clq_size_upper_bound(k);
      }
    }
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      if (this->Q.size() + R.back().get_clq_size_upper_bound() > __K - 1 &&
          WeightPolicy::greater(this->Q.get_weight() +
                                    R.back().get_clq_weight_upper_bound(),
                                get_lowest_weight(__best_cliques))) {
        this->Q.push(R.back().get_vertex_no());
        this->Q.increase_weight(__weights[R.back().get_vertex_no()]);
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size() && this->Q.size() < __K) {
          ++this->pk;
          this->color_sort(Rp);
          this->expand(Rp);
        } else if (this->Q.size() == __K &&
                   WeightPolicy::greater(this->Q.get_weight(),
                                         get_lowest_weight(__best_cliques))) {
          __best_cliques.insert(this->Q);
        }
        this->Q.pop();
        this->Q.decrease_weight(__weights[R.back().get_vertex_no()]);
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
