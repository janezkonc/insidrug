#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "maxclique_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class MCQWStrategy : public MaxCliqueStrategy {
protected:
  const std::vector<glib::weight_t> &__weights;
  friend VertexPolicy;
  VertexPolicy __vertex_policy; // must be declared AFTER __weights

public:
  MCQWStrategy(const helper::Array2d<bool> &conn,
               const std::vector<glib::weight_t> &weights,
               const std::optional<std::size_t> max_steps = {})
      : MaxCliqueStrategy::MaxCliqueStrategy(conn, max_steps),
        __weights(weights), __vertex_policy(*this) {
    assert(std::ranges::all_of(weights, [](auto w) { return w > 0; }) ||
           std::ranges::all_of(weights, [](auto w) { return w < 0; }));
  }

  auto run() {
    return run_weighted_MCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, __vertex_policy);
  }

  auto get_result() const {
    // std::cout << "Number of steps (MCQW) = " << this->get_steps_count() << std::endl;
    return std::pair{this->QMAX.get_vertices(), this->QMAX.get_weight()};
  }
  template <typename Vertices> void color_sort(Vertices &R) {
    node_idx_t maxno{1};
    assert(this->C.size() >= 3);
    this->C[1].rewind();
    this->C[2].rewind();
    // for each color class, store the maximum weight of its vertices
    std::vector<glib::weight_t> cc_max_weights(
        this->C.size(), WeightPolicy::lowest_possible_weight());
    // partition vertices into color classes (each color class contains vertices
    // that are not connected)
    for (node_idx_t i = 0; i < R.size(); ++i) {
      const node_idx_t pi = R[i].get_vertex_no();
      node_idx_t k{1};
      while (this->cut1(pi, this->C[k]))
        ++k;
      if (k > maxno) {
        assert(k == maxno + 1);
        maxno = k;
        assert(this->C.size() > maxno + 1);
        this->C[maxno + 1].rewind();
        assert(cc_max_weights.size() > maxno + 1);
        assert(cc_max_weights[maxno + 1] ==
               WeightPolicy::lowest_possible_weight());
      }
      // each color class is assigned the maximum weight of any of its vertices
      cc_max_weights[k] = WeightPolicy::max(__weights[pi], cc_max_weights[k]);
      this->C[k].push(pi);
      R[i].set_clq_size_upper_bound(k);
    }
    // determine min_k (color class number below which vertices cannot be used
    // to extend the growing clique)

    // caveat: this probably does not work if weights are negative and
    // HighestWeightPolicy is used... see kcqw_strategy for potential solution
    // (using weight_policy.min), although this should be done differently
    // here by seeting QMAX's initial weight for
    // weight_policy.lowest_possible_weight()... but how?
    const glib::weight_t diff_weight_q_qmax =
        this->QMAX.get_weight() - this->Q.get_weight();
    node_idx_t min_k{1};
    glib::weight_t cum_weight{cc_max_weights[min_k]};
    while (min_k < maxno &&
           !WeightPolicy::greater(cum_weight, diff_weight_q_qmax)) {
      cum_weight += cc_max_weights[++min_k];
    }
    // copy vertices that are not going to be used to the beginning of R
    node_idx_t j{};
    for (node_idx_t i = 0; i < R.size(); ++i) {
      if (R[i].get_clq_size_upper_bound() < min_k) {
        R[j++].set_vertex_no(R[i].get_vertex_no());
      }
    }
    // for vertices that are going to be used, set their weights (these are used
    // for pruning the search tree)
    glib::weight_t cum_weight_k =
        std::accumulate(cc_max_weights.begin() + 1,
                        cc_max_weights.begin() + min_k, glib::weight_t{});
    for (node_idx_t k = min_k; k <= maxno; ++k) {
      cum_weight_k += cc_max_weights[k];
      for (node_idx_t i = 0; i < this->C[k].size(); ++i) {
        assert(j < R.size());
        auto &v = R[j++];
        v.set_clq_weight_upper_bound(cum_weight_k);
        v.set_vertex_no(this->C[k].at(i));
      }
    }
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      if (WeightPolicy::greater(this->Q.get_weight() +
                                    R.back().get_clq_weight_upper_bound(),
                                this->QMAX.get_weight())) {
        this->Q.push(R.back().get_vertex_no());
        this->Q.increase_weight(__weights[R.back().get_vertex_no()]);
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size()) {
          ++this->pk;
          this->color_sort(Rp);
          this->expand(Rp);
        } else if (WeightPolicy::greater(this->Q.get_weight(),
                                         this->QMAX.get_weight())) {
          this->QMAX = this->Q;
        }
        this->Q.pop();
        this->Q.decrease_weight(__weights[R.back().get_vertex_no()]);
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
