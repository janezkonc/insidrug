#pragma once

#include "glib/graph.hpp"
#include "vertex_degree.hpp"

namespace insilab::glib::algorithms::internals {
  
class VertexWeight : public VertexDegree {
  glib::weight_t w{};

public:
  void set_clq_weight_upper_bound(const auto weight) { w = weight; }
  auto get_clq_weight_upper_bound() const { return w; }
};

} // namespace insilab::glib::algorithms::internals
