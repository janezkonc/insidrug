#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "maxclique_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class MCQWExpStrategy : public MaxCliqueStrategy {
protected:
  const std::vector<glib::weight_t> &__weights;
  friend VertexPolicy;
  VertexPolicy __vertex_policy; // must be declared AFTER __weights

public:
  MCQWExpStrategy(const helper::Array2d<bool> &conn,
                  const std::vector<glib::weight_t> &weights,
                  const std::optional<std::size_t> max_steps = {})
      : MaxCliqueStrategy::MaxCliqueStrategy(conn, max_steps),
        __weights(weights), __vertex_policy(*this) {
    assert(std::ranges::all_of(weights, [](auto w) { return w > 0; }) ||
           std::ranges::all_of(weights, [](auto w) { return w < 0; }));
  }

  auto run() {
    return run_weighted_MCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, __vertex_policy);
  }

  auto get_result() const {
    std::cout << "Number of steps (Exp) = " << this->get_steps_count()
              << std::endl;
    return std::pair{this->QMAX.get_vertices(), this->QMAX.get_weight()};
  }

  void recolor_vertices(std::vector<glib::weight_t> &max_weights,
                        std::vector<node_idx_t> &max_vertices,
                        const node_idx_t first, const node_idx_t last) {

    // const auto sum_max_weights =
    //     std::accumulate(max_weights.begin(), max_weights.end(), 0);

    bool possible = true;
    // int iteration = 1;
    while (possible) {
      possible = false;

      for (auto k = first; k >= last; --k) {
        for (auto k2 = k - 1; k2 >= last; --k2) {
          if (this->C[k2].empty())
            continue;
          // can we hide the k2-th color class's max weight in k-th color
          // class?
          if (WeightPolicy::greater(max_weights[k2], max_weights[k]))
            continue;
          // try move the vertex from k2 -> k
          const auto i = max_vertices[k2];
          const auto pi = this->C[k2].at(i);
          if (this->cut1(pi, this->C[k]))
            continue;
          possible = true;
          // p is not adjacent to any vertex in color class k
          // so push p to the k-th color class
          this->C[k].push(pi);
          // and remove p from k2-th color class
          this->C[k2].erase(i);
          // update k2-th color class's max weight
          max_weights[k2] = 0; // if k2-th color class is empty
          for (auto i = 0; i < this->C[k2].size(); ++i) {
            const auto vi = this->C[k2].at(i);
            const auto wi = this->__weights[vi];
            if (WeightPolicy::greater(wi, max_weights[k2])) {
              max_weights[k2] = wi;
              max_vertices[k2] = i;
            }
          }
          // k-th color class's max weight needs NO updating
        }
      }
      //   const auto sum_max_weights_new =
      //       std::accumulate(max_weights.begin(), max_weights.end(), 0);
      //   if (sum_max_weights != sum_max_weights_new) {
      //     std::cout << "Iteration " << iteration++
      //               << " Upper bound = " << sum_max_weights
      //               << " new upper bound = " << sum_max_weights_new <<
      //               std::endl;
      //   }
    }
  }

  template <typename Vertices>
  void color_sort(Vertices &R, const bool recolor = true) {

    // put vertices in color classes
    std::vector<glib::weight_t> max_weights(
        R.size() + 1, WeightPolicy::lowest_possible_weight());
    std::vector<node_idx_t> max_vertices(R.size() + 1);

    node_idx_t maxno{1};
    assert(this->C.size() >= 3);
    this->C[1].rewind();
    this->C[2].rewind();

    // partition vertices into color classes (each color class contains vertices
    // that are not connected)
    for (node_idx_t i = 0; i < R.size(); ++i) {
      const node_idx_t pi = R[i].get_vertex_no();
      node_idx_t k{1};
      while (this->cut1(pi, this->C[k]))
        ++k;

      if (k > maxno) {
        assert(k == maxno + 1);
        maxno = k;
        assert(this->C.size() > maxno + 1);
        this->C[maxno + 1].rewind();
        assert(max_weights.size() > maxno + 1);
        assert(max_weights[maxno + 1] ==
               WeightPolicy::lowest_possible_weight());
      }
      this->C[k].push(pi);
      R[i].set_clq_size_upper_bound(k);
      // update maximum weight of k-th color class
      if (WeightPolicy::greater(this->__weights[pi], max_weights[k])) {
        max_weights[k] = this->__weights[pi];
        max_vertices[k] = this->C[k].size() - 1;
      }
    }

    // determine min_k (color class number below which vertices cannot be used
    // to extend the growing clique)

    // caveat: this probably does not work if weights are negative and
    // HighestWeightPolicy is used... see kcqw_strategy for potential solution
    // (using weight_policy.min), although this should be done differently
    // here by seeting QMAX's initial weight for
    // weight_policy.lowest_possible_weight()... but how?
    const glib::weight_t diff_weight_q_qmax =
        this->QMAX.get_weight() - this->Q.get_weight();
    node_idx_t min_k{1};
    glib::weight_t cum_weight{max_weights[min_k]};
    while (min_k < maxno &&
           !WeightPolicy::greater(cum_weight, diff_weight_q_qmax)) {
      cum_weight += max_weights[++min_k];
    }

    // try to put as many as possible vertices with the maximum weight in a
    // color class to color classes with even higher maximum weights
    if (recolor) {
      recolor_vertices(max_weights, max_vertices, min_k - 1, 1);
      recolor_vertices(max_weights, max_vertices, maxno, min_k);
      //   this->C.sort_decreasing_size(min_k, maxno); // THIS DOES NOT WORK YET
      /* THIS BELOW IS A NEW IDEA TO SORT WITHIN EACH COLOR CLASS BY WEIGHTS -
       * SELECT THOSE WITH HIGHER WEIGHTS FIRST */
      //   for (node_idx_t k = min_k; k <= maxno; ++k) {
      //     C[k].sort_increasing_weight(this->__weights,
      //     WeightPolicy::greater);
      //   }
    }
    // copy vertices that are not going to be used to the beginning of R
    node_idx_t j{};
    for (node_idx_t i = 0; i < R.size(); ++i) {
      if (R[i].get_clq_size_upper_bound() < min_k) {
        R[j++].set_vertex_no(R[i].get_vertex_no());
      }
    }

    // for vertices that are going to be used, set their weights (these are
    // used for pruning the search tree)
    glib::weight_t cum_weight_k = std::accumulate(
        max_weights.begin() + 1, max_weights.begin() + min_k, glib::weight_t{});
    for (node_idx_t k = min_k; k <= maxno; ++k) {
      auto curr_max_weight_k = glib::weight_t{};
      for (node_idx_t i = 0; i < this->C[k].size(); ++i) {
        assert(j < R.size());
        auto &v = R[j++];
        const auto pi = this->C[k].at(i);
        curr_max_weight_k =
            WeightPolicy::max(curr_max_weight_k, this->__weights[pi]);
        v.set_clq_weight_upper_bound(cum_weight_k + curr_max_weight_k);
        // v.set_clq_size_upper_bound(k); // this is not necessary
        v.set_vertex_no(pi);
      }
      cum_weight_k += max_weights[k];
    }
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      if (WeightPolicy::greater(this->Q.get_weight() +
                                    R.back().get_clq_weight_upper_bound(),
                                this->QMAX.get_weight())) {
        this->Q.push(R.back().get_vertex_no());
        this->Q.increase_weight(__weights[R.back().get_vertex_no()]);
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size()) {
          ++this->pk;
          this->color_sort(Rp);
          this->expand(Rp);
        } else if (WeightPolicy::greater(this->Q.get_weight(),
                                         this->QMAX.get_weight())) {
          this->QMAX = this->Q;
        }
        this->Q.pop();
        this->Q.decrease_weight(__weights[R.back().get_vertex_no()]);
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
