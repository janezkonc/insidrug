#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "mcqw_exp_strategy.hpp"
#include "step_count.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class MCQDWExpStrategy : public MCQWExpStrategy<WeightPolicy, VertexPolicy> {
protected:
  StepCount __S;

public:
  MCQDWExpStrategy(const helper::Array2d<bool> &conn,
                   const std::vector<glib::weight_t> &weights,
                   const std::optional<std::size_t> max_steps = {},
                   const double Tlimit = 0.025)
      : MCQWExpStrategy<WeightPolicy, VertexPolicy>::MCQWExpStrategy(
            conn, weights, max_steps),
        __S(conn.get_szi(), Tlimit) {}

  auto run() {
    return run_weighted_MCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, this->__vertex_policy);
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    __S.init_steps_level();
    while (R.size()) {
      if (WeightPolicy::greater(this->Q.get_weight() +
                                    R.back().get_clq_weight_upper_bound(),
                                this->QMAX.get_weight())) {
        this->Q.push(R.back().get_vertex_no());
        this->Q.increase_weight(this->__weights[R.back().get_vertex_no()]);
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size()) {
          ++this->pk;
          bool recolor = false;
          __S.do_steps_level_lt_tlimit(this->pk, [&]() {
            sort_vertices(Rp, this->__vertex_policy(Rp, this->__conn));
            recolor = true;
          });
          this->color_sort(Rp, recolor);
          __S.increase_steps_level();
          this->expand(Rp);
          __S.decrease_level();
        } else if (WeightPolicy::greater(this->Q.get_weight(),
                                         this->QMAX.get_weight())) {
          this->QMAX = this->Q;
        }
        this->Q.pop();
        this->Q.decrease_weight(this->__weights[R.back().get_vertex_no()]);
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
