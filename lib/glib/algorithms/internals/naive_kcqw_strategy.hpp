#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "kcqw_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class NaiveKCQWStrategy : public KCQWStrategy<WeightPolicy, VertexPolicy> {
public:
  NaiveKCQWStrategy(const helper::Array2d<bool> &conn,
                    const std::vector<glib::weight_t> &weights,
                    const node_idx_t k,
                    const std::optional<std::size_t> n_cliques = {},
                    const std::optional<std::size_t> max_steps = {})
      : KCQWStrategy<WeightPolicy, VertexPolicy>::KCQWStrategy(
            conn, weights, k, n_cliques, max_steps) {}

  auto run() {
    return run_weighted_KCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, this->__vertex_policy);
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      this->Q.push(R.back().get_vertex_no());
      this->Q.increase_weight(this->__weights[R.back().get_vertex_no()]);
      Vertices Rp(R.size());
      this->cut2(R, Rp);
      if (Rp.size() && this->Q.size() < this->__K) {
        ++this->pk;
        this->expand(Rp);
      } else if (this->Q.size() == this->__K &&
                 WeightPolicy::greater(
                     this->Q.get_weight(),
                     this->get_lowest_weight(this->__best_cliques))) {
        this->__best_cliques.insert(this->Q);
      }
      this->Q.pop();
      this->Q.decrease_weight(this->__weights[R.back().get_vertex_no()]);
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
