#pragma once

#include "clique.hpp"
#include <limits>
#include <queue>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <typename Queue> class BestCliques {
private:
  std::size_t maxsz;
  Queue QMAXES{};

public:
  BestCliques() = delete;
  BestCliques(const std::size_t maxsz) : maxsz(maxsz) {}

  /// If maxsz hasn't been reached, insert new clique in queue, else remove the
  /// worst clique from queue and then insert the new one.
  void insert(const Clique &clique) {
    if (QMAXES.size() == maxsz)
      QMAXES.pop();      // remove worst clique in queue
    QMAXES.push(clique); // add a new clique to queue
  }

  auto &top() const { return QMAXES.top(); }
  auto empty() const { return QMAXES.empty(); }
  auto full() const { return QMAXES.size() == maxsz; }

  /**
   * Get all cliques in the queue.
   *
   * @note the order of cliques in queue is reversed so we have to reverse it
   * again
   * @return a vector of cliques where the first clique is the best one
   * (according to criteria defined outside this class)
   */
  std::vector<Clique> get_cliques() const {
    Queue qmaxes(QMAXES);
    std::vector<Clique> result;
    while (!qmaxes.empty()) {
      result.push_back(qmaxes.top());
      qmaxes.pop();
    }
    std::ranges::reverse(result);
    return result;
  }
};

} // namespace insilab::glib::algorithms::internals
