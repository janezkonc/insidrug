#pragma once

#include "glib/graph.hpp"
#include <vector>

namespace insilab::glib::algorithms::internals {

class Clique {
public:
  std::vector<node_idx_t> vertices{};
  glib::weight_t weight{};

public:
  auto get_weight() const { return weight; }
  void set_weight(const auto ene) { weight = ene; }
  const std::vector<node_idx_t> &get_vertices() const { return vertices; }
  void increase_weight(const auto e) { weight += e; }
  void decrease_weight(const auto e) { weight -= e; }
  void push(const node_idx_t vertex) { vertices.push_back(vertex); }
  void pop() { vertices.pop_back(); }
  auto size() const { return vertices.size(); }
  void print(const std::vector<glib::weight_t> & = {});
};

} // namespace insilab::glib::algorithms::internals
