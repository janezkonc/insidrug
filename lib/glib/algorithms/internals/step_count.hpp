#pragma once

#include <queue>
#include <vector>

namespace insilab::glib::algorithms::internals {

class StepCount {

  class StepsOnLevel {
    std::size_t i1, i2;

  public:
    StepsOnLevel() : i1(0), i2(0) {}
    void set_i1(const auto ii) { i1 = ii; }
    auto get_i1() const { return i1; }
    void set_i2(const auto ii) { i2 = ii; }
    auto get_i2() const { return i2; }
    void inc_i1() { ++i1; }
  };

  std::vector<StepsOnLevel> __S;
  const double __Tlimit;
  std::size_t __level{1};

public:
  StepCount(const std::size_t sz, const double Tlimit)
      : __S(sz + 2), __Tlimit(Tlimit) {
    for (auto &item : __S) {
      item.set_i1(0);
      item.set_i2(0);
    }
  }
  void init_steps_level() {
    assert(__S.size() > __level);
    __S[__level].set_i1(__S[__level].get_i1() + __S[__level - 1].get_i1() -
                        __S[__level].get_i2());
    __S[__level].set_i2(__S[__level - 1].get_i1());
  }
  void do_steps_level_lt_tlimit(const std::size_t pk, auto fn) {
    if ((double)__S[__level].get_i1() / pk < __Tlimit) {
      fn();
    }
  }
  void increase_steps_level() { __S[__level++].inc_i1(); }
  void decrease_level() { --__level; }
};

} // namespace insilab::glib::algorithms::internals
