#pragma once

#include "glib/graph.hpp"

namespace insilab::glib::algorithms::internals {

class VertexDegree {
  node_idx_t i{}, d{};

public:
  void set_vertex_no(const auto ii) { i = ii; }
  auto get_vertex_no() const { return i; }

  /// @brief This function is used to store two different things: upper bound to
  /// the size of clique OR degree of a vertex.
  /// @param dd
  void set_clq_size_upper_bound(const auto dd) { d = dd; }
  auto get_clq_size_upper_bound() const { return d; }
};

} // namespace insilab::glib::algorithms::internals
