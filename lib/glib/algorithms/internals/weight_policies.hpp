#pragma once

#include "glib/graph.hpp"

namespace insilab::glib::algorithms::internals {

template <typename T>
concept WeightPolicyConcept =
    requires(T policy, const glib::weight_t &a, const glib::weight_t &b) {
      { policy.max(a, b) } -> std::same_as<glib::weight_t>;
      { policy.min(a, b) } -> std::same_as<glib::weight_t>;
      { policy.lowest_possible_weight() } -> std::same_as<glib::weight_t>;
      { policy.highest_possible_weight() } -> std::same_as<glib::weight_t>;
      { policy.greater(a, b) } -> std::same_as<bool>;
      { typename T::less{}(a, b) } -> std::same_as<bool>;
    };

struct HighestWeightPolicy {
  static glib::weight_t max(const glib::weight_t &a, const glib::weight_t &b) {
    return std::max(a, b);
  }
  static glib::weight_t min(const glib::weight_t &a, const glib::weight_t &b) {
    return std::min(a, b);
  }
  static glib::weight_t lowest_possible_weight() { return {}; }
  static glib::weight_t highest_possible_weight() {
    return std::numeric_limits<glib::weight_t>::max();
  }
  static bool greater(const glib::weight_t &a, const glib::weight_t &b) {
    return a > b;
  }
  struct less {
    bool operator()(const glib::weight_t &a, const glib::weight_t &b) const {
      return a < b;
    }
  };
};

struct LowestWeightPolicy {
  static glib::weight_t max(const glib::weight_t &a, const glib::weight_t &b) {
    return std::min(a, b);
  }
  static glib::weight_t min(const glib::weight_t &a, const glib::weight_t &b) {
    return std::max(a, b);
  }
  static glib::weight_t lowest_possible_weight() {
    return std::numeric_limits<glib::weight_t>::max();
  }
  static glib::weight_t highest_possible_weight() { return {}; }
  static bool greater(const glib::weight_t &a, const glib::weight_t &b) {
    return a < b;
  }
  struct less {
    bool operator()(const glib::weight_t &a, const glib::weight_t &b) const {
      return a > b;
    }
  };
};

} // namespace insilab::glib::algorithms::internals
