#pragma once

#include "vertices_ops.hpp"
#include "weight_policies.hpp"
#include <type_traits>
#include <vector>

namespace insilab::glib::algorithms::internals {

class VertexDegree;
class VertexWeight;

template <typename T>
concept VertexPolicyConcept =
    std::is_invocable_v<T, const Vertices<VertexWeight> &,
                        const helper::Array2d<bool> &> ||
    std::is_invocable_v<T, const Vertices<VertexDegree> &,
                        const helper::Array2d<bool> &>;

struct DecreasingNeighborWeightsPlusSelfVertexPolicy {
  const std::vector<glib::weight_t> &weights;

public:
  DecreasingNeighborWeightsPlusSelfVertexPolicy(const auto &strategy)
      : weights(strategy.__weights) {}
  auto operator()(const auto &vertices, const auto &conn) const {
    auto nwt = calculate_sum_neighbor_weights(vertices, conn, weights);
    for (const auto &v : vertices)
      nwt[v.get_vertex_no()] += weights[v.get_vertex_no()];
    return [nwt](const auto &vi, const auto &vj) {
      return nwt[vi.get_vertex_no()] > nwt[vj.get_vertex_no()];
    };
  }
};

template <WeightPolicyConcept WeightPolicy> struct CliquerWeightsVertexPolicy {
  const std::vector<glib::weight_t> &weights;
  auto operator()(const auto &vertices, const auto &conn) const {
    const auto &importance =
        cliquer_weighted_greedy_coloring_TEST<WeightPolicy>(vertices, conn,
                                                            weights);
    return [importance](const auto &vi, const auto &vj) {
      return importance[vi.get_vertex_no()] < importance[vj.get_vertex_no()];
    };
  }
};

class IncreasingWeightsVertexPolicy {
  const std::vector<glib::weight_t> &weights;

public:
  IncreasingWeightsVertexPolicy(const auto &strategy)
      : weights(strategy.__weights) {}
  auto operator()(const auto &vertices, const auto &conn) const {
    return [this](const auto &vi, const auto &vj) {
      return weights[vi.get_vertex_no()] < weights[vj.get_vertex_no()];
    };
  }
};

class DecreasingNeighborWeightsVertexPolicy {
  const std::vector<glib::weight_t> &weights;

public:
  DecreasingNeighborWeightsVertexPolicy(const auto &strategy)
      : weights(strategy.__weights) {}
  auto operator()(const auto &vertices, const auto &conn) const {
    const auto &nwt = calculate_sum_neighbor_weights(vertices, conn, weights);
    return [nwt](const auto &vi, const auto &vj) {
      return nwt[vi.get_vertex_no()] > nwt[vj.get_vertex_no()];
    };
  }
};

class DecreasingDegreesIncreasingWeightsVertexPolicy {
  const std::vector<glib::weight_t> &weights;
  std::vector<node_idx_t> degrees;

public:
  DecreasingDegreesIncreasingWeightsVertexPolicy(const auto &strategy)
      : weights(strategy.__weights), degrees(strategy.__conn.get_szi()) {}
  auto operator()(const auto &vertices, const auto &conn) {
    set_degrees(vertices, conn, degrees);
    return [this](const auto &vi, const auto &vj) {
      return degrees[vi.get_vertex_no()] > degrees[vj.get_vertex_no()] ||
             degrees[vi.get_vertex_no()] == degrees[vj.get_vertex_no()] &&
                 weights[vi.get_vertex_no()] < weights[vj.get_vertex_no()];
    };
  }
};

class DecreasingDegreesVertexPolicy {
  std::vector<node_idx_t> degrees;

public:
  DecreasingDegreesVertexPolicy(const auto &strategy)
      : degrees(strategy.__conn.get_szi()) {}
  const auto &get_degrees() const { return degrees; }
  auto operator()(const auto &vertices, const auto &conn) {
    set_degrees(vertices, conn, degrees);
    return [this](const auto &vi, const auto &vj) {
      return degrees[vi.get_vertex_no()] > degrees[vj.get_vertex_no()];
    };
  }
};

} // namespace insilab::glib::algorithms::internals
