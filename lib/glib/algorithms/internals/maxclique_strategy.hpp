#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "initialization_strategies.hpp"
#include "vertex_degree.hpp"
#include "vertex_weight.hpp"
#include "vertices.hpp"
#include "vertices_ops.hpp"
#include "weight_policies.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

class MaxCliqueStrategy {
protected:
  const helper::Array2d<bool> &__conn;
  const std::optional<std::size_t> __max_steps;
  std::size_t pk{};
  ColorClasses C;
  Clique Q, QMAX;

  bool cut1(const node_idx_t pi, const ColorClass &A) const {
    for (node_idx_t i{}; i < A.size(); ++i)
      if (__conn.get(pi, A.at(i)))
        return true;
    return false;
  }

  template <typename Vertices> void cut2(const Vertices &A, Vertices &B) const {
    for (node_idx_t i{}; i < A.size() - 1; ++i) {
      if (__conn.get(A.back().get_vertex_no(), A[i].get_vertex_no()))
        B.push(A[i].get_vertex_no());
    }
  }

public:
  MaxCliqueStrategy(const helper::Array2d<bool> &conn,
                    const std::optional<std::size_t> max_steps)
      : __conn(conn), C(conn.get_szi()), __max_steps(max_steps) {
    assert(conn.get_szi() < std::numeric_limits<node_idx_t>::max());
    // test if signed vs. unsigned comparison is working properly
    assert(0 > static_cast<signed_node_idx_t>(-1));
    assert(static_cast<node_idx_t>(7) < static_cast<signed_node_idx_t>(-1));
  }
  const auto get_steps_count() const { return pk; }
};

} // namespace insilab::glib::algorithms::internals
