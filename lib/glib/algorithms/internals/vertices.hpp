#pragma once

#include "glib/graph.hpp"
#include <cassert>
#include <memory>

namespace insilab::glib::algorithms::internals {

template <typename Vertex> class Vertices {
  std::unique_ptr<Vertex[]> v;
  node_idx_t sz;
  const node_idx_t sz_alloc;

public:
  Vertices(const node_idx_t size)
      : sz(0), sz_alloc(size),
        v(std::make_unique_for_overwrite<Vertex[]>(size)) {}

  Vertices(const Vertices &) = delete;
  Vertices(Vertices &&othr) = delete;
  Vertices &operator=(const Vertices &) = delete;
  Vertices &operator=(Vertices &&) = delete;

  constexpr auto empty() const { return !sz; }
  constexpr auto size() const { return sz; }
  constexpr auto size_alloc() const { return sz_alloc; }
  constexpr auto begin() const { return v.get(); }
  constexpr auto end() const { return v.get() + sz; }

  void push(const node_idx_t ii) {
    assert(sz < sz_alloc);
    v[sz++].set_vertex_no(ii);
  }

  void pop() {
    assert(sz > 0);
    --sz;
  }

  constexpr auto &operator[](const node_idx_t ii) const {
    assert(ii < sz);
    return v[ii];
  }

  constexpr auto &back() const {
    assert(sz > 0);
    return v[sz - 1];
  }
};

} // namespace insilab::glib::algorithms::internals
