#pragma once

#include "best_cliques.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "maxclique_strategy.hpp"
#include "vertices.hpp"
#include <algorithm>
#include <numeric>
#include <optional>
#include <queue>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <VertexPolicyConcept VertexPolicy>
class KCQStrategy : public MaxCliqueStrategy {
protected:
  friend VertexPolicy;
  VertexPolicy __vertex_policy;

  struct comp {
    bool operator()(const Clique &lhs, const Clique &rhs) const {
      return lhs.size() > rhs.size(); // sorting doesn't have any significance
                                      // here (but has to be implemented), since
                                      // all cliques will have the same size (k)
    }
  };
  using Queue = std::priority_queue<Clique, std::vector<Clique>, comp>;
  BestCliques<Queue> __best_cliques;
  const std::size_t __K; /* size of cliques that we wish to find */

public:
  KCQStrategy(const helper::Array2d<bool> &conn, const node_idx_t k,
              const std::optional<std::size_t> n_cliques = {},
              const std::optional<std::size_t> max_steps = {})
      : MaxCliqueStrategy::MaxCliqueStrategy(conn, max_steps),
        __best_cliques(
            n_cliques.value_or(std::numeric_limits<std::size_t>::max())),
        __K(k), __vertex_policy(*this) {}

  auto run() { return this->run_unweighted(*this); }

  auto get_result() const {
    std::vector<std::vector<node_idx_t>> result;
    std::ranges::transform(__best_cliques.get_cliques(),
                           std::back_inserter(result), &Clique::get_vertices);
    return result;
  }

  template <typename Vertices> void color_sort(Vertices &R) {
    node_idx_t j{};
    node_idx_t maxno{1};
    const node_idx_t min_k =
        std::max(static_cast<signed_node_idx_t>(__K - this->Q.size()),
                 static_cast<signed_node_idx_t>(1));
    assert(this->C.size() >= 3);
    this->C[1].rewind();
    this->C[2].rewind();
    for (node_idx_t i = 0; i < R.size(); ++i) {
      const node_idx_t pi = R[i].get_vertex_no();
      node_idx_t k{1};
      while (this->cut1(pi, this->C[k]))
        ++k;
      if (k > maxno) {
        assert(k == maxno + 1);
        maxno = k;
        this->C[maxno + 1].rewind();
      }
      this->C[k].push(pi);
      if (k < min_k) {
        R[j++].set_vertex_no(pi);
      }
    }
    if (j > 0)
      R[j - 1].set_clq_size_upper_bound(0);
    for (node_idx_t k = min_k; k <= maxno; ++k) {
      for (node_idx_t i = 0; i < this->C[k].size(); ++i) {
        assert(j < R.size());
        auto &v = R[j++];
        v.set_vertex_no(this->C[k].at(i));
        v.set_clq_size_upper_bound(k);
      }
    }
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      if (this->Q.size() + R.back().get_clq_size_upper_bound() > __K - 1) {
        this->Q.push(R.back().get_vertex_no());
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size() && this->Q.size() < __K) {
          ++this->pk;
          this->color_sort(Rp);
          this->expand(Rp);
        } else if (this->Q.size() == __K) {
          __best_cliques.insert(this->Q);
        }
        this->Q.pop();
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
