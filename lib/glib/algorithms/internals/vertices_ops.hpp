#pragma once

#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include "vertex_policies.hpp"
#include "weight_policies.hpp"
#include <cassert>
#include <ranges>
#include <vector>

namespace insilab::glib::algorithms::internals {

void init_vertices(auto &vertices, const helper::Array2d<bool> &conn) {
  for (auto i{0uz}; i < conn.get_szi(); ++i)
    vertices.push(i);
}

void init_clq_size_upper_bounds(auto &vertices, const auto &degrees) {
  if (vertices.empty())
    return;
  // vertices can be sorted differently, that's why we cannot rely (as before)
  // that first vertex will have max degree!
  const auto max_degree = *std::ranges::max_element(degrees);
  assert(max_degree <= vertices.size_alloc());
  assert(vertices.size() <= vertices.size_alloc());
  for (node_idx_t i{}; i < max_degree; ++i)
    vertices[i].set_clq_size_upper_bound(i + 1);
  for (node_idx_t i = max_degree; i < vertices.size(); ++i)
    vertices[i].set_clq_size_upper_bound(max_degree + 1);
}

void set_degrees_up_to_vertex(auto &vertices, const auto &conn,
                              auto &degrees_v) {
  assert(vertices.size() <= vertices.size_alloc());
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    node_idx_t degree{};
    for (node_idx_t j{}; j < i; ++j)
      if (conn.get(vertices[i].get_vertex_no(), vertices[j].get_vertex_no()))
        ++degree;
    degrees_v[vertices[i].get_vertex_no()] = degree;
  }
}

void init_clq_size_upper_bounds_TEST(auto &vertices, const auto &conn) {
  std::vector<node_idx_t> degrees(conn.get_szi());
  set_degrees_up_to_vertex(vertices, conn, degrees);
  assert(vertices.size() <= vertices.size_alloc());
  node_idx_t max_degree{};
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    max_degree = std::max(max_degree, degrees[vertices[i].get_vertex_no()] + 1);
    vertices[i].set_clq_size_upper_bound(max_degree);
  }
}

/**
 * Calculate the sum of adjacent vertices' weights.
 *
 * @return a vector in which each element is the sum of that vertex's neighbors'
 * weights
 */
auto calculate_sum_neighbor_weights(const auto &vertices, const auto &conn,
                                    const auto &weights) {
  std::vector<weight_t> result(weights.size());
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    for (node_idx_t j{}; j < vertices.size(); ++j)
      if (conn.get(vertices[i].get_vertex_no(), vertices[j].get_vertex_no()))
        result[vertices[i].get_vertex_no()] +=
            weights[vertices[j].get_vertex_no()];
  }
  return result;
}

auto calculate_sum_neighbor_weights_up_to_vertex(const auto &vertices,
                                                 const auto &conn,
                                                 const auto &weights) {
  std::vector<weight_t> result(vertices.size());
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    for (node_idx_t j{}; j < i; ++j)
      if (conn.get(vertices[i].get_vertex_no(), vertices[j].get_vertex_no()))
        result[vertices[i].get_vertex_no()] +=
            weights[vertices[j].get_vertex_no()];
  }
  return result;
}

template <WeightPolicyConcept WeightPolicy>
void init_clq_weight_upper_bounds_TEST(auto &vertices, const auto &conn,
                                       const auto &weights) {
  // std::cout << "init_clq_weight_upper_bounds_TEST\n";
  const auto &nwt =
      calculate_sum_neighbor_weights_up_to_vertex(vertices, conn, weights);
  weight_t max_weight{WeightPolicy::lowest_possible_weight()};
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    max_weight =
        WeightPolicy::max(max_weight, nwt[vertices[i].get_vertex_no()] +
                                          weights[vertices[i].get_vertex_no()]);
    vertices[i].set_clq_weight_upper_bound(max_weight);
  }
}

void set_degrees(const auto &vertices, const auto &conn, auto &degrees_v) {
  assert(vertices.size() <= vertices.size_alloc());
  assert(vertices.size_alloc() == degrees_v.size());
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    node_idx_t degree{};
    for (node_idx_t j{}; j < vertices.size(); ++j)
      if (conn.get(vertices[i].get_vertex_no(), vertices[j].get_vertex_no()))
        ++degree;
    degrees_v[vertices[i].get_vertex_no()] = degree;
  }
}

void sort_vertices(const auto &vertices, auto comp) {
  // std::stable_sort(vertices.begin(), vertices.end(), comp);
  std::sort(vertices.begin(), vertices.end(), comp);
}

void print(auto &vertices, const std::vector<weight_t> &weights) {
  assert(weights.empty() || vertices.size() == weights.size());
  std::clog << "PRINTING VERTICES" << std::endl;
  for (node_idx_t i{}; i < vertices.size(); ++i) {
    const auto pi = vertices[i].get_vertex_no();
    std::clog << "VERTEX=" << pi;
    if (!weights.empty()) {
      std::clog << " WEIGHT=" << weights.at(pi)
                << " UB=" << vertices[i].get_clq_weight_upper_bound();
    }
    std::clog << " DEGREE=" << vertices[i].get_clq_size_upper_bound()
              << std::endl;
  }
}

/**
 * Calculates importance of vertices in a graph by considering one vertex at a
 * time, always adding the vertex that (in order of importance):
 *
 *  1. has the minimum weight in the remaining graph
 *  2. has the largest sum of weights surrounding the vertex
 *
 * @note experimentally efficient for use with weighted graphs.
 * @note SOMETHING IS WRONG WITH THIS AS TESTS FAILED (NO CLIQUE EVER GOT
 * CALCULATED)
 *
 * @return a vector of numbers 0 to N, where indexes corresponds to vertex_nos
 * and where lower numbers mean higher importance (the vertices should be
 * ordered so that those with smaller numbers are first)
 */
template <WeightPolicyConcept WeightPolicy>
auto cliquer_weighted_greedy_coloring_TEST(
    const auto &vertices, const helper::Array2d<bool> &conn,
    const std::vector<weight_t> &weights) {

  // sum of adjacent vertices' weights
  auto nwt = calculate_sum_neighbor_weights(vertices, conn, weights);

  std::vector<bool> used(vertices.size());
  std::vector<node_idx_t> importance(weights.size());

  for (node_idx_t cnt{}; cnt < vertices.size(); ++cnt) {
    weight_t min_wt = WeightPolicy::highest_possible_weight();
    weight_t max_nwt = WeightPolicy::lowest_possible_weight();
    node_idx_t p{};
    for (node_idx_t i{vertices.size() - 1}; i >= 0; --i)
      if (!used[i])
        min_wt =
            WeightPolicy::min(min_wt, weights[vertices[i].get_vertex_no()]);
    for (node_idx_t i{vertices.size() - 1}; i >= 0; --i) {
      if (used[i] ||
          WeightPolicy::greater(weights[vertices[i].get_vertex_no()], min_wt))
        continue;
      if (WeightPolicy::greater(nwt[vertices[i].get_vertex_no()], max_nwt)) {
        max_nwt = nwt[vertices[i].get_vertex_no()];
        p = i;
      }
    }
    importance[vertices[p].get_vertex_no()] = cnt;
    used[p] = true;
    for (node_idx_t i{}; i < vertices.size(); ++i)
      if (!used[i] &&
          conn.get(vertices[i].get_vertex_no(), vertices[p].get_vertex_no()))
        nwt[vertices[i].get_vertex_no()] -=
            weights[vertices[p].get_vertex_no()];
  }
  return importance;
}

} // namespace insilab::glib::algorithms::internals
