#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "mcq_strategy.hpp"
#include "step_count.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <VertexPolicyConcept VertexPolicy>
class MCQDStrategy : public MCQStrategy<VertexPolicy> {
private:
  StepCount __S;

public:
  MCQDStrategy(const helper::Array2d<bool> &conn,
               const std::optional<std::size_t> max_steps = {},
               const double Tlimit = 0.025)
      : MCQStrategy<VertexPolicy>::MCQStrategy(conn, max_steps),
        __S(conn.get_szi(), Tlimit) {}

  auto run() {
    return run_unweighted(*this, this->__conn, this->__vertex_policy);
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    __S.init_steps_level();
    while (R.size()) {
      if (this->Q.size() + R.back().get_clq_size_upper_bound() >
          this->QMAX.size()) {
        this->Q.push(R.back().get_vertex_no());
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size()) {
          ++this->pk;
          __S.do_steps_level_lt_tlimit(this->pk, [&]() {
            sort_vertices(Rp, this->__vertex_policy(Rp, this->__conn));
          });
          this->color_sort(Rp);
          __S.increase_steps_level();
          this->expand(Rp);
          __S.decrease_level();
        } else if (this->Q.size() > this->QMAX.size()) {
          this->QMAX = this->Q;
        }
        this->Q.pop();
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
