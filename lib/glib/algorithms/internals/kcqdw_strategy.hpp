#pragma once

#include "best_cliques.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "kcqw_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class KCQDWStrategy : public KCQWStrategy<WeightPolicy, VertexPolicy> {
private:
  StepCount __S;

public:
  KCQDWStrategy(const helper::Array2d<bool> &conn,
                const std::vector<glib::weight_t> &weights, const node_idx_t k,
                const std::optional<std::size_t> n_cliques = {},
                const std::optional<std::size_t> max_steps = {},
                const double Tlimit = 0.025)
      : KCQWStrategy<WeightPolicy, VertexPolicy>::KCQWStrategy(
            conn, weights, k, n_cliques, max_steps),
        __S(conn.get_szi(), Tlimit) {}

  auto run() {
    return run_weighted_KCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, this->__vertex_policy);
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    __S.init_steps_level();
    while (R.size()) {
      if (this->Q.size() + R.back().get_clq_size_upper_bound() >
              this->__K - 1 &&
          WeightPolicy::greater(
              this->Q.get_weight() + R.back().get_clq_weight_upper_bound(),
              this->get_lowest_weight(this->__best_cliques))) {
        this->Q.push(R.back().get_vertex_no());
        this->Q.increase_weight(this->__weights[R.back().get_vertex_no()]);
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size() && this->Q.size() < this->__K) {
          ++this->pk;
          __S.do_steps_level_lt_tlimit(this->pk, [&]() {
            sort_vertices(Rp, this->__vertex_policy(Rp, this->__conn));
          });
          this->color_sort(Rp);
          __S.increase_steps_level();
          this->expand(Rp);
          __S.decrease_level();
        } else if (this->Q.size() == this->__K &&
                   WeightPolicy::greater(
                       this->Q.get_weight(),
                       this->get_lowest_weight(this->__best_cliques))) {
          this->__best_cliques.insert(this->Q);
        }
        this->Q.pop();
        this->Q.decrease_weight(this->__weights[R.back().get_vertex_no()]);
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
