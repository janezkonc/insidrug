#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "mcqw_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <WeightPolicyConcept WeightPolicy, VertexPolicyConcept VertexPolicy>
class NaiveMCQWStrategy : public MCQWStrategy<WeightPolicy, VertexPolicy> {
public:
  NaiveMCQWStrategy(const helper::Array2d<bool> &conn,
                    const std::vector<glib::weight_t> &weights,
                    const std::optional<std::size_t> max_steps = {})
      : MCQWStrategy<WeightPolicy, VertexPolicy>::MCQWStrategy(conn, weights,
                                                               max_steps) {}

  auto run() {
    return run_weighted_MCQW<decltype(*this), WeightPolicy>(
        *this, this->__conn, this->__weights, this->__vertex_policy);
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      this->Q.push(R.back().get_vertex_no());
      this->Q.increase_weight(this->__weights[R.back().get_vertex_no()]);
      Vertices Rp(R.size());
      this->cut2(R, Rp);
      if (Rp.size()) {
        ++this->pk;
        this->expand(Rp);
      } else if (WeightPolicy::greater(this->Q.get_weight(),
                                       this->QMAX.get_weight())) {
        this->QMAX = this->Q;
      }
      this->Q.pop();
      this->Q.decrease_weight(this->__weights[R.back().get_vertex_no()]);
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
