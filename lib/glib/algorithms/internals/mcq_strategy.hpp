#pragma once

#include "clique.hpp"
#include "color_classes.hpp"
#include "helper/array2d.hpp"
#include "maxclique_strategy.hpp"
#include "vertices.hpp"
#include <numeric>
#include <optional>
#include <vector>

namespace insilab::glib::algorithms::internals {

template <VertexPolicyConcept VertexPolicy>
class MCQStrategy : public MaxCliqueStrategy {
protected:
  friend VertexPolicy;
  VertexPolicy __vertex_policy;

public:
  MCQStrategy(const helper::Array2d<bool> &conn,
              const std::optional<std::size_t> max_steps = {})
      : MaxCliqueStrategy::MaxCliqueStrategy(conn, max_steps),
        __vertex_policy(*this) {}
  auto run() { return run_unweighted(*this, this->__conn, __vertex_policy); }

  auto get_result() const { return this->QMAX.get_vertices(); }

  template <typename Vertices> void color_sort(Vertices &R) {
    node_idx_t j{};
    node_idx_t maxno{1};
    const node_idx_t min_k = std::max(
        static_cast<signed_node_idx_t>(this->QMAX.size() - this->Q.size() + 1),
        static_cast<signed_node_idx_t>(1));
    assert(this->C.size() >= 3);
    this->C[1].rewind();
    this->C[2].rewind();
    for (node_idx_t i = 0; i < R.size(); ++i) {
      const node_idx_t pi = R[i].get_vertex_no();
      node_idx_t k{1};
      while (this->cut1(pi, this->C[k]))
        ++k;
      if (k > maxno) {
        assert(k == maxno + 1);
        maxno = k;
        this->C[maxno + 1].rewind();
      }
      this->C[k].push(pi);
      if (k < min_k) {
        R[j++].set_vertex_no(pi);
      }
    }
    if (j > 0)
      R[j - 1].set_clq_size_upper_bound(0);
    for (node_idx_t k = min_k; k <= maxno; ++k) {
      for (node_idx_t i = 0; i < this->C[k].size(); ++i) {
        assert(j < R.size());
        auto &v = R[j++];
        v.set_vertex_no(this->C[k].at(i));
        v.set_clq_size_upper_bound(k);
      }
    }
  }

  template <typename Vertices> void expand(Vertices &R) {
    if (this->__max_steps.has_value() && this->pk > this->__max_steps.value())
      return;
    while (R.size()) {
      if (this->Q.size() + R.back().get_clq_size_upper_bound() >
          this->QMAX.size()) {
        this->Q.push(R.back().get_vertex_no());
        Vertices Rp(R.size());
        this->cut2(R, Rp);
        if (Rp.size()) {
          ++this->pk;
          this->color_sort(Rp);
          this->expand(Rp);
        } else if (this->Q.size() > this->QMAX.size()) {
          this->QMAX = this->Q;
        }
        this->Q.pop();
      } else {
        return;
      }
      R.pop();
    }
  }
};

} // namespace insilab::glib::algorithms::internals
