#pragma once

#include "color_class.hpp"
#include <cassert>
#include <vector>

namespace insilab::glib::algorithms::internals {

class ColorClasses {
  std::vector<ColorClass> C;

public:
  ColorClasses(const std::size_t conn_szi) : C(conn_szi + 3) {
    assert(C.size() >= 3); // see color_sort, line with C[2]
    for (auto i{0uz}; i < C.size(); ++i)
      C[i].init(conn_szi + 1);
  }

  constexpr auto &operator[](std::size_t i) {
    assert(i >= 0 && i < C.size());
    return C[i];
  }
  constexpr auto size() { return C.size(); }
  // void sort_decreasing_size(const std::size_t first, const std::size_t last) {
  //   std::sort(
  //       std::begin(C) + first, std::begin(C) + last + 1,
  //       [](const auto &Ci, const auto &Cj) { return Ci.size() > Cj.size(); });
  // }
};

} // namespace insilab::glib::algorithms::internals
