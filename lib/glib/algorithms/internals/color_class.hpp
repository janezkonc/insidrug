#pragma once

#include "glib/graph.hpp"
#include <cassert>
#include <memory>

namespace insilab::glib::algorithms::internals {

class ColorClass {
  std::unique_ptr<node_idx_t[]> i{};
  node_idx_t sz{};

public:
  ColorClass() = default;
  ColorClass(const node_idx_t sz) = delete;
  ColorClass(const ColorClass &) = delete;
  void init(const node_idx_t sz) {
    i = std::make_unique_for_overwrite<node_idx_t[]>(sz);
    rewind();
  }
  void push(const node_idx_t ii) { i[sz++] = ii; };
  void pop() {
    assert(sz > 0);
    --sz;
  };
  void rewind() { sz = 0; };
  auto size() const { return sz; }
  auto &at(const node_idx_t ii) const {
    assert(ii < sz);
    return i[ii];
  }
  auto empty() const { return sz == 0; }
  void erase(const node_idx_t ii) {
    assert(ii < sz);
    i[ii] = i[sz - 1];
    pop();
  }
  void sort_increasing_weight(const auto &weights, const auto greater) {
    std::sort(i.get(), i.get() + sz,
              [&weights, &greater](const auto &p, const auto &r) {
                return !greater(weights[p], weights[r]);
              });
  }
};

} // namespace insilab::glib::algorithms::internals
