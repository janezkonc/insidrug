#pragma once

#include "glib/graph.hpp"

namespace insilab::glib::algorithms {

/**
 * Find all (or most) cycles in a graph, which can be composed of multiple
 * disconnected components.
 *
 * @note some cycles may not be found as the number of steps taken by the
 * algorithm is limited
 *
 * @param graph a graph-like container
 * @param max_steps limit for the number of recursive steps to take
 * @return a set of sets of pointers to vertices, representing cycles found
 * in a graph
 */
template <GraphLike T>
auto find_cycles(const T &graph, const std::size_t max_steps = 10000) {

  using vertex_t = Vertex<T>;

  if (graph.empty())
    return SubGraphs<T>{};

  const auto expand = [&max_steps](vertex_t &v, std::vector<vertex_t *> &path,
                                   SubGraphs<T> &cycles,
                                   std::set<const vertex_t *> &visited,
                                   std::size_t &steps, auto &&expand) {
    if (steps++ > max_steps) // needed for molecules like CWO with many
                             // rings (see issue #71)
      return;
    path.push_back(&v);
    visited.insert(&v);
    for (auto &adj_v : v) {
      if (!visited.contains(&adj_v)) {
        expand(adj_v, path, cycles, visited, steps, expand);
      } else {
        const auto it = std::find(path.begin(), path.end(), &adj_v);
        const std::set<vertex_t *> cycle(it, path.end());
        if (cycle.size() > 2) {
          cycles.insert(cycle);
        }
      }
    }
    visited.erase(&v);
    path.pop_back();
  };
  std::set<const vertex_t *> visited;
  SubGraphs<T> cycles{};
  for (auto &pv : graph) {
    std::vector<vertex_t *> path;
    if (auto steps{0uz}; !visited.contains(&*pv))
      expand(*pv, path, cycles, visited, steps, expand);
  }
  return cycles;
}

/**
 * Find fused rings (rings that share at least one edge) in a graph.
 *
 * @param graph a graph-like container
 * @return a set of sets of pointers to vertices, representing fused rings found
 * in a graph
 */
template <GraphLike T> auto find_fused_rings(const T &graph) {

  using vertex_t = Vertex<T>;

  SubGraphs<T> fused;
  auto cycles = find_cycles(graph);

  bool mergeable = true;
  // merge cycles until no more can be merged
  while (mergeable) {
    mergeable = false;
    for (auto it = cycles.begin(); it != cycles.end(); ++it) {
      const auto &first = *it;
      std::set<vertex_t *> current(first.begin(), first.end());
      auto it2 = it;
      for (++it2; it2 != cycles.end();) {
        const auto &second = *it2;
        std::set<const vertex_t *> inter;
        std::ranges::set_intersection(first, second,
                                      std::inserter(inter, std::begin(inter)));
        // merge first with second if > 1 vertices in common
        if (inter.size() > 1) {
          mergeable = true;
          current.insert(second.begin(), second.end());
          it2 = cycles.erase(it2);
        } else {
          ++it2;
        }
      }
      fused.insert(current);
    }
    if (mergeable) {
      cycles = fused;
      fused.clear();
    }
  }
  return fused;
}

/**
 * Find rings (cycles that are not made out of smaller cycles) in a graph.
 *
 * @param graph a graph-like container
 * @return a set of sets of pointers to vertices, representing rings found
 * in a graph
 */
template <GraphLike T> auto find_rings(const T &graph) {

  using vertex_t = Vertex<T>;

  SubGraphs<T> rings;
  const auto &cycles = find_cycles(graph);
  std::vector<std::set<vertex_t *>> v(cycles.begin(), cycles.end());
  std::ranges::sort(
      v, [](const auto &i, const auto &j) { return i.size() < j.size(); });
  std::set<const vertex_t *> mapped;
  // go over cycles sorted by increasing size
  for (const auto &cycle : v) {
    // find cycles that are not made out of smaller cycles
    std::set<const vertex_t *> inter;
    std::ranges::set_intersection(cycle, mapped,
                                  std::inserter(inter, std::begin(inter)));
    if (inter.size() == cycle.size()) {
      continue;
    }
    mapped.insert(cycle.begin(), cycle.end());
    rings.insert(cycle);
  }
  return rings;
}

/**
 * Find disconnected subgraphs (components) within a graph defined as a set of
 * vertices.
 *
 * @param graph a graph-like container
 * @return a set of sets of pointers to vertices, representing components found
 * in a graph
 */
template <GraphLike T> auto find_components(const T &graph) {

  using vertex_t = Vertex<T>;

  const auto expand = [](vertex_t &v, std::set<vertex_t *> &component,
                         std::set<const vertex_t *> &visited,
                         std::set<const vertex_t *> &graph_s,
                         auto &&expand) -> void {
    visited.insert(&v);
    component.insert(&v);
    for (auto &adj_v : v) {
      if (!visited.contains(&adj_v) && graph_s.contains(&adj_v)) {
        expand(adj_v, component, visited, graph_s, expand);
      }
    }
  };

  SubGraphs<T> components{};
  std::set<const vertex_t *> visited;
  std::set<const vertex_t *> graph_s;
  for (const auto &pvertex : graph)
    graph_s.insert(&*pvertex);
  for (const auto &pvertex : graph) {
    if (!visited.contains(&*pvertex)) {
      std::set<vertex_t *> component;
      expand(*pvertex, component, visited, graph_s, expand);
      components.insert(component);
    }
  }
  return components;
}

} // namespace insilab::glib::algorithms
