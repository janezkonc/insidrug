#include "fdlayout.hpp"
#include "geom3d/linear.hpp"
#include "helper/debug.hpp"

using namespace insilab::geom3d;

namespace insilab::glib::algorithms {

geom3d::Point::Vec FdLayout::run() {

  dbgmsg("Running force-directed graph optimization");

  for (int i = 0; i < __nsteps; ++i) {

    __accelerate();
    __integrate();

    if (i % __nupdate == 0) {
      std::clog << "Updating edges at step " << i << " of " << __nsteps
                << std::endl;
      __update_edges();
    }
  }

  geom3d::Point::Vec vertices;
  for (auto &v : __g) {
    vertices.push_back(*v);
  }
  return vertices;
}

void FdLayout::__update_edges() {
  geom3d::Point::Vec vertices;
  for (auto &v : __g) {
    vertices.push_back(*v);
  }
  __g = Point::create_graph(vertices, __dist);
}

void FdLayout::__accelerate() {

  for (int i = 0; i < __g.size(); ++i) {
    geom3d::Point &vi = *__g[i];
    __a[i] = geom3d::Point();
    for (int j = 0; j < vi.size(); ++j) { // go over edges
      geom3d::Point &vj = vi[j];
      geom3d::Point p = vj - vi;
      double dx = p.length() - __l;
      p = p.norm();
      p = p * __k * dx;
      __a[i] = __a[i] + p;
    }
  }
}

void FdLayout::__integrate() {
  for (int i = 0; i < __g.size(); ++i) {
    geom3d::Point &vi = *__g[i];
    geom3d::Point v0 = __v[i] * 0.99;
    __v[i] = __a[i] * __tstep + v0;
    vi = __a[i] / 2 * __tstep * __tstep + v0 * __tstep + vi;
  }
}

} // namespace insilab::glib
