#pragma once

#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include <vector>

namespace insilab::glib::test {

/**
 * Shuffle the indexes of graph vertices.
 *
 * @note graph remains the same as before with respect to edges and weights,
 * just indicies of vertices are shuffled
 *
 * @param adjacency_matrix N x N adjacency matrix of vertex connections
 * @param weights a vector of weights
 *
 * @return a pair, consting of shuffled adjacency matrix (full) and shuffled
 * weights
 */
std::pair<helper::Array2d<bool>, std::vector<weight_t>>
shuffle_vertices(const helper::Array2d<bool> &adjacency_matrix,
                 const std::vector<weight_t> &weights);

/**
 * Assign random weights according to normal distribution to an existing
 * unweighted graph.
 *
 * @param adjacency_matrix N x N adjacency matrix of vertex connections
 * @param mean mean of weights
 * @param sd standard deviation for weights
 *
 * @return vector of newly assigned weights
 */
std::vector<weight_t>
assign_random_weights(const helper::Array2d<bool> &adjacency_matrix,
                      const double mean, const double sd);

/**
 * Assign weights to graph vertices using a method that seems to be popular in
 * the literature: w(vi) = (i + 1) mod 200 + 1. We assume indices i start at 0.
 *
 * @param adjacency_matrix N x N adjacency matrix of vertex connections
 *
 * @return vector of newly assigned weights
 */
std::vector<weight_t>
assign_mod200_weights(const helper::Array2d<bool> &adjacency_matrix);

/**
 * Assign random connections (edges) according to uniform normal distribution.
 *
 * @param sz number of vertices in a graph to generate
 * @param p probability that an edge exist between two vertices
 *
 * @return random adjacency matrix (just values above the diagonal are filled)
 */
helper::Array2d<bool> generate_random_adjacency_matrix(const std::size_t sz,
                                                       const double p);

} // namespace insilab::glib::test
