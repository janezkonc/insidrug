#include "graph_manipulators.hpp"
#include <random>
#include <ranges>
#include <vector>

namespace insilab::glib::test {

std::pair<helper::Array2d<bool>, std::vector<weight_t>>
shuffle_vertices(const helper::Array2d<bool> &adjacency_matrix,
                 const std::vector<weight_t> &weights) {
  std::random_device rd;
  std::mt19937 gen{rd()};
  std::vector<std::size_t> indexes(weights.size());
  for (auto i{0uz}; i < indexes.size(); ++i)
    indexes[i] = i;
  std::ranges::shuffle(indexes, gen);

  std::vector<weight_t> shuff_weights(weights.size());
  helper::Array2d<bool> shuff_adjacency_matrix(adjacency_matrix.get_szi());
  for (auto i{0uz}; i < indexes.size(); ++i) {
    shuff_weights[indexes[i]] = weights[i];
    for (auto j{0uz}; j < indexes.size(); ++j) {
      if (adjacency_matrix.get(i, j)) {
        shuff_adjacency_matrix.set(indexes[i], indexes[j]);
        shuff_adjacency_matrix.set(indexes[j], indexes[i]);
      }
    }
  }
  return std::pair{std::move(shuff_adjacency_matrix), std::move(shuff_weights)};
}

std::vector<weight_t>
assign_random_weights(const helper::Array2d<bool> &adjacency_matrix,
                      const double mean, const double sd) {

  const auto sz = adjacency_matrix.get_szi();
  std::vector<weight_t> weights(sz);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<> normal_distrib(mean, sd);
  for (auto i{0uz}; i < sz; ++i) {
    const double w = normal_distrib(gen);
    if (mean < 0 && w >= 0 || mean >= 0 && w <= 0)
      throw std::out_of_range("[WHOOPS] Select mean and standard deviation "
                              "such that all weights are either "
                              "lower or higher than zero!");
    weights[i] = static_cast<weight_t>(w);
  }
  return weights;
}

std::vector<weight_t>
assign_mod200_weights(const helper::Array2d<bool> &adjacency_matrix) {

  const auto sz = adjacency_matrix.get_szi();
  std::vector<weight_t> weights(sz);
  for (auto i{0uz}; i < sz; ++i) {
    weights[i] = static_cast<weight_t>((i + 1) % 200 + 1);
  }
  return weights;
}

helper::Array2d<bool> generate_random_adjacency_matrix(const std::size_t sz,
                                                       const double p) {

  helper::Array2d<bool> adjacency_matrix(sz);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> distrib01(0.0, 1.0);
  for (auto i{0uz}; i < sz; ++i) {
    for (auto j = i + 1; j < sz; ++j) {
      if (distrib01(gen) < p)
        adjacency_matrix.set(i, j);
    }
  }
  return adjacency_matrix;
}

} // namespace insilab::glib::test
