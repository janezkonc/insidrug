#pragma once

#include "helper/help.hpp"

namespace insilab::glib::test {

/**
 * Check that the presumed clique is really a clique.
 *
 *
 * @param clique a presumed clique given as a vector of vertex indices
 * @param weight a presumed clique's weight
 * @param adjacency_matrix
 * @param weights
 *
 * @return true, if the presumed clique is really a clique (it must be fully
 * connected and its weight must be equal to the here calculated weight), false
 * otherwise
 */
bool check_clique(const auto &clique, const auto &weight,
                  const auto &adjacency_matrix, const auto &weights) {
  std::remove_cvref_t<decltype(weight)> tot_weight{};
  for (auto i{0uz}; i < clique.size(); ++i) {
    for (auto j = i + 1; j < clique.size(); ++j) {
      if (!adjacency_matrix.get(clique[i], clique[j]))
        return false;
    }
    tot_weight += weights.at(clique[i]);
  }
  return helper::approx_equal(tot_weight, weight);
};

} // namespace insilab::glib::test
