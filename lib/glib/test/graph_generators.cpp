#include "graph_generators.hpp"
#include "glib/io/io.hpp"
#include "graph_manipulators.hpp"
#include "helper/array2d.hpp"
#include <map>
#include <random>
#include <vector>

namespace insilab::glib::test {

void generate_random_weighted_graph(const std::string &filename,
                                    const std::string &method,
                                    const std::size_t sz, const double p,
                                    const double mean, const double sd) {

  const auto adjacency_matrix = generate_random_adjacency_matrix(sz, p);
  const auto weights = method == "mod200"
                           ? assign_mod200_weights(adjacency_matrix)
                           : assign_random_weights(adjacency_matrix, mean, sd);
  glib::io::write_dimacs_graph(filename, adjacency_matrix, weights);
}

} // namespace insilab::glib::test
