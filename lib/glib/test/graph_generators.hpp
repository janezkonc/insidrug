#pragma once

#include <string>

namespace insilab::glib::test {

/**
 * Generate a random weighed graph and write it to a file
 *
 * @note weights are normally distributed values around the given mean value
 * @note weights must be ONLY negative or ONLY positive values (depending on the
 * choice of mean and sd), function throws if it is not so
 * @note if method is mod200, weights are calculated using the equation
 * (i + 1) mod 200 + 1
 *
 * @param filename a file that will contain the generated graph in DIMACS format
 * @param method a string, determines which method to use to determine vertex
 * weights: mod200 or random
 * @param sz number of vertices
 * @param p probability of an edge
 * @param mean average weight
 * @param sd standard deviation of weights
 */
void generate_random_weighted_graph(const std::string &filename,
                                    const std::string &method = "random",
                                    const std::size_t sz = 100,
                                    const double p = 0.8,
                                    const double mean = -100.0,
                                    const double sd = 20.0);

} // namespace insilab::glib::test
