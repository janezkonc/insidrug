#include "residue.hpp"
#include "atom.hpp"
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {

std::ostream &operator<<(std::ostream &stream, const Residue &residue) {
  stream << "residue.resn = " << residue.resn() << " "
         << "residue.resi = " << residue.resi();
  for (auto &atom : residue) {
    stream << "  " << atom;
  }
  return stream;
}

std::ostream &operator<<(std::ostream &stream, const Residue::PVec &rvec) {
  for (auto &presidue : rvec) {
    stream << *presidue;
  }
  return stream;
}

Residue &Residue::set_rest(const std::string &rest) {
  __params.rest = details::to_rest(rest);
  return *this;
}

Residue &Residue::set_rest(const Residue::Type rest) {
  __params.rest = rest;
  return *this;
}

} // namespace insilab::molib
