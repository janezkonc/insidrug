#pragma once

#include "nlohmann/json.hpp"
#include <vector>

/*********************************************************************************
 * Helper functions to_json and from_json are required by nlohmann's Json
 * library and enable to automatically convert an object to/from json. More
 * information at: https://github.com/nlohmann/json#arbitrary-types-conversions
 *********************************************************************************/

namespace insilab::molib::algorithms {
class Fragment;
}

namespace insilab::molib {

class Bond;
class Molecule;

void from_json(const nlohmann::json &molecule_j, Molecule &molecule);

/**
 * Since bonds are given for the whole Molecule, several atoms
 * may have the same atom numbers, eg. when there are multiple Models or
 * Assemblies within a Molecule. It is therefore checked that the bonds that are
 * outputted to Json have unique atom numbers.
 */
void to_json(nlohmann::json &bonds_j, const std::vector<Bond *> &bonds);
void to_json(nlohmann::json &molecule_j, const Molecule &molecule);

} // namespace insilab::molib
