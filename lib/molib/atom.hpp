#pragma once

#include "bond.hpp"
#include "element.hpp"
#include "geom3d/coordinate.hpp"
#include "geom3d/matrix.hpp"
#include "grid/grid.hpp"
#include "helper/help.hpp"
#include "helper/it.hpp"
#include "molib/details/utils.hpp"

namespace insilab::molib {

class Residue;

class Atom : public helper::template_vector_container<Atom>,
             public details::Utils<Atom> {
public:
  using atom_tuple =
      std::tuple<int, std::string, std::unique_ptr<geom3d::Coordinate>, double>;
  using Pair = std::pair<Atom *, Atom *>;
  using Vec = std::vector<Atom>;
  using PVec = std::vector<Atom *>;
  using CPVec = std::vector<const Atom *>;
  using PSet = std::set<Atom *>;
  using CPSet = std::set<const Atom *>;
  using Grid = grid::Grid<Atom>;

  struct Params {
    int atom_number{1};
    std::string atom_name{"C"};
    geom3d::Coordinate crd{};
    char alt_loc{' '};
    double temperature_factor{};
    Element element{};
    int idatm_type{149};
    std::string sybyl_type{};
    std::string gaff_type{};
    double charge{};
    std::string smiles_label{};
    std::map<std::string, int> smiles_prop{};
    std::map<int, int> aps{};
  };

private:
  Params __params;
  std::map<const Atom *, std::shared_ptr<Bond>> __bonds;

public:
  Atom() = default;
  Atom(const Params &p) : __params(p) {}
  // this code below IS REQUIRED AS LONG AS WE HAVE __bonds...
  Atom(Atom &&rhs) =
      default; // this will create default move constructor (if copy constructor
               // is defined, there is no default for move)
  Atom(const Atom &rhs) : __params(rhs.__params) {
    // [NOTE] __bonds is not copied because it requires
    // references to other bonded atoms, that become known
    // only after parsing the whole molecule
    dbgmsg("Copy constructor : atom");
  }

  const Params &params() const { return __params; }

  Bond &connect(Atom &a2);
  Atom &clear_bonds() {
    __bonds.clear();
    return *this;
  }
  Bond::PVec get_bonds() const {
    Bond::PVec bonds;
    for (auto &kv : __bonds)
      bonds.push_back(&*kv.second);
    return bonds;
  }
  const std::shared_ptr<Bond> &get_shared_ptr_bond(const Atom &other) const {
    return __bonds.at(&other);
  }
  Bond &get_bond(const Atom &other) const {
    return *get_shared_ptr_bond(other);
  }
  std::shared_ptr<Bond> &insert_bond(const Atom &other,
                                     const std::shared_ptr<Bond> &bond) {
    return __bonds.insert({&other, bond}).first->second;
  }
  Atom &erase_bond(const Atom &other) {
    __bonds.erase(&other);
    return *this;
  }
  bool is_adjacent(const Atom &other) const { return __bonds.contains(&other); }
  bool is_adjacent(const std::string &atom_name) const {
    for (auto &other : *this)
      if (other.atom_name() == atom_name)
        return true;
    return false;
  }
  int get_num_hydrogens() const;
  int atom_number() const { return __params.atom_number; }
  Atom &set_atom_name(const std::string &atom_name) {
    __params.atom_name = atom_name;
    return *this;
  }
  Atom &set_atom_number(int atom_number) {
    __params.atom_number = atom_number;
    return *this;
  }
  Atom &set_idatm_type(const std::string &idatm_type) {
    __params.idatm_type = helper::idatm_mask.at(idatm_type);
    return *this;
  }
  Atom &set_gaff_type(const std::string &gaff_type) {
    __params.gaff_type = gaff_type;
    return *this;
  }
  Atom &set_sybyl_type(const std::string &sybyl_type) {
    __params.sybyl_type = sybyl_type;
    return *this;
  }
  Atom &set_smiles_prop(const std::map<std::string, int> &smiles_prop) {
    __params.smiles_prop = smiles_prop;
    return *this;
  }
  const auto &get_smiles_prop() const { return __params.smiles_prop; }
  Atom &insert_property(const std::string &prop, const int count) {
    __params.smiles_prop.insert({prop, count});
    return *this;
  }
  Atom &add_property(const std::string &prop) {
    __params.smiles_prop[prop]++;
    return *this;
  }
  Atom &erase_property(const std::string &prop) {
    __params.smiles_prop.erase(prop);
    return *this;
  }
  Atom &erase_properties() {
    __params.smiles_prop.clear();
    return *this;
  }
  bool has_property(const std::string &prop) const {
    return __params.smiles_prop.count(prop);
  }
  int get_num_property(const std::string &prop) const {
    if (__params.smiles_prop.count(prop) == 0)
      return 0;
    return __params.smiles_prop.at(prop);
  }
  const auto &get_smiles_label() const { return __params.smiles_label; }
  int get_num_bond_with_bond_gaff_type(const std::string &prop) const;
  int compute_num_property(const std::string &prop) const;
  Atom &set_crd(const geom3d::Coordinate &crd) {
    __params.crd = crd;
    return *this;
  }
  std::string idatm_type_unmask() const {
    return helper::idatm_unmask[__params.idatm_type];
  }
  int idatm_type() const { return __params.idatm_type; }
  const std::string &sybyl_type() const { return __params.sybyl_type; }
  Atom &set_charge(const double charge) {
    __params.charge = charge;
    return *this;
  }
  double charge() const { return __params.charge; }
  double radius() const { return __params.element.vdw_radius(); }
  const std::string &gaff_type() const { return __params.gaff_type; }
  const std::string &atom_name() const { return __params.atom_name; }
  const auto &alt_loc() const { return __params.alt_loc; }
  const auto &temperature_factor() const { return __params.temperature_factor; }
  const Element &element() const { return __params.element; }
  geom3d::Coordinate &crd() { return __params.crd; }
  const geom3d::Coordinate &crd() const { return __params.crd; }
  Atom &set_aps(const auto &aps) {
    __params.aps = aps;
    return *this;
  }
  const std::map<int, int> &get_aps() const { return __params.aps; }

  friend std::ostream &operator<<(std::ostream &stream, const Atom &a);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const Atom::PSet &atoms);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const Atom::PVec &atoms);
};

} // namespace insilab::molib
