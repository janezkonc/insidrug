#pragma once

#include "atom.hpp"
#include "details/utils.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "helper/it.hpp"
#include "residue.hpp"
#include "segment.hpp"

namespace insilab::molib {

class Atom;
class Residue;
class Model;

class Chain
    : public helper::template_map_container<Segment, Chain, std::string>,
      public details::Utils<Chain> {
public:
  struct Params {
    std::string chain_id{"A"};
  };

private:
  Params __params;

public:
  Chain() = default;
  Chain(const Params &p) : __params(p) {}

  const Params &params() const { return __params; }

  Segment &add(Segment s) {
    const auto i = s.seg_id();
    return this->insert(i, std::move(s), this);
  }
  Residue &residue(Residue::Pair p) const {
    for (auto &segment : *this) {
      if (segment.has_element(p)) {
        return segment.element(p);
      }
    }
    throw helper::Error("[WHOOPS] Chain does not contain residue...");
  }

  bool has_residue(Residue::Pair p) {
    for (auto &segment : *this) {
      if (segment.has_element(p)) {
        return true;
      }
    }
    return false;
  }
  std::string chain_id() const { return __params.chain_id; }
};

} // namespace insilab::molib
