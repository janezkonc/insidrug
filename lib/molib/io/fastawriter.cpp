#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "writer.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::stringstream Writer::FastaWriter::write(const Molecules &mols) {
  std::stringstream ss;
  for (const auto &molecule : mols) {
    ss << this->write(molecule).str();
  }
  return ss;
}

std::stringstream Writer::FastaWriter::write(const Molecule &molecule) {
  std::stringstream ss;
  for (const auto &[chain_id, seqres] : molecule.get_params().seqres) {
    // write out fasta title
    ss << ">Chain " << chain_id << "|" << molecule.name() << std::endl;
    // write out fasta sequence
    for (const auto &resn : seqres) {
      char resn_one_letter{'X'}; // any amino acid
      if (const auto o = helper::get_one_letter(resn); o) {
        resn_one_letter = o.value();
      } else if (const auto resn_standard =
                     molecule.get_standard_resn_for_modified(resn);
                 resn_standard) { // try if it is a modified residue...
        if (const auto o = helper::get_one_letter(resn_standard.value()); o) {
          resn_one_letter = o.value();
        }
      }
      ss << resn_one_letter;
    }
    ss << std::endl;
  }
  return ss;
}

} // namespace insilab::molib::io
