#pragma once

#include "helper/parser_base.hpp"
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {
class Molecules;
}

namespace insilab::molib::io {

class Parser {
public:
  enum Options {
    first_model = 1,
    all_models = 2,
    hydrogens = 4,
    skip_hetatm = 8,
    sparse_macromol = 16,
    panic_bad_structure = 32,
  };

private:
  class PdbParser : public helper::ParserBase<Molecules> {
  public:
    using ParserBase<Molecules>::ParserBase;
    Molecules parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };
  class CifParser : public helper::ParserBase<Molecules> {
  public:
    using ParserBase<Molecules>::ParserBase;
    Molecules parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };
  class Mol2Parser : public helper::ParserBase<Molecules> {
  public:
    using ParserBase<Molecules>::ParserBase;
    Molecules parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };
  class SdfParser : public helper::ParserBase<Molecules> {
  public:
    using ParserBase<Molecules>::ParserBase;
    Molecules parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };
  class JsonParser : public helper::ParserBase<Molecules> {
  public:
    using ParserBase<Molecules>::ParserBase;
    Molecules parse(const std::vector<std::string> &) override;
    std::vector<std::string> read_file() override;
  };

  std::unique_ptr<helper::ParserBase<Molecules>> __p;
  mutable std::mutex __mtx;

public:
  Parser(const std::string &molecule_file, int hm = Options::all_models,
         const int num_occur = -1);
  Parser(const Parser &o) = delete;
  Parser(Parser &&other) : __p(std::move(other.__p)), __mtx{} {
    other.__p = nullptr;
  };
  void rewind();
  bool is_begin() const;
  Molecules parse();
  Molecules parse(const std::string &molecule_file_contents);
};

} // namespace insilab::molib::io
