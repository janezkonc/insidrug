#include "writer.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

template <Writable T> void Writer::write(const T &m) {
  this->write_file(__file, __w->write(m), __hm);
}

template void Writer::write(const Molecule &);
template void Writer::write(const Molecules &);

void Writer::write_file(const std::string file, const std::stringstream &ss,
                        Writer::Options hm) {
  std::ios_base::openmode opts =
      (hm & Writer::Options::open) ? std::ios_base::out : std::ios_base::app;
  inout::file_open_put_stream(file, ss, opts);
}

Writer::Writer(const std::string &file, Writer::Options hm)
    : __file(file), __hm(hm) {
  if (path::has_suffix(file, ".pdb") || path::has_suffix(file, ".pdb.gz") ||
      path::has_suffix(file, ".ent") || path::has_suffix(file, ".ent.gz"))
    __w = std::make_unique<PdbWriter>();
  else if (path::has_suffix(file, ".cif") || path::has_suffix(file, ".cif.gz"))
    __w = std::make_unique<CifWriter>();
  else if (path::has_suffix(file, ".mol2") ||
           path::has_suffix(file, ".mol2.gz"))
    __w = std::make_unique<Mol2Writer>();
  else if (path::has_suffix(file, ".json") ||
           path::has_suffix(file, ".json.gz"))
    __w = std::make_unique<JsonWriter>();
  else if (path::has_suffix(file, ".fasta") ||
           path::has_suffix(file, ".fasta.gz"))
    __w = std::make_unique<FastaWriter>();
  else
    throw helper::Error("[WHOOPS] could not determine type of input file " +
                        file);
}

} // namespace insilab::molib::io
