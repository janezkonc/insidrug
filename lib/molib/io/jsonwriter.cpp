#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "writer.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::molib::io {

/**
 * Json writer saves one Molecule on each line.
 *
 * The 'name' property of Molecules is not saved, its not used anyway, and
 * should be deprecated. Now a Molecules or Molecule is written consistently,
 * while before we had two possibilities:
 *
 * one Molecules per line: 'mols.write("file.json.gz");'
 * one Molecule per line: 'molecule.write("file.json.gz",
 * Writer::Options::append);'
 *
 * This was confusing and it limited num_occur to 1, which was inconvenient.
 */
std::stringstream Writer::JsonWriter::write(const Molecules &mols) {
  std::stringstream ss;
  for (const auto &molecule : mols) {
    ss << this->write(molecule).str();
  }
  return ss;
}

std::stringstream Writer::JsonWriter::write(const Molecule &molecule) {
  std::stringstream ss;
  ss << nlohmann::json(molecule) << std::endl;
  return ss;
}

} // namespace insilab::molib::io
