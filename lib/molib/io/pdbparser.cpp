#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/algorithms/element_type.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/algorithms/structure_integrity.hpp"
#include "molib/bond.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecules.hpp"
#include "parser.hpp"
#include <map>
#include <memory>
#include <ranges>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::vector<std::string> Parser::PdbParser::read_file() {
  std::vector<std::string> pdb_raw;
  dbgmsg("num_occur = " << __num_occur);
  std::tie(pdb_raw, __pos) = inout::read_file(
      __file, __pos, inout::FileNotFound::panic, __num_occur, "^END$");
  return pdb_raw;
}

Molecules Parser::PdbParser::parse(const std::vector<std::string> &pdb_raw) {
  Molecules mols;
  try {
    std::smatch m;

    std::map<std::string, std::vector<std::string>>
        seqres; // chain_id -> sequence
    std::set<Element> experimental;
    std::vector<Residue::Id> missing;
    std::set<Residue::Id> hetero;
    std::map<Residue::Id,
             std::string> modified; // Residue::Id -> standard resn
    std::multimap<std::string, Residue::Id> site;
    std::map<int, std::map<int, geom3d::Matrix>> bio_rota;
    std::map<int, std::vector<std::string>> bio_num_to_segment;
    std::vector<std::string> bio_segment;
    std::map<const Model *, std::map<const int, Atom *>> atom_number_to_atom;
    nlohmann::json comments;
    double resolution{-1.0};

    int biomolecule_number = -1;
    int coord_index = 1;
    std::string molecule_name = "";
    int assembly_id = 0;
    std::string assembly_name = "ASYMMETRIC UNIT";
    int model_id = 1;
    bool read_first_CONECT_then_stop = false, read_CONECT_started = false,
         found_ATOM = false;

    for (const std::string &line : pdb_raw) {
      if (read_first_CONECT_then_stop && read_CONECT_started &&
          !line.starts_with("CONECT"))
        break;
      if ((__hm & Parser::Options::skip_hetatm) && line.starts_with("HETATM"))
        continue;

      if (!read_first_CONECT_then_stop) {
        if (line.starts_with("ATOM") || line.starts_with("HETATM")) {
          if (!found_ATOM) {
            found_ATOM = true;
            mols.add(Molecule({
                .name = molecule_name,
                .seqres = seqres,
                .experimental = experimental,
                .missing = missing,
                .modified = modified,
                .hetero = hetero,
                .site = site,
                .bio_rota = bio_rota,
                .bio_segment = bio_num_to_segment,
                .comments = comments,
                .resolution = resolution,
            }));
            seqres.clear();
            experimental.clear();
            missing.clear();
            modified.clear();
            hetero.clear();
            site.clear();
            bio_rota.clear();
            bio_num_to_segment.clear();
            comments.clear();
            resolution = -1.0;
          }
          std::string record_name = helper::trim(line.substr(0, 6));
          int atom_number = std::stoi(line.substr(6, 5));
          dbgmsg("atom_number = " << atom_number);
          std::string atom_name = helper::trim(line.substr(12, 4));
          char alt_loc = line.at(16);
          std::string resn = helper::trim(line.substr(17, 3));
          std::string chain_id = line.substr(21, 1);
          int resi = std::stoi(line.substr(22, 4));
          char ins_code = line.at(26);
          geom3d::Coordinate crd(std::stod(line.substr(30, 8)),
                                 std::stod(line.substr(38, 8)),
                                 std::stod(line.substr(46, 8)));
          const auto rest = algorithms::determine_protein_nucleic(resn);
          const auto temperature_factor = std::stod(line.substr(60, 6));
          if (line.size() > 77 && (helper::trim(line.substr(76, 2)) == "X" ||
                                   helper::trim(line.substr(76, 2)) == "LP")) {
            // skip atoms with unknown element
            continue;
          }
          const std::string element =
              line.size() > 77
                  ? Element::sanitize(helper::trim(line.substr(76, 2)))
                  : algorithms::determine_element(atom_name, rest);
          const bool hydrogen = (element == "H" || (atom_name.size() == 1 &&
                                                    atom_name.at(0) == 'H'));
          dbgmsg("hydrogen = " << boolalpha << hydrogen);

          if ((__hm & Parser::Options::hydrogens) || !hydrogen) {
            if (alt_loc == ' ' || alt_loc == 'A') {
              Molecule &molecule = mols.last();
              // this could be replaced by a filter function .filter("(protein
              // or nucleic) and name CA") issue #65
              if ((__hm & Parser::Options::sparse_macromol) &&
                  (rest == Residue::Type::protein && atom_name != "CA" ||
                   rest == Residue::Type::nucleic && atom_name != "P"))
                continue;

              // for pdb seg_id and chain_id are the same
              Atom &a = molecule
                            .add(Assembly({.assembly_id = assembly_id,
                                           .name = assembly_name}))
                            .add(Model({.model_id = model_id}))
                            .add(Chain({.chain_id = chain_id}))
                            .add(Segment({.seg_id = chain_id}))
                            .add(Residue({.resn = resn,
                                          .resi = resi,
                                          .ins_code = ins_code,
                                          .rest = rest}))
                            .add(Atom({.atom_number = atom_number,
                                       .atom_name = atom_name,
                                       .crd = crd,
                                       .alt_loc = alt_loc,
                                       .temperature_factor = temperature_factor,
                                       .element = element}));

              Model &model =
                  mols.last()
                      .last()
                      .last(); // can do this as long as we are inserting at the
                               // end of Assembly, ...
              dbgmsg("atom_number_to_atom[" << model.number() << "]["
                                            << atom_number << "] = " << &a);
              atom_number_to_atom[&model][atom_number] = &a;
            }
          }
        } else if (line == "END" ||
                   std::regex_search(line, m, std::regex("END\\s+"))) {
          found_ATOM = false;
        } else if (line.starts_with("MODEL")) {
          if (std::regex_search(line, m, std::regex("MODEL\\s+(\\d+)"))) {
            if (m[1].matched) {
              model_id = std::stoi(m[1].str());
            }
          } else
            throw helper::Error("[WHOOPS] model number not found in pdb");
        } else if (line.starts_with("ENDMDL")) {
          if (__hm & Parser::Options::first_model)
            read_first_CONECT_then_stop = true;
        } else if (line.starts_with("REMARK 101")) {
          comments = nlohmann::json::parse(line.substr(11));
        } else if (line.starts_with("TITLE")) {
          molecule_name += helper::squeeze(helper::trim_right(line.substr(10)));
        } else if (line.starts_with("REMARK 210") ||
                   line.starts_with("REMARK 245") ||
                   line.starts_with("REMARK 280")) {
          for (auto &kv : helper::buffer_ions) { // read the experimental buffer
                                                 // ions
            if (line.find(kv.first) != std::string::npos) {
              experimental.insert(Element(kv.second));
            }
          }
        } else if (line.starts_with("REMARK 350")) {
          if (std::regex_search(
                  line, m, std::regex("REMARK 350 BIOMOLECULE:\\s*(\\d+)"))) {
            if (m[1].matched) {
              biomolecule_number = std::stoi(m[1].str());
            }
            bio_segment.clear();
          } else if (
              std::regex_search(
                  line, m,
                  std::regex(
                      "REMARK 350 APPLY THE FOLLOWING TO "
                      "CHAINS:\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?"
                      "(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?"
                      "\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?")) ||
              std::regex_search(
                  line, m,
                  std::regex(
                      "REMARK 350                    AND "
                      "CHAINS:\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?"
                      "(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?"
                      "\\s?(\\S{1})?,?\\s?(\\S{1})?,?\\s?(\\S{1})?,?"))) {
            if (m.empty()) {
              throw helper::Error("[WHOOPS] problem in REMARK 350 APPLY");
            }
            for (auto &el : m) {
              if (el != *m.begin() && el.matched) {
                // bio_chain.insert(el.str().at(0));
                bio_segment.push_back(el.str());
              }
            }
          } else if (std::regex_search(
                         line, m,
                         std::regex("REMARK 350   "
                                    "BIOMT(\\d+)\\s+(\\d+)\\s+(\\S+)\\s+(\\S+"
                                    ")\\s+(\\S+)\\s+(\\S+)"))) {
            if (m.empty()) {
              throw helper::Error("[WHOOPS] problem in REMARK 350   BIOMT");
            }
            if (m[1].matched && m[2].matched && m[3].matched && m[4].matched &&
                m[5].matched && m[6].matched) {
              if (biomolecule_number == -1) {
                throw helper::Error(
                    "[WHOOPS] missing 'REMARK 350 BIOMOLECULE'");
              }
              int row = std::stoi(m[1].str());
              int rn = std::stoi(m[2].str());
              double a = std::stod(m[3].str());
              double b = std::stod(m[4].str());
              double c = std::stod(m[5].str());
              double w = std::stod(m[6].str());
              if (row == 1) {
                bio_num_to_segment[biomolecule_number] = bio_segment;
              }
              bio_rota[biomolecule_number][rn].set_row(
                  row - 1, std::make_tuple(a, b, c, w));
            }
          }
        } else if (line.starts_with("REMARK 465")) { // missing residues
          try {
            const auto model_no =
                (line.substr(10, 4) == "    " ? 1
                                              : std::stoi(line.substr(10, 4)));
            if (model_no == 1) {
              const auto &resn = line.substr(15, 3);
              const auto &chain_id = line.substr(19, 1);
              const auto resi = std::stoi(line.substr(21, 5));
              const auto ins_code = line.at(26);
              missing.push_back(Residue::Id(chain_id, resn, resi, ins_code));
            }
          } catch (...) {
          }
        } else if (line.starts_with("REMARK   2 ")) { // resolution
          try {
            resolution = std::stod(line.substr(23, 7));
          } catch (...) {
          }
        } else if (
            line.starts_with("SEQRES") &&
            std::regex_search(
                line, m,
                std::regex(
                    R"(^SEQRES.{5}(.{1}).{7}(\S{3})? (\S{3})? (\S{3})? (\S{3})? (\S{3})? (\S{3})? )"
                    R"((\S{3})? (\S{3})? (\S{3})? (\S{3})? (\S{3})? (\S{3})? (\S{3})?)"))) {
          if (m.size() < 3)
            throw helper::Error(
                "[WHOOPS] Wrong format of SEQRES records - check your input!");
          const std::string chain_id = m[1].str();
          if (chain_id == " ")
            throw helper::Error(
                "[WHOOPS] Wrong SEQRES format: Chain ID is not set!");
          for (auto i{2uz}; i < m.size(); ++i) {
            if (m[i].matched) {
              seqres[chain_id].push_back(m[i].str());
            }
          }
        } else if (line.starts_with("HET   ") &&
                   std::regex_search(
                       line, m,
                       std::regex(R"(^HET    (.{3})  (.{1})(.{4})(.{1}))"))) {
          const std::string resn = helper::trim(m[1].str());
          const std::string chain_id = m[2].str();
          const int resi = std::stoi(m[3].str());
          const char ins_code = m[4].str().at(0);
          hetero.insert(Residue::Id(chain_id, resn, resi, ins_code));
        } else if (line.starts_with("MODRES") &&
                   std::regex_search(
                       line, m,
                       std::regex(
                           R"(^MODRES.{6}(\S{3}) (.) (.{4})(.{1}) (\S{3}))"))) {
          if (m.size() != 6)
            throw helper::Error(
                "[WHOOPS] Wrong format of MODRES records - check your input!");

          const auto &resn = m[5].str();

          if (helper::amino_acids.count(resn)) {
            // modified 'standard' residues: MET -> MSE; BUT NOT glycosylated:
            // ASN -> ASN
            const auto &resn_mod = m[1].str();
            if (resn != resn_mod) {
              const auto &chain_id = m[2].str();
              const auto resi = std::stoi(m[3].str());
              const auto ins_code = m[4].str().at(0);
              modified.emplace(Residue::Id{chain_id, resn_mod, resi, ins_code},
                               resn);
            }
          }
        } else if (line.starts_with("SITE") &&
                   std::regex_search(
                       line, m,
                       std::regex("SITE\\s+\\d+\\s+(\\S+)\\s+\\S+(\\s+\\S+"
                                  "\\s?\\S{1}\\s{0,3}\\d{1,4}\\S?)?(\\s+\\S+"
                                  "\\s?\\S{1}\\s{0,3}\\d{1,4}\\S?)?(\\s+\\S+"
                                  "\\s?\\S{1}\\s{0,3}\\d{1,4}\\S?)?(\\s+\\S+"
                                  "\\s?\\S{1}\\s{0,3}\\d{1,4}\\S?)?"))) {
          if (m.empty()) {
            throw helper::Error("[WHOOPS] problem in SITE");
          }
          std::string site_name = (m[1].matched ? m[1].str() : "");
          for (auto &el : m) {
            if (el != *m.begin() && el.matched) {
              std::string s = helper::trim(el.str());
              if (s.size() > 8) {
                std::string resn = s.substr(0, 3);
                std::string chain_id = s.substr(4, 1);
                int resi = std::stoi(s.substr(5, 4));
                char ins_code = (s.size() == 10 ? s.at(9) : ' ');
                if (helper::amino_acids.contains(resn)) {
                  site.insert(std::make_pair(
                      site_name, Residue::Id(chain_id, resn, resi, ins_code)));
                }
              }
            }
          }
        }
      } // not read first conect then stop

      if (line.starts_with("CONECT")) {
        found_ATOM = false;
        if (read_first_CONECT_then_stop)
          read_CONECT_started = true;
        const std::string ln = helper::trim_right(line.substr(6));
        std::vector<int> anum;
        for (int i = 0; i < ln.size(); i += 5) {
          const int atom_number = std::stoi(ln.substr(i, 5));
          anum.push_back(atom_number);
        }
        Molecule &molecule = mols.last(); // the CONECT words are valid for
                                          // the
                                          // whole molecule
        for (const auto &assembly : molecule) {
          for (const auto &model : assembly) {
            std::vector<int>::iterator it = anum.begin();
            const auto atom_iterator1 =
                atom_number_to_atom[&model].find(*anum.begin());
            if (atom_iterator1 == atom_number_to_atom[&model].end())
              continue;
            Atom &a1 = *atom_iterator1->second;
            while (++it != anum.end()) {
              auto atom_iterator2 = atom_number_to_atom[&model].find(*it);
              if (atom_iterator2 == atom_number_to_atom[&model].end())
                continue;
              Atom &a2 = *atom_iterator2->second;
              a1.connect(a2);
            }
          }
        }
      }
    }
    for (const auto &molecule : mols) {
      Bond::connect_bonds(
          details::get_bonds_from<details::get_bonds_t::in_as_set>(molecule));
    }
    if (__hm & Options::panic_bad_structure) {
      if (!algorithms::is_structure_ok(mols))
        throw helper::Error(
            "[WHOOPS] Bad structure detected (check input file)!");
    }
  } catch (const std::exception &e) {
    throw helper::Error("[NOTE] An error occured during reading of pdb file: " +
                        helper::to_string(e.what()));
  }
  return mols;
}

} // namespace insilab::molib::io
