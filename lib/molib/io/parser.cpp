#include "parser.hpp"
#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

void Parser::rewind() { __p->set_pos(0); }
bool Parser::is_begin() const { return __p->get_pos() == 0; }

/**
 * Thread-safe reading of molecules from a file in various formats.
 * @return Molecules object which is convertible to bool, which enables it to be
 * used in eg. while loops (see operator bool in it.hpp).
 */
Molecules Parser::parse() {
  std::lock_guard<std::mutex> guard(__mtx);
  const auto raw_file = __p->read_file();
  return __p->parse(raw_file);
}

Molecules Parser::parse(const std::string &molecule_file_contents) {
  std::vector<std::string> raw_file;
  std::stringstream ss;
  ss << molecule_file_contents;
  for (std::string line; std::getline(ss, line);)
    raw_file.push_back(line);

  return __p->parse(raw_file);
}

Parser::Parser(const std::string &molecule_file, int hm, const int num_occur) {
  if (path::has_suffix(molecule_file, ".pdb") ||
      path::has_suffix(molecule_file, ".pdb.gz") ||
      path::has_suffix(molecule_file, ".ent") ||
      path::has_suffix(molecule_file, ".ent.gz"))
    __p = std::make_unique<PdbParser>(molecule_file, hm, num_occur);
  else if (path::has_suffix(molecule_file, ".cif") ||
           path::has_suffix(molecule_file, ".cif.gz"))
    __p = std::make_unique<CifParser>(molecule_file, hm, num_occur);
  else if (path::has_suffix(molecule_file, ".mol2") ||
           path::has_suffix(molecule_file, ".mol2.gz"))
    __p = std::make_unique<Mol2Parser>(molecule_file, hm, num_occur);
  else if (path::has_suffix(molecule_file, ".sdf") ||
           path::has_suffix(molecule_file, ".sdf.gz"))
    __p = std::make_unique<SdfParser>(molecule_file, hm, num_occur);
  else if (path::has_suffix(molecule_file, ".json") ||
           path::has_suffix(molecule_file, ".json.gz"))
    __p = std::make_unique<JsonParser>(molecule_file, hm, num_occur);
  else
    throw helper::Error("[WHOOPS] could not determine type of input file " +
                        molecule_file);
}

} // namespace insilab::molib::io
