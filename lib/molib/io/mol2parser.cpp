#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecules.hpp"
#include "parser.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::vector<std::string> Parser::Mol2Parser::read_file() {
  std::vector<std::string> mol2_raw;
  std::tie(mol2_raw, __pos) =
      inout::read_file(__file, __pos, inout::FileNotFound::panic, __num_occur,
                       "^@<TRIPOS>MOLECULE.*");
  return mol2_raw;
}

molib::Molecules
Parser::Mol2Parser::parse(const std::vector<std::string> &mol2_raw) {
  Molecules mols;
  try {
    static const std::vector<char> ucodes = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
        'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C',
        'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    std::map<const Model *, std::map<const int, Atom *>> atom_number_to_atom;
    std::map<std::string, int> unique_atom_count; // rename atom names if they
                                                  // are not unique (issue #45)
    for (int i = 0; i < mol2_raw.size(); ++i) {
      const std::string &line = mol2_raw[i];
      if (line.find("@<TRIPOS>ATOM") != std::string::npos) {
        for (i = i + 1; i < mol2_raw.size(); ++i) {
          const std::string &atom_line = mol2_raw[i];
          dbgmsg(atom_line);
          if (atom_line.empty() || atom_line[0] == '@')
            break;
          std::stringstream ss(atom_line);
          int atom_id, subst_id;
          std::string atom_name, sybyl_type, subst_name;
          double x, y, z, charge;
          ss >> atom_id >> atom_name >> x >> y >> z >> sybyl_type >> subst_id >>
              subst_name >> charge;

          const int uc = unique_atom_count[atom_name]++;
          if (uc) {
            dbgmsg("renaming atom " << atom_name << " to make it unique");
            bool ex = false;
            if (atom_name.size() == 1) {
              if (uc == 999)
                ex = true;
              atom_name += helper::to_string(uc);
            } else {
              if (uc == ucodes.size() - 1)
                ex = true;
              atom_name += helper::to_string(ucodes[uc]);
            }
            if (ex)
              throw helper::Error(
                  "[WHOOPS] Too many non-unique atom names, which means "
                  "you need to ensure that atom names will be unique "
                  "within each molecule Mol2 file");

            unique_atom_count[atom_name]++; // save new atom name too
          }

          subst_id = 1; // fix value to prevent multiresidue small molecules
                        // (issue #110)
          subst_name = "LIG"; // prevent idatm typing for standard residues
                              // (issue #111)

          auto sybyl_v = helper::split(sybyl_type, ".");

          // correct eg. S.O2 to S.o2
          if (sybyl_v.size() == 2) {
            std::ranges::transform(sybyl_v[1], sybyl_v[1].begin(), ::tolower);
            sybyl_type = sybyl_v[0] + "." + sybyl_v[1];
          }

          const std::string element = helper::sybyl.count(sybyl_type)
                                          ? helper::sybyl.at(sybyl_type)
                                          : "";
          subst_name = subst_name.substr(0, 3); // truncate longer than 3-lett
                                                // (pde5a bug)

          const bool hydrogen = (element == "H" || (atom_name.size() == 1 &&
                                                    atom_name.at(0) == 'H'));

          dbgmsg("atom_id = "
                 << atom_id << " atom_name = " << atom_name << " x = " << x
                 << " y = " << y << " z = " << z
                 << " sybyl_type = " << sybyl_type << " subst_id = " << subst_id
                 << " subst_name = " << subst_name << " charge = " << charge
                 << " element = " << element << " hydrogen = " << hydrogen);
          geom3d::Coordinate crd(x, y, z);

          if ((__hm & Parser::Options::hydrogens) || !hydrogen) {
            Residue::Type rest(Residue::Type::compound);
            Atom &a =
                mols.last()
                    .add(Assembly())
                    .add(Model())
                    .add(Chain())
                    .add(Segment())
                    .add(Residue(
                        {.resn = subst_name, .resi = subst_id, .rest = rest}))
                    .add(Atom({.atom_number = atom_id,
                               .atom_name = atom_name,
                               .crd = crd,
                               .element = element,
                               .sybyl_type = sybyl_type,
                               .charge = charge}));

            Model &model = mols.last().last().last();
            atom_number_to_atom[&model][atom_id] = &a;
          }
        }
        --i;
      } else if (line.find("@<TRIPOS>MOLECULE") != std::string::npos) {
        if (i + 1 >= mol2_raw.size())
          throw helper::Error("[WHOOPS] wrong format of mol2 file");
        const std::string molecule_name = mol2_raw[i + 1];
        dbgmsg(molecule_name);
        mols.add(Molecule({.name = molecule_name}));
        unique_atom_count.clear();
      } else if (line.find("@<TRIPOS>BOND") != std::string::npos) {
        Molecule &molecule = mols.last(); // the CONECT words are valid for
                                          // the
                                          // whole molecule
        for (i = i + 1; i < mol2_raw.size(); ++i) {
          const std::string &bond_line = mol2_raw[i];
          if (bond_line.empty() || bond_line[0] == '@')
            break;
          int bond_id, origin_atom_id, target_atom_id;
          std::string sybyl_type;
          std::stringstream ss(bond_line);
          ss >> bond_id >> origin_atom_id >> target_atom_id >> sybyl_type;
          for (auto &assembly : molecule) {
            for (auto &model : assembly) {
              auto it1 = atom_number_to_atom[&model].find(origin_atom_id);
              auto it2 = atom_number_to_atom[&model].find(target_atom_id);
              if (it1 != atom_number_to_atom[&model].end() &&
                  it2 != atom_number_to_atom[&model].end()) {
                Atom &a1 = *it1->second;
                Atom &a2 = *it2->second;
                a1.connect(a2).set_sybyl_type(sybyl_type);
              }
            }
          }
        }
        Bond::connect_bonds(
            details::get_bonds_from<details::get_bonds_t::in_as_set>(molecule));
        --i;
      }
    }
  } catch (const std::exception &e) {
    throw helper::Error(
        "[NOTE] An error occured during reading of mol2 file: " +

        helper::to_string(e.what()));
  }
  return mols;
}

} // namespace insilab::molib::io
