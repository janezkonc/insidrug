#include "iomanip.hpp"
#include <iostream>

namespace insilab::molib::io {

std::ostream &IOmanip::reset(std::ostream &os) {
  IOmanip::set_IOtype(os, IOmanip::IOtype::default_t);
  return os;
}

std::ostream &IOmanip::pdb(std::ostream &os) {
  IOmanip::set_IOtype(os, IOmanip::IOtype::pdb_t);
  return os;
}

std::ostream &IOmanip::mol2(std::ostream &os) {
  IOmanip::set_IOtype(os, IOmanip::IOtype::mol2_t);
  return os;
}

std::ostream &IOmanip::cif(std::ostream &os) {
  IOmanip::or_IOtype(os, IOmanip::IOtype::cif_t);
  return os;
}

std::ostream &IOmanip::json(std::ostream &os) {
  IOmanip::or_IOtype(os, IOmanip::IOtype::json_t);
  return os;
}

} // namespace insilab::molib::io
