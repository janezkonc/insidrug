#pragma once

#include <concepts>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::molib {

class Molecules;
class Molecule;
} // namespace insilab::molib

namespace insilab::molib::io {

template <class T>
concept Writable =
    std::is_same<T, Molecule>::value || std::is_same<T, Molecules>::value;

/**
 * A template class that serves as a base class for different writers (eg. to
 * write various molecule formats, probis surface files, etc.).
 *
 * I've tried to make this more general, e.g. by making base class template and
 * have only one 'write' method; however, although it work, the code is
 * complicated and I'm not sure about the benefits..
 */
class WriterBase {
public:
  virtual std::stringstream write(const Molecule &) = 0;
  virtual std::stringstream write(const Molecules &) = 0;
};

class Writer {
public:
  enum Options {
    open = 1, // overwrite file if exists
    append = 2
  };

  class PdbWriter : public WriterBase {
  public:
    using WriterBase::WriterBase;
    std::stringstream write(const Molecules &) override;
    std::stringstream write(const Molecule &) override;
  };
  class CifWriter : public WriterBase {
  public:
    using WriterBase::WriterBase;
    std::stringstream write(const Molecules &) override;
    std::stringstream write(const Molecule &) override;
  };
  class Mol2Writer : public WriterBase {
  public:
    using WriterBase::WriterBase;
    std::stringstream write(const Molecules &) override;
    std::stringstream write(const Molecule &) override;
  };
  class JsonWriter : public WriterBase {
  public:
    using WriterBase::WriterBase;
    std::stringstream write(const Molecules &) override;
    std::stringstream write(const Molecule &) override;
  };
  class FastaWriter : public WriterBase {
  public:
    using WriterBase::WriterBase;
    std::stringstream write(const Molecules &) override;
    std::stringstream write(const Molecule &) override;
  };

private:
  std::unique_ptr<WriterBase> __w;
  const std::string __file;
  const Options __hm;

public:
  Writer(const std::string &file, Options hm = Options::open);
  void write_file(const std::string file, const std::stringstream &ss,
                  Writer::Options hm);

  /**
   * Write a molecule to a file in specified format which is determined from the
   * file suffix. Writing is thread-safe due to use of (thread-safe) inout
   * functions.
   *
   * @param m a Molecule or Molecules object
   */
  template <Writable T> void write(const T &m);
};

} // namespace insilab::molib::io
