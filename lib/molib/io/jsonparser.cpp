#include "inout/inout.hpp"
#include "molib/algorithms/structure_integrity.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "parser.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::vector<std::string> Parser::JsonParser::read_file() {
  std::vector<std::string> json_raw;
  std::tie(json_raw, __pos) = inout::read_file(
      __file, __pos, inout::FileNotFound::panic, __num_occur, "$");
  return json_raw;
}

Molecules Parser::JsonParser::parse(const std::vector<std::string> &json_raw) {
  Molecules mols;
  try {
    for (const auto &line : json_raw) {
      const auto &molecule_j = nlohmann::json::parse(line);
      mols.add(molecule_j.get<Molecule>());
    }
    if (__hm & Options::panic_bad_structure) {
      if (!algorithms::is_structure_ok(mols))
        throw helper::Error(
            "[WHOOPS] Bad structure detected (check input file)!");
    }
  } catch (const std::exception &e) {
    throw helper::Error("[NOTE] An error occured during reading of molecular "
                        "file in Json format: " +
                        helper::to_string(e.what()));
  }
  return mols;
}

} // namespace insilab::molib::io
