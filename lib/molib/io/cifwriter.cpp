#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "writer.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::stringstream Writer::CifWriter::write(const Molecules &mols) {
  std::stringstream ss;
  for (const auto &molecule : mols) {
    ss << this->write(molecule).str();
  }
  return ss;
}

std::stringstream Writer::CifWriter::write(const Molecule &molecule) {
  std::stringstream ss;
  ss << "data_" << molecule.name() << std::endl
     << "#" << std::endl
     << "_entry.id   " << molecule.name() << "#" << std::endl;
  ss << "loop_" << std::endl
     << "_atom_site.group_PDB " << std::endl
     << "_atom_site.id " << std::endl
     << "_atom_site.type_symbol " << std::endl
     << "_atom_site.label_atom_id " << std::endl
     << "_atom_site.label_alt_id " << std::endl
     << "_atom_site.label_comp_id " << std::endl
     << "_atom_site.label_asym_id " << std::endl
     << "_atom_site.label_entity_id " << std::endl
     << "_atom_site.label_seq_id " << std::endl
     << "_atom_site.pdbx_PDB_ins_code " << std::endl
     << "_atom_site.Cartn_x " << std::endl
     << "_atom_site.Cartn_y " << std::endl
     << "_atom_site.Cartn_z " << std::endl
     << "_atom_site.occupancy " << std::endl
     << "_atom_site.B_iso_or_equiv " << std::endl
     << "_atom_site.pdbx_formal_charge " << std::endl
     << "_atom_site.auth_seq_id " << std::endl
     << "_atom_site.auth_comp_id " << std::endl
     << "_atom_site.auth_asym_id " << std::endl
     << "_atom_site.auth_atom_id " << std::endl
     << "_atom_site.pdbx_PDB_model_num " << std::endl;

  for (const auto &patom : molecule.get_atoms()) {
    const auto &atom = *patom;
    const auto &residue = atom.residue();
    const auto &segment = residue.segment();
    const auto &chain = segment.chain();
    const auto &model = chain.model();

    ss << (residue.rest() & molib::Residue::Type::protein ||
                   residue.rest() & molib::Residue::Type::nucleic
               ? "ATOM"
               : "HETATM")
       << " " << atom.atom_number() << " " << atom.element() << " "
       << atom.atom_name() << " "
       << "."
       << " " << residue.resn() << " " << segment.seg_id() << " "
       << "."
       << " " << residue.resi() << " "
       << (residue.ins_code() == ' ' ? '?' : residue.ins_code()) << " "
       << atom.crd().x() << " " << atom.crd().y() << " " << atom.crd().z()
       << " "
       << "1.00"
       << " "
       << "1.00"
       << " "
       << "?"
       << " " << residue.resi() << " " << residue.resn() << " "
       << chain.chain_id() << " " << atom.atom_name() << " " << model.model_id()
       << std::endl;
  }
  return ss;
}

} // namespace insilab::molib::io
