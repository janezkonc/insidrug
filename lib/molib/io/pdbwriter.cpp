#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/model.hpp"
#include "molib/molecules.hpp"
#include "writer.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::stringstream Writer::PdbWriter::write(const Molecules &mols) {
  std::stringstream ss;
  for (const auto &molecule : mols) {
    ss << this->write(molecule).str();
  }
  return ss;
}

std::stringstream Writer::PdbWriter::write(const Molecule &molecule) {
  std::stringstream ss;
  if (!molecule.name().empty()) {
    ss << std::setw(6) << std::left << "TITLE" << std::setw(4) << " "
       << molecule.name() << std::endl;
  }
  if (!(molecule.get_comments().is_null() || molecule.get_comments().empty()))
    ss << "REMARK 101 " << molecule.get_comments().dump() << std::endl;
  for (const auto &assembly : molecule) {
    for (const auto &model : assembly) {
      ss << std::setw(6) << std::left << "MODEL" << std::setw(4) << " "
         << std::setw(4) << std::right << model.model_id() << std::endl;
      for (const auto &chain : model) {
        for (const auto &segment : chain)
          for (const auto &residue : segment)
            for (const auto &atom : residue) {
              ss << std::setw(6) << std::left
                 << (details::is_protein(residue.rest()) ||
                             details::is_nucleic(residue.rest())
                         ? "ATOM"
                         : "HETATM")
                 << std::setw(5) << std::right << atom.atom_number()
                 << std::setw(1) << " " << std::setw(4) << std::left
                 << (atom.atom_name().size() < 4 ? " " + atom.atom_name()
                                                 : atom.atom_name())
                 << std::setw(1) << " " << std::setw(3) << std::right
                 << residue.resn() << std::setw(1) << " " << std::setw(1)
                 << chain.chain_id()[0] // one-letter just for pdb format
                 << std::setw(4) << std::right << residue.resi() << std::setw(1)
                 << residue.ins_code() << std::setw(27) << atom.crd().pdb()
                 << std::setw(6) << std::setprecision(2) << std::fixed
                 << std::right << 1.0 << std::setw(6) << std::setprecision(2)
                 << std::fixed << std::right << atom.temperature_factor()
                 << std::setw(12) << std::right << atom.element().name()
                 << std::setw(2) << std::right << " " << std::endl;
            }
        ss << "TER" << std::endl;
      }
      ss << "ENDMDL" << std::endl;
    }
  }
  std::set<std::pair<int, int>> connected;
  for (const auto &presidue : molecule.get_residues()) {
    const auto &residue = *presidue;
    // don't write conect for standard residues
    if (helper::standard_residues.contains(residue.resn()))
      continue;
    for (const auto &atom : residue) {
      for (const auto &adj_a : atom) {
        if (!connected.count(
                std::make_pair(atom.atom_number(), adj_a.atom_number()))) {
          int bond_order = atom.get_bond(adj_a).get_bo();
          if (bond_order == 0)
            bond_order = 1;
          for (int bo = 0; bo < bond_order; ++bo) {
            ss << "CONECT" << std::setw(5) << std::right << atom.atom_number()
               << std::setw(5) << std::right << adj_a.atom_number()
               << std::endl;
          }
        }
        connected.insert(
            std::make_pair(atom.atom_number(), adj_a.atom_number()));
      }
    }
  }
  ss << "END" << std::endl;
  return ss;
}

} // namespace insilab::molib::io
