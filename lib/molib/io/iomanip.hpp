#pragma once

#include <iostream>

namespace insilab::molib::io {

struct IOmanip {
public:
  enum IOtype {
    default_t = 0,
    pdb_t = 1,
    mol2_t = 2,
    cif_t = 4,
    json_t = 8,
  };

  static void or_IOtype(std::ios_base &s, IOtype o) { flag(s) |= o; }
  static void set_IOtype(std::ios_base &s, IOtype o) { flag(s) = o; }
  static IOtype get_IOtype(std::ios_base &s) {
    return static_cast<IOtype>(flag(s));
  }

  static std::ostream &reset(std::ostream &os);
  static std::ostream &pdb(std::ostream &os);
  static std::ostream &mol2(std::ostream &os);
  static std::ostream &cif(std::ostream &os);
  static std::ostream &json(std::ostream &os);

private:
  static long &flag(std::ios_base &s) {
    static int n = std::ios_base::xalloc();
    return s.iword(n);
  }
};

} // namespace insilab::molib::io
