#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecules.hpp"
#include "parser.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::vector<std::string> Parser::SdfParser::read_file() {
  std::vector<std::string> sdf_raw;
  std::tie(sdf_raw, __pos) = inout::read_file(
      __file, __pos, inout::FileNotFound::panic, __num_occur, "^\\$\\$\\$\\$$");
  return sdf_raw;
}

molib::Molecules
Parser::SdfParser::parse(const std::vector<std::string> &sdf_raw) {
  Molecules mols;
  try {
    static const std::vector<char> ucodes = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
        'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C',
        'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    auto atom_section{false};
    std::map<const Model *, std::map<const int, Atom *>> atom_number_to_atom;
    std::map<std::string, int> unique_atom_count; // rename atom names if they
    // are not unique (issue #45)
    auto atom_id{1};
    for (const auto &line : sdf_raw) {
      if (line.contains("END")) {
        atom_section = false;
        Molecule &molecule = mols.last();
        Bond::connect_bonds(
            details::get_bonds_from<details::get_bonds_t::in_as_set>(molecule));
      } else if (line.contains("V2000")) {
        atom_section = true;
        mols.add(Molecule());
        unique_atom_count.clear();
        atom_id = 1;
      } else if (atom_section && line.size() >= 34) { // read atom section
        const auto crd = geom3d::Coordinate{std::stod(line.substr(0, 10)),
                                            std::stod(line.substr(10, 10)),
                                            std::stod(line.substr(20, 10))};
        const auto element = helper::trim(line.substr(31, 3));
        auto atom_name = element;
        const bool hydrogen = element == "H";
        const int uc = unique_atom_count[atom_name]++;
        if (uc) {
          dbgmsg("renaming atom " << atom_name << " to make it unique");
          bool ex = false;
          if (atom_name.size() == 1) {
            if (uc == 999)
              ex = true;
            atom_name += helper::to_string(uc);
          } else {
            if (uc == ucodes.size() - 1)
              ex = true;
            atom_name += helper::to_string(ucodes[uc]);
          }
          if (ex)
            throw helper::Error(
                "[WHOOPS] Too many non-unique atom names, which means "
                "you need to ensure that atom names will be unique "
                "within each molecule SDF file");

          unique_atom_count[atom_name]++; // save the new atom name too
        }
        if ((__hm & Parser::Options::hydrogens) || !hydrogen) {
          Atom &a = mols.last()
                        .add(Assembly())
                        .add(Model())
                        .add(Chain())
                        .add(Segment())
                        .add(Residue({.resn = "<0>",
                                      .resi = 1,
                                      .rest = Residue::Type::compound}))
                        .add(Atom({
                            .atom_number = atom_id,
                            .atom_name = atom_name,
                            .crd = crd,
                            .element = element,
                        }));

          Model &model = mols.last().last().last();
          atom_number_to_atom[&model][atom_id] = &a;
          ++atom_id;
        }
      } else if (atom_section && line.size() < 34) { // read bond section
        Molecule &molecule = mols.last();
        const int origin_atom_id = std::stoi(line.substr(0, 3)),
                  target_atom_id = std::stoi(line.substr(3, 3)),
                  bond_order = std::stoi(line.substr(6, 3));
        for (auto &assembly : molecule) {
          for (auto &model : assembly) {
            const auto it1 = atom_number_to_atom[&model].find(origin_atom_id);
            const auto it2 = atom_number_to_atom[&model].find(target_atom_id);
            if (it1 != atom_number_to_atom[&model].end() &&
                it2 != atom_number_to_atom[&model].end()) {
              Atom &a1 = *it1->second;
              Atom &a2 = *it2->second;
              a1.connect(a2).set_bo(bond_order);
            }
          }
        }
      }
    }
  } catch (const std::exception &e) {
    throw helper::Error("[NOTE] An error occured while reading SDF file: " +
                        helper::to_string(e.what()));
  }
  return mols;
}

} // namespace insilab::molib::io
