#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "writer.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::stringstream Writer::Mol2Writer::write(const Molecules &mols) {
  std::stringstream ss;
  for (const auto &molecule : mols) {
    ss << this->write(molecule).str();
  }
  return ss;
}

std::stringstream Writer::Mol2Writer::write(const Molecule &molecule) {
  std::stringstream ss;
  // count number of bonds
  int num_bonds = 0;
  const auto atoms = molecule.get_atoms();
  for (auto &patom : atoms) {
    for (auto &adj_a : *patom) {
      if (patom->atom_number() < adj_a.atom_number()) {
        ++num_bonds;
      }
    }
  }

  ss << "@<TRIPOS>MOLECULE" << std::endl
     << molecule.name() << std::endl
     << std::setw(5) << std::right << atoms.size() << std::setw(6) << std::right
     << num_bonds << std::setw(6) << std::right << 0 << std::setw(6)
     << std::right << 0 << std::setw(6) << std::right << 0 << std::endl
     << "SMALL" << std::endl
     << "USER_CHARGES" << std::endl
     << std::endl
     << "@<TRIPOS>ATOM" << std::endl;

  for (const auto &patom : atoms) {
    const auto &atom = *patom;
    const auto &residue = atom.residue();

    ss << std::setw(7) << std::right << atom.atom_number() << " "
       << std::setw(8) << std::left << atom.atom_name() << std::setw(10)
       << std::fixed << std::setprecision(4) << std::right << atom.crd().x()
       << std::setw(10) << std::fixed << std::setprecision(4) << std::right
       << atom.crd().y() << std::setw(10) << std::fixed << std::setprecision(4)
       << std::right << atom.crd().z() << " " << std::setw(6) << std::left
       << atom.sybyl_type() << std::setw(5) << std::right << residue.resi()
       << std::setw(4) << std::right << residue.resn() << std::setw(15)
       << std::right << atom.charge() << std::endl;
  }

  ss << "@<TRIPOS>BOND" << std::endl;
  int i = 1;
  for (auto &patom : atoms) {
    auto &atom = *patom;
    for (auto &adj_a : atom) {
      if (atom.atom_number() < adj_a.atom_number()) {
        int bond_order = atom.get_bond(adj_a).get_bo();
        const std::string &bond_sybyl_type =
            atom.get_bond(adj_a).get_sybyl_type();
        ss << std::setw(6) << std::right << i++ << std::setw(6) << std::right
           << atom.atom_number() << std::setw(6) << std::right
           << adj_a.atom_number() << std::setw(5) << std::right
           << (bond_sybyl_type.empty() ? helper::to_string(bond_order)
                                       : bond_sybyl_type)
           << std::endl;
      }
    }
  }
  ss << std::endl;
  return ss;
}

} // namespace insilab::molib::io
