#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "iomanip.hpp"
#include "molib/algorithms/element_type.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/algorithms/structure_integrity.hpp"
#include "molib/bond.hpp"
#include "molib/molecules.hpp"
#include "parser.hpp"
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib::io {

std::vector<std::string> Parser::CifParser::read_file() {
  std::vector<std::string> cif_raw;
  std::tie(cif_raw, __pos) =
      inout::read_file(__file, __pos, inout::FileNotFound::panic, __num_occur);
  return cif_raw;
}

Molecules Parser::CifParser::parse(const std::vector<std::string> &cif_raw) {
  Molecules mols;
  try {
    std::set<Element> experimental;
    std::vector<Residue::Id> missing;
    std::set<Residue::Id> modified;
    std::multimap<std::string, Residue::Id> site;
    std::map<int, std::map<int, geom3d::Matrix>> bio_rota;
    std::map<int, std::vector<std::string>> bio_num_to_segment;
    bool loop = false;
    std::vector<std::string> names;
    std::map<std::string, std::vector<std::string>> tokens;
    std::map<int, std::map<int, std::tuple<double, double, double, double>>>
        operations;

    for (int i = 0; i < cif_raw.size(); ++i) {
      auto line = cif_raw[i];
      // dbgmsg(line);
      if (line.starts_with(";")) {
        continue;
      }
      if (line.starts_with("loop_")) {
        loop = true;
        dbgmsg("loop = true");
        names.clear();
        continue;
      }
      if (line.starts_with("#")) {
        loop = false;
        dbgmsg("loop = false");
        names.clear();
        continue;
      }
      if (!loop && line.starts_with("_")) { // read tokens not in loop
        std::stringstream ss(line);
        std::string name, token;
        ss >> name >> std::quoted(token, '\'');
        dbgmsg("reading tokens not in loop name, token = " << name << ", "
                                                           << token);
        // some data are in next line starting with semicolon (e.g. 5xnl.cif)
        if (token.empty() && i + 1 < cif_raw.size() &&
            cif_raw[i + 1][0] == ';') {
          line = cif_raw[++i];
          token = line.substr(1);
        }
        if (token.size() == 1 && (token == "." || token == "?")) {
          token = " "; // convert undefined and not applicable to space
        }
        tokens[name].push_back(token);
        continue;
      }
      if (loop && line.starts_with("_")) {
        std::stringstream ss(line);
        std::string name;
        ss >> name;
        names.push_back(name); // so that you know how many are there
        dbgmsg("reading names name = [" << name << "]");
        continue;
      }
      if (loop && !line.starts_with("_")) {
        std::stringstream ss(line);
        for (int j = 0; j < names.size();) {
          std::string token;
          if (ss >> std::quoted(token, '\'')) { // read data in a loop
            dbgmsg("reading tokens in loop name, token = " << names[j] << ", "
                                                           << token);
            if (token.size() == 1 && (token == "." || token == "?")) {
              token = " "; // convert undefined and not applicable to space
            }
            tokens[names[j++]].push_back(token);
          } else {
            // if data spans multiple lines...
            line = cif_raw[++i]; // read the next line
            ss.str("");
            ss.clear();
            ss << line;
          }
        }
      }
    }
    std::string molecule_name =
        tokens["_entry.id"].empty() ? "" : tokens["_entry.id"][0];
    Molecule &molecule = mols.add(Molecule({.name = molecule_name}));

    for (int i = 0; i < tokens["_pdbx_struct_oper_list.id"].size(); ++i) {
      int op = std::stoi(tokens["_pdbx_struct_oper_list.id"][i]);
      operations[op][0] = std::make_tuple(
          std::stod(tokens["_pdbx_struct_oper_list.matrix[1][1]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[1][2]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[1][3]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.vector[1]"][i]));
      operations[op][1] = std::make_tuple(
          std::stod(tokens["_pdbx_struct_oper_list.matrix[2][1]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[2][2]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[2][3]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.vector[2]"][i]));
      operations[op][2] = std::make_tuple(
          std::stod(tokens["_pdbx_struct_oper_list.matrix[3][1]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[3][2]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.matrix[3][3]"][i]),
          std::stod(tokens["_pdbx_struct_oper_list.vector[3]"][i]));
    }

    for (int i = 0; i < tokens["_pdbx_struct_assembly_gen.assembly_id"].size();
         ++i) {
      int biomolecule_number =
          std::stoi(tokens["_pdbx_struct_assembly_gen.assembly_id"][i]);
      auto op_ids = helper::split<int>(
          tokens["_pdbx_struct_assembly_gen.oper_expression"][i], ",", false,
          [](const std::string &s) { return std::stoi(s); });
      bio_num_to_segment[biomolecule_number] = helper::split(
          tokens["_pdbx_struct_assembly_gen.asym_id_list"][i], ",");
      for (auto op : op_ids) {
        bio_rota[biomolecule_number][op].set_row(0, operations[op][0]);
        bio_rota[biomolecule_number][op].set_row(1, operations[op][1]);
        bio_rota[biomolecule_number][op].set_row(2, operations[op][2]);
      }
    }

    molecule.set_bio_segment(bio_num_to_segment);
    molecule.set_bio_rota(bio_rota);

    for (int i = 0; i < tokens["_atom_site.id"].size(); ++i) {
      const auto record_name = tokens["_atom_site.group_PDB"][i];
      const auto atom_number = std::stoi(tokens["_atom_site.id"][i]);
      // use label_atom_id if auth id is missing e.g. in PyMOL generated cif
      const auto atom_name =
          helper::trim(tokens["_atom_site.auth_atom_id"].empty()
                           ? tokens["_atom_site.label_atom_id"][i]
                           : tokens["_atom_site.auth_atom_id"][i],
                       '"');
      const auto alt_loc = tokens["_atom_site.label_alt_id"][i][0];
      // use label_comp_id if auth id is missing e.g. in PyMOL generated cif
      const auto resn = tokens["_atom_site.auth_comp_id"].empty()
                            ? tokens["_atom_site.label_comp_id"][i]
                            : tokens["_atom_site.auth_comp_id"][i];

      const auto rest = algorithms::determine_protein_nucleic(resn);

      const auto element = tokens["_atom_site.type_symbol"][i] == "."
                               ? algorithms::determine_element(atom_name, rest)
                               : tokens["_atom_site.type_symbol"][i];

      const auto seg_id = tokens["_atom_site.label_asym_id"][i];
      const auto chain_id = tokens["_atom_site.auth_asym_id"][i];
      // use label_seq_id if auth id is missing e.g. in PyMOL generated cif
      const auto resi = tokens["_atom_site.auth_seq_id"].empty()
                            ? std::stoi(tokens["_atom_site.label_seq_id"][i])
                            : std::stoi(tokens["_atom_site.auth_seq_id"][i]);
      const auto ins_code = tokens["_atom_site.pdbx_PDB_ins_code"][i][0];
      const auto x_coord = std::stod(tokens["_atom_site.Cartn_x"][i]);
      const auto y_coord = std::stod(tokens["_atom_site.Cartn_y"][i]);
      const auto z_coord = std::stod(tokens["_atom_site.Cartn_z"][i]);
      const auto crd = geom3d::Coordinate(x_coord, y_coord, z_coord);

      const auto model_id =
          std::stoi(tokens["_atom_site.pdbx_PDB_model_num"][i]);

      dbgmsg(record_name << " " << atom_number << " " << element << " "
                         << atom_name << " alt_loc='" << alt_loc << "' " << resn
                         << " seg_id=" << seg_id << " chain_id=" << chain_id
                         << " " << resi << " ins_code='" << ins_code << "' "
                         << x_coord << " " << y_coord << " " << z_coord
                         << " model_id=" << model_id);

      if ((__hm & Parser::Options::skip_hetatm) && record_name == "HETATM")
        continue;

      const bool hydrogen =
          (element == "H" || (atom_name.size() == 1 && atom_name.at(0) == 'H'));
      dbgmsg("hydrogen = " << std::boolalpha << hydrogen);

      if ((__hm & Parser::Options::hydrogens) || !hydrogen) {
        if (alt_loc == ' ' ||
            alt_loc == 'A') { // @issue: properly address alternate locations
          if ((__hm & Parser::Options::sparse_macromol) &&
              (rest == Residue::Type::protein && atom_name != "CA" ||
               rest == Residue::Type::nucleic && atom_name != "P"))
            continue;

          Atom &a = molecule.add(Assembly())
                        .add(Model({.model_id = model_id}))
                        .add(Chain({.chain_id = chain_id}))
                        .add(Segment({.seg_id = seg_id}))
                        .add(Residue({.resn = resn,
                                      .resi = resi,
                                      .ins_code = ins_code,
                                      .rest = rest}))
                        .add(Atom({.atom_number = atom_number,
                                   .atom_name = atom_name,
                                   .crd = crd,
                                   .alt_loc = alt_loc,
                                   .element = element}));
          dbgmsg(a);
        }
      }
    }
    if (__hm & Options::panic_bad_structure) {
      if (!algorithms::is_structure_ok(mols))
        throw helper::Error(
            "[WHOOPS] Bad structure detected (check input file)!");
    }

  } catch (const std::exception &e) {
    throw helper::Error(
        "[NOTE] An error occured during reading of cif file. Errmsg: " +
        helper::to_string(e.what()));
  }
  return mols;
}

} // namespace insilab::molib::io
