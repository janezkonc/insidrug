#include "helper/help.hpp"
#include "molecule.hpp"
#include <regex>

namespace insilab::molib {

void Bond::shallow_copy(const Bond &rhs) {
  this->__idx1 = rhs.__idx1;
  this->__idx2 = rhs.__idx2;
  this->__rotatable = rhs.__rotatable;
  this->__bond_gaff_type = rhs.__bond_gaff_type;
  this->__sybyl_type = rhs.__sybyl_type;
  this->__bo = rhs.__bo;
  this->__ring = rhs.__ring;
  this->__angles = rhs.__angles;
  this->__drive_id = rhs.__drive_id;
}


/////// ENABLE THE LOWER VERSION WHEN ISSUE #118 IS DONE ///////////
bool Bond::is_adjacent(const Bond &other) const {
  if (this == &other)
    return false;
  return atom1().atom_number() == other.atom1().atom_number() ||
         atom2().atom_number() == other.atom2().atom_number() ||
         atom1().atom_number() == other.atom2().atom_number() ||
         atom2().atom_number() == other.atom1().atom_number();
}

// bool Bond::is_adjacent(const Bond &other) const {
//   if (this == &other)
//     return false;
//   return &atom1() == &other.atom1() || &atom2() == &other.atom2() ||
//          &atom1() == &other.atom2() || &atom2() == &other.atom1();
// }

double Bond::length() const { return atom1().crd().distance(atom2().crd()); }

void Bond::connect_bonds(const Bond::PSet &bonds) {
  for (auto &pbond1 : bonds) {
    dbgmsg("bond is " << *pbond1);
    Bond::PSet connected;
    for (auto &pbond2 : pbond1->atom1().get_bonds()) {
      dbgmsg("    bond of atom1 = " << *pbond2);
      if (pbond1 != pbond2)
        connected.insert(pbond2);
    }
    for (auto &pbond2 : pbond1->atom2().get_bonds()) {
      dbgmsg("    bond of atom2 = " << *pbond2);
      if (pbond1 != pbond2)
        connected.insert(pbond2);
    }
    for (auto &pbond : connected)
      pbond1->add(pbond);
    dbgmsg("conn = " << std::endl << connected << std::endl << "end conn");
  }
  dbgmsg("connected bonds : " << std::endl
                              << bonds << std::endl
                              << "----------------");
}

void Bond::erase_bonds(const Bond::PSet &bonds) {
  for (auto &pbond : bonds) {
    pbond->clear();
  }
}
void Bond::erase_stale_refs(const Bond &deleted_bond, const Bond::PVec &bonds) {
  for (auto &pbond : bonds) {
    auto &bond = *pbond;
#ifndef NDEBUG
    dbgmsg("bonds before deletion");
    for (auto &adj : bond)
      dbgmsg(adj);
#endif
    for (int j = 0; j < bond.size(); ++j) {
      if (&deleted_bond == &bond[j]) {
        dbgmsg("deleting reference to bond"
               << std::endl
               << " bond " << bond << " is no longer connected to bond "
               << bond[j]);
        bond.erase(j--);
      }
    }
#ifndef NDEBUG
    dbgmsg("bonds after deletion");
    for (auto &adj : bond)
      dbgmsg(adj);
#endif
  }
}

std::ostream &operator<<(std::ostream &stream, const Bond &b) {
  return stream << "(address = " << &b << " atom1 = "
                << (b.__atom1 ? helper::to_string(b.atom1().atom_number())
                              : "nullptr")
                << ", atom2 = "
                << (b.__atom2 ? helper::to_string(b.atom2().atom_number())
                              : "nullptr")
                << ", bond_gaff_type = " << b.get_bond_gaff_type()
                << ", rotatable = " << b.get_rotatable()
                << ", bo = " << b.get_bo() << ", aromatic = " << std::boolalpha
                << b.is_aromatic() << ")" << std::noboolalpha;
}
std::ostream &operator<<(std::ostream &stream, const Bond::PVec &bonds) {
  for (const auto &pbond : bonds) {
    stream << " bond : " << *pbond << std::endl;
    for (const auto &bond2 : *pbond)
      stream << "       is connected to : " << bond2 << std::endl;
  }
  return stream;
}
std::ostream &operator<<(std::ostream &stream, const Bond::PSet &bonds) {
  Bond::PVec bv(bonds.begin(), bonds.end());
  return stream << bv;
}

} // namespace insilab::molib
