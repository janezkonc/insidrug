#pragma once

#include "assembly.hpp"
#include "details/common.hpp"
#include "details/utils.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "grid/gridwrapper.hpp"
#include "helper/it.hpp"
#include "jsonio.hpp" // gives Json powers
#include "nlohmann/json.hpp"
#include "residue.hpp"
#include <map>

namespace insilab::molib {

class Molecules;
class Molecule;
class Assembly;
class Model;
class Chain;
class Residue;
class Atom;

class Molecule : public helper::template_map_container<Assembly, Molecule>,
                 public details::Utils<Molecule> {
public:
  using Writer = io::Writer; // declare molecule writer
  using PVec = std::vector<Molecule *>;
  using Grid = grid::Grid<grid::GridAdapter<Molecule *>>;
  using GridWrapped = std::vector<grid::GridAdapter<Molecule *>>;
  enum class BioHowMany { first_bio, all_bio, asym };

  struct Params {
    std::string name{}; // usually pdb file
    std::map<std::string, std::vector<std::string>>
        seqres{}; // chain_id -> sequence
    std::set<Element> experimental{};
    std::vector<Residue::Id> missing{};
    std::map<Residue::Id,
             std::string> modified; // Residue::Id -> standard resn
    std::set<Residue::Id> hetero{};
    std::multimap<std::string, Residue::Id> site{};
    std::map<int, std::map<int, geom3d::Matrix>> bio_rota{};
    std::map<int, std::vector<std::string>> bio_segment{};
    nlohmann::json comments{};
    double resolution{};
  };

private:
  Params __params;

public:
  Molecule() = default;
  Molecule(const Params &p) : __params(p) {}
  Molecule(Molecule &&rhs) = default;
  Molecule(const Molecule &rhs); // deep copy
  Molecule(const molib::Molecule &rhs, const geom3d::Point::Vec &crds);
  template <details::IsAtomVecOrSet T> Molecule(const T &atoms);

  const Params &params() const { return __params; }

  void shallow_copy(const Molecule &rhs);
  Molecule &operator=(const Molecule &rhs);
  Assembly &asym() { return this->first(); }

  Molecule &set_experimental(const std::set<Element> &e) {
    __params.experimental = e;
    return *this;
  }
  const auto &get_experimental() const { return __params.experimental; }

  Molecule &set_seqres(const auto &s) {
    __params.seqres = s;
    return *this;
  }
  const auto is_seqres(const auto &resn) const {
    for (const auto &[chain_id, seqres] : __params.seqres) {
      if (std::find(std::begin(seqres), std::end(seqres), resn) !=
          std::end(seqres))
        return true;
    }
    return false;
  }
  Molecule &set_missing(const std::vector<Residue::Id> &m) {
    __params.missing = m;
    return *this;
  }
  const std::vector<Residue::Id> &get_missing() const {
    return __params.missing;
  }

  Molecule &set_modified(const auto &m) {
    __params.modified = m;
    return *this;
  }
  const auto is_modified(const Residue::Id &r) const {
    return __params.modified.contains(r);
  }

  const std::optional<std::string>
  get_standard_resn_for_modified(const std::string &resn_mod) const {
    for (const auto &[rid, resn] : __params.modified) {
      if (resn_mod == std::get<1>(rid))
        return resn;
    }
    return {};
  }

  const auto &get_params() const { return __params; }

  Molecule &set_hetero(const std::set<Residue::Id> &m) {
    __params.hetero = m;
    return *this;
  }
  const auto is_hetero(Residue::Id r) const { return __params.hetero.count(r); }

  Molecule &set_site(const std::multimap<std::string, Residue::Id> &s) {
    __params.site = s;
    return *this;
  }

  Molecule &set_name(const std::string &name) {
    __params.name = name;
    return *this;
  }
  const std::string &name() const { return __params.name; }

  Molecule &regenerate_bonds(const Molecule &);

  Assembly &add(Assembly a) {
    const auto i = a.assembly_id();
    return this->insert(i, std::move(a), this);
  }

  Molecule generate_bio(BioHowMany bhm = Molecule::BioHowMany::all_bio,
                        const bool fallback_to_asymmetric = false) const;
  Molecule &
  set_bio_rota(const std::map<int, std::map<int, geom3d::Matrix>> &b) {
    __params.bio_rota = b;
    return *this;
  }
  Molecule &set_bio_segment(const std::map<int, std::vector<std::string>> &b) {
    __params.bio_segment = b;
    return *this;
  }

  Molecule &set_comments(const nlohmann::json &comments) {
    __params.comments = comments;
    return *this;
  }
  const auto &get_comments() const { return __params.comments; }
  const auto get_resolution() const { return __params.resolution; }

  friend std::ostream &operator<<(std::ostream &stream, const Molecule &m);
};

} // namespace insilab::molib
