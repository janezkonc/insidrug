#pragma once

#include "assembly.hpp"
#include "atom.hpp"
#include "chain.hpp"
#include "details/common.hpp"
#include "details/utils.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "grid/grid.hpp"
#include "helper/help.hpp"
#include "helper/it.hpp"
#include "io/parser.hpp"
#include "model.hpp"
#include "molecule.hpp"
#include "residue.hpp"

namespace insilab::molib {

class Molecules : public helper::template_map_container<Molecule, Molecules>,
                  public details::Utils<Molecules> {
public:
  using Parser = io::Parser; // declare molecules reader
  using Writer = io::Writer; // declare molecules writer

  struct Params {
    std::string name{}; // nr-pdb name
  };

private:
  Params __params;

public:
  Molecules() = default;
  Molecules(const Params &p) : __params(p) {}
  template <details::IsAtomVecOrSet T> Molecules(const T &atoms);

  const Params &params() const { return __params; }

  void shallow_copy(const Molecules &rhs);
  Molecule &add(Molecule m) { return this->insert(std::move(m), this); }
  Molecules &set_name(const std::string &name) {
    __params.name = name;
    return *this;
  }
  const std::string &name() const { return __params.name; }
  friend std::ostream &operator<<(std::ostream &stream, const Molecules &m);
};

} // namespace insilab::molib
