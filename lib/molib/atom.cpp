#include "atom.hpp"
#include "bond.hpp"
#include "molecule.hpp"
#include "io/writer.hpp"
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {

std::ostream &operator<<(std::ostream &stream, const Atom &atom) {
  stream << "atom.atom_number = " << atom.atom_number()
         << " atom.atom_name = " << atom.atom_name()
         << " atom.crd = " << atom.crd() << " atom.element = " << atom.element()
         << " atom.idatm_type = " << atom.idatm_type_unmask()
         << " atom.smiles_label = " << atom.get_smiles_label();
  return stream;
}

std::ostream &operator<<(std::ostream &stream, const Atom::PSet &atoms) {
  stream << io::Writer::PdbWriter().write(Molecule(atoms)).str();
  return stream;
}
std::ostream &operator<<(std::ostream &stream, const Atom::PVec &atoms) {
  stream << io::Writer::PdbWriter().write(Molecule(atoms)).str();
  return stream;
}

int Atom::get_num_hydrogens() const {
  const Atom &atom = *this;
  int num_h = 0;
  for (const auto &atom2 : atom)
    if (atom2.element() == Element::H)
      num_h++;
  return num_h;
}

Bond &Atom::connect(Atom &a2) {
  Atom &a1 = *this;
  if (!a1.is_adjacent(a2)) {
    dbgmsg("connecting atoms " << a1.atom_number() << " and "
                               << a2.atom_number());
    a1.add(&a2);
    a2.add(&a1);
    const auto bond = Bond::create(a1, a2);
    a1.insert_bond(a2, bond);         // insert if not exists
    return *a2.insert_bond(a1, bond); // insert if not exists
  }
  return a1.get_bond(a2); // if already connected return existing bond
}

int Atom::compute_num_property(const std::string &prop) const {
  if (prop == "B")
    return this->size();
  else if (prop == "H")
    return this->get_num_hydrogens();
  else if (prop == "sb" || prop == "db" || prop == "tb" ||
           //~ prop == "SB" || prop == "DB" || prop == "TB" || prop == "AB")
           prop == "SB" || prop == "DB" || prop == "DL")
    return this->get_num_bond_with_bond_gaff_type(prop);
  else if (prop.substr(0, 2) == "AR" || prop.substr(0, 2) == "RG" ||
           prop.substr(0, 2) == "NG" || prop.substr(0, 2) == "ag")
    return this->get_num_property(prop);
  throw helper::Error("[WHOOPS] Invalid atom property");
}

int Atom::get_num_bond_with_bond_gaff_type(const std::string &prop) const {
  const Atom &atom = *this;
  int num_bwp = 0;
  for (auto &pbond : atom.get_bonds())
    if (pbond->get_bond_gaff_type() == prop)
      num_bwp++;
  return num_bwp;
}

} // namespace insilab::molib
