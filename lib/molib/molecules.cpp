#include "molecules.hpp"
#include "assembly.hpp"
#include "atom.hpp"
#include "bond.hpp"
#include "chain.hpp"
#include "details/common.hpp"
#include "io/iomanip.hpp"
#include "model.hpp"
#include "molecule.hpp"
#include "residue.hpp"
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {

std::ostream &operator<<(std::ostream &stream, const Molecules &mols) {

  using namespace io;

  const IOmanip::IOtype io = IOmanip::get_IOtype(stream);

  if ((io == IOmanip::IOtype::default_t) || (io & IOmanip::IOtype::pdb_t)) {
    stream << Writer::PdbWriter().write(mols).str();
  } else if (io == IOmanip::IOtype::mol2_t) {
    stream << Writer::Mol2Writer().write(mols).str();
  } else if (io == IOmanip::IOtype::cif_t) {
    stream << Writer::CifWriter().write(mols).str();
  } else if (io == IOmanip::IOtype::json_t) {
    stream << Writer::JsonWriter().write(mols).str();
  }
  return stream;
}

void Molecules::shallow_copy(const Molecules &rhs) { __params = rhs.__params; }

/**
 * This constructor enables to create a new (set of) molecules based on selected
 * atoms.
 */
template <details::IsAtomVecOrSet T> Molecules::Molecules(const T &atoms) {
  if (atoms.empty())
    return;
  this->shallow_copy((*atoms.begin())->molecules());
  std::map<Molecule *, Atom::PVec> mapping;
  for (const auto &patom : atoms) {
    mapping[&patom->molecule()].push_back(patom);
  }
  for (const auto &[_unused, patoms] : mapping) {
    this->add(Molecule(patoms));
  }
}

template Molecules::Molecules(const Atom::PSet &);
template Molecules::Molecules(const Atom::PVec &);

} // namespace insilab::molib
