#pragma once

#include "atom.hpp"
#include "chain.hpp"
#include "details/utils.hpp"
#include "element.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "grid/grid.hpp"
#include "helper/it.hpp"
#include "residue.hpp"

namespace insilab::molib {

class Chain;
class Residue;
class Atom;
class Assembly;

class Model : public helper::template_map_container<Chain, Model, std::string>,
              public details::Utils<Model> {
public:
  struct Params {
    int model_id{1};
  };
  using PVec = std::vector<Model *>;
  using CPVec = std::vector<const Model *>;
  using PSet = std::set<Model *>;
  using CPSet = std::set<const Model *>;

private:
  Params __params;

public:
  Model() = default;
  Model(const Params &p) : __params(p) {}

  const Params &params() const { return __params; }
  Chain &add(Chain chain) {
    const auto i = chain.chain_id();
    return this->insert(i, std::move(chain), this);
  }
  Chain &chain(const std::string &chain_id) const {
    return this->element(chain_id);
  }
  bool has_chain(const std::string &chain_id) const {
    return this->has_element(chain_id);
  }
  int model_id() const { return __params.model_id; }
};

} // namespace insilab::molib
