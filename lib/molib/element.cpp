#include "element.hpp"
#include "helper/error.hpp"
#include <algorithm>
#include <numeric>

namespace insilab::molib {

const std::vector<double> Element::__vdw_radius{
    1.20, // H
    1.20, // D
    1.20, // T
    1.40, // He
    1.82, // Li
    1.53, // Be
    1.92, // B
    1.70, // C
    1.55, // N
    1.52, // O
    1.47, // F
    1.54, // Ne
    2.27, // Na
    1.73, // Mg
    1.84, // Al
    2.10, // Si
    1.80, // P
    1.80, // S
    1.75, // Cl
    1.88, // Ar
    2.75, // K
    2.31, // Ca
    2.11, // Sc
    2.0,  // Ti // a mock value
    2.0,  // V // a mock value
    2.0,  // Cr // a mock value
    2.0,  // Mn // a mock value
    2.0,  // Fe // a mock value
    2.0,  // Co // a mock value
    1.63, // Ni
    1.40, // Cu
    1.39, // Zn
    1.87, // Ga
    2.11, // Ge
    1.85, // As
    1.90, // Se
    1.85, // Br
    2.02, // Kr
    3.03, // Rb
    2.49, // Sr
    2.0,  // Y // a mock value
    2.0,  // Zr // a mock value
    2.0,  // Nb // a mock value
    2.0,  // Mo // a mock value
    2.0,  // Tc // a mock value
    2.0,  // Ru // a mock value
    2.0,  // Rh // a mock value
    1.63, // Pd
    1.72, // Ag
    1.58, // Cd
    1.93, // In
    2.17, // Sn
    2.06, // Sb
    2.06, // Te
    1.98, // I
    2.16, // Xe
    3.43, // Cs
    2.68, // Ba
    2.0,  // La // a mock value
    2.0,  // Ce // a mock value
    2.0,  // Pr // a mock value
    2.0,  // Nd // a mock value
    2.0,  // Pm // a mock value
    2.0,  // Sm // a mock value
    2.0,  // Eu // a mock value
    2.0,  // Gd // a mock value
    2.0,  // Tb // a mock value
    2.0,  // Dy // a mock value
    2.0,  // Ho // a mock value
    2.0,  // Er // a mock value
    2.0,  // Tm // a mock value
    2.0,  // Yb // a mock value
    2.0,  // Lu // a mock value
    2.0,  // Hf // a mock value
    2.0,  // Ta // a mock value
    2.0,  // W // a mock value
    2.0,  // Re // a mock value
    2.0,  // Os // a mock value
    2.0,  // Ir // a mock value
    1.75, // Pt
    1.66, // Au
    1.55, // Hg
    1.96, // Tl
    2.02, // Pb
    2.07, // Bi
    1.97, // Po
    2.02, // At
    2.20, // Rn
    3.48, // Fr
    2.83, // Ra
    2.0,  // Ac // a mock value
    2.0,  // Th // a mock value
    2.0,  // Pa // a mock value
    1.86, // U
    2.0,  // Np // a mock value
    2.0,  // Pu // a mock value
    2.0,  // Am // a mock value
    2.0,  // Cm // a mock value
    2.0,  // Bk // a mock value
    2.0,  // Cf // a mock value
    2.0,  // Es // a mock value
    2.0,  // Fm // a mock value
    2.0,  // Md // a mock value
    2.0,  // No // a mock value
    2.0,  // Lr // a mock value
    2.0,  // Rf // a mock value
    2.0,  // Db // a mock value
    2.0,  // Sg // a mock value
    2.0,  // Bh // a mock value
    2.0,  // Hs // a mock value
    2.0,  // Mt // a mock value
    2.0,  // Ds // a mock value
    2.0,  // Rg // a mock value
    2.0,  // Cn // a mock value
    2.0,  // Nh // a mock value
    2.0,  // Fl // a mock value
    2.0,  // Mc // a mock value
    2.0,  // Lv // a mock value
    2.0,  // Ts // a mock value
    2.0,  // Og // a mock value
};

const std::vector<std::string> Element::__atom_symbol{
    "H",  "D",  "T",  "He", "Li", "Be", "B",  "C",  "N",  "O",  "F",  "Ne",
    "Na", "Mg", "Al", "Si", "P",  "S",  "Cl", "Ar", "K",  "Ca", "Sc", "Ti",
    "V",  "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se",
    "Br", "Kr", "Rb", "Sr", "Y",  "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd",
    "Ag", "Cd", "In", "Sn", "Sb", "Te", "I",  "Xe", "Cs", "Ba", "La", "Ce",
    "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb",
    "Lu", "Hf", "Ta", "W",  "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb",
    "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U",  "Np", "Pu",
    "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg",
    "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og",
};

Element::atom_index Element::__get_index(const std::string &name) {
  auto temp{name};
  std::erase_if(temp, [](char const &c) { return std::isspace(c); });
  if (temp.empty())
    throw helper::Error(
        "[WHOOPS] Could not determine element symbol from:" + name + "!");

  // deal with nice, clean element, e.g., Na, Ca, C, H, N, B, Ba
  if ((temp.size() == 1 && std::isupper(temp[0])) ||
      (temp.size() == 2 && std::isupper(temp[0]) && std::islower(temp[1]))) {
    for (auto i = 0uz; i < __atom_symbol.size(); ++i) {
      if (__atom_symbol[i] == temp) // exact match!
        return atom_index(i);
    }
  }
  // probably we are dealing with atom name, e.g., CAB, C1B, NA, NB etc.
  if (temp.size() > 2)
    temp.resize(2);
  temp = sanitize(temp);
  // CAB could be C or Ca - can we determine element unambiguously?
  std::vector<std::size_t> alternatives;
  for (auto i = 0uz; i < __atom_symbol.size(); ++i) {
    if (temp.starts_with(__atom_symbol[i]))
      alternatives.push_back(i);
  }
  if (alternatives.empty()) {
    throw helper::Error(
        "[WHOOPS] Could not determine element symbol from: " + name + "!");
  } else if (alternatives.size() > 1) {
    const auto s = std::accumulate(
        std::next(alternatives.begin()), alternatives.end(),
        __atom_symbol.at(alternatives.at(0)), [](std::string a, std::size_t b) {
          return std::move(a) + " or " + __atom_symbol.at(b);
        });

    throw helper::Error(
        "[WHOOPS] Ambigous element symbols could be determined from: " + name +
        " (" + s + ")!");
  }
  return atom_index(alternatives.at(0));
}

std::ostream &operator<<(std::ostream &os, const Element &a) {
  os << a.name();
  return os;
}

std::string Element::sanitize(std::string s) {
  if (s.empty() || s.size() > 2)
    throw helper::Error("[WHOOPS] Cannot sanitize element symbols that are not "
                        "1- or 2-letter (" +
                        s + ")!");
  s[0] = std::toupper(s[0]);
  if (s.size() > 1)
    s[1] = std::tolower(s[1]);
  return s;
}

} // namespace insilab::molib
