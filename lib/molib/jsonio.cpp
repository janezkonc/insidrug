#include "jsonio.hpp"
#include "molib/bond.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "nlohmann/json.hpp"

namespace insilab::molib {

void from_json(const nlohmann::json &molecule_j, Molecule &molecule) {
  std::map<std::pair<Model *, int>, Atom *> atoms;

  molecule.set_name(molecule_j.at("molecule_name"));
  molecule.set_comments(molecule_j.at("comments"));
  for (const auto &assembly_j : molecule_j.at("assemblies")) {
    auto &assembly =
        molecule.add(Assembly({.assembly_id = assembly_j.at("assembly_id"),
                               .name = assembly_j.at("assembly_name")}));
    for (const auto &model_j : assembly_j.at("models")) {
      auto &model = assembly.add(Model({.model_id = model_j.at("model_id")}));
      for (const auto &chain_j : model_j.at("chains")) {
        auto &chain = model.add(Chain({.chain_id = chain_j.at("chain_id")}));
        for (const auto &segment_j : chain_j.at("segments")) {
          auto &segment =
              chain.add(Segment({.seg_id = segment_j.at("segment_id")}));
          for (const auto &residue_j : segment_j.at("residues")) {
            auto &residue = segment.add(Residue({
                .resn = residue_j.at("resn"),
                .resi = residue_j.at("resi"),
                .ins_code = residue_j.at("ins_code").get<char>(),
                .rest = residue_j.at("rest").get<Residue::Type>(),
            }));
            for (const auto &atom_j : residue_j.at("atoms")) {
              // atomic penalty scores parsing (not in from_json, since its
              // signature is too general)
              std::map<int, int> aps;
              for (const auto &aps_j : atom_j.at("aps")) {
                aps[aps_j.at(0)] = aps_j.at(1);
              }
              auto &atom = residue.add(Atom({
                  .atom_number = atom_j.at("atom_number"),
                  .atom_name = atom_j.at("atom_name"),
                  .crd = atom_j.at("crd").get<geom3d::Point>(),
                  .alt_loc = atom_j.at("alt_loc").get<char>(),
                  .temperature_factor =
                      atom_j.contains("temperature_factor")
                          ? atom_j.at("temperature_factor").get<double>()
                          : 1.0, // for older files without b-factors
                  .element = atom_j.at("element").get<std::string>(),
                  .idatm_type = helper::idatm_mask.at(atom_j.at("idatm")),
                  .sybyl_type = atom_j.at("sybyl"),
                  .gaff_type = atom_j.at("gaff"),
                  .smiles_prop = atom_j.at(
                      "prop"), // smiles prop is converted automatically
                  .aps = aps,
              }));
              atoms[{&model, atom_j.at("atom_number")}] = &atom;
            }
          }
        }
      }
    }
  }
  // bond parsing comes after molecule parsing, since we need references to
  // atoms
  for (const auto &bond_j : molecule_j.at("bonds")) {
    for (const auto &pmodel : molecule.get_models()) {
      if (!atoms.contains({pmodel, bond_j.at("atom1")}) ||
          !atoms.contains({pmodel, bond_j.at("atom2")}))
        continue;
      Atom &atom1 = *atoms.at({pmodel, bond_j.at("atom1")});
      Atom &atom2 = *atoms.at({pmodel, bond_j.at("atom2")});
      atom1.connect(atom2)
          .set_bo(bond_j.at("bond_order"))
          .set_bond_gaff_type(bond_j.at("gaff"))
          .set_sybyl_type(bond_j.at("sybyl"))
          .set_rotatable(bond_j.at("rotatable"));
    }
  }
}

/**
 * Since bonds are given for the whole Molecule, several atoms
 * may have the same atom numbers, eg. when there are multiple Models or
 * Assemblies within a Molecule. It is therefore checked that the bonds that are
 * outputted to Json have unique atom numbers.
 */
void to_json(nlohmann::json &bonds_j, const Bond::PVec &bonds) {
  std::set<std::pair<int, int>> unique;
  for (const auto &pbond : bonds) {
    const auto &bond = *pbond;
    const auto a1 = bond.atom1().atom_number();
    const auto a2 = bond.atom2().atom_number();
    if (unique.contains({a1, a2}) || unique.contains({a2, a1}))
      continue;
    unique.emplace(a1, a2);
    bonds_j.push_back({
        {"atom1", a1},
        {"atom2", a2},
        {"rotatable", bond.is_rotatable() ? bond.get_rotatable() : ""},
        {"bond_order", bond.get_bo()},
        {"gaff", bond.get_bond_gaff_type()},
        {"sybyl", bond.get_sybyl_type()},
    });
  }
}

void to_json(nlohmann::json &molecule_j, const Molecule &molecule) {
  molecule_j = nlohmann::json{
      {"molecule_name", molecule.name()},
      {"comments", molecule.get_comments()},
      {"bonds", details::get_bonds_from<details::get_bonds_t::in>(
                    molecule)}, // atom numbers are restarted in
                                // each Model, therefore
      // {"bonds", molecule.bonds()}, // to be implemented (see issue #81)
  };
  for (const auto &assembly : molecule) {
    nlohmann::json assembly_j{
        {"assembly_name", assembly.name()},
        {"assembly_id", assembly.assembly_id()},
    };
    for (const auto &model : assembly) {
      nlohmann::json model_j{
          {"model_id", model.model_id()},
      };
      for (const auto &chain : model) {
        nlohmann::json chain_j{
            {"chain_id", chain.chain_id()},
        };
        for (const auto &segment : chain) {
          nlohmann::json segment_j{
              {"segment_id", segment.seg_id()},
          };
          for (const auto &residue : segment) {
            nlohmann::json residue_j{
                {"resn", residue.resn()},
                {"resi", residue.resi()},
                {"ins_code", residue.ins_code()},
                {"rest", residue.rest()},
            };
            for (const auto &atom : residue) {
              residue_j["atoms"].push_back({
                  {"atom_number", atom.atom_number()},
                  {"atom_name", atom.atom_name()},
                  {"crd", atom.crd()},
                  {"alt_loc", atom.alt_loc()},
                  {"temperature_factor", atom.temperature_factor()},
                  {"element", atom.element().name()},
                  {"sybyl", atom.sybyl_type()},
                  {"idatm", atom.idatm_type_unmask()},
                  {"gaff", atom.gaff_type()},
                  {"aps", atom.get_aps()}, // converts to list of pairs
                  {"prop", atom.get_smiles_prop()},
              });
            }
            segment_j["residues"].push_back(residue_j);
          }
          chain_j["segments"].push_back(segment_j);
        }
        model_j["chains"].push_back(chain_j);
      }
      assembly_j["models"].push_back(model_j);
    }
    molecule_j["assemblies"].push_back(assembly_j);
  }
}

} // namespace insilab::molib
