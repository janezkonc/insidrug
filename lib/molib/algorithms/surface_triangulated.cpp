#include "surface_triangulated.hpp"
#include "geom3d/linear.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include <cstdint>
#include <exception>
#include <iostream>

namespace insilab::molib::algorithms {

static const std::vector<int32_t> edgeTable = {
    0x0,   0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c, 0x80c, 0x905, 0xa0f,
    0xb06, 0xc0a, 0xd03, 0xe09, 0xf00, 0x190, 0x99,  0x393, 0x29a, 0x596, 0x49f,
    0x795, 0x69c, 0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90, 0x230,
    0x339, 0x33,  0x13a, 0x636, 0x73f, 0x435, 0x53c, 0xa3c, 0xb35, 0x83f, 0x936,
    0xe3a, 0xf33, 0xc39, 0xd30, 0x3a0, 0x2a9, 0x1a3, 0xaa,  0x7a6, 0x6af, 0x5a5,
    0x4ac, 0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0, 0x460, 0x569,
    0x663, 0x76a, 0x66,  0x16f, 0x265, 0x36c, 0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a,
    0x963, 0xa69, 0xb60, 0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff,  0x3f5, 0x2fc,
    0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0, 0x650, 0x759, 0x453,
    0x55a, 0x256, 0x35f, 0x55,  0x15c, 0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53,
    0x859, 0x950, 0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc,  0xfcc,
    0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0, 0x8c0, 0x9c9, 0xac3, 0xbca,
    0xcc6, 0xdcf, 0xec5, 0xfcc, 0xcc,  0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9,
    0x7c0, 0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c, 0x15c, 0x55,
    0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650, 0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6,
    0xfff, 0xcf5, 0xdfc, 0x2fc, 0x3f5, 0xff,  0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
    0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c, 0x36c, 0x265, 0x16f,
    0x66,  0x76a, 0x663, 0x569, 0x460, 0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af,
    0xaa5, 0xbac, 0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa,  0x1a3, 0x2a9, 0x3a0, 0xd30,
    0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c, 0x53c, 0x435, 0x73f, 0x636,
    0x13a, 0x33,  0x339, 0x230, 0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895,
    0x99c, 0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99,  0x190, 0xf00, 0xe09,
    0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c, 0x70c, 0x605, 0x50f, 0x406, 0x30a,
    0x203, 0x109, 0x0};

static const std::vector<int32_t> triTable = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  8,  3,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  1,  9,  -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1,  8,  3,  9,  8,  1,  -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, 1,  2,  10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 0,  8,  3,  1,  2,  10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 9,  2,  10, 0,  2,  9,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2,  8,
    3,  2,  10, 8,  10, 9,  8,  -1, -1, -1, -1, -1, -1, -1, 3,  11, 2,  -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  11, 2,  8,  11, 0,  -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, 1,  9,  0,  2,  3,  11, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, 1,  11, 2,  1,  9,  11, 9,  8,  11, -1, -1, -1, -1, -1,
    -1, -1, 3,  10, 1,  11, 10, 3,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,
    10, 1,  0,  8,  10, 8,  11, 10, -1, -1, -1, -1, -1, -1, -1, 3,  9,  0,  3,
    11, 9,  11, 10, 9,  -1, -1, -1, -1, -1, -1, -1, 9,  8,  10, 10, 8,  11, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 4,  7,  8,  -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 4,  3,  0,  7,  3,  4,  -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, 0,  1,  9,  8,  4,  7,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    4,  1,  9,  4,  7,  1,  7,  3,  1,  -1, -1, -1, -1, -1, -1, -1, 1,  2,  10,
    8,  4,  7,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3,  4,  7,  3,  0,  4,
    1,  2,  10, -1, -1, -1, -1, -1, -1, -1, 9,  2,  10, 9,  0,  2,  8,  4,  7,
    -1, -1, -1, -1, -1, -1, -1, 2,  10, 9,  2,  9,  7,  2,  7,  3,  7,  9,  4,
    -1, -1, -1, -1, 8,  4,  7,  3,  11, 2,  -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 11, 4,  7,  11, 2,  4,  2,  0,  4,  -1, -1, -1, -1, -1, -1, -1, 9,  0,
    1,  8,  4,  7,  2,  3,  11, -1, -1, -1, -1, -1, -1, -1, 4,  7,  11, 9,  4,
    11, 9,  11, 2,  9,  2,  1,  -1, -1, -1, -1, 3,  10, 1,  3,  11, 10, 7,  8,
    4,  -1, -1, -1, -1, -1, -1, -1, 1,  11, 10, 1,  4,  11, 1,  0,  4,  7,  11,
    4,  -1, -1, -1, -1, 4,  7,  8,  9,  0,  11, 9,  11, 10, 11, 0,  3,  -1, -1,
    -1, -1, 4,  7,  11, 4,  11, 9,  9,  11, 10, -1, -1, -1, -1, -1, -1, -1, 9,
    5,  4,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 9,  5,  4,  0,
    8,  3,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  5,  4,  1,  5,  0,  -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 8,  5,  4,  8,  3,  5,  3,  1,  5,  -1,
    -1, -1, -1, -1, -1, -1, 1,  2,  10, 9,  5,  4,  -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, 3,  0,  8,  1,  2,  10, 4,  9,  5,  -1, -1, -1, -1, -1, -1, -1,
    5,  2,  10, 5,  4,  2,  4,  0,  2,  -1, -1, -1, -1, -1, -1, -1, 2,  10, 5,
    3,  2,  5,  3,  5,  4,  3,  4,  8,  -1, -1, -1, -1, 9,  5,  4,  2,  3,  11,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  11, 2,  0,  8,  11, 4,  9,  5,
    -1, -1, -1, -1, -1, -1, -1, 0,  5,  4,  0,  1,  5,  2,  3,  11, -1, -1, -1,
    -1, -1, -1, -1, 2,  1,  5,  2,  5,  8,  2,  8,  11, 4,  8,  5,  -1, -1, -1,
    -1, 10, 3,  11, 10, 1,  3,  9,  5,  4,  -1, -1, -1, -1, -1, -1, -1, 4,  9,
    5,  0,  8,  1,  8,  10, 1,  8,  11, 10, -1, -1, -1, -1, 5,  4,  0,  5,  0,
    11, 5,  11, 10, 11, 0,  3,  -1, -1, -1, -1, 5,  4,  8,  5,  8,  10, 10, 8,
    11, -1, -1, -1, -1, -1, -1, -1, 9,  7,  8,  5,  7,  9,  -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, 9,  3,  0,  9,  5,  3,  5,  7,  3,  -1, -1, -1, -1, -1,
    -1, -1, 0,  7,  8,  0,  1,  7,  1,  5,  7,  -1, -1, -1, -1, -1, -1, -1, 1,
    5,  3,  3,  5,  7,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 9,  7,  8,  9,
    5,  7,  10, 1,  2,  -1, -1, -1, -1, -1, -1, -1, 10, 1,  2,  9,  5,  0,  5,
    3,  0,  5,  7,  3,  -1, -1, -1, -1, 8,  0,  2,  8,  2,  5,  8,  5,  7,  10,
    5,  2,  -1, -1, -1, -1, 2,  10, 5,  2,  5,  3,  3,  5,  7,  -1, -1, -1, -1,
    -1, -1, -1, 7,  9,  5,  7,  8,  9,  3,  11, 2,  -1, -1, -1, -1, -1, -1, -1,
    9,  5,  7,  9,  7,  2,  9,  2,  0,  2,  7,  11, -1, -1, -1, -1, 2,  3,  11,
    0,  1,  8,  1,  7,  8,  1,  5,  7,  -1, -1, -1, -1, 11, 2,  1,  11, 1,  7,
    7,  1,  5,  -1, -1, -1, -1, -1, -1, -1, 9,  5,  8,  8,  5,  7,  10, 1,  3,
    10, 3,  11, -1, -1, -1, -1, 5,  7,  0,  5,  0,  9,  7,  11, 0,  1,  0,  10,
    11, 10, 0,  -1, 11, 10, 0,  11, 0,  3,  10, 5,  0,  8,  0,  7,  5,  7,  0,
    -1, 11, 10, 5,  7,  11, 5,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, 6,
    5,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  8,  3,  5,  10,
    6,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 9,  0,  1,  5,  10, 6,  -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, 1,  8,  3,  1,  9,  8,  5,  10, 6,  -1, -1,
    -1, -1, -1, -1, -1, 1,  6,  5,  2,  6,  1,  -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, 1,  6,  5,  1,  2,  6,  3,  0,  8,  -1, -1, -1, -1, -1, -1, -1, 9,
    6,  5,  9,  0,  6,  0,  2,  6,  -1, -1, -1, -1, -1, -1, -1, 5,  9,  8,  5,
    8,  2,  5,  2,  6,  3,  2,  8,  -1, -1, -1, -1, 2,  3,  11, 10, 6,  5,  -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 11, 0,  8,  11, 2,  0,  10, 6,  5,  -1,
    -1, -1, -1, -1, -1, -1, 0,  1,  9,  2,  3,  11, 5,  10, 6,  -1, -1, -1, -1,
    -1, -1, -1, 5,  10, 6,  1,  9,  2,  9,  11, 2,  9,  8,  11, -1, -1, -1, -1,
    6,  3,  11, 6,  5,  3,  5,  1,  3,  -1, -1, -1, -1, -1, -1, -1, 0,  8,  11,
    0,  11, 5,  0,  5,  1,  5,  11, 6,  -1, -1, -1, -1, 3,  11, 6,  0,  3,  6,
    0,  6,  5,  0,  5,  9,  -1, -1, -1, -1, 6,  5,  9,  6,  9,  11, 11, 9,  8,
    -1, -1, -1, -1, -1, -1, -1, 5,  10, 6,  4,  7,  8,  -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 4,  3,  0,  4,  7,  3,  6,  5,  10, -1, -1, -1, -1, -1, -1,
    -1, 1,  9,  0,  5,  10, 6,  8,  4,  7,  -1, -1, -1, -1, -1, -1, -1, 10, 6,
    5,  1,  9,  7,  1,  7,  3,  7,  9,  4,  -1, -1, -1, -1, 6,  1,  2,  6,  5,
    1,  4,  7,  8,  -1, -1, -1, -1, -1, -1, -1, 1,  2,  5,  5,  2,  6,  3,  0,
    4,  3,  4,  7,  -1, -1, -1, -1, 8,  4,  7,  9,  0,  5,  0,  6,  5,  0,  2,
    6,  -1, -1, -1, -1, 7,  3,  9,  7,  9,  4,  3,  2,  9,  5,  9,  6,  2,  6,
    9,  -1, 3,  11, 2,  7,  8,  4,  10, 6,  5,  -1, -1, -1, -1, -1, -1, -1, 5,
    10, 6,  4,  7,  2,  4,  2,  0,  2,  7,  11, -1, -1, -1, -1, 0,  1,  9,  4,
    7,  8,  2,  3,  11, 5,  10, 6,  -1, -1, -1, -1, 9,  2,  1,  9,  11, 2,  9,
    4,  11, 7,  11, 4,  5,  10, 6,  -1, 8,  4,  7,  3,  11, 5,  3,  5,  1,  5,
    11, 6,  -1, -1, -1, -1, 5,  1,  11, 5,  11, 6,  1,  0,  11, 7,  11, 4,  0,
    4,  11, -1, 0,  5,  9,  0,  6,  5,  0,  3,  6,  11, 6,  3,  8,  4,  7,  -1,
    6,  5,  9,  6,  9,  11, 4,  7,  9,  7,  11, 9,  -1, -1, -1, -1, 10, 4,  9,
    6,  4,  10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4,  10, 6,  4,  9,  10,
    0,  8,  3,  -1, -1, -1, -1, -1, -1, -1, 10, 0,  1,  10, 6,  0,  6,  4,  0,
    -1, -1, -1, -1, -1, -1, -1, 8,  3,  1,  8,  1,  6,  8,  6,  4,  6,  1,  10,
    -1, -1, -1, -1, 1,  4,  9,  1,  2,  4,  2,  6,  4,  -1, -1, -1, -1, -1, -1,
    -1, 3,  0,  8,  1,  2,  9,  2,  4,  9,  2,  6,  4,  -1, -1, -1, -1, 0,  2,
    4,  4,  2,  6,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 8,  3,  2,  8,  2,
    4,  4,  2,  6,  -1, -1, -1, -1, -1, -1, -1, 10, 4,  9,  10, 6,  4,  11, 2,
    3,  -1, -1, -1, -1, -1, -1, -1, 0,  8,  2,  2,  8,  11, 4,  9,  10, 4,  10,
    6,  -1, -1, -1, -1, 3,  11, 2,  0,  1,  6,  0,  6,  4,  6,  1,  10, -1, -1,
    -1, -1, 6,  4,  1,  6,  1,  10, 4,  8,  1,  2,  1,  11, 8,  11, 1,  -1, 9,
    6,  4,  9,  3,  6,  9,  1,  3,  11, 6,  3,  -1, -1, -1, -1, 8,  11, 1,  8,
    1,  0,  11, 6,  1,  9,  1,  4,  6,  4,  1,  -1, 3,  11, 6,  3,  6,  0,  0,
    6,  4,  -1, -1, -1, -1, -1, -1, -1, 6,  4,  8,  11, 6,  8,  -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 7,  10, 6,  7,  8,  10, 8,  9,  10, -1, -1, -1, -1,
    -1, -1, -1, 0,  7,  3,  0,  10, 7,  0,  9,  10, 6,  7,  10, -1, -1, -1, -1,
    10, 6,  7,  1,  10, 7,  1,  7,  8,  1,  8,  0,  -1, -1, -1, -1, 10, 6,  7,
    10, 7,  1,  1,  7,  3,  -1, -1, -1, -1, -1, -1, -1, 1,  2,  6,  1,  6,  8,
    1,  8,  9,  8,  6,  7,  -1, -1, -1, -1, 2,  6,  9,  2,  9,  1,  6,  7,  9,
    0,  9,  3,  7,  3,  9,  -1, 7,  8,  0,  7,  0,  6,  6,  0,  2,  -1, -1, -1,
    -1, -1, -1, -1, 7,  3,  2,  6,  7,  2,  -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 2,  3,  11, 10, 6,  8,  10, 8,  9,  8,  6,  7,  -1, -1, -1, -1, 2,  0,
    7,  2,  7,  11, 0,  9,  7,  6,  7,  10, 9,  10, 7,  -1, 1,  8,  0,  1,  7,
    8,  1,  10, 7,  6,  7,  10, 2,  3,  11, -1, 11, 2,  1,  11, 1,  7,  10, 6,
    1,  6,  7,  1,  -1, -1, -1, -1, 8,  9,  6,  8,  6,  7,  9,  1,  6,  11, 6,
    3,  1,  3,  6,  -1, 0,  9,  1,  11, 6,  7,  -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, 7,  8,  0,  7,  0,  6,  3,  11, 0,  11, 6,  0,  -1, -1, -1, -1, 7,
    11, 6,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 7,  6,  11, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3,  0,  8,  11, 7,  6,  -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  1,  9,  11, 7,  6,  -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 8,  1,  9,  8,  3,  1,  11, 7,  6,  -1, -1, -1, -1,
    -1, -1, -1, 10, 1,  2,  6,  11, 7,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    1,  2,  10, 3,  0,  8,  6,  11, 7,  -1, -1, -1, -1, -1, -1, -1, 2,  9,  0,
    2,  10, 9,  6,  11, 7,  -1, -1, -1, -1, -1, -1, -1, 6,  11, 7,  2,  10, 3,
    10, 8,  3,  10, 9,  8,  -1, -1, -1, -1, 7,  2,  3,  6,  2,  7,  -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, 7,  0,  8,  7,  6,  0,  6,  2,  0,  -1, -1, -1,
    -1, -1, -1, -1, 2,  7,  6,  2,  3,  7,  0,  1,  9,  -1, -1, -1, -1, -1, -1,
    -1, 1,  6,  2,  1,  8,  6,  1,  9,  8,  8,  7,  6,  -1, -1, -1, -1, 10, 7,
    6,  10, 1,  7,  1,  3,  7,  -1, -1, -1, -1, -1, -1, -1, 10, 7,  6,  1,  7,
    10, 1,  8,  7,  1,  0,  8,  -1, -1, -1, -1, 0,  3,  7,  0,  7,  10, 0,  10,
    9,  6,  10, 7,  -1, -1, -1, -1, 7,  6,  10, 7,  10, 8,  8,  10, 9,  -1, -1,
    -1, -1, -1, -1, -1, 6,  8,  4,  11, 8,  6,  -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, 3,  6,  11, 3,  0,  6,  0,  4,  6,  -1, -1, -1, -1, -1, -1, -1, 8,
    6,  11, 8,  4,  6,  9,  0,  1,  -1, -1, -1, -1, -1, -1, -1, 9,  4,  6,  9,
    6,  3,  9,  3,  1,  11, 3,  6,  -1, -1, -1, -1, 6,  8,  4,  6,  11, 8,  2,
    10, 1,  -1, -1, -1, -1, -1, -1, -1, 1,  2,  10, 3,  0,  11, 0,  6,  11, 0,
    4,  6,  -1, -1, -1, -1, 4,  11, 8,  4,  6,  11, 0,  2,  9,  2,  10, 9,  -1,
    -1, -1, -1, 10, 9,  3,  10, 3,  2,  9,  4,  3,  11, 3,  6,  4,  6,  3,  -1,
    8,  2,  3,  8,  4,  2,  4,  6,  2,  -1, -1, -1, -1, -1, -1, -1, 0,  4,  2,
    4,  6,  2,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1,  9,  0,  2,  3,  4,
    2,  4,  6,  4,  3,  8,  -1, -1, -1, -1, 1,  9,  4,  1,  4,  2,  2,  4,  6,
    -1, -1, -1, -1, -1, -1, -1, 8,  1,  3,  8,  6,  1,  8,  4,  6,  6,  10, 1,
    -1, -1, -1, -1, 10, 1,  0,  10, 0,  6,  6,  0,  4,  -1, -1, -1, -1, -1, -1,
    -1, 4,  6,  3,  4,  3,  8,  6,  10, 3,  0,  3,  9,  10, 9,  3,  -1, 10, 9,
    4,  6,  10, 4,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4,  9,  5,  7,  6,
    11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  8,  3,  4,  9,  5,  11, 7,
    6,  -1, -1, -1, -1, -1, -1, -1, 5,  0,  1,  5,  4,  0,  7,  6,  11, -1, -1,
    -1, -1, -1, -1, -1, 11, 7,  6,  8,  3,  4,  3,  5,  4,  3,  1,  5,  -1, -1,
    -1, -1, 9,  5,  4,  10, 1,  2,  7,  6,  11, -1, -1, -1, -1, -1, -1, -1, 6,
    11, 7,  1,  2,  10, 0,  8,  3,  4,  9,  5,  -1, -1, -1, -1, 7,  6,  11, 5,
    4,  10, 4,  2,  10, 4,  0,  2,  -1, -1, -1, -1, 3,  4,  8,  3,  5,  4,  3,
    2,  5,  10, 5,  2,  11, 7,  6,  -1, 7,  2,  3,  7,  6,  2,  5,  4,  9,  -1,
    -1, -1, -1, -1, -1, -1, 9,  5,  4,  0,  8,  6,  0,  6,  2,  6,  8,  7,  -1,
    -1, -1, -1, 3,  6,  2,  3,  7,  6,  1,  5,  0,  5,  4,  0,  -1, -1, -1, -1,
    6,  2,  8,  6,  8,  7,  2,  1,  8,  4,  8,  5,  1,  5,  8,  -1, 9,  5,  4,
    10, 1,  6,  1,  7,  6,  1,  3,  7,  -1, -1, -1, -1, 1,  6,  10, 1,  7,  6,
    1,  0,  7,  8,  7,  0,  9,  5,  4,  -1, 4,  0,  10, 4,  10, 5,  0,  3,  10,
    6,  10, 7,  3,  7,  10, -1, 7,  6,  10, 7,  10, 8,  5,  4,  10, 4,  8,  10,
    -1, -1, -1, -1, 6,  9,  5,  6,  11, 9,  11, 8,  9,  -1, -1, -1, -1, -1, -1,
    -1, 3,  6,  11, 0,  6,  3,  0,  5,  6,  0,  9,  5,  -1, -1, -1, -1, 0,  11,
    8,  0,  5,  11, 0,  1,  5,  5,  6,  11, -1, -1, -1, -1, 6,  11, 3,  6,  3,
    5,  5,  3,  1,  -1, -1, -1, -1, -1, -1, -1, 1,  2,  10, 9,  5,  11, 9,  11,
    8,  11, 5,  6,  -1, -1, -1, -1, 0,  11, 3,  0,  6,  11, 0,  9,  6,  5,  6,
    9,  1,  2,  10, -1, 11, 8,  5,  11, 5,  6,  8,  0,  5,  10, 5,  2,  0,  2,
    5,  -1, 6,  11, 3,  6,  3,  5,  2,  10, 3,  10, 5,  3,  -1, -1, -1, -1, 5,
    8,  9,  5,  2,  8,  5,  6,  2,  3,  8,  2,  -1, -1, -1, -1, 9,  5,  6,  9,
    6,  0,  0,  6,  2,  -1, -1, -1, -1, -1, -1, -1, 1,  5,  8,  1,  8,  0,  5,
    6,  8,  3,  8,  2,  6,  2,  8,  -1, 1,  5,  6,  2,  1,  6,  -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 1,  3,  6,  1,  6,  10, 3,  8,  6,  5,  6,  9,  8,
    9,  6,  -1, 10, 1,  0,  10, 0,  6,  9,  5,  0,  5,  6,  0,  -1, -1, -1, -1,
    0,  3,  8,  5,  6,  10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, 5,  6,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 11, 5,  10, 7,  5,  11,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 11, 5,  10, 11, 7,  5,  8,  3,  0,
    -1, -1, -1, -1, -1, -1, -1, 5,  11, 7,  5,  10, 11, 1,  9,  0,  -1, -1, -1,
    -1, -1, -1, -1, 10, 7,  5,  10, 11, 7,  9,  8,  1,  8,  3,  1,  -1, -1, -1,
    -1, 11, 1,  2,  11, 7,  1,  7,  5,  1,  -1, -1, -1, -1, -1, -1, -1, 0,  8,
    3,  1,  2,  7,  1,  7,  5,  7,  2,  11, -1, -1, -1, -1, 9,  7,  5,  9,  2,
    7,  9,  0,  2,  2,  11, 7,  -1, -1, -1, -1, 7,  5,  2,  7,  2,  11, 5,  9,
    2,  3,  2,  8,  9,  8,  2,  -1, 2,  5,  10, 2,  3,  5,  3,  7,  5,  -1, -1,
    -1, -1, -1, -1, -1, 8,  2,  0,  8,  5,  2,  8,  7,  5,  10, 2,  5,  -1, -1,
    -1, -1, 9,  0,  1,  5,  10, 3,  5,  3,  7,  3,  10, 2,  -1, -1, -1, -1, 9,
    8,  2,  9,  2,  1,  8,  7,  2,  10, 2,  5,  7,  5,  2,  -1, 1,  3,  5,  3,
    7,  5,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  8,  7,  0,  7,  1,  1,
    7,  5,  -1, -1, -1, -1, -1, -1, -1, 9,  0,  3,  9,  3,  5,  5,  3,  7,  -1,
    -1, -1, -1, -1, -1, -1, 9,  8,  7,  5,  9,  7,  -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, 5,  8,  4,  5,  10, 8,  10, 11, 8,  -1, -1, -1, -1, -1, -1, -1,
    5,  0,  4,  5,  11, 0,  5,  10, 11, 11, 3,  0,  -1, -1, -1, -1, 0,  1,  9,
    8,  4,  10, 8,  10, 11, 10, 4,  5,  -1, -1, -1, -1, 10, 11, 4,  10, 4,  5,
    11, 3,  4,  9,  4,  1,  3,  1,  4,  -1, 2,  5,  1,  2,  8,  5,  2,  11, 8,
    4,  5,  8,  -1, -1, -1, -1, 0,  4,  11, 0,  11, 3,  4,  5,  11, 2,  11, 1,
    5,  1,  11, -1, 0,  2,  5,  0,  5,  9,  2,  11, 5,  4,  5,  8,  11, 8,  5,
    -1, 9,  4,  5,  2,  11, 3,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2,  5,
    10, 3,  5,  2,  3,  4,  5,  3,  8,  4,  -1, -1, -1, -1, 5,  10, 2,  5,  2,
    4,  4,  2,  0,  -1, -1, -1, -1, -1, -1, -1, 3,  10, 2,  3,  5,  10, 3,  8,
    5,  4,  5,  8,  0,  1,  9,  -1, 5,  10, 2,  5,  2,  4,  1,  9,  2,  9,  4,
    2,  -1, -1, -1, -1, 8,  4,  5,  8,  5,  3,  3,  5,  1,  -1, -1, -1, -1, -1,
    -1, -1, 0,  4,  5,  1,  0,  5,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 8,
    4,  5,  8,  5,  3,  9,  0,  5,  0,  3,  5,  -1, -1, -1, -1, 9,  4,  5,  -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4,  11, 7,  4,  9,  11, 9,
    10, 11, -1, -1, -1, -1, -1, -1, -1, 0,  8,  3,  4,  9,  7,  9,  11, 7,  9,
    10, 11, -1, -1, -1, -1, 1,  10, 11, 1,  11, 4,  1,  4,  0,  7,  4,  11, -1,
    -1, -1, -1, 3,  1,  4,  3,  4,  8,  1,  10, 4,  7,  4,  11, 10, 11, 4,  -1,
    4,  11, 7,  9,  11, 4,  9,  2,  11, 9,  1,  2,  -1, -1, -1, -1, 9,  7,  4,
    9,  11, 7,  9,  1,  11, 2,  11, 1,  0,  8,  3,  -1, 11, 7,  4,  11, 4,  2,
    2,  4,  0,  -1, -1, -1, -1, -1, -1, -1, 11, 7,  4,  11, 4,  2,  8,  3,  4,
    3,  2,  4,  -1, -1, -1, -1, 2,  9,  10, 2,  7,  9,  2,  3,  7,  7,  4,  9,
    -1, -1, -1, -1, 9,  10, 7,  9,  7,  4,  10, 2,  7,  8,  7,  0,  2,  0,  7,
    -1, 3,  7,  10, 3,  10, 2,  7,  4,  10, 1,  10, 0,  4,  0,  10, -1, 1,  10,
    2,  8,  7,  4,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4,  9,  1,  4,  1,
    7,  7,  1,  3,  -1, -1, -1, -1, -1, -1, -1, 4,  9,  1,  4,  1,  7,  0,  8,
    1,  8,  7,  1,  -1, -1, -1, -1, 4,  0,  3,  7,  4,  3,  -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, 4,  8,  7,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, 9,  10, 8,  10, 11, 8,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3,
    0,  9,  3,  9,  11, 11, 9,  10, -1, -1, -1, -1, -1, -1, -1, 0,  1,  10, 0,
    10, 8,  8,  10, 11, -1, -1, -1, -1, -1, -1, -1, 3,  1,  10, 11, 3,  10, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 1,  2,  11, 1,  11, 9,  9,  11, 8,  -1,
    -1, -1, -1, -1, -1, -1, 3,  0,  9,  3,  9,  11, 1,  2,  9,  2,  11, 9,  -1,
    -1, -1, -1, 0,  2,  11, 8,  0,  11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    3,  2,  11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2,  3,  8,
    2,  8,  10, 10, 8,  9,  -1, -1, -1, -1, -1, -1, -1, 9,  10, 2,  0,  9,  2,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2,  3,  8,  2,  8,  10, 0,  1,  8,
    1,  10, 8,  -1, -1, -1, -1, 1,  10, 2,  -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 1,  3,  8,  9,  1,  8,  -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 0,  9,  1,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  3,
    8,  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

struct Surface {
  std::vector<int> rgba;
  std::map<int, std::pair<int, std::vector<int>>> vertexIndex;
  std::vector<std::vector<int>> faces;
  geom3d::Point::Vec vertices;
  geom3d::Point::Vec face_normals;
  geom3d::Point::Vec normals;
};

Surface polygonize_pdb(const std::vector<int> &size,
                       const std::vector<double> &gridVals,
                       const double isolevel) {

  std::map<int, std::pair<int, std::vector<int>>> vertexIndex;
  std::vector<std::vector<int>> faces;
  geom3d::Point::Vec vertices;
  geom3d::Point::Vec face_normals;

  geom3d::Point::Vec vertlist(12, geom3d::Point(0.0, 0.0, 0.0));

  for (int z = 0; z < size[2] - 1; z++) {
    for (int y = 0; y < size[1] - 1; y++) {
      for (int x = 0; x < size[0] - 1; x++) {

        dbgmsg("x = " << x << " y = " << y << " z = " << z);

        const int grid0 = x + size[0] * (y + size[1] * z);
        const int grid1 = (x + 1) + size[0] * (y + size[1] * z);
        const int grid2 = (x + 1) + size[0] * (y + size[1] * (z + 1));
        const int grid3 = x + size[0] * (y + size[1] * (z + 1));
        const int grid4 = x + size[0] * ((y + 1) + size[1] * z);
        const int grid5 = (x + 1) + size[0] * ((y + 1) + size[1] * z);
        const int grid6 = (x + 1) + size[0] * ((y + 1) + size[1] * (z + 1));
        const int grid7 = x + size[0] * ((y + 1) + size[1] * (z + 1));

        dbgmsg("grid0 = " << grid0 << " grid1 = " << grid1
                          << " grid2 = " << grid2);

        int cubeIndex = 0;
        if (gridVals[grid0] < isolevel)
          cubeIndex |= 1;
        if (gridVals[grid1] < isolevel)
          cubeIndex |= 2;
        if (gridVals[grid2] < isolevel)
          cubeIndex |= 4;
        if (gridVals[grid3] < isolevel)
          cubeIndex |= 8;
        if (gridVals[grid4] < isolevel)
          cubeIndex |= 16;
        if (gridVals[grid5] < isolevel)
          cubeIndex |= 32;
        if (gridVals[grid6] < isolevel)
          cubeIndex |= 64;
        if (gridVals[grid7] < isolevel)
          cubeIndex |= 128;

        dbgmsg("cubeIndex = " << cubeIndex);
        dbgmsg("edgeTable[" << cubeIndex << "] = " << edgeTable[cubeIndex]);

        if (edgeTable[cubeIndex] == 0)
          continue;

        geom3d::Point v0, v1;
        if (edgeTable[cubeIndex] & 1) {
          geom3d::Point v0(x, y, z);
          geom3d::Point v1(x + 1, y, z);
          const double iv = (isolevel - gridVals[grid0]) /
                            (gridVals[grid1] - gridVals[grid0]);
          vertlist[0] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[0] = " << vertlist[0]);
        }
        if (edgeTable[cubeIndex] & 2) {
          geom3d::Point v0(x + 1, y, z);
          geom3d::Point v1(x + 1, y, z + 1);
          const double iv = (isolevel - gridVals[grid1]) /
                            (gridVals[grid2] - gridVals[grid1]);
          vertlist[1] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[1] = " << vertlist[1]);
        }
        if (edgeTable[cubeIndex] & 4) {
          geom3d::Point v0(x + 1, y, z + 1); // grid2
          geom3d::Point v1(x, y, z + 1);     // grid3
          const double iv = (isolevel - gridVals[grid2]) /
                            (gridVals[grid3] - gridVals[grid2]);
          vertlist[2] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[2] = " << vertlist[2]);
        }
        if (edgeTable[cubeIndex] & 8) {
          geom3d::Point v0(x, y, z + 1); // grid3
          geom3d::Point v1(x, y, z);     // grid0
          const double iv = (isolevel - gridVals[grid3]) /
                            (gridVals[grid0] - gridVals[grid3]);
          vertlist[3] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[3] = " << vertlist[3]);
        }
        if (edgeTable[cubeIndex] & 16) {
          geom3d::Point v0(x, y + 1, z);     // grid4
          geom3d::Point v1(x + 1, y + 1, z); // grid5
          const double iv = (isolevel - gridVals[grid4]) /
                            (gridVals[grid5] - gridVals[grid4]);
          vertlist[4] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[4] = " << vertlist[4]);
        }
        if (edgeTable[cubeIndex] & 32) {
          geom3d::Point v0(x + 1, y + 1, z);     // grid5
          geom3d::Point v1(x + 1, y + 1, z + 1); // grid6
          const double iv = (isolevel - gridVals[grid5]) /
                            (gridVals[grid6] - gridVals[grid5]);
          vertlist[5] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[5] = " << vertlist[5]);
        }
        if (edgeTable[cubeIndex] & 64) {
          geom3d::Point v0(x + 1, y + 1, z + 1); // grid6
          geom3d::Point v1(x, y + 1, z + 1);     // grid7
          const double iv = (isolevel - gridVals[grid6]) /
                            (gridVals[grid7] - gridVals[grid6]);
          vertlist[6] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[6] = " << vertlist[6]);
        }
        if (edgeTable[cubeIndex] & 128) {
          geom3d::Point v0(x, y + 1, z + 1); // grid7
          geom3d::Point v1(x, y + 1, z);     // grid4
          const double iv = (isolevel - gridVals[grid7]) /
                            (gridVals[grid4] - gridVals[grid7]);
          vertlist[7] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[7] = " << vertlist[7]);
        }
        if (edgeTable[cubeIndex] & 256) {
          geom3d::Point v0(x, y, z);     // grid0
          geom3d::Point v1(x, y + 1, z); // grid4
          const double iv = (isolevel - gridVals[grid0]) /
                            (gridVals[grid4] - gridVals[grid0]);
          vertlist[8] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[8] = " << vertlist[8]);
        }
        if (edgeTable[cubeIndex] & 512) {
          geom3d::Point v0(x + 1, y, z);     // grid1
          geom3d::Point v1(x + 1, y + 1, z); // grid5
          const double iv = (isolevel - gridVals[grid1]) /
                            (gridVals[grid5] - gridVals[grid1]);
          vertlist[9] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[9] = " << vertlist[9]);
        }
        if (edgeTable[cubeIndex] & 1024) {
          geom3d::Point v0(x + 1, y, z + 1);     // grid2
          geom3d::Point v1(x + 1, y + 1, z + 1); // grid6
          const double iv = (isolevel - gridVals[grid2]) /
                            (gridVals[grid6] - gridVals[grid2]);
          vertlist[10] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[10] = " << vertlist[10]);
        }
        if (edgeTable[cubeIndex] & 2048) {
          geom3d::Point v0(x, y, z + 1);     // grid3
          geom3d::Point v1(x, y + 1, z + 1); // grid7
          const double iv = (isolevel - gridVals[grid3]) /
                            (gridVals[grid7] - gridVals[grid3]);
          vertlist[11] = geom3d::line_evaluate(v0, v1 - v0, iv);
          dbgmsg("vertlist[11] = " << vertlist[11]);
        }

        for (int i = 0; triTable[16 * cubeIndex + i] != -1; i += 3) {
          const geom3d::Point vert1 =
              vertlist[triTable[16 * cubeIndex + i + 1]];
          const geom3d::Point vert2 =
              vertlist[triTable[16 * cubeIndex + i + 2]];
          const geom3d::Point vert3 = vertlist[triTable[16 * cubeIndex + i]];

          dbgmsg("vert1 = " << vert1 << " vert2 = " << vert2
                            << " vert3 = " << vert3);

          // represent the position as an index with respect to the grid size
          const int idx1 =
              floor(vert1.x()) +
              size[0] * (floor(vert1.y()) + size[1] * floor(vert1.z()));
          const int idx2 =
              floor(vert2.x()) +
              size[0] * (floor(vert2.y()) + size[1] * floor(vert2.z()));
          const int idx3 =
              floor(vert3.x()) +
              size[0] * (floor(vert3.y()) + size[1] * floor(vert3.z()));

          dbgmsg("idx1 = " << idx1 << " idx2 = " << idx2 << " idx3 = " << idx3);
          std::vector<int> triangle = {-1, -1, -1};

          // vertices that map to the same index (i.e. have the same or have
          // virtually the same position), are not added again, but are instead
          // referenced (i.e. via indexed buffers)
          if (!vertexIndex.count(idx1)) {
            vertexIndex[idx1] = {vertices.size(), std::vector<int>()};
            vertices.push_back(vert1);
          }
          vertexIndex[idx1].second.push_back(faces.size());

          if (!vertexIndex.count(idx2)) {
            vertexIndex[idx2] = {vertices.size(), std::vector<int>()};
            vertices.push_back(vert2);
          }
          vertexIndex[idx2].second.push_back(faces.size());

          if (!vertexIndex.count(idx3)) {
            vertexIndex[idx3] = {vertices.size(), std::vector<int>()};
            vertices.push_back(vert3);
          }
          vertexIndex[idx3].second.push_back(faces.size());

          triangle[0] = vertexIndex[idx1].first;
          triangle[1] = vertexIndex[idx2].first;
          triangle[2] = vertexIndex[idx3].first;

          geom3d::Point v0(vert2 - vert1);
          geom3d::Point v1(vert3 - vert1);

          geom3d::Point normal = geom3d::Point::cross(v0, v1);

          dbgmsg("vertices.back() = " << vertices.back());
          dbgmsg("normal = " << normal);
          dbgmsg("triangle[0] = " << triangle[0]
                                  << " triangle[1] = " << triangle[1]
                                  << " triangle[2] = " << triangle[2]);

          faces.push_back(triangle);
          face_normals.push_back(normal);
        }
      }
    }
  }
  Surface surf;
  surf.vertices = vertices;
  surf.faces = faces;
  surf.vertexIndex = vertexIndex;
  surf.face_normals = face_normals;

  dbgmsg("surf.vertices.size() = " << surf.vertices.size());
  dbgmsg("surf.faces.size() = " << surf.faces.size());
  dbgmsg("surf.vertexIndex.size() = " << surf.vertexIndex.size());
  dbgmsg("surf.face_normals.size() = " << surf.face_normals.size());

  return surf;
}

void smoothFunc(const double factor, geom3d::Point::Vec &vertexTemp,
                const std::vector<std::vector<int>> &vertexKeys,
                const geom3d::Point::Vec &vertices) {

  std::pair<geom3d::Point, int> avgCoord;

  for (int v = 0; v < vertices.size(); v++) {
    avgCoord.first = geom3d::Point();
    avgCoord.second = 0;

    for (int i = 0; i < vertexKeys[v].size(); i++) {
      geom3d::Point ref = vertices[vertexKeys[v][i]];
      avgCoord.first = avgCoord.first + ref;
      avgCoord.second += 1;
    }
    if (avgCoord.second == 0)
      continue;

    const double tmp = 1. / avgCoord.second;
    avgCoord.first = avgCoord.first * tmp;

    vertexTemp[v] = vertices[v] + (avgCoord.first - vertices[v]) * factor;
  }
}

// ** performs taubin smoothing for geometry (such as coarse/ccp4 surface) **
void taubinSmoothing_pdb(geom3d::Point::Vec &vertices,
                         const std::vector<std::vector<int>> &faces,
                         const double lambda, const double mu, const int iter) {

  std::vector<std::vector<bool>> vertexInfo(
      vertices.size(), std::vector<bool>(vertices.size(), false));

  for (int f = 0; f < faces.size(); f++) {
    vertexInfo[faces[f][0]][faces[f][1]] = true;
    vertexInfo[faces[f][0]][faces[f][2]] = true;
    vertexInfo[faces[f][1]][faces[f][0]] = true;
    vertexInfo[faces[f][1]][faces[f][2]] = true;
    vertexInfo[faces[f][2]][faces[f][0]] = true;
    vertexInfo[faces[f][2]][faces[f][1]] = true;
  }

  std::vector<std::vector<int>> vertexKeys(vertexInfo.size());

  for (int v = 0; v < vertices.size(); v++) {
    //			vertexInfo[v] = Object.keys(vertexInfo[v]);
    for (int k = 0; k < vertices.size(); k++) {
      if (vertexInfo[v][k]) {
        vertexKeys[v].push_back(k);
      }
    }
  }

  geom3d::Point::Vec vertexTemp(vertices.size(), geom3d::Point());

  for (int it = 0; it < iter; it++) {
    smoothFunc(lambda, vertexTemp, vertexKeys, vertices);
    for (int v = 0; v < vertices.size(); v++) {
      vertices[v] = vertexTemp[v];
    }
    smoothFunc(mu, vertexTemp, vertexKeys, vertices);
    for (int v = 0; v < vertices.size(); v++) {
      vertices[v] = vertexTemp[v];
    }
  }
}

Surface coarseSurface_pdb(const molib::Atom::PVec &atoms, const double res,
                          const double probeR) {

  const double inv_res = 1 / res;
  // make this also work with res not being a number, but an array of integers,
  // so Nx, Ny, Nz, where this represents the number of voxels...
  std::vector<double> geomRanges = {1e99, -1e99, 1e99, -1e99, 1e99, -1e99};

  std::map<std::string, double> elements;

  for (auto &patom : atoms) {
    auto &atom = *patom;
    if (atom.crd().x() < geomRanges[0])
      geomRanges[0] = atom.crd().x();
    if (atom.crd().x() > geomRanges[1])
      geomRanges[1] = atom.crd().x();

    if (atom.crd().y() < geomRanges[2])
      geomRanges[2] = atom.crd().y();
    if (atom.crd().y() > geomRanges[3])
      geomRanges[3] = atom.crd().y();

    if (atom.crd().z() < geomRanges[4])
      geomRanges[4] = atom.crd().z();
    if (atom.crd().z() > geomRanges[5])
      geomRanges[5] = atom.crd().z();

    elements[atom.element().name()] = atom.radius();
    dbgmsg("elements[" << atom.element().name()
                       << "] = " << elements[atom.element().name()]);
  }

  const int rX =
      (int)ceil((geomRanges[1] - geomRanges[0] + (2 * probeR) + (2 * 1.8)) /
                res) +
      2;
  const int rY =
      (int)ceil((geomRanges[3] - geomRanges[2] + (2 * probeR) + (2 * 1.8)) /
                res) +
      2;
  const int rZ =
      (int)ceil((geomRanges[5] - geomRanges[4] + (2 * probeR) + (2 * 1.8)) /
                res) +
      2;

  dbgmsg("rX = " << rX << " rY = " << rY << " rZ = " << rZ);

  const double dX = -geomRanges[0] + probeR + 1.8 + res;
  const double dY = -geomRanges[2] + probeR + 1.8 + res;
  const double dZ = -geomRanges[4] + probeR + 1.8 + res;

  const int gs = rX * rY * rZ;

  std::map<std::string, std::vector<double>> grid_r, grid_a;

  for (auto &kv : elements) {
    const std::string &a = kv.first;
    grid_r[a] = std::vector<double>(gs);
    grid_a[a] = std::vector<double>();
    for (int xi = 0; xi < gs; xi++) {
      grid_r[a][xi] = 1e99;
      grid_a[a].push_back(0);

      dbgmsg("grid_r[" << a << "][" << xi << "] = " << grid_r[a][xi]);
      dbgmsg("grid_a[" << a << "][" << xi << "] = " << grid_a[a][xi]);
    }
  }

  std::vector<double> block_ranges = {0, 0, 0, 0, 0, 0};

  for (auto &patom : atoms) {
    auto &atom = *patom;

    dbgmsg("atom = " << atom);

    std::vector<double> &gr = grid_r[atom.element().name()];
    std::vector<double> &ga = grid_a[atom.element().name()];

    dbgmsg("gr.size() = " << gr.size());
    dbgmsg("ga.size() = " << ga.size());

    const double r = elements[atom.element().name()];
    dbgmsg("elements[" << atom.element().name()
                       << "] = " << elements[atom.element().name()]);

    const double aX = atom.crd().x() + dX;
    const double aY = atom.crd().y() + dY;
    const double aZ = atom.crd().z() + dZ;

    block_ranges[0] = 0;
    block_ranges[1] = rX;
    block_ranges[2] = 0;
    block_ranges[3] = rY;
    block_ranges[4] = 0;
    block_ranges[5] = rZ;

    for (int xi = block_ranges[0]; xi < block_ranges[1]; xi++) {
      const double xb = xi * res;
      for (int yi = block_ranges[2]; yi < block_ranges[3]; yi++) {
        const double yb = yi * res;
        for (int zi = block_ranges[4]; zi < block_ranges[5]; zi++) {
          const double zb = zi * res;

          const int mp = xi + rX * (yi + rY * zi);

          const double dx = aX - xb;
          const double dy = aY - yb;
          const double dz = aZ - zb;
          const double tmp1 = dx * dx + dy * dy + dz * dz;
          if (tmp1 < gr[mp]) {
            gr[mp] = tmp1;
            ga[mp] = r;
            dbgmsg("gr[" << mp << "] = " << gr[mp]);
            dbgmsg("ga[" << mp << "] = " << ga[mp]);
          }
        }
      }
    }
  }

  // now that for each grid point & element the nearest atom is calculated -->
  // map it back to the surface...

  std::vector<double> grid_x(gs);

  for (int xi = 0; xi < gs; xi++) {
    double mp = 1e99;
    for (auto &kv : elements) {
      const std::string &a = kv.first;
      const double r = sqrt(grid_r[a][xi]) -
                       (probeR + grid_a[a][xi]); // distance from point - grid
      if (r < mp)
        mp = r;
    }
    grid_x[xi] = mp * inv_res;
    dbgmsg("grid_x[" << xi << "] = " << grid_x[xi]);
  }

  const double probeR_alt = probeR / res;
  for (int xi = 0; xi < gs; xi++) {
    grid_x[xi] += probeR_alt;
    dbgmsg("grid_x[" << xi << "] = " << grid_x[xi]);
  }

  Surface surf = polygonize_pdb({rX, rY, rZ}, grid_x, 0.0);

  taubinSmoothing_pdb(surf.vertices, surf.faces, .5, -.53, 10);

  dbgmsg("surf.vertices.size() = " << surf.vertices.size());
  geom3d::Point::Vec normals(surf.vertices.size(), geom3d::Point(0, 0, 0));

  for (auto &kv : surf.vertexIndex) {
    const int i = kv.first;
    dbgmsg("i = " << i);
    geom3d::Point &normal = normals[surf.vertexIndex.at(i).first];
    std::vector<int> &faces = surf.vertexIndex.at(i).second;

    dbgmsg("faces.size() = " << faces.size());

    for (int f = 0; f < faces.size(); ++f)
      normal = normal + surf.face_normals[faces[f]];
    dbgmsg("normal = " << normal);
    normal = normal.norm();
  }
  surf.normals = normals;

  taubinSmoothing_pdb(surf.normals, surf.faces, .5, -.53, 10);

  //		if (settings.deproj) {
  //
  //			var sf = 1./res;
  //
  //			for (i=0; i<normals.length; i++) {
  //				surf.vertices[i][0] = ((surf.vertices[i][0] -
  // normals[i][0]
  //* probeR * sf) * res) - dX; surf.vertices[i][1]
  //=
  //((surf.vertices[i][1] - normals[i][1] * probeR * sf) * res) - dY;
  // surf.vertices[i][2] =
  //((surf.vertices[i][2] - normals[i][2] * probeR * sf) * res) - dZ;
  //			}
  //
  //		}
  //		else {

  geom3d::Point dXYZ(dX, dY, dZ);
  for (int i = 0; i < normals.size(); i++) {
    surf.vertices[i] = (surf.vertices[i] * res) - dXYZ;
  }

  return surf;
}

std::pair<std::vector<float>, std::vector<int>>
compute_solvent_accessible_surface(const molib::Atom::PVec &atoms,
                                   const std::vector<int> &rgba) {

  // initial check if float is 4 bytes..
  // eventually I'll change this to boost::float32_t which is in boost 1.61
  if (sizeof(float) != 4)
    throw helper::Error(
        "[WHOOPS] cannot calculate molecular surface since float type "
        "must be 4 bytes");

  bool alpha = false;

  Surface surf = coarseSurface_pdb(atoms, 1.0, 1.4);

  surf.rgba = rgba;
  alpha = alpha || surf.rgba[3] != 255;

  std::vector<float> vertices(surf.vertices.size() *
                              7); // x, y, z, nx, ny, nz, rgba

  int sz_v8 = vertices.size() * sizeof(float);
  uint8_t *vertices8 =
      (uint8_t *)vertices.data(); // vertices8 points to vertices buffer

  std::vector<int> indices(surf.faces.size() * 3);

  for (int c = 0, m8 = 0, m = 0; c < surf.vertices.size(); ++c, m8 += 28) {
    vertices[m++] = surf.vertices[c].x();
    vertices[m++] = surf.vertices[c].y();
    vertices[m++] = surf.vertices[c].z();

    vertices[m++] = surf.normals[c].x();
    vertices[m++] = surf.normals[c].y();
    vertices[m++] = surf.normals[c].z();

    vertices8[m8 + 24] = rgba[0];
    vertices8[m8 + 25] = rgba[1];
    vertices8[m8 + 26] = rgba[2];
    vertices8[m8 + 27] = rgba[3];
    m++; // color
  }

  int offset = 0;

  for (int c = 0, i = 0; c < surf.faces.size(); c++) {
    indices[i++] = surf.faces[c][0] + offset;
    indices[i++] = surf.faces[c][1] + offset;
    indices[i++] = surf.faces[c][2] + offset;
  }
  offset += surf.vertices.size();

  return {vertices, indices};
}

} // namespace insilab::molib::algorithms
