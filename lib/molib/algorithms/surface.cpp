#include "surface.hpp"
#include "geom3d/linear.hpp"
#include "helper/benchmark.hpp"
#include "molib/atom.hpp"
#include "molib/molecule.hpp"
#include <cmath>
#include <numbers>
#include <optional>
#include <span>
#include <utility>

namespace insilab::molib::algorithms {

/**
 * On the line between the centers of atoms at1 and at2 is point X which is
 * directly below the center of the probe sphere as it touches the two atoms.
 *
 * @return the distance between point X and atom center at1.
 */
inline double refpoint(const double r1, const double r2, const double r3,
                       const double dist12) {
  return (std::pow(r1 + r3, 2) - std::pow(r2 + r3, 2) + std::pow(dist12, 2)) /
         (2 * dist12);
}

geom3d::Point rotate_vector_probis(const geom3d::Point &x_vec,
                                   const geom3d::Point &y_vec,
                                   const geom3d::Point &X, const double angle,
                                   const double r) {
  return X + x_vec * (r * std::cos(angle)) + y_vec * (r * std::sin(angle));
}

/**
 * @return true if atom overlaps with point
 */
bool overlap(const auto &neighbors, const Atom &atm,
             const geom3d::Point &center, const double probe_radius,
             const double eps) {
  for (const auto &neighb : neighbors) {
    if (neighb->radius() + probe_radius - neighb->crd().distance(center) > eps)
      return true;
  }
  return false;
}

/**
 * Calculate the spherical probe centers (two) as they sit in between the three
 * atoms at1, at2, at3.
 *
 * @param at1,at2,at3 first, second and third atom
 * @return two center vectors (above and below atoms at1, at2, at3) or no value
 * if the center of the probe atom cannot be calculated
 */
std::optional<std::pair<geom3d::Point, geom3d::Point>>
compute_probe_center(const Atom &at1, const Atom &at2, const Atom &at3,
                     const double probe_radius) {
  geom3d::Point n, X1, X2, c1_c2, c1_c3, p1, p2, tmp, Y;
  double dT4Y;
  c1_c2 = at2.crd() - at1.crd();
  c1_c3 = at3.crd() - at1.crd();
  n = c1_c2 % c1_c3;
  n = n.norm();
  c1_c2 = c1_c2.norm();
  c1_c3 = c1_c3.norm();
  X1 = at1.crd() + c1_c2 * refpoint(at1.radius(), at2.radius(), probe_radius,
                                    at1.crd().distance(at2.crd()));
  X2 = at1.crd() + c1_c3 * refpoint(at1.radius(), at3.radius(), probe_radius,
                                    at1.crd().distance(at3.crd()));
  p1 = c1_c2 % n;
  p2 = c1_c3 % n;
  Y = geom3d::line_intersection(X1, p1, X2, p2);
  tmp = Y - at1.crd();
  dT4Y = std::pow(at1.radius() + probe_radius, 2) - tmp * tmp;
  if (dT4Y < 0)
    return {};
  dT4Y = std::sqrt(dT4Y);
  geom3d::Point center_1 = Y + n * dT4Y;
  geom3d::Point center_2 = Y + n * (-dT4Y);
  return std::pair{center_1, center_2};
}

/**
 * A probe sits in-between three atoms. Now, we assign this probe (and the
 * opposite atom) to each edge of this triangle.
 */
void assign_probes_edges(const Atom *atm1, const Atom *atm2, const Atom *atm3,
                         const geom3d::Point &center, auto &probes,
                         auto &edges) {

  const auto probe =
      std::make_shared<Probe>(center, std::tuple{atm1, atm2, atm3});
  probes.push_back(probe);
  edges[{atm1, atm2}].push_back({probe, const_cast<Atom *>(atm3)});
  if (atm2->atom_number() < atm3->atom_number()) {
    edges[{atm2, atm3}].push_back({probe, const_cast<Atom *>(atm1)});
  } else {
    edges[{atm3, atm2}].push_back({probe, const_cast<Atom *>(atm1)});
  }
  edges[{atm1, atm3}].push_back({probe, const_cast<Atom *>(atm2)});
}

auto sort_probes_by_surface_size(const auto &probes, const auto &colors) {
  int max_color = 0;
  for (const auto &[probe, color] : colors)
    if (color > max_color)
      max_color = color;
  std::vector<std::vector<std::shared_ptr<Probe>>> probes_by_surface_size(
      max_color);
  for (const auto &probe : probes) {
    probes_by_surface_size[colors.at(probe) - 1].push_back(probe);
  }
  std::ranges::sort(probes_by_surface_size, [](const auto &i, const auto &j) {
    return i.size() > j.size();
  });
  return probes_by_surface_size;
}

/**
 * We have an edge with probe centers around it. We have to know which pairs of
 * probe centers are on accessible from one anoher.
 * @param moves output vector of possible moves for each probe center (there can
 * be one for each edge)
 */
void moves_from_centers(const Atom &atm1, const Atom &atm2, auto &edge,
                        auto &moves) {

  if (edge.size() % 2 != 0) {
    dbgmsg("edge size is odd =" << std::to_string(edge.size()));
    edge.pop_back();
  }

  if (edge.empty())
    return;

  const geom3d::Point at21 = atm2.crd() - atm1.crd();
  const auto &[probe0, atm0] = edge[0];
  const auto &[probe0_crd, _unused] = *probe0;
  const geom3d::Point X =
      atm1.crd() + geom3d::project(probe0_crd - atm1.crd(), at21);
  std::vector<double> fi(edge.size());
  std::vector<geom3d::Point> vec(edge.size());

  dbgmsg("edge.size = " << edge.size());
  dbgmsg("probe0->crd = (" << probe0->crd.x << "," << probe0->crd.y << ","
                           << probe0->crd.z << ")");
  vec[0] = probe0_crd - X;
  fi[0] = 0.0;
  for (int i = 1; i < edge.size(); ++i) {
    vec[i] = edge[i].first->first - X;
    fi[i] = geom3d::angle_normal(vec[0], vec[i], at21);
  }
  const double r{std::sqrt(vec[0] * vec[0])};

  const geom3d::Point x_vec{vec[0].norm()};
  const geom3d::Point y_vec{(at21 % x_vec).norm()};

  dbgmsg("x_vec.x = " << x_vec.x() << "x_vec.y = " << x_vec.y()
                      << "x_vec.z = " << x_vec.z());
  dbgmsg("y_vec.x = " << y_vec.x() << "y_vec.y = " << y_vec.y()
                      << "y_vec.z = " << y_vec.z());

  const geom3d::Point center_new =
      rotate_vector_probis(x_vec, y_vec, X, 0.001, r);

  if (center_new.distance(atm0->crd()) > probe0_crd.distance(atm0->crd())) {
    for (int i = 1; i < edge.size(); ++i)
      for (int j = i + 1; j < edge.size(); ++j)
        if (fi[j] < fi[i]) {
          std::swap(edge[i], edge[j]);
          std::swap(fi[i], fi[j]);
          std::swap(vec[i], vec[j]);
        }
  } else {
    fi[0] = 2 * std::numbers::pi;
    for (int i = 1; i < edge.size(); ++i)
      for (int j = i + 1; j < edge.size(); ++j)
        if (fi[j] > fi[i]) {
          std::swap(edge[i], edge[j]);
          std::swap(fi[i], fi[j]);
          std::swap(vec[i], vec[j]);
        }
  }
  for (int i = 0; i < edge.size(); i += 2) {
    const auto &[probe_i, atm_i] = edge[i];
    const auto &[probe_i1, atm_i1] = edge[i + 1];
    moves[probe_i].push_back(probe_i1);
    if (moves[probe_i].size() > 3) {
      throw helper::Error("[WHOOPS] probe->size_move > 3 (=" +
                          std::to_string(moves[probe_i].size()) + ")");
    }
    moves[probe_i1].push_back(probe_i);
    if (moves[probe_i1].size() > 3) {
      throw helper::Error("[WHOOPS] probe->size_move > 3 (=" +
                          std::to_string(moves[probe_i1].size()) + ")");
    }
  }
}

void expand(const auto &probe, const auto &moves, const int curr_color,
            auto &colors) {
  if (colors.contains(probe))
    return;
  colors[probe] = curr_color;
  if (!moves.contains(probe))
    return;
  for (const auto &neighb_probe : moves.at(probe)) {
    expand(neighb_probe, moves, curr_color, colors);
  }
}

/**
 * We give probe centers distinct colors indicating to which cavity they belong
 * to (colors start with one).
 *
 * @return color of the largest surface
 */
auto enumerate_clefts(const auto &probes, const auto &moves) {
  int curr_color = 1; // colors start with 1
  std::map<std::shared_ptr<Probe>, int> colors;
  for (const auto &probe : probes) {
    if (!colors.contains(probe)) {
      expand(probe, moves, curr_color++, colors);
    }
  }
  return colors;
}

/**
 * Find all atoms of the protein, which are less than surf_depth Angstroms (see
 * defs.h) below the surface. The distance begins at the radius of the probe
 * center and ends at the surface of the protein atom. Return value: function
 * returns a map in which each atom is mapped to one (or several) surfaces
 * (colors)
 */
auto get_atoms_below_surface(const auto &grid,
                             const auto &probes_by_surface_size,
                             const double probe_radius, const double surf_depth,
                             const double max_radius) {
  struct ProbeWrapper {
    const Probe &p;
    const double probe_radius;
    const auto &crd() const { return p.first; }
    const auto radius() const { return probe_radius; }
  };
  std::vector<Atom::PSet> atoms_by_surface_size(probes_by_surface_size.size());
  for (auto i{0uz}; const auto &probes : probes_by_surface_size) {
    for (const auto &probe : probes) {
      for (const auto &patom : grid.get_neighbors_atom_radius(
               ProbeWrapper{*probe, probe_radius}, probe_radius + surf_depth,
               max_radius)) {
        atoms_by_surface_size[i].insert(patom);
      }
    }
    ++i;
  }
  std::vector<Atom::PVec> result;
  for (const auto &surf : atoms_by_surface_size) {
    result.push_back(Atom::PVec{std::begin(surf), std::end(surf)});
  }
  return result;
}

std::pair<std::vector<std::vector<Atom *>>,
          std::vector<std::vector<std::shared_ptr<Probe>>>>
compute_surface_atoms(const Atom::PVec &atoms, const double probe_radius,
                      const double surf_depth, const double eps,
                      const double max_radius) {
  helper::Benchmark<std::chrono::milliseconds> b;
  Atom::PSet visited;
  std::vector<std::shared_ptr<Probe>> probes;
  std::map<std::pair<const Atom *, const Atom *>,
           std::vector<std::pair<std::shared_ptr<Probe>, Atom *>>>
      edges;
  std::map<std::shared_ptr<Probe>, std::vector<std::shared_ptr<Probe>>> moves;
  const Atom::Grid grid(atoms);

  for (const auto &atm1 : atoms) {
    if (visited.contains(atm1))
      continue;
    visited.insert(atm1);
    const auto &neighbors =
        grid.get_neighbors_atom_radius(*atm1, 2 * probe_radius, max_radius);
    for (int i = 0; i < neighbors.size(); ++i) {
      const auto &atm2 = neighbors[i];
      if (visited.contains(atm2))
        continue;
      for (int j = i + 1; j < neighbors.size(); ++j) {
        const auto &atm3 = neighbors[j];
        if (visited.contains(atm3))
          continue;
        if (atm3->radius() + 2 * probe_radius + atm2->radius() -
                atm3->crd().distance(atm2->crd()) >
            eps) {
          if (const auto pc =
                  compute_probe_center(*atm1, *atm2, *atm3, probe_radius)) {
            const auto &[center_1, center_2] = pc.value();
            if (!overlap(neighbors, *atm1, center_1, probe_radius, eps))
              assign_probes_edges(atm1, atm2, atm3, center_1, probes, edges);
            if (!overlap(neighbors, *atm1, center_2, probe_radius, eps))
              assign_probes_edges(atm1, atm2, atm3, center_2, probes, edges);
          }
        }
      }
      if (edges.contains({atm1, atm2})) {
        moves_from_centers(*atm1, *atm2, edges.at({atm1, atm2}), moves);
      }
    }
  }
  std::clog << "Calculation of molecular surface took " << b.duration() << " ms"
            << std::endl;

  const auto colors = enumerate_clefts(probes, moves);
  const auto sorted_probes = sort_probes_by_surface_size(probes, colors);
  const auto sorted_atoms = get_atoms_below_surface(
      grid, sorted_probes, probe_radius, surf_depth, max_radius);

  return {sorted_atoms, sorted_probes};
}

} // namespace insilab::molib::algorithms
