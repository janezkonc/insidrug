#include "bond_type.hpp"
#include "internals/typing_steps.hpp"
#include "molib/atom.hpp"

namespace insilab::molib::algorithms {

void compute_rotatable_bonds(const Atom::PVec &atoms) {
  internals::run_steps_for("rotatable_bonds", atoms);
}

void compute_bond_gaff_type(const Atom::PVec &atoms) {
  internals::run_steps_for("bond_gaff_type", atoms);
}

void compute_bond_order(const Atom::PVec &atoms) {
  internals::run_steps_for("bond_order", atoms);
}

} // namespace insilab::molib::algorithms