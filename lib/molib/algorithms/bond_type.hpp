#pragma once

#include "molib/details/common.hpp"

namespace insilab::molib {
class Atom;
class Bond;
} // namespace insilab::molib

namespace insilab::molib::algorithms {

/**
 * Find rotatable bonds in a molecule. Rotatable are bonds that may have several
 * energy minimums in which they preferrentially exist.
 *
 * @note standard protein or nucleic or single-atom residues are ignored
 *
 * @param atoms a vector of pointers to atoms (their bonds are modified with
 * added rotatable property)
 */
void compute_rotatable_bonds(const std::vector<Atom *> &atoms);

/**
 * Find rotatable bonds in a molecule. Rotatable are bonds that may have several
 * energy minimums in which they preferrentially exist.
 *
 * @note standard protein or nucleic or single-atom residues are ignored
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - rotatable types are assigned to bonds as
 * properties)
 */
template <details::HasGetAtoms T> void compute_rotatable_bonds(const T &cont) {
  compute_rotatable_bonds(cont.get_atoms());
}

/**
 * Assign GAFF types for bonds: sb - single bond, db - double bond, tb - triple
 * bond, DL - delocalized bond.
 *
 * @param atoms a vector of pointers to atoms (their bonds are modified with
 * added gaff type property)
 */
void compute_bond_gaff_type(const std::vector<Atom *> &atoms);

/**
 * Assign GAFF types for bonds: sb - single bond, db - double bond, tb - triple
 * bond, DL - delocalized bond.
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - gaff types are assigned to bonds as
 * properties)
 */
template <details::HasGetAtoms T> void compute_bond_gaff_type(const T &cont) {
  compute_bond_gaff_type(cont.get_atoms());
}

/**
 * Assign bond orders to bonds: 1 - single bond, 2 - double bond, 3 - tripple
 * bond.
 *
 * @param atoms a vector of pointers to atoms (their bonds are modified with
 * added bond order property)
 */
void compute_bond_order(const std::vector<Atom *> &atoms);

/**
 * Assign bond orders to bonds: 1 - single bond, 2 - double bond, 3 - tripple
 * bond.
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - bond orders are assigned to bonds as
 * properties)
 */
template <details::HasGetAtoms T> void compute_bond_order(const T &cont) {
  compute_bond_order(cont.get_atoms());
}

} // namespace insilab::molib::algorithms
