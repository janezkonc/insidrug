#include "fragmenter.hpp"
#include "helper/help.hpp"
#include "internals/unique.hpp"
#include "molib/atom.hpp"
#include "molib/bond.hpp"
#include "molib/details/utility.hpp"
#include "molib/molecule.hpp"
#include <iterator>
#include <queue>

namespace insilab::molib::algorithms {

internals::Unique Fragment::__unique{};

std::ostream &operator<<(std::ostream &os, const Fragment::Vec &fragments) {
  for (int i = 0; i < fragments.size(); ++i) {
    auto &fragment = fragments[i];
    os << "FRAGMENT " << i << " SEED_ID = " << fragment.get_seed_id() << " : "
       << std::endl;
    for (auto &patom : fragment.get_core())
      os << "CORE ATOM :        " << *patom;
    for (auto &patom : fragment.get_join())
      os << "JOIN ATOM :        " << *patom;
    os << "-------------------" << std::endl;
  }
  return os;
}

Fragment::Fragment(Atom::PSet core, Atom::PSet join)
    : __core(core), __join(join) {
  /* Seeds are rigid segments with > 1 core atoms
   * THESE ARE NOT SEEDS:
   *
   *         C
   *         |
   *         C          C    etc.
   *        / \        / \
   *       C   C      C   C
   *
   */
  if (__core.size() > 1 || __core.size() + __join.size() > 3) {
    auto p = __unique.get_seed_id(this->get_all());
    __seed_id = p.first;
    __known = p.second;
  } else {
    __seed_id = -1;
    __known = false;
  }
}

Fragment::Vec
compute_overlapping_rigid_segments(const std::vector<Atom *> &atoms) {

  Fragment::Vec fragments;
  Atom::PSet visited;
  for (const auto &pa : atoms) {
    if (visited.contains(pa))
      continue;
    // insert core atoms that are connected to this atom
    // by non-rotatable bond(s)
    Atom::PSet core, join;
    std::queue<Atom *> q;
    q.push(pa);
    while (!q.empty()) {
      Atom &current = *q.front();
      q.pop();
      core.insert(&current);
      visited.insert(&current);
      for (auto &pbond : current.get_bonds())
        if (!visited.contains(&pbond->second_atom(current)) &&
            !pbond->is_rotatable())
          q.push(&pbond->second_atom(current));
    }
    // insert join atoms that are connected to the segment
    // by one rotatable bond
    for (const auto &pbond :
         details::get_bonds_from<details::get_bonds_t::out>(core)) {
      Bond &bond = *pbond;
      if (bond.is_rotatable()) {
        if (!core.contains(&bond.atom1()))
          join.insert(&bond.atom1());
        if (!core.contains(&bond.atom2()))
          join.insert(&bond.atom2());
      }
    }

    const Fragment frag(core, join);
    // ensure segments have >= 3 atoms
    if (frag.size() >= 3) {
      fragments.push_back(frag);
    }
  }
  dbgmsg("------------- RIGID SEGMENTS -------------------" << std::endl
                                                            << fragments);
  dbgmsg("number of rigid fragments found = " << fragments.size());
  return fragments;
}

} // namespace insilab::molib::algorithms
