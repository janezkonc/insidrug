#pragma once

#include "molib/details/common.hpp"

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms {

/**
 * Add hydrogens to atoms by calculating the number of hydrogens for each atom
 * based on IDATM type and then generating correct number of bonds.
 *
 * @note new hydrogen atoms have coordinates set to zero
 *
 * @param atoms a vector of pointers to atoms
 * @return vector of pointers to atoms with newly added hydrogens
 */
std::vector<Atom *> compute_hydrogen(const std::vector<Atom *> &atoms);

/**
 * Add hydrogens to atoms by calculating the number of hydrogens for each atom
 * based on IDATM type and then generating correct number of bonds.
 *
 * @note new hydrogen atoms have coordinates set to zero
 *
 * @param cont any molib container that has get_atoms function (the container is
 * modified with added hydrogen atoms)
 */
template <details::HasGetAtoms T> void compute_hydrogen(const T &cont) {
  compute_hydrogen(cont.get_atoms());
}

/**
 * Erase hydrogens.
 *
 * @param atoms a vector of pointers to atoms
 * @return vector of pointers to atoms with erased hydrogens
 */
std::vector<Atom *> erase_hydrogen(const std::vector<Atom *> &atoms);

/**
 * Erase hydrogens.
 *
 * @param cont any molib container that has get_atoms function (the container is
 * modified with added hydrogen atoms)
 */
template <details::HasGetAtoms T> void erase_hydrogen(const T &cont) {
  erase_hydrogen(cont.get_atoms());
}

} // namespace insilab::molib::algorithms
