#pragma once

#include "molib/details/common.hpp"
#include <exception>
#include <type_traits>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms {

/**
 * Determines if a ring composed of atoms is aromatic or not.
 *
 *  @param ring a vector (or set) of pointers to atoms where atoms are bonded
 * in a way that they form a ring
 *  @return true, if a ring is aromatic, false otherwise
 */
template <details::IsAtomVecOrSet T> bool is_aromatic(const T &ring);

/**
 * Determine types of rings (aromatic, aliphatic, etc.) a set of atoms
 * representing e.g., a molecule, for non-standard residues.Assigned
 * atom properties: AR1 - pure aromatic, AR5 - pure aliphatic, AR2, AR3, AR4 -
 * in-between, NG - not in ring, RGn - size of ring.
 *
 * @note standard protein or nucleic residues are ignored
 *
 * @param atoms a vector of pointers to atoms (expected are atoms at the level
 * of residue, but could be a molecule or even multiple molecules)
 */
void compute_ring_type(const std::vector<Atom *> &atoms);

/**
 * Determine types of rings (aromatic, aliphatic, etc.) in a molecule for
 * non-standard residues. Assigned atom properties: AR1 - pure aromatic, AR5 -
 * pure aliphatic, AR2, AR3, AR4 - in-between, NG - not in ring, RGn - size of
 * ring.
 *
 * @note standard protein or nucleic residues are ignored
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - ring types are assigned to atoms as
 * properties)
 */
template <details::HasGetAtoms T> void compute_ring_type(const T &cont) {
  compute_ring_type(cont.get_atoms());
}

/**
 * Compute General Amber Force Field (GAFF) atom types for small molecules
 * such as synthetic compounds.
 *
 * @note GAFF types are not determined for standard residues
 *
 * @param atoms a vector of pointers to atoms (expected are atoms at the level
 * of at least residue)
 *
 */
void compute_gaff_type(const std::vector<Atom *> &atoms);

/**
 * Compute General Amber Force Field (GAFF) atom types for small molecules
 * such as synthetic compounds.
 *
 * @note GAFF types are not determined for standard residues
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - ring types are assigned to atoms as
 * properties)
 *
 */
template <details::HasGetAtoms T> void compute_gaff_type(const T &cont) {
  compute_gaff_type(cont.get_atoms());
}

/**
 * Compute IDATM types for molecule's atoms.
 *
 * @note throws exception if computing IDATM types fails at some point, which
 * leaves a molecule in an undefined (half computed) state
 *
 * @param atoms a vector of pointers to atoms (expected are atoms at the level
 * of at least residue)
 *
 */
void compute_idatm_type(const std::vector<Atom *> &atoms);

/**
 * Compute IDATM types for molecule's atoms.
 *
 * @note throws exception if computing IDATM types fails at some point, which
 * leaves a molecule in an undefined (half computed) state
 *
 * @param cont any molib container that is a residue or more, e.g., a segment,
 * a chain,... (cont is modified - IDATM types are assigned to atoms as
 * properties)
 *
 */
template <details::HasGetAtoms T> void compute_idatm_type(const T &cont) {
  compute_idatm_type(cont.get_atoms());
}

/**
 * Renumber atoms so that they are numbered consecutively (by atom_number).
 *
 * @note a function that deletes atoms, e.g. erase_hydrogens, leaves gaps in
 * atom numbering, and this function repairs this
 */
void renumber_atoms(const std::vector<Atom *> &atoms);

/**
 * Renumber atoms so that they are numbered consecutively (by atom_number).
 *
 * @note a function that deletes atoms, e.g. erase_hydrogens, leaves gaps in
 * atom numbering, and this function repairs this
 */
template <details::HasGetAtoms T> void renumber_atoms(const T &cont) {
  renumber_atoms(cont.get_atoms());
}

} // namespace insilab::molib::algorithms
