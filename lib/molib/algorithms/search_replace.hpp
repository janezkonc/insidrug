#pragma once

#include "helper/help.hpp"
#include "molib/details/common.hpp"
#include "molib/details/utility.hpp"
#include <functional>
#include <tuple>
#include <utility>

namespace insilab::molib::algorithms {

/**
 * Search for substructure(s) in a molecule based on atom types, connectivity
 * (bonds) and geometry. Apply a rename rule to each found substructure (set of
 * atoms) greedily.
 *
 * @param atoms a vector of pointers to atoms
 * @param rename_rules a vector, in which each element is a struct containing
 * pattern and one or more rules. A pattern is a vector that describes
 * the searched-for substructure as a graph, where each vertex represents a bond
 * with atom number and properties of the first and second atom and bond
 * property that are matched to atoms and bonds in the searched-in molecule.
 * Rules is a vector in which each element defines a subset of atoms (numbers)
 * listed in the corresponding pattern and a string (can be any name), which is
 * assigned to the subset of atoms; if multiple rules can be applied to the same
 * set of atoms, then only the first one will be applied
 * @param compare_atoms a function that returns true if two atoms are equal
 * @param compare_bonds a function that returns true if two bonds are equal
 * @return a vector, in which each element is a pair with first element a vector
 * of pointers to matched atoms (only those that are relevant for the
 * rule), and second element the corresponding matched rule of string type
 */
std::vector<std::pair<std::vector<Atom *>, std::string>> search_substructure(
    const std::vector<Atom *> &atoms, const helper::rename_rules &rename_rules,
    std::function<bool(const Atom &, const Atom &)> compare_atoms =
        details::compare_atom_pseudoatom_by_idatm_prop,
    std::function<bool(const Bond &, const Bond &)> compare_bonds =
        details::compare_bond_pseudobond_by_gaff_bond_order);

/**
 * Search for substructure(s) in a molecule based on atom types, connectivity
 * (bonds) and geometry. Apply a rename rule to each found substructure (set of
 * atoms) greedily.
 *
 * @param cont any molib container that supports get_atoms function
 * @param rename_rules a vector, in which each element is a struct containing
 * pattern and one or more rules. A pattern is a vector that describes
 * the searched-for substructure as a graph, where each vertex represents a bond
 * with atom number and properties of the first and second atom and bond
 * property that are matched to atoms and bonds in the searched-in molecule.
 * Rules is a vector in which each element defines a subset of atoms (numbers)
 * listed in the corresponding pattern and a string (can be any name), which is
 * assigned to the subset of atoms; if multiple rules can be applied to the same
 * set of atoms, then only the first one will be applied
 * @param compare_atoms a function that returns true if two atoms are equal
 * @param compare_bonds a function that returns true if two bonds are equal
 * @return a vector, in which each element is a pair with first element a vector
 * of pointers to matched atoms (only those that are relevant for the
 * rule), and second element the corresponding matched rule of string type
 */
template <details::HasGetAtoms T>
std::vector<std::pair<std::vector<Atom *>, std::string>> search_substructure(
    const T &cont, const helper::rename_rules &rename_rules,
    std::function<bool(const Atom &, const Atom &)> compare_atoms =
        details::compare_atom_pseudoatom_by_idatm_prop,
    std::function<bool(const Bond &, const Bond &)> compare_bonds =
        details::compare_bond_pseudobond_by_gaff_bond_order) requires
    details::HasGetAtoms<T> {
  return search_substructure(cont.get_atoms(), rename_rules, compare_atoms,
                             compare_bonds);
}

/**
 * Search for substructure(s) in a molecule based on atom types, connectivity
 * (bonds) and geometry. Run a user provided function for each match (atoms that
 * are listed in the rule part of a rename_rule).
 *
 * The rename rule for the first smiles graph in the list of rename rules that
 * is a match, is applied, the possible subsequent matches down the list are
 * ignored. A rename rule is simply a string that says what to do with the
 * identified substructure (e.g., a rename rule can "rename" a pair of matched
 * connected atoms with specific atom types to a text saying this is an amide
 * bond).
 *
 * @param atoms a vector of pointers to atoms
 * @param rename_rules rename rules that allow matching any number of atoms
 * of a molecule to a corresponding rule according to local substructure
 * graph (the order of rules is important as the matching is done greedily,
 * so make sure the more specific rules are first)
 * @param apply_rule a function that applies the rule
 * @param compare_atoms a function that returns true if two atoms are equal
 * @param compare_bonds a function that returns true if two bonds are equal
 */
void replace_substructure(
    const std::vector<Atom *> &atoms, const helper::rename_rules &rename_rules,
    std::function<
        void(const std::vector<std::pair<std::vector<Atom *>, std::string>> &)>
        apply_rule = details::apply_rule_atom,
    std::function<bool(const Atom &, const Atom &)> compare_atoms =
        details::compare_atom_pseudoatom_by_idatm_prop,
    std::function<bool(const Bond &, const Bond &)> compare_bonds =
        details::compare_bond_pseudobond_by_gaff_bond_order);

/**
 * Search for substructure(s) in a molecule based on atom types, connectivity
 * (bonds) and geometry. Run a user provided function for each match (atoms that
 * are listed in the rule part of a rename_rule).
 *
 * The rename rule for the first smiles graph in the list of rename rules that
 * is a match, is applied, the possible subsequent matches down the list are
 * ignored. A rename rule is simply a string that says what to do with the
 * identified substructure (e.g., a rename rule can "rename" a pair of matched
 * connected atoms with specific atom types to a text saying this is an amide
 * bond).
 *
 * @param atoms a vector of pointers to atoms
 * @param rename_rules rename rules that allow matching any number of atoms
 * of a molecule to a corresponding rule according to local substructure
 * graph (the order of rules is important as the matching is done greedily,
 * so make sure the more specific rules are first)
 * @param apply_rule a function that applies the rule
 * @param compare_atoms a function that returns true if two atoms are equal
 * @param compare_bonds a function that returns true if two bonds are equal
 */
template <details::HasGetAtoms T>
void replace_substructure(
    const T &cont, const helper::rename_rules &rename_rules,
    std::function<
        void(const std::vector<std::pair<std::vector<Atom *>, std::string>> &)>
        apply_rule = details::apply_rule_atom,
    std::function<bool(const Atom &, const Atom &)> compare_atoms =
        details::compare_atom_pseudoatom_by_idatm_prop,
    std::function<bool(const Bond &, const Bond &)> compare_bonds =
        details::compare_bond_pseudobond_by_gaff_bond_order) {

  replace_substructure(cont.get_atoms(), rename_rules, apply_rule,
                       compare_atoms, compare_bonds);
}

} // namespace insilab::molib::algorithms
