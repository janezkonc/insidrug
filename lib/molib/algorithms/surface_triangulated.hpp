#pragma once

#include <utility>
#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms {

std::pair<std::vector<float>, std::vector<int>>
compute_solvent_accessible_surface(const std::vector<molib::Atom *> &atoms,
                                   const std::vector<int> &rgba);

}