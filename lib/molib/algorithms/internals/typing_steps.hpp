#pragma once

#include <string>
#include <vector>

namespace insilab::molib {
class Atom;
class Residue;
} // namespace insilab::molib

namespace insilab::molib::algorithms::internals {

/// Run steps that progressively type molecule's residues with residue types
/// (rest)
void run_steps_for(const std::string &stop_name,
                   const std::vector<Residue *> &residues,
                   const double moltype_tanimoto_cutoff = 0.8,
                   const unsigned small = 20);

/// Run steps that progressively type molecule's atoms and bonds until (and
/// including) the given step identified by its stop_name
std::vector<Atom *> run_steps_for(const std::string &stop_name,
                                  const std::vector<Atom *> &atoms);

} // namespace insilab::molib::algorithms::internals
