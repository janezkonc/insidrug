#include "typing_steps.hpp"
#include "atom_type_impl.hpp"
#include "bond_type_impl.hpp"
#include "hydrogens_impl.hpp"
#include "molib/details/common.hpp"
#include "molib/residue.hpp"
#include "residue_type_impl.hpp"
#include <functional>
#include <variant>
#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms::internals {

void run_steps_for(const std::string &stop_name, const Residue::PVec &residues,
                   const double moltype_tanimoto_cutoff, const unsigned small) {

  using Func1 = std::function<void(const Residue::PVec &)>;
  using Func2 =
      std::function<void(const Residue::PVec &, const unsigned, const int)>;
  using Func3 = std::function<void(const Residue::PVec &, const int)>;

  static const std::vector<
      std::pair<std::string, std::variant<Func1, Func2, Func3>>>
      steps{
          {"pnwi", internals::compute_protein_nucleic_water_ion},
          {"nonstandard", internals::compute_nonstandard_polymer},
          {"peptides", internals::compute_peptides},
          {"compounds", internals::compute_compounds},
          {"glycans", internals::compute_glycans},
          {"cofactors", internals::compute_cofactors},
          {"buffer", internals::compute_buffer},
          {"covalent", internals::compute_covalent},
      };

  for (const auto &[name, f] : steps) {
    if (std::holds_alternative<Func1>(f))
      std::get<Func1>(f)(residues);
    else if (std::holds_alternative<Func2>(f))
      std::get<Func2>(f)(residues, small, 0);
    else
      std::get<Func3>(f)(residues, moltype_tanimoto_cutoff);
    if (name == stop_name)
      break;
  }
}

Atom::PVec run_steps_for(const std::string &stop_name,
                         const Atom::PVec &atoms) {

  using Func1 = std::function<void(const Atom::PVec &)>;
  using Func2 = std::function<Atom::PVec(const Atom::PVec &, const int)>;

  static const std::vector<std::pair<std::string, std::variant<Func1, Func2>>>
      steps{
          {"", internals::compute_ion},
          {"", internals::compute_idatm_type},
          {"hydrogens", internals::compute_hydrogen},
          {"", internals::compute_bond_order_ions},
          {"bond_order", internals::compute_bond_order},
          {"bond_gaff_type", internals::compute_bond_gaff_type},
          {"idatm_type", internals::refine_idatm_type},
          {"", internals::erase_hydrogen},
          {"", internals::compute_hydrogen},
          {"ring_type", internals::compute_ring_type},
          {"gaff_type", internals::compute_gaff_type},
          {"rotatable_bonds", internals::compute_rotatable_bonds},
          {"", internals::erase_hydrogen},
          {"all", internals::renumber_atoms},
      };

  auto all_atoms = atoms;

  for (const auto &[name, f] : steps) {
    if (std::holds_alternative<Func1>(f))
      std::get<Func1>(f)(all_atoms);
    else {
      // for compute_hydrogen, erase_hydrogen, we need to get new atoms!
      all_atoms = std::get<Func2>(f)(all_atoms, 0);
    }
    if (name == stop_name)
      break;
  }
  if (stop_name != "hydrogens" && stop_name != "all") {
    all_atoms = internals::erase_hydrogen(all_atoms);
    internals::renumber_atoms(all_atoms);
  }
  return all_atoms;
}

} // namespace insilab::molib::algorithms::internals
