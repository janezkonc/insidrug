#pragma once

#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <tuple>
#include <utility>

namespace insilab::molib {
class Atom;
class Molecule;
} // namespace insilab::molib

namespace insilab::molib::algorithms::internals {

class Unique {
  using USeeds =
      std::multimap<std::size_t,
                    std::pair<std::unique_ptr<Molecule>, std::size_t>>;
  USeeds __unique_seeds{};
  bool __match(const std::set<Atom *> &seed, USeeds::iterator, USeeds::iterator,
               std::size_t &);
  std::size_t __hash(const std::set<Atom *> &);
  std::pair<std::size_t, bool> __unique(const std::set<Atom *> &);
  mutable std::mutex __mtx;

public:
  void clear() {
    const std::lock_guard<std::mutex> lock(__mtx);
    __unique_seeds.clear();
  }
  std::pair<std::size_t, bool> get_seed_id(const std::set<Atom *> &a) {
    const std::lock_guard<std::mutex> lock(__mtx);
    return __unique(a);
  }
};

} // namespace insilab::molib::algorithms::internals
