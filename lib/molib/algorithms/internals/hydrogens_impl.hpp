#pragma once

#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms::internals {
std::vector<Atom *> compute_hydrogen(const std::vector<Atom *> &all_atoms,
                                     const int = 0);
std::vector<Atom *> erase_hydrogen(const std::vector<Atom *> &all_atoms,
                                   const int = 0);
} // namespace insilab::molib::algorithms::internals