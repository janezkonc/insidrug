#pragma once

#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms::internals {

void compute_bond_order_ions(const std::vector<Atom *> &all_atoms);
void compute_bond_order(const std::vector<Atom *> &all_atoms);
void compute_bond_gaff_type(const std::vector<Atom *> &all_atoms);
void compute_rotatable_bonds(const std::vector<Atom *> &all_atoms);

} // namespace insilab::molib::algorithms::internals