#include "glib/algorithms/ring.hpp"
#include "helper/help.hpp"
#include "molib/algorithms/search_replace.hpp"
#include "molib/atom.hpp"
#include "molib/details/utility.hpp"

////////////////////////////////////////////////////////////////
//// Local implementation functions for bond_type //////////////
////////////////////////////////////////////////////////////////

namespace insilab::molib::algorithms::internals {

struct AtomParams {
  int val;
  int aps;
  int con;
};

using ValenceState = std::map<Atom *, AtomParams>;
using ValenceStateVec = std::vector<ValenceState>;
using BondToOrder = std::map<Bond *, int>;

class BondOrderError : public helper::Error {
  using helper::Error::Error;
};

bool basic_rules(ValenceState &valence_state, BondToOrder &bond_orders);

std::ostream &operator<<(std::ostream &os, const ValenceState &valence_state) {
  for (auto &kv : valence_state) {
    const Atom &atom = *kv.first;
    const AtomParams &param = kv.second;
    os << "atom = " << atom.idatm_type_unmask() << "_" << atom.atom_number()
       << " valence = " << param.val << " aps = " << param.aps
       << " connectivity = " << param.con << std::endl;
  }
  return os;
}
std::ostream &operator<<(std::ostream &os,
                         const ValenceStateVec &valence_states) {
  for (int i = 0; i < valence_states.size(); ++i) {
    os << "VALENCE STATE NR. " << i << " : " << std::endl << valence_states[i];
  }
  return os;
}
std::ostream &operator<<(std::ostream &os, const BondToOrder &bond_orders) {
  for (auto &kv : bond_orders) {
    const Bond &b = *kv.first;
    os << "bond_order(" << b.atom1().atom_number() << ","
       << b.atom2().atom_number() << ") = " << kv.second << std::endl;
  }
  return os;
}

void dfs(const int level, const int sum, const int tps,
         const std::vector<std::vector<AtomParams>> &V,
         std::vector<AtomParams> &Q,
         std::vector<std::vector<AtomParams>> &valence_states,
         const int max_valence_states) {
  if (valence_states.size() > max_valence_states)
    return;
  dbgmsg("before accessing V at level " << level);
  auto &params = V[level];
  dbgmsg("after accessing V at level " << level);
  for (auto &p : params) {
    dbgmsg("in loop");

    if (p.aps + sum <= tps) {
      Q.push_back(p);
      if (level + 1 < V.size()) {
        internals::dfs(level + 1, p.aps + sum, tps, V, Q, valence_states,
                       max_valence_states);
      } else if (p.aps + sum == tps) {
        // save valence state from Q
        valence_states.push_back(Q);
      }
      Q.pop_back();
    }
  }
  dbgmsg("exiting internals::dfs ");
}

ValenceStateVec create_valence_states(const Atom::PVec &atoms,
                                      const Atom::PSet &out_atoms,
                                      const int max_valence_states) {
#ifndef NDEBUG
  helper::Benchmark b;
#endif
  std::vector<std::vector<AtomParams>>
      V; // vector of AtomParams are sorted by increasing aps
  std::vector<std::vector<AtomParams>> valence_states;
  std::vector<AtomParams> Q;

  Atom::PSet atoms_s(atoms.begin(), atoms.end());

  // prepare for recursive valence state determination
  for (auto &patom : atoms) {

    Atom &atom = *patom;

    dbgmsg(atom);
    V.push_back(std::vector<AtomParams>());
    auto &aps = V.back();

    for (auto &kv : atom.get_aps()) {

      // if atom is from another residue (eg. from HIS or ASN or another NAG),
      // then set its connectivity = valence = 1, since hydrogens were not
      // assigned logic: the first atom in vec is certainly from the residue in
      // question, the "out" atoms are at the end of this vec
      dbgmsg(*atoms.front());
      dbgmsg(atom);
      if (out_atoms.contains(patom)) {

        // determine valence and connectivity
        int val = 0, con = 0;
        for (auto &pa : atom) {
          if (!out_atoms.contains(&pa)) {
            ++val;
            if (atoms_s.contains(&pa)) {
              ++con;
            }
          }
        }

        dbgmsg("determined valence " << val << " and connectivity " << con
                                     << " for " << *patom);

        aps.push_back(AtomParams{val, kv.second, con});
      } else {

        int con = 0;

        for (auto &pa : atom) {
          if (atoms_s.contains(&pa)) {
            ++con;
          }
        }

        aps.push_back(AtomParams{kv.first, kv.second, con});
      }
    }

    // sort increasingly atomic penalty scores for each atom
    std::ranges::sort(aps, [](const AtomParams &i, const AtomParams &j) {
      return i.aps < j.aps;
    });
#ifndef NDEBUG
    for (auto &x : aps)
      dbgmsg(x.val << " " << x.aps << " " << x.con);
#endif
  }
  // recursively find valence states
  for (int tps = 0; tps < 32; ++tps) {
    internals::dfs(0, 0, tps, V, Q, valence_states, max_valence_states);
  }

  // sort valence states within each tps from lower to higher individual aps
  // (issue #103)
  std::ranges::sort(valence_states, [](const std::vector<AtomParams> &i,
                                       const std::vector<AtomParams> &j) {
    int tps_i = 0;
    for (auto &e : i)
      tps_i += e.aps;
    int tps_j = 0;
    for (auto &e : j)
      tps_j += e.aps;
    const AtomParams max_ap_i = *max_element(
        i.begin(), i.end(), [](const AtomParams &iap, const AtomParams &jap) {
          return iap.aps < jap.aps;
        });
    const AtomParams max_ap_j = *max_element(
        j.begin(), j.end(), [](const AtomParams &iap, const AtomParams &jap) {
          return iap.aps < jap.aps;
        });
    if (tps_i == tps_j)
      return max_ap_i.aps < max_ap_j.aps;
    else
      return tps_i < tps_j;
  });

  // convert the result to atom mapping
  ValenceStateVec vss;
  for (auto &valence_state : valence_states) {
    vss.push_back(ValenceState());
    ValenceState &vs = vss.back();
    for (int i = 0; i < valence_state.size(); ++i) {
      vs[atoms[i]] = valence_state[i];
    }
  }
  dbgmsg(vss);
#ifndef NDEBUG
  dbgmsg("Creating valence states took " << b.duration() << "s");
  b.reset();
#endif
  return vss;
}

bool discrepancy(const ValenceState &valence_state) {
  // b) if discrepancy happens (av is not 0 when con is 0 or av is 0 when con is
  // not 0) reset the bond order to 2 and then 3.
  for (auto &kv : valence_state) {
    const Atom &atom = *kv.first;
    const AtomParams &apar = kv.second;
    if (apar.val != 0 && apar.con == 0 || apar.val == 0 && apar.con != 0) {
      return true;
    }
  }
  return false;
}

bool success(const ValenceState &valence_state) {
  // rule 4 : if all the bonds are successfully assigned, con and av
  // of every atom are both 0 (boaf returns 1 and stops)
  for (auto &kv : valence_state) {
    const AtomParams &apar = kv.second;
    dbgmsg("atom = " << kv.first->atom_name() << " val = " << apar.val
                     << " con = " << apar.con << " aps = " << apar.aps);
    if (apar.val != 0 || apar.con != 0) {
      return false;
    }
  }
  return true;
}

Bond &get_first_unassigned_bond(const ValenceState &valence_state,
                                BondToOrder &bond_orders) {
  for (auto &kv : valence_state) {
    const Atom &atom = *kv.first;
    const AtomParams &apar = kv.second;
    for (auto &pbond : atom.get_bonds()) {

      Bond &bond = *pbond;

      dbgmsg("checking bond = " << bond);
      if (!bond_orders.contains(&bond)) { // unassigned bond order
        if (valence_state.contains(&bond.second_atom(atom))) {
          return bond;
        }
      }
    }
  }
  throw BondOrderError("exception : cannot find unassigned bond");
}

void trial_error(ValenceState &valence_state, BondToOrder &bond_orders) {
  for (int bo = 1; bo <= 3; ++bo) {
    // save valence state
    ValenceState saved = valence_state;
    BondToOrder saved_bond_orders = bond_orders;
    // a) assume bond order (of one randomly selected bond) is 1, then continue
    // bond order assignemnt according to the basic rules
    Bond &bond =
        internals::get_first_unassigned_bond(valence_state, bond_orders);
    bond_orders[&bond] = bo;
    AtomParams &apar1 = valence_state.at(&bond.atom1());
    AtomParams &apar2 = valence_state.at(&bond.atom2());
    apar1.con -= 1;
    apar1.val -= bo;
    apar2.con -= 1;
    apar2.val -= bo;
    if (internals::basic_rules(valence_state, bond_orders))
      return;
    // reset valence state to the saved one
    valence_state = saved;
    bond_orders = saved_bond_orders;
  }
  // c) if discrepancies happen for all 3 bond orders, boaf exits and returns 0
  throw BondOrderError(
      "exception : discrepancies happened for all 3 bond orders");
}

bool basic_rules(ValenceState &valence_state, BondToOrder &bond_orders) {
  while (!internals::success(valence_state)) {
    bool bo_was_set = false;
    for (auto &kv : valence_state) {
      Atom &atom = *kv.first;
      AtomParams &apar = kv.second;
      // rule 2 : for one atom, if its con equals to av, the bond
      // orders of its unassigned bonds are set to 1
      if (apar.con == apar.val) {
        for (auto &pbond : atom.get_bonds()) {
          Bond &bond = *pbond;

          if (!bond_orders.contains(&bond)) { // unassigned bond order

            dbgmsg("Is this atom ("
                   << bond.second_atom(atom).atom_number()
                   << ") connected to the rest of the residue check = "
                   << valence_state.contains(&bond.second_atom(atom)));

            if (valence_state.contains(&bond.second_atom(atom))) {
              // rule 1 : for each atom in a bond, if the bond order bo
              // is determined, con is deducted by 1 and av is deducted by bo
              bond_orders[&bond] = 1;
              dbgmsg("getting valence state for atom "
                     << bond.second_atom(atom));
              AtomParams &apar2 = valence_state.at(&bond.second_atom(atom));
              apar.con -= 1;
              apar.val -= 1;
              apar2.con -= 1;
              apar2.val -= 1;
              bo_was_set = true;
            }
          }
        }
      }
      // rule 3 : for one atom, if its con equals to 1, the bond
      // order of the last bond is set to av
      else if (apar.con == 1) {
        for (auto &pbond : atom.get_bonds()) {
          Bond &bond = *pbond;

          if (!bond_orders.contains(&bond)) { // unassigned bond order

            dbgmsg("Is this atom ("
                   << bond.second_atom(atom).atom_number()
                   << ") connected to the rest of the residue check = "
                   << valence_state.contains(&bond.second_atom(atom)));

            if (valence_state.contains(&bond.second_atom(atom))) {

              // rule 1 : for each atom in a bond, if the bond order bo
              // is determined, con is deducted by 1 and av is deducted by bo
              const int bo = apar.val;
              // new rule (Janez) : if trying to assign a zero bond order then
              // try another valence state
              dbgmsg("bo =" << bo);

              if (bo <= 0) {
                throw BondOrderError(
                    "exception : zero bond order for atom " +
                    helper::to_string(atom.atom_number() + " for bond = " +
                                      helper::to_string(bond)));
              }

              bond_orders[&bond] = bo;
              dbgmsg("getting valence state for atom "
                     << bond.second_atom(atom) << " contains = "
                     << valence_state.contains(&bond.second_atom(atom)));
              AtomParams &apar2 = valence_state.at(&bond.second_atom(atom));
              apar.con -= 1;
              apar.val -= bo;
              apar2.con -= 1;
              apar2.val -= bo;
              bo_was_set = true;
            }
          }
        }
      }
    }
    // b) if discrepancy happens (av is not 0 when con is 0 or av is 0 when con
    // is not 0) reset the bond order to 2 and then 3.
    if (internals::discrepancy(valence_state))
      return false;
    // if non of the above rules can be applied do the trial-error test :
    if (!bo_was_set && !internals::success(valence_state))
      internals::trial_error(valence_state, bond_orders);
  }
  return true;
}

bool compute_bond_order_one_try(const Atom::PVec &atoms,
                                const Atom::PSet &out_atoms,
                                const int max_valence_states) {
  // assign atom types for atomic penalty scores
  algorithms::replace_substructure(atoms, helper::atomic_penalty_scores);

  // std::cout << "MOLECULE AFTER ASSIGNING ATOMIC PENALTY SCORES " << std::endl
  //           << Writer::JsonWriter().write(Molecule(atoms)).str() <<
  //           std::endl;

  // for tps 0, 1, 2, 3, ..., N create all possible combinations of valence
  // states
  ValenceStateVec valence_states =
      internals::create_valence_states(atoms, out_atoms, max_valence_states);

  dbgmsg("VALENCE STATES ARE " << std::endl << valence_states << "-----");

  // for each valence state determine bond orders (boaf procedure)
  for (auto &valence_state : valence_states) {
    try {

      BondToOrder bond_orders;

      // some bond orders (i.e., for ions) may already have been assigned
      for (auto &patom : atoms) {
        for (auto &pbond : patom->get_bonds()) {
          if (pbond->get_bo() > 0) {
            dbgmsg("this bond was already assigned bond order " << *pbond);
            bond_orders[pbond] = pbond->get_bo();
          }
        }
      }

      if (internals::basic_rules(valence_state, bond_orders)) {
        dbgmsg("successfully determined bond orders at max_valence_states "
               << max_valence_states << " for molecule : " << std::endl
               << bond_orders);
        // set the newly determined bond orders
        for (auto &kv : bond_orders) {
          Bond &bond = *kv.first;
          bond.set_bo(kv.second);
        }
        dbgmsg("MOLECULE AFTER COMPUTING BOND ORDERS"
               << std::endl
               << IOmanip::pdb << IOmanip::extended << atoms);
        return true;
      }
    } catch (const BondOrderError &e) {
      dbgmsg(e.what());
    }
  }
  return false;
}

void compute_bond_order_try_valences(const Atom::PVec &atoms,
                                     const Atom::PSet &out_atoms) {
  dbgmsg("MOLECULE BEFORE ASSIGNING ATOMIC PENALTY SCORES "
         << std::endl
         << IOmanip::pdb << IOmanip::extended << atoms);

  auto max_valence_states = 2000;
  for (; max_valence_states < 10000; max_valence_states += 2000) {

    if (internals::compute_bond_order_one_try(atoms, out_atoms,
                                              max_valence_states)) {
      return;
    }
  }

  // if boaf fails for all saved valence states, a warning message is given
  throw helper::Error(
      "[WHOOPS] bond order assignment failed max_valence_states " +
      helper::to_string(max_valence_states) +
      " for residue = " + helper::to_string(atoms));
}

void compute_bond_order(const Atom::PVec &all_atoms) {
  try {
    // filter out standard residues and ions
    const auto atoms = details::filter_atoms(all_atoms, details::comp_nsi);

    // if there are multiple molecules (not connected) deal with each separately
    for (auto component : glib::algorithms::find_components(atoms)) {
      // account for inter-residue atoms eg., HEM is bound to histidine, so also
      // N from histidine needs to be added
      const auto out_bonds =
          details::get_bonds_from<details::get_bonds_t::out>(component);
      const auto out_atoms = details::get_atoms_from(out_bonds);

      // get all atoms that stick out except ions
      Atom::PSet out_component;
      std::ranges::copy_if(
          out_atoms, std::inserter(out_component, std::end(out_component)),
          [&component](const auto &patom) {
            return !component.contains(patom) &&
                   !details::is_ion(patom->residue().rest());
          });
      component.insert(std::begin(out_component), std::end(out_component));
      internals::compute_bond_order_try_valences(
          Atom::PVec(std::begin(component), std::end(component)),
          out_component);
    }
  } catch (const std::exception &e) {
    std::cerr << "[NOTE] Bond order assignment failed for atoms " << all_atoms
              << std::endl;
    throw e;
  }
}

void compute_bond_gaff_type(const Atom::PVec &all_atoms) {
  // filter out standard residues
  const auto atoms = details::filter_atoms(all_atoms, details::comp_ns);
  // assign atom types for atomic penalty scores
  algorithms::replace_substructure(atoms, helper::bond_gaff_type,
                                   details::apply_rule_bond);
}

void compute_rotatable_bonds(const Atom::PVec &all_atoms) {
  // filter out standard residues
  const auto atoms = details::filter_atoms(all_atoms, details::comp_ns);

  algorithms::replace_substructure(atoms, helper::rotatable,
                                   details::apply_rule_bond);
  // rotatable bond inside a ring is changed back to non-rotatable
  const auto rings = glib::algorithms::find_fused_rings(atoms);
  for (const auto &ring : rings) {
    for (const auto &pbond :
         details::get_bonds_from<details::get_bonds_t::in>(ring)) {
      pbond->set_rotatable("");
    }
  }
}

void compute_bond_order_ions(const Atom::PVec &all_atoms) {
  // filter out everything but ions
  const auto atoms = details::filter_atoms(all_atoms, details::comp_i);
  Residue::PSet ions;
  std::ranges::transform(atoms, std::inserter(ions, std::end(ions)),
                         [](const auto &patom) { return &patom->residue(); });
  // for ions, just set all bond orders to one (single bond)
  for (const auto &presidue : ions) {
    for (const auto &patom : presidue->get_atoms()) {
      for (const auto &pbond : patom->get_bonds()) {
        pbond->set_bo(1);
      }
    }
  }
}

} // namespace insilab::molib::algorithms::internals
