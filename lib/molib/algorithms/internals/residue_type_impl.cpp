#include "cofactors.hpp"
#include "glib/algorithms/ring.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "lisica/lisica.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/chain.hpp"
#include "molib/details/residuewrap.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "ommiface/prepare_for_mm.hpp"
#include "sugars.hpp"

////////////////////////////////////////////////////////////////
//// Local implementation functions for residue_type ///////////
////////////////////////////////////////////////////////////////

namespace insilab::molib::algorithms::internals {

bool is_cofactor(const Atom::PVec &atoms,
                 const double moltype_tanimoto_cutoff) {

  const static Molecules cofactors_d =
      Molecules::Parser("karkoli.mol2", Molecules::Parser::Options::all_models)
          .parse(data::cofactors);

  using namespace lisica;
  try {
    const auto &allowed = {Element::C, Element::N, Element::O, Element::S,
                           Element::P};
    Atom::PVec filtered;
    for (const auto &patom : atoms) {
      if (std::count(std::begin(allowed), std::end(allowed), patom->element()))
        filtered.push_back(patom);
    }
    Lisica l(filtered);
    l.set_compare_atom_types(Lisica::CompareAtomTypes::element);
    l.run(cofactors_d);
    const auto &tanimoto_list = l.get_tanimoto_list();
    if (tanimoto_list.empty())
      throw helper::Error(
          "[WHOOPS] LiSiCA returned empty Tanimoto list while testing "
          "if ligand is a cofactor!");
    return tanimoto_list[0].first > moltype_tanimoto_cutoff;

  } catch (const std::exception &e) {
    std::cerr << e.what() << " skipping..." << std::endl;
    return false;
  }
}

bool is_sugar(const Atom::PVec &atoms, const double moltype_tanimoto_cutoff) {

  const static Molecules sugars_d =
      Molecules::Parser("karkoli.mol2", Molecules::Parser::Options::all_models)
          .parse(data::sugars);

  using namespace lisica;
  try {
    Lisica l(atoms);
    l.set_compare_atom_types(Lisica::CompareAtomTypes::element);
    l.run(sugars_d);
    std::vector<pair<double, std::string>> tanimoto_list =
        l.get_tanimoto_list();
    if (tanimoto_list.empty())
      throw helper::Error(
          "[WHOOPS] LiSiCA returned empty Tanimoto list while testing "
          "if ligand is a sugar");
    return tanimoto_list[0].first > moltype_tanimoto_cutoff;

  } catch (const std::exception &e) {
    std::cerr << e.what() << " skipping..." << std::endl;
    return false;
  }
}

void compute_cofactors(const Residue::PVec &residues,
                       const double moltype_tanimoto_cutoff) {
  for (const auto &presidue : residues) {
    if (details::is_compound(presidue->rest())) {
      if (algorithms::is_cofactor(*presidue, moltype_tanimoto_cutoff)) {
        presidue->set_rest(Residue::Type::small | Residue::Type::cofactor);
      }
    }
  }
}

void compute_buffer(const Residue::PVec &residues) {
  for (const auto &presidue : residues) {
    if (details::is_compound(presidue->rest()) &&
            helper::non_specific_binders.contains(presidue->resn()) ||
        details::is_ion(presidue->rest()) &&
            helper::non_specific_ion_binders.contains(presidue->resn())) {
      // exp. section is not checked, because it may contain biologically
      // relevant ions, e.g., 2xqc
      presidue->set_rest(Residue::Type::buffer);
    }
  }
}

void compute_glycans(const Residue::PVec &residues,
                     const double moltype_tanimoto_cutoff) {
  Residue::PVec sugars;
  std::ranges::copy_if(residues, std::back_inserter(sugars),
                       [&moltype_tanimoto_cutoff](const auto &presidue) {
                         return details::is_compound(presidue->rest()) &&
                                algorithms::is_sugar(*presidue,
                                                     moltype_tanimoto_cutoff);
                       });
  // determine separate oligosaccharides
  const auto &residue_w = details::ResidueWrap::create_graph(sugars);
  const auto &components = glib::algorithms::find_components(
      residue_w); // find disconnected subgraphs
  // determine which of the oligosaccharides are attached to the protein
  std::vector<Residue::PVec> glycans;
  for (const auto &oligowrap : components) {
    Residue::PVec oligo;
    std::ranges::transform(
        oligowrap, std::back_inserter(oligo), [](const auto &presidue_w) {
          return const_cast<Residue *>(&presidue_w->get_residue());
        });
    for (const auto &psugar : oligo) {
      for (const auto &sugar_atom : *psugar) {
        for (const auto &adj_a : sugar_atom) {
          const auto &adjacent = adj_a.residue();
          // N-glycan attached to Asn
          // O-glycan attached to Ser or Thr
          if (adjacent.resn() == "ASN" || adjacent.resn() == "SER" ||
              adjacent.resn() == "THR") {
            glycans.push_back(oligo);
            break;
          }
        }
      }
    }
  }
  // amend residue types
  for (auto i{0uz}; i < glycans.size(); ++i) {
    for (const auto &psugar : glycans[i]) {
      psugar->set_rest(Residue::Type::glycan);
    }
  }
}

void compute_protein_nucleic_water_ion(const Residue::PVec &residues) {
  for (const auto &presidue : residues) {
    const auto &molecule = presidue->molecule();
    const auto &chain = presidue->chain();
    const auto residue_id = Residue::Id(chain.chain_id(), presidue->resn(),
                                        presidue->resi(), presidue->ins_code());
    if (helper::amino_acids.contains(presidue->resn()) &&
        !molecule.is_hetero(residue_id))
      presidue->set_rest(Residue::Type::protein);
    else if (helper::nucleic_acids_DNA.contains(presidue->resn()) &&
             !molecule.is_hetero(residue_id))
      presidue->set_rest(Residue::Type::nucleic | Residue::Type::DNA);
    else if (helper::nucleic_acids_RNA.contains(presidue->resn()) &&
             !molecule.is_hetero(residue_id))
      presidue->set_rest(Residue::Type::nucleic | Residue::Type::RNA);
    else if (helper::ions.contains(presidue->resn()))
      presidue->set_rest(Residue::Type::ion);
    else if (presidue->resn() == "HOH")
      presidue->set_rest(Residue::Type::water);
    else
      presidue->set_rest(Residue::Type::notassigned);
  }
}

void compute_ion(const Atom::PVec &atoms) {
  Residue::PSet residues;
  std::ranges::transform(atoms, std::inserter(residues, std::end(residues)),
                         [](const auto &patom) { return &patom->residue(); });
  for (const auto &presidue : residues) {
    if (helper::ions.contains(presidue->resn()))
      presidue->set_rest(Residue::Type::ion);
  }
}

void compute_nonstandard_polymer(const Residue::PVec &residues) {
  Segment::PSet segments;
  std::ranges::transform(
      residues, std::inserter(segments, std::end(segments)),
      [](const auto &presidue) { return &presidue->segment(); });
  // determine rest's for segments (esp. protein or nucleic)
  std::map<Segment *, Residue::Type> segment_to_rest;
  std::ranges::transform(
      segments, std::inserter(segment_to_rest, std::end(segment_to_rest)),
      [](const auto &psegment) -> std::pair<Segment *, Residue::Type> {
        for (const auto &residue : *psegment) {
          if (details::is_protein(residue.rest()) ||
              details::is_nucleic(residue.rest()))
            return {psegment, residue.rest()};
        }
        return {psegment, Residue::Type::notassigned};
      });
  for (const auto &presidue : residues) {
    const auto segment_rest = segment_to_rest.at(&presidue->segment());
    if (segment_rest == Residue::Type::notassigned ||
        presidue->rest() != Residue::Type::notassigned ||
        helper::amino_acids.contains(presidue->resn()) ||
        helper::nucleic_acids_DNA.contains(presidue->resn()) ||
        helper::nucleic_acids_RNA.contains(presidue->resn()))
      continue;
    const auto &molecule = presidue->molecule();
    const auto &chain = presidue->chain();
    // check if a residue (must not be a standard amino acid or nucleic acid) is
    // consecutive chemical components covalently linked in a linear fashion to
    // form a polymer
    if (molecule.is_seqres(presidue->resn()) || // is part of polpypeptide chain
        molecule.is_modified(Residue::Id(chain.chain_id(), presidue->resn(),
                                         presidue->resi(),
                                         presidue->ins_code()))) {
      presidue->set_rest(segment_rest | Residue::Type::nonstandard);
    }
  }
}

void compute_peptides(const Residue::PVec &residues, const unsigned small,
                      const int) {
  Segment::PSet segments;
  std::ranges::transform(
      residues, std::inserter(segments, std::end(segments)),
      [](const auto &presidue) { return &presidue->segment(); });
  // determine size (=num. resi) for protein segments
  std::map<Segment *, std::size_t> segment_to_size;
  std::ranges::transform(
      segments, std::inserter(segment_to_size, std::end(segment_to_size)),
      [](const auto &psegment) -> std::pair<Segment *, size_t> {
        const auto sz =
            std::ranges::count_if(psegment->get_residues(), [](const auto &pr) {
              return details::is_protein(pr->rest());
            });
        return {psegment, sz};
      });
  for (const auto &presidue : residues) {
    const auto sz = segment_to_size.at(&presidue->segment());
    if (!sz)
      continue;
    if (details::is_protein(presidue->rest()))
      presidue->set_rest(presidue->rest() |
                         (sz < small ? Residue::Type::oligopeptide
                                     : Residue::Type::polypeptide));
  }
}

void compute_compounds(const Residue::PVec &residues) {
  for (const auto &presidue : residues) {
    const auto &molecule = presidue->molecule();
    const auto &chain = presidue->chain();
    if (!molecule.is_hetero(Residue::Id(chain.chain_id(), presidue->resn(),
                                        presidue->resi(),
                                        presidue->ins_code())))
      continue;
    if (details::is_notassigned(presidue->rest()))
      presidue->set_rest(Residue::Type::small | Residue::Type::compound);
  }
}

void compute_covalent(const Residue::PVec &residues) {
  for (const auto &presidue : residues) {
    if (details::is_cofactor(presidue->rest()) ||
        details::is_compound(presidue->rest())) {
      for (const auto &atom : *presidue) {
        for (const auto &neighb : atom) {
          if (details::is_protein(neighb.residue().rest())) {
            presidue->set_rest(presidue->rest() | Residue::Type::covalent);
            goto end_of_loop;
          }
        }
      }
    end_of_loop:;
    }
  }
}

std::vector<Atom::PVec>
split_biochemical_components(const Atom::PVec &all_atoms) {

  helper::Benchmark<std::chrono::milliseconds> b;

  ommiface::connect_bonds_standard_residues(all_atoms);

  // group atoms by segment and rest
  std::map<std::pair<Segment *, Residue::Type>, Atom::PVec> split;
  for (const auto &patom : all_atoms) {
    const auto rest = details::unset_nonstandard(patom->residue().rest());
    split[{&patom->segment(), rest}].push_back(patom);
  }
  std::vector<Atom::PVec> result;
  for (const auto &[key, atoms] : split) {
    const auto &[segment_id, rest] = key;
    // for proteins and nucleic acids the whole segment is copied to the result
    // since these polymers may be broken into several pieces due to missing
    // residues
    if (details::is_protein(rest) || details::is_nucleic(rest)) {
      result.push_back(atoms);
      continue;
    }
    // for other molecule types find separate entities (components) within each
    // segment/rest pair
    for (const auto &component : glib::algorithms::find_components(atoms)) {
      result.push_back(Atom::PVec{std::begin(component), std::end(component)});
    }
  }

  std::clog << "Splitting a molecule into biochemical components took "
            << b.duration() << " ms and resulted in " << result.size()
            << " components.\n";

  return result;
}

Residue::Type determine_protein_nucleic(const std::string &resn) {
  if (helper::amino_acids.contains(resn))
    return Residue::Type::protein;
  else if (helper::nucleic_acids_DNA.contains(resn) ||
           helper::nucleic_acids_RNA.contains(resn))
    return Residue::Type::nucleic;
  return Residue::Type::notassigned;
}

} // namespace insilab::molib::algorithms::internals
