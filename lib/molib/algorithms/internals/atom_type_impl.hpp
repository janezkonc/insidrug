#pragma once

#include "molib/details/common.hpp"
#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms::internals {

template <details::IsAtomVecOrSet T> bool is_aromatic(const T &ring);
void compute_gaff_type(const std::vector<Atom *> &all_atoms);
void compute_ring_type(const std::vector<Atom *> &all_atoms);
void compute_idatm_type(const std::vector<Atom *> &atoms);
void refine_idatm_type(const std::vector<Atom *> &all_atoms);
void renumber_atoms(const std::vector<Atom *> &atoms);

} // namespace insilab::molib::algorithms::internals