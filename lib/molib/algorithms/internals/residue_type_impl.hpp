#pragma once

#include <vector>

namespace insilab::molib {
class Atom;
class Residue;
} // namespace insilab::molib

namespace insilab::molib::details {
enum class Rest : std::size_t;
}

namespace insilab::molib::algorithms::internals {

bool is_cofactor(const std::vector<Atom *> &atoms,
                 const double moltype_tanimoto_cutoff);
bool is_sugar(const std::vector<Atom *> &atoms,
              const double moltype_tanimoto_cutoff);

void compute_cofactors(const std::vector<Residue *> &residues,
                       const double moltype_tanimoto_cutoff = 0.8);
void compute_buffer(const std::vector<Residue *> &residues);
void compute_glycans(const std::vector<Residue *> &residues,
                     const double moltype_tanimoto_cutoff = 0.8);
void compute_protein_nucleic_water_ion(const std::vector<Residue *> &residues);
void compute_ion(const std::vector<Atom *> &atoms);
void compute_nonstandard_polymer(const std::vector<Residue *> &residues);
void compute_peptides(const std::vector<Residue *> &residues,
                      const unsigned small, const int = 0);
void compute_compounds(const std::vector<Residue *> &residues);
void compute_covalent(const std::vector<Residue *> &residues);
std::vector<std::vector<Atom *>>
split_biochemical_components(const std::vector<Atom *> &all_atoms);
details::Rest determine_protein_nucleic(const std::string &resn);

} // namespace insilab::molib::algorithms::internals