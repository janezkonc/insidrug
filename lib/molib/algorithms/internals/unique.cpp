#include "unique.hpp"
#include "glib/algorithms/match.hpp"
#include "molib/algorithms/geometry.hpp"
#include "molib/details/utility.hpp"
#include "molib/io/iomanip.hpp"
#include "molib/molecule.hpp"

namespace insilab::molib::algorithms::internals {

bool Unique::__match(const Atom::PSet &seed, USeeds::iterator it1,
                     USeeds::iterator it2, std::size_t &seed_id) {

  while (it1 != it2) {
    const auto &saved_seed = it1->second.first->get_atoms();
    if (glib::algorithms::isomorphic(
            seed, saved_seed, [](const auto &a1, const auto &a2) {
              return a1.idatm_type_unmask() == a2.idatm_type_unmask();
            })) {
      // even if graph are isomorphic, seeds can still differ (e.g.,
      // enantiomers)
      Atom::PVec atoms1(seed.begin(), seed.end());
      if (algorithms::compute_rmsd(atoms1, saved_seed, true) < 0.0001) {
        seed_id = it1->second.second;
        return true;
      }
    }
    it1++;
  }
  return false;
}

std::size_t Unique::__hash(const Atom::PSet &atoms) {
  std::map<std::string, int> chemical_formula;
  for (const auto &a : atoms) {
    assert(!a->idatm_type_unmask().empty());
    chemical_formula[a->idatm_type_unmask()]++;
  }
  std::stringstream ss;
  for (const auto &[label, cnt] : chemical_formula)
    ss << label << " " << cnt;
  return std::hash<std::string>{}(ss.str());
}

std::pair<std::size_t, bool> Unique::__unique(const Atom::PSet &seed) {
  std::size_t hsh = __hash(seed);
  std::size_t seed_id{0};
  bool known = true;
  const auto ret = __unique_seeds.equal_range(hsh);
  // seed's hash OR graph doesn't match any hash OR graph already in db, so add
  // seed
  if (ret.first == ret.second ||
      !__match(seed, ret.first, ret.second, seed_id)) {
    seed_id = __unique_seeds.size();
    known = false;
    molib::Molecule seed_molecule(seed);
    __unique_seeds.emplace(
        hsh,
        std::pair{std::make_unique<molib::Molecule>(seed_molecule), seed_id});
  }
  return {seed_id, known};
}

} // namespace insilab::molib::algorithms::internals
