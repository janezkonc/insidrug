#include "glib/algorithms/ring.hpp"
#include "helper/help.hpp"
#include "molib/atom.hpp"
#include "molib/details/utility.hpp"
#include "molib/io/iomanip.hpp"
#include "molib/molecule.hpp"

////////////////////////////////////////////////////////////////
//// Local implementation functions for hydrogens //////////////
////////////////////////////////////////////////////////////////

namespace insilab::molib::algorithms::internals {

Atom::PVec compute_hydrogen(const Atom::PVec &all_atoms, const int) {
  // filter out standard residues and ions
  const auto atoms = details::filter_atoms(all_atoms, details::comp_nsi);
  const auto diff_atoms = helper::vector_difference(all_atoms, atoms);

  if (atoms.empty()) {
    // add atoms that were filtered out
    return diff_atoms;
  }

  Atom::PVec result{atoms};
  // atom numbers must be unique for the WHOLE molecule, that is why
  // max atom number has to be found on molecule level
  int max_atom_number = 0;
  const auto &molecule = atoms.front()->molecule();
  for (const auto &patom : molecule.get_atoms())
    max_atom_number = std::max(max_atom_number, patom->atom_number());

  for (const auto &patom : atoms) {
    Residue &residue = const_cast<Residue &>(patom->residue());
    // don't visit "just added" hydrogens
    if (patom->element() == Element::H)
      continue;
    int con = helper::get_info_map(patom->idatm_type_unmask()).substituents;

    /* EXCEPTION for 3-substituted P */
    if (patom->idatm_type_unmask() == "P" && patom->size() == 3)
      con = 3;
    /* ***************************** */

    int num_h = con - patom->size();
    if (num_h > 0) {
      // computing num_h missing hydrogens for atom
      // add dummy hydrogen atoms with zero coordinates
      for (int i = 0; i < num_h; ++i) {
        const auto &idatm_type = (patom->element() == Element::C ? "HC" : "H");
        Atom &hatom = residue.add(Atom({
            .atom_number = ++max_atom_number,
            .atom_name = "H",
            .element = Element::H,
            .idatm_type = helper::idatm_mask.at(idatm_type),
        }));
        patom->connect(hatom);
        result.push_back(&hatom);
      }
    } else if (num_h < 0) {
      // deleting excess hydrogens because according to IDATM type this atom
      // should have less hydrogens!
      int h_excess = std::abs(num_h);
      for (int i = 0; i < patom->size(); ++i) {
        auto &bondee = (*patom)[i];
        if (bondee.element() == Element::H && h_excess-- > 0) {
          patom->erase(i--);
          auto &shpbond = patom->get_shared_ptr_bond(bondee);
          const Bond &deleted_bond = *shpbond;
          Bond::erase_stale_refs(deleted_bond,
                                 patom->get_bonds()); // delete references
          patom->erase_bond(bondee);
          bondee.erase_bond(*patom);
          residue.erase(bondee.atom_number());
          const auto num_erased = std::erase(result, &bondee);
          if (num_erased == 0)
            throw helper::Error(
                "[WHOOPS] Cannot find newly added hydrogen in vector!");
        }
      }
      if (h_excess > 0)
        throw helper::Error("[WHOOPS] Deleting of excess hydrogens failed!");
    }
  }
  // connect the new hydrogen bonds with the rest of the bond-graph
  Bond::erase_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(result));
  Bond::connect_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(result));

  // add atoms that were filtered out
  result.insert(result.end(), diff_atoms.begin(), diff_atoms.end());
  // sort resulting non-H atoms according to their atoms numbers
  std::ranges::sort(result, [](const auto &pa1, const auto &pa2) {
    return pa1->atom_number() < pa2->atom_number();
  });
  dbgmsg("MOLECULE AFTER COMPUTING HYDROGENS " << std::endl
                                               << IOmanip::pdb
                                               << IOmanip::extended << result);
  dbgmsg("BONDS AFTER COMPUTING HYDROGENS " << std::endl
                                            << details::get_bonds_in(result));
  return result;
}

Atom::PVec erase_hydrogen(const Atom::PVec &all_atoms, const int) {
  // filter out standard residues and ions
  const auto atoms = details::filter_atoms(all_atoms, details::comp_nsi);
  // calculate only the atoms that were filtered out
  const auto diff_atoms = helper::vector_difference(all_atoms, atoms);
  // calculate standard and ion residues without H atoms (these are needed at
  // the end of this function)
  const auto no_h_atoms =
      details::filter_atoms(diff_atoms, [](const auto &patom) {
        return patom->element() != Element::H;
      });

  Atom::PVec result{atoms};
  std::erase_if(result, [](const auto &patom) {
    const auto &hatom = *patom;
    if (hatom.element() != Element::H)
      return false;
    // H is always bonded to just one other heavy atom
    assert(!hatom.empty());
    auto &bondee = hatom[0];
    // delete the H from bondee's neighbor list
    for (int i = 0; i < bondee.size(); ++i) {
      if (bondee[i].atom_number() == hatom.atom_number()) {
        bondee.erase(i);
        break;
      }
    }
    // erase bond to bondee-H
    bondee.erase_bond(hatom);
    // erase H from this residue
    Residue &residue = const_cast<Residue &>(hatom.residue());
    residue.erase(hatom.atom_number());
    return true; // erase H from atoms vector
  });

  // we have to reconnect the bond graph from scratch
  Bond::erase_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(result));
  Bond::connect_bonds(
      details::get_bonds_from<details::get_bonds_t::in_as_set>(result));

  dbgmsg("MOLECULE AFTER ERASING HYDROGENS " << std::endl
                                             << IOmanip::pdb
                                             << IOmanip::extended << result);
  result.insert(result.end(), no_h_atoms.begin(), no_h_atoms.end());
  // sort resulting non-H atoms according to their atoms numbers
  std::ranges::sort(result, [](const auto &pa1, const auto &pa2) {
    return pa1->atom_number() < pa2->atom_number();
  });

  return result;
}

} // namespace insilab::molib::algorithms::internals
