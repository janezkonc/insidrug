#include "geometry.hpp"
#include "cluster/greedy.hpp"
#include "geom3d/coordinate.hpp"
#include "geom3d/kabsch.hpp"
#include "geom3d/linear.hpp"
#include "glib/algorithms/match.hpp"
#include "molib/atom.hpp"
#include <vector>

namespace insilab::molib::algorithms {

Atom &find_most_distant_atom(const std::vector<Atom *> &atoms,
                             const geom3d::Coordinate &crd) {
  if (atoms.empty())
    throw helper::Error(
        "[WHOOPS] Trying to find most distant atom for empty set of atoms!");
  double md_sq = -1.0;
  Atom *result{atoms.front()};
  for (const auto &patom : atoms) {
    const double dist_sq = crd.distance_sq(patom->crd());
    if (dist_sq > md_sq) {
      md_sq = dist_sq;
      result = patom;
    }
  }
  return *result;
}

std::pair<std::vector<geom3d::Coordinate>, std::vector<double>>
compute_centroids(const Atom::PVec &atoms, const double centro_clus_rad) {

  std::pair<geom3d::Point::Vec, std::vector<double>> result;

  geom3d::Point::Vec crds;
  for (const auto &patom : atoms)
    crds.push_back(patom->crd());
  return geom3d::compute_simplified_representation(crds, centro_clus_rad);
}

double compute_rmsd(const Atom::PVec &first, const Atom::PVec &second,
                    const bool superimpose) {

  if (first.size() != second.size())
    throw helper::Error(
        "[WHOOPS] RMSD can only be calculated for two conformations "
        "with the same topology!");
  const auto m = glib::algorithms::match(
      first, second, glib::algorithms::compute_shortest_path_matrix(first),
      [](const auto &a, const auto &b) {
        return a.atom_name() == b.atom_name();
      });

  // try calculating RMSD of each mapping of molecule to molecule and return the
  // smallest one...
  double min_rmsd_sq = std::numeric_limits<double>::max();
  for (const auto &[vertices1, vertices2] : m) {
    // match must stretch over the whole graph first (and second)
    if (vertices2.size() != first.size())
      throw helper::Error("[WHOOPS] RMSD calculation aborted due to imperfect "
                          "match found, check your input!");

    geom3d::Point::Vec crds1, crds2;

    for (auto i{0uz}; i < vertices2.size(); ++i) {

      const geom3d::Point &crd1 = first[vertices1[i]]->crd();
      const geom3d::Point &crd2 = second[vertices2[i]]->crd();

      crds1.push_back(crd1);
      crds2.push_back(crd2);
    }

    // if requested, superimpose crds2 onto crds1
    if (superimpose) {

      geom3d::Kabsch k;
      crds2 = k.superimpose(crds1, crds2);
    }

    double rmsd_sq = geom3d::compute_rmsd_sq(crds1, crds2);
    min_rmsd_sq = std::min(min_rmsd_sq, rmsd_sq);
  }
  return std::sqrt(min_rmsd_sq);
}

} // namespace insilab::molib::algorithms
