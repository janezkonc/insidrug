#include "atom_type.hpp"
#include "internals/atom_type_impl.hpp"
#include "internals/typing_steps.hpp"
#include "molib/atom.hpp"
#include "molib/details/common.hpp"

namespace insilab::molib::algorithms {

template <details::IsAtomVecOrSet T> bool is_aromatic(const T &ring) {
  return internals::is_aromatic(ring);
}

template bool is_aromatic(const Atom::PVec &ring);
template bool is_aromatic(const Atom::PSet &ring);

void compute_ring_type(const Atom::PVec &atoms) {
  internals::run_steps_for("ring_type", atoms);
}

void compute_gaff_type(const Atom::PVec &atoms) {
  internals::run_steps_for("gaff_type", atoms);
}

void compute_idatm_type(const Atom::PVec &atoms) {
  internals::run_steps_for("idatm_type", atoms);
}

void renumber_atoms(const std::vector<Atom *> &atoms) {
  internals::renumber_atoms(atoms);
}

} // namespace insilab::molib::algorithms
