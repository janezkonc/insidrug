#pragma once

#include "molib/details/common.hpp"
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <limits>

namespace insilab::geom3d {
class Coordinate;
class Matrix;
Coordinate compute_geometric_center(const std::vector<Coordinate> &crds);
double compute_hausdorff_distance(const std::vector<Coordinate> &first,
                                  const std::vector<Coordinate> &second,
                                  const double max_hd);
double compute_max_radius(const Coordinate &center,
                          const std::vector<Coordinate> &crds);
std::pair<std::vector<Coordinate>, std::vector<double>>
compute_simplified_representation(const std::vector<Coordinate> &points,
                                  const double clus_rad);
double compute_rmsd(const std::vector<Coordinate> &crds1,
                    const std::vector<Coordinate> &crds2);

} // namespace insilab::geom3d

namespace insilab::molib::algorithms {

/**
 * Find atom in a molecule that is farthest away from a given point.
 *
 * Throws if atoms is empty.
 *
 * @param atoms a vector of pointers to atoms
 * @param crd a coordinate from which we calculate distance
 * @return a reference to the most distant atom
 *
 */
Atom &find_most_distant_atom(const std::vector<Atom *> &atoms,
                             const geom3d::Coordinate &crd);

/**
 * Find atom in a molecule that is farthest away from a given point.
 *
 * Throws if atoms is empty.
 *
 * @param cont any molib container that supports get_atoms function
 * @param crd a coordinate from which we calculate distance
 * @return a reference to the most distant atom
 *
 */
template <details::HasGetAtoms T>
Atom &find_most_distant_atom(const T &cont, const geom3d::Coordinate &crd) {
  return find_most_distant_atom(cont.get_atoms(), crd);
}

/**
 * Calculate geometric center coordinate of a molecule.
 *
 * @param cont any molib container that supports get_crds function
 * @return a Coordinate, geoemtric center of a molecule
 *
 */
template <details::HasGetCrds T> auto compute_geometric_center(const T &cont) {
  return geom3d::compute_geometric_center(cont.get_crds());
}

/**
 * Calculate distance to the farthest atom (its center) from the geometric
 * center of a molecule.
 *
 * @param cont any molib container that supports get_crds function
 * @return maximum distance from geometric center to any atom in this molecule
 */
template <details::HasGetCrds T> auto compute_max_radius(const T &cont) {
  return geom3d::compute_max_radius(compute_geometric_center(cont),
                                    cont.get_crds());
}

/**
 * Compute a modified (min instead of max) Hausdorff distance between two
 * molecules, which is a good measure for how well they overlap.

 * @param cont1 any molib container that supports get_crds function
 * @param cont2 any molib container that supports get_crds function
 * @param max_hd maximum possible hausdorff distance
 * @return Hausdorff distance or std::numeric_limits<double>::max() if hd >=
 * max_hd
 */
template <details::HasGetCrds T, details::HasGetCrds P>
auto compute_hausdorff_distance(
    const T &cont1, const P &cont2,
    const double max_hd = std::numeric_limits<double>::max()) {
  return geom3d::compute_hausdorff_distance(cont1.get_crds(), cont2.get_crds(),
                                            max_hd);
}

/**
 * Perturb coordinates of a molecule's atoms by a small random offset. By
 * default by one-thousand'th of an Angstrom.
 *
 * @param cont any molib container that supports get_atoms function (contains
 * new coordinates after the function returns)
 * @param factor how much to jiggle (lower is more)
 *
 */
template <details::HasGetAtoms T>
void jiggle_atoms(T &cont, const double factor = 1000) {
  std::srand(std::time(nullptr));
  for (auto &patom : cont.get_atoms()) {
    patom->set_crd(
        patom->crd() +
        geom3d::Coordinate(((double)std::rand() / (double)RAND_MAX) / factor,
                           ((double)std::rand() / (double)RAND_MAX) / factor,
                           ((double)std::rand() / (double)RAND_MAX) / factor));
  }
}

/**
 * Represent one or more molecules overlapping in 3D space as a set of
 * centroids, i.e., (x,y,z) points, each with a radius r. The centroids cover
 * the space occupied by the molecule(s), and there are usually fewer centroids
 * than atoms, thus simplifying the representation of molecule(s).
 *
 * @param atoms a vector of pointers to atoms
 * @param centro_clus_rad the minimum distance between centroids
 * @return a pair, in which the first element is a vector of coordinates (x,y,z)
 * and the second element is a vector of the corresponding radiuses r
 */
std::pair<std::vector<geom3d::Coordinate>, std::vector<double>>
compute_centroids(const std::vector<Atom *> &atoms,
                  const double centro_clus_rad);

/**
 * Represent one or more molecules overlapping in 3D space as a set of
 * centroids, i.e., (x,y,z) points, each with a radius r. The centroids cover
 * the space occupied by the molecule(s), and there are usually fewer centroids
 * than atoms, thus simplifying the representation of molecule(s).
 *
 * @param cont any molib container that supports get_crds function
 * @param centro_clus_rad the minimum distance between centroids
 * @return a pair, in which the first element is a vector of coordinates (x,y,z)
 * and the second element is a vector of the corresponding radiuses r
 */
template <details::HasGetCrds T>
std::pair<std::vector<geom3d::Coordinate>, std::vector<double>>
compute_centroids(const T &cont, const double centro_clus_rad) {
  return geom3d::compute_simplified_representation(cont.get_crds(),
                                                   centro_clus_rad);
}

/**
 * Calculate root-mean-square deviation (RMSD) between two equally sized sets of
 * atoms representing two conformations of the same molecule. Optionally, the
 * molecules can be superimposed before RMSD calculation.
 *
 * The order of atoms in both vectors is not important as atoms are treated as
 * graph and they are topologically matched before RMSD calculation.
 *
 * @param first first vector of pointers to atoms
 * @param second second vector of pointers to atoms
 * @param superimpose if true, superimpose beforehand, using Kabsch algorithm
 * @return minimum RMSD that could be found
 */
double compute_rmsd(const std::vector<Atom *> &first,
                    const std::vector<Atom *> &second, const bool superimpose);

/**
 * Calculate root-mean-square deviation (RMSD) between two conformations of the
 * same molecule. Optionally, the molecules can be superimposed before RMSD
 * calculation.
 *
 * The order of atoms in both molecules is not important as atoms are treated as
 * graph and they are topologically matched before RMSD calculation.
 *
 * @param first first molecule
 * @param second second molecule
 * @param superimpose if true, superimpose beforehand, using Kabsch algorithm
 * @return minimum RMSD that could be found
 */
template <details::HasGetAtoms T>
double compute_rmsd(const T &cont1, const T &cont2, bool superimpose) {
  return compute_rmsd(cont1.get_atoms(), cont2.get_atoms(), superimpose);
}

/**
 * Calculate root-mean-square deviation (RMSD) between two conformations of the
 * same molecule. The atoms in both molecules must be in the same order.
 *
 * @param first first molecule
 * @param second second molecule
 * @return RMSD
 */
template <details::HasGetCrds T>
double compute_rmsd_ord(const T &cont1, const T &cont2) {
  return geom3d::compute_rmsd(cont1.get_crds(), cont2.get_crds());
}

} // namespace insilab::molib::algorithms
