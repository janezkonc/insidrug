#include "search_replace.hpp"
#include "glib/algorithms/match.hpp"
#include "helper/help.hpp"
#include "molib/atom.hpp"
#include "molib/details/utility.hpp"
#include <regex>
#include <vector>

namespace insilab::molib::algorithms {

/// Convert from a match withing bond graph (vertices are bonds) to a match
/// within atom graph (vertcies are atoms)
auto convert_to_atom_match(
    const std::map<const Bond *, const Bond *> &bond_match, const bool reverse,
    std::function<bool(const Atom &, const Atom &)> compare_atoms) {
  std::map<int, const Atom *> amatch;
  std::set<const Bond *> visited;
  std::queue<const Bond *> q;
  q.push(bond_match.begin()->first);
  while (!q.empty()) {
    const Bond &bond1 = *q.front();
    q.pop();
    const Atom &atom11 = bond1.atom1();
    const Atom &atom12 = bond1.atom2();
    const int anum11 = atom11.atom_number();
    const int anum12 = atom12.atom_number();

    const Bond &bond2 = *bond_match.at(&bond1);
    const Atom &atom21 = bond2.atom1();
    const Atom &atom22 = bond2.atom2();

    if (visited.empty()) { // first bond-match considered
      if (!reverse) {
        if (compare_atoms(atom21, atom11)) {
          amatch[anum11] = &atom21;
          amatch[anum12] = &atom22;
        } else
          throw helper::Error("[NOTE] Trying with reversed first bond");
      } else {
        if (compare_atoms(atom22, atom11)) {
          amatch[anum11] = &atom22;
          amatch[anum12] = &atom21;
        } else
          throw helper::Error("[NOTE] Ran out of options");
      }
    } else { // not first bond
      if (amatch[anum11] == &atom21 && compare_atoms(atom22, atom12))
        amatch[anum12] = &atom22;
      else if (amatch[anum11] == &atom22 && compare_atoms(atom21, atom12))
        amatch[anum12] = &atom21;
      else if (amatch[anum12] == &atom21 && compare_atoms(atom22, atom11))
        amatch[anum11] = &atom22;
      else if (amatch[anum12] == &atom22 && compare_atoms(atom21, atom11))
        amatch[anum11] = &atom21;
      else
        throw helper::Error(
            "[NOTE] Cannot convert from bond match to atom match");
    }
    visited.insert(&bond1);
    for (const auto &bond : bond1)
      if (!visited.contains(&bond) && bond_match.contains(&bond))
        q.push(&bond);
  }
  return amatch;
}

/// In molecule's atoms find a substructure defined as "smiles" graph.
auto grep(const std::vector<Bond *> &bonds,
          const std::vector<std::unique_ptr<Bond>> &smiles_graph,
          std::function<bool(const Atom &, const Atom &)> compare_atoms,
          std::function<bool(const Bond &, const Bond &)> compare_bonds,
          const helper::Array2d<glib::node_idx_t> &distance_matrix1) {

  std::vector<std::map<int, const Atom *>> mvec;

  const auto compare_bonds_atoms =
      [&compare_atoms, &compare_bonds](const Bond &bond1, const Bond &bond2) {
        return (compare_atoms(bond1.atom1(), bond2.atom1()) &&
                    compare_atoms(bond1.atom2(), bond2.atom2()) ||
                compare_atoms(bond1.atom1(), bond2.atom2()) &&
                    compare_atoms(bond1.atom2(), bond2.atom1())) &&
               compare_bonds(bond1, bond2);
      };
  const auto &matches = glib::algorithms::match(
      bonds, smiles_graph, distance_matrix1, compare_bonds_atoms);
  for (const auto &match : matches) {
    std::map<const Bond *, const Bond *> bond_match;
    for (int i = 0; i < match.first.size(); ++i) {
      const auto &nid1 = match.first[i];
      const auto &nid2 = match.second[i];
      Bond &bond1 = *smiles_graph[nid2];
      Bond &bond2 = *bonds[nid1];
      bond_match[&bond1] = &bond2;
    }
    for (const bool reverse :
         {false, true}) { // two tries : forward and reverse
      try {
        const auto &m =
            convert_to_atom_match(bond_match, reverse, compare_atoms);
        mvec.push_back(m);
      } catch (const std::exception &e) {
        // before the overhaul (commit 415) we were catching Error here, did it
        // have any significance?
      }
    }
  }
  return mvec;
}

std::vector<std::pair<Atom::PVec, std::string>> search_substructure(
    const Atom::PVec &atoms, const helper::rename_rules &rename_rules,
    std::function<bool(const Atom &, const Atom &)> compare_atoms,
    std::function<bool(const Bond &, const Bond &)> compare_bonds) {

  assert(!rename_rules.empty());

  helper::Benchmark<std::chrono::milliseconds> b;

  /// Assign a rule string to the set of matched atoms
  const auto assign_rule = [](const std::map<int, const Atom *> &m,
                              const std::vector<std::string> &rules,
                              auto &assigned_rules) {
    for (const auto &rule : rules) {
      const auto &vec1 = helper::split(rule, ":", true);
      const auto &vec2 = helper::split<int>(
          vec1[0], ",", true,
          [](const std::string &s) -> int { return std::stoi(s); });
      assert(vec1.size() == 2);
      assert(!vec2.empty());
      std::set<const Atom *> matched_atoms; // needed as map's key!
      Atom::CPVec matched_atoms_v;          // preserves order of atoms
      for (const auto &atom_number : vec2) {
        const Atom &atom = *m.at(atom_number);
        matched_atoms.insert(&atom);
        matched_atoms_v.push_back(&atom);
      }
      assigned_rules.emplace(matched_atoms,
                             std::pair{vec1[1], matched_atoms_v});
    }
  };

  std::map<std::set<const Atom *>, std::pair<std::string, Atom::CPVec>>
      assigned_rules;

  // below is optimization for issue #121 that allows distance_matrix for graph1
  // to be computed only once (and not for every rename_rule as before);
  // distance matrix is pre-computed using the largest smiles graph as max_dist
  // parameter so as to cover all rename_rules

  std::vector<std::pair<std::vector<std::unique_ptr<Bond>>,
                        std::vector<std::unique_ptr<Atom>>>>
      precomputed_smiles_graphs;
  for (const auto &p : rename_rules) {
    precomputed_smiles_graphs.push_back(details::create_bonds(
        p.pattern)); // be careful not declare smiles_graph as const
                     // reference (this results in seg. fault!)
  }
  const auto &bonds = details::get_bonds_from<details::get_bonds_t::in>(atoms);
  const auto max_g2_sz =
      std::ranges::max_element(precomputed_smiles_graphs,
                               [](const auto &item1, const auto &item2) {
                                 return item1.first.size() < item2.first.size();
                               })
          ->first.size();
  const auto &distance_matrix1 =
      glib::algorithms::compute_shortest_path_matrix_sparse_graph(bonds,
                                                                  max_g2_sz);
  for (auto i{0uz}; i < rename_rules.size(); ++i) {
    const auto &p = rename_rules[i];
    const auto &atom_matches =
        grep(bonds, precomputed_smiles_graphs[i].first, compare_atoms,
             compare_bonds, distance_matrix1);
    for (const auto &atom_match : atom_matches) {
      // rule is assigned greedily: assigned_rules is map and we use emplace
      // which Inserts a new element if there is no element with the key in the
      // container
      assign_rule(atom_match, p.rule, assigned_rules);
    }
  }
  std::vector<std::pair<Atom::PVec, std::string>> result;
  for (const auto &[_unused, rule_matched_atoms] : assigned_rules) {
    const auto &[rule, matched_atoms] = rule_matched_atoms;
    Atom::PVec non_const_matched_atoms;
    for (const auto &patom : matched_atoms) {
      non_const_matched_atoms.push_back(const_cast<Atom *>(patom));
    }
    result.emplace_back(non_const_matched_atoms, rule);
  }

  dbgmsg("Search substructure took " << b.duration() << " ms and resulted in "
                                     << result.size() << " matches found.\n");

  return result;
}

void replace_substructure(
    const std::vector<Atom *> &atoms, const helper::rename_rules &rename_rules,
    std::function<
        void(const std::vector<std::pair<std::vector<Atom *>, std::string>> &)>
        apply_rule,
    std::function<bool(const Atom &, const Atom &)> compare_atoms,
    std::function<bool(const Bond &, const Bond &)> compare_bonds) {

  const auto matches =
      search_substructure(atoms, rename_rules, compare_atoms, compare_bonds);
  apply_rule(matches);
}

} // namespace insilab::molib::algorithms
