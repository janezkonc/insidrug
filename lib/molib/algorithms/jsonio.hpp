#pragma once

#include "nlohmann/json.hpp"

/*********************************************************************************
 * Helper functions to_json and from_json are required by nlohmann's Json
 * library and enable to automatically convert an object to/from json. More
 * information at: https://github.com/nlohmann/json#arbitrary-types-conversions
 *********************************************************************************/

namespace insilab::molib::algorithms {

class Fragment;

void to_json(nlohmann::json &fragment_j, const Fragment &fragment);

} // namespace insilab::molib::algorithms
