#pragma once

#include "jsonio.hpp" // gives Json powers
#include "molib/details/common.hpp"
#include <ostream>
#include <set>
#include <vector>

namespace insilab::molib {
class Atom;
}

namespace insilab::molib::algorithms::internals {
class Unique;
}

namespace insilab::molib::algorithms {

// A fragment, consists of core atoms (rigid parts) and join atoms (extend into
// rotatable bonds), unique seed_id > 0 if it is a larger fragment (-1 if it is
// not seed), known = true if the fragment was already seen.
class Fragment {
  std::set<Atom *> __core, __join;
  int __seed_id;
  bool __known;

  static internals::Unique __unique;

public:
  using Vec = std::vector<Fragment>;

  Fragment(std::set<Atom *> core, std::set<Atom *> join);
  std::set<Atom *> &get_core() { return __core; }
  std::set<Atom *> &get_join() { return __join; }
  const std::set<Atom *> &get_core() const { return __core; }
  const std::set<Atom *> &get_join() const { return __join; }
  std::set<Atom *> get_all() const {
    std::set<Atom *> all{__core};
    all.insert(__join.begin(), __join.end());
    return all;
  }
  bool is_seed() const { return __seed_id != -1; }
  bool is_known() const { return __known; }
  int get_seed_id() const { return __seed_id; }
  int size() const { return __core.size() + __join.size(); }

  friend std::ostream &operator<<(std::ostream &os, const Vec &fragments);
};

/**
 * Compute fragments that are separated by rotatable bonds (rigid). Label atoms
 * that are part of rigid strucuture as 'core', and those that reach into
 * rotatable bonds as 'join' atoms.
 *
 * @note this function calls Molecule's copy constructor in one of the Unique's
 * methods while reading seeds from file
 *
 * @param atoms vector of pointers to atoms representing a molecule that is to
 * be fragmented
 * @return vector of fragments
 */
Fragment::Vec
compute_overlapping_rigid_segments(const std::vector<Atom *> &atoms);

/**
 * Compute fragments that are separated by rotatable bonds (rigid). Label atoms
 * that are part of rigid strucuture as 'core', and those that reach into
 * rotatable bonds as 'join' atoms.
 *
 * @note this function calls Molecule's copy constructor in one of the Unique's
 * methods while reading seeds from file
 * @param cont any molib container that supports get_atoms function
 * @return vector of fragments
 */
template <details::HasGetAtoms T>
Fragment::Vec compute_overlapping_rigid_segments(const T &cont) {
  return compute_overlapping_rigid_segments(cont.get_atoms());
}

} // namespace insilab::molib::algorithms
