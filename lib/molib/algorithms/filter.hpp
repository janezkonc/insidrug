#pragma once

#include "molib/details/common.hpp"
#include <functional>
#include <optional>
#include <set>
#include <string>
#include <variant>
#include <vector>

namespace insilab::molib {
class Atom;
class Molecule;
} // namespace insilab::molib

namespace insilab::molib::details {
enum class Rest : std::size_t;
}

namespace insilab::grid {
template <typename T> class Grid;
}

namespace insilab::molib::algorithms {

enum class Key {
  P_OPEN,
  P_CLOSE,
  OR,
  AND,
  NOT,
  ATOM_NAME,
  RESN,
  RESI,
  REST,
  ENTITY,
  SEGI,
  CHAIN,
  MODEL,
  MOLECULE_NAME,
  AROUND,
  WITHIN,
  OF,
  BYRES,
  ALL,
  END,
  NUMBER_D,
  NUMBER_I,
  REST_V,
  GRID_V,
  STRING,
  ERROR,
  GRID,
  CONTAINER,
  CONTAINER_V,
};

using ExprVec =
    std::vector<std::variant<Key, details::Rest, double, int, std::string,
                             std::reference_wrapper<const grid::Grid<Atom>>,
                             std::reference_wrapper<const Molecule>>>;

std::ostream &operator<<(std::ostream &o, const ExprVec &expr_v);

/**
 * Filter atoms using an expression very similar to PyMOL's selection algebra.
 * For example, to get only protein residues, use filter expression: {Key::REST,
 * Residue::Type::protein}. See test/check_filter.cpp for more examples.
 *
 * @param atoms a vector of pointers to atoms
 * @param filter_expr a vector with filter expression
 * @return a vector of pointers to atoms in the existing molecule that passed
 * the filter
 */
std::vector<Atom *> filter(const std::vector<Atom *> &atoms,
                           const ExprVec &filter_expr);

/**
 * Filter atoms using an expression very similar to PyMOL's selection algebra.
 * For example, to get only protein residues, use filter expression: {Key::REST,
 * Residue::Type::protein}. See test/check_filter.cpp for more examples.
 *
 * @param cont any molib container that supports get_atoms
 * @param filter_expr a vector with filter expression
 * @return a vector of pointers to atoms in the existing molecule that passed
 * the filter
 */
template <details::HasGetAtoms T>
std::vector<Atom *> filter(const T &cont, const ExprVec &filter_expr) {
  return filter(cont.get_atoms(), filter_expr);
}

} // namespace insilab::molib::algorithms
