#pragma once
#include "geom3d/coordinate.hpp"
#include "molib/details/common.hpp"
#include <tuple>
#include <utility>

namespace insilab::molib::algorithms {

constexpr double PROBE = 1.4;
constexpr double SURF = 1.1;
constexpr double EPS = 0.001;
constexpr double MAXR = 2.0; // originally 1.8 (see vdw radii in helper.cpp)

using Probe = std::pair<geom3d::Point,
                        std::tuple<const Atom *, const Atom *, const Atom *>>;

/**
 * Compute molecular surface atoms and surface probes, where each probe touches
 * three neighboring atoms. There can be several molecular surfaces, e.g., a
 * main one facing the solution and several smaller that are closed inside a
 * protein.
 *
 * @param atoms vector of pointers to atoms
 * @return a pair in which the first element is a vector of vectors of surface
 * atoms; the second element is a vector of vectors of corresponding surface
 * probes; both vectors are sorted from the largest to the smallest surface
 * according to surface probes
 */
std::pair<std::vector<std::vector<Atom *>>,
          std::vector<std::vector<std::shared_ptr<Probe>>>>
compute_surface_atoms(const std::vector<Atom *> &atoms,
                      const double probe_radius = PROBE,
                      const double surf_depth = SURF, const double eps = EPS,
                      const double max_radius = MAXR);

/**
 * Compute molecular surface atoms and surface probes, where each probe touches
 * three neighboring atoms. There can be several molecular surfaces, e.g., a
 * main one facing the solution and several smaller that are closed inside a
 * protein.
 *
 * @param cont any molib container that supports get_atoms function
 * @return a pair in which the first element is a vector of vectors of surface
 * atoms; the second element is a vector of vectors of corresponding surface
 * probes; both vectors are sorted from the largest to the smallest surface
 * according to surface probes
 */
template <details::HasGetAtoms T>
std::pair<std::vector<std::vector<Atom *>>,
          std::vector<std::vector<std::shared_ptr<Probe>>>>
compute_surface_atoms(const T &cont, const double probe_radius = PROBE,
                      const double surf_depth = SURF, const double eps = EPS,
                      const double max_radius = MAXR) {
  return compute_surface_atoms(cont.get_atoms(), probe_radius, surf_depth, eps,
                               max_radius);
}

} // namespace insilab::molib::algorithms
