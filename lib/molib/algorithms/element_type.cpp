#include "element_type.hpp"
#include "helper/help.hpp"
#include "molib/details/rest.hpp"

namespace insilab::molib::algorithms {

std::string determine_element(const std::string &atom_name,
                              const details::Rest &rest) {
  if (details::is_protein(rest) || details::is_nucleic(rest)) {
    if (helper::heavy_atoms.contains(atom_name))
      return helper::heavy_atoms.at(atom_name);
    else if (helper::protein_hydrogen_atoms.contains(atom_name))
      return helper::protein_hydrogen_atoms.at(atom_name);
    else if (helper::nucleic_hydrogen_atoms.contains(atom_name))
      return helper::nucleic_hydrogen_atoms.at(atom_name);
  }
  return atom_name;
}

} // namespace insilab::molib::algorithms