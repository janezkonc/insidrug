#include "hydrogens.hpp"
#include "internals/hydrogens_impl.hpp"
#include "internals/typing_steps.hpp"
#include "molib/atom.hpp"

namespace insilab::molib::algorithms {

Atom::PVec compute_hydrogen(const Atom::PVec &atoms) {
  return internals::run_steps_for("hydrogens", atoms);
}

Atom::PVec erase_hydrogen(const Atom::PVec &atoms) {
  return internals::erase_hydrogen(atoms);
}

} // namespace insilab::molib::algorithms
