#pragma once

#include "molib/details/common.hpp"
#include <string>
#include <vector>

namespace insilab::molib {
class Atom;
class Residue;
} // namespace insilab::molib

namespace insilab::molib::details {
enum class Rest : std::size_t;
}

namespace insilab::molib::algorithms {

/**
 * Determine if a molecule is a cofactor by comparing it to known cofactors.
 *
 * @note considered are only C N O S P atoms otherwise it will fail for heme (if
 * one has Fe connected to nitrogens and the other does not, shortest paths will
 * be different, resulting in low Tanimoto).
 *
 * @param atoms a vector of pointer to atoms
 * @param moltype_tanimoto_cutoff [0.0-1.0] similarity threshold that the typed
 * molecule needs to be above to be typed as a cofactor
 * @return true, if the input molecule is a cofactor
 */
bool is_cofactor(const std::vector<Atom *> &atoms,
                 const double moltype_tanimoto_cutoff = 0.8);

/**
 * Determine if a molecule is a cofactor by comparing it to known cofactors.
 *
 * @note considered are only C N O S P atoms otherwise it will fail for heme (if
 * one has Fe connected to nitrogens and the other does not, shortest paths will
 * be different, resulting in low Tanimoto).
 *
 * @param cont any molib container supporting get_atoms function
 * @param moltype_tanimoto_cutoff [0.0-1.0] similarity threshold that the typed
 * molecule needs to be above to be typed as a cofactor
 * @return true, if the input molecule is a cofactor
 */
template <details::HasGetAtoms T>
bool is_cofactor(const T &cont, const double moltype_tanimoto_cutoff = 0.8) {
  return is_cofactor(cont.get_atoms());
}

/**
 * Determine if a molecule is a monosaccharide by comparing it to known sugars.
 *
 * @param atoms a vector of pointer to atoms
 * @param moltype_tanimoto_cutoff [0.0-1.0] similarity threshold that the typed
 * molecule needs to be above to be typed as a sugar
 * @return true, if the input molecule is a sugar
 */
bool is_sugar(const std::vector<Atom *> &atoms,
              const double moltype_tanimoto_cutoff = 0.8);

/**
 * Determine if a molecule is a monosaccharide by comparing it to known sugars.
 *
 * @param cont any molib container supporting get_atoms function
 * @param moltype_tanimoto_cutoff [0.0-1.0] similarity threshold that the typed
 * molecule needs to be above to be typed as a sugar
 * @return true, if the input molecule is a sugar
 */
template <details::HasGetAtoms T>
bool is_sugar(const T &cont, const double moltype_tanimoto_cutoff = 0.8) {
  return is_sugar(cont.get_atoms());
}

/**
 * Determine if a compound is a cofactor by 2D structural comparison to known
 * cofactors. This procedure correctly identifies ions that are part of a
 * cofactor for Fe in heme and Co in (methyl-)cobalamine.
 *
 * @param residues a vector of pointer to residues
 */
void compute_cofactors(const std::vector<Residue *> &residues);

/**
 * Determine if a compound is a cofactor by 2D structural comparison to known
 * cofactors. This procedure correctly identifies ions that are part of a
 * cofactor for Fe in heme and Co in (methyl-)cobalamine.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T> void compute_cofactors(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_cofactors({const_cast<Residue *>(&cont)});
  else
    compute_cofactors(cont.get_residues());
}

/**
 * Determine buffer compounds and ions, i.e., things that are there due to
 * experimental conditions of structure determination.
 *
 * @param residues a vector of pointer to residues
 */
void compute_buffer(const std::vector<Residue *> &residues);

/**
 * Determine buffer compounds and ions, i.e., things that are there due to
 * experimental conditions of structure determination.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T> void compute_buffer(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_buffer({const_cast<Residue *>(&cont)});
  else
    compute_buffer(cont.get_residues());
}

/**
 * Determine glycan-components based on similarity to a list of known
 * monosaccharides and connect them into (branched) glycans.
 *
 * @param residues a vector of pointer to residues
 */
void compute_glycans(const std::vector<Residue *> &residues);

/**
 * Determine glycan-components based on similarity to a list of known
 * monosaccharides and connect them into (branched) glycans.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T> void compute_glycans(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_glycans({const_cast<Residue *>(&cont)});
  else
    compute_glycans(cont.get_residues());
}

/**
 * Determine protein, nucleic, ion and water residues. These types may be more
 * precisely qualified with additional residue types in later typing steps.
 * Protein: standard amino acids and not hetero (to avoid typing free amino
 *          acid ligands as protein)
 * Nucleic: similar as protein
 *
 * @param residues a vector of pointer to residues
 */
void compute_protein_nucleic_water_ion(const std::vector<Residue *> &residues);

/**
 * Determine protein, nucleic, ion and water residues. These types may be more
 * precisely qualified with additional residue types in later typing steps.
 * Protein: standard amino acids and not hetero (to avoid typing free amino
 *          acid ligands as protein)
 * Nucleic: similar as protein
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T>
void compute_protein_nucleic_water_ion(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_protein_nucleic_water_ion({const_cast<Residue *>(&cont)});
  else
    compute_protein_nucleic_water_ion(cont.get_residues());
}

/**
 * Determine nonstandard and modified residues that are part of polymer chain
 * (protein or nucleic), e.g., are linked to it by peptide bonds.
 *
 * @note compute_residue_types MUST be run before this function!
 * @note amino acids that are free ligands (eg. ASP in 2pcu) are not considered
 * by checking that a residue is not one of standard amino acids or nucleic
 * acids.
 *
 * @param residues a vector of pointer to residues
 */
void compute_nonstandard_polymer(const std::vector<Residue *> &residues);

/**
 * Determine nonstandard and modified residues that are part of polymer chain
 * (protein or nucleic), e.g., are linked to it by peptide bonds.
 *
 * @note compute_residue_types MUST be run before this function!
 * @note amino acids that are free ligands (eg. ASP in 2pcu) are not considered
 * by checking that a residue is not one of standard amino acids or nucleic
 * acids.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T>
void compute_nonstandard_polymer(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_nonstandard_polymer({const_cast<Residue *>(&cont)});
  else
    compute_nonstandard_polymer(cont.get_residues());
}

/**
 * Differentiate proteins into oligopeptides (short peptides) and polypeptides.
 *
 * @param residues a vector of pointer to residues
 * @param small cutoff for the number of residues below which a protein is
 * considered a peptide
 */
void compute_peptides(const std::vector<Residue *>, const unsigned small = 20);

/**
 * Differentiate proteins into oligopeptides (short peptides) and polypeptides.
 *
 * @param cont any molib container above and including a Residue
 * @param small cutoff for the number of residues below which a protein is
 * considered a peptide
 */
template <details::ResiduePlus T>
void compute_peptides(const T &cont, const unsigned small = 20) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_peptides({const_cast<Residue *>(&cont)}, small);
  else
    compute_peptides(cont.get_residues(), small);
}

/**
 * Determine compounds as hetero atoms.
 *
 * @param residues a vector of pointer to residues
 */
void compute_compounds(const std::vector<Residue *> &residues);

/**
 * Determine compounds as hetero atoms.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T> void compute_compounds(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_compounds({const_cast<Residue *>(&cont)});
  else
    compute_compounds(cont.get_residues());
}

/**
 * Compounds or cofactors that are covalently linked to the polypeptide chain,
 * e.g., covalent inhibitors.
 *
 * @param residues a vector of pointer to residues
 */
void compute_covalent(const std::vector<Residue *> &residues);

/**
 * Compounds or cofactors that are covalently linked to the polypeptide chain,
 * e.g., covalent inhibitors.
 *
 * @param cont any molib container above and including a Residue
 */
template <details::ResiduePlus T> void compute_covalent(const T &cont) {
  if constexpr (std::is_same_v<T, Residue>)
    compute_covalent({const_cast<Residue *>(&cont)});
  else
    compute_covalent(cont.get_residues());
}

/**
 * Split a molecule into one or several entities based on determined residue
 * types and chemical bonds.
 *
 * @note residues that represent a separate biological component (e.g., a glycan
 * unit, a cofactor, a protein chain, etc.) are grouped as a set of atoms
 *
 * @param atoms a vector of pointer to atoms
 * @return a vector of biochemical components, where each component is
 * represented as a vector of pointers to atoms (where pointers refer to the
 * input molecule)
 */
std::vector<std::vector<Atom *>>
split_biochemical_components(const std::vector<Atom *> &atoms);

/**
 * Split a molecule into one or several entities based on determined residue
 * types and chemical bonds.
 *
 * @note residues that represent a separate biological component (e.g., a glycan
 * unit, a cofactor, a protein chain, etc.) are grouped as a set of atoms
 *
 * @param cont any molib container above and including a Residue
 * @return a vector of biochemical components, where each component is
 * represented as a vector of pointers to atoms (where pointers refer to the
 * input molecule)
 */
template <details::HasGetAtoms T>
std::vector<std::vector<Atom *>> split_biochemical_components(const T &cont) {
  return split_biochemical_components(cont.get_atoms());
}

/**
 * Quickly determine basic residue type of a residue. Only protein, nucleic or
 * notassigned are distinguished.
 *
 * @note a residue's type needed for quick decisions during parsing molecular
 * files
 *
 * @param resn residue name
 * @return residue type
 */
details::Rest determine_protein_nucleic(const std::string &resn);

} // namespace insilab::molib::algorithms
