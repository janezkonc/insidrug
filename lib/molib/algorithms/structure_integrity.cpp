#include "structure_integrity.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "molib/molecule.hpp"
#include <iostream>
#include <ranges>
#include <vector>

namespace insilab::molib::algorithms {

bool is_structure_ok(const std::vector<Residue *> &residues,
                     const double cutoff_bad) {
  helper::Benchmark<std::chrono::milliseconds> b;
  auto bad{0uz}, all(0uz);
  for (const auto &presidue : residues) {
    if (presidue->resn() == "HOH" ||
        !helper::standard_residues.contains(presidue->resn()))
      continue;
    const auto num_heavy_pdb =
        std::ranges::count_if(presidue->get_atoms(), [](const auto &patom) {
          return helper::heavy_atoms.contains(patom->atom_name());
        });
    auto num_heavy_standard{0u};
    for (const auto &[atom_name, idatm_type] :
         helper::standard_residues.at(presidue->resn())) {
      if (idatm_type != "H" && idatm_type != "HC" && atom_name != "OXT")
        ++num_heavy_standard;
    }
    if (num_heavy_pdb < num_heavy_standard) {
      ++bad;
      // std::cout << "BAD: " << presidue->resn() << " " << presidue->resi()
      //           << " has " << num_heavy_pdb << " should have "
      //           << num_heavy_standard << " atoms." << std::endl;
    }
    ++all;
  }

  const auto fract_bad = (static_cast<double>(bad) / all);

  std::clog << "Structural integrity test took " << b.duration() << " ms, "
            << bad << " bad residues found out of " << all << " ("
            << helper::dtos(fract_bad * 100, 1) << "%).\n";
  return fract_bad < cutoff_bad;
}

} // namespace insilab::molib::algorithms
