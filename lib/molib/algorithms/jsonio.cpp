#include "jsonio.hpp"
#include "fragmenter.hpp"
#include "molib/atom.hpp"

namespace insilab::molib::algorithms {

void to_json(nlohmann::json &fragment_j, const Fragment &fragment) {
  fragment_j = nlohmann::json{
      {"seed_id", fragment.get_seed_id()},
  };
  for (const auto &patom : fragment.get_core())
    fragment_j["core"].push_back(patom->atom_number());
  for (const auto &patom : fragment.get_join())
    fragment_j["join"].push_back(patom->atom_number());
}

} // namespace insilab::molib::algorithms
