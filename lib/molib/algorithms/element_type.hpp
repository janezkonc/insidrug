#pragma once

#include <string>

namespace insilab::molib::details {
enum class Rest : std::size_t;
}
namespace insilab::molib::algorithms {

/**
 * Try to guess the element based on standard atom names. This can be done
 * only for protein and nucleic acids.
 *
 * @return a better guess for element, if this could not be found, the
 * unchanged atom name
 */
std::string determine_element(const std::string &atom_name,
                              const details::Rest &rest);

} // namespace insilab::molib::algorithms