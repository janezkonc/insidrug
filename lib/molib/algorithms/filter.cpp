#include "filter.hpp"
#include "grid/grid.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/molecule.hpp"

namespace insilab::molib::algorithms {

struct Token {
  Key key;
  details::Rest rest;
  double number_d;
  int number_i;
  std::string str;
  std::optional<std::reference_wrapper<const grid::Grid<Atom>>> grid;
  std::optional<std::reference_wrapper<const Molecule>> molecule;
};

class FilterInternals {

  class Lexer {
    const ExprVec __filter_expr;
    int __i = 0;

  public:
    Lexer(const ExprVec &v) : __filter_expr(v) {}
    Token get_current_token();
    Token fetch_next_token();
  };

  const Atom::PSet __atoms;
  Atom::PSet __current_left;
  Lexer __lexer;
  std::optional<std::reference_wrapper<const grid::Grid<Atom>>> __grid;

  Atom::PSet __prim(bool bNextToken); // extract number, handle NOT,
                                      // handle parentheses '(' expr ')'
  Atom::PSet __term(bool bNextToken); // handles AND
  Atom::PSet __expr(bool bNextToken); // handles OR

public:
  FilterInternals(const Atom::PVec &atoms, const ExprVec &filter_expr)
      : __atoms(atoms.begin(), atoms.end()), __lexer(filter_expr) {}
  Atom::PVec filter();
};

Atom::PVec filter(const Atom::PVec &atoms, const ExprVec &filter_expr) {
  if (filter_expr.empty())
    return atoms;
  return FilterInternals(atoms, filter_expr).filter();
}

std::ostream &operator<<(std::ostream &o, const ExprVec &expr_v) {
  for (const auto &e : expr_v) {
    if (std::holds_alternative<Key>(e)) {
      o << std::to_underlying(std::get<Key>(e)) << " ";
    } else if (std::holds_alternative<std::string>(e)) {
      o << std::get<std::string>(e) << " ";
    }
  }
  return o;
}

Token FilterInternals::Lexer::get_current_token() {
  const auto &c = __filter_expr.at(__i);
  if (std::holds_alternative<Key>(c)) {
    return {
        .key = std::get<Key>(c),
    };
  } else if (std::holds_alternative<Residue::Type>(c)) {
    return {
        .key = Key::REST_V,
        .rest = std::get<Residue::Type>(c),
    };
  } else if (std::holds_alternative<double>(c)) {
    return {
        .key = Key::NUMBER_D,
        .number_d = std::get<double>(c),
    };
  } else if (std::holds_alternative<int>(c)) {
    return {
        .key = Key::NUMBER_I,
        .number_i = std::get<int>(c),
    };
  } else if (std::holds_alternative<std::reference_wrapper<const Atom::Grid>>(
                 c)) {
    return {
        .key = Key::GRID_V,
        .grid = std::get<std::reference_wrapper<const Atom::Grid>>(c),
    };
  } else if (std::holds_alternative<std::reference_wrapper<const Molecule>>(
                 c)) {
    return {
        .key = Key::CONTAINER_V,
        .molecule = std::get<std::reference_wrapper<const Molecule>>(c),
    };
  } else if (std::holds_alternative<std::string>(c)) {
    return {
        .key = Key::STRING,
        .str = std::get<std::string>(c),
    };
  }
  return {.key = Key::ERROR};
};

Token FilterInternals::Lexer::fetch_next_token() {
  if (__i == __filter_expr.size() - 1)
    return {.key = Key::END};
  ++__i;
  return get_current_token();
};

Atom::PSet FilterInternals::__prim(bool bNextToken) {
  if (bNextToken)
    this->__lexer.fetch_next_token();
  switch (this->__lexer.get_current_token().key) {

  case Key::AROUND: { // for example: (resn CYC around 5.0) will select atoms
                      // within 5.0 Angstroms of CYC but not CYC itself
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::NUMBER_D)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: in around expression, "
          "a distance in Angstroms (floating point number) is expected");
    const auto dist = this->__lexer.get_current_token().number_d;
    // negate current left
    Atom::PSet not_current_left;
    std::set_difference(
        __atoms.cbegin(), __atoms.cend(), __current_left.cbegin(),
        __current_left.cend(),
        std::inserter(not_current_left, not_current_left.cbegin()));

    Atom::PSet result;
    Atom::Grid g(not_current_left);
    for (const auto &patom : __current_left) {
      const auto neighbors = g.get_neighbors(patom->crd(), dist);
      result.insert(neighbors.cbegin(), neighbors.cend());
    }
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::ALL: {
    Atom::PSet result(__atoms);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::ATOM_NAME: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::STRING)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Name of atom "
          "as a std::string is expected");
    const auto str = this->__lexer.get_current_token().str;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->atom_name() == str)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::REST: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::REST_V)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Residue::Type "
          "is expected");
    const auto rest = this->__lexer.get_current_token().rest;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->residue().rest() & rest)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::CONTAINER: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::CONTAINER_V)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Molecule is expected");
    const auto &molecule =
        this->__lexer.get_current_token().molecule.value().get();
    const auto &atoms = molecule.get_atoms();
    Atom::PSet result{atoms.cbegin(), atoms.cend()};
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::GRID: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::GRID_V)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Atom::Grid "
          "is expected");
    __grid = this->__lexer.get_current_token().grid.value();
    this->__lexer.fetch_next_token(); // enables further processing
    return {}; // the real 'return' here is __grid, so just return empty set
  }

  case Key::RESN: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::STRING)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: residue name is "
          "expected");
    const auto resn = this->__lexer.get_current_token().str;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->residue().resn() == resn)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::RESI: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::NUMBER_I)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: residue index "
          "is expected");
    const auto resi = this->__lexer.get_current_token().number_i;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->residue().resi() == resi)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::ENTITY: { // cannot be used in chained 'filter' calls, since
                      // entity_id can change between them (filtered molecule's
                      // entity_id != original entity_id)
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::NUMBER_I)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Entity ID (a "
          "non-negative integral number) is expected");
    const auto entity_id = this->__lexer.get_current_token().number_i;
    const auto &components = algorithms::split_biochemical_components(
        Atom::PVec(std::begin(__atoms), std::end(__atoms)));
    if (entity_id >= components.size())
      throw helper::Error(
          "[WHOOPS] The provided entity_id is larger than the "
          "number of entities that are in molecule (check filter expression)!");
    Atom::PSet result;
    for (const auto &patom : components[entity_id])
      result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::SEGI: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::STRING)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: segment id is "
          "expected");
    const auto seg_id = this->__lexer.get_current_token().str;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->segment().seg_id() == seg_id)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::CHAIN: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::STRING)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: chain id is "
          "expected");
    const auto chain_id = this->__lexer.get_current_token().str;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->chain().chain_id() == chain_id)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::MODEL: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::NUMBER_I)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Model ID (a "
          "positive integral number) is expected");
    const auto model_id = this->__lexer.get_current_token().number_i;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->model().model_id() == model_id)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::MOLECULE_NAME: {
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::STRING)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: Molecule name (a "
          "string) is expected");
    const auto molecule_name = this->__lexer.get_current_token().str;
    Atom::PSet result;
    for (const auto &patom : __atoms)
      if (patom->molecule().name() == molecule_name)
        result.insert(patom);
    this->__lexer.fetch_next_token(); // enables further processing
    return result;
  }

  case Key::NOT: {
    Atom::PSet result;
    const auto right = this->__prim(true);
    std::set_difference(__atoms.cbegin(), __atoms.cend(), right.cbegin(),
                        right.cend(), std::inserter(result, result.cbegin()));
    return result;
  }

  case Key::BYRES: {
    Atom::PSet result;
    const auto right = this->__expr(true);
    for (const auto &patom : right) {
      for (auto &atom : patom->residue()) {
        result.insert(&atom);
      }
    }
    return result;
  }

  case Key::P_OPEN: {
    const auto result = this->__expr(true);
    if (this->__lexer.get_current_token().key != Key::P_CLOSE) {
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression:\')\' is expected");
    }
    // eat up closing parenthesis )
    this->__lexer.fetch_next_token();
    return result;
  }

  default: {
    throw helper::Error("[WHOOPS] Syntax error in filter expression");
    return {};
  }
  }
}

Atom::PSet FilterInternals::__term(bool bNextToken) {
  Atom::PSet left = this->__prim(bNextToken);
  switch (this->__lexer.get_current_token().key) {

  case Key::AND: {
    Atom::PSet result;
    const auto right = this->__term(true);
    std::set_intersection(left.cbegin(), left.cend(), right.cbegin(),
                          right.cend(), std::inserter(result, result.cbegin()));
    return result;
  }

  case Key::WITHIN: { // for example: (protein within 5.0 of CYC) will select
                      // protein atoms within 5.0 Angstroms of CYC
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::NUMBER_D)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: in within...of  a "
          "distance in Angstroms (floating point number) is expected");
    const auto dist = this->__lexer.get_current_token().number_d;
    this->__lexer.fetch_next_token();
    if (this->__lexer.get_current_token().key != Key::OF)
      throw helper::Error(
          "[WHOOPS] Syntax error in filter expression: in within...of "
          "Key::OF is expected");
    const auto right = this->__term(true);
    Atom::PSet result;
    // if next term was GRID, use the provided Grid instead of making new
    if (__grid.has_value()) {
      const Atom::Grid &g = __grid.value().get();
      for (const auto &patom : left) {
        if (!g.has_neighbor_within(patom->crd(), dist))
          continue;
        result.insert(patom);
      }
      __grid.reset();
    } else {
      const Atom::Grid g(right);
      for (const auto &patom : left) {
        if (!g.has_neighbor_within(patom->crd(), dist))
          continue;
        result.insert(patom);
      }
    }
    return result;
  }

  default:
    return left;
  }
}

Atom::PSet FilterInternals::__expr(bool bNextToken) {
  Atom::PSet left = this->__term(bNextToken);
  __current_left = left;
  switch (this->__lexer.get_current_token().key) {

  case Key::AROUND: // this is a postfix operator, so special care is taken
    return this->__term(false);

  case Key::OR: {
    Atom::PSet result;
    const auto right = this->__expr(true);
    std::set_union(left.cbegin(), left.cend(), right.cbegin(), right.cend(),
                   std::inserter(result, result.cbegin()));
    return result;
  }

  default:
    return left;
  }
}

Atom::PVec FilterInternals::filter() {
  const auto atoms = this->__expr(false);
  return Atom::PVec(atoms.cbegin(), atoms.cend());
}

} // namespace insilab::molib::algorithms
