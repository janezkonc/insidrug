#pragma once

#include "molib/details/common.hpp"
#include <string>
#include <vector>

namespace insilab::molib {
class Residue;
} // namespace insilab::molib

namespace insilab::molib::algorithms {

/**
 * Check if PDB structure (proteins and nucleic acids) are complete, i.e., they
 * are not missing too many atoms like in CA or P models.
 *
 * @param residues a vector of pointer to residues
 * @param cutoff_bad tolerable percentage of incomplete residues in the PDB
 * @return true, if structure is OK (passed the test), false otherwise
 */
bool is_structure_ok(const std::vector<Residue *> &residues,
                     const double cutoff_bad = 0.2);

/**
 * Check if PDB structure (proteins and nucleic acids) are complete, i.e., they
 * are not missing too many atoms like in CA or P models.
 *
 * @param cont any molib container supporting get_residues function
 * @param cutoff_bad tolerable percentage of incomplete residues in the PDB
 * @return true, if structure is OK (passed the test), false otherwise
 */
template <details::HasGetResidues T>
bool is_structure_ok(const T &cont, const double cutoff_bad = 0.2) {
  return is_structure_ok(cont.get_residues());
}

} // namespace insilab::molib::algorithms
