#include "residue_type.hpp"
#include "internals/residue_type_impl.hpp"
#include "molib/atom.hpp"
#include "molib/residue.hpp"

namespace insilab::molib::algorithms {

bool is_cofactor(const Atom::PVec &atoms,
                 const double moltype_tanimoto_cutoff) {
  return internals::is_cofactor(atoms, moltype_tanimoto_cutoff);
}

bool is_sugar(const Atom::PVec &atoms, const double moltype_tanimoto_cutoff) {
  return internals::is_sugar(atoms, moltype_tanimoto_cutoff);
}

void compute_cofactors(const Residue::PVec &residues) {
  internals::run_steps_for("cofactors", residues);
}

void compute_buffer(const Residue::PVec &residues) {
  internals::run_steps_for("buffer", residues);
}

void compute_glycans(const Residue::PVec &residues) {
  internals::run_steps_for("glycans", residues);
}

void compute_protein_nucleic_water_ion(const Residue::PVec &residues) {
  internals::run_steps_for("pnwi", residues);
}

void compute_nonstandard_polymer(const Residue::PVec &residues) {
  internals::run_steps_for("nonstandard", residues);
}

void compute_peptides(const Residue::PVec &residues, const unsigned small) {
  internals::run_steps_for("peptides", residues, small);
}

void compute_compounds(const Residue::PVec &residues) {
  internals::run_steps_for("compounds", residues);
}

void compute_covalent(const Residue::PVec &residues) {
  internals::run_steps_for("covalent", residues);
}

std::vector<Atom::PVec>
split_biochemical_components(const Atom::PVec &all_atoms) {
  return internals::split_biochemical_components(all_atoms);
}

Residue::Type determine_protein_nucleic(const std::string &resn) {
  return internals::determine_protein_nucleic(resn);
}

} // namespace insilab::molib::algorithms
