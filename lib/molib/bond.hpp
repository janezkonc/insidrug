#pragma once

#include "helper/it.hpp"
#include "jsonio.hpp" // gives Json powers
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {

class Atom;
class Bond;

class Bond : public helper::template_vector_container<Bond> {
  Atom *__atom1, *__atom2;
  int __idx1, __idx2;
  std::string __rotatable;
  std::string __bond_gaff_type;
  std::string __sybyl_type;
  int __bo;
  bool __ring;
  std::set<int> __angles;
  int __drive_id;

public:
  using PVec = std::vector<Bond *>;
  using CPVec = std::vector<const Bond *>;
  using PSet = std::set<Bond *>;
  using CPSet = std::set<const Bond *>;
  using Graph = std::vector<std::unique_ptr<Bond>>;

  Bond()
      : __atom1(nullptr), __atom2(nullptr), __idx1(0), __idx2(0), __ring(false),
        __bo(0), __drive_id(0), __angles(std::set<int>()), __rotatable(""),
        __bond_gaff_type(""), __sybyl_type("") {}
  Bond(Atom *atom1, Atom *atom2)
      : __atom1(atom1), __atom2(atom2), __idx1(0), __idx2(0), __ring(false),
        __bo(0), __angles(std::set<int>()), __drive_id(0), __rotatable(""),
        __bond_gaff_type(""), __sybyl_type("") {}
  Bond(Atom *atom1, Atom *atom2, int idx1, int idx2)
      : __atom1(atom1), __atom2(atom2), __idx1(idx1), __idx2(idx2),
        __ring(false), __bo(0), __angles(std::set<int>()), __drive_id(0),
        __rotatable(""), __bond_gaff_type(""), __sybyl_type("") {}
  void shallow_copy(const Bond &rhs);
  bool is_set() const { return __atom1 != nullptr; }
  bool is_adjacent(const Bond &other) const;
  void set_angle(int angle) { __angles.insert(angle); }
  void set_drive_id(int drive_id) { __drive_id = drive_id; }
  void set_rotatable(const std::string &rotatable) { __rotatable = rotatable; }
  const std::string &get_rotatable() const { return __rotatable; }
  void set_ring(bool ring) { __ring = ring; }
  bool is_ring() const { return __ring; }
  bool is_rotatable() const {
    return !__rotatable.empty() && __rotatable != "amide";
  }
  Bond &set_bond_gaff_type(const std::string &bond_gaff_type) {
    __bond_gaff_type = bond_gaff_type;
    return *this;
  }
  Bond &set_sybyl_type(const std::string &sybyl_type) {
    __sybyl_type = sybyl_type;
    return *this;
  }
  Bond &set_bo(const int &bo) {
    __bo = bo;
    return *this;
  }
  const std::string &get_bond_gaff_type() const { return __bond_gaff_type; }
  const std::string &get_sybyl_type() const { return __sybyl_type; }
  int get_bo() const { return __bo; }
  bool is_single() const { return __bo == 1; }
  bool is_double() const { return __bo == 2; }
  bool is_triple() const { return __bo == 3; }
  bool is_aromatic() const {
    return __bond_gaff_type == "AB" || __bond_gaff_type == "SB" ||
           __bond_gaff_type == "DB";
  }
  Atom &first_atom(const Atom &origin) const {
    return (&origin == __atom1 ? *__atom1 : *__atom2);
  }
  Atom &second_atom(const Atom &origin) const {
    return (&origin == __atom1 ? *__atom2 : *__atom1);
  }
  Atom &atom1() const { return *__atom1; }
  Atom &atom2() const { return *__atom2; }
  int idx1() const { return __idx1; }
  int idx2() const { return __idx2; }
  double length() const;
  static void erase_stale_refs(const Bond &deleted_bond,
                               const Bond::PVec &bonds);

  static void connect_bonds(const Bond::PSet &bonds);
  static void erase_bonds(const Bond::PSet &bonds);

  // Factory for creating bonds
  static auto create(Atom &a1, Atom &a2) {
    return std::make_shared<Bond>(&a1, &a2);
  }

  friend std::ostream &operator<<(std::ostream &stream, const Bond &b);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const Bond::PVec &bonds);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const Bond::PSet &bonds);
};

} // namespace insilab::molib
