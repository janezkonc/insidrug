#pragma once

#include "atom.hpp"
#include "details/rest.hpp"
#include "details/utils.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "helper/it.hpp"

namespace insilab::molib {

class Segment;
class Chain;

class Residue : public helper::template_map_container<Atom, Residue, int>,
                public details::Utils<Residue> {
public:
  using Type = details::Rest;
  using Id = std::tuple<std::string, std::string, int, char>;
  using Pair = std::pair<int, char>;
  using PVec = std::vector<Residue *>;
  using CPVec = std::vector<const Residue *>;
  using PSet = std::set<Residue *>;
  using CPSet = std::set<const Residue *>;

  struct Params {
    std::string resn{"XXX"};
    int resi{1};
    char ins_code{' '};
    Type rest{Type::notassigned};
  };

private:
  Params __params;

public:
  Residue() = default;
  Residue(const Params &p) : __params(p) {}

  const Params &params() const { return __params; }

  Atom &add(Atom a) {
    const auto i = a.atom_number();
    return this->insert(i, std::move(a), this);
  }
  const std::string &resn() const { return __params.resn; }
  Residue &set_resn(const std::string &resn) {
    __params.resn = resn;
    return *this;
  }
  int resi() const { return __params.resi; }
  char ins_code() const { return __params.ins_code; }
  Type rest() const { return __params.rest; }
  Residue &set_rest(const std::string &);
  Residue &set_rest(const Residue::Type rest);
  Atom &atom(int p) const { return this->element(p); }
  bool has_atom(int p) { return this->has_element(p); }
  bool operator==(const Residue &right) const {
    return (this->__params.resn == right.resn() &&
            this->__params.resi == right.resi() &&
            this->__params.ins_code == right.ins_code());
  }

  bool operator!=(const Residue &right) const { return !(*this == right); }

  friend std::ostream &operator<<(std::ostream &stream, const Residue &r);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const Residue::PVec &rvec);
};

} // namespace insilab::molib
