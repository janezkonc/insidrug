#include "utility.hpp"
#include "molib/atom.hpp"
#include "molib/bond.hpp"
#include "molib/residue.hpp"
#include <cassert>
#include <map>
#include <regex>
#include <vector>

namespace insilab::molib::details {

bool compare_atom_pseudoatom_by_idatm_prop(const Atom &atom1,
                                           const Atom &atom2) {
  if (!std::regex_search(atom1.idatm_type_unmask(),
                         std::regex(atom2.get_smiles_label())))
    return false;
  for (const auto &[p, cnt] : atom2.get_smiles_prop()) {
    const auto atom1_cnt = atom1.compute_num_property(p);
    if (!((cnt == -1 && atom1_cnt > 0) || cnt == atom1_cnt))
      return false;
  }
  return true;
}

bool compare_bond_pseudobond_by_gaff_bond_order(const Bond &bond1,
                                                const Bond &bond2) {
  return (bond2.get_bond_gaff_type().empty() ||
          bond2.get_bond_gaff_type() == bond1.get_bond_gaff_type()) &&
         (bond2.get_bo() == 0 || bond2.get_bo() == bond1.get_bo());
}

/**
 * Set the parameters of an atom based on a string rule.
 *
 * @param atom Atom whose parameters we wish to set (modifies Atom)
 * @param str rule - a string that defines which property to set
 */
void set_members(Atom &atom, const std::string &rule) {
  std::smatch m;
  // set atomic penalty scores
  if (std::regex_search(rule, m, std::regex(R"(aps=(\S+))"))) {
    if (m[1].matched) {
      const auto &aps_str = m[1].str();
      std::match_results<std::string::const_iterator> matches;
      std::string::const_iterator begin = aps_str.begin(), end = aps_str.end();
      std::map<int, int> result;
      while (std::regex_search(begin, end, matches,
                               std::regex(R"(\{(\d+,\d+)\})"))) {
        std::string s(matches[1].first, matches[1].second);
        const auto vec = helper::split(s, ",");
        assert(vec.size() == 2);
        const int val = std::stoi(vec[0]);
        const int aps = std::stoi(vec[1]);
        result[val] = aps;
        begin = matches[1].second;
      }
      atom.set_aps(result);
    }
  }

  // set smiles properties
  if (std::regex_search(rule, m, std::regex(R"(prop=(\S+))"))) {
    if (m[1].matched) {
      const auto &prop_str = m[1].str();
      std::match_results<std::string::const_iterator> matches;
      std::string::const_iterator begin = prop_str.begin(),
                                  end = prop_str.end();
      std::map<std::string, int> result;
      while (std::regex_search(begin, end, matches,
                               std::regex(R"(\{(\w+,\d+)\})"))) {
        const std::string s(matches[1].first, matches[1].second);
        const auto vec = helper::split(s, ",");
        assert(vec.size() == 2);
        const std::string val = vec[0];
        const int count = std::stoi(vec[1]);
        result[val] = count;
        begin = matches[1].second;
      }
      atom.set_smiles_prop(result);
    }
  }
  // set sybyl type
  if (std::regex_search(rule, m, std::regex(R"(sybyl=(\S+))"))) {
    if (m[1].matched) {
      atom.set_sybyl_type(m[1].str());
    }
  }
  // set gaff type
  if (std::regex_search(rule, m, std::regex(R"(gaff=(\S+))"))) {
    if (m[1].matched) {
      atom.set_gaff_type(m[1].str());
    }
  }
  // set idatm type
  if (std::regex_search(rule, m, std::regex(R"(idatm=(\S+))"))) {
    if (m[1].matched) {
      atom.set_idatm_type(m[1].str());
    }
  }
  // set moltype type
  if (std::regex_search(rule, m, std::regex(R"(rest=(\S+))"))) {
    if (m[1].matched) {
      atom.residue().set_rest(m[1].str());
    }
  }
}

void apply_rule_atom(
    const std::vector<std::pair<Atom::PVec, std::string>> &assigned_rules) {
  for (const auto &[matched_atoms, rule] : assigned_rules) {
    assert(matched_atoms.size() == 1);
    auto patom = matched_atoms.at(0);
    // no need to worry that the same atom will be set twice (matched_atoms
    // appear only once in assigned_rule, see search_replace)!
    set_members(*patom, rule);
  }
}

/**
 * Set the parameters of a bond based on a string rule.
 *
 * @param bond Bond whose parameters we wish to set (modifies Bond)
 * @param str rule - a string that defines which property to set
 */
void set_members(Bond &bond, const std::string &rule) {
  // set angles
  std::match_results<std::string::const_iterator> matches;
  auto i = rule.find("angles");
  if (i != std::string::npos) {
    std::string::const_iterator begin = rule.begin() + i, end = rule.end();
    while (
        std::regex_search(begin, end, matches, std::regex("([-]*\\d+)[,}]"))) {
      const std::string s(matches[1].first, matches[1].second);
      const int angle = std::stoi(s);
      bond.set_angle(angle);
      begin = matches[1].second;
      if (*begin == '}')
        break;
    }
  }
  // set rotatable type
  std::smatch m;
  if (std::regex_search(rule, m, std::regex("rota=([^,$]+)"))) {
    if (m[1].matched) {
      bond.set_rotatable(m[1].str());
    }
  }
  // set drive_id
  if (std::regex_search(rule, m, std::regex("drive_id=(\\w+)"))) {
    if (m[1].matched) {
      bond.set_drive_id(std::stoi(m[1].str()));
    }
  }
  // set gaff bond type
  if (std::regex_search(rule, m, std::regex("bond_gaff_type=(\\w+)"))) {
    if (m[1].matched) {
      bond.set_bond_gaff_type(m[1].str());
    }
  }
  // set bond order
  if (std::regex_search(rule, m, std::regex("bo=(\\w+)"))) {
    if (m[1].matched) {
      bond.set_bo(std::stoi(m[1].str()));
    }
  }
}

void apply_rule_bond(
    const std::vector<std::pair<Atom::PVec, std::string>> &assigned_rules) {
  for (const auto &[matched_atoms, rule] : assigned_rules) {
    assert(matched_atoms.size() == 2);
    const auto &atom1 = *matched_atoms.at(0);
    const auto &atom2 = *matched_atoms.at(1);
    auto &bond12 = atom1.get_bond(atom2);
    // no need to worry that the same bond will be set twice (matched_atoms
    // appear only once in assigned_rule, see search_replace)!
    set_members(bond12, rule);
  }
}

std::pair<std::vector<std::unique_ptr<Bond>>,
          std::vector<std::unique_ptr<Atom>>>
create_bonds(const helper::smiles &edges) {

  const auto decode_smiles_prop = [](const std::vector<std::string> &s) {
    std::map<std::string, int> smiles_prop;
    if (s.size() > 2) {
      const auto bonds = helper::split(s.at(2), ",", true);
      if (bonds.size() > 0) {
        smiles_prop["B"] = std::stoi(bonds.at(0));
      }
      if (bonds.size() == 2) {
        smiles_prop["H"] = std::stoi(bonds.at(1).substr(0, 1)); // e.g. 3H
      }
    }
    if (s.size() > 3) {
      const auto vec = helper::split(s.at(3), ",", true);
      for (const auto &prop : vec) {
        const auto cnt =
            std::isdigit(prop.at(0)) ? std::stoi(prop.substr(0, 1)) : -1;
        const auto nm = std::isdigit(prop.at(0)) ? prop.substr(1) : prop;
        smiles_prop[nm] = cnt;
      }
    }
    return smiles_prop;
  };

  std::map<int, int> added;
  std::vector<std::tuple<int, std::string, std::map<std::string, int>>> atoms;
  std::vector<std::unique_ptr<Bond>> bonds;
  std::vector<std::unique_ptr<Atom>> atoms_keep_alive;
  for (const auto &e : edges) {
    const auto s1 = helper::split(e.atom_property1, "#", true);
    const auto s2 = helper::split(e.atom_property2, "#", true);
    assert(s1.size() >= 2);
    assert(s2.size() >= 2);
    const auto smiles_label1 = s1.at(0);
    const auto smiles_label2 = s2.at(0);
    const auto idx1 = std::stoi(s1.at(1));
    const auto idx2 = std::stoi(s2.at(1));
    if (!added.contains(idx1)) {
      added[idx1] = atoms.size();
      const auto smiles_prop1 = decode_smiles_prop(s1);
      atoms.push_back({added[idx1] + 1, smiles_label1, smiles_prop1});
      dbgmsg("idx1 = " << idx1 << " added[idx1] = " << added[idx1]);
    }
    if (!added.contains(idx2)) {
      added[idx2] = atoms.size();
      const auto smiles_prop2 = decode_smiles_prop(s2);
      atoms.push_back({added[idx2] + 1, smiles_label2, smiles_prop2});
      dbgmsg("idx2 = " << idx2 << " added[idx2] = " << added[idx2]);
    }
    const auto &[added_atom_number1, added_smiles_label1, added_smiles_prop1] =
        atoms[added[idx1]];
    const auto &[added_atom_number2, added_smiles_label2, added_smiles_prop2] =
        atoms[added[idx2]];
    // here bond "owns" atoms and must delete them in destructor
    atoms_keep_alive.push_back(std::make_unique<Atom>(
        Atom::Params{.atom_number = added_atom_number1,
                     .smiles_label = added_smiles_label1,
                     .smiles_prop = added_smiles_prop1}));
    atoms_keep_alive.push_back(std::make_unique<Atom>(
        Atom::Params{.atom_number = added_atom_number2,
                     .smiles_label = added_smiles_label2,
                     .smiles_prop = added_smiles_prop2}));
    bonds.push_back(std::make_unique<Bond>(
        &*atoms_keep_alive.at(atoms_keep_alive.size() - 2),
        &*atoms_keep_alive.at(atoms_keep_alive.size() - 1)));
    set_members(*bonds.back(), e.bond_property);
  }
  for (auto i{0uz}; i < bonds.size(); ++i) {
    Bond &bond1 = *bonds[i];
    for (auto j = i + 1; j < bonds.size(); ++j) {
      Bond &bond2 = *bonds[j];
      if (bond1.is_adjacent(bond2)) {
        bond1.add(&bond2);
        bond2.add(&bond1);
      }
    }
  }
  return {std::move(bonds), std::move(atoms_keep_alive)};
}

} // namespace insilab::molib::details
