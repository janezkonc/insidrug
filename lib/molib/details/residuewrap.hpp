#pragma once

#include "helper/it.hpp"
#include <functional>
#include <memory>
#include <tuple>
#include <vector>

namespace insilab::molib {
class Residue;
}

namespace insilab::molib::details {

class ResidueWrap : public helper::template_vector_container<ResidueWrap> {
  const Residue &__residue;

public:
  using Graph = std::vector<std::unique_ptr<ResidueWrap>>;

  ResidueWrap(const Residue &residue) : __residue(residue) {}
  const Residue &get_residue() const { return __residue; }
  static Graph create_graph(const std::vector<Residue *> &residues);
};

} // namespace insilab::molib::details
