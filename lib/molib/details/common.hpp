#pragma once

#include <concepts>
#include <set>
#include <vector>

namespace insilab::geom3d {
class Coordinate;
} // namespace insilab::geom3d

namespace insilab::molib {

class Molecules;
class Molecule;
class Assembly;
class Model;
class Chain;
class Segment;
class Residue;
class Atom;
class Bond;

} // namespace insilab::molib

namespace insilab::molib::details {

template <class T>
concept HasGetResidues = requires(const T &t) {
  { t.get_residues() } -> std::same_as<std::vector<Residue *>>;
};

template <class T>
concept HasGetAtoms = requires(const T &t) {
  { t.get_atoms() } -> std::same_as<std::vector<Atom *>>;
};

template <class T>
concept HasGetCrds = requires(const T &t) {
  { t.get_crds() } -> std::same_as<std::vector<geom3d::Coordinate>>;
};

template <class T>
concept IsAtomVecOrSet = std::is_same_v<T, std::vector<Atom *>> ||
                         std::is_same_v<T, std::set<Atom *>>;

template <class T>
concept IsBondVecOrSet = std::is_same_v<T, std::vector<Bond *>> ||
                         std::is_same_v<T, std::set<Bond *>>;

template <class T>
concept MoleculePlus =
    std::is_same_v<T, Molecule> || std::is_same_v<T, Molecules>;

template <class T>
concept MoleculeMinus =
    std::is_same_v<T, Atom> || std::is_same_v<T, Residue> ||
    std::is_same_v<T, Segment> || std::is_same_v<T, Chain> ||
    std::is_same_v<T, Model> || std::is_same_v<T, Assembly> ||
    std::is_same_v<T, Molecule>;

template <class T>
concept AssemblyPlus = MoleculePlus<T> || std::is_same_v<T, Assembly>;

template <class T>
concept AssemblyMinus =
    std::is_same_v<T, Atom> || std::is_same_v<T, Residue> ||
    std::is_same_v<T, Segment> || std::is_same_v<T, Chain> ||
    std::is_same_v<T, Model> || std::is_same_v<T, Assembly>;

template <class T>
concept ModelPlus = AssemblyPlus<T> || std::is_same_v<T, Model>;

template <class T>
concept ModelMinus = std::is_same_v<T, Atom> || std::is_same_v<T, Residue> ||
                     std::is_same_v<T, Segment> || std::is_same_v<T, Chain> ||
                     std::is_same_v<T, Model>;

template <class T>
concept ChainPlus = ModelPlus<T> || std::is_same_v<T, Chain>;

template <class T>
concept ChainMinus = std::is_same_v<T, Atom> || std::is_same_v<T, Residue> ||
                     std::is_same_v<T, Segment> || std::is_same_v<T, Chain>;

template <class T>
concept SegmentPlus = ChainPlus<T> || std::is_same_v<T, Segment>;

template <class T>
concept SegmentMinus = std::is_same_v<T, Atom> || std::is_same_v<T, Residue> ||
                       std::is_same_v<T, Segment>;

template <class T>
concept ResiduePlus = SegmentPlus<T> || std::is_same_v<T, Residue>;

template <class T>
concept ResidueMinus = std::is_same_v<T, Atom> || std::is_same_v<T, Residue>;

template <class T>
concept AtomPlus = ResiduePlus<T> || std::is_same_v<T, Atom>;

template <typename T> struct FromType {};
template <> struct FromType<Atom> {
  typedef Residue *type;
};
template <> struct FromType<Residue> {
  typedef Segment *type;
};
template <> struct FromType<Segment> {
  typedef Chain *type;
};
template <> struct FromType<Chain> {
  typedef Model *type;
};
template <> struct FromType<Model> {
  typedef Assembly *type;
};
template <> struct FromType<Assembly> {
  typedef Molecule *type;
};
template <> struct FromType<Molecule> {
  typedef Molecules *type;
};
template <> struct FromType<Molecules> {
  typedef void *type;
};

} // namespace insilab::molib::details
