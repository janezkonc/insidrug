#include "residuewrap.hpp"
#include "helper/benchmark.hpp"
#include "helper/help.hpp"
#include "molib/residue.hpp"

namespace insilab::molib::details {

ResidueWrap::Graph ResidueWrap::create_graph(const Residue::PVec &residues) {
  dbgmsg("Create residue graph ...");
  std::vector<std::unique_ptr<ResidueWrap>> vertices;
  for (auto &r : residues) { // add vertices to graph
    vertices.push_back(std::unique_ptr<ResidueWrap>(new ResidueWrap(*r)));
  }
  for (int i = 0; i < vertices.size(); ++i) { // ... edges ...
    for (int j = i + 1; j < vertices.size(); ++j) {
      const Residue &res_i = vertices[i]->get_residue();
      const Residue &res_j = vertices[j]->get_residue();
      if (res_i.is_adjacent(res_j)) {

        dbgmsg("adjacent residues i = " << i << " j = " << j << " " << res_i
                                        << " " << res_j);

        ResidueWrap &rwrap1 = *vertices[i];
        ResidueWrap &rwrap2 = *vertices[j];
        rwrap1.add(&rwrap2);
        rwrap2.add(&rwrap1);
      }
    }
  }
  return vertices;
}

} // namespace insilab::molib::details
