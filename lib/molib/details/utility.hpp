#pragma once

#include "common.hpp"
#include "helper/help.hpp"
#include "molib/algorithms/filter.hpp"
#include "molib/atom.hpp"
#include "molib/residue.hpp"
#include "rest.hpp"
#include <memory>
#include <queue>
#include <set>
#include <vector>

namespace insilab::molib {
class Bond;
class Atom;
} // namespace insilab::molib

namespace insilab::molib::details {

/// Default comparison function to compare two atoms. The second one is a
/// pseudo atom generated from rename rules and the first is a real atom
bool compare_atom_pseudoatom_by_idatm_prop(const Atom &atom1,
                                           const Atom &atom2);

/// Default comparison function to compare two bonds. The second one is a
/// "smiles" bond (pseudo bond generated using rename_rules) and the first
/// one is a "real" bond in a molecule.
bool compare_bond_pseudobond_by_gaff_bond_order(const Bond &bond1,
                                                const Bond &bond2);

/// Default function that applies the rule to the matched atom (assigned_rules
/// contain only one (the first) rule for each atom).
void apply_rule_atom(
    const std::vector<std::pair<std::vector<Atom *>, std::string>>
        &assigned_rules);

/// Default function that applies the rule to the matched bond (assigned_rules
/// contain only one (the first) rule for each bond)
void apply_rule_bond(
    const std::vector<std::pair<std::vector<Atom *>, std::string>>
        &assigned_rules);

/**
 * Get atoms in a set of bonds.
 *
 * @param bonds a vector (or set) of pointers to bonds
 * @return a vector of pointers to atoms that are included in bonds
 */
template <IsBondVecOrSet T> auto get_atoms_from(const T &bonds) {
  std::set<Atom *> atoms_s;
  for (const auto &pbond : bonds) {
    atoms_s.insert(&pbond->atom1());
    atoms_s.insert(&pbond->atom2());
  }
  return std::vector<Atom *>(std::begin(atoms_s), std::end(atoms_s));
}

/// For template parameter for get_bonds function
enum class get_bonds_t {
  all,
  in,
  out,
  all_as_set,
  in_as_set,
  out_as_set,
};

/**
 * Get bonds in a set of atoms.
 *
 * @note different selections can be made: all bonds, bonds that are within
 * atoms, or bonds that stick out
 *
 * @tparam btype get_bonds_t::all (default) - at least one of the pair of atoms
 * that form a bond is in atoms vector, get_bonds_t::in - bond's first and
 * second atoms are both in atoms, get_bonds_t::out - one of the bond's atoms is
 * not in atoms
 *
 * @param atoms a vector (or set) of pointers to atoms
 * @return a vector (a set, if '_as_set' version of btype is used) of pointers
 * to bonds
 */
template <get_bonds_t btype = get_bonds_t::all, IsAtomVecOrSet T>
auto get_bonds_from(const T &atoms) {

  const auto lambda_get_bonds = [](const std::set<Atom *> &atoms_s) {
    std::set<Bond *> bonds;
    for (auto &patom1 : atoms_s) {
      for (const auto &pbond : patom1->get_bonds()) {
        auto &atom2 = pbond->second_atom(*patom1);
        if constexpr (btype == get_bonds_t::in ||
                      btype == get_bonds_t::in_as_set) {
          if (atoms_s.contains(&atom2))
            bonds.insert(pbond);
        } else if constexpr (btype == get_bonds_t::out ||
                             btype == get_bonds_t::out_as_set) {
          if (!atoms_s.contains(&atom2))
            bonds.insert(pbond);
        } else {
          bonds.insert(pbond);
        }
      }
    }
    if constexpr (btype == get_bonds_t::in || btype == get_bonds_t::out ||
                  btype == get_bonds_t::all)
      return std::vector<Bond *>(std::begin(bonds), std::end(bonds));
    else
      return bonds;
  };

  if constexpr (std::is_same_v<T, std::vector<Atom *>>) {
    const std::set<Atom *> atoms_s(std::begin(atoms), std::end(atoms));
    return lambda_get_bonds(atoms_s);
  } else
    return lambda_get_bonds(atoms);
}

/**
 * Get bonds in a molecule.
 *
 * @note different selections can be made: all bonds, bonds that are within
 * atoms, or bonds that stick out
 *
 * @tparam btype get_bonds_t::all (default) - at least one of the pair of atoms
 * that form a bond is in atoms vector, get_bonds_t::in - bond's first and
 * second atoms are both in atoms, get_bonds_t::out - one of the bond's atoms is
 * not in atoms
 *
 * @param cont any molib container that has get_atoms function
 * @return a vector of pointers to bonds  (if '_as_set' btype template parameter
 * is used, return set instead)
 */
template <get_bonds_t btype = get_bonds_t::all, HasGetAtoms T>
auto get_bonds_from(const T &cont) {
  return get_bonds_from<btype>(cont.get_atoms());
}

/**
 * Generate a graph-like bond structure from Smiles-like (chemical smiles
 * codes) rules so that this structure can be compared (searched for) in a
 * molecule.
 *
 * @param edges a smiles-like structure defining the graph to be made
 * @return a vector of unique ptrs to generated Bonds (owning container), each
 * bond can be iterated-over to get its neighbor bonds
 */
std::pair<std::vector<std::unique_ptr<Bond>>,
          std::vector<std::unique_ptr<Atom>>>
create_bonds(const helper::smiles &edges);

/// Comparison functions for filter_atoms
const auto comp_ns = [](const auto &patom) {
  return !helper::standard_residues.contains(patom->residue().resn());
};
const auto comp_nsi = [](const auto &patom) {
  return !helper::standard_residues.contains(patom->residue().resn()) &&
         !details::is_ion(patom->residue().rest());
};
const auto comp_i = [](const auto &patom) {
  return details::is_ion(patom->residue().rest());
};

/**
 * Filter atoms according to provided comparison function and (optionally)
 * extend the resulting atoms with atoms that are bonded to them.
 *
 * @note e.g., if atoms represent residue HEM, this HEM is bound to histidine's
 * N atom, so also this N needs to be added for the HEM's atom to be correctly
 * typed (with algorithms::replace_substructure)
 *
 * @param atoms vector of pointers to atoms
 * @param comp a comparison function (unary predicate)
 * @param extend extend filtered atoms with those that are connected to them
 * @return vector of atoms that satisfy the comp predicate, plus (optionally)
 * atoms that are bonded to them
 */
auto filter_atoms(const Atom::PVec &atoms, const auto &comp,
                  const bool extend = false) {
  Atom::PVec result;
  std::ranges::copy_if(atoms, std::back_inserter(result), comp);
  if (!extend)
    return result;
  const auto bonds = details::get_bonds_from(result);
  const auto result_extended = details::get_atoms_from(bonds);
  return result_extended;
}

/**
 * Convert chain identifier(s) given to an expression vector that can be used
 * with the filter algorithm.
 *
 * @note this is a convenience function to automate a common operation
 *
 * @param chains a vector, in which each element is a string representing a
 * Chain ID OR a string of Chain ID(s) with no spaces between them e.g., ABC
 * @return an ExprVec that enables selection of the chains with the given Chain
 * IDs (there is an OR between Chain IDs) when given as argument to the filter
 * algorithm
 */
auto chains_to_exprvec(const auto &chains) {
  using namespace algorithms;
  auto result = ExprVec{};
  for (const auto &c : chains) {
    result.insert(result.end(), {Key::CHAIN, helper::to_string(c), Key::OR});
  }
  if (!result.empty())
    result.pop_back();
  return result;
}

} // namespace insilab::molib::details
