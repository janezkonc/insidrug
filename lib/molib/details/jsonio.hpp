#pragma once

#include "nlohmann/json.hpp"

/*********************************************************************************
 * Helper functions to_json and from_json are required by nlohmann's Json
 * library and enable to automatically convert an object to/from json. More
 * information at: https://github.com/nlohmann/json#arbitrary-types-conversions
 *********************************************************************************/

namespace insilab::molib::details {

enum class Rest : std::size_t;

void from_json(const nlohmann::json &rest_j, Rest &rest);
void to_json(nlohmann::json &rest_j, const Rest &rest);

} // namespace insilab::molib::details
