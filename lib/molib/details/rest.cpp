#include "rest.hpp"
#include "helper/help.hpp"
#include <iostream>
#include <vector>

namespace insilab::molib::details {

std::ostream &operator<<(std::ostream &stream, const Rest &t) {
  std::vector<std::string> v;
  if (t == Rest::notassigned)
    v.push_back("notassigned");
  if (t & Rest::buffer)
    v.push_back("buffer");
  if (t & Rest::protein)
    v.push_back("protein");
  if (t & Rest::nucleic)
    v.push_back("nucleic");
  if (t & Rest::ion)
    v.push_back("ion");
  if (t & Rest::water)
    v.push_back("water");
  if (t & Rest::small)
    v.push_back("small");
  if (t & Rest::glycan)
    v.push_back("glycan");
  if (t & Rest::oligopeptide)
    v.push_back("oligopeptide");
  if (t & Rest::polypeptide)
    v.push_back("polypeptide");
  if (t & Rest::compound)
    v.push_back("compound");
  if (t & Rest::cofactor)
    v.push_back("cofactor");
  if (t & Rest::substrate)
    v.push_back("substrate");
  if (t & Rest::DNA)
    v.push_back("DNA");
  if (t & Rest::RNA)
    v.push_back("RNA");
  if (t & Rest::nonstandard)
    v.push_back("nonstandard");
  if (t & Rest::covalent)
    v.push_back("covalent");
  if (t & Rest::competitive)
    v.push_back("competitive");
  if (t & Rest::N_linked)
    v.push_back("N_linked");
  if (t & Rest::O_linked)
    v.push_back("O_linked");
  if (t & Rest::conserved)
    v.push_back("conserved");

  for (std::size_t i = 0; i < v.size(); ++i) {
    stream << v[i] << (i < v.size() - 1 ? "." : "");
  }
  return stream;
}

Rest to_rest(const std::string &rest) {
  const auto &rest_v = helper::split(rest, ".");
  Rest result{Rest::notassigned};
  for (const auto &token : rest_v) {
    if (token == "notassigned")
      return Rest::notassigned; // immediately return
    else if (token == "buffer")
      result = Rest::buffer;
    else if (token == "protein")
      result = Rest::protein;
    else if (token == "small")
      result = Rest::small;
    else if (token == "nucleic")
      result = Rest::nucleic;
    else if (token == "ion")
      result = Rest::ion;
    else if (token == "water")
      result = Rest::water;
    else if (token == "glycan")
      result = Rest::glycan;
    else if (token == "oligopeptide")
      result |= Rest::oligopeptide;
    else if (token == "polypeptide")
      result |= Rest::polypeptide;
    else if (token == "compound")
      result |= Rest::compound;
    else if (token == "cofactor")
      result |= Rest::cofactor;
    else if (token == "substrate")
      result |= Rest::substrate;
    else if (token == "DNA")
      result |= Rest::DNA;
    else if (token == "RNA")
      result |= Rest::RNA;
    else if (token == "nonstandard")
      result |= Rest::nonstandard;
    else if (token == "covalent")
      result |= Rest::covalent;
    else if (token == "conserved")
      result |= Rest::conserved;
    else if (token == "competitive")
      result |= Rest::competitive;
    else if (token == "N_linked")
      result |= Rest::N_linked;
    else if (token == "O_linked")
      result |= Rest::O_linked;
  }
  if (result == Rest::notassigned) {
    throw helper::Error("[WHOOPS] Unrecognized residue type: " + rest);
  }
  return result;
}

bool is_notassigned(Rest rest) { return rest == Rest::notassigned; }
bool is_nonstandard(Rest rest) { return rest & Rest::nonstandard; }
bool is_buffer(Rest rest) { return rest & Rest::buffer; }
bool is_protein(Rest rest) { return rest & Rest::protein; }
bool is_small(Rest rest) { return rest & Rest::small; }
bool is_nucleic(Rest rest) { return rest & Rest::nucleic; }
bool is_glycan(Rest rest) { return rest & Rest::glycan; }
bool is_water(Rest rest) { return rest & Rest::water; }
bool is_ion(Rest rest) { return rest & Rest::ion; }
bool is_polypeptide(Rest rest) {
  return is_protein(rest) && rest & Rest::polypeptide;
}
bool is_oligopeptide(Rest rest) {
  return is_protein(rest) && rest & Rest::oligopeptide;
}
bool is_compound(Rest rest) { return is_small(rest) && rest & Rest::compound; }
bool is_cofactor(Rest rest) { return is_small(rest) && rest & Rest::cofactor; }
bool is_substrate(Rest rest) {
  return is_small(rest) && rest & Rest::substrate;
}
bool is_DNA(Rest rest) { return is_nucleic(rest) && rest & Rest::DNA; }
bool is_RNA(Rest rest) { return is_nucleic(rest) && rest & Rest::RNA; }
bool is_covalent(Rest rest) { return rest & Rest::covalent; }
bool is_cofactor_competitive(Rest rest) {
  return is_cofactor(rest) && rest & Rest::competitive;
}
bool is_substrate_competitive(Rest rest) {
  return is_substrate(rest) && rest & Rest::competitive;
  // with the new operator& could be rewritten to:
  // return rest & (Rest::small | Rest::substrate | Rest::competitive);
}

Rest unset_nonstandard(const Rest rest) {
  return Rest{static_cast<std::underlying_type_t<Rest>>(rest) &
              ~static_cast<std::underlying_type_t<Rest>>(Rest::nonstandard)};
}

} // namespace insilab::molib::details