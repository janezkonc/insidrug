#include "jsonio.hpp"
#include "helper/help.hpp"
#include "rest.hpp"

namespace insilab::molib::details {

void from_json(const nlohmann::json &rest_j, Rest &rest) {
  rest = to_rest(rest_j.get<std::string>());
}

void to_json(nlohmann::json &rest_j, const Rest &rest) {
  rest_j = helper::to_string(rest);
}

} // namespace insilab::molib::details
