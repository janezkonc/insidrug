#pragma once

#include "jsonio.hpp" // gives Json powers
#include <functional>
#include <iostream>
#include <map>

namespace insilab::molib::details {

// Vocabulary for composing residue types (rest) and binding site type
// (bsite_rest)
enum class Rest : std::size_t {
  notassigned = 0,
  nonstandard = 1, // protein|nucleic and nonstandard = modprot|modnucl
  buffer = 2,      // non-specific binders
  protein = 4,     // protein -> oligopeptide + polypeptide
  nucleic = 8,
  ion = 16, // metal ions
  water = 32,
  small = 64, // small molecule (compound or cofactor)
  glycan = 128,
  compound = 256,
  cofactor = 512,
  DNA = 1024,                     // nucleic and dna = dna
  RNA = 2048,                     // nucleic and rna = rna
  covalent = 4096,                // eg. compound and covalent = covcompound
  oligopeptide = 8192,            // eg. peptide  < 20 aa.
  polypeptide = oligopeptide * 2, // eg. >= 20 aa.
  competitive = polypeptide * 2,  // eg. cofactor-competitive,
  N_linked = competitive * 2,
  O_linked = N_linked * 2,
  conserved = O_linked * 2,
  substrate = conserved * 2,
};

// Add a residue type term (rhs) to existing residue type (lhs)
constexpr Rest operator|(const Rest &lhs, const Rest &rhs) {
  return static_cast<Rest>(static_cast<std::underlying_type_t<Rest>>(lhs) |
                           static_cast<std::underlying_type_t<Rest>>(rhs));
}

constexpr Rest &operator|=(Rest &lhs, const Rest &rhs) {
  lhs = lhs | rhs;
  return lhs;
}

/**
 * Test for the presence of multiple rhs flags in lhs (test if all flags
 * of rhs are set in lhs). For example:
 *
 * 0111 & 0110 = true (both rhs flags are set in lhs)
 * 0101 & 0110 = false (flag 2 is not set in lhs)
 * (protein | oligopeptide) & (protein | polypeptide) = false
 * (small | compound | covalent) & (small | compound) = true
 *
 * This is a generalization of simple & operator which would just test for the
 * presence of a single rhs flag in lhs. With simple &, the above examples would
 * give unexpected (wrong) results:
 *
 * 0111 & 0110 = true
 * 0101 & 0110 = true (wrong)
 * (protein | oligopeptide) & (protein | polypeptide) = true (wrong)
 * (small | compound | covalent) & (small | compound) = true
 *
 *
 */
constexpr bool operator&(const Rest &lhs, const Rest &rhs) {
  return (static_cast<std::underlying_type_t<Rest>>(lhs) &
          static_cast<std::underlying_type_t<Rest>>(rhs)) ==
         static_cast<std::underlying_type_t<Rest>>(rhs);
}

// Convenience shorthand values for types
const Rest compound_v = Rest::small | Rest::compound;

const Rest cofactor_v = Rest::small | Rest::cofactor;

const Rest substrate_competitive_v =
    Rest::small | Rest::substrate | Rest::competitive;

const Rest cofactor_competitive_v =
    Rest::small | Rest::cofactor | Rest::competitive;

// Write residue type to output stream, e.g., small.cofactor
std::ostream &operator<<(std::ostream &stream, const Rest &t);

/**
 * Convert string to residue type (rest).
 *
 * @param rest a string representing residue type, e.g., small.cofactor
 * @return residue type
 */
Rest to_rest(const std::string &rest);

// Convenience functions
bool is_notassigned(Rest rest);
bool is_nonstandard(Rest rest);
bool is_buffer(Rest rest);
bool is_protein(Rest rest);
bool is_small(Rest rest);
bool is_nucleic(Rest rest);
bool is_glycan(Rest rest);
bool is_water(Rest rest);
bool is_ion(Rest rest);
bool is_polypeptide(Rest rest);
bool is_oligopeptide(Rest rest);
bool is_compound(Rest rest);
bool is_cofactor(Rest rest);
bool is_substrate(Rest rest);
bool is_DNA(Rest rest);
bool is_RNA(Rest rest);
bool is_covalent(Rest rest);
bool is_cofactor_competitive(Rest rest);
bool is_substrate_competitive(Rest rest);

/// Unset nonstandard bit from residue type mask
Rest unset_nonstandard(const Rest rest);

// Map residue type to a function that determines this type
const std::map<Rest, std::function<bool(Rest)>> rest_fn{
    {Rest::notassigned, is_notassigned},
    {Rest::nonstandard, is_nonstandard},
    {Rest::buffer, is_buffer},
    {Rest::protein, is_protein},
    {Rest::small, is_small},
    {Rest::nucleic, is_nucleic},
    {Rest::glycan, is_glycan},
    {Rest::water, is_water},
    {Rest::ion, is_ion},
    {Rest::protein | Rest::polypeptide, is_polypeptide},
    {Rest::protein | Rest::oligopeptide, is_oligopeptide},
    {Rest::small | Rest::compound, is_compound},
    {Rest::small | Rest::cofactor, is_cofactor},
    {Rest::substrate, is_substrate},
    {Rest::nucleic | Rest::DNA, is_DNA},
    {Rest::nucleic | Rest::RNA, is_RNA},
    {Rest::covalent, is_covalent},
    {Rest::small | Rest::cofactor | Rest::competitive, is_cofactor_competitive},
    {Rest::small | Rest::substrate | Rest::competitive,
     is_substrate_competitive},
};

} // namespace insilab::molib::details
