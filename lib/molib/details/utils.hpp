#pragma once

#include "common.hpp"
#include "geom3d/coordinate.hpp"
#include "molib/algorithms/filter.hpp"
#include "molib/algorithms/internals/typing_steps.hpp"
#include "molib/atom.hpp"
#include "molib/io/writer.hpp"
#include <concepts>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <vector>

namespace insilab::helper {
template <typename, typename, typename> class template_map_container;
}

namespace insilab::molib::details {

/**
 * A generic class that adds various functionalities (eg. access to molecular
 * substructures) from all levels of molecular hierarchy classes (see
 * molecules.hpp, molecule.hpp, assembly.hpp, etc.). Thus, this class is
 * intended to be inherited from each and all of the molecule class hiearchy
 * (possible levels are molecules, molecule, assembly, model, chain,
 * segment, residue). For example: to access atoms from any level use
 * .get_atoms(); to access residues (level > residue): .get_residues(); to
 * access coordinates: .get_crds();
 */
template <typename T> class Utils {

  template <typename> friend class Utils; // need this to access .br()
  template <typename, typename, typename>
  friend class insilab::helper::template_map_container; // so that it has access
                                                        // to set_br()
  using BackReferencePointer = typename FromType<T>::type;
  BackReferencePointer __br; // should not be declared if T == MOLECULES but
                             // std::enable_if doesn't seem to be working...
  // typename std::enable_if<std::is_same_v<T, Atom>, bool>::type janez = true;
  // this doesn't compile...

  auto &br() const
    requires MoleculeMinus<T>
  {
    return *__br;
  }

  /**
   * Sets back reference (eg. for Atom, parent is Residue) during insertion of
   * new Atom, Residue,...
   *
   * @param br back reference pointer to Residue if T = Atom (for example)
   */
  void set_br(BackReferencePointer br)
    requires MoleculeMinus<T>
  {
    __br = br;
  }

public:
  Utils() = default;
  Utils(const Utils &rhs) = default;

  /**
   * Move constructor for types Molecules, Molecule, Assembly, Model, Chain,
   * Segment and Residue need to be explicitely defined, because we have to fix
   * the underlying type' back references each time a Molecule is moved,e.g.,
   * for Molecule, we have to fix Assembly's back references.
   *
   * Example:
   *
   * Molecule m2 = std::move(m1); // because &m2 != &m1, the addresses of m2's
   * assemblies are invalid (see also Hydrogens::compute_hydrogen for example)
   *
   * Another solution could be to return std::unique_ptr<Molecule> from
   * functions such as generate_bio, in which case Molecule doesn't need a move
   * constructor at all as unique_ptr's move constructor will be called (but
   * then we have to deal with unique_ptr's which could be a mess).
   */
  template <typename P = T>
  Utils(Utils &&rhs)
    requires ResiduePlus<T>
  {
    for (auto &subcont : *static_cast<const T *>(this))
      subcont.set_br(static_cast<T *>(this));
  }

  /**
   * Write a molecule to a file in the specified format which is determined from
   * the file suffix. Writing is thread-safe due to use of (thread-safe) inout
   * functions.
   *
   * Example: molecule.write("test.pdb").
   *
   * @param filename file name, for supported formats see writer.cpp
   * @param hm open or append to file
   */
  template <io::Writable P = T>
  void write(const std::string &filename,
             const io::Writer::Options hm = io::Writer::Options::open) const {
    typename T::Writer(filename, hm).write(*static_cast<const T *>(this));
  }

  Residue &residue() const
    requires std::is_same_v<T, Atom>
  {
    return static_cast<const T *>(this)->br();
  }

  Segment &segment() const
    requires ResidueMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  Chain &chain() const
    requires SegmentMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br().br();
    } else if constexpr (std::is_same_v<T, Residue>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  Model &model() const
    requires ChainMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br().br().br();
    } else if constexpr (std::is_same_v<T, Residue>) {
      return static_cast<const T *>(this)->br().br().br();
    } else if constexpr (std::is_same_v<T, Segment>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  Assembly &assembly() const
    requires ModelMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Residue>) {
      return static_cast<const T *>(this)->br().br().br().br();
    } else if constexpr (std::is_same_v<T, Segment>) {
      return static_cast<const T *>(this)->br().br().br();
    } else if constexpr (std::is_same_v<T, Chain>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  Molecule &molecule() const
    requires AssemblyMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Residue>) {
      return static_cast<const T *>(this)->br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Segment>) {
      return static_cast<const T *>(this)->br().br().br().br();
    } else if constexpr (std::is_same_v<T, Chain>) {
      return static_cast<const T *>(this)->br().br().br();
    } else if constexpr (std::is_same_v<T, Model>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  Molecules &molecules() const
    requires MoleculeMinus<T>
  {
    if constexpr (std::is_same_v<T, Atom>) {
      return static_cast<const T *>(this)->br().br().br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Residue>) {
      return static_cast<const T *>(this)->br().br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Segment>) {
      return static_cast<const T *>(this)->br().br().br().br().br();
    } else if constexpr (std::is_same_v<T, Chain>) {
      return static_cast<const T *>(this)->br().br().br().br();
    } else if constexpr (std::is_same_v<T, Model>) {
      return static_cast<const T *>(this)->br().br().br();
    } else if constexpr (std::is_same_v<T, Assembly>) {
      return static_cast<const T *>(this)->br().br();
    } else {
      return static_cast<const T *>(this)->br();
    }
  }

  /**
   * Get reference to an atom by providing its atom number.
   *
   * @param atom_number unique atomic number within a molecule
   * @return reference to an atom if atom with this atom number is found, else
   * throw an exception
   */
  Atom &get_atom(const int atom_number) const
    requires MoleculeMinus<T>
  {
    const auto &atoms = this->get_atoms();
    const auto &it =
        std::ranges::find(atoms, atom_number, [](const auto *patom) {
          return patom->atom_number();
        });
    if (it == atoms.end())
      throw helper::Error("[WHOOPS] Atom number " +
                          std::to_string(atom_number) + " not found!");
    return **it;
  }

  template <typename P = T>
  auto get_molecules() const
    requires std::is_same_v<T, Molecules>
  {
    // type P is needed to delay instantiation, not used..
    std::vector<Molecule *> molecules;
    for (auto &subcont : *static_cast<const T *>(this)) {
      molecules.push_back(&subcont);
    }
    return molecules;
  }

  template <typename P = T>
  auto get_assemblies() const
    requires MoleculePlus<T>
  {
    // type P is needed to delay instantiation, not used..
    std::vector<Assembly *> assemblies;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same_v<T, Molecule>) {
        assemblies.push_back(&subcont);
      } else {
        auto ret = subcont.template get_assemblies<decltype(subcont)>();
        assemblies.insert(assemblies.end(), ret.begin(), ret.end());
      }
    }
    return assemblies;
  }

  template <typename P = T>
  auto get_models() const
    requires AssemblyPlus<T>
  {
    // type P is needed to delay instantiation, not used..
    std::vector<Model *> models;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same_v<T, Assembly>) {
        models.push_back(&subcont);
      } else {
        auto ret = subcont.template get_models<decltype(subcont)>();
        models.insert(models.end(), ret.begin(), ret.end());
      }
    }
    return models;
  }

  template <typename P = T>
  auto get_chains() const
    requires ModelPlus<T>
  {
    // type P is needed to delay instantiation, not used..
    std::vector<Chain *> chains;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same_v<T, Model>) {
        chains.push_back(&subcont);
      } else {
        auto ret = subcont.template get_chains<decltype(subcont)>();
        chains.insert(chains.end(), ret.begin(), ret.end());
      }
    }
    return chains;
  }

  template <typename P = T>
  auto get_segments() const
    requires ChainPlus<T>
  {
    // type P is needed to delay instantiation, not used..
    std::vector<Segment *> segments;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same<T, Chain>::value) {
        segments.push_back(&subcont);
      } else {
        auto ret = subcont.template get_segments<decltype(subcont)>();
        segments.insert(segments.end(), ret.begin(), ret.end());
      }
    }
    return segments;
  }

  template <typename P = T>
  auto get_residues() const
    requires SegmentPlus<T>
  {
    std::vector<Residue *> residues;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same<T, Segment>::value) {
        residues.push_back(&subcont);
      } else {
        auto ret = subcont.template get_residues<decltype(subcont)>();
        residues.insert(residues.end(), ret.begin(), ret.end());
      }
    }
    return residues;
  }

  template <typename P = T>
  auto get_atoms() const
    requires ResiduePlus<T>
  {
    std::vector<Atom *> atoms;
    for (auto &subcont : *static_cast<const T *>(this)) {
      if constexpr (std::is_same<T, Residue>::value) {
        atoms.push_back(&subcont);
      } else {
        auto ret = subcont.template get_atoms<decltype(subcont)>();
        atoms.insert(atoms.end(), ret.begin(), ret.end());
      }
    }
    return atoms;
  }

  auto get_crds() const
    requires ResiduePlus<T>
  {
    std::vector<geom3d::Coordinate> crds;
    for (auto &patom : this->get_atoms())
      crds.push_back(patom->crd());
    return crds;
  }

  T &rotate(const geom3d::Matrix &rota, const bool inverse = false)
    requires ResiduePlus<T>
  {
    if (inverse) {
      for (auto &patom : this->get_atoms())
        patom->crd().inverse_rotate_inline(rota);
    } else {
      for (auto &patom : this->get_atoms())
        patom->crd().rotate_inline(rota);
    }
    return static_cast<T &>(*this);
  }

  T &erase_properties()
    requires ResiduePlus<T>
  {
    for (auto &patom : this->get_atoms())
      patom->erase_properties();
    return static_cast<T &>(*this);
  }

  std::set<int> get_idatm_types() const
    requires ResiduePlus<T>
  {
    std::set<int> idatms;
    for (auto &patom : this->get_atoms()) {
      idatms.insert(patom->idatm_type());
    }
    return idatms;
  }

  /**
   * Builder pattern method that extends the lifetime of a chain of methods by
   * invoking a move constructor. Use to terminate chains of functios. This is
   * efficient, since a move is called instead of copy constructor.
   *
   * Example:
   * const Molecule m =
   * Parser(...).parse().first().generate_bio(...).compute_types().build();
   *
   * @return an explicitly st::move-ed Molecule or Molecules object
   * @note std::move needs to be called on return explicitly
   */
  T build()
    requires MoleculePlus<T>
  {
    return std::move(static_cast<T &>(*this));
  }

  /**
   * Select subcomponents using a lambda function that has access to the child
   * component.
   *
   * @note comp function's parameter must be child (subcontainer) of current
   * container, that's why we need 'decltype' in requires expression.
   *
   * @param comp a function that returns true if the subcontainer is to be
   * included in the selection, eg. [](const Molecule *i) -> bool
   * @return a new molecule (or molecules) that passed the filter criterion
   */
  template <typename Compare>
  T select(Compare comp) const
    requires MoleculePlus<T> &&
             requires(Compare comp,
                      decltype(std::declval<T>().first()) subcont) {
               comp(subcont);
               { comp(subcont) } -> std::same_as<bool>;
             }
  {
    T result;
    result.shallow_copy(*static_cast<const T *>(this));
    for (auto &subcont : *static_cast<const T *>(this)) {
      if (comp(subcont))
        result.add(subcont);
    }
    return result; // std::move called implicitly
  }

  /**
   * Filter the atoms of a molib container using an expression very similar
   * to PyMOL's selection algebra. For example, to get only protein residues,
   * use filter expression: {Key::REST, Residue::Type::protein}. See
   * test/check_filter.cpp for more examples.
   *
   * @tparam ret_t if false (default), return a new molib container, if true,
   * return a vector of atom pointers to the original molecule
   * @param filter_expr a vector with filter expression
   * @return a new molib container, e.g., Molecule, that contains only atoms of
   * the original molecule that passed the filtering (note: filtered atom
   * pointers are different from the ones in the original molecule) OR a vector
   * of pointers to atoms in the existing molecule that passed the filter
   */
  template <bool ret_t = false>
  auto filter(const algorithms::ExprVec &filter_expr) const
    requires MoleculePlus<T>
  {
    const auto filtered_atoms =
        algorithms::filter(this->get_atoms(), filter_expr);
    if constexpr (ret_t)
      return filtered_atoms;
    else {
      const T filtered{filtered_atoms};
      return filtered;
    }
  }

  /**
   * Compute residue types: protein, nucleic, peptide, small, etc.
   */
  T &compute_residue_types(const double moltype_tanimoto_cutoff = 0.8,
                           const unsigned small = 20)
    requires ResiduePlus<T>
  {
    if constexpr (std::is_same_v<T, Residue>)
      algorithms::internals::run_steps_for("covalent",
                                           {static_cast<T &>(*this)},
                                           moltype_tanimoto_cutoff, small);
    else
      algorithms::internals::run_steps_for("covalent", this->get_residues(),
                                           moltype_tanimoto_cutoff, small);
    return static_cast<T &>(*this);
  }

  /**
   * Compute atom and bond types: IDATM types, GAFF types, ring types, rotatable
   * bonds, bond orders, bond gaff types.
   */
  T &compute_atom_bond_types()
    requires ResiduePlus<T>
  {
    algorithms::internals::run_steps_for("all", this->get_atoms());
    return static_cast<T &>(*this);
  }

  /**
   * Determines whether two containers (e.g., two residues) are adjacent, that
   * is they are connected through a bond and have no atoms in common.
   *
   * @param other any molib container that is at least a Residue
   * @return true, if containers are connected by one or more bonds and have
   * empty intersection, false otherwise
   */
  bool is_adjacent(const T &other) const
    requires HasGetAtoms<T>
  {
    const auto &atoms1 = this->get_atoms();
    const auto &atoms2 = other.get_atoms();
    const std::set<Atom *> atoms1_s(std::begin(atoms1), std::end(atoms1));
    const std::set<Atom *> atoms2_s(std::begin(atoms2), std::end(atoms2));
    if (helper::has_element_in_common(std::begin(atoms1_s), std::end(atoms1_s),
                                      std::begin(atoms2_s), std::end(atoms2_s)))
      return false;
    for (const auto &patom1 : atoms1) {
      for (auto &bondee : *patom1) {
        if (atoms2_s.contains(&bondee))
          return true;
      }
    }
    return false;
  }
};

} // namespace insilab::molib::details
