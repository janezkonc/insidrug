#include "molecule.hpp"
#include "assembly.hpp"
#include "atom.hpp"
#include "bond.hpp"
#include "chain.hpp"
#include "details/utility.hpp"
#include "geom3d/linear.hpp"
#include "io/iomanip.hpp"
#include "model.hpp"
#include "residue.hpp"
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <variant>
#include <vector>

namespace insilab::molib {

std::ostream &operator<<(std::ostream &stream, const Molecule &molecule) {
  
  using namespace io;

  const IOmanip::IOtype io = IOmanip::get_IOtype(stream);

  if ((io == IOmanip::IOtype::default_t) || (io & IOmanip::IOtype::pdb_t)) {
    stream << Writer::PdbWriter().write(molecule).str();
  } else if (io == IOmanip::IOtype::mol2_t) {
    stream << Writer::Mol2Writer().write(molecule).str();
  } else if (io == IOmanip::IOtype::cif_t) {
    stream << Writer::CifWriter().write(molecule).str();
  } else if (io == IOmanip::IOtype::json_t) {
    stream << Writer::JsonWriter().write(molecule).str();
  }

  return stream;
}

// Copy molecule with new coordinates
Molecule::Molecule(const Molecule &rhs, const geom3d::Point::Vec &crds)
    : Molecule(rhs) {
  auto atoms = this->get_atoms();
  for (int i = 0; i < atoms.size(); ++i) {
    atoms[i]->set_crd(crds[i]);
  }
}

Molecule::Molecule(const Molecule &rhs) // deep copy constructor
    : template_map_container<Assembly, Molecule>(rhs) {
  this->shallow_copy(rhs);
  this->regenerate_bonds(rhs);
}

/**
 * Create a new molecule from selected atoms.
 *
 * @note we cannot simply call Molecule::operator=, because atoms may be a
 * subset of all molecule's atoms
 */
template <details::IsAtomVecOrSet T> Molecule::Molecule(const T &atoms) {
  if (atoms.empty())
    return;
  for (auto &patom : atoms) {

    this->add(Assembly(patom->assembly().params()))
        .add(Model(patom->model().params()))
        .add(Chain(patom->chain().params()))
        .add(Segment(patom->segment().params()))
        .add(Residue(patom->residue().params()))
        .add(Atom(patom->params()));
  }
  const auto &molecule = (*atoms.begin())->molecule();
  this->shallow_copy(molecule);
  this->regenerate_bonds(molecule);
}

template Molecule::Molecule(const Atom::PSet &);
template Molecule::Molecule(const Atom::PVec &);

void Molecule::shallow_copy(const Molecule &rhs) { __params = rhs.__params; }

Molecule &Molecule::operator=(const Molecule &rhs) {
  if (this != &rhs) {
    this->template_map_container<Assembly, Molecule>::operator=(rhs);
    this->shallow_copy(rhs);
    this->regenerate_bonds(rhs);
  }
  return *this;
}

/**
 * Generate biological assembly(ies) based on the PDB record BIOMOLECULE.
 *
 * @param bhm generate first, all biological assemblies, or asymmetric unit
 * @param fallback_to_asymmetric if there is no biological assembly, just output
 * asymmetric unit
 * @return a new Molecule with the requested biomolecule(s)
 * @throws error when there is no BIOMOLECULE record (bhm=[first, all])
 */
Molecule Molecule::generate_bio(BioHowMany bhm,
                                const bool fallback_to_asymmetric) const {
  if (bhm == BioHowMany::asym ||
      fallback_to_asymmetric && this->__params.bio_rota.empty())
    return Molecule(*this);
  if (!fallback_to_asymmetric && this->__params.bio_rota.empty())
    throw helper::Error(
        "[WHOOPS] request to generate biological assembly failed "
        "because molecule " +
        this->name() + " does not contain biomolecule information");
  Molecule result;
  result.shallow_copy(*this);
  for (const auto &[biological_unit_id, matrices] : this->__params.bio_rota) {
    const auto &segments_to_apply_matrix =
        this->__params.bio_segment.at(biological_unit_id);
    for (const auto &[matrix_id, rotational_matrix] : matrices) {
      auto &model = result
                        .add(Assembly({.assembly_id = biological_unit_id,
                                       .name = "BIOLOGICAL ASSEMBLY"}))
                        .add(Model({.model_id = matrix_id}));
      for (const auto &psegment : this->first().get_segments()) {
        if (std::count(segments_to_apply_matrix.begin(),
                       segments_to_apply_matrix.end(), psegment->seg_id())) {
          const auto &chain = psegment->chain();
          model.add(Chain(chain));
        }
      }
      model.rotate(rotational_matrix);
    }
    if (bhm == Molecule::BioHowMany::first_bio)
      break;
  }
  result.regenerate_bonds(*this);
  return result; // implicitly, return will use std::move()
}

/**
 * Whenever a molecule is copied, atom and bond pointers still point to the
 * original molecule. Here, they are regenerated. If the template molecule is
 * bigger (e.g., when regenerating bonds for fragments), there may be no atom in
 * the copied molecule with the original atom_number. This is checked - if atom
 * is not found. It is assumed that within each model, atom numbers are unique,
 * but atom numbers are not unique across models (typical in PDB).
 */
Molecule &Molecule::regenerate_bonds(const Molecule &template_molecule) {
  const auto &template_atoms = template_molecule.get_atoms();
  for (const auto &pmodel : this->get_models()) {
    const auto &atoms = pmodel->get_atoms();
    // connect all atoms with same numbers (within each model) in this molecule
    for (auto &patom : template_atoms) {
      auto &atom = *patom;
      auto it1 = std::find_if(atoms.begin(), atoms.end(), [&atom](Atom *a) {
        return atom.atom_number() == a->atom_number();
      });
      if (it1 == atoms.end())
        continue;
      for (const auto &neighb : atom) {
        auto it2 = std::find_if(atoms.begin(), atoms.end(), [&neighb](Atom *a) {
          return neighb.atom_number() == a->atom_number();
        });
        if (it2 == atoms.end())
          continue;
        const auto &old_bond = atom.get_bond(neighb);
        Atom &atom1 = **it1;
        Atom &atom2 = **it2;
        auto &bond = atom1.connect(atom2);
        bond.shallow_copy(old_bond); // copy bond parameters - this should be
                                     // fixed (see issue #81)
      }
    }
  }
  Bond::connect_bonds(details::get_bonds_from<details::get_bonds_t::in_as_set>(
      this->get_atoms()));
  return *this;
}

} // namespace insilab::molib
