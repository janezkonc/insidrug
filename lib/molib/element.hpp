#pragma once

#include <array>
#include <iostream>
#include <utility>
#include <vector>

namespace insilab::molib {

class Element {
public:
  // atomic index (atoms are listed in the same order as in periodic table, but
  // this is not atomic number, because of H, D and T (there is only H in
  // periodic table)
  enum atom_index : unsigned short {
    H,
    D,
    T,
    He,
    Li,
    Be,
    B,
    C,
    N,
    O,
    F,
    Ne,
    Na,
    Mg,
    Al,
    Si,
    P,
    S,
    Cl,
    Ar,
    K,
    Ca,
    Sc,
    Ti,
    V,
    Cr,
    Mn,
    Fe,
    Co,
    Ni,
    Cu,
    Zn,
    Ga,
    Ge,
    As,
    Se,
    Br,
    Kr,
    Rb,
    Sr,
    Y,
    Zr,
    Nb,
    Mo,
    Tc,
    Ru,
    Rh,
    Pd,
    Ag,
    Cd,
    In,
    Sn,
    Sb,
    Te,
    I,
    Xe,
    Cs,
    Ba,
    La,
    Ce,
    Pr,
    Nd,
    Pm,
    Sm,
    Eu,
    Gd,
    Tb,
    Dy,
    Ho,
    Er,
    Tm,
    Yb,
    Lu,
    Hf,
    Ta,
    W,
    Re,
    Os,
    Ir,
    Pt,
    Au,
    Hg,
    Tl,
    Pb,
    Bi,
    Po,
    At,
    Rn,
    Fr,
    Ra,
    Ac,
    Th,
    Pa,
    U,
    Np,
    Pu,
    Am,
    Cm,
    Bk,
    Cf,
    Es,
    Fm,
    Md,
    No,
    Lr,
    Rf,
    Db,
    Sg,
    Bh,
    Hs,
    Mt,
    Ds,
    Rg,
    Cn,
    Nh,
    Fl,
    Mc,
    Lv,
    Ts,
    Og,
  };

  // enum class crashes compiler (gcc 12.1) with error: internal compiler error:
  // in tsubst_copy, at cp/pt.cc:16919 (UNCOMMENT BELOW (AND CHANGE ATOM_INDEX
  // TO ENUM CLASS) WITH NEW COMPILER VERSION!)

  // using enum atom_index;

private:
  static const std::vector<double> __vdw_radius;
  static const std::vector<std::string> __atom_symbol;

  atom_index __get_index(const std::string &name);
  atom_index __as;

public:
  Element() = default;
  Element(const atom_index &as) : __as(as) {}
  Element(const std::string &name) : __as(__get_index(name)) {}

  /// Get element symbol as a string
  const std::string &name() const {
    return __atom_symbol.at(std::to_underlying(__as));
  }

  /// Get van der Waals radius for element
  double vdw_radius() const {
    return __vdw_radius.at(std::to_underlying(__as));
  }

  auto operator==(const Element &rhs) const {
    return std::to_underlying(__as) == std::to_underlying(rhs.__as);
  }

  auto operator<=>(const Element &rhs) const {
    return std::to_underlying(__as) <=> std::to_underlying(rhs.__as);
  }

  friend std::ostream &operator<<(std::ostream &, const Element &);

  /**
   * First letter to upper case, second letter to lower case, if it exists.
   *
   * @param s expects 1 or 2 letter string element symbol, e.g. CL, cl, cL
   * (throws if s.empty or s.size > 2)
   * @return sanitized element symbol as string, e.g., Cl
   */
  static std::string sanitize(std::string s);
};

} // namespace insilab::molib
