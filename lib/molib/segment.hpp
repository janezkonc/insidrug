#pragma once

#include "atom.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "helper/it.hpp"
#include "molib/details/utils.hpp"
#include "residue.hpp"
#include <set>
#include <vector>

namespace insilab::molib {

class Residue;
class Atom;
class Model;

class Segment
    : public helper::template_map_container<Residue, Segment, Residue::Pair>,
      public details::Utils<Segment> {
public:
  struct Params {
    std::string seg_id{"A"}; // segment id from cif file
  };

private:
  Params __params;

public:
  using PVec = std::vector<Segment *>;
  using PSet = std::set<Segment *>;
  Segment() = default;
  Segment(const Params &p) : __params(p) {}

  const Params &params() const { return __params; }

  Residue &add(Residue r) {
    const auto i = Residue::Pair(r.resi(), r.ins_code());
    return this->insert(i, std::move(r), this);
  }
  Residue &residue(Residue::Pair p) const { return this->element(p); }

  bool has_residue(Residue::Pair p) { return this->has_element(p); }
  std::string seg_id() const { return __params.seg_id; }
};

} // namespace insilab::molib
