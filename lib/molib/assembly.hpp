#pragma once

#include "atom.hpp"
#include "chain.hpp"
#include "details/utils.hpp"
#include "element.hpp"
#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "grid/grid.hpp"
#include "helper/it.hpp"
#include "model.hpp"
#include "residue.hpp"

namespace insilab::molib {

class Chain;
class Residue;
class Atom;
class Molecule;

class Assembly : public helper::template_map_container<Model, Assembly>,
                 public details::Utils<Assembly> {
public:
  struct Params {
    int assembly_id{0};
    std::string name{"ASYMMETRIC UNIT"};
  };

private:
  Params __params;

public:
  Assembly() = default;
  Assembly(const Params &p) : __params(p) {}
  const Params &params() const { return __params; }

  int assembly_id() const { return __params.assembly_id; }
  std::string name() const { return __params.name; }
  Model &add(Model m) {
    const auto i = m.model_id();
    return this->insert(i, std::move(m), this);
  }
};

} // namespace insilab::molib
