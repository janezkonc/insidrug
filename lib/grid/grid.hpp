#pragma once

#include "geom3d/coordinate.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

namespace insilab::grid {

// template <class U>
// concept HasCrd = requires(const U &u) {
//   { u.crd() } -> std::same_as<const geom3d::Point &>;
// };

template <class U>
concept HasCrdAndRadius = requires(const U &u) {
  { u.radius() } -> std::same_as<double>;
  { u.crd() } -> std::same_as<const geom3d::Point &>;
};

template <class A> struct is_unique_pointer : std::false_type {};
template <class A, class B>
struct is_unique_pointer<std::unique_ptr<A, B>> : std::true_type {};

template <class A> struct is_const_unique_pointer : std::false_type {};
template <class A, class B>
struct is_const_unique_pointer<const std::unique_ptr<A, B>> : std::true_type {};

/**
 * Class Grid enables mapping coordinates (and other objects that expose
 * required interface) to a cubic grid, which enables fast neighbors
 * computations.
 *
 * @param T may be any container (eg. Point, Atom etc) that exposes .crd()
 * function, which returns a coordinate
 * @note Grid is a non-owning container (stores only pointers), which means that
 * objects that are given to Grid must be available for the lifetime of Grid,
 * for example:
 *
 * This is correct usage:
 * Point::Vec points = molecule.get_crds();
 * Point::Grid grid (points);
 *
 * This will result in undefined behavior:
 * Point::Grid grid(molecule.get_crds());
 */
template <typename T>
// requires requires(const T &t) {
//   { t.crd() } -> std::same_as<const geom3d::Point>;
// }
class Grid {
public:
  using Points = std::vector<T *>;
  using Vec = std::vector<Grid>;

private:
  Points ***storage;
  int szi, szj, szk;

  geom3d::Coordinate __min_crd;

  template <typename U>
  geom3d::Coordinate __correct(const U &point, const double &dist) const {
    geom3d::Coordinate crd = point - __min_crd + dist;
    if (dist < 0) {
      if (crd.x() < 0)
        crd.set_x(0);
      if (crd.y() < 0)
        crd.set_y(0);
      if (crd.z() < 0)
        crd.set_z(0);
    } else {
      if (crd.x() >= szi)
        crd.set_x(szi - 1);
      if (crd.y() >= szj)
        crd.set_y(szj - 1);
      if (crd.z() >= szk)
        crd.set_z(szk - 1);
    }
    return crd;
  }
  void __allocate(const int szii, const int szjj, const int szkk) {
    storage = new Points **[szii]; // allocate memory
    for (int i = 0; i < szii; ++i) {
      storage[i] = new Points *[szjj];
      for (int j = 0; j < szjj; ++j) {
        storage[i][j] = new Points[szkk];
      }
    }
  }
  void __deallocate() {
    for (int i = 0; i < szi; ++i) {
      for (int j = 0; j < szj; ++j) {
        for (int k = 0; k < szk; ++k)
          storage[i][j][k].clear();
        delete[] storage[i][j];
      }
      delete[] storage[i];
    }
    delete[] storage;
  }

  void __copy(const Grid &rhs) {
    // Allocate new space, copy values...
    this->__allocate(rhs.szi, rhs.szj, rhs.szk);
    this->__min_crd = rhs.__min_crd;
    this->szi = rhs.szi;
    this->szj = rhs.szj;
    this->szk = rhs.szk;
    for (int i = 0; i < szi; ++i) {
      for (int j = 0; j < szj; ++j) {
        for (int k = 0; k < szk; ++k)
          storage[i][j][k].assign(rhs.storage[i][j][k].begin(),
                                  rhs.storage[i][j][k].end());
      }
    }
  }

  template <class Z = T,
            typename std::enable_if<is_const_unique_pointer<Z>::value>::type * =
                nullptr>
  Z &ptr(Z &obj) const {
    return obj;
  }

  template <class Z = T, typename std::enable_if<
                             is_unique_pointer<Z>::value>::type * = nullptr>
  Z &ptr(Z &obj) const {
    return obj;
  }

  template <class Z = T, typename std::enable_if<
                             std::is_pointer<Z>::value>::type * = nullptr>
  Z ptr(Z &obj) const {
    return obj;
  }

  template <class Z = T,
            typename std::enable_if<
                !std::is_pointer<Z>::value && !is_unique_pointer<Z>::value &&
                !is_const_unique_pointer<Z>::value>::type * = nullptr>
  Z *ptr(Z &obj) const {
    return &obj;
  }

public:
  //~ template<typename U>
  //~ Grid(Grid<U>&& other) noexcept {
  //~ std::clog << "Grid move constructor" << std::endl;
  //~ }
  //~

  Grid() : storage(nullptr), szi(0), szj(0), szk(0) {}

  Grid(const Grid &rhs) { this->__copy(rhs); }

  bool empty() const { return szi == 0 && szj == 0 && szk == 0; }

  Grid &operator=(const Grid &rhs) {
    dbgmsg("Grid assignment operator");
    if (this != &rhs) {
      this->__deallocate();
      this->__copy(rhs);
    }
    return *this;
  }
  bool operator==(const Grid &rhs) const {
    const bool pods = this->szi == rhs.szi && this->szj == rhs.szj &&
                      this->szk == rhs.szk && this->__min_crd == rhs.__min_crd;
    bool nonpods = true;
    for (int i = 0; i < this->szi; ++i) {
      for (int j = 0; j < this->szj; ++j) {
        for (int k = 0; k < this->szk; ++k) {
          nonpods = nonpods && this->storage[i][j][k].size() ==
                                   rhs.storage[i][j][k].size();
          for (int z = 0; z < this->storage[i][j][k].size(); ++z) {
            nonpods = nonpods && this->storage[i][j][k].at(z) ==
                                     rhs.storage[i][j][k].at(z);
          }
        }
      }
    }
    return pods && nonpods;
  }
  template <typename P>
  Grid(const P &points) : storage(nullptr), szi(0), szj(0), szk(0) {
    dbgmsg("size of points in grid " << points.size());
    dbgmsg("points is empty " << std::boolalpha << points.empty());
    if (!points.empty()) {
      // calculate min and max crd
      geom3d::Coordinate min_crd(std::numeric_limits<double>::max(),
                                 std::numeric_limits<double>::max(),
                                 std::numeric_limits<double>::max()),
          max_crd(std::numeric_limits<double>::lowest(),
                  std::numeric_limits<double>::lowest(),
                  std::numeric_limits<double>::lowest());
      for (const auto &point : points) {
        const T *ppoint = &*ptr(point);
        if (ppoint->crd().x() < min_crd.x())
          min_crd.set_x(ppoint->crd().x());
        if (ppoint->crd().y() < min_crd.y())
          min_crd.set_y(ppoint->crd().y());
        if (ppoint->crd().z() < min_crd.z())
          min_crd.set_z(ppoint->crd().z());
        if (ppoint->crd().x() > max_crd.x())
          max_crd.set_x(ppoint->crd().x());
        if (ppoint->crd().y() > max_crd.y())
          max_crd.set_y(ppoint->crd().y());
        if (ppoint->crd().z() > max_crd.z())
          max_crd.set_z(ppoint->crd().z());
      }
      __min_crd = min_crd;
      geom3d::Coordinate corr = max_crd - min_crd; // calculate size of grid
      szi = corr.i() + 1;
      szj = corr.j() + 1;
      szk = corr.k() + 1;
      this->__allocate(szi, szj, szk);
      for (const auto &point : points) { // set data points to grid cells
        const T *ppoint = &*ptr(point);
        geom3d::Coordinate crd = ppoint->crd() - min_crd;
        storage[crd.i()][crd.j()][crd.k()].push_back(const_cast<T *>(ppoint));
      }
    }
    dbgmsg("points is empty2 " << std::boolalpha << points.empty());
  }

  ~Grid() {
    dbgmsg("Grid destructor");
    this->__deallocate();
  }

  template <typename U>
  Points get_neighbors(const U &point, const double &dist) const {
    geom3d::Coordinate cmin = __correct(point, -dist);
    geom3d::Coordinate cmax = __correct(point, dist);
    Points points;
    const double dist_sq = pow(dist, 2);
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            const double d_sq = point.distance_sq(neighbor->crd());
            if (d_sq < dist_sq && d_sq > 0) {
              points.push_back(neighbor);
            }
          }
        }
    return points;
  }

  template <HasCrdAndRadius U>
  Points get_neighbors_atom_radius(const U &atom, const double dist,
                                   const double maxr) const {
    geom3d::Coordinate cmin = __correct(atom.crd(), -(dist + 2 * maxr));
    geom3d::Coordinate cmax = __correct(atom.crd(), (dist + 2 * maxr));
    Points atoms;
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            if (atom.crd().distance(neighbor->crd()) <
                atom.radius() + dist + neighbor->radius()) {
              atoms.push_back(neighbor);
            }
          }
        }
    return atoms;
  }

  template <typename U>
  Points get_neighbors_including_self(const U &point,
                                      const double &dist) const {
    geom3d::Coordinate cmin = __correct(point, -dist);
    geom3d::Coordinate cmax = __correct(point, dist);
    Points points;
    const double dist_sq = pow(dist, 2);
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            const double d_sq = point.distance_sq(neighbor->crd());
            if (d_sq < dist_sq) {
              points.push_back(neighbor);
            }
          }
        }
    return points;
  }

  template <typename U>
  Points get_neighbors_within_tolerance(const U &point, const double &dist,
                                        const double &tol) const {
    return get_neighbors_within_tolerance_asymmetric(point, dist, tol, tol);
  }

  template <typename U>
  Points
  get_neighbors_within_tolerance_asymmetric(const U &point, const double &dist,
                                            const double &lower_tol,
                                            const double &upper_tol) const {
    geom3d::Coordinate cmin = __correct(point, -(dist + upper_tol));
    geom3d::Coordinate cmax = __correct(point, dist + upper_tol);
    Points points;
    const double dist_sq_max = pow(dist + upper_tol, 2);
    const double dist_sq_min =
        dist > lower_tol ? pow(dist - lower_tol, 2) : 0.0;
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            dbgmsg("neighbor = " << neighbor->crd());
            const double d_sq = point.distance_sq(neighbor->crd());
            if (d_sq < dist_sq_max && d_sq > dist_sq_min) {
              points.push_back(neighbor);
            }
          }
        }
    return points;
  }

  template <typename U>
  Points get_closest_including_self(const U &point, const double &tol) const {

    Points points;
    Points neighbors = get_neighbors_including_self(point, tol);

    if (!neighbors.empty()) {

      double min_d = std::numeric_limits<double>::max();
      T *closest = nullptr;

      for (auto &neighbor : neighbors) {

        const double d = point.distance_sq(neighbor->crd());

        if (d < min_d) {
          min_d = d;
          closest = neighbor;
        }
      }

      points.push_back(closest);
    }
    return points;
  }

  template <typename U>
  bool has_neighbor_within(const U &point, const double &dist) const {
    geom3d::Coordinate cmin = __correct(point, -dist);
    geom3d::Coordinate cmax = __correct(point, dist);
    const double dist_sq = pow(dist, 2);
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            const double d_sq = point.distance_sq(neighbor->crd());
            if (d_sq < dist_sq && d_sq > 0) {
              return true;
            }
          }
        }
    return false;
  }

  template <typename U>
  Points get_sorted_neighbors(const U &point, const double &dist) const {

    Points points = get_neighbors(point, dist);
    std::map<T *, double> distances;

    for (auto &neighbor : points) {
      distances[neighbor] = point.distance_sq(neighbor->crd());
    }

    sort(points.begin(), points.end(), [&distances](T *i, T *j) {
      return (distances[i] < distances[j]);
    }); // sort in ascending order
    return points;
  }

  template <typename V>
  Points get_clashed(const V &atom, const double clash_coeff) const {
    const double dist = 3.0;
    const double vdw1 = atom.radius();
    geom3d::Coordinate cmin = __correct(atom.crd(), -dist);
    geom3d::Coordinate cmax = __correct(atom.crd(), dist);
    Points points;
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            const double d_sq = atom.crd().distance_sq(neighbor->crd());
            const double vdw2 = neighbor->radius();
            if (d_sq < pow(clash_coeff * (vdw1 + vdw2), 2) && d_sq > 0) {
              points.push_back(neighbor);
            }
          }
        }
    return points;
  }

  template <typename V>
  bool clashes(const V &atom, const double clash_coeff) const {
    const double dist = 3.0;
    const double vdw1 = atom.radius();
    geom3d::Coordinate cmin = __correct(atom.crd(), -dist);
    geom3d::Coordinate cmax = __correct(atom.crd(), dist);
    for (int i = cmin.i(); i <= cmax.i(); ++i)
      for (int j = cmin.j(); j <= cmax.j(); ++j)
        for (int k = cmin.k(); k <= cmax.k(); ++k) {
          for (auto &neighbor : storage[i][j][k]) {
            const double d_sq = atom.crd().distance_sq(neighbor->crd());
            const double vdw2 = neighbor->radius();
            if (d_sq < pow(clash_coeff * (vdw1 + vdw2), 2) && d_sq > 0) {
              return true;
            }
          }
        }
    return false;
  }

  template <typename P>
  int count_near(const P &points, const double dist,
                 const int min_close = std::numeric_limits<int>::max()) const {
    int close = 0;
    for (auto &point : points) {
      if (this->has_neighbor_within(ptr(point)->crd(), dist)) {
        if (++close > min_close) {
          return close;
        }
      }
    }
    return close;
  }

  template <typename P>
  bool is_near(const P &points, const double dist,
               const int min_close = 0) const {
    return count_near(points, dist, min_close) > min_close;
  }
};

} // namespace insilab::grid
