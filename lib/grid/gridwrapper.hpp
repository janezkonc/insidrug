#pragma once

namespace insilab::grid {

/**
 * Class GridWrapper enables wrapping various classes that lack the interface
 * required by Grid, ie., adds crd(), so that they can be used with Grid.
 *
 * @param T is type standard container of objects, eg., Molecule::PVec or
 * Point::Vec, that we wish to "griddify"
 * @param GetCrd is type function that returns coordinate of each object
 * @note see check_molib.cpp for example usage
 *
 */
template <typename P> struct GridAdapter {
  const P p;
  const geom3d::Coordinate c;
  const auto &crd() const { return c; }
  auto &orig() const { return p; }
};

template <typename T, typename GetCrd> class GridWrapper {
public:
  using P = typename T::value_type;

private:
  std::vector<GridAdapter<P>> __v;

public:
  /**
   * Wrap each object in the provided container with adapter class GridWrapper.
   *
   * @param cont eg. a vector of objects, eg. molecules
   * @param get_crd a function that calculates coordinate for each object, ie.
   * [](const P &obj) -> Point
   */
  GridWrapper(const T &cont, GetCrd get_crd) {
    for (const auto &p : cont) {
      __v.push_back({.p = p, .c = get_crd(p)});
    }
  }

  // This should be called, the class is then unusable
  auto get_wrapped() const { return std::move(this->__v); }
};

} // namespace insilab::grid
