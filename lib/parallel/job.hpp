#pragma once

#include "joblistener.hpp"
#include "result.hpp"
#include <memory>
#include <string>

namespace insilab::parallel {

/**
 * Base class for commands (jobs) that provides common interface to commands. It
 * also acts as publisher (observer design pattern) and lets listeners subscribe
 * to it, so that they know when a job is done.
 */
struct Job {

  /**
   * @return true if job is executable
   */
  virtual bool is_executable() const = 0;

  /**
   * Execute job.
   */
  virtual std::shared_ptr<Result> execute() const = 0;

  /**
   * Subscribe a listener to the job, so that listener is notified through it's
   * notify() method when job is done.
   *
   * @param listener a listener to subscribe to job
   */
  virtual void subscribe(const std::shared_ptr<JobListener> listener) {
    __listener = listener;
    __listener->increase_subscribed_job_count();
  }

  /**
   * @return job's listener (job can have one listener)
   */
  virtual std::shared_ptr<JobListener> get_listener() const {
    return __listener;
  }

  // needed so that derived classes are properly destructed
  virtual ~Job() = default;

private:
  std::shared_ptr<JobListener> __listener{};
};

} // namespace insilab::parallel
