#pragma once

#include "job.hpp"
#include "joblistener.hpp"
#include "result.hpp"
#include "threadsafequeue.hpp"
#include <condition_variable>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>

namespace insilab::parallel {

/**
 * This class enables executing jobs in parallel. Job or multiple jobs can be
 * associated with a single listener that is notified when each of its job is
 * done calculating. Upon notification, the listener gets information which job
 * is finished.
 */
class Jobs {

  struct QueueValue {
    std::shared_ptr<JobListener> listener;
    std::shared_ptr<Job> job;
    std::shared_ptr<Result> result;
  };

  mutable std::mutex __mtx{};
  ThreadsafeQueue<std::shared_ptr<Job>> __jobs; // FIFO queue
  std::vector<bool> __executing;
  ThreadsafeQueue<QueueValue> __waiting_listeners;
  bool __no_more_jobs{};
  const int __max_jobs;
  std::condition_variable jobs_cond;

  /**
   * Execute the job whose turn it is and try to notify its listener (and other
   * listeners that are waiting).
   *
   * @param job a job to be executed
   * @param thread_id identifier of the thread on which job is executed
   */
  void __execute(const std::shared_ptr<Job> job, const int thread_id) {
    this->__set_executing(thread_id, true);
    const auto result = job->execute();
    if (job->get_listener()) {
      __waiting_listeners.emplace(job->get_listener(), job, result);
      this->__notify_waiting_listeners();
    }
    this->__set_executing(thread_id, false);
  }

  /**
   * Notify waiting listeners that (one of) their job(s) is done. Listeners
   * start listening only when their start_listening() method is called, that's
   * why some listeners must wait.
   */
  void __notify_waiting_listeners() {
    std::queue<QueueValue> tmp_listeners;
    while (const auto val = __waiting_listeners.try_pop()) {
      const auto [listener, job, result] = *val;
      if (listener->is_listening()) {
        // send the job to its listener as context information
        listener->notify(job, result);
      } else {
        tmp_listeners.emplace(listener, job, result);
      }
    }
    while (!tmp_listeners.empty()) {
      const auto [listener, job, result] = tmp_listeners.front();
      __waiting_listeners.emplace(listener, job, result);
      tmp_listeners.pop();
    }
  }

  /**
   * Determines if all jobs have finished and there will be no more jobs and the
   * program should exit. First we check whether any thread is executing a job
   * at the moment. If one of them is, there is possibility it will spawn more
   * new jobs, so we have to keep alive all the threads (we are NOT done yet).
   * If no thread is executing, then, we check if there are no jobs on the
   * queue. If both conditions are true, we are done!
   */
  bool __done() const {
    const std::lock_guard guard(__mtx);
    if (!__no_more_jobs)
      return false;
    for (const auto &thread_executing : __executing) {
      if (thread_executing) {
        return false;
      }
    }
    return __jobs.size() == 0;
  }

  /**
   * Set executing flag to true/false for the givent thread to tell if a job is
   * currently executing.
   *
   * @param thread_id thread identifier
   * @param is_executing true if thread is executing a job
   */
  void __set_executing(const int thread_id, const bool is_executing) {
    const std::lock_guard guard(__mtx);
    __executing[thread_id] =
        is_executing; // no lock guard needed because each position of vector is
                      // accessed by a different thread, right ?
  }

public:
  Jobs(const int ncpu, const int max_jobs = std::numeric_limits<int>::max())
      : __executing(ncpu, false), __max_jobs(max_jobs) {}

  /**
   * Push a new job to the end of the FIFO queue.
   *
   * @param job a new job
   * @param listener a new listener (observer) for that job
   */
  void push(const std::shared_ptr<Job> job,
            const std::shared_ptr<JobListener> listener = nullptr) {
    const std::lock_guard guard(__mtx);
    __jobs.emplace(job);
    if (listener) {
      job->subscribe(listener);
    }
  }

  /**
   * Push a new job to the end of the FIFO queue if there are less than max_jobs
   * on the queue, else wait. This function should only be called from the main
   * thread that for example reads data from disk, and not from worker threads,
   * since it blocks execution.
   *
   * @param job a new job
   * @param listener a new listener (observer) for that job
   */
  void push_wait(const std::shared_ptr<Job> job,
                 const std::shared_ptr<JobListener> listener = nullptr) {
    std::unique_lock guard(__mtx);
    jobs_cond.wait(guard, [this] { return __jobs.size() <= __max_jobs; });
    guard.unlock();
    this->push(job, listener);
  }

  /**
   * Pop a job from the queue.
   *
   * @return a job from the front of the FIFO queue
   */
  std::shared_ptr<Job> pop() {
    const std::lock_guard guard(__mtx);
    const auto res = __jobs.try_pop();
    if (!res)
      return {};
    jobs_cond.notify_one();
    return *res;
  }

  /**
   * This function should be called once on each thread (out of ncpu threads) to
   * begin executing jobs on this thread.
   *
   * @param thread_id current thread identifier
   */
  void start(const int thread_id) {
    while (!this->__done()) {
      while (const auto job = this->pop()) {
        if (job->is_executable()) {
          this->__execute(job, thread_id);
        } else {
          this->push(job); // ...job should wait some more
        }
      }
    }
  }

  /**
   * This function should be called when the main thread stops placing new jobs
   * on the queue. It assures that worker threads will not exit prematurely.
   */
  void set_no_more_jobs() {
    const std::lock_guard guard(__mtx);
    __no_more_jobs = true;
  }

  /**
   * @return size of the queue (thread safe)
   */
  int size() {
    const std::lock_guard guard(__mtx);
    return __jobs.size();
  }
};

} // namespace insilab::parallel
