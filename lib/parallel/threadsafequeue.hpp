#pragma once

#include <condition_variable>
#include <queue>

namespace insilab::parallel {

/**
 * This class implements a thread-safe FIFO queue.
 */
template <typename T> class ThreadsafeQueue {
  std::queue<T> __queue;
  mutable std::mutex __mtx;
  std::condition_variable data_cond;

public:
  ThreadsafeQueue() = default;
  ThreadsafeQueue(const ThreadsafeQueue<T> &other) {
    const std::lock_guard guard(other.__mtx);
    __queue = other.__queue;
  }
  ThreadsafeQueue(ThreadsafeQueue<T> &&other) {
    const std::lock_guard lock(__mtx);
    __queue = std::move(other.__queue);
  }
  ThreadsafeQueue &operator=(const ThreadsafeQueue<T> &) = delete;
  ThreadsafeQueue &operator=(ThreadsafeQueue<T> &&) = delete;
  virtual ~ThreadsafeQueue() = default;

  void push(const T &new_value) {
    const std::lock_guard guard(__mtx);
    __queue.push(new_value);
    data_cond.notify_one();
  }

  template <class... Args> decltype(auto) emplace(Args &&...args) {
    const std::lock_guard guard(__mtx);
    __queue.emplace(std::forward<Args>(args)...);
    data_cond.notify_one();
  }

  auto size() const {
    const std::lock_guard lock(__mtx);
    return __queue.size();
  }

  auto empty() const {
    const std::lock_guard lock(__mtx);
    return __queue.empty();
  }

  void wait_and_pop(T &value) {
    std::unique_lock guard(__mtx);
    data_cond.wait(guard, [this] { return !__queue.empty(); });
    value = __queue.front();
    __queue.pop();
  }

  std::shared_ptr<T> wait_and_pop() {
    std::unique_lock guard(__mtx);
    data_cond.wait(guard, [this] { return !__queue.empty(); });
    const auto res = std::make_shared<T>(__queue.front());
    __queue.pop();
    return res;
  }

  bool try_pop(T &value) {
    const std::lock_guard lock(__mtx);
    if (__queue.empty())
      return false;
    value = __queue.front();
    __queue.pop();
    return true;
  }

  std::shared_ptr<T> try_pop() {
    const std::lock_guard lock(__mtx);
    if (__queue.empty())
      return {};
    const auto res(std::make_shared<T>(__queue.front()));
    __queue.pop();
    return res;
  }
};

} // namespace insilab::parallel
