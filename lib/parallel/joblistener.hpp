#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <vector>

namespace insilab::parallel {

struct Job;
struct Result;

/**
 * A job listener base class. Enables executing code when a job to which it is
 * subscribed to, is done. A listener can be subscribed to many jobs in which
 * case it's notify() method is called each time one of the jobs is done.
 */
class JobListener {
  std::atomic_bool __listening{false};
  std::atomic_int __subscribed_job_count{};

public:
  /**
   * This method is called when a job is done.
   *
   * @param job the job that was done
   * @param result the job's result
   */
  virtual void notify(const std::shared_ptr<Job> job = nullptr,
                      const std::shared_ptr<Result> result = nullptr) = 0;

  /**
   * Listener can be notified that a job is done only after this method is
   * called. This ensures that, in the notify() method, the number of jobs this
   * listener is subscribed to (__subscribed_job_count), is correct.
   */
  virtual void start_listening() { __listening = true; }

  /**
   * @return true if listening, notify can be called
   */
  virtual bool is_listening() const { return __listening; }

  /**
   * Increases the number of jobs this listener is subscribed to by one
   * atomically.
   *
   */
  virtual void increase_subscribed_job_count() { ++__subscribed_job_count; }

  /**
   * @return true if the last (subscribed) job has finished
   */
  virtual bool jobs_done() { return --__subscribed_job_count == 0; }

  // needed so that derived classes are properly destructed
  virtual ~JobListener() {}
};

} // namespace insilab::parallel
