#pragma once

#include <string>

namespace insilab::parallel {

/**
 * Base class for results object that a job returns when executed.
 */
struct Result {

  virtual ~Result() {}
};

} // namespace insilab::parallel
