#pragma once

#include "glib/algorithms/product.hpp"
#include "glib/graph.hpp"
#include "helper/array2d.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecules.hpp"
#include <mutex>
#include <set>
#include <thread>
#include <vector>

namespace insilab::lisica {

class Lisica {

public:
  enum class ScreenType { tanimoto, fragment };
  enum class CompareAtomTypes { sybyl, idatm, gaff, element };

private:
  std::vector<std::string>
  __set_ctypes(const molib::Atom::PVec &molecule_atoms) const;
  std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>>
  __generate_vertices(const molib::Molecule &target) const;

  helper::Array2d<bool> __generate_product_graph_3d(
      const molib::Molecule &target,
      std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> &vertices,
      const double max_allow_dist) const;

  void __parallel_fun(molib::Molecules::Parser &t);

  const molib::Atom::PVec __ref_atoms;
  std::vector<std::string> __ref_types;
  std::map<const molib::Atom *, glib::node_idx_t> __ref_idx;
  const helper::Array2d<glib::node_idx_t> __ref_distance_matrix;

  static std::mutex __mtx;

  std::size_t __ncpu = 1, __dimension = 2;
  glib::node_idx_t __max_allow_path = 1;
  double __max_allow_dist = 1.0;
  CompareAtomTypes __ctype = CompareAtomTypes::sybyl;
  ScreenType __stype = ScreenType::tanimoto;

public:
  class Screened {
    double __tanimoto;
    std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> __matches;
    const molib::Atom::PVec &__ref_atoms;
    molib::Molecule __target;
    const std::vector<std::string> __ref_types;

  public:
    Screened(const double t,
             const std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> c,
             const molib::Atom::PVec &r, const molib::Molecule g,
             const std::vector<std::string> &y)
        : __tanimoto(t), __matches(c), __ref_atoms(r), __target(g),
          __ref_types(y) {}
    double get_tanimoto() const { return __tanimoto; }
    const std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> &
    get_matches() const {
      return __matches;
    }
    const molib::Atom::PVec &get_ref_atoms() const { return __ref_atoms; }
    const molib::Molecule &get_target() const { return __target; }
    const std::vector<std::string> get_ref_types() const { return __ref_types; }
    friend std::ostream &operator<<(std::ostream &stream,
                                    const Screened &screened);
  };

  class TopScreened {
    struct tanimoto_compare {
      bool operator()(const Screened &lhs, const Screened &rhs) const {
        return lhs.get_tanimoto() > rhs.get_tanimoto();
      }
    };

    std::multiset<Screened, tanimoto_compare> top;
    std::size_t sz = 0;

  public:
    void insert(const Screened &s);

    std::set<Screened, tanimoto_compare>::iterator begin() {
      return top.begin();
    }
    std::set<Screened, tanimoto_compare>::iterator end() { return top.end(); }

    const std::set<Screened, tanimoto_compare>::iterator begin() const {
      return top.begin();
    }
    const std::set<Screened, tanimoto_compare>::iterator end() const {
      return top.end();
    }

    bool empty() { return top.empty(); }
    auto size() { return top.size(); }
    void set_size(const std::size_t sz) { this->sz = sz; }

    friend std::ostream &operator<<(std::ostream &stream,
                                    const TopScreened &top);
  };

private:
  std::vector<std::pair<double, std::string>> __tanimoto_list;
  TopScreened __top_screened;

public:
  Lisica(const molib::Atom::PVec &ref_atoms)
      : __ref_atoms(ref_atoms),
        __ref_distance_matrix(
            glib::algorithms::compute_shortest_path_matrix(ref_atoms)),
        __ref_types(__set_ctypes(ref_atoms)) {
    for (glib::node_idx_t i{0}; const auto &patom : ref_atoms) {
      __ref_idx[patom] = i++;
    }

    dbgmsg("size of ref_types = " << __ref_types.size());
  }

  Lisica(const molib::Molecule &reference) : Lisica(reference.get_atoms()) {}

  void compare(const molib::Molecule &target);
  void run(molib::Molecules::Parser &t);
  void run(const molib::Molecules &mols);

  void set_num_cpu(const std::size_t ncpu) { __ncpu = ncpu; }
  void set_num_mols_out(const std::size_t num_mols_out) {
    __top_screened.set_size(num_mols_out);
  }
  void set_dimension(const std::size_t dimension) { __dimension = dimension; }
  void set_max_allow_path(const glib::node_idx_t max_allow_path) {
    __max_allow_path = max_allow_path;
  }
  void set_max_allow_dist(const double max_allow_dist) {
    __max_allow_dist = max_allow_dist;
  }
  void set_compare_atom_types(const CompareAtomTypes ctype) {
    __ctype = ctype;
    __ref_types = __set_ctypes(__ref_atoms);
  }
  void set_screen_type(const ScreenType stype) { __stype = stype; }

  std::vector<std::pair<double, std::string>> get_tanimoto_list();
  TopScreened get_top() { return __top_screened; }
};

} // namespace insilab::lisica
