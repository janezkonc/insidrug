#include "lisica.hpp"
#include "glib/algorithms/mcqd.hpp"
#include "glib/algorithms/product.hpp"
#include "molib/io/iomanip.hpp"
#include "molib/molecules.hpp"

namespace insilab::lisica {

// init static members
std::mutex Lisica::__mtx;

std::vector<std::string>
Lisica::__set_ctypes(const molib::Atom::PVec &molecule_atoms) const {

  std::vector<std::string> molecule_types;
  dbgmsg("ctype = " << __ctype);

  switch (__ctype) {
  case CompareAtomTypes::sybyl:
    for (auto &patom : molecule_atoms)
      molecule_types.push_back(patom->sybyl_type());
    break;
  case CompareAtomTypes::idatm:
    for (auto &patom : molecule_atoms)
      molecule_types.push_back(patom->idatm_type_unmask());
    break;
  case CompareAtomTypes::gaff:
    for (auto &patom : molecule_atoms)
      molecule_types.push_back(patom->gaff_type());
    break;
  case CompareAtomTypes::element:
    for (auto &patom : molecule_atoms)
      molecule_types.push_back(helper::to_string(patom->element()));
    break;
  }

  return molecule_types;
}

std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>>
Lisica::__generate_vertices(const molib::Molecule &target) const {

  std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> vertices;
  const molib::Atom::PVec &tar_atoms = target.get_atoms();
  const std::vector<std::string> tar_types = __set_ctypes(tar_atoms);
  dbgmsg("generating vertices");
  dbgmsg("size of ref_atoms = " << __ref_atoms.size());
  dbgmsg("size of tar_atoms = " << tar_atoms.size());
  dbgmsg("size of ref_types = " << __ref_types.size());
  dbgmsg("size of tar_types = " << tar_types.size());
  for (glib::node_idx_t i = 0; i < __ref_atoms.size(); ++i) {
    for (glib::node_idx_t j = 0; j < tar_atoms.size(); ++j) {
      if (__ref_types[i] == tar_types[j]) {
        dbgmsg("matching vertex pair = " << __ref_types[i] << " "
                                         << tar_types[j]);
        vertices.push_back({i, j});
      }
    }
  }
  dbgmsg("size of vertices = " << vertices.size());

  return vertices;
}

helper::Array2d<bool> Lisica::__generate_product_graph_3d(
    const molib::Molecule &target,
    std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> &vertices,
    const double max_allow_dist) const {

  const molib::Atom::PVec &tar_atoms = target.get_atoms();
  helper::Array2d<bool> adjacency_matrix(vertices.size());

  for (glib::node_idx_t i = 0; i < vertices.size(); ++i) {
    for (glib::node_idx_t j = 0; j < vertices.size(); ++j) {
      if (vertices[i].first != vertices[j].first &&
          vertices[i].second != vertices[j].second) {

        const double dist_a = __ref_atoms[vertices[i].first]->crd().distance(
            __ref_atoms[vertices[j].first]->crd());
        const double dist_b = tar_atoms[vertices[i].second]->crd().distance(
            tar_atoms[vertices[j].second]->crd());

        const double R = fabs(dist_a - dist_b);

        if (R < max_allow_dist) {
          adjacency_matrix.set(i, j);
          adjacency_matrix.set(j, i);
        }
      }
    }
  }
  return adjacency_matrix;
}

std::vector<std::pair<double, std::string>> Lisica::get_tanimoto_list() {
  sort(__tanimoto_list.begin(), __tanimoto_list.end(),
       [](const std::pair<double, std::string> &i,
          const std::pair<double, std::string> &j) {
         return i.first > j.first;
       });
  return __tanimoto_list;
}

void Lisica::__parallel_fun(molib::Molecules::Parser &t) {

  dbgmsg("in function __parallel_fun");
  while (auto targets = t.parse()) {
    dbgmsg("after parsing targets");
    for (auto &target : targets) {
      dbgmsg("target = " << target);
      this->compare(target);
    }
  }
}

void Lisica::run(molib::Molecules::Parser &t) {
  dbgmsg("in function Lisica::run");
  std::vector<std::thread> threads;
  for (std::size_t i = 0; i < __ncpu; ++i) {
    threads.push_back(std::thread(&Lisica::__parallel_fun, this, std::ref(t)));
  }

  dbgmsg("before joining threads");
  for (auto &t : threads) {
    t.join();
  }
  dbgmsg("after joining threads");
}

void Lisica::run(const molib::Molecules &mols) {

  dbgmsg("in single-threaded function Lisica::run");

  for (const auto &target : mols) {
    this->compare(target);
  }
}

void Lisica::compare(const molib::Molecule &target) {
  try {
    helper::Array2d<bool> adjacency_matrix;
    std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> vertices;

    switch (__dimension) {
    case 2: {
      const auto &tar_types = __set_ctypes(target.get_atoms());
      std::map<const molib::Atom *, glib::node_idx_t> tar_idx;
      for (glib::node_idx_t i{0}; const auto &patom : target.get_atoms()) {
        tar_idx[patom] = i++;
      }
      std::tie(vertices, adjacency_matrix) =
          glib::algorithms::generate_product_graph_topological(
              __ref_atoms, target.get_atoms(),
              [this, &tar_types, &tar_idx](const molib::Atom &ratom,
                                           const molib::Atom &tatom) {
                return this->__ref_types[this->__ref_idx[&ratom]] ==
                       tar_types[tar_idx[&tatom]];
              },
              __ref_distance_matrix, __max_allow_path);
      break;
    }
    case 3:
      vertices = __generate_vertices(target);
      adjacency_matrix =
          __generate_product_graph_3d(target, vertices, __max_allow_dist);
      break;
    }

    const auto qmax = glib::algorithms::find_maximum_clique(adjacency_matrix);
    if (qmax.empty())
      throw helper::Error("[NOTE] No maximum clique found!");

    dbgmsg("size of qmax = " << qmax.size());
    double T;

    switch (__stype) {
    case ScreenType::tanimoto:
      T = (double)qmax.size() /
          (double)(__ref_atoms.size() + target.get_atoms().size() -
                   qmax.size());
      break;
    case ScreenType::fragment:
      T = (qmax.size() == __ref_atoms.size() ? 1.0 : 0.0);
      break; // 1.0 (yes) if reference fragment is found in target, otherwise
             // 0.0 (no)
    }
    dbgmsg("after determining T = " << T);
    std::vector<std::pair<glib::node_idx_t, glib::node_idx_t>> matches;
    for (const auto &i : qmax) {
      dbgmsg("i = " << i);
      matches.push_back(vertices[i]);
    }
    dbgmsg("after reading qmax");

    {
      std::lock_guard<std::mutex> guard(__mtx);

      __tanimoto_list.push_back({T, target.name()});
      dbgmsg("__stype = " << __stype);
      if (__stype == ScreenType::fragment) {
        if (T == 1.0) {
          __top_screened.insert(
              Lisica::Screened(T, matches, __ref_atoms, target, __ref_types));
        }
      } else {
        __top_screened.insert(
            Lisica::Screened(T, matches, __ref_atoms, target, __ref_types));
      }
      dbgmsg("size of __top_screened = " << __top_screened.size());
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << " (LiSiCA: skipping comparison)" << std::endl;
  }
  dbgmsg("out of compare");
}

std::ostream &operator<<(std::ostream &stream,
                         const Lisica::Screened &screened) {
  stream << molib::io::IOmanip::mol2 << screened.get_target()
         << molib::io::IOmanip::reset << std::endl;
  stream << "@<TRIPOS>COMMENT" << std::endl;
  stream << "_____________________________LiSiCA "
            "RESULTS________________________________"
         << std::endl;
  stream << "Tanimoto = " << screened.get_tanimoto() << std::endl;
  stream << "____________________________ Matching "
            "Atoms________________________________"
         << std::endl;
  stream << "Ref. Position\tRef. Atom\tTar. Position\tTar. Atom\tAtom Type"
         << std::endl;
  stream << "------------------------------------------------------------------"
            "---------"
         << std::endl;

  const molib::Atom::PVec &ref_atoms = screened.get_ref_atoms();
  const molib::Atom::PVec &tar_atoms = screened.get_target().get_atoms();

  const std::vector<std::string> &ref_types = screened.get_ref_types();

  dbgmsg("size of ref_atoms = " << ref_atoms.size());
  dbgmsg("size of tar_atoms = " << tar_atoms.size());
  dbgmsg("size of __ref_types = " << ref_types.size());

  for (auto &c : screened.get_matches()) {
    dbgmsg("first vertex = " << c.first << " second vertex = " << c.second);
    stream << ref_atoms[c.first]->atom_number() << "\t\t"
           << ref_atoms[c.first]->atom_name() << "\t\t"
           << tar_atoms[c.second]->atom_number() << "\t\t"
           << tar_atoms[c.second]->atom_name() << "\t\t" << ref_types[c.first]
           << std::endl;
  }
  stream << std::endl;

  return stream;
}

std::ostream &operator<<(std::ostream &stream, const Lisica::TopScreened &top) {
  for (auto &screened : top) {
    stream << screened;
  }
  return stream;
}
} // namespace insilab::lisica
