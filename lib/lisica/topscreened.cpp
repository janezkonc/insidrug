#include "lisica.hpp"

namespace insilab::lisica {

void Lisica::TopScreened::insert(const Screened &s) {
  if (sz == 0)
    return;
  dbgmsg("inserting top sz = " << sz);
  dbgmsg("size of top = " << top.size());
  if (top.size() < sz) {
    top.insert(s);
  } else {
    const Screened &last = *top.rbegin();
    if (last.get_tanimoto() < s.get_tanimoto()) {
      top.insert(s);

      if (top.size() > sz) { // remove worst
        auto it = top.end();
        --it;
        top.erase(it);
      }
    }
  }
}

} // namespace insilab::lisica
