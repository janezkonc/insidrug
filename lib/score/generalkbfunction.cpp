#include "generalkbfunction.hpp"
#include "geom3d/interpolation.hpp"
#include "helper/array1d.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "path/path.hpp"
#include "scoringf.hpp"
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <math.h>

namespace insilab::score {

std::ostream &operator<<(std::ostream &stream,
                         const std::vector<double> &energy) {
  for (int i = 0; i < energy.size(); ++i) {
    stream << i << "=" << energy[i] << " ";
  }
  return stream;
}

std::ostream &operator<<(std::ostream &stream,
                         const GeneralKbFunction::AtomPairValues &energies) {
  for (auto &kv : energies) {
    auto &atom_pair = kv.first;
    auto &energy = kv.second;
    for (int i = 0; i < energy.size(); ++i) {
      stream << helper::idatm_unmask[atom_pair.first] << " "
             << helper::idatm_unmask[atom_pair.second] << " " << i << " "
             << energy[i] << std::endl;
    }
  }
  return stream;
}

std::ostream &operator<<(std::ostream &stream, const GeneralKbFunction &score) {
  stream << "Using general KB scoring function (details below):" << std::endl;
  for (auto &kv : score.__energies) {
    auto &atom_pair = kv.first;
    auto &ene = score.__energies.at(atom_pair);
    stream << "Scoring function :" << std::endl;
    for (int i = 0; i < ene.size(); ++i) {
      stream << helper::idatm_unmask[atom_pair.first] << "\t"
             << helper::idatm_unmask[atom_pair.second] << "\t" << std::fixed
             << std::setprecision(3) << score.__get_lower_bound(i) << "\t"
             << std::fixed << std::setprecision(3) << ene[i] << std::endl;
    }
  }
  return stream;
}

helper::Array1d<double> GeneralKbFunction::compute_energy(
    const geom3d::Coordinate &crd, const std::set<int> &ligand_atom_types,
    const molib::Atom::Grid &gridrec, const bool bumped) const {

  dbgmsg("computing energy");
  helper::Array1d<double> energy_sum(*ligand_atom_types.rbegin() + 1);

  for (auto &patom : gridrec.get_neighbors(
           crd, __dist_cutoff - __eps)) { // subtract eps (issue #32)
    const double dist = patom->crd().distance(crd);
    const auto &atom_1 = patom->idatm_type();
    const int index = __get_index(dist);
    for (auto &l : ligand_atom_types) {
      auto atom_pair = std::minmax(atom_1, l);
      // if (!__energies.count(atom_pair))
      //   throw helper::Error("[WHOOPS] undefined idatm types [" +
      //                       helper::idatm_unmask[atom_pair.first] + "," +
      //                       helper::idatm_unmask[atom_pair.second] +
      //                       "] for atom pair in __energies");
      // if (index >= __energies.at(atom_pair).size()) {
      //   throw helper::Error("[WHOOPS] undefined index " +
      //                       helper::to_string(index) +
      //                       " for distance = " + helper::to_string(dist) +
      //                       " in __energies of size = " +
      //                       helper::to_string(__energies.at(atom_pair).size()));
      // }
      energy_sum.data[l] += __energies.at(atom_pair).at(index);
    }
  }
  dbgmsg("out of compute energy energy_sum = " << energy_sum);
  return energy_sum;
}

double
GeneralKbFunction::non_bonded_energy(const Atom::PVec &atoms,
                                     const geom3d::Point::Vec &crds,
                                     const molib::Atom::Grid &gridrec) const {

  double energy_sum = 0.0;

  for (int i = 0; i < atoms.size(); ++i) {
    const Atom &atom2 = *atoms[i];
    const geom3d::Coordinate &atom2_crd = crds[i];
    const auto &atom_2 = atom2.idatm_type();
    dbgmsg("ligand atom = " << atom2.atom_number()
                            << " crd= " << atom2_crd.pdb());
    for (auto &atom1 : gridrec.get_neighbors(
             atom2_crd, __dist_cutoff - __eps)) { // subtract eps (issue #32)
      const double dist = atom1->crd().distance(atom2_crd);
      dbgmsg("dist = " << std::setprecision(12) << dist);
      dbgmsg("dist_sq = " << std::setprecision(12)
                          << atom1->crd().distance_sq(atom2_crd));
      const auto &atom_1 = atom1->idatm_type();
      const pair_of_ints atom_pair = std::minmax(atom_1, atom_2);
      const int idx = __get_index(dist);
      energy_sum += __energies.at(atom_pair).at(idx);
#ifndef NDEBUG
      dbgmsg("ligand atom = "
             << atom2.atom_number() << " crd= " << atom2_crd.pdb()
             << "protein atom = " << atom1->atom_number()
             << " crd= " << atom1->crd().pdb() << " dist= " << dist
             << " atom_pair={" << helper::idatm_unmask[atom_pair.first] << ","
             << helper::idatm_unmask[atom_pair.second] << "}"
             << " lower_bound= " << __get_lower_bound(idx)
             << " energy_sum=" << energy_sum);
#endif
    }
  }
  dbgmsg("exiting non_bonded_energy");
  return energy_sum;
}

GeneralKbFunction &GeneralKbFunction::clear() {
  // clear old from previous fragment
  __energies.clear();
  __prot_lig_pairs.clear();
  return *this;
}

GeneralKbFunction &GeneralKbFunction::__compile_scoring_function() {

  std::clog << "Compiling scoring function...\n";
  auto energy_function = __ref_state == "mean"
                             ? mem_fn(&GeneralKbFunction::__energy_mean)
                             : mem_fn(&GeneralKbFunction::__energy_cumulative);

  for (auto &el1 : __gij_of_r_numerator) {
    const pair_of_ints &atom_pair = el1.first;
    const std::vector<double> &gij_of_r_vals = el1.second;
    const double w1 =
        Element(helper::idatm_element_unmask[atom_pair.first]).vdw_radius();
    const double w2 =
        Element(helper::idatm_element_unmask[atom_pair.second]).vdw_radius();
    const double vdW_sum = ((w1 > 0 && w2 > 0) ? w1 + w2 : 4.500);
    const int repulsion_idx = __get_index(vdW_sum - 0.6);
    const std::string idatm_type1 = helper::idatm_unmask[atom_pair.first];
    const std::string idatm_type2 = helper::idatm_unmask[atom_pair.second];
    dbgmsg("atom1 = " << idatm_type1 << " atom2 = " << idatm_type2
                      << " vdW_sum = " << vdW_sum
                      << " repulsion index (below is forbidden area) = "
                      << repulsion_idx);
    std::vector<double> energy(gij_of_r_vals.size(),
                               std::numeric_limits<double>::lowest());
    for (int i = 0; i < gij_of_r_vals.size(); ++i) {
      const double lower_bound = __get_lower_bound(i);
      const double &gij_of_r_numerator = gij_of_r_vals[i];
      dbgmsg("lower bound for atom_pair "
             << idatm_type1 << " " << idatm_type2 << " " << lower_bound
             << " gij_of_r_numerator = " << gij_of_r_numerator
             << " __sum_gij_of_r_numerator[atom_pair] = "
             << __sum_gij_of_r_numerator[atom_pair]);

      if (__sum_gij_of_r_numerator[atom_pair] < __eps) {
        energy[i] = 0;
      } else if (gij_of_r_numerator < __eps && (i + 1 < repulsion_idx)) {
        energy[i] = 5.0;
      } else if (gij_of_r_numerator < __eps && (i + 1 >= repulsion_idx)) {
        energy[i] = 0;
      } else {
        energy[i] = energy_function(*this, atom_pair, lower_bound);
      }
    }

    dbgmsg("raw energies for scoring : " << std::endl << energy);

    if (idatm_type1 == "H" || idatm_type1 == "HC" || idatm_type2 == "H" ||
        idatm_type2 == "HC") {
      energy.assign(energy.size(), 0);
    }
    __energies[atom_pair] = energy;
  }
  dbgmsg("out of loop");
  return *this;
}

GeneralKbFunction &GeneralKbFunction::__define_composition() {

  dbgmsg("receptor_idatm_types.size() = " << __receptor_idatm_types.size());
  dbgmsg("ligand_idatm_types.size() = " << __ligand_idatm_types.size());

  if (__comp == "reduced") {
    for (auto &prot_key : __receptor_idatm_types) {
      for (auto &lig_key : __ligand_idatm_types) {
        __prot_lig_pairs.insert(std::minmax(prot_key, lig_key));
      }
    }
  } else if (__comp == "complete") {
    int sz = helper::idatm_unmask.size();
    for (int i = 0; i < sz; ++i) {
      for (int j = i; j < sz; ++j) {
        __prot_lig_pairs.insert({i, j});
      }
    }
  }
  dbgmsg("__prot_lig_pairs.size() = " << __prot_lig_pairs.size());
#ifndef NDEBUG
  for (auto &i : __prot_lig_pairs) {
    dbgmsg("pairs: " << helper::idatm_unmask[i.first] << " "
                     << helper::idatm_unmask[i.second]);
  }
#endif
  return *this;
}

GeneralKbFunction &GeneralKbFunction::__process_distributions_file() {
  helper::Benchmark b;
  std::clog << "processing combined histogram (scoring function step is "
            << __step_in_file << " Angstroms)...\n";

  const bool rad_or_raw(__rad_or_raw == "normalized_frequency");

  std::stringstream ss0(data::scoringf_dat);

  dbgmsg("step in file = " << __step_in_file);

  std::string line;
  __bin_range_sum.resize(__get_index(__dist_cutoff) + 1, 0);

  std::string atom_1, atom_2;
  double lower_bound = -__step_in_file, upper_bound = 0, quantity;

  while (getline(ss0, line)) {

    std::stringstream ss(line); // dist_file is simply too big to use boost
    std::vector<std::string> ln;
    std::string temp;

    while (ss >> temp) {
      ln.push_back(temp);
    }

    if (ln.size() == 2) {
      lower_bound = -__step_in_file;
      upper_bound = 0;
      atom_1 = ln[0];
      atom_2 = ln[1];
      continue;
    } else if (ln.size() == 1) {
      lower_bound += __step_in_file;
      upper_bound += __step_in_file;
      quantity = stoi(ln[0]);
    } else {
      continue;
    }

    if (upper_bound <= __dist_cutoff) {
      pair_of_ints atom_pair = std::minmax(helper::idatm_mask.at(atom_1),
                                           helper::idatm_mask.at(atom_2));
      if (__prot_lig_pairs.count(atom_pair)) {

        double shell_volume =
            (rad_or_raw ? 1.0
                        : 4 * M_PI * pow(upper_bound, 3) / 3 -
                              4 * M_PI * pow(lower_bound, 3) / 3);
        __gij_of_r_numerator[atom_pair].push_back(quantity / shell_volume);
        __sum_gij_of_r_numerator[atom_pair] += quantity / shell_volume;
        // JANEZ : next two are for cumulative scoring function
        // (compile_cumulative_scoring_function)
        __bin_range_sum[__get_index(lower_bound)] += quantity / shell_volume;
        __total_quantity += quantity / shell_volume;
        dbgmsg(" " << atom_1 << " " << atom_2 << " " << lower_bound << " "
                   << upper_bound << " " << quantity);
      }
    }
  }

  // JANEZ : next part only needed for compile_mean_scoring_function
  __gij_of_r_bin_range_sum.resize(__get_index(__dist_cutoff) + 1, 0);
  for (auto &el1 : __gij_of_r_numerator) {
    const pair_of_ints &atom_pair = el1.first;
    if (__sum_gij_of_r_numerator[atom_pair] > 0) {
      const std::vector<double> &gij_of_r_vals = el1.second;
      for (int i = 0; i < gij_of_r_vals.size(); ++i) {
        const double &gij_of_r_value = gij_of_r_vals[i];
        __gij_of_r_bin_range_sum[i] +=
            gij_of_r_value / __sum_gij_of_r_numerator[atom_pair];
        dbgmsg("__gij_of_r_bin_range_sum["
               << __get_lower_bound(i) << "]= " << __gij_of_r_bin_range_sum[i]);
      }
    }
  }
  std::clog << "Processing distributions file took " << b.duration() << "s"
            << std::endl;
  return *this;
}

double GeneralKbFunction::__energy_mean(const pair_of_ints &atom_pair,
                                        const double &lower_bound) {
  const int idx = __get_index(lower_bound);
#ifndef NDEBUG
  if (!__gij_of_r_numerator.count(atom_pair))
    throw helper::Error("[WHOOPS] undefined __gij_of_r_numerator");
  if (idx >= __gij_of_r_numerator.at(atom_pair).size())
    throw helper::Error("[WHOOPS] undefined __gij_of_r_numerator");
  if (idx >= __gij_of_r_bin_range_sum.size())
    throw helper::Error("[WHOOPS] undefined __gij_of_r_bin_range_sum");
#endif
  double gij_of_r = __gij_of_r_numerator[atom_pair][idx] /
                    __sum_gij_of_r_numerator[atom_pair];
  dbgmsg("gij_of_r = " << gij_of_r);
  double denominator = __gij_of_r_bin_range_sum[idx] / __prot_lig_pairs.size();
  dbgmsg("denominator = " << denominator);
  double ratio = gij_of_r / denominator;
  return -log(ratio);
}

double GeneralKbFunction::__energy_cumulative(const pair_of_ints &atom_pair,
                                              const double &lower_bound) {
  const int idx = __get_index(lower_bound);
#ifndef NDEBUG
  if (!__gij_of_r_numerator.count(atom_pair))
    throw helper::Error("[WHOOPS] undefined __gij_of_r_numerator");
  if (idx >= __gij_of_r_numerator.at(atom_pair).size())
    throw helper::Error("[WHOOPS] undefined __gij_of_r_numerator");
  if (idx >= __bin_range_sum.size())
    throw helper::Error("[WHOOPS] undefined __bin_range_sum");
#endif
  double numerator = __gij_of_r_numerator[atom_pair][idx] /
                     __sum_gij_of_r_numerator[atom_pair];
  double denominator = __bin_range_sum[idx] / __total_quantity;
  double ratio = numerator / denominator;
  return -log(ratio);
}

} // namespace insilab::score
