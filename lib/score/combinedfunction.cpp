#include "combinedfunction.hpp"
#include "generalkbfunction.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include "molib/molecule.hpp"
#include "probisfunction.hpp"
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <math.h>

namespace insilab::score {

std::ostream &operator<<(std::ostream &stream, const CombinedFunction &score) {
  stream
      << "Using combined function (below are probis and generalkb functions):"
      << std::endl;
  stream << *score.__p << std::endl;
  stream << *score.__g << std::endl;
  return stream;
}

helper::Array1d<double> CombinedFunction::compute_energy(
    const geom3d::Coordinate &crd, const std::set<int> &ligand_atom_types,
    const molib::Atom::Grid &gridrec, const bool bumped) const {

  const int b = (int)!bumped; // if atom bumps with receptor then zero out
                              // general kb energy (use only probis energy)

  return __g->compute_energy(crd, ligand_atom_types, gridrec, bumped) *
             (1 - __probis_level) * b +
         __p->compute_energy(crd, ligand_atom_types, gridrec, bumped) *
             __probis_level;
}

double
CombinedFunction::non_bonded_energy(const Atom::PVec &atoms,
                                    const geom3d::Point::Vec &crds,
                                    const molib::Atom::Grid &gridrec) const {

  return __g->non_bonded_energy(atoms, crds, gridrec) * (1 - __probis_level) +
         __p->non_bonded_energy(atoms, crds, gridrec) * __probis_level;
}

} // namespace insilab::score
