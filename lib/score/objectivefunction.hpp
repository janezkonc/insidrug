#pragma once

#include "cluster/optics.hpp"
#include "generalkbfunction.hpp"
#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>

namespace insilab::score {

class ObjectiveFunction : public GeneralKbFunction {

  class InterpolationError : public helper::Error {
  public:
    InterpolationError(const std::string &msg) : Error(msg) {}
  };

  AtomPairValues __derivatives;

public:
  // use base class constructor
  using GeneralKbFunction::GeneralKbFunction;

  const std::vector<double> &get_potential(const int atom1,
                                           const int atom2) const;
  const std::vector<double> &get_derivative(const int atom1,
                                            const int atom2) const;

  ObjectiveFunction &clear();

  ObjectiveFunction &compile_objective_function();
  ObjectiveFunction &parse_objective_function(const double scale_non_bond);
  ObjectiveFunction &output_objective_function(const std::string &obj_dir);

  friend std::ostream &operator<<(std::ostream &stream, const ObjectiveFunction &score);
};

} // namespace insilab::score
