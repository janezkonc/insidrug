#include "probisfunction.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include "molib/molecule.hpp"
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <math.h>

namespace insilab::score {

std::ostream &operator<<(std::ostream &stream, const ProbisFunction &score) {
  stream << "Using ProBiS scoring function with dist cutoff "
         << score.__dist_cutoff_probis_template << " and pow of "
         << score.__pow_probis_template << std::endl;
  return stream;
}

helper::Array1d<double> ProbisFunction::compute_energy(
    const geom3d::Coordinate &crd, const std::set<int> &ligand_atom_types,
    const molib::Atom::Grid &gridrec, const bool bumped) const {

  dbgmsg("computing template probis energy");

  helper::Array1d<double> energy_sum(*ligand_atom_types.rbegin() + 1);

  auto neighbors =
      __gridprobis.get_sorted_neighbors(crd, __dist_cutoff_probis_template);

  dbgmsg("Coordinate: " << crd << " Neighbors number: " << neighbors.size());

  for (auto &lig_type : ligand_atom_types) {
    dbgmsg("~~ || Lig type: " << lig_type << "||~~\n");

    for (auto &patom : neighbors) {
      dbgmsg("idatm type of patom = " << patom->idatm_type());
      if (patom->idatm_type() == lig_type) { // type filtering

        energy_sum.data[lig_type] -=
            __get_energy(lig_type, crd.distance(patom->crd()));
        dbgmsg("idatm type of atom1 = "
               << patom->idatm_type_unmask() << " crds of atom1 = "
               << patom->crd() << " idatm type of atom2 = " << lig_type
               << " distance = " << crd.distance(patom->crd())
               << " energy_sum = " << energy_sum.data[lig_type]);

        break;
      }
    }
  }
  dbgmsg("energy sum (compute_energy) = " << energy_sum);
  return energy_sum;
}

double
ProbisFunction::non_bonded_energy(const Atom::PVec &atoms,
                                  const geom3d::Point::Vec &crds,
                                  const molib::Atom::Grid &gridrec) const {

  double energy_sum = 0.0;

  for (int i = 0; i < atoms.size(); ++i) {

    const Atom &atom2 = *atoms[i];
    const geom3d::Coordinate &atom2_crd = crds[i];
    const auto &atom_2 = atom2.idatm_type();

    dbgmsg("ligand atom = " << atom2.atom_number()
                            << " crd= " << atom2_crd.pdb());

    auto neighbors = __gridprobis.get_sorted_neighbors(
        atom2_crd, __dist_cutoff_probis_template);

    for (auto &atom1 : neighbors) { // this is a temporary cutoff
      dbgmsg("idatm type of atom1 = " << atom1->idatm_type()
                                      << " idatm type of atom2 = "
                                      << atom2.idatm_type());
      if (atom1->idatm_type() == atom2.idatm_type()) {

        energy_sum -=
            __get_energy(atom1->idatm_type(), atom1->crd().distance(atom2_crd));

        dbgmsg("idatm type of atom1 = "
               << atom1->idatm_type_unmask()
               << " idatm type of atom2 = " << atom2.idatm_type_unmask()
               << " distance = " << atom1->crd().distance(atom2_crd)
               << " energy = " << energy_sum);

        break;
      }
    }
  }
  dbgmsg("energy sum (non_bonded_energy) = " << energy_sum);
  return energy_sum;
}

double ProbisFunction::__get_energy(const int lig_type, const double d) const {
  dbgmsg("calculating probis energy for lig_type = "
         << lig_type << " distance = " << d
         << " energy = " << 1 / pow(d + 1, __pow_probis_template));

  return 1 / pow(d + 1, __pow_probis_template);
}

} // namespace insilab::score
