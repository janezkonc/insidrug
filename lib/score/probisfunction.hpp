#pragma once

#include "cluster/optics.hpp"
#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include "molib/molecules.hpp"
#include "scoringfunction.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>


namespace insilab::score {

class ProbisFunction : public Function {

  const Atom::Grid __gridprobis;
  const double __dist_cutoff_probis_template;
  const double __pow_probis_template;

  double __get_energy(const int lig_type, const double d) const;

public:
  ProbisFunction(const Molecules &template_ligands,
                 const double dist_cutoff_probis_template,
                 const double pow_probis_template)
      : __gridprobis(Atom::Grid(template_ligands.get_atoms())),
        __dist_cutoff_probis_template(dist_cutoff_probis_template),
        __pow_probis_template(pow_probis_template) {}

  double non_bonded_energy(const Atom::PVec &atoms,
                           const geom3d::Point::Vec &crds,
                           const Atom::Grid &gridrec) const;
  helper::Array1d<double> compute_energy(const geom3d::Coordinate &crd,
                                 const std::set<int> &ligand_atom_types,
                                 const Atom::Grid &gridrec,
                                 const bool bumped) const;

  friend std::ostream &operator<<(std::ostream &stream,
                                  const ProbisFunction &score);
};

} // namespace insilab::score
