#pragma once

#include "cluster/optics.hpp"
#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <typeinfo>

namespace insilab::molib {
class Molecule;
}

namespace insilab::score {

using namespace molib;

class Function { // override this class to create your own scoring function
public:
  virtual double non_bonded_energy(const Atom::PVec &atoms,
                                   const geom3d::Point::Vec &crds,
                                   const Atom::Grid &gridrec) const = 0;
  virtual helper::Array1d<double>
  compute_energy(const geom3d::Coordinate &crd,
                 const std::set<int> &ligand_atom_types,
                 const Atom::Grid &gridrec, const bool bumped) const = 0;
  friend std::ostream &operator<<(std::ostream &stream, const Function &score);
};

class ScoringFunction {
  std::unique_ptr<Function> __s;

public:
  ScoringFunction(std::unique_ptr<Function> s) : __s(std::move(s)) {}
  double non_bonded_energy(const Molecule &ligand,
                           const Atom::Grid &gridrec) const;
  double non_bonded_energy(const Atom::PVec &atoms,
                           const geom3d::Point::Vec &crds,
                           const Atom::Grid &gridrec) const;
  helper::Array1d<double> compute_energy(const geom3d::Coordinate &crd,
                                         const std::set<int> &ligand_atom_types,
                                         const Atom::Grid &gridrec,
                                         const bool bumped = false) const;
  friend std::ostream &operator<<(std::ostream &stream,
                                  const ScoringFunction &score);
};

} // namespace insilab::score
