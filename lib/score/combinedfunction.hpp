#pragma once

#include "cluster/optics.hpp"
#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include "scoringfunction.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>

namespace insilab::score {

class ProbisFunction;
class GeneralKbFunction;

class CombinedFunction : public Function {

  std::unique_ptr<ProbisFunction> __p;
  std::unique_ptr<GeneralKbFunction> __g;

  const double __probis_level;

public:
  CombinedFunction(ProbisFunction &p, GeneralKbFunction &g,
                   const double probis_level)
      : __p(std::make_unique<ProbisFunction>(std::move(p))),
        __g(std::make_unique<GeneralKbFunction>(std::move(g))),
        __probis_level(probis_level) {}

  double non_bonded_energy(const Atom::PVec &atoms,
                           const geom3d::Point::Vec &crds,
                           const Atom::Grid &gridrec) const;
  helper::Array1d<double> compute_energy(const geom3d::Coordinate &crd,
                                 const std::set<int> &ligand_atom_types,
                                 const Atom::Grid &gridrec,
                                 const bool bumped) const;

  friend std::ostream &operator<<(std::ostream &stream, const CombinedFunction &score);
};

} // namespace insilab::score
