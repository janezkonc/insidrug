#include "objectivefunction.hpp"
#include "geom3d/interpolation.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "objectivef_0_01.hpp"
#include "ommiface/ommerrors.hpp"
#include "path/path.hpp"
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <math.h>

namespace insilab::score {

std::ostream &operator<<(std::ostream &stream,
                         const ObjectiveFunction &objective) {
  for (const auto &[atom_pair, potential] : objective.__energies) {
    const auto &derivative =
        objective.get_derivative(atom_pair.first, atom_pair.second);
    stream << "ObjectiveFunction function : " << std::endl;
    for (int i = 0; i < potential.size(); ++i) {
      stream << helper::idatm_unmask[atom_pair.first] << "\t"
             << helper::idatm_unmask[atom_pair.second] << "\t" << fixed
             << std::setprecision(3) << i * objective.__step_non_bond << "\t"
             << std::fixed << std::setprecision(3) << potential[i] << "\t"
             << fixed << std::setprecision(3) << derivative[i] << std::endl;
    }
  }
  return stream;
}

ObjectiveFunction &ObjectiveFunction::clear() {
  // clear old from previous fragment
  __energies.clear();
  __derivatives.clear();

  GeneralKbFunction::clear();
  return *this;
}

ObjectiveFunction &
ObjectiveFunction::output_objective_function(const std::string &obj_dir) {
  for (auto &kv : __energies) {
    auto &atom_pair = kv.first;
    auto &ene = __energies.at(atom_pair);

    std::stringstream ss;

    const std::string idatm_type1 = helper::idatm_unmask[atom_pair.first];
    const std::string idatm_type2 = helper::idatm_unmask[atom_pair.second];

    for (int i = 0; i < ene.size(); ++i) {
      ss << std::setprecision(8) << ene[i] << std::endl;
    }
    const std::string &filename = idatm_type1 + "_" + idatm_type2 + ".txt";

    inout::file_open_put_stream(
        path::join(obj_dir, helper::to_string(__step_non_bond), filename), ss);
  }
  return *this;
}

ObjectiveFunction &
ObjectiveFunction::parse_objective_function(const double scale_non_bond) {

  std::stringstream ss0(data::objectivef_0_01_dat);
  std::string line;

  pair_of_ints atom_pair;
  std::string idatm_type1, idatm_type2;

  // knowing ALL POSSIBLE atom pairs is needed for objective function which
  // has to work between protein-ligand, protein-protein, and ligand-ligand
  // atoms
  std::set<pair_of_ints> all_atom_pairs;

  int sz = helper::idatm_unmask.size();
  for (int i = 0; i < sz; ++i) {
    for (int j = i; j < sz; ++j) {
      all_atom_pairs.insert({i, j});
    }
  }

  while (getline(ss0, line)) {

    std::stringstream ss(line);
    std::string temp;
    std::vector<std::string> ln;

    while (ss >> temp) {
      ln.push_back(temp);
    }

    if (ln.size() == 2) {

      idatm_type1 = ln[0];
      idatm_type2 = ln[1];
      dbgmsg("parsing objective function for " << idatm_type1 << " and "
                                               << idatm_type2);

      atom_pair = minmax(helper::idatm_mask.at(idatm_type1),
                         helper::idatm_mask.at(idatm_type2));

    } else if (ln.size() == 1) {

      __energies[atom_pair].push_back(stod(ln[0]));
    }
  }

  const int max_sz =
      (__energies.empty()
           ? 0
           : __energies.begin()->second.size()); // size of vector
  dbgmsg("max_sz = " << max_sz);

  // fill in the missing energies with zeroes
  for (auto &atom_pair : all_atom_pairs) {
    if (!__energies.count(atom_pair)) {

      __energies[atom_pair].assign(max_sz, 0);
    }
  }

  // calculate the derivatives
  for (auto &kv : __energies) {

    auto &atom_pair = kv.first;
    dbgmsg("after reading contents " << __energies[atom_pair].size());

    __derivatives[atom_pair] = geom3d::Interpolation::derivative(
        __energies[atom_pair], __step_non_bond);
    dbgmsg("after derivatives");

    // scale derivatives ONLY not energies and don't do it in kbforce cause here
    // is more efficient
    for (auto &dEdt : __derivatives[atom_pair]) {
      dEdt *= scale_non_bond;
    }
  }
  dbgmsg("parsed objective function");
  return *this;
}

ObjectiveFunction &ObjectiveFunction::compile_objective_function() {
  std::clog << "Compiling objective function for minimization...\n";
  auto energy_function = __ref_state == "mean"
                             ? mem_fn(&ObjectiveFunction::__energy_mean)
                             : mem_fn(&ObjectiveFunction::__energy_cumulative);

  for (auto &el1 : __gij_of_r_numerator) {
    const pair_of_ints &atom_pair = el1.first;
    const std::vector<double> &gij_of_r_vals = el1.second;
    dbgmsg(atom_pair.first << " " << atom_pair.second);
    const double w1 =
        Element(helper::idatm_element_unmask[atom_pair.first]).vdw_radius();
    const double w2 =
        Element(helper::idatm_element_unmask[atom_pair.second]).vdw_radius();
    const double vdW_sum = ((w1 > 0 && w2 > 0) ? w1 + w2 : 4.500);

    const std::string idatm_type1 = helper::idatm_unmask[atom_pair.first];
    const std::string idatm_type2 = helper::idatm_unmask[atom_pair.second];

    std::vector<double> energy(gij_of_r_vals.size(),
                               std::numeric_limits<double>::lowest());

    const int start_idx = __get_index(vdW_sum - 0.6);
    const int end_idx = __get_index(vdW_sum + 1.0);

    dbgmsg(start_idx << " " << end_idx);

    for (int i = 0; i < gij_of_r_vals.size(); ++i) {
      const double lower_bound = __get_lower_bound(i);
      dbgmsg(lower_bound);
      const double &gij_of_r_numerator = gij_of_r_vals[i];
      dbgmsg("lower bound for atom_pair "
             << idatm_type1 << " " << idatm_type2 << " " << lower_bound
             << " gij_of_r_numerator = " << gij_of_r_numerator
             << " __sum_gij_of_r_numerator[atom_pair] = "
             << __sum_gij_of_r_numerator[atom_pair]);

      if (__sum_gij_of_r_numerator[atom_pair] < __eps) {
        energy[i] = 0;
      } else if (gij_of_r_numerator < __eps && (i + 1 < start_idx)) {
        energy[i] = 5.0;
      } else if (gij_of_r_numerator < __eps && (i + 1 >= start_idx)) {
        energy[i] = 0;
      } else {
        energy[i] = energy_function(*this, atom_pair, lower_bound);
      }
    }
    dbgmsg("before interpolations");
    // dbgmsg("raw energies before interpolations : " << std::endl << energy);

    // correct for outliers only in the TRUE potential region of the potential
    for (int i = start_idx; i < energy.size() - 2; ++i) {
      if (energy[i] != 0 && energy[i] != 5) {
        int j = i + 1;
        while (j < i + 3 && j < energy.size() &&
               (energy[j] == 0 || energy[j] == 5)) {
          ++j;
        }
        if (j > i + 1 && j < energy.size()) {
          const double k0 = (energy[j] - energy[i]) / (j - i);
          for (int k = 1; k < j - i; ++k) {
            energy[i + k] = energy[i] + k0 * k;
          }
        }
      }
    }

    // locate global minimum and repulsion index in interval [vdW_sum - 0.6,
    // vdW_sum + 1.0]
    double global_min = std::numeric_limits<double>::max();
    int global_min_idx = start_idx;
    int repulsion_idx = global_min_idx;

    try {
      repulsion_idx = __get_index(
          helper::repulsion_idx.at(std::make_pair(idatm_type1, idatm_type2)));
    } catch (const std::out_of_range &) {
      try {
        repulsion_idx = __get_index(
            helper::repulsion_idx.at(std::make_pair(idatm_type2, idatm_type1)));
      } catch (const std::out_of_range &) {

        dbgmsg("de-novo calculation of repulsion idx");

        for (int i = start_idx; i < end_idx && i < energy.size(); ++i) {
          if (energy[i] != std::numeric_limits<double>::lowest()) {
            if (energy[i] < global_min) {
              global_min = energy[i];
              global_min_idx = i;

              // minimum has to have steep downward slope on the left side
              // leave the slope intact and unset everything left of the slope
              // start point
              repulsion_idx = i;
              while (repulsion_idx > 0 &&
                     energy[repulsion_idx] !=
                         std::numeric_limits<double>::lowest() &&
                     ((energy[repulsion_idx] - energy[repulsion_idx - 1]) /
                      __step_in_file) < 0.75)
                --repulsion_idx;
            }
          }
        }
      }
    }

    dbgmsg("repulsion idx (before correction) = "
           << repulsion_idx
           << " repulsion distance (below is forbidden area) = "
           << __get_lower_bound(repulsion_idx));

    // calculate slope points & minor correction to repulsion index
    std::vector<double> deriva;
    for (int i = repulsion_idx; i < repulsion_idx + 5 && i < energy.size() - 1;
         ++i) {
      double d = (energy[i + 1] - energy[i]) / __step_in_file;
      deriva.push_back(d);
    }

    // find up to 3 most negative derivatives -> slope
    std::set<int> slope;
    for (int i = 0; i < 3 && i < deriva.size(); ++i) {
      auto it = min_element(deriva.begin(), deriva.end(),
                            [](double i, double j) { return i < j; });
      if (*it < 0) { // derivative < 0
        int i0 = repulsion_idx + (it - deriva.begin());
        deriva.erase(it);
        slope.insert(i0);
        slope.insert(i0 + 1);
      }
    }

    try {

      if (slope.size() <= 1)
        throw InterpolationError("[NOTE] slope not found in data");

      repulsion_idx = *slope.begin(); // correct repulsion_idx

      dbgmsg("atom1 = " << idatm_type1 << " atom2 = " << idatm_type2
                        << " vdW_sum = " << vdW_sum
                        << " repulsion idx = " << repulsion_idx
                        << " step_non_bond = " << __step_non_bond
                        << " repulsion distance (below is forbidden area) = "
                        << __get_lower_bound(repulsion_idx)
                        << " minimum distance = "
                        << __get_lower_bound(global_min_idx)
                        << " begin slope idx = " << *slope.begin()
                        << " end slope idx = " << *slope.rbegin());

      // dbgmsg("energies before interpolations : " << std::endl << energy);
      dbgmsg("before interpolations");

      std::vector<double> dataX, dataY;
      for (int i = repulsion_idx; i < energy.size();
           ++i) { // unset everything below this value
        dataX.push_back(__get_lower_bound(i));
        dataY.push_back(energy[i]);
      }

      std::vector<double> potential =
          geom3d::Interpolation::interpolate_bspline(dataX, dataY,
                                                     __step_non_bond);

      // add repulsion term by fitting 1/x**12 function to slope points
      const double x1 = __get_lower_bound(*slope.begin());
      const double x2 = __get_lower_bound(*slope.rbegin());
      std::string datapoints("");
      int i = 0;
      for (double xi = dataX.front(); xi <= dataX.back();
           xi += __step_non_bond) {
        if (xi >= x1 && xi <= x2) {
          datapoints += helper::to_string(xi) + " " +
                        helper::to_string(potential[i]) + "\n";
        }
        ++i;
      }

      dbgmsg("x1 = " << x1);
      dbgmsg("x2 = " << x2);
      dbgmsg("datapoints = " << datapoints);

      // fit function to slope
      const auto [coeffA, coeffB, WSSR] = helper::gnuplot(x1, x2, datapoints);

      if (WSSR == std::numeric_limits<double>::max())
        throw InterpolationError(
            "[NOTE] could not fit repulsion term, zero everything");

      dbgmsg("atom1 = " << idatm_type1 << " atom2 = " << idatm_type2
                        << " coeffA = " << coeffA << " coeffB = " << coeffB
                        << " WSSR = " << WSSR);

      std::vector<double> repulsion;
      for (double xi = 0; xi < dataX.front(); xi += __step_non_bond) {
        const double yi = coeffA / pow(xi, 12) + coeffB;
        repulsion.push_back(std::isinf(yi)
                                ? 10 * coeffA / pow(xi + __step_non_bond, 12) +
                                      coeffB
                                : yi);
      }
      dbgmsg("repulsion.size() = " << repulsion.size());
#ifndef NDEBUG
      for (int i = 0; i < repulsion.size(); ++i)
        dbgmsg("i = " << i << " repulsion = " << repulsion[i]);
#endif
      // if the repulsion term comes under the potential do a linear
      // interpolation to get smooth joint
      int w = 0;
      while (!repulsion.empty() && repulsion.back() < potential.front()) {
        repulsion.pop_back();
        ++w;
      }

      dbgmsg("repulsion.size() = " << repulsion.size());

      const double rep_good = repulsion.back();
      const double k0 = (potential.front() - rep_good) / w;
      for (int k = 1; k < w; ++k) {
        repulsion.push_back(rep_good + k0 * k);
      }

      // add repulsion term before bsplined potential
      potential.insert(potential.begin(), repulsion.begin(), repulsion.end());

      __energies[atom_pair].assign(potential.begin(), potential.end());
#ifndef NDEBUG
      for (int i = 0; i < potential.size(); ++i) {
        dbgmsg("interpolated " << helper::idatm_unmask[atom_pair.first] << " "
                               << helper::idatm_unmask[atom_pair.second] << " "
                               << i * __step_non_bond
                               << " pot = " << potential[i]);
      }
#endif
    } // END of try
    catch (InterpolationError &e) {
      dbgmsg(e.what());
      const int n = (int)floor(__dist_cutoff / __step_non_bond) + 1;
      dbgmsg("n = " << n);
      __energies[atom_pair].assign(n, 0.0);
    }
  }
  dbgmsg("out of loop");
  return *this;
}

const std::vector<double> &
ObjectiveFunction::get_potential(const int atom1, const int atom2) const {
  try {
    return __energies.at({atom1, atom2});
  } catch (const std::out_of_range &) {
  }
  try {
    return __energies.at({atom2, atom1});
  } catch (const std::out_of_range &) {
  }
  throw ommiface::ParameterError("[NOTE] missing potential energy for " +
                                 helper::idatm_unmask[atom1] + "-" +
                                 helper::idatm_unmask[atom2]);
}

const std::vector<double> &
ObjectiveFunction::get_derivative(const int atom1, const int atom2) const {
  try {
    return __derivatives.at({atom1, atom2});
  } catch (const std::out_of_range &) {
  }
  try {
    return __derivatives.at({atom2, atom1});
  } catch (const std::out_of_range &) {
  }
  throw ommiface::ParameterError("[NOTE] missing derivative for " +
                                 helper::idatm_unmask[atom1] + "-" +
                                 helper::idatm_unmask[atom2]);
}

} // namespace insilab::score
