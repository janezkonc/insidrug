#include "scoringfunction.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include "molib/molecule.hpp"
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <math.h>

namespace insilab::score {

std::ostream &operator<<(std::ostream &stream, const Function &score) { return stream; }

std::ostream &operator<<(std::ostream &stream, const ScoringFunction &score) {
  stream << *score.__s;
  return stream;
}

double
ScoringFunction::non_bonded_energy(const Atom::PVec &atoms,
                                   const geom3d::Point::Vec &crds,
                                   const molib::Atom::Grid &gridrec) const {

  return __s->non_bonded_energy(atoms, crds, gridrec);
}

double
ScoringFunction::non_bonded_energy(const Molecule &ligand,
                                   const molib::Atom::Grid &gridrec) const {

  return __s->non_bonded_energy(ligand.get_atoms(), ligand.get_crds(), gridrec);
}

helper::Array1d<double> ScoringFunction::compute_energy(
    const geom3d::Coordinate &crd, const std::set<int> &ligand_atom_types,
    const molib::Atom::Grid &gridrec, const bool bumped) const {

  return __s->compute_energy(crd, ligand_atom_types, gridrec, bumped);
}

} // namespace insilab::score
