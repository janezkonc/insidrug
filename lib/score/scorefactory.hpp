#pragma once

#include "scoringfunction.hpp"
#include <memory>

namespace insilab::molib {

class Molecule;
class Molecules;

} // namespace insilab::molib

namespace insilab::score {

using namespace molib;

class Function;

std::unique_ptr<Function>
create_scoring_function(const Molecule &receptor, const Molecule &ligand,
                        const Molecules &template_ligands,
                        const double probis_level, const std::string &ref_state,
                        const std::string &comp, const std::string &rad_or_raw,
                        const double dist_cutoff, const double step_non_bond,
                        const double dist_cutoff_probis_template,
                        const double pow_probis_template);

} // namespace insilab::score
