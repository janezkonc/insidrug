#include "scorefactory.hpp"
#include "combinedfunction.hpp"
#include "generalkbfunction.hpp"
#include "molib/molecules.hpp"
#include "probisfunction.hpp"
#include "scoringfunction.hpp"
#include <memory>

namespace insilab::score {

std::unique_ptr<Function>
create_scoring_function(const Molecule &receptor, const Molecule &ligand,
                        const Molecules &template_ligands,
                        const double probis_level, const std::string &ref_state,
                        const std::string &comp, const std::string &rad_or_raw,
                        const double dist_cutoff, const double step_non_bond,
                        const double dist_cutoff_probis_template,
                        const double pow_probis_template) {
  if (probis_level == 0.0) {
    return std::make_unique<GeneralKbFunction>(GeneralKbFunction(
        receptor.get_idatm_types(), ligand.get_idatm_types(), ref_state, comp,
        rad_or_raw, dist_cutoff, step_non_bond));
  }

  if (probis_level == 1.0) {
    return std::make_unique<ProbisFunction>(ProbisFunction(
        template_ligands, dist_cutoff_probis_template, pow_probis_template));
  }

  GeneralKbFunction g(receptor.get_idatm_types(), ligand.get_idatm_types(),
                      ref_state, comp, rad_or_raw, dist_cutoff, step_non_bond);

  ProbisFunction p(template_ligands, dist_cutoff_probis_template,
                   pow_probis_template);

  return std::make_unique<CombinedFunction>(
      CombinedFunction(p, g, probis_level));
}

} // namespace insilab::score
