#pragma once

#include "cluster/optics.hpp"
#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "molib/atom.hpp"
#include "scoringfunction.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>

namespace insilab::score {

class GeneralKbFunction : public Function {
protected:
  typedef std::pair<int, int> pair_of_ints;
  typedef std::map<pair_of_ints, std::vector<double>> AtomPairValues;

  AtomPairValues __energies;
  AtomPairValues __gij_of_r_numerator;

  std::map<pair_of_ints, double> __sum_gij_of_r_numerator;
  std::vector<double> __gij_of_r_bin_range_sum, __bin_range_sum;

  double __total_quantity;
  std::set<pair_of_ints> __prot_lig_pairs;

  const double __eps;
  const std::string __ref_state, __comp, __rad_or_raw;
  const double __dist_cutoff, __step_non_bond;

  double __step_in_file;

  const std::set<int> __receptor_idatm_types, __ligand_idatm_types;

  double __energy_mean(const pair_of_ints &, const double &);
  double __energy_cumulative(const pair_of_ints &, const double &);

  int __get_index(const double d) const {
    return (int)floor((d + 0.00000000000001) / (const double)__step_in_file);
  }
  // add a very small number (even smaller than __eps) to avoid issue #32

  double __get_lower_bound(const int idx) const {
    return (double)idx * (const double)__step_in_file;
  }

  GeneralKbFunction &__define_composition();
  GeneralKbFunction &__process_distributions_file();
  GeneralKbFunction &__compile_scoring_function();

public:
  GeneralKbFunction(const std::set<int> receptor_idatm_types,
                    const std::set<int> ligand_idatm_types, const std::string &ref_state,
                    const std::string &comp, const std::string &rad_or_raw,
                    const double &dist_cutoff, const double &step_non_bond)

      : __receptor_idatm_types(receptor_idatm_types),
        __ligand_idatm_types(ligand_idatm_types), __ref_state(ref_state),
        __comp(comp), __rad_or_raw(rad_or_raw), __dist_cutoff(dist_cutoff),
        __step_non_bond(step_non_bond), __total_quantity(0), __eps(0.0000001),
        __step_in_file(0.1) {

    this->__define_composition()
        .__process_distributions_file()
        .__compile_scoring_function();
  }

  GeneralKbFunction &clear();

  double non_bonded_energy(const Atom::PVec &atoms,
                           const geom3d::Point::Vec &crds,
                           const Atom::Grid &gridrec) const;
  helper::Array1d<double> compute_energy(const geom3d::Coordinate &crd,
                                 const std::set<int> &ligand_atom_types,
                                 const Atom::Grid &gridrec,
                                 const bool bumped) const;

  friend std::ostream &operator<<(std::ostream &stream, const GeneralKbFunction &score);
  friend std::ostream &operator<<(std::ostream &stream,
                             const GeneralKbFunction::AtomPairValues &energies);
  friend std::ostream &operator<<(std::ostream &stream, const std::vector<double> &energy);
};

} // namespace insilab::score
