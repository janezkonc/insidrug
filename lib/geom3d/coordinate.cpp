#include "coordinate.hpp"
#include "matrix.hpp"
#include <algorithm>
#include <fstream>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_vector_double.h>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace insilab::geom3d {

void Coordinate::rotate_inline(const Matrix &matrix) {
  gsl_vector *vec1 = gsl_vector_alloc(3);
  gsl_vector *vec2 = gsl_vector_alloc(3);
  gsl_vector_set(vec1, 0, __x);
  gsl_vector_set(vec1, 1, __y);
  gsl_vector_set(vec1, 2, __z);
  gsl_blas_dgemv(CblasNoTrans, 1, matrix.rota(), vec1, 0, vec2);
  gsl_vector_add(vec2, matrix.trans());
  __x = gsl_vector_get(vec2, 0);
  __y = gsl_vector_get(vec2, 1);
  __z = gsl_vector_get(vec2, 2);
  gsl_vector_free(vec1);
  gsl_vector_free(vec2);
}

std::unique_ptr<Coordinate> Coordinate::rotate(const Matrix &matrix) {
  std::unique_ptr<Coordinate> c(new Coordinate(*this));
  c->rotate_inline(matrix);
  return c;
}

void Coordinate::inverse_rotate_inline(const Matrix &matrix) {
  gsl_vector *vec1 = gsl_vector_alloc(3);
  gsl_vector *vec2 = gsl_vector_alloc(3);
  gsl_vector_set(vec1, 0, __x);
  gsl_vector_set(vec1, 1, __y);
  gsl_vector_set(vec1, 2, __z);
  gsl_vector_sub(vec1, matrix.trans());
  gsl_blas_dgemv(CblasTrans, 1, matrix.rota(), vec1, 0, vec2);
  __x = gsl_vector_get(vec2, 0);
  __y = gsl_vector_get(vec2, 1);
  __z = gsl_vector_get(vec2, 2);
  gsl_vector_free(vec1);
  gsl_vector_free(vec2);
}

std::unique_ptr<Coordinate> Coordinate::inverse_rotate(const Matrix &matrix) {
  std::unique_ptr<Coordinate> c(new Coordinate(*this));
  c->inverse_rotate_inline(matrix);
  return c;
}

Coordinate::Graph Coordinate::create_graph(const Coordinate::Vec &vert,
                                           const double radius) {

  dbgmsg("Creating graph of "
         << vert.size() << " points, those that are closer than " << radius
         << " are connected");

  std::vector<std::unique_ptr<Coordinate>> vertices;

  for (const auto &v : vert) {
    vertices.push_back(std::make_unique<Coordinate>(v));
    vertices.back()->clear(); // clear edges if any
  }

  for (auto i{0uz}; i < vertices.size(); ++i) {
    auto &v1 = *vertices[i];
    for (auto j = i + 1; j < vertices.size(); ++j) {
      auto &v2 = *vertices[j];
      if (v1.distance(v2) < radius) {
        v1.add(&v2);
        v2.add(&v1);
      }
    }
  }
  return vertices;
}

} // namespace insilab::geom3d
