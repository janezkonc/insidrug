#pragma once

#include "helper/error.hpp"
#include "jsonio.hpp" // gives Json powers
#include "nlohmann/json.hpp"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_vector_double.h>
#include <iomanip>
#include <math.h>
#include <memory>
#include <sstream>
#include <string>

namespace insilab::geom3d {

// Stores rotation matrix and translation vector
class Matrix {
  gsl_matrix *__rotation_matrix;
  gsl_vector *__translation_vector;

public:
  using matrix_tuple = std::tuple<double, double, double, double>;

  Matrix()
      : __rotation_matrix{gsl_matrix_alloc(3, 3)}, __translation_vector{
                                                       gsl_vector_alloc(3)} {}

  // Initialize rotation matrix from array of size=9, zero-initialize
  // translation vector
  Matrix(const double d[9]) : Matrix{} {
    for (auto i = 0; i < 9; ++i) {
      gsl_matrix_set(__rotation_matrix, i / 3, i % 3, d[i]);
    }
    gsl_vector_set_all(__translation_vector, 0);
  }

  // Initialize from gsl matrix and vector
  Matrix(const gsl_matrix *U, const gsl_vector *t) : Matrix{} {
    gsl_matrix_memcpy(__rotation_matrix, U);
    gsl_vector_memcpy(__translation_vector, t);
  }

  // Initialize from Json matrix and vector separately (for compatibility with
  // ProBiS algorithm)
  Matrix(const nlohmann::json &rota, const nlohmann::json &trans) : Matrix{} {
    for (auto i = 0; i < 9; ++i) {
      gsl_matrix_set(__rotation_matrix, i / 3, i % 3, rota.at(i / 3).at(i % 3));
    }
    for (auto i = 0; i < 3; ++i) {
      gsl_vector_set(__translation_vector, i, trans.at(i));
    }
  }

  virtual ~Matrix() {
    gsl_matrix_free(__rotation_matrix);
    gsl_vector_free(__translation_vector);
    __rotation_matrix = nullptr;
    __translation_vector = nullptr;
  }

  // Copy constructor
  Matrix(const Matrix &other) : Matrix{} {
    gsl_matrix_memcpy(__rotation_matrix, other.rota());
    gsl_vector_memcpy(__translation_vector, other.trans());
  }

  void swap(Matrix &other) noexcept {
    std::swap(__rotation_matrix, other.__rotation_matrix);
    std::swap(__translation_vector, other.__translation_vector);
  }

  // Copy-assignment operator
  Matrix &operator=(const Matrix &other) {
    Matrix temp{other};
    swap(temp);
    return *this;
  }

  // Transpose rotation matrix, leave translation vector untouched
  Matrix transpose() const {
    Matrix temp{*this};
    gsl_matrix_transpose(temp.__rotation_matrix);
    return temp;
  }

  gsl_matrix *rota() const { return __rotation_matrix; }
  gsl_vector *trans() const { return __translation_vector; }

  // Get rotation matrix as vector of double of size = 9
  std::vector<double> serialize_rota() const {
    std::vector<double> result;
    for (int i = 0; i < 9; ++i) {
      result.push_back(gsl_matrix_get(__rotation_matrix, i / 3, i % 3));
    }
    return result;
  }

  // Get translation vector to vector of double of size = 3
  std::vector<double> serialize_trans() const {
    std::vector<double> result;
    for (int i = 0; i < 3; ++i) {
      result.push_back(gsl_vector_get(__translation_vector, i));
    }
    return result;
  }

  // Set all columns of a specified row in rotation matrix (and one column in
  // translation vector) simulataneously
  void set_row(const int &row, const matrix_tuple &t) {
    gsl_matrix_set(__rotation_matrix, row, 0, get<0>(t));
    gsl_matrix_set(__rotation_matrix, row, 1, get<1>(t));
    gsl_matrix_set(__rotation_matrix, row, 2, get<2>(t));
    gsl_vector_set(__translation_vector, row, get<3>(t));
  }

  /**
   * Sets one element in rotation matrix or translation vector.
   *
   * @param i a row in rotation matrix, if i=[0,2], or translation vector if i=3
   * @param j a column, j=[0,2], in rotation matrix or translation vector
   * @param d a double value to set
   */
  void set_element(const size_t i, const size_t j, const double d) {
    if (i < 3) {
      gsl_matrix_set(__rotation_matrix, i, j, d);
    }
    gsl_vector_set(__translation_vector, j, d);
  }

  /**
   * Gets one element from rotation matrix or translation vector.
   *
   * @param i a row in rotation matrix, if i=[0,2], or translation vector if i=3
   * @param j a column, j=[0,2], in rotation matrix or translation vector
   * @return a double value at i,j
   */
  double get_element(const size_t i, const size_t j) const {
    if (i < 3) {
      return gsl_matrix_get(__rotation_matrix, i, j);
    }
    return gsl_vector_get(__translation_vector, j);
  }

  friend std::ostream &operator<<(std::ostream &stream, const Matrix &m) {
    gsl_matrix *r = m.rota();
    gsl_vector *v = m.trans();
    stream << "[" << gsl_matrix_get(r, 0, 0) << "," << gsl_matrix_get(r, 0, 1)
           << "," << gsl_matrix_get(r, 0, 2) << "," << gsl_vector_get(v, 0)
           << "]" << std::endl;
    stream << "[" << gsl_matrix_get(r, 1, 0) << "," << gsl_matrix_get(r, 1, 1)
           << "," << gsl_matrix_get(r, 1, 2) << "," << gsl_vector_get(v, 1)
           << "]" << std::endl;
    stream << "[" << gsl_matrix_get(r, 2, 0) << "," << gsl_matrix_get(r, 2, 1)
           << "," << gsl_matrix_get(r, 2, 2) << "," << gsl_vector_get(v, 2)
           << "]" << std::endl;
    return stream;
  }
};

} // namespace insilab::geom3d
