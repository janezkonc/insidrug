#pragma once

#include "nlohmann/json.hpp"

/*********************************************************************************
 * Helper functions to_json and from_json are required by nlohmann's Json
 * library and enable to automatically convert an object to/from json. More
 * information at: https://github.com/nlohmann/json#arbitrary-types-conversions
 *********************************************************************************/

namespace insilab::geom3d {
class Coordinate;
class Matrix;

void from_json(const nlohmann::json &crd_j, Coordinate &crd);
void to_json(nlohmann::json &crd_j, const Coordinate &crd);

void from_json(const nlohmann::json &mx_j, Matrix &mx);
void to_json(nlohmann::json &mx_j, const Matrix &mx);

} // namespace insilab::geom3d
