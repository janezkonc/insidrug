#pragma once

#include "helper/error.hpp"
#include "helper/help.hpp"
#include "helper/it.hpp"
#include "jsonio.hpp" // gives Json powers
#include <cmath>
#include <iomanip>
#include <limits>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

namespace insilab::grid {
template <typename> class Grid;
} // namespace insilab::grid

namespace insilab::geom3d {

class Matrix;

enum class Axis { X = 1, Y = 2, Z = 4 };

class Coordinate : public helper::template_vector_container<Coordinate> {
  double __x, __y, __z;

public:
  using Vec = std::vector<Coordinate>; // note: cannot have std::vector<const
                                       // Coordinate>
  using PVec = std::vector<Coordinate *>;
  using CPVec = std::vector<const Coordinate *>;
  using PSet = std::set<Coordinate *>;
  using CPSet = std::set<const Coordinate *>;
  using Pair = std::pair<Coordinate, Coordinate>;
  using Grid = insilab::grid::Grid<const Coordinate>;
  using Graph = std::vector<std::unique_ptr<Coordinate>>;

  struct CompareDistanceLessThanConstant {
    bool operator()(const geom3d::Coordinate *x, const geom3d::Coordinate *y,
                    const double c) const {
      return x->distance(*y) < c;
    }
  };

  Coordinate() : __x(0), __y(0), __z(0) {}
  Coordinate(double x, double y, double z) : __x(x), __y(y), __z(z) {}

  void set_x(const double &x) { this->__x = x; }
  void set_y(const double &y) { this->__y = y; }
  void set_z(const double &z) { this->__z = z; }
  double x() const { return __x; }
  double y() const { return __y; }
  double z() const { return __z; }
  int i() const { return static_cast<int>(::floor(__x)); }
  int j() const { return static_cast<int>(::floor(__y)); }
  int k() const { return static_cast<int>(::floor(__z)); }
  const Coordinate &crd() const { return *this; }
  Coordinate floor() {
    return Coordinate(::floor(__x), ::floor(__y), ::floor(__z));
  }
  bool operator==(const Coordinate &right) const {
    // epsilon is chosen low intentionally (we only print coordinate with
    // precision of 3, so if we compare the same printed and calculated
    // coordinate (with more decimal places), they shey should be equal)
    return (helper::approx_equal(this->x(), right.x(), 0.001) &&
            helper::approx_equal(this->y(), right.y(), 0.001) &&
            helper::approx_equal(this->z(), right.z(), 0.001));
  }
  bool operator<(const Coordinate &right) const {
    return (!(*this == right)) && std::tie(this->__x, this->__y, this->__z) <
                                      std::tie(right.__x, right.__y, right.__z);
  }
  bool operator>(const Coordinate &right) const {
    return (!(*this == right)) && std::tie(this->__x, this->__y, this->__z) >
                                      std::tie(right.__x, right.__y, right.__z);
  }
  static Coordinate cross(const Coordinate &l, const Coordinate &r) {
    return Coordinate(l.y() * r.z() - l.z() * r.y(),
                      -l.x() * r.z() + l.z() * r.x(),
                      l.x() * r.y() - l.y() * r.x());
  }
  static double scalar(const Coordinate &l, const Coordinate &r) {
    return l.x() * r.x() + l.y() * r.y() + l.z() * r.z();
  }
  Coordinate operator+(const double &right) const {
    return Coordinate(__x + right, __y + right, __z + right);
  }
  Coordinate operator+(const Coordinate &right) const {
    return Coordinate(__x + right.x(), __y + right.y(), __z + right.z());
  }
  Coordinate operator-(const double &right) const {
    return Coordinate(__x - right, __y - right, __z - right);
  }
  Coordinate operator-(const Coordinate &right) const {
    return Coordinate(__x - right.x(), __y - right.y(), __z - right.z());
  }
  Coordinate operator/(const double &right) const {
    if (right == 0)
      throw helper::Error("Coordinate::operator/  division by zero\n");
    return Coordinate(__x / right, __y / right, __z / right);
  }
  Coordinate operator*(const double &right) const {
    return Coordinate(__x * right, __y * right, __z * right);
  }
  Coordinate operator-() const { return Coordinate(-x(), -y(), -z()); }
  Coordinate operator%(const Coordinate &right) const {
    // cross product as operator
    return Coordinate::cross(*this, right);
  }
  double operator*(const Coordinate &right) const {
    // scalar product as operator
    return Coordinate::scalar(*this, right);
  }
  double distance(const Coordinate &c) const {
    return std::sqrt(distance_sq(c));
  }
  double distance_sq(const Coordinate &c) const {
    return pow(__x - c.x(), 2) + pow(__y - c.y(), 2) + pow(__z - c.z(), 2);
  }

  double distance(const Coordinate &c, const Axis &a) const {
    return std::sqrt(distance_sq(c, a));
  }
  double distance_sq(const Coordinate &c, const Axis &a) const {
    return (a == geom3d::Axis::X
                ? pow(__x - c.x(), 2)
                : (a == geom3d::Axis::Y ? pow(__y - c.y(), 2)
                                        : pow(__z - c.z(), 2)));
  }

  double length() const { return this->distance(Coordinate()); }
  Coordinate norm() const { return Coordinate(*this / length()); }
  std::string pdb() const {
    std::stringstream outs;
    outs << std::fixed << std::right << std::setprecision(3) << std::setw(8)
         << __x << std::fixed << std::right << std::setprecision(3)
         << std::setw(8) << __y << std::fixed << std::right
         << std::setprecision(3) << std::setw(8) << __z;
    return outs.str();
  }
  std::string simple() const {
    std::stringstream outs;
    outs << std::fixed << std::setprecision(3) << __x << " " << std::fixed
         << std::setprecision(3) << __y << " " << std::fixed
         << std::setprecision(3) << __z;
    return outs.str();
  }
  std::string with_underscores() const {
    std::stringstream outs;
    outs << std::setprecision(3) << __x << "_" << std::setprecision(3) << __y
         << "_" << std::setprecision(3) << __z;
    return outs.str();
  }
  void rotate_inline(const Matrix &);
  std::unique_ptr<Coordinate> rotate(const Matrix &);
  void inverse_rotate_inline(const Matrix &);
  std::unique_ptr<Coordinate> inverse_rotate(const Matrix &);

  static Coordinate::Graph create_graph(const Coordinate::Vec &vert,
                                        const double radius);

  friend std::ostream &operator<<(std::ostream &stream, const Coordinate &c) {
    stream << std::fixed << std::setprecision(3) << "[" << c.x() << "," << c.y()
           << "," << c.z() << "]";
    return stream;
  }
};

// define some aliases
using Point = Coordinate;
using Vector3 = Coordinate;
using GridPoints = std::map<int, Point::Vec>;

} // namespace insilab::geom3d
