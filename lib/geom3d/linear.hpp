#pragma once

#include "coordinate.hpp"
#include <map>
#include <vector>

namespace insilab::geom3d {

double degrees(double);
double radians(double);
double angle(const Vector3 &, const Vector3 &);
double angle(const Point &, const Point &, const Point &);

/**
 * Calculate angle between two vectors a and b given the vector n which is
 * perpendicular to both a and b
 *
 * @param a,b vectors a and b
 * @param n vector n, which must be perpendicular to a and b
 * @return angle between a and b
 */
double angle_normal(const Point &a, const Point &b, const Point &n);
double dihedral(const Point &, const Point &, const Point &, const Point &);
Point line_evaluate(const Point &, const Vector3 &, const double);
double compute_rmsd_sq(const Point::Vec &crds1, const Point::Vec &crds2);
double compute_rmsd(const Point::Vec &crds1, const Point::Vec &crds2);
Point compute_geometric_center(const geom3d::Point::Vec &crds);
double compute_max_radius(const geom3d::Point &center,
                          const geom3d::Point::Vec &crds);
Point::Pair compute_minmax_point_axis(const geom3d::Point::Vec &crds,
                                      const geom3d::Axis &axis);

/**
 * Computes a modified (min instead of max) Hausdorff distance betwen two sets
 * of coordinates if hd < max_hd.
 */
double compute_hausdorff_distance(const Point::Vec &first,
                                  const Point::Vec &second,
                                  const double max_hd);

/**
 * Faster computation of the modified (min instead of max) Hausdorff distance
 * betwen two sets of coordinates if hd < max_hd.
 */
double compute_hausdorff_distance_fast(const Point::Vec &first,
                                       const Point::Vec &second,
                                       const double max_hd,
                                       const Point::Grid &g1,
                                       const Point::Grid &g2);
/**
 * Distribute n points on a sphere evenly.
 */
Point::Vec uniform_sphere(const int n);

/**
 * Calculate the intersection of two lines defined by vector point t1 and
 * directional vector e and vector point t2 and directional vector f. We have to
 * divide with the greatest tmp{1,2,3}.
 */
Point line_intersection(const geom3d::Point &t1, const geom3d::Point &e,
                        const geom3d::Point &t2, const geom3d::Point &f);
/**
 * Project vector v1 on v2.
 *
 * @return vector projection of vector v1 on vector v2
 */
Point project(const geom3d::Point &v1,
              const geom3d::Point &v2); // project v1 on v2

/**
 * Represent a set of points as a (smaller) set of points that are more than
 * some distance apart from each other.
 *
 * @param points a vector of coordinates
 * @param clus_rad a distance, by how much the resulting points should be apart
 * @return a pair, in which first element is vector of points and second element
 * is vector of radiuses, where each radius is the distance from a point to its
 * closest neighbor point
 */
std::pair<geom3d::Point::Vec, std::vector<double>>
compute_simplified_representation(const geom3d::Point::Vec &points,
                                  const double clus_rad);
std::ostream &operator<<(std::ostream &os, const geom3d::Point::Vec &points);
std::ostream &operator<<(std::ostream &os,
                         const geom3d::Point::CPSet &points);

} // namespace insilab::geom3d
