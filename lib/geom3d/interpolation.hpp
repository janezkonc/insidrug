#pragma once

#include "geom3d/linear.hpp"
#include "helper/array1d.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>

namespace insilab::geom3d {

class Interpolation {
public:
  static std::vector<double> derivative(const std::vector<double> &y, const double step);
  static std::vector<double> interpolate(const std::vector<double> &dataX,
                                    const std::vector<double> &dataY,
                                    const double step);
  static std::vector<double> interpolate_bspline(const std::vector<double> &dataX,
                                            const std::vector<double> &dataY,
                                            const double step,
                                            const std::size_t k = 4,
                                            const std::size_t ncoeff = 24);
};
} // namespace insilab::geom3d
