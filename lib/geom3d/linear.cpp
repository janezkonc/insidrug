#include "linear.hpp"
#include "cluster/greedy.hpp"
#include "grid/grid.hpp"
#include "helper/debug.hpp"
#include <algorithm>
#include <assert.h>
#include <numbers>

namespace insilab::geom3d {

double degrees(double radians) { return radians * 57.29577951308232286465; }
double radians(double degrees) { return degrees / 57.29577951308232286465; }
double angle(const Vector3 &v0, const Vector3 &v1) { // angle in radians
  double acc = Coordinate::scalar(v0, v1);
  double d0 = v0.distance(Coordinate(0, 0, 0));
  double d1 = v1.distance(Coordinate(0, 0, 0));
  if (d0 <= 0 || d1 <= 0)
    return 0;
  acc /= (d0 * d1);
  if (acc > 1)
    acc = 1;
  else if (acc < -1)
    acc = -1;
  return std::acos(acc);
}
double angle(const Point &p0, const Point &p1, const Point &p2) {
  return angle(p0 - p1, p2 - p1);
}

double angle_normal(const Point &a, const Point &b, const Point &n) {
  const Point an = a.norm();
  const Point bn = b.norm();
  const double s = an * bn;
  const Point c = an % bn;
  const double p = c * n;
  if (p < 0) {
    const double r = 2 * std::numbers::pi - std::acos(s);
    return std::isnan(r) ? 1e-02 : r;
  } else {
    const double r = std::acos(s);
    return std::isnan(r) ? 1e-02 : r;
  }
}

double dihedral(const Point &p0, const Point &p1, const Point &p2,
                const Point &p3) { // in radians
  Vector3 v10 = p1 - p0;
  Vector3 v12 = p1 - p2;
  Vector3 v23 = p2 - p3;
  Vector3 t = Coordinate::cross(v10, v12);
  Vector3 u = Coordinate::cross(v23, v12);
  Vector3 v = Coordinate::cross(u, t);
  double w = Coordinate::scalar(v, v12);
  double acc = angle(u, t);
  if (w < 0)
    acc = -acc;
  return acc;
}
Point line_evaluate(const Point &origin, const Vector3 &unit_vector,
                    const double position) {
  return origin + unit_vector * position;
}

double compute_rmsd_sq(const Point::Vec &crds1, const Point::Vec &crds2) {
  dbgmsg("calculate rmsd between two ordered sets of points");
  assert(crds1.size() == crds2.size());

  double sum_squared = 0;

  for (int i = 0; i < crds1.size(); ++i) {
    sum_squared += crds1[i].distance_sq(crds2[i]);
  }
  return sum_squared / crds1.size();
}

double compute_rmsd(const Point::Vec &crds1, const Point::Vec &crds2) {
  return std::sqrt(geom3d::compute_rmsd_sq(crds1, crds2));
}

Point compute_geometric_center(const geom3d::Point::Vec &crds) {
  geom3d::Point center;
  for (auto &crd : crds) {
    center = center + crd;
  }
  center = center / crds.size();
  return center;
}

double compute_max_radius(const geom3d::Point &center,
                          const geom3d::Point::Vec &crds) {
  double max_r = 0.0;
  for (const auto &crd : crds) {
    const double d = center.distance_sq(crd);
    max_r = std::max(d, max_r);
  }
  return std::sqrt(max_r);
}

Point::Pair compute_minmax_point_axis(const geom3d::Point::Vec &crds,
                                      const geom3d::Axis &axis) {
  assert(!crds.empty());
  Point minp(std::numeric_limits<double>::max(),
             std::numeric_limits<double>::max(),
             std::numeric_limits<double>::max()),
      maxp(std::numeric_limits<double>::lowest(),
           std::numeric_limits<double>::lowest(),
           std::numeric_limits<double>::lowest());

  for (int i = 0; i < crds.size(); ++i) {

    switch (axis) {
    case geom3d::Axis::X:
      maxp.set_x(std::max(maxp.x(), crds[i].x()));
      minp.set_x(std::min(minp.x(), crds[i].x()));
      break;
    case geom3d::Axis::Y:
      maxp.set_y(std::max(maxp.y(), crds[i].y()));
      minp.set_y(std::min(minp.y(), crds[i].y()));
      break;
    case geom3d::Axis::Z:
      maxp.set_z(std::max(maxp.z(), crds[i].z()));
      minp.set_z(std::min(minp.z(), crds[i].z()));
      break;
    }
  }

  return {minp, maxp};
}

double partial_hd_fast(const Point::Vec &first, const Point::Vec &second,
                       const double max_hd, const Point::Grid &g) {
  double max_d = std::numeric_limits<double>::lowest(),
         partial_hd = std::numeric_limits<double>::max();
  for (auto &crd : first) {
    // for this atom find closest atom in other molecule
    auto closest = g.get_closest_including_self(crd, max_hd);
    if (closest.empty())
      return std::numeric_limits<double>::max(); // closest atom couldn't be
                                                 // found, meaning that hd >=
                                                 // max_hd
    auto &closest_crd = *closest.front();
    double min_d = crd.distance_sq(closest_crd);

    if (min_d > max_d) {
      partial_hd = min_d;
      max_d = min_d;
    }
  }
  return partial_hd;
}

double partial_hd(const Point::Vec &first, const Point::Vec &second,
                  const double max_hd) {
  double max_d = std::numeric_limits<double>::lowest(),
         partial_hd = std::numeric_limits<double>::max();
  Point::Grid grid(second);
  for (auto &crd : first) {
    // for this atom find closest atom in other molecule
    auto closest = grid.get_closest_including_self(crd, max_hd);
    if (closest.empty())
      return std::numeric_limits<double>::max(); // closest atom couldn't be
                                                 // found, meaning that hd >=
                                                 // max_hd
    auto &closest_crd = *closest.front();
    double min_d = crd.distance_sq(closest_crd);

    if (min_d > max_d) {
      partial_hd = min_d;
      max_d = min_d;
    }
  }
  return partial_hd;
}

double compute_hausdorff_distance(const Point::Vec &first,
                                  const Point::Vec &second,
                                  const double max_hd) {

  return std::sqrt(std::min(partial_hd(first, second, max_hd),
                            partial_hd(second, first, max_hd)));
}

double compute_hausdorff_distance_fast(const Point::Vec &first,
                                       const Point::Vec &second,
                                       const double max_hd,
                                       const Point::Grid &g1,
                                       const Point::Grid &g2) {

  return std::sqrt(std::min(partial_hd_fast(first, second, max_hd, g2),
                            partial_hd_fast(second, first, max_hd, g1)));
}

Point::Vec uniform_sphere(const int n) {

  struct c_unique {
    double first, last, d;
    int i;
    c_unique(double f, double l, int n) {
      first = f;
      last = l;
      i = 0;
      d = (last - first) / n;
    }
    double operator()() { return first + d * (++i); }
  } UniqueNumber(1 - 1.0 / n, 1.0 / n - 1, n);

  std::vector<double> z(n);
  generate(z.begin(), z.end(), UniqueNumber);

  double golden_angle = std::numbers::pi * (3 - std::sqrt(5));

  std::vector<double> theta;
  for (int i = 0; i < n; ++i)
    theta.push_back(golden_angle * i);

  Point::Vec points;
  for (int i = 0; i < n; ++i) {
    double radius = std::sqrt(1 - z[i] * z[i]);
    points.push_back(
        Point(radius * std::cos(theta[i]), radius * std::sin(theta[i]), z[i]));
  }
  return points;
}

Point line_intersection(const geom3d::Point &t1, const geom3d::Point &e,
                        const geom3d::Point &t2, const geom3d::Point &f) {
  double lambda1, tmp1, tmp2, tmp3;
  tmp1 = e.z() * f.x() - e.x() * f.z();
  tmp2 = e.z() * f.y() - e.y() * f.z();
  tmp3 = e.y() * f.x() - e.x() * f.y();
  if (std::fabs(tmp1) > std::fabs(tmp2))
    if (std::fabs(tmp1) > std::fabs(tmp3))
      lambda1 = (f.z() * (t1.x() - t2.x()) + f.x() * (t2.z() - t1.z())) / tmp1;
    else
      lambda1 = (f.y() * (t1.x() - t2.x()) + f.x() * (t2.y() - t1.y())) / tmp3;
  else if (fabs(tmp2) > fabs(tmp3))
    lambda1 = (f.z() * (t1.y() - t2.y()) + f.y() * (t2.z() - t1.z())) / tmp2;
  else
    lambda1 = (f.y() * (t1.x() - t2.x()) + f.x() * (t2.y() - t1.y())) / tmp3;

  return t1 + e * lambda1;
}

Point project(const geom3d::Point &v1, const geom3d::Point &v2) {
  const double dv2 = v2 * v2;
  const double dv1v2 = v1 * v2;
  return v2 * (dv1v2 / dv2);
}

std::pair<geom3d::Point::Vec, std::vector<double>>
compute_simplified_representation(const geom3d::Point::Vec &points,
                                  const double clus_rad) {

  std::pair<geom3d::Point::Vec, std::vector<double>> result;

  const geom3d::Point::CPVec pcrds = [&points] {
    geom3d::Point::CPVec result;
    for (const auto &crd : points)
      result.push_back(&crd);
    return result;
  }();

  cluster::GreedyCluster<const geom3d::Point,
                         geom3d::Point::CompareDistanceLessThanConstant>
      greedy;

  const geom3d::Point::CPVec representatives =
      greedy.compute_uniform_representatives(pcrds, clus_rad);

  if (representatives.size() == 1)
    return {{*representatives.at(0)}, {clus_rad}};
  for (const auto &point : representatives) {
    // find closest point and calculate distance to this point
    double min_dist = std::numeric_limits<double>::max();
    for (const auto &point2 : representatives) {
      if (point == point2)
        continue;
      min_dist = std::min(point->distance_sq(*point2), min_dist);
    }
    result.first.push_back(*point);
    result.second.push_back(std::sqrt(min_dist));
  }
  return result;
}

std::ostream &operator<<(std::ostream &os, const geom3d::Point::Vec &points) {
  for (auto &point : points) {
    os << "ATOM      1   U  DIK     1    " << point.pdb() << std::endl;
  }
  return os;
}
std::ostream &operator<<(std::ostream &os, const geom3d::Point::CPSet &points) {
  for (const auto &point : points) {
    os << "ATOM      1   U  DIK     1    " << point->pdb() << std::endl;
  }
  return os;
}

} // namespace insilab::geom3d
