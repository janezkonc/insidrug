#pragma once

#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_vector_double.h>
#include <tuple>

namespace insilab::geom3d {
typedef std::tuple<gsl_matrix *, gsl_matrix *, gsl_vector *>
    PrincipalComponents;
PrincipalComponents pca(const gsl_matrix *data, unsigned int L);

} // namespace insilab::geom3d
