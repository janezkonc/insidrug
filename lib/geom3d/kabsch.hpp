#pragma once

#include "geom3d/linear.hpp"
#include "geom3d/matrix.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_vector_double.h>


namespace insilab::geom3d {

class Kabsch {

  gsl_matrix *__X, *__Y;
  gsl_matrix *__U;
  gsl_vector *__t;
  int __sz;
  void __gsl_vector_cross(const gsl_vector *, const gsl_vector *, gsl_vector *);
  int __kabsch(unsigned int, gsl_matrix *, gsl_matrix *, gsl_matrix *,
               gsl_vector *, double *);
  static const double __NORM_EPS;

  void __resize(const int sz);
  void __clear();

  void __add_vertices(const geom3d::Point::Vec &crds1,
                      const geom3d::Point::Vec &crds2);
  void __add_vertex(const geom3d::Coordinate &c, const geom3d::Coordinate &d,
                    const int i);

public:
  Kabsch() : __sz(0), __X(nullptr), __Y(nullptr), __U(nullptr), __t(nullptr) {}
  ~Kabsch() { __clear(); }

  geom3d::Point::Vec superimpose(const geom3d::Point::Vec &crds1,
                                 const geom3d::Point::Vec &crds2);
  geom3d::Matrix get_rotational_matrix(const geom3d::Point::Vec &crds1,
                                       const geom3d::Point::Vec &crds2);
};

} // namespace insilab::geom3d
