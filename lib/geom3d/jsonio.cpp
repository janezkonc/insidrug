#include "jsonio.hpp"
#include "coordinate.hpp"
#include "helper/help.hpp"
#include "matrix.hpp"
#include "nlohmann/json.hpp"

namespace insilab::geom3d {

void from_json(const nlohmann::json &crd_j, Coordinate &crd) {
  crd.set_x(crd_j.at(0));
  crd.set_y(crd_j.at(1));
  crd.set_z(crd_j.at(2));
}

/// Round coordinate to three decimal places by default.
void to_json(nlohmann::json &crd_j, const Coordinate &crd) {
  crd_j = nlohmann::json::array({
      std::stod(helper::dtos(crd.x())),
      std::stod(helper::dtos(crd.y())),
      std::stod(helper::dtos(crd.z())),
  });
}

void from_json(const nlohmann::json &mx_j, Matrix &mx) {
  for (auto i = 0; i < 12; ++i) {
    mx.set_element(i / 3, i % 3, mx_j.at(i / 3).at(i % 3));
  }
}

void to_json(nlohmann::json &mx_j, const Matrix &mx) {
  mx_j = nlohmann::json::array({nlohmann::json::array({0.0, 0.0, 0.0}),
                                nlohmann::json::array({0.0, 0.0, 0.0}),
                                nlohmann::json::array({0.0, 0.0, 0.0}),
                                nlohmann::json::array({0.0, 0.0, 0.0})});
  for (auto i = 0; i < 12; ++i) {
    mx_j.at(i / 3).at(i % 3) = mx.get_element(i / 3, i % 3);
  }
}

} // namespace insilab::geom3d
