#include "seeddockjob.hpp"
#include "decorators.hpp"
#include "docker/conformations.hpp"
#include "docker/dock.hpp"
#include "docker/gpoints.hpp"
#include "helper/md5.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "program/argumentparser.hpp"
#include "score/scorefactory.hpp"

namespace fs = std::filesystem;

namespace insilab::probisdock {

bool SeedDockJob::is_executable() const {
  // std::cout << "testing if seeddockjob is executable" << std::endl;
  // std::cout << "template ligands = " <<
  // __params.p_template_ligands->is_ready()
  //           << std::endl;
  // std::cout << "receptor = " << __params.p_receptor->is_ready() << std::endl;
  return __params.p_template_ligands->is_ready() &&
         __params.p_receptor->is_ready();
}

/**
 * Dock each seed fragment by moving it over the binding site grid and filter
 * docked poses that clash with the receptor (in the future we could give
 * more/less weight to more/less frequent seeds).
 *
 */
std::shared_ptr<Result> SeedDockJob::execute() const {
  using namespace score;
  using namespace docker;
  try {
    std::clog << "Docking seed " << __params.fragment.get_seed_id()
              << " to receptor " << __params.p_receptor->name() << std::flush
              << std::endl;
    // create a seed molecule and erase its atoms' properties since these make
    // subsequent graph matching incorrect
    const auto seed = Molecule(__params.fragment.get_all()).erase_properties();

    // scoring function is individually compiled for each docked
    // __params.fragment, such that it reflects its atom composition
    auto sf = create_scoring_function(
        *__params.p_receptor, seed, *__params.p_template_ligands,
        __params.args.get<double>("probis_level"),
        __params.args.get<std::string>("ref_state"),
        __params.args.get<std::string>("comp"),
        __params.args.get<std::string>("rad_or_raw"),
        __params.args.get<double>("dist_cutoff"),
        __params.args.get<double>("step_non_bond"),
        __params.args.get<double>("dist_cutoff_probis_template"),
        __params.args.get<double>("pow_probis_template"));
    ScoringFunction score(std::move(sf));

    // create gridpoints for all binding site centroids
    Gpoints gpoints;
    gpoints.identify_gridpoints(
        *__params.p_receptor, *__params.p_template_ligands,
        *__params.p_centroids, score, seed.get_idatm_types(),
        __params.args.get<double>("grid_spacing"),
        __params.args.get<double>("dist_cutoff"),
        __params.args.get<double>("excluded_radius"),
        __params.args.get<double>("max_interatomic_distance"),
        __params.args.get<double>("dist_cutoff_probis_template"));

    // compute all conformations of this seed with the center atom fixed on
    // coordinate origin
    Conformations conf(seed, __params.args.get<double>("conf_spin"),
                       __params.args.get<int>("num_univec"),
                       __params.args.get<double>("clus_rad"),
                       __params.args.get<double>("grid_spacing"),
                       __params.args.get<double>("max_frag_radius"));
    // dock this seed's conformations to the entire grid by moving them over all
    // gridpoints and probe where they clash with the receptor:
    // cluster docked conformations based on rmsd and energy and output only
    // best-scored cluster representatives
    Dock dock(gpoints, conf, seed, __params.args.get<double>("clus_rad"));
    const auto docked_seeds = dock.run().get_docked();

    // filter the 'top_percent' of docked_seeds ranked by score
    const int sz =
        (int)(docked_seeds.size() * __params.args.get<double>("top_percent"));
    Molecules top_seeds;
    for (int i = 0; i < sz && i < docked_seeds.size(); ++i) {
      top_seeds.add(docked_seeds[i]);
    }
    if (__params.args.get<bool>("debug")) {
      top_seeds.write(
          fs::temp_directory_path() /
          fs::path(helper::MD5::md5sum(*__params.p_receptor) + "_" +
                   helper::MD5::md5sum(__params.fragment.get_seed_id()) +
                   ".json.gz"));
    }
    // insert docked seeds into cache (by copying)
    for (const auto &top_seed : top_seeds) {
      __params.p_docked_seed_poses->add_thread_safe(top_seed);
    }
    __params.p_docked_seed_poses->set_ready(true);

  } catch (const std::exception &e) {
    std::cerr << "[NOTE] Skipping docking of seed "
              << __params.fragment.get_seed_id() << " to receptor "
              << __params.p_receptor->get_id() << " due to : " << e.what()
              << std::endl;
  }
  return nullptr;
}

} // namespace insilab::probisdock
