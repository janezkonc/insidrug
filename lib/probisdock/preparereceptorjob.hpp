#pragma once

#include "parallel/jobs.hpp"
#include <memory>
#include <string>

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::probisdock {

using namespace parallel;
using namespace program;

class MoleculeDecorator;

struct PrepareReceptorJob : Job {
  struct Params {
    const std::shared_ptr<MoleculeDecorator> p_receptor;
    const ArgumentParser &args;
  };

private:
  const Params __params;

public:
  PrepareReceptorJob(const Params &params) : __params(params) {}

  bool is_executable() const override { return true; }
  std::shared_ptr<Result> execute() const override;
};

} // namespace insilab::probisdock
