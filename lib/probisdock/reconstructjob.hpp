#pragma once

#include "cachedock.hpp"
#include "decorators.hpp"
#include "helper/cache.hpp"
#include "molib/algorithms/fragmenter.hpp"
#include "parallel/jobs.hpp"
#include <memory>
#include <string>

namespace insilab::score {
class ObjectiveFunction;
}

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::ommiface {
class ForceField;
}

namespace insilab::probisdock {

using namespace parallel;
using namespace molib;
using namespace ommiface;
using namespace program;

class MoleculeDecorator;
class MoleculesDecorator;

struct ReconstructJob : Job {

  struct Params {
    std::shared_ptr<Molecule> p_ligand;
    std::shared_ptr<MoleculeDecorator> p_receptor;
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    std::shared_ptr<nlohmann::json> p_centroids;
    const std::vector<algorithms::Fragment> fragments{};
    std::map<DockedSeedKey, DockedSeedVal> ligand_docked_seeds{};
    ForceField &ffield;
    const score::ObjectiveFunction &objective;
    Jobs &jobs;
    const ArgumentParser &args;
    helper::Cache<DockedSeedKey, MoleculesDecorator> &cache;
  };

private:
  const Params __params;

public:
  ReconstructJob(const Params &params) : __params(params) {}

  bool is_executable() const override;
  std::shared_ptr<Result> execute() const override;
};

} // namespace insilab::probisdock
