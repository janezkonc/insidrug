#pragma once

#include "linker/dockedconformation.hpp"
#include "molib/molecule.hpp"
#include "parallel/jobs.hpp"
#include <memory>
#include <string>
#include <vector>

using namespace insilab::molib;

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::score {
class ScoringFunction;
}

namespace insilab::molib {
class Atom;
class Molecule;
class Molecules;
} // namespace insilab::molib

namespace insilab::ommiface {
class ForceField;
}

namespace insilab::linker {
class Linker;
class Partial;
} // namespace insilab::linker

namespace insilab::probisdock {

using namespace linker;
using namespace parallel;
using namespace program;
using namespace molib;
using namespace score;
using namespace ommiface;

class MoleculeDecorator;
class MoleculesDecorator;

struct MinimizeResult : Result {
  MinimizeResult(DockedConformation docked) : docked(std::move(docked)) {}
  DockedConformation docked;
};

struct MinimizeJob : Job {

  struct Params {
    // must have all these pointers to keep them alive!
    std::shared_ptr<Molecule> p_ligand;
    std::shared_ptr<MoleculeDecorator> p_receptor;
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    std::shared_ptr<const Partial> p_partial;
    std::shared_ptr<Linker> p_linker;
    std::shared_ptr<ForceField> p_ffield;
    std::shared_ptr<std::vector<Atom *>> p_flexible_atoms;
    std::shared_ptr<ScoringFunction> p_score;
    std::shared_ptr<std::vector<std::pair<int, Molecules>>> p_top_seeds;
    const int conf_id;
    const ArgumentParser &args;
  };

private:
  const Params __params;

public:
  MinimizeJob(const Params &params) : __params(params) {}

  bool is_executable() const override { return true; }
  std::shared_ptr<Result> execute() const override;

  const Params &get_params() const { return __params; }
};

} // namespace insilab::probisdock
