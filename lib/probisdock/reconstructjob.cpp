#include "reconstructjob.hpp"
#include "centro/centroids.hpp"
#include "decorators.hpp"
#include "linker/linker.hpp"
#include "minimizejoblistener.hpp"
#include "molib/algorithms/geometry.hpp"
#include "ommiface/forcefield.hpp"
#include "ommiface/modeler.hpp"
#include "ommiface/prepare_for_mm.hpp"
#include "score/scorefactory.hpp"

namespace insilab::probisdock {

/**
 * The ligand reconstruction job is executable if all the seed fragments of this
 * ligand have been docked already.
 */
bool ReconstructJob::is_executable() const {
  for (const auto &[_unused, val] : __params.ligand_docked_seeds) {
    if (!val->is_ready())
      return false;
  }
  return true;
}

/**
 * Reconnect the docked seeds into the original ligand by finding best-scoring
 * combinations (k-cliques) of docked seed fragments using maximum weight
 * clique algorithm.
 */
std::shared_ptr<Result> ReconstructJob::execute() const {

  using namespace linker;
  using namespace insilab::molib::algorithms;

  try {
    std::clog << "Reconnecting ligand " << __params.p_ligand->name()
              << " in receptor " << __params.p_receptor->get_id() << std::flush
              << std::endl;

    auto p_ffield = std::make_shared<ommiface::ForceField>(
        __params.ffield); // make a local copy so that we don't pollute the
                          // topology residue names

    // make a local copy of receptor
    auto p_local_receptor =
        std::make_shared<MoleculeDecorator>(*__params.p_receptor);

    // prepare receptor for molecular mechanics: histidines, N-[C-]terminals
    // bonds, disulfide bonds, main chain bonds
    ommiface::prepare_for_mm(*p_local_receptor);

    // insert topology for cofactors, but not for standard residues that are
    // already known to forcefield (e.g., amino acid residues)
    p_ffield->insert_topology(*p_local_receptor); // fixes issue #115

    // scoring function is individually compiled for each docked ligand, such
    // that it reflects its atom composition
    auto sf = create_scoring_function(
        *p_local_receptor, *__params.p_ligand, *__params.p_template_ligands,
        __params.args.get<double>("probis_level"),
        __params.args.get<std::string>("ref_state"),
        __params.args.get<std::string>("comp"),
        __params.args.get<std::string>("rad_or_raw"),
        __params.args.get<double>("dist_cutoff"),
        __params.args.get<double>("step_non_bond"),
        __params.args.get<double>("dist_cutoff_probis_template"),
        __params.args.get<double>("pow_probis_template"));

    auto p_score = std::make_shared<ScoringFunction>(std::move(sf));

    // create forcefield for small molecules (and KB non-bonded with receptor);
    // read receptor's forcefield xml file(s) into ffield
    p_ffield->insert_topology(*__params.p_ligand);

    // get top seeds for this ligand from cache (MAKE A COPY of each docked
    // seed)
    auto p_top_seeds =
        std::make_shared<std::vector<std::pair<int, Molecules>>>();
    for (const auto &[key, val] : __params.ligand_docked_seeds) {
      p_top_seeds->emplace_back(key.seed_id, *val);
      __params.cache.maybe_remove();
    }

    __params.p_ligand->erase_properties(); // required for graph matching
    for (auto &[_unused, top_seed] : *p_top_seeds) {
      top_seed.erase_properties(); // required for graph matching
      // to avoid minimization failures with initial bonded relaxation failed
      // errors
      molib::algorithms::jiggle_atoms(top_seed);
    }

    auto p_flexible_atoms = std::make_shared<std::vector<Atom *>>();

    if (!__params.args.get<bool>("only_ligand_flexible")) {
      // define flexible receptor residues around binding site centroids
      Molecule centro_molecule({.name = "centroids"});
      // we assume there is exactly one binding site
      const auto &crds = __params.p_centroids->at(0)
                             .at("data")
                             .at(0)
                             .get<geom3d::Point::Vec>();
      for (const auto &point : crds) {
        centro_molecule.add(Assembly())
            .add(Model())
            .add(Chain())
            .add(molib::Segment())
            .add(Residue())
            .add(Atom({.crd = point}));
      }
      const auto &flexible_receptor_atoms = p_local_receptor->filter<true>(
          {Key::CONTAINER, centro_molecule, Key::AROUND,
           __params.args.get<double>("flex_radius")});
      p_flexible_atoms->insert(std::end(*p_flexible_atoms),
                               std::begin(flexible_receptor_atoms),
                               std::end(flexible_receptor_atoms));
    }
    const auto ligand_atoms = __params.p_ligand->get_atoms();
    p_flexible_atoms->insert(p_flexible_atoms->end(), ligand_atoms.cbegin(),
                             ligand_atoms.cend());

    // init minimization options and constants, including ligand and receptor
    // topology
    Modeler::Options modeler_opts({
        .ffield = &*p_ffield,
        .objective = __params.objective,
        .flexible_atoms = *p_flexible_atoms,
        .fftype = __params.args.get<std::string>("fftype"),
        .dist_cutoff = __params.args.get<double>("dist_cutoff"),
        .mini_tol = __params.args.get<double>("mini_tol"),
        .max_iterations = __params.args.get<int>("max_iterations"),
        .update_freq = __params.args.get<int>("update_freq"),
        .position_tolerance = __params.args.get<double>("position_tolerance"),
        .force_tol = __params.args.get<double>("force_tol"),
        .use_constraints = false,
        .step_size_in_fs = 2.0,
    });

    // connect seeds with rotatable linkers, account for symmetry, optimize
    // seeds with appendices, minimize partial conformations between linking.
    Linker::Options linker_opts({
        .ic = __params.p_ligand->get_atoms(),
        .receptor = *p_local_receptor,
        .ligand = *__params.p_ligand,
        .fragments = __params.fragments,
        .top_seeds = *p_top_seeds,
        .gridrec = p_local_receptor->get_atoms(),
        .score = *p_score,
        .modeler_opts = modeler_opts,
        .iterative = __params.args.get<bool>("iterative"),
        .dist_cutoff = __params.args.get<double>("dist_cutoff"),
        .spin_degrees =
            geom3d::radians(__params.args.get<double>("spin_degrees") / 2),
        // spin_degrees must be divided by two due to quaternion rotation (it
        // seems to double the angles)..
        .tol_seed_dist = __params.args.get<double>("tol_seed_dist"),
        .lower_tol_seed_dist = __params.args.get<double>("lower_tol_seed_dist"),
        .upper_tol_seed_dist = __params.args.get<double>("upper_tol_seed_dist"),
        .clash_coeff = __params.args.get<double>("clash_coeff"),
        .docked_clus_rad = __params.args.get<double>("docked_clus_rad"),
        .max_allow_energy = __params.args.get<double>("max_allow_energy"),
        .max_possible_conf = __params.args.get<int>("max_possible_conf"),
        .link_iter = __params.args.get<int>("link_iter"),
        .n_cliques = __params.args.get<int>("n_cliques"),
        .max_steps = __params.args.get<int>("max_steps"),
        .k_clique_size = __params.args.get<int>("k_clique_size"),
        .max_iterations_final = __params.args.get<int>("max_iterations_final"),
    });

    auto p_linker = std::make_shared<Linker>(linker_opts);
    const Partial::Vec partial_conformations = p_linker->compute_partial();

    auto on_done = std::make_shared<MinimizeJobListener>(
        __params.args.get<bool>("output_top_scored_only"));

    // instantiate new 'minimize' jobs that will minimize ligands in receptor
    for (int conf_id = 0; conf_id < partial_conformations.size(); ++conf_id) {
      __params.jobs.push(
          std::make_shared<MinimizeJob>(MinimizeJob::Params{
              __params.p_ligand, p_local_receptor, __params.p_template_ligands,
              std::make_shared<Partial>(
                  std::move(partial_conformations.at(conf_id))),
              p_linker, p_ffield, p_flexible_atoms, p_score, p_top_seeds,
              conf_id, __params.args}),
          on_done);
    }
    on_done->start_listening();
  } catch (const std::exception &e) {
    std::cerr
        << "[NOTE] Skipping the search for partial docked poses of ligand "
        << __params.p_ligand->name() << " in receptor "
        << __params.p_receptor->get_id() << " due to : " << e.what()
        << std::endl;
  }
  return nullptr;
}

} // namespace insilab::probisdock
