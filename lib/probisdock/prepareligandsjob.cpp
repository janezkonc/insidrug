#include "prepareligandsjob.hpp"
#include "cachedock.hpp"
#include "decorators.hpp"
#include "helper/cache.hpp"
#include "helper/md5.hpp"
#include "molib/algorithms/fragmenter.hpp"
#include "parallel/jobs.hpp"
#include "program/argumentparser.hpp"
#include "reconstructjob.hpp"
#include "seeddockjob.hpp"

namespace fs = std::filesystem;

namespace insilab::probisdock {

/**
 * Prepare ligands by computing their properties, such as idatm atom types,
 * fragments, seeds, rotatable bonds etc. Compute seed id's of this ligand and
 * check if seeds weren't already docked - if a seed is in seeds_file, it means
 * it has been (or will be shortly) docked.
 */
std::shared_ptr<Result> PrepareLigandsJob::execute() const {
  std::clog << "Computing ligand types batch_size " << __params.ligands.size()
            << " first-in-batch name = " << __params.ligands.first().name()
            << std::flush << std::endl;
  for (auto &ligand : __params.ligands) {
    try {
      auto p_ligand = std::make_shared<Molecule>(std::move(ligand));
      p_ligand->compute_atom_bond_types();

      if (__params.args.get<bool>("debug")) {
        p_ligand->write(fs::temp_directory_path() /
                        fs::path(helper::MD5::md5sum(*p_ligand) + ".json.gz"));
      }

      const auto &fragments =
          molib::algorithms::compute_overlapping_rigid_segments(*p_ligand);

      // get seed ids for the ligand needed in reconstruction and update
      // popularity of ligand_docked_seeds in cache
      std::map<DockedSeedKey, DockedSeedVal> ligand_docked_seeds;
      for (const auto &fragment : fragments) {
        if (!fragment.is_seed())
          continue;
        const auto key = DockedSeedKey{fragment.get_seed_id(),
                                       __params.p_receptor->get_id()};
        ligand_docked_seeds.emplace(
            key, __params.cache.get_or_create_value_pointer(key));
      }

      for (const auto &fragment : fragments) {
        const auto key = DockedSeedKey{fragment.get_seed_id(),
                                       __params.p_receptor->get_id()};
        if (!fragment.is_seed() || ligand_docked_seeds.at(key)->is_ready() ||
            ligand_docked_seeds.at(key)->is_calculating())
          continue;
        // create a new seed dock job
        __params.jobs.push(std::make_shared<SeedDockJob>(SeedDockJob::Params{
            p_ligand, fragment, __params.p_receptor,
            __params.p_template_ligands, __params.p_centroids,
            ligand_docked_seeds.at(key), __params.args}));
      }
      // create a new ligand reconstruction job
      __params.jobs.push(
          std::make_shared<ReconstructJob>(ReconstructJob::Params{
              p_ligand, __params.p_receptor, __params.p_template_ligands,
              __params.p_centroids, fragments, ligand_docked_seeds,
              __params.ffield, __params.objective, __params.jobs, __params.args,
              __params.cache}));
    } catch (const std::exception &e) {
      std::cerr << "[NOTE] Skipping preparing ligand " << ligand.name()
                << " due to " << e.what() << std::endl;
    }
  }
  return nullptr;
}

} // namespace insilab::probisdock
