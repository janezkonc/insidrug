#pragma once

#include "linker/dockedconformation.hpp"
#include "minimizejob.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "parallel/joblistener.hpp"
#include "program/argumentparser.hpp"
#include <atomic>
#include <memory>
#include <mutex>

namespace insilab::probisdock {

using namespace molib;

class MinimizeJobListener : public JobListener {
  const bool __output_top_scored_only;
  std::shared_ptr<MinimizeResult> __top_result{};
  mutable std::mutex __mtx{};

public:
  MinimizeJobListener(const bool output_top_scored_only)
      : __output_top_scored_only(output_top_scored_only) {}

  void notify(const std::shared_ptr<Job> job,
              const std::shared_ptr<Result> result) override {
    const std::lock_guard guard(__mtx);
    if (!result) // if minimization failed, exit immediately
      return;
    const auto tmp_job = std::dynamic_pointer_cast<MinimizeJob>(job);
    const auto tmp_result = std::dynamic_pointer_cast<MinimizeResult>(
        result); // downcast to get the right type
    if (__output_top_scored_only) {
      if (!this->__top_result) {
        this->__top_result = tmp_result;
      }
      if (tmp_result->docked.get_energy() < __top_result->docked.get_energy()) {
        this->__top_result = tmp_result;
      }
      if (this->jobs_done()) {
        this->write(tmp_job, this->__top_result);
      }
    } else {
      this->write(tmp_job, tmp_result);
    }
  }

  void write(const std::shared_ptr<MinimizeJob> job,
             const std::shared_ptr<MinimizeResult> result) {

    using namespace insilab::molib::algorithms;
    
    if (job->get_params().args.get<bool>("output_docked_complexes")) {
      auto complex_atoms = result->docked.get_ligand().get_atoms();
      const auto receptor_atoms = result->docked.get_receptor().get_atoms();
      const int max_atom_number = this->compute_max_atom_number(receptor_atoms);
      // renumber ligand atoms so that they don't clash with receptor
      for (auto &patom : complex_atoms) {
        patom->set_atom_number(patom->atom_number() + max_atom_number);
      }
      const Atom::Grid g_ligand(complex_atoms);
      complex_atoms.insert(complex_atoms.end(), receptor_atoms.begin(),
                           receptor_atoms.end());
      Molecule complex(complex_atoms);
      // filter binding site (ligand + binding site residues)
      const auto filtered = complex.filter(
          {Key::BYRES, Key::ALL, Key::WITHIN,
           job->get_params().args.get<double>("receptor_trim_dist"), Key::OF,
           Key::GRID, g_ligand});
      Molecule(filtered).write(
          job->get_params().args.get<std::string>("docked_complexes_file"),
          Molecules::Writer::Options::append);
    } else {
      result->docked.get_ligand().write(
          job->get_params().args.get<std::string>("docked_complexes_file"),
          Molecules::Writer::Options::append);
    }
  }

  int compute_max_atom_number(const Atom::PVec &atoms) {
    if (atoms.empty())
      throw("[WHOOPS] Cannot get max atom number for empty set of atoms");
    return (*std::max_element(atoms.begin(), atoms.end(),
                              [](const auto &pi, const auto &pj) {
                                return pi->atom_number() < pj->atom_number();
                              }))
        ->atom_number();
  }
};

} // namespace insilab::probisdock
