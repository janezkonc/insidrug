#pragma once

#include "helper/cache.hpp"
#include "molib/molecules.hpp"
#include "probisdock/cachedock.hpp"
#include "probisdock/decorators.hpp"
#include <atomic>
#include <string>
#include <vector>

class ArgumentParser;

namespace insilab::score {
class ObjectiveFunction;
}

namespace insilab::parallel {
class Jobs;
}

namespace insilab::ommiface {
class ForceField;
}

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::probisdock {

using namespace parallel;
using namespace program;
using namespace molib;
using namespace ommiface;

/**
 * This function does all the reading from files and creates initial jobs.
 * Docking of several ligands against several receptors is supported. It is
 * executed on a single (main) thread.
 */
void reader_fun(parallel::Jobs &jobs, const program::ArgumentParser &args,
                helper::Cache<DockedSeedKey, MoleculesDecorator> &cache,
                molib::Molecules::Parser &lpdb,
                std::vector<std::pair<Molecules::Parser, std::string>> &rpdb,
                std::vector<molib::Molecules::Parser> &tpdb,
                const std::vector<std::string> &centro_files,
                ommiface::ForceField &ffield,
                const score::ObjectiveFunction &objective);

/**
 * This function holds the 'event' loop that executes the jobs. It is
 * executed in parallel on multiple threads and gets jobs from the 'reader'
 * thread asynchronously.
 */
void parallel_fun(parallel::Jobs &jobs, const int thread_id);

} // namespace insilab::probisdock
