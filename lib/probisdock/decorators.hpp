#pragma once

#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include <atomic>
#include <mutex>

using namespace insilab::molib;

namespace insilab::probisdock {

class MoleculeDecorator : public Molecule {
  std::atomic_bool __ready = false;
  const int __id;
  const std::string __filename;

public:
  MoleculeDecorator(const Molecule m, const int id, const std::string filename)
      : Molecule(std::move(m)), __id(id), __filename(filename) {}
  MoleculeDecorator(const MoleculeDecorator &o)
      : Molecule(o), __id(o.__id), __filename(o.__filename),
        __ready(static_cast<bool>(o.__ready)) {}
  void set_ready() { __ready = true; }
  bool is_ready() const { return __ready; }
  int get_id() const { return __id; }
  auto get_filename() const { return __filename; }
};

class MoleculesDecorator : public Molecules {
  mutable std::mutex __mtx{};
  std::atomic_bool __ready{false};
  std::atomic_bool __calculating{false};
  std::atomic_int __popularity{0};

public:
  MoleculesDecorator(const int popularity = 0)
      : Molecules{}, __popularity(popularity) {}

  void add_thread_safe(const Molecule &m) { this->add(m); }

  /**
   * Mark that docking of seed fragment has been done.
   *
   * @param done true if done
   */
  void set_ready(const bool done) { __ready = done; }

  /**
   * @return true if docking of this seed is done.
   */
  bool is_ready() const { return __ready; }

  /**
   * Set the seed's popularity (how many times it appears in ligands). We use
   * this for ordering docked seeds in cache (priority queue).
   *
   * @param p popularity
   */
  void set_popularity(const int p) { __popularity = p; }

  /**
   * Assure that only one job does docking of this seed by 'locking' this object
   * on the first use of this function.
   *
   * @return true on the first check, otherwise false
   */
  bool is_calculating() {
    const std::lock_guard<std::mutex> guard(__mtx);
    const bool c = __calculating;
    __calculating = true;
    return c;
  }

  /**
   * Inverted sorting with 'greater than' enables that the most 'unpopular'
   * docked seeds are popped as first out of the priority queue (the more
   * unpopular ==> the highest priority).
   */
  friend bool operator<(const MoleculesDecorator &lhs,
                        const MoleculesDecorator &rhs) {
    return lhs.__popularity > rhs.__popularity;
  }
};

} // namespace insilab::probisdock
