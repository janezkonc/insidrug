#pragma once

#include <memory>
#include <tuple>

namespace insilab::probisdock {

class MoleculesDecorator;

struct DockedSeedKey {
  const int seed_id;
  const int parser_id;
  friend bool operator<(const DockedSeedKey &lhs, const DockedSeedKey &rhs) {
    return std::tie(lhs.seed_id, lhs.parser_id) <
           std::tie(rhs.seed_id, rhs.parser_id);
  }
};

using DockedSeedVal = std::shared_ptr<MoleculesDecorator>;

} // namespace insilab::probisdock
