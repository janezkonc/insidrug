#pragma once

#include "molib/molecules.hpp"
#include "parallel/job.hpp"
#include "program/argumentparser.hpp"
#include <memory>
#include <string>

namespace program {
class ArgumentParser;
}

namespace insilab::probisdock {

using namespace parallel;
using namespace molib;
using namespace program;

class MoleculesDecorator;

struct PrepareTemplateLigandsJob : Job {
  struct Params {
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    Molecules template_ligands;
    const ArgumentParser &args;
  };

private:
  const Params __params;

public:
  PrepareTemplateLigandsJob(const Params &params) : __params(params) {}

  bool is_executable() const override { return true; }
  std::shared_ptr<Result> execute() const override;
  const Params &get_params() const { return __params; }
};

} // namespace insilab::probisdock
