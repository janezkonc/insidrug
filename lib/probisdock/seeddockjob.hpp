#pragma once

#include "cachedock.hpp"
#include "molib/algorithms/fragmenter.hpp"
#include "molib/molecule.hpp"
#include "parallel/jobs.hpp"
#include <memory>
#include <string>

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisdock {

using namespace program;
using namespace parallel;
using namespace molib;

class MoleculeDecorator;
class MoleculesDecorator;

struct SeedDockJob : Job {

  struct Params {
    std::shared_ptr<Molecule> p_ligand;
    const algorithms::Fragment fragment;
    std::shared_ptr<MoleculeDecorator> p_receptor;
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    std::shared_ptr<nlohmann::json> p_centroids;
    DockedSeedVal p_docked_seed_poses;
    const ArgumentParser &args;
  };

private:
  const Params __params;

public:
  SeedDockJob(const Params &params) : __params(params) {}

  bool is_executable() const override;
  std::shared_ptr<Result> execute() const override;
};

} // namespace insilab::probisdock
