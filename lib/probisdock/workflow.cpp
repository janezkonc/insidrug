#include "workflow.hpp"
#include "centro/centroids.hpp"
#include "decorators.hpp"
#include "helper/md5.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecules.hpp"
#include "parallel/jobs.hpp"
#include "prepareligandsjob.hpp"
#include "preparereceptorjob.hpp"
#include "preparetemplateligandsjob.hpp"
#include "program/argumentparser.hpp"
#include "templatejoblistener.hpp"
#include <memory>
#include <string>

namespace insilab::probisdock {

namespace fs = std::filesystem;

void reader_fun(Jobs &jobs, const ArgumentParser &args,
                helper::Cache<DockedSeedKey, MoleculesDecorator> &cache,
                Molecules::Parser &lpdb,
                std::vector<std::pair<Molecules::Parser, std::string>> &rpdb,
                std::vector<Molecules::Parser> &tpdb,
                const std::vector<std::string> &centro_files,
                ommiface::ForceField &ffield,
                const score::ObjectiveFunction &objective) {
  try {
    int parser_id{0}; // receptor parser index (which receptor,
                      // centroid and template ligands reader to use)
    bool reading_done{false};

    std::shared_ptr<MoleculeDecorator> p_receptor;
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    std::shared_ptr<nlohmann::json> p_centroids;

    while (!reading_done) {

      if (const auto receptors = rpdb.at(parser_id).first.parse(); receptors) {
        // read next receptor
        p_receptor = std::make_shared<MoleculeDecorator>(
            receptors.first(), parser_id, rpdb.at(parser_id).second);
        jobs.push(std::make_shared<PrepareReceptorJob>(
            PrepareReceptorJob::Params{p_receptor, args}));

        // read binding site centroids
        p_centroids = std::make_shared<nlohmann::json>(
            centro::parse_file(centro_files.at(parser_id)));

        // read next template ligands
        p_template_ligands = std::make_shared<MoleculesDecorator>();
        if (args.get<double>("probis_level") != 0.0) {
          auto on_done = std::make_shared<TemplateJobListener>();
          while (const auto template_ligands = tpdb.at(parser_id).parse()) {
            jobs.push(std::make_shared<PrepareTemplateLigandsJob>(
                          PrepareTemplateLigandsJob::Params{
                              p_template_ligands, template_ligands, args}),
                      on_done);
          }
          on_done->start_listening();
        } else {
          p_template_ligands->set_ready(true);
        }
      }

      // read next batch of ligands
      if (const auto ligands = lpdb.parse(); ligands) {
        jobs.push_wait(std::make_shared<PrepareLigandsJob>(
            PrepareLigandsJob::Params{ligands, p_receptor, p_template_ligands,
                                      p_centroids, ffield, objective, jobs,
                                      args, cache}));
      } else {
        // no more ligands to parse? increase the parser counter so that we
        // start reading the next receptor and its binding site (template
        // ligands and centroids)
        if (++parser_id == rpdb.size()) {
          reading_done = true; // no more receptors? we are done reading!
        }
        lpdb.rewind(); // start reading ligands all over again
      }
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  jobs.set_no_more_jobs();
}

void parallel_fun(parallel::Jobs &jobs, const int thread_id) {
  jobs.start(thread_id);
}

} // namespace insilab::probisdock
