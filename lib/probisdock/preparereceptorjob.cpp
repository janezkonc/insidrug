#include "preparereceptorjob.hpp"
#include "decorators.hpp"
#include "helper/md5.hpp"
#include "program/argumentparser.hpp"

namespace fs = std::filesystem;

namespace insilab::probisdock {

/**
 * Prepare receptor by computing its atom types for receptor and accessory
 * ligands, eg. cofactor(s): gaff types for protein, Mg ions, and water are read
 * from the forcefield xml file later on while gaff types for cofactors (ADP,
 * POB, etc.) are calculated de-novo here.
 *
 */
std::shared_ptr<Result> PrepareReceptorJob::execute() const {
  try {
    std::clog << "Computing receptor types batch_size "
              << __params.p_receptor->size()
              << " first-in-batch name = " << __params.p_receptor->name()
              << std::flush << std::endl;

    __params.p_receptor->compute_atom_bond_types();
    if (__params.args.get<bool>("debug")) {
      __params.p_receptor->write(
          fs::temp_directory_path() /
          fs::path(helper::MD5::md5sum(*__params.p_receptor) + ".json.gz"));
    }
    __params.p_receptor->set_ready();
  } catch (const std::exception &e) {
    std::cerr << "[NOTE] Skipping preparing receptor due to " << e.what()
              << std::endl;
  }
  return nullptr;
}

} // namespace insilab::probisdock
