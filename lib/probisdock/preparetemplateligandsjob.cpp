#include "preparetemplateligandsjob.hpp"
#include "decorators.hpp"
#include "helper/md5.hpp"

namespace fs = std::filesystem;

namespace insilab::probisdock {

/**
 * Prepare template ligands in a similar way as receptors. Filter out template
 * ligands that come from proteins with that are too similar to the query
 * protein - needed for testing.
 *
 */
std::shared_ptr<Result> PrepareTemplateLigandsJob::execute() const {
  std::clog << "Computing template ligands types batch_size "
            << __params.template_ligands.size() << " first-in-batch name = "
            << __params.template_ligands.first().name() << std::flush
            << std::endl;

  for (auto &template_ligand : __params.template_ligands) {
    try {
      if (template_ligand.get_comments().contains("seq_id") &&
          template_ligand.get_comments().at("seq_id").get<double>() >
              __params.args.get<double>("max_seq_id"))
        continue;
      template_ligand.compute_atom_bond_types();
      if (__params.args.get<bool>("debug")) {
        template_ligand.write(
            fs::temp_directory_path() /
                fs::path(helper::MD5::md5sum(__params.template_ligands) +
                         ".json.gz"),
            Molecules::Writer::Options::append);
      }
      __params.p_template_ligands->add_thread_safe(template_ligand);

    } catch (const std::exception &e) {
      std::cerr << "[NOTE] Skipping preparing a template ligand " +
                       template_ligand.get_residues().front()->resn() +
                       " due to "
                << e.what() << std::endl;
    }
  }
  return nullptr;
}

} // namespace insilab::probisdock
