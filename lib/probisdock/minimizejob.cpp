#include "minimizejob.hpp"
#include "decorators.hpp"
#include "inout/inout.hpp"
#include "linker/dockedconformation.hpp"
#include "linker/linker.hpp"
#include "program/argumentparser.hpp"
#include <string>

namespace insilab::probisdock {

/**
 * Link a partial conformation using A-STAR search algorithm and minimize the
 * corresponding protein-ligand complex using L-BFGS algorithm (at the end or
 * in-between linking). Output docked ligand, receptor and energy for each
 * docked conformation to a file.
 *
 */
std::shared_ptr<Result> MinimizeJob::execute() const {
  try {
    std::clog << "Minimizing partial conformation of ligand "
              << __params.p_ligand->name() << " (docked pose "
              << __params.conf_id << ") in receptor "
              << __params.p_receptor->get_id() << std::flush << std::endl;

    DockedConformation docked =
        __params.p_linker->link_minimize(*__params.p_partial);

    docked.get_ligand().set_name(
        "Receptor=" + __params.p_receptor->get_filename() +
        " Ligand=" + __params.p_ligand->name() +
        " ConfID=" + helper::to_string(__params.conf_id) +
        " Score=" + helper::to_string(docked.get_energy()));

    const std::string docking_summary =
        __params.p_receptor->get_filename() + "\t" + __params.p_ligand->name() +
        "\t" + helper::to_string(__params.conf_id) + "\t" +
        helper::to_string(docked.get_energy()) + "\n";

    inout::output_file(docking_summary,
                       __params.args.get<std::string>("summary_file"), {},
                       std::ios_base::app);

    return std::make_shared<MinimizeResult>(std::move(docked));

  } catch (const std::exception &e) {
    std::cerr << "[NOTE] Skipping linking and minimization of docked pose of "
              << __params.p_ligand->name() << " in receptor "
              << __params.p_receptor->get_id() << " due to : " << e.what()
              << std::endl;
  }
  return nullptr;
}

} // namespace insilab::probisdock
