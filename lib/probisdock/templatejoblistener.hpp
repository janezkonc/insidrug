#pragma once

#include "parallel/joblistener.hpp"
#include <atomic>
#include <mutex>

namespace insilab::probisdock {
using namespace parallel;

class TemplateJobListener : public JobListener {
public:
  void notify(const std::shared_ptr<Job> job,
              const std::shared_ptr<Result> result) override {
    if (this->jobs_done()) {
      std::dynamic_pointer_cast<PrepareTemplateLigandsJob>(job)
          ->get_params()
          .p_template_ligands->set_ready(true);
    }
  }
};
} // namespace insilab::probisdock
