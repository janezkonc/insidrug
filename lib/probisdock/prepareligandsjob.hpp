#pragma once

#include "cachedock.hpp"
#include "decorators.hpp"
#include "helper/cache.hpp"
#include "molib/molecules.hpp"
#include "parallel/jobs.hpp"
#include <memory>
#include <string>

namespace insilab::score {
class ObjectiveFunction;
}

namespace insilab::ommiface {
class ForceField;
}

namespace insilab::program {
class ArgumentParser;
}

namespace insilab::probisdock {

using namespace parallel;
using namespace program;
using namespace molib;
using namespace ommiface;

struct PrepareLigandsJob : Job {

  struct Params {
    Molecules ligands;
    std::shared_ptr<MoleculeDecorator> p_receptor;
    std::shared_ptr<MoleculesDecorator> p_template_ligands;
    std::shared_ptr<nlohmann::json> p_centroids;
    ForceField &ffield;
    const score::ObjectiveFunction &objective;
    Jobs &jobs;
    const ArgumentParser &args;
    helper::Cache<DockedSeedKey, MoleculesDecorator> &cache;
  };

private:
  const Params __params;

public:
  PrepareLigandsJob(const Params &params) : __params(params) {}

  bool is_executable() const override { return true; }
  std::shared_ptr<Result> execute() const override;
};

} // namespace insilab::probisdock
