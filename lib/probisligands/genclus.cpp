#include "genclus.hpp"
#include "cluster/optics.hpp"
#include "common.hpp"
#include "gencentro.hpp"
#include "geom3d/matrix.hpp"
#include "glib/algorithms/ring.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/filter.hpp"
#include "molib/algorithms/geometry.hpp"
#include "molib/io/iomanip.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecules.hpp"
#include "nosqlreader.hpp"
#include "path/path.hpp"
#include <algorithm>
#include <exception>
#include <functional>
#include <iostream>
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time */
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

int get_number_aligned_residues_near_ligand(
    Atom::Grid &gridrec, const Molecule &ligand,
    const nlohmann::json &aligned_residues, const double dist_cutoff_aligned) {

  std::set<Residue::Id> a = json_to_set(aligned_residues).first;
  std::set<Residue::Id> aligned_near;

  for (auto &patom : ligand.get_atoms()) {
    for (auto &pneighb :
         gridrec.get_neighbors(patom->crd(), dist_cutoff_aligned)) {
      const int near_resi = pneighb->residue().resi();
      const std::string near_chain_id = pneighb->chain().chain_id();
      for (auto &rt : a) {
        const int aligned_resi = get<2>(rt);
        const std::string aligned_chain_id = get<0>(rt);
        if (aligned_resi == near_resi && aligned_chain_id == near_chain_id) {
          aligned_near.insert(rt);
        }
      }
    }
  }

  return aligned_near.size();
}

/**
 * Calculate Hausdorff distance between all ligand pairs. The calculation of
 * Hausdorff distances is quite heavy (eg. protein 1aoi), so this function has
 * been optimized to reuse Point::Grids.
 */
auto create_pairwise_distances(
    const std::vector<geom3d::Point::Vec> &ligand_centroids, const double eps) {

  helper::Benchmark<std::chrono::milliseconds> b;
  std::map<std::pair<std::size_t, std::size_t>, double> pairwise_distances;
  std::vector<geom3d::Point::Grid> grids(ligand_centroids.size());

  for (auto i{0uz}; i < ligand_centroids.size(); ++i) {
    grids[i] = geom3d::Point::Grid(ligand_centroids[i]);
  }

  for (auto i{0uz}; i < ligand_centroids.size(); ++i) {
    const auto &crds_i = ligand_centroids[i];
    // calculate distance even for self!!
    for (auto j = i; j < ligand_centroids.size(); ++j) {
      const auto &crds_j = ligand_centroids[j];
      const auto hd = geom3d::compute_hausdorff_distance_fast(
          crds_i, crds_j, eps, grids[i], grids[j]);
      if (hd <= eps) {
        pairwise_distances[{i, j}] = hd;
      }
    }
  }
  std::clog << "Creating pairwise distances took " << b.duration() << "ms"
            << std::endl;

  return pairwise_distances;
}

auto create_pairwise_distances_single_atom(
    const std::vector<geom3d::Point::Vec> &ligand_centroids, const double eps) {

  helper::Benchmark<std::chrono::milliseconds> b;
  std::map<std::pair<std::size_t, std::size_t>, double> pairwise_distances;

  // calculate neighbors
  for (auto i{0uz}; i < ligand_centroids.size(); ++i) {
    const auto &crds_i = ligand_centroids[i];
    for (auto j = i; j < ligand_centroids.size(); ++j) {
      const auto &crds_j = ligand_centroids[j];
      const auto d = crds_i.at(0).distance(crds_j.at(0));
      if (d <= eps) {
        pairwise_distances[{i, j}] = d;
      }
    }
  }
  std::clog << "Creating pairwise distances (single atom version) took "
            << b.duration() << "ms" << std::endl;

  return pairwise_distances;
}

/// Calculate complex score as defined in ProBiS-Dock Database paper (JCIM,
/// 2021)
int compute_complex_score(const Molecule &molecule) {
  std::set<Element> elements;
  const auto &atoms = molecule.get_atoms();
  for (const auto &patom : atoms) {
    if (patom->element() != Element::H) {
      elements.insert(patom->element());
    }
  }
  const auto rings = glib::algorithms::find_rings(atoms);
  const int n_rings = rings.size();
  const int n_elements = elements.size();
  return (n_rings + 1) * n_elements; // complexity score
}

nlohmann::json read_filter_transpose_ligands(
    const Molecule &receptor, const nlohmann::json &probis_j,
    const std::string &bio_dir, const double dist_cutoff_aligned,
    const double min_z_score, const int min_aligned_near,
    const double polymer_centro_clus_rad,
    std::function<bool(Residue::Type)> is_of_type,
    const double max_interatomic_distance) {

  helper::Benchmark b;

  namespace fs = std::filesystem;

  Atom::Grid gridrec(receptor.get_atoms());
  nlohmann::json result;
  int tot_align{};

  // get total alignment numbers per residue type
  for (const auto &d : probis_j) {
    const double z_score0 = d.at("alignment").at(0).at("scores").at("z_score");
    if (z_score0 > min_z_score)
      ++tot_align;
  }
  // read the alignments from json
  for (const auto &d : probis_j) {
    // if something goes wrong, e.g., a file with ligands is not found, don't
    // exit, just skip that file
    try {
      const auto &alignments = d.at("alignment");
      const double z_score0 = alignments.at(0).at("scores").at("z_score");
      if (z_score0 <= min_z_score)
        continue;

      // aligned representative protein's PDB & Chain ID
      const auto pdb_id = d.at("pdb_id").get<std::string>().substr(0, 4);
      const auto chain_id = d.at("chain_id").get<std::string>();

      // iterate over all ligands for this aligned representative protein
      for (const auto &bio_dir_entry : fs::recursive_directory_iterator(
               path::join(bio_dir, pdb_id + chain_id))) {
        if (fs::is_directory(bio_dir_entry))
          continue;
        // determine rest from name of a file (which is a comments)
        const auto fn = bio_dir_entry.path().filename();
        const auto comments = fn.extension().string() == ".json"
                                  ? fn.stem().string()         // .json
                                  : fn.stem().stem().string(); // .json.gz
        const auto comments_j = comments == "water"
                                    ? nlohmann::json()
                                    : nlohmann::json::parse(comments);
        const auto rest = comments == "water"
                              ? Residue::Type::water
                              : comments_j.at("rest").get<Residue::Type>();

        // skip ligand if it's rest is not the one we want
        if (!is_of_type(rest))
          continue;

        const auto ligands =
            Molecules::Parser(bio_dir_entry.path().string(),
                              Molecules::Parser::Options::all_models)
                .parse();

        for (const auto &ligand : ligands) {
          int max_num_aligned_near{}, best_alignment_no{-1};
          Molecule best_rotated_ligand{};

          // determine which alignment gives the best superposition of the
          // ligand onto query protein
          for (const auto &alignment : alignments) {
            const double z_score = alignment.at("scores").at("z_score");

            // skip alignment if it's Z-score is bad
            if (z_score <= min_z_score)
              continue;

            const int alignment_no = alignment.at("scores").at("alignment_no");
            const geom3d::Matrix mx(alignment.at("rotation_matrix"),
                                    alignment.at("translation_vector"));

            Molecule rotated_ligand(ligand);
            rotated_ligand.rotate(mx, true); // inverse rotation

            // calculate number of nearby aligned residues
            const int num_aligned_near =
                get_number_aligned_residues_near_ligand(
                    gridrec, rotated_ligand, alignment["aligned_residues"],
                    dist_cutoff_aligned);
            if (num_aligned_near <= max_num_aligned_near)
              continue;
            max_num_aligned_near = num_aligned_near;
            best_alignment_no = alignment_no;
            best_rotated_ligand = rotated_ligand;
          }

          // skip if ligand was not transposed
          if (best_alignment_no == -1)
            continue;

          // consider adding the ligand (things can still go wrong)
          const double z_score =
              alignments.at(best_alignment_no).at("scores").at("z_score");

          const geom3d::Matrix mx(
              alignments.at(best_alignment_no).at("rotation_matrix"),
              alignments.at(best_alignment_no).at("translation_vector"));
          const auto t = mx.serialize_trans();
          if (t.size() != 3)
            throw helper::Error(
                "[WHOOPS] Translation vector has wrong format!");

          if (
              // add the crystal ligands from the query protein, because
              // alignments can be on arbitrary side of the query protein
              // (binding site is not preferred, since all residues are
              // conserved)
              std::fabs(t[0]) < 0.000001 && std::fabs(t[1]) < 0.000001 &&
                  std::fabs(t[2]) < 0.000001 ||
              // add ligands from very similar proteins
              z_score > 10.0 ||
              // don't add ligands that are not in aligned regions
              max_num_aligned_near > min_aligned_near) {

            // simplify representation of large molecules (polypeptides and
            // nucleic acids) to save memory
            const auto centroids =
                (details::is_polypeptide(rest) || details::is_nucleic(rest))
                    ? algorithms::compute_centroids(best_rotated_ligand,
                                                    polymer_centro_clus_rad)
                    : [&best_rotated_ligand] {
                        std::pair<geom3d::Point::Vec, std::vector<double>>
                            result;
                        for (const auto &patom :
                             best_rotated_ligand.get_atoms()) {
                          result.first.push_back(patom->crd());
                          result.second.push_back(patom->radius());
                        }
                        return result;
                      }();
            // for (const auto &p : centroids.first) {
            //   std::cout << p << std::endl;
            // }
            // for (const auto &r : centroids.second) {
            //   std::cout << r << std::endl;
            // }
            const std::vector centroids_v{nlohmann::json(centroids)};
            const std::vector<nlohmann::json> trimmed_centroids =
                trim_centroids(centroids_v, receptor, max_interatomic_distance);
            if (trimmed_centroids.empty())
              continue;
            // std::cout << "trimmed_centroids = " << trimmed_centroids.at(0)
            //           << std::endl;
            result.push_back({
                {"tot_align", tot_align},
                {"alignment_no", best_alignment_no},
                {"complex_score",
                 details::is_small(rest) ? compute_complex_score(ligand) : -1},
                {"centro", trimmed_centroids.at(0)},
                {"rep", pdb_id + chain_id},
                {"comments", ligand.get_comments()},
                {"z_score", stod(helper::dtos(z_score))},
                {"mx", mx},
            });
          }
        }
      }
    } catch (const std::exception &e) {
      std::cerr << "[NOTE] Reading of bio files reports: " << e.what()
                << " ... skipping ... " << std::endl;
    }
  }
  std::clog << "Reading of ligands took " << b.duration() << " s" << std::endl;
  return result;
}

/**
 * Iteratively find reasonable clusters by decreasing epsilon and repeating
 * density-based clustering for each epsilon. Used for water and ion ligands,
 * where clusters appear when there are many thousands predicted ligands
 * uniformly distributed in larger region.
 */
std::vector<std::vector<std::size_t>> compute_density_based_clusters_iterative(
    const std::map<std::pair<std::size_t, std::size_t>, double>
        &pairwise_distances,
    const std::vector<double> &scores, double eps, const std::size_t min_pts,
    const std::vector<geom3d::Point::Vec> &ligand_centroids,
    const double max_r) {

  // calculate the maximum radius within each cluster in a set of clusters and
  // report the largest one of the set
  const auto global_max_r = [](const auto &ligand_centroids,
                               const auto ligand_clusters) -> double {
    double ld{};
    for (const auto &cluster : ligand_clusters) {
      geom3d::Point::Vec points;
      for (const auto &i : cluster) {
        points.insert(points.end(), ligand_centroids.at(i).begin(),
                      ligand_centroids.at(i).end());
      }
      const auto &center = geom3d::compute_geometric_center(points);
      ld = std::max(geom3d::compute_max_radius(center, points), ld);
    }
    return ld;
  };
  std::vector<std::vector<std::size_t>> ligand_clusters;
  do {
    std::clog << "Clustering at epsilon = " << eps << std::endl;
    ligand_clusters = cluster::compute_density_based_clusters(
        pairwise_distances, scores, eps + 0.1, eps, min_pts);
    eps -= 0.5;
  } while (eps > 0.0 &&
           global_max_r(ligand_centroids, ligand_clusters) > max_r);
  return ligand_clusters;
}

std::vector<nlohmann::json>
generate_ligand_clusters(const Molecule &receptor,
                         const nlohmann::json &ligands_j,
                         const double dist_cutoff_aligned, const double eps,
                         const int min_pts, const double max_clus_rad) {

  if (ligands_j.empty())
    return {};

  Molecules ligands;
  Atom::Grid gridrec(receptor.get_atoms());
  std::vector<double> scores(ligands_j.size());
  std::vector<geom3d::Point::Vec> ligand_centroids(ligands_j.size());

  // initialize
  for (auto i{0uz}; i < ligands_j.size(); ++i) {
    const auto &d = ligands_j[i];
    ligand_centroids[i] =
        d.at("centro")
            .get<std::pair<geom3d::Point::Vec, std::vector<double>>>()
            .first;
    scores[i] = d.at("z_score");
  }
  // see step20.cpp where rest gets added to comments
  const auto rest =
      ligands_j.at(0).at("comments").at("rest").get<Residue::Type>();
  const auto pairwise_distances =
      (details::is_water(rest) || details::is_ion(rest)
           ? create_pairwise_distances_single_atom(ligand_centroids, eps + 0.1)
           : create_pairwise_distances(ligand_centroids, eps + 0.1));

  const double max_r = (details::is_water(rest) || details::is_ion(rest)
                            ? max_clus_rad
                            : std::numeric_limits<double>::max());

  const auto ligand_clusters = compute_density_based_clusters_iterative(
      pairwise_distances, scores, eps, min_pts, ligand_centroids, max_r);

  std::vector<nlohmann::json> result;
  for (const auto &cluster : ligand_clusters) {
    result.push_back(nlohmann::json::array());
    for (const auto &i : cluster) {
      result.back().push_back(ligands_j.at(i));
    }
  }
  return result;
}

ScoredClusters score_binding_sites(const std::vector<nlohmann::json> &clustered,
                                   const int min_num_occ,
                                   const double min_water_cons) {

  ScoredClusters result;

  for (const auto &ligands_j : clustered) {
    if (ligands_j.empty())
      continue;

    const int tot_align = ligands_j.at(0).at("tot_align");
    const auto rest =
        ligands_j.at(0).at("comments").at("rest").get<Residue::Type>();

    std::set<std::string> occupancy_nr, occupancy;
    int max_complex_score{-1};

    // calculate scores
    for (const auto &d : ligands_j) {
      const std::string &pdb_id = d.at("comments").at("pdb_id");
      const std::string &chain_id = d.at("comments").at("chain_id");
      const std::string &rep = d.at("rep");

      if (details::is_small(rest)) {
        max_complex_score =
            std::max(d.at("complex_score").get<int>(), max_complex_score);
      }
      // count how many times an ion occurs at this position in different pdbs
      occupancy.insert(pdb_id + chain_id);
      occupancy_nr.insert(rep);
    }
    const auto num_occ = occupancy.size();
    const auto num_occ_nr = occupancy_nr.size();
    const auto cons = std::stod(
        helper::dtos(static_cast<double>(occupancy_nr.size()) / tot_align));
    int compound_score{-1};

    // don't add water and ion binding sites that don't meet minimum occupancy
    if (details::is_water(rest) || details::is_ion(rest))
      if (num_occ <= min_num_occ)
        continue;
    if (details::is_water(rest))
      if (cons <= min_water_cons)
        continue;

    if (details::is_small(rest)) {
      const double weight = max_complex_score < 12 ? 0.0 : 100.0;
      compound_score = static_cast<int>(max_complex_score + weight * cons);
    }
    result[{compound_score, max_complex_score, num_occ, num_occ_nr, cons}]
        .push_back(ligands_j);
  }
  return result;
}

} // namespace insilab::probisligands
