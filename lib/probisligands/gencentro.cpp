#include "gencentro.hpp"
#include "geom3d/coordinate.hpp"
#include "molib/molecule.hpp"
#include <utility>
#include <vector>

namespace insilab::probisligands {

using namespace molib;

std::vector<nlohmann::json> generate_centroids(const ScoredClusters &scored,
                                               const double centro_clus_rad) {
  std::vector<nlohmann::json> result;

  for (const auto &[_unused, clusters] : scored) {
    for (const auto &cluster : clusters) {
      geom3d::Point::Vec points;
      for (const auto &d : cluster) {
        const auto &centro =
            d.at("centro")
                .get<std::pair<geom3d::Point::Vec, std::vector<double>>>();
        points.insert(points.end(), centro.first.begin(), centro.first.end());
      }
      result.push_back(
          geom3d::compute_simplified_representation(points, centro_clus_rad));
    }
  }
  return result;
}

std::vector<nlohmann::json>
trim_centroids(const std::vector<nlohmann::json> &centroids,
               const molib::Molecule &receptor,
               const double max_interatomic_distance) {
  std::vector<nlohmann::json> result;
  for (const auto &cp : centroids) {
    result.push_back(nlohmann::json::array(
        {nlohmann::json::array(), nlohmann::json::array()}));
    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();
    if (crds.size() != radiuses.size())
      throw helper::Error(
          "[WHOOPS] Error in centroids - size of centers != size of radiuses!");
    for (auto i = 0uz; i < crds.size(); ++i) {
      for (const auto &patom : receptor.get_atoms()) {
        if (crds[i].distance(patom->crd()) < max_interatomic_distance) {
          result.back().at(0).push_back(crds[i]);
          result.back().at(1).push_back(radiuses[i]);
          break;
        }
      }
    }
  }
  return result;
}

} // namespace insilab::probisligands
