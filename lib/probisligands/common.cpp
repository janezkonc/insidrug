#include "common.hpp"
#include "molib/io/parser.hpp"
#include "molib/molecule.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"

namespace insilab::probisligands {

using namespace molib;
using namespace molib::algorithms;

std::ostream &operator<<(std::ostream &stream, const ResidueSet &residues) {
  for (auto &presidue : residues)
    stream << *presidue << std::endl;
  return stream;
}

ResMap json_to_map(const nlohmann::json &aligned_residues) {
  ResMap a;
  // convert json aligned residues to a set of Id
  for (auto &rpair : aligned_residues) {
    std::vector<std::string> tok = helper::split(rpair.at("a"), "=");
    std::vector<std::string> tok1 = helper::split(tok[0], ":");
    std::vector<std::string> tok2 = helper::split(tok[1], ":");
    // {"a":"Y:212:A=Y:212:B","c":"x"}
    a.insert({Residue::Id(tok1[2], tok1[0], stoi(tok1[1]), ' '),
              Residue::Id(tok2[2], tok2[0], stoi(tok2[1]), ' ')});
    dbgmsg("A " << tok2[2].at(0) << ":" << tok2[0] << ":" << stoi(tok2[1]));
  }
  return a;
}
std::pair<ResSet, ResSet> json_to_set(const nlohmann::json &aligned_residues) {
  ResSet a, b;
  // convert json aligned residues to a set of Id
  for (auto &rpair : aligned_residues) {
    std::vector<std::string> tok = helper::split(rpair.at("a"), "=");
    std::vector<std::string> tok1 = helper::split(tok[0], ":");
    std::vector<std::string> tok2 = helper::split(tok[1], ":");
    // {"a":"Y:212:A=Y:212:B","c":"x"}
    a.insert(Residue::Id(tok1[2], tok1[0], stoi(tok1[1]), ' '));
    b.insert(Residue::Id(tok2[2], tok2[0], stoi(tok2[1]), ' '));
    dbgmsg("A " << tok2[2].at(0) << ":" << tok2[0] << ":" << stoi(tok2[1]));
  }
  return {a, b};
}
ResMap json_to_map_reverse(const nlohmann::json &aligned_residues) {
  ResMap result;
  // convert json aligned residues to a set of Id
  for (auto &rpair : aligned_residues) {
    std::vector<std::string> tok = helper::split(rpair.at("a"), "=");
    std::vector<std::string> tok1 = helper::split(tok[0], ":");
    std::vector<std::string> tok2 = helper::split(tok[1], ":");
    // {"a":"Y:212:A=Y:212:B","c":"x"}
    result.insert({Residue::Id(tok2[2], tok2[0], stoi(tok2[1]), ' '),
                   Residue::Id(tok1[2], tok1[0], stoi(tok1[1]), ' ')});
    dbgmsg("A " << tok2[2].at(0) << ":" << tok2[0] << ":" << stoi(tok2[1]));
  }
  //~ return {a, b};
  return result;
}

nlohmann::json filter(const nlohmann::json &j,
                      std::function<bool(const nlohmann::json &item)> f) {
  nlohmann::json result;
  std::ranges::copy_if(j, std::back_inserter(result), f);
  return result;
}

Molecule read_rotate_ligand(const nlohmann::json &d, const std::string &bio_dir,
                            bool all_atoms_protein_nucleic) {

  static std::map<std::string, Molecule> water_cache;

  const auto &comments_j = [&d] {
    nlohmann::json result = d.at("comments");
    result.erase("pdb_bind"); // not in 'bio' filename
    result.erase("binding_db");
    result.erase("seq_id");
    return result;
  }();
  const auto rest = comments_j.at("rest");
  const auto &comments = comments_j.dump();
  const auto &rep = d.at("rep").get<std::string>();

  if (details::is_water(rest)) {
    if (water_cache.contains(comments)) {
      return water_cache.at(comments);
    }
  }

  const auto &mx = d.at("mx").get<geom3d::Matrix>();

  const auto file_path = [&] {
    const auto &file_stem = details::is_water(rest) ? "water" : comments;
    const auto &result = path::join(bio_dir, rep, file_stem + ".json");
    return path::file_exists(result) ? result : result + ".gz";
  }();

  const auto filter_expr = all_atoms_protein_nucleic
                               ? ExprVec{}
                               : ExprVec{
                                     Key::REST,
                                     Residue::Type::protein,
                                     Key::AND,
                                     Key::ATOM_NAME,
                                     "CA",
                                     Key::OR,
                                     Key::REST,
                                     Residue::Type::nucleic,
                                     Key::AND,
                                     Key::ATOM_NAME,
                                     "P",
                                     Key::OR,
                                     Key::NOT,
                                     Key::P_OPEN,
                                     Key::REST,
                                     Residue::Type::protein,
                                     Key::OR,
                                     Key::REST,
                                     Residue::Type::nucleic,
                                     Key::P_CLOSE,
                                 };
  const auto ligands =
      Molecules::Parser(file_path, Molecules::Parser::Options::all_models)
          .parse()
          .filter(filter_expr)
          .rotate(mx, true)
          .build();

  if (details::is_water(rest)) {
    for (const auto &ligand : ligands) {
      water_cache[ligand.get_comments().dump()] = ligand;
    }
    return water_cache.at(comments);
  }
  return ligands.first();
}

std::map<Residue::Type, ScoredClustersFlat>
vectorize_ligands(const nlohmann::json &ligands_flat_j) {
  std::map<Residue::Type, std::size_t> current_bs_id;
  std::map<Residue::Type, ScoredClustersFlat> result;
  for (const auto &cluster : ligands_flat_j) {
    const auto bsite_rest = cluster.at("bsite_rest").get<Residue::Type>();
    const auto bs_id = cluster.at("bs_id").get<std::size_t>();
    auto scores = cluster;
    scores.erase("data");
    for (const auto &d : cluster.at("data")) {
      if (current_bs_id[bsite_rest] != bs_id) {
        result[bsite_rest].push_back(
            {{cluster.at("compound_score"), cluster.at("complex_score"),
              cluster.at("num_occ"), cluster.at("num_occ_nr"),
              cluster.at("cons")},
             nlohmann::json::array()});
        current_bs_id[bsite_rest] = bs_id;
      }
      result[bsite_rest].back().second.push_back(d);
    }
  }
  return result;
}

std::map<Residue::Type, ScoredClustersFlat> add_ligand_centro(
    std::map<Residue::Type, ScoredClustersFlat> vectorized_ligands,
    const std::string &bio_dir) {
  std::map<Residue::Type, ScoredClustersFlat> result(vectorized_ligands);
  for (auto &[_unused, scored_flat] : result) {
    for (auto &[scores, ligand_clusters] : scored_flat) {
      for (auto &d : ligand_clusters) {
        const auto ligand = probisligands::read_rotate_ligand(d, bio_dir);
        d["ligand"] = ligand;
        d["centro"] = [&ligand] {
          std::pair<geom3d::Point::Vec, std::vector<double>> result;
          for (const auto &patom : ligand.get_atoms()) {
            result.first.push_back(patom->crd());
            result.second.push_back(patom->radius());
          }
          return result;
        }();
      }
    }
  }
  return result;
}

} // namespace insilab::probisligands
