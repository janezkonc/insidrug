#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <streambuf>
#include <string>

namespace insilab::probisligands {

class NosqlReader {
  std::map<std::string, unsigned int> __hash;
  unsigned int __num;
  unsigned int __hash_num(const std::string &name) {
    auto i = __hash.find(name);
    if (i == __hash.end()) {
      __hash[name] = ++__num;
      return __num;
    } else {
      return i->second;
    }
  }

public:
  NosqlReader() : __num(0) {}
  void parse_NOSQL(const std::string);
  void parse_dir_of_NOSQL(const std::string);
};

} // namespace insilab::probisligands
