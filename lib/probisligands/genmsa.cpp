#include "genmsa.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "molib/molecule.hpp"
#include "molib/residue.hpp"
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

nlohmann::json
generate_multiple_sequence_alignment(const molib::Molecule &receptor,
                                     const nlohmann::json &probis_j,
                                     const double align_min_z_score) {

  helper::Benchmark<std::chrono::milliseconds> b;

  auto seq = nlohmann::json::array();

  for (const auto &pchain : receptor.get_chains()) {
    const auto &residues = pchain->get_residues();
    const auto &first_residue = *residues.front();
    const auto &last_residue = *residues.back();

    const auto first_resi = first_residue.resi();
    const auto last_resi = last_residue.resi();
    // align to first/last smaller/larger tens
    const auto fs = first_resi >= 0 ? first_resi % 10 : 10 - (-first_resi) % 10;
    const auto ls = last_resi >= 0 ? 10 - last_resi % 10 : (-last_resi) % 10;

    const auto first_idx = first_resi - fs;
    const auto last_idx = last_resi + ls + 1;

    for (auto i = first_idx; i < last_idx; ++i) {
      const auto result = std::find_if(
          std::cbegin(residues), std::cend(residues), [i](const auto presidue) {
            return presidue->resi() == i && presidue->ins_code() == ' ';
          });
      const auto &query_residue =
          result != std::end(residues)
              ? **result
              : molib::Residue({.resn = "", .resi = i, .ins_code = ' '});
      seq.push_back({
          {"query_resi", query_residue.resi()},
          {"query_resn",
           helper::to_string(
               helper::get_one_letter(query_residue.resn()).value_or('-'))},
          {"query_ins_code", query_residue.ins_code()},
          {"query_chain_id", pchain->chain_id()},
          {"aligned_resn", "-"},
      });
    }
  }
  auto result = nlohmann::json::array();

  // insert query sequence
  result.push_back({
      {"pdb_id", "query_protein"},
      {"sequence", seq},
  });

  for (const auto &d : probis_j) {
    result.push_back({
        {"pdb_id", d.at("pdb_id").get<std::string>()},
        {"sequence", seq},
    });
    auto &current_seq = result.back().at("sequence");

    std::map<std::tuple<int, char, std::string>, nlohmann::json *> m;
    for (auto &c : current_seq) {
      m[{c.at("query_resi").get<int>(), c.at("query_ins_code").get<char>(),
         c.at("query_chain_id").get<std::string>()}] = &c;
    }

    for (const auto &e : d.at("alignment")) {
      const auto alignment_no = e.at("scores").at("alignment_no").get<int>();
      const auto z_score = e.at("scores").at("z_score").get<double>();
      if (z_score < align_min_z_score)
        continue;

      dbgmsg("alignment_no = " << alignment_no << " z_score = " << z_score);
      for (const auto &f : e.at("aligned_residues")) {
        const auto &rrc = f.at("a").get<std::string>();
        const auto &vec = helper::split(rrc, "=:");

        assert(vec.size() == 6);

        const auto &rrd = f.at("c").get<std::string>(); // unused
        const auto flx = rrd.contains("x");
        const auto fingerprint = rrd.contains("f");

        const auto qresn = vec[0];
        const auto qresi = stoi(vec[1]);
        const auto qchain_id = vec[2];

        const auto aresn = vec[3];
        const auto aresi = stoi(vec[4]);
        const auto achain_id = vec[5];

        // dbgmsg("qresn = " << qresn << " qresi = " << qresi << " qins_code =
        // "
        //                   << ' ' << " qchain_id = " << qchain_id
        //                   << " aresn = " << aresn << " aresi = " << aresi
        //                   << " ains_code = " << ' '
        //                   << " achain_id = " << achain_id);
        if (!m.contains({qresi, ' ', qchain_id}))
          continue;
        auto &c = *m.at({qresi, ' ', qchain_id});
        if (c.contains("alignment_no") &&
            c.at("alignment_no").get<int>() < alignment_no)
          continue;
        if (c.at("query_resi").get<int>() != qresi ||
            c.at("query_ins_code").get<char>() != ' ' ||
            c.at("query_chain_id").get<std::string>() != qchain_id)
          continue;

        c.at("aligned_resn") = aresn;
        c["aligned_resi"] = aresi;
        c["aligned_ins_code"] = ' ';
        c["aligned_chain_id"] = achain_id;
        c["alignment_no"] = alignment_no;
      }
    }

    for (auto &c : current_seq) {
      c.erase("query_resn");
      c.erase("query_resi");
      c.erase("query_ins_code");
      c.erase("query_chain_id");
    }
  }

  std::clog << "Generating multiple sequence alignment took " << b.duration()
            << " ms" << std::endl;

  return result;
}

} // namespace insilab::probisligands
