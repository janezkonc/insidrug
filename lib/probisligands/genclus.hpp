#pragma once

#include "common.hpp"
#include "nlohmann/json.hpp"

namespace insilab::probisligands {

/**
 * Read ligands from bio files (each contains one ligand from cluster members
 * transposed to cluster representative) and transpose them to the query
 * protein.
 *
 * @param receptor query protein
 * @param probis_j protein local structural alignments Json obtained with ProBiS
 * algorithm
 * @param bio_dir a top directory of bio files with ligands
 * @param dist_cutoff_aligned cutoff for calculating he number of aligned
 * residues near ligand
 * @param min_z_score ligands from aligned proteins with Z-score greater than
 * this value are tranposed
 * @param min_aligned_near cutoff number of residues for determining if a ligand
 * is in aligned region of a protein
 * @param is_of_type a function to filter ligands of correct type
 * @return a Json array containing rotated and translated ligands that passed
 * filter so that they sit on the query protein
 *
 */
nlohmann::json read_filter_transpose_ligands(
    const molib::Molecule &receptor, const nlohmann::json &probis_j,
    const std::string &bio_dir, const double dist_cutoff_aligned,
    const double min_z_score, const int min_aligned_near,
    const double polymer_centro_clus_rad,
    std::function<bool(molib::Residue::Type)> is_of_type,
    const double max_interatomic_distance);

/**
 * Determine clusters of ligands by their type (rest) and spatial proximity
 * (latter determined by Hausdorff distance for multi-atom ligands and simple
 * distance for single-atoms).
 *
 * @param receptor query protein
 * @param ligands_j predicted ligands of specific type
 * @param dist_cutoff_aligned distance of the ligand from aligned region of
 * receptor (ensures that only ligands in similar regions are being transposed)
 * @param eps epsilon for the Optics algorithm
 * @param min_pts minimum number of points (ligands) to be considered a cluster
 * @param max_clus_rad maximum cluster radius for water and ion clusters
 * @return a vector, in which each element is a Json array of clustered ligands
 * (each cluster is one binding site) of selected type according to their
 * spatial proximity
 */
std::vector<nlohmann::json> generate_ligand_clusters(
    const molib::Molecule &receptor, const nlohmann::json &ligands_j,
    const double dist_cutoff_aligned, const double eps, const int min_pts,
    const double max_clus_rad);

/**
 * Calculate score(s) for each binding site. Binding sites of all types get:
 *    - occupancy score = number of liganded aligned protein chains
 *    - occupancy-non-redundant = num. of liganded representativen chains
 *    - conservation = num. liganded divided by tot. num. aligned chains
 *
 * Small molecule binding sites get also:
 *    - compound score (Sbsite in JCIM, 2022 paper, measures druggability)
 *    - complex score (measures mol. complexity - part of compound score).
 *
 * @param clustered a vector, in which each element is a Json array represening
 * a cluster of ligands
 * @param min_num_occ required minimum number of water or ion ligands in a
 * cluster (doesn't affect other rest types)
 * @param min_water_cons a lower threshold for the conservation of predicted
 * water cluster
 * @return a map, in which a key is a tuple (compound_score, complex_score,
 * num_occ, num_occ_nr, cons) and a value is a vector of ligand clusters (a
 * vector, because there can be more with the same key). Keys are sorted
 * according to scores, so that small molecule binding sites with higher
 * compound_scores are first, and other binding sites with higher conservation
 * (cons) are first
 */
ScoredClusters score_binding_sites(const std::vector<nlohmann::json> &clustered,
                                   const int min_num_occ,
                                   const double min_water_cons);

} // namespace insilab::probisligands
