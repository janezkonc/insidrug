#pragma once

#include "nlohmann/json.hpp"

namespace insilab::molib::details {
enum class Rest : std::size_t;
}

namespace insilab::probisligands {

/**
 * Generate a triangulated surface for each binding site.
 *
 * @param gpoints a vector, in which each element represents one binding site,
 * and is a Json array of points that cover the space of this binding site
 * @param rest a binding site residue type, which determines the color for each
 * type
 * @return a vector of Json array, in which each array is a triangulated surface
 * for one binding site
 */
std::vector<nlohmann::json>
generate_surfaces(const std::vector<nlohmann::json> &gpoints,
                  const molib::details::Rest &rest);

} // namespace insilab::probisligands
