#pragma once

#include <string>

namespace insilab::probisligands {

void generate_biological_assemblies(const std::string &models,
                                    const bool hydrogens,
                                    const std::string &qpdb_file,
                                    const std::string &qcid,
                                    const std::string &bio,
                                    const std::string &bio_file);

}
