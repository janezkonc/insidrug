#include "genbresi.hpp"
#include "centro/centroids.hpp"
#include "common.hpp"
#include "geom3d/matrix.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "path/path.hpp"
#include <algorithm>
#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

std::vector<nlohmann::json>
generate_binding_site_residues(const Molecule &receptor,
                               const std::vector<nlohmann::json> &centroids) {

  std::vector<nlohmann::json> result;
  Atom::Grid gridrec(receptor.get_atoms());

  for (const auto &cp : centroids) {
    result.push_back(nlohmann::json::array());
    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();
    if (crds.size() != radiuses.size())
      throw helper::Error(
          "[WHOOPS] Error in centroids - size of centers != size of radiuses!");

    Residue::CPSet residues;
    for (auto i{0uz}; i < crds.size(); ++i) {
      for (const auto &pneighb : gridrec.get_neighbors(crds[i], radiuses[i])) {
        residues.insert(&pneighb->residue());
      }
    }
    for (const auto &presi : residues) {
      result.back().push_back(
          {{"rest", presi->rest()},
           {"resi", presi->resi()},
           {"ins_code", presi->ins_code()},
           {"resn", presi->resn()},
           {"seg_id", presi->segment().seg_id()},
           {"chain_id", presi->chain().chain_id()},
           {"model_id", presi->model().model_id()},
           {"assembly_id", presi->assembly().assembly_id()}});
    }
  }
  return result;
}

std::vector<nlohmann::json>
generate_binding_site_residues(const Molecule &receptor,
                               const std::vector<nlohmann::json> &gpoints,
                               const double &bresi_dist) {

  // convert gpoints to centroids so that we can reuse existing function for
  // centroids
  std::vector<nlohmann::json> centroids;
  for (const auto &gpoints_bsite : gpoints) {
    centroids.push_back(nlohmann::json::array(
        {nlohmann::json::array(), nlohmann::json::array()}));
    const auto &gp = gpoints_bsite.get<geom3d::Point::Vec>();
    centroids.back().at(0) = gp;
    centroids.back().at(1) = std::vector<double>(gp.size(), bresi_dist);
  }
  return generate_binding_site_residues(receptor, centroids);
}

nlohmann::json from_ligand(const std::string &receptor_file,
                           const std::string &ligand_file, const double d,
                           const std::string &receptor_type) {
  const Molecule receptor = Molecules::Parser(receptor_file).parse().first();
  const Atom::Grid grid(receptor.get_atoms());

  const Molecule ligand = Molecules::Parser(ligand_file).parse().first();
  const Atom::PVec ligand_atoms = ligand.get_atoms();
  std::map<Residue *, double> neighbors; // map residues -> distances
  for (const auto &patom : ligand_atoms) {
    const auto &neighb = grid.get_neighbors(patom->crd(), d);
    for (const auto &pneighb : neighb) {
      const auto presi = &(pneighb->residue());
      const auto dist = pneighb->crd().distance(patom->crd());
      // on the first encounter of residue, insert distance unconditionally
      if (!neighbors.count(presi)) {
        neighbors[presi] = std::numeric_limits<double>::max();
        continue;
      }
      // ... for the rest of times, update only if (new) dist < old_dist
      neighbors[presi] = std::min(dist, neighbors.at(presi));
    }
  }
  nlohmann::json j;
  for (const auto &[presi, dist] : neighbors) {
    if (receptor_type != "all" &&
        helper::to_string(presi->rest()) != receptor_type)
      continue; // don't output if not std::right receptor type
    j.push_back({{"dist", dist},
                 {"bs_id", 0},
                 {"bsite_rest", Residue::Type::notassigned},
                 {"resi", presi->resi()},
                 {"resn", presi->resn()},
                 {"rest", presi->rest()},
                 {"seg_id", presi->segment().seg_id()},
                 {"chain_id", presi->chain().chain_id()},
                 {"model_id", presi->model().model_id()},
                 {"assembly_id", presi->assembly().assembly_id()}});
  }
  return j;
}

nlohmann::json from_centroids(const std::string &receptor_file,
                              const std::string &centroids_file,
                              const std::string &receptor_type) {

  const Molecule receptor = Molecules::Parser(receptor_file).parse().first();
  const Atom::Grid grid(receptor.get_atoms());
  const nlohmann::json centroids = centro::parse_file(centroids_file);

  nlohmann::json result;

  for (const auto &d : centroids) {
    const auto &bsite_rest = d.at("bsite_rest").get<Residue::Type>();
    const auto bs_id = d.at("bs_id").get<std::size_t>();
    const auto &cp = d.at("data");

    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();

    if (crds.size() != radiuses.size())
      throw helper::Error(
          "[WHOOPS] Error in centroids - size of centers != size of radiuses!");

    std::map<Residue *, double> neighbors;

    for (auto i{0uz}; i < crds.size(); ++i) {
      for (const auto &pneighb : grid.get_neighbors(crds[i], radiuses[i])) {
        const auto presi = &(pneighb->residue());
        const double dist = pneighb->crd().distance(crds[i]);
        // on the first encounter of residue, insert distance unconditionally
        if (!neighbors.contains(presi)) {
          neighbors[presi] = std::numeric_limits<double>::max();
          continue;
        }
        // ... for the rest of times, update only if (new) dist < old_dist
        neighbors[presi] = std::min(dist, neighbors.at(presi));
      }
    }
    for (const auto &[presi, dist] : neighbors) {
      if (receptor_type != "all" &&
          helper::to_string(presi->rest()) != receptor_type)
        continue; // don't output if not std::right receptor type
      result.push_back({{"dist", dist},
                        {"bs_id", bs_id},
                        {"bsite_rest", bsite_rest},
                        {"resi", presi->resi()},
                        {"resn", presi->resn()},
                        {"rest", presi->rest()},
                        {"seg_id", presi->segment().seg_id()},
                        {"chain_id", presi->chain().chain_id()},
                        {"model_id", presi->model().model_id()},
                        {"assembly_id", presi->assembly().assembly_id()}});
    }
  }
  return result;
}

nlohmann::json
bresitxt_to_json(const std::vector<std::string> &bresitxt_lines_v) {
  auto result = nlohmann::json::array();
  nlohmann::json obj;
  for (const auto &line : bresitxt_lines_v) {
    if (line.contains("#") || line.empty())
      continue;
    std::stringstream ss(line);
    std::string keyword, equal_sign, value;
    ss >> keyword >> equal_sign >> value;
    if (line.starts_with("resn"))
      obj["resn"] = value;
    else if (line.starts_with("resi"))
      obj["resi"] = std::stoi(value);
    else if (line.starts_with("ins_code"))
      obj["ins_code"] = std::stoi(value);
    else if (line.starts_with("seg_id"))
      obj["seg_id"] = value;
    else if (line.starts_with("chain_id"))
      obj["chain_id"] = value;
    else if (line.starts_with("model_id"))
      obj["model_id"] = std::stoi(value);
    else if (line.starts_with("assembly_id"))
      obj["assembly_id"] = std::stoi(value);
    else if (line.starts_with("rest"))
      obj["rest"] = value;
    else if (line.starts_with("---")) { // end of a residue reached
      result.push_back(obj);
      obj = {}; // reset an object
      continue;
    }
  }
  nlohmann::json result_j = {{
      {"data", result},
      {"bs_id", 0},
      {"bsite_rest", Residue::Type::notassigned},
  }};
  return result_j;
}

std::vector<std::string> json_to_bresitxt(const nlohmann::json &bresi_j) {
  std::vector<std::string> result;
  for (const auto &bsite_j : bresi_j) {
    result.push_back(">>> " + bsite_j.at("bsite_rest").get<std::string>() +
                     " " +
                     std::to_string(bsite_j.at("bs_id").get<std::size_t>()));
    for (const auto &residue_j : bsite_j.at("data")) {
      result.push_back("resn = " + residue_j.at("resn").get<std::string>());
      result.push_back("resi = " +
                       std::to_string(residue_j.at("resi").get<int>()));
      result.push_back("ins_code = " +
                       std::to_string(residue_j.at("ins_code").get<char>()));
      result.push_back("seg_id = " + residue_j.at("seg_id").get<std::string>());
      result.push_back("chain_id = " +
                       residue_j.at("chain_id").get<std::string>());
      result.push_back("model_id = " +
                       std::to_string(residue_j.at("model_id").get<int>()));
      result.push_back("assembly_id = " +
                       std::to_string(residue_j.at("assembly_id").get<int>()));
      result.push_back("rest = " + residue_j.at("rest").get<std::string>());
      result.push_back("---"); // marks end of a residue
    }
  }
  return result;
}

void write_bresi_file(const std::string &bresi_file,
                      const nlohmann::json &bresi_j) {
  if (path::has_suffix(bresi_file, ".txt") ||
      path::has_suffix(bresi_file, ".txt.gz")) {
    inout::file_open_put_contents(bresi_file, json_to_bresitxt(bresi_j));
  } else if (path::has_suffix(bresi_file, ".json") ||
             path::has_suffix(bresi_file, ".json.gz")) {
    inout::output_file(bresi_j, bresi_file);
  }
}

nlohmann::json parse_bresi_file(const std::string &bresi_file) {
  if (path::has_suffix(bresi_file, ".txt") ||
      path::has_suffix(bresi_file, ".txt.gz")) {
    return bresitxt_to_json(inout::read_file(bresi_file).first);
  } else if (path::has_suffix(bresi_file, ".json") ||
             path::has_suffix(bresi_file, ".json.gz")) {
    const auto &result =
        nlohmann::json::parse(inout::read_file_to_str(bresi_file).first);
    return result;
  }
  throw helper::Error("[WHOOPS] Not a valid filename for bresi input file!");
}

} // namespace insilab::probisligands
