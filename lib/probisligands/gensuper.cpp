#include "gensuper.hpp"
#include "common.hpp"
#include "geom3d/matrix.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "path/path.hpp"
#include "probis/parser.hpp"
#include "probis/surface.hpp"
#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;
using namespace molib::algorithms;

// to superimpose proteins SRF files according to json
nlohmann::json superimpose(const nlohmann::json &probis_j,
                           const std::string &srf_dir,
                           const double super_min_z_score,
                           const std::string &super_out_mode) {

  nlohmann::json super;
  for (const auto &d : probis_j) {
    try {
      const auto pdb_id = d.at("pdb_id").get<std::string>().substr(0, 4);
      const auto chain_id = d.at("chain_id").get<std::string>();
      const std::string srf_file_base =
          path::join(srf_dir, pdb_id + chain_id + ".srf");
      const std::string srf_file =
          std::filesystem::exists(std::filesystem::path(srf_file_base))
              ? srf_file_base
              : srf_file_base + ".gz"; // try with gzipped srf file...

      dbgmsg("srf_file = " << srf_file);

      const nlohmann::json &alignments = d.at("alignment");
      dbgmsg("size of alignments = " << alignments.size());

      for (auto &alignment : alignments) {
        const int alignment_no = alignment.at("scores").at("alignment_no");
        const double z_score = alignment.at("scores").at("z_score");
        const geom3d::Matrix mx(alignment.at("rotation_matrix"),
                                alignment.at("translation_vector"));

        dbgmsg("alignment_no = " << alignment_no << " z_score = " << z_score
                                 << " mx = " << mx);

        if (z_score < super_min_z_score)
          continue;

        std::set<Residue::Id> super_residues =
            json_to_set(alignment.at("aligned_residues")).second;

        Molecule molecule =
            probis::Surface::Parser(
                srf_file, probis::Surface::Parser::Options::all_surfaces)
                .parse()
                .molecule();

        // TODO: mark all residues that are in the superimposed region
        // with 1.00 in their beta factors

        molecule.set_name(pdb_id + chain_id);
        molecule.rotate(mx, true);

        nlohmann::json obj;
        obj["alignment_no"] = alignment_no;
        obj["z_score"] = z_score;
        obj["pdb_id"] = pdb_id;
        obj["chain_id"] = chain_id;
        obj["mx"] = mx;

        if (super_out_mode == "allatom") {
          obj["pdb_file"] =
              molib::io::Writer::PdbWriter()
                  .write(molecule)
                  .str(); // no extra info, just output ordinary PDB file

        } else if (super_out_mode == "ca") {
          auto calpha = molecule.filter({Key::ATOM_NAME, "CA"});
          obj["pdb_file"] =
              molib::io::Writer::PdbWriter()
                  .write(calpha)
                  .str(); // no extra info, just output ordinary PDB file
        }

        super.push_back(obj);
      }
    } catch (const std::exception &e) {
      std::cerr << e.what() << " ... strange, skipping a srf file ... "
                << std::endl;
    }
  }
  return super;
}

} // namespace insilab::probisligands
