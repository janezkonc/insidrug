#include "gengridpoints.hpp"
#include "centro/centroids.hpp"
#include "geom3d/linear.hpp"
#include "grid/grid.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include <exception>
#include <iostream>

namespace insilab::probisligands {

using namespace molib;

std::vector<nlohmann::json> generate_gridpoints(
    const Molecule &receptor, const std::vector<nlohmann::json> &centroids,
    const double &grid_spacing, const int &dist_cutoff,
    const double &excluded_radius, const double &max_interatomic_distance) {

  helper::Benchmark<std::chrono::milliseconds> b;

  Atom::Grid grid(receptor.get_atoms());
  std::vector<nlohmann::json> result;

  for (const auto &cp : centroids) {

    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();
    if (crds.size() != radiuses.size())
      throw helper::Error(
          "[WHOOPS] Error in centroids - size of centers != size of radiuses!");

    result.push_back(nlohmann::json::array());
    geom3d::Coordinate min(std::numeric_limits<double>::max(),
                           std::numeric_limits<double>::max(),
                           std::numeric_limits<double>::max());
    geom3d::Coordinate max(std::numeric_limits<double>::lowest(),
                           std::numeric_limits<double>::lowest(),
                           std::numeric_limits<double>::lowest());

    // find the minimium and maximum coordinates of this binding site
    for (auto i = 0uz; i < crds.size(); ++i) {
      const auto &centroid = crds[i];
      const auto &radial_check = radiuses[i];
      geom3d::Coordinate min2 = centroid - ceil(radial_check);
      geom3d::Coordinate max2 = centroid + ceil(radial_check);

      if (min2.x() < min.x())
        min.set_x(min2.x());
      if (min2.y() < min.y())
        min.set_y(min2.y());
      if (min2.z() < min.z())
        min.set_z(min2.z());
      if (max2.x() > max.x())
        max.set_x(max2.x());
      if (max2.y() > max.y())
        max.set_y(max2.y());
      if (max2.z() > max.z())
        max.set_z(max2.z());
    }

    dbgmsg("min point = " << min.pdb());
    dbgmsg("max point = " << max.pdb());

    const int total_gridpoints = 3 * ceil((max.x() - min.x()) / grid_spacing) *
                                 ceil((max.y() - min.y()) / grid_spacing) *
                                 ceil((max.z() - min.z()) / grid_spacing);

    std::clog << "approximately " << total_gridpoints
              << " gridpoints to evaluate\n\n";
    int points_kept = 0;
    int gridpoint_counter = 0;
    const double r = grid_spacing / 2;
    const double max_d = min.distance(max); // distance between min and max
    const int last_column = ceil(max_d / r);
    const int last_row = ceil(max_d / (sqrt(3) * r));
    const int last_layer = ceil(max_d / (2 * r * sqrt(6) / 3));

    dbgmsg("last_column = " << last_column << " last_row = " << last_row
                            << " last_layer = " << last_layer);

    // initialize mapping between gridpoints and discretized 3D space

    geom3d::Coordinate eval;

    for (int column = 0; column <= last_column; column++) {
      int even_column = (column % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
      for (int row = 0; row <= last_row; row++) {
        int even_row = (row % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
        for (int layer = 0; layer <= last_layer; layer++) {
          int even_layer = (layer % 2 == 0) ? 1 : 0; // 1 if odd, 0 if even
          if ((even_column == 0 && even_row == 0) ||
              (even_column == 1 && even_row == 1)) {
            if (even_layer == 1) {
              eval.set_x(min.x() + column * r);
              eval.set_y(min.y() + sqrt(3) * r * row);
              eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
            } else {
              eval.set_x(min.x() + r + column * r);
              eval.set_y(min.y() + r / sqrt(3) + sqrt(3) * r * row);
              eval.set_z(min.z() + layer * 2 * r * sqrt(6) / 3);
            }

            int okay_min = 1;
            int okay_max = 1;
            double closest = std::numeric_limits<double>::max();
            // if the point is within the radial_check of ANY of the
            // centroids...
            bool is_within_r_of_c = false;

            for (auto i = 0uz; i < crds.size(); ++i) {
              const auto &centroid = crds[i];
              const auto &radial_check = radiuses[i];
              if (eval.distance(centroid) <= radial_check) {
                is_within_r_of_c = true;
                break;
              }
            }
            if (is_within_r_of_c) {
              for (Atom *a : grid.get_neighbors(eval, dist_cutoff)) {

                Atom &atom = *a;
                const double vdW = atom.radius();

                const double eval_dist = excluded_radius + 0.9 * vdW;
                const double distance = atom.crd().distance(eval);

                if (distance <= eval_dist) {
                  okay_min = 0;
                  dbgmsg("distance = " << distance
                                       << " eval_dist = " << eval_dist);
                  break;
                } else {
                  okay_min = 1;
                  if (distance < closest)
                    closest = distance;
                }
              }
              if (closest > max_interatomic_distance)
                okay_max = 0;
              if (okay_min * okay_max > 0) {
                dbgmsg("before adding to __gridpoints");
                result.back().push_back(geom3d::Point(
                    stod(helper::dtos(eval.x())), stod(helper::dtos(eval.y())),
                    stod(helper::dtos(eval.z()))));
                dbgmsg("really out");

                points_kept++;
              }
            }
          }
          const int mod = gridpoint_counter % 10000;
          if (mod == 0) {
            dbgmsg("Processing gridpoint "
                   << gridpoint_counter << " of approximately "
                   << total_gridpoints << " (cumulative time: " << b.duration()
                   << "ms)");
          }
          gridpoint_counter++;
        }
        dbgmsg("column = " << column);
      }
    }
    // the last ones that did not get to the next mod==0
    std::clog << "Generating gridpoints took " << b.duration() << " ms and "
              << points_kept << " points were kept out of " << gridpoint_counter
              << " total gridpoints\n";
  }
  return result;
}

} // namespace insilab::probisligands
