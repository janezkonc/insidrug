#include "gendockready.hpp"
#include "cluster/optics.hpp"
#include "common.hpp"
#include "genclus.hpp"
#include "geom3d/linear.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/geometry.hpp"
#include "molib/algorithms/residue_type.hpp"
#include "molib/molecules.hpp"
#include "molib/residue.hpp"
#include "nosqlreader.hpp"
#include "path/path.hpp"
#include <algorithm>
#include <assert.h>
#include <exception>
#include <iostream>
#include <numeric>
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time */
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

/**
 * Find the binding site whose geometric center is closest to the center of the
 * entity (maximum distance is 2.0 Angstroms). Entity is assumed to be ion or
 * water, so this is not a problem. Hausdorff distance should be used if at some
 * later point we decide to include ligands with more than one atom.
 *
 * @param geom_centers a vector, in which each element is a point, representing
 * the center of a ligand cluster
 * @param entity_center center of an entity that is part of the receptor
 * @return if found, return index of ligand cluster in geom_centers vector; if
 * not found, return optional with no value
 *
 */
std::optional<std::size_t>
__get_best_cluster_by_3d_overlap(const std::vector<geom3d::Point> geom_centers,
                                 const geom3d::Point entity_center) {
  std::optional<std::size_t> index{};
  double minimum_distance = 2.0;
  for (auto i{0uz}; i < geom_centers.size(); ++i) {
    if (const double dist = geom_centers[i].distance(entity_center);
        dist < minimum_distance) {
      minimum_distance = dist;
      index = i;
    }
  }
  return index;
}

/**
 * Find accessory ligands for each binding site: conserved ions, waters,
 * cofactors, glycans. Delete any compounds that are part of biological assemby
 * and overlap with a binding site. Keep all protein chains of the bioassembly
 * not regarding if its conserved...
 */
nlohmann::json __decide_whats_compatible(const nlohmann::json &cp,
                                         const nlohmann::json &components,
                                         const Residue::Type &bsite_rest,
                                         const int min_num_occ,
                                         const double min_water_cons,
                                         const double max_overlap) {

  nlohmann::json compatible_accessory_ligands;
  const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
  const auto &radiuses = cp.at(1).get<std::vector<double>>();
  const double ave_radius =
      std::accumulate(radiuses.begin(), radiuses.end(), 0.0) / radiuses.size();

  for (const auto &d : components) {
    const auto rest = d.at("comments").at("rest").get<Residue::Type>();
    const auto entity = d.get<Molecule>();
    Atom::Grid grid_entity(entity.get_atoms());

    const int num_occ = d.at("comments").at("num_occ");
    const double cons = d.at("comments").at("cons");

    // don't allow buffer... but allow nonstandard residues
    if (details::is_buffer(rest))
      continue;
    // for cofactor binding sites, don't keep cofactors inside
    // BINDING SITES 'SMALL' NEED TO BE DIVIDED ALREADY
    if (details::is_cofactor_competitive(bsite_rest) &&
        details::is_cofactor(rest))
      continue;
    // don't allow ions in cofactor binding sites
    if (details::is_cofactor_competitive(bsite_rest) && details::is_ion(rest))
      continue;

    const int num_overlapping = grid_entity.count_near(crds, ave_radius);
    const double overlap = (double)num_overlapping / crds.size();

    const double close_overlap =
        (double)grid_entity.count_near(crds, 2.0) / crds.size();

    // if entity (part of bioassembly) is near the bsite then keep it
    if (!details::is_water(rest) && !details::is_ion(rest) &&
            !details::is_compound(rest) && overlap > 0 &&
            close_overlap < max_overlap ||
        details::is_water(rest) && overlap > 0 && cons > min_water_cons &&
            num_occ > min_num_occ ||
        details::is_ion(rest) && overlap > 0 && num_occ > min_num_occ ||
        details::is_cofactor(rest) && overlap > 0) {
      // add the ligand that is compatible with this binding site
      compatible_accessory_ligands.push_back(d);
    }
  }
  return compatible_accessory_ligands;
}

nlohmann::json
determine_components(const std::map<Residue::Type, ScoredClustersFlat> &ligands,
                     const Molecule &bio_assembly) {

  // compute geom. center for each ligand cluster
  std::map<Residue::Type, geom3d::Point::Vec> geom_centers;
  for (const auto &[bsite_rest, binding_sites] : ligands) {
    for (const auto &[_unused, ligand_cluster] : binding_sites) {
      Molecules ligs;
      for (const auto &d : ligand_cluster) {
        ligs.add(d.at("ligand").get<Molecule>());
      }
      geom_centers[bsite_rest].push_back(
          algorithms::compute_geometric_center(ligs));
    }
  }
  // annotate ligands in the PDB file with information on which binding site
  // they belong to and how conserved they are
  nlohmann::json result;
  for (auto &component_atoms :
       algorithms::split_biochemical_components(bio_assembly)) {
    auto component{Molecule(component_atoms)};
    const auto first_residue = *component.get_residues().front();
    const auto rest = first_residue.rest();
    const auto resn = first_residue.resn();
    const auto resi = first_residue.resi();

    double cons{0.0};
    int num_occ{0};

    if (details::is_water(rest) || details::is_ion(rest)) {
      // comparing bsite_rest with rest would not work for other types!
      if (!geom_centers.contains(rest))
        continue;
      const auto i = __get_best_cluster_by_3d_overlap(
          geom_centers.at(rest),
          algorithms::compute_geometric_center(component));
      if (!i)
        continue;
      // get scores for ligand cluster that overlaps with component
      const auto &[scores, _unused] = ligands.at(rest).at(i.value());
      num_occ = std::get<2>(scores);
      cons = std::get<4>(scores);
    }
    component.set_comments(nlohmann::json{{"rest", rest},
                                          {"resn", resn},
                                          {"resi", resi},
                                          {"num_occ", num_occ},
                                          {"cons", cons}});
    result.push_back(component);
  }
  return result;
}

std::array<std::vector<nlohmann::json>, 2>
determine_substrate_cofactor_competitive(const ScoredClustersFlat &compounds,
                                         const ScoredClustersFlat &cofactors) {
  // generate a grid for every cofactor bsite
  std::vector<Atom::Grid> cofactor_grids;
  std::vector<Molecules> ligs; // valid grid pointers!
  for (const auto &[_unused, ligand_cluster] : cofactors) {
    ligs.emplace_back();
    for (const auto &d : ligand_cluster) {
      ligs.back().add(d.at("ligand").get<Molecule>());
    }
    cofactor_grids.emplace_back(ligs.back().get_atoms());
  }
  // label compounds overlapping cofactors as cofactor-competitive
  std::vector<nlohmann::json> substrate_competitive, cofactor_competitive;
  std::transform(std::cbegin(cofactors), std::cend(cofactors),
                 std::back_inserter(cofactor_competitive),
                 [](const auto &p) { return p.second; });
  for (const auto &[_unused, ligand_cluster] : compounds) {
    bool incr_subs_bsite_cnt{true};
    for (const auto &d : ligand_cluster) {
      const auto ligand = d.at("ligand").get<Molecule>();
      bool overlaps = false;
      // if compound ligand overlaps with any cofactor bsite
      for (auto i{0uz}; const auto &cofactor_grid : cofactor_grids) {
        if (cofactor_grid.is_near(ligand.get_atoms(), 2.0, 3)) {
          cofactor_competitive[i].push_back(d);
          overlaps = true;
          break;
        }
        ++i;
      }
      // label compounds that do not overlap as substrate-competitive
      if (!overlaps) {
        if (incr_subs_bsite_cnt) {
          substrate_competitive.push_back(nlohmann::json::array());
          incr_subs_bsite_cnt = false;
        }
        substrate_competitive.back().push_back(d);
      }
    }
  }
  return {substrate_competitive, cofactor_competitive};
}

std::pair<std::vector<nlohmann::json>, std::vector<nlohmann::json>>
determine_compatible_accessory_ligands(
    const std::vector<nlohmann::json> &centroids,
    const nlohmann::json &components, const int min_num_occ,
    const double min_water_cons, const double max_overlap,
    const Residue::Type &bsite_rest) {

  std::vector<nlohmann::json> receptor(centroids.size()),
      accessory_ligands(centroids.size());
  // go over compound and cofactor binding sites
  for (auto i{0uz}; i < centroids.size(); ++i) {
    const auto &cp = centroids[i];
    if (details::is_cofactor_competitive(bsite_rest) ||
        details::is_substrate_competitive(bsite_rest)) {
      const auto compatible = __decide_whats_compatible(
          cp, components, bsite_rest, min_num_occ, min_water_cons, max_overlap);
      for (const auto &d : compatible) {
        const auto rest = d.at("comments").at("rest").get<Residue::Type>();
        if (details::is_polypeptide(rest)) {
          receptor.at(i).push_back(d);
        } else {
          accessory_ligands.at(i).push_back(d);
        }
      }
    }
  }
  return {receptor, accessory_ligands};
}

} // namespace insilab::probisligands
