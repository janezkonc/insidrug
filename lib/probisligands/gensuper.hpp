#pragma once

#include "nlohmann/json.hpp"
#include <iostream>

namespace insilab::probisligands {

nlohmann::json superimpose(const nlohmann::json &probis_j,
                           const std::string &srf_dir,
                           const double super_min_z_score,
                           const std::string &super_out_mode);
} // namespace insilab::probisligands
