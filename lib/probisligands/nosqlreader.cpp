#include "nosqlreader.hpp"
#include "helper/error.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"
#include <algorithm>
#include <boost/regex.hpp>
#include <fstream>
#include <iostream>
#include <sstream>
#include <streambuf>
#include <string>

namespace insilab::probisligands {

void NosqlReader::parse_NOSQL(const std::string NOSQL_file) {
  boost::smatch m;
  std::vector<std::string> vec = inout::read_file(NOSQL_file).first;
  std::string bname = std::filesystem::path(NOSQL_file).stem();
  const unsigned int num1 = __hash_num(bname);
  for (std::string &line : vec) {
    if (boost::regex_search(
            line, m,
            boost::regex("(\\S+)\\s+\\S+\\s+[^,]+,[^,]+,[^,]+,[^,]+,[^,]+,[^,]+"
                         ",[^,]+,[^,]+,([^,]+).*"))) {
      if (m[1].matched && m[2].matched) {
        const unsigned int num2 = __hash_num(m[1].str());
        std::clog << num1 << "\t" << num2 << "\t" << bname << "\t" << m[1].str()
                  << "\t" << m[2].str() << std::endl;
      }
    }
  }
}

void NosqlReader::parse_dir_of_NOSQL(const std::string NOSQL_dir) {
  //~ std::clog << NOSQL_dir << std::endl;
  std::vector<std::string> files =
      path::files_matching_pattern(NOSQL_dir, ".nosql$");
  for (std::string &f : files) {
    //~ std::clog << f << std::endl;
    parse_NOSQL(f);
  }
}

} // namespace insilab::probisligands
