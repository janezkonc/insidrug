#include "gensurf.hpp"
#include "helper/benchmark.hpp"
#include "helper/debug.hpp"
#include "inout/inout.hpp"
#include "molib/algorithms/surface_triangulated.hpp"
#include "molib/molecule.hpp"
#include "molib/residue.hpp"
#include <cstdint>
#include <exception>
#include <iostream>

namespace insilab::probisligands {

using namespace molib;

const std::map<Residue::Type, std::vector<int>> surface_colors{
    {Residue::Type::small, {150, 150, 150, 70}},
    {Residue::Type::protein, {255, 255, 0, 70}},
    {Residue::Type::protein | Residue::Type::oligopeptide, {0, 230, 230, 70}},
    {Residue::Type::nucleic, {64, 224, 208, 70}},
    {Residue::Type::small | Residue::Type::compound, {0, 255, 0, 70}},
    {Residue::Type::small | Residue::Type::cofactor, {102, 255, 255, 70}},
    {Residue::Type::glycan, {0, 204, 255, 70}},
    {Residue::Type::ion, {255, 128, 0, 70}},
    {Residue::Type::water, {255, 0, 0, 70}}};

std::vector<nlohmann::json>
generate_surfaces(const std::vector<nlohmann::json> &gpoints,
                  const Residue::Type &rest) {

  std::vector<nlohmann::json> result;
  for (const auto &gpoints_bsite : gpoints) {
    const auto points = gpoints_bsite.get<geom3d::Point::Vec>();

    Chain chain({.chain_id = "A"});
    Residue &r =
        chain.add(Segment({.seg_id = "A"})).add(Residue({.resn = "GRI"}));

    int atom_number = 1;

    for (const auto &crd : points) {
      r.add(Atom({.atom_number = atom_number++,
                  .atom_name = "CA",
                  .crd = crd,
                  .element = Element("C")}));
    }
    const auto &[vertices, indices] =
        algorithms::compute_solvent_accessible_surface(chain.get_atoms(),
                                                       surface_colors.at(rest));
    result.push_back({
        {"surf",
         {
             {"vP", 0},
             {"iP", 0},
             {"alphaMode", true},
             {"vertexBuffer", vertices},
             {"indexBuffer", indices},
         }},
    });
  }
  return result;
}

} // namespace insilab::probisligands
