#include "genvina.hpp"
#include "inout/inout.hpp"
#include "path/path.hpp"

namespace insilab::probisligands {

using namespace molib;

std::vector<nlohmann::json>
generate_vina_boxes(const std::vector<nlohmann::json> &centroids) {

  std::vector<nlohmann::json> result;
  for (const auto &cp : centroids) {

    result.push_back(nlohmann::json::object());

    const auto &crds = cp.at(0).get<geom3d::Point::Vec>();
    const auto &radiuses = cp.at(1).get<std::vector<double>>();

    if (crds.size() != radiuses.size())
      throw helper::Error(
          "[WHOOPS] Error in centroids - size of centers != size of radiuses!");

    const double ave_radius =
        std::accumulate(radiuses.begin(), radiuses.end(), 0.0) /
        radiuses.size();

    const geom3d::Point::Pair minmax_point_x =
        geom3d::compute_minmax_point_axis(crds, geom3d::Axis::X);
    const geom3d::Point::Pair minmax_point_y =
        geom3d::compute_minmax_point_axis(crds, geom3d::Axis::Y);
    const geom3d::Point::Pair minmax_point_z =
        geom3d::compute_minmax_point_axis(crds, geom3d::Axis::Z);

    const geom3d::Point center(
        (minmax_point_x.first.x() + minmax_point_x.second.x()) / 2,
        (minmax_point_y.first.y() + minmax_point_y.second.y()) / 2,
        (minmax_point_z.first.z() + minmax_point_z.second.z()) / 2);

    const double sz_x =
        2 *
        (center.distance(minmax_point_x.first, geom3d::Axis::X) + ave_radius);
    const double sz_y =
        2 *
        (center.distance(minmax_point_y.first, geom3d::Axis::Y) + ave_radius);
    const double sz_z =
        2 *
        (center.distance(minmax_point_z.first, geom3d::Axis::Z) + ave_radius);

    result.back() = {
        {"center_x", center.x()}, {"center_y", center.y()},
        {"center_z", center.z()}, {"size_x", sz_x},
        {"size_y", sz_y},         {"size_z", sz_z},
    };
  }
  return result;
}

nlohmann::json
vinatxt_to_json(const std::vector<std::string> &vinatxt_lines_v) {
  nlohmann::json result;
  for (const auto &line : vinatxt_lines_v) {
    if (line.contains("#") || line.empty())
      continue;
    std::stringstream ss(line);
    std::string keyword, equal_sign, value;
    ss >> keyword >> equal_sign >> value;
    if (line.starts_with("center_x"))
      result["center_x"] = std::stod(value);
    else if (line.starts_with("center_y"))
      result["center_y"] = std::stod(value);
    else if (line.starts_with("center_z"))
      result["center_z"] = std::stod(value);
    else if (line.starts_with("size_x"))
      result["size_x"] = std::stod(value);
    else if (line.starts_with("size_y"))
      result["size_y"] = std::stod(value);
    else if (line.starts_with("size_z"))
      result["size_z"] = std::stod(value);
  }
  nlohmann::json result_j = {{
      {"data", result},
      {"bs_id", 0},
      {"bsite_rest", Residue::Type::notassigned},
  }};
  return result_j;
}

std::vector<std::string> json_to_vinatxt(const nlohmann::json &vina_j) {
  std::vector<std::string> result;
  for (const auto &bsite_j : vina_j) {
    result.push_back(">>> " + bsite_j.at("bsite_rest").get<std::string>() +
                     " " +
                     std::to_string(bsite_j.at("bs_id").get<std::size_t>()));
    result.push_back("center_x = " +
                     helper::dtos(bsite_j.at("data").at("center_x")));
    result.push_back("center_y = " +
                     helper::dtos(bsite_j.at("data").at("center_y")));
    result.push_back("center_z = " +
                     helper::dtos(bsite_j.at("data").at("center_z")));
    result.push_back("size_x = " +
                     helper::dtos(bsite_j.at("data").at("size_x")));
    result.push_back("size_y = " +
                     helper::dtos(bsite_j.at("data").at("size_y")));
    result.push_back("size_z = " +
                     helper::dtos(bsite_j.at("data").at("size_z")));
  }
  return result;
}

void write_vina_file(const std::string &vina_file,
                     const nlohmann::json &vina_j) {
  if (path::has_suffix(vina_file, ".txt") ||
      path::has_suffix(vina_file, ".txt.gz")) {
    inout::file_open_put_contents(vina_file, json_to_vinatxt(vina_j));
  } else if (path::has_suffix(vina_file, ".json") ||
             path::has_suffix(vina_file, ".json.gz")) {
    inout::output_file(vina_j, vina_file);
  }
}

nlohmann::json parse_vina_file(const std::string &vina_file) {
  if (path::has_suffix(vina_file, ".txt") ||
      path::has_suffix(vina_file, ".txt.gz")) {
    return vinatxt_to_json(inout::read_file(vina_file).first);
  } else if (path::has_suffix(vina_file, ".json") ||
             path::has_suffix(vina_file, ".json.gz")) {
    const auto &result =
        nlohmann::json::parse(inout::read_file_to_str(vina_file).first);
    return result;
  }
  throw helper::Error("[WHOOPS] Not a valid filename for vina input file!");
}

} // namespace insilab::probisligands
