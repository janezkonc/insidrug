#pragma once

#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
}

namespace insilab::geom3d {
class Coordinate;
}

namespace insilab::probisligands {

/**
 * Generate hexagonal close-packed (HCP) gridpoints for all binding sites, each
 * represented by one or several centroids.
 *
 * @param receptor a Molecule, receptor protein
 * @param centroids a vector of 2-element Json arrays, in which first element is
 * a vector of centroid centers, and second element is a vector of their
 * radiuses
 * @param grid_spacing a double, the distance between adjacent points
 * @param dist_cutoff a radius with a point at the center, in which atoms are
 * considered for score calculation (here, we don't calculate energy, so this
 * has no meaning)
 * @param excluded_radius a cutoff, which determines an exclusion zone around
 * atoms where there are no points
 * @param max_interatomic_distance a cutoff, how far can a point be from the
 * closest atom
 * @return a vector, in which each element represents a binding site, and is a
 * Json array of coordinates
 *
 */
std::vector<nlohmann::json>
generate_gridpoints(const molib::Molecule &receptor,
                    const std::vector<nlohmann::json> &centroids,
                    const double &grid_spacing, const int &dist_cutoff,
                    const double &excluded_radius,
                    const double &max_interatomic_distance);

} // namespace insilab::probisligands
