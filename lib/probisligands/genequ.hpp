#pragma once

#include "common.hpp"
#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisligands {

/**
 * Calculate pairs of binding sites on the same protein (and of the same
 * bsite_rest type) that contain some of the same ligands (identified by their
 * residue names (resn)). These binding sites may be at different locations on
 * the protein, but have some ligands in common (with the same residue names -
 * resn field). Works for small.compound and small.cofactor ligands only.
 *
 * @param scored_flat a vector of pairs, in which the pair's first element are
 * scores (a tuple) and second element is a cluster of ligands as Json array
 * @return a vector, whose index represents binding site ids (index = bs_id -
 * 1), and whose elements are vectors of equivalent binding sites (their bs_ids)
 */
std::vector<std::vector<std::size_t>>
generate_equivalent_binding_sites(const ScoredClusters &scored_flat);

} // namespace insilab::probisligands
