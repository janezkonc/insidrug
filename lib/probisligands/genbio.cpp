#include "genbio.hpp"
#include "common.hpp"
#include "geom3d/matrix.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"
#include <exception>
#include <iostream>
#include <typeinfo>

namespace insilab::probisligands {

/** Output rotated bio assemblies (or asymmetric units if NO bioassembly)
 *
 */
void generate_biological_assemblies(const std::string &models,
                                    const bool hydrogens,
                                    const std::string &qpdb_file,
                                    const std::string &qcid,
                                    const std::string &bio,
                                    const std::string &bio_file) {
  const auto which_bio =
      bio == "all" ? molib::Molecule::BioHowMany::all_bio
                   : (bio == "first" ? molib::Molecule::BioHowMany::first_bio
                                     : molib::Molecule::BioHowMany::asym);
  const auto which_models =
      models == "all" ? molib::Molecules::Parser::Options::all_models
                      : molib::Molecules::Parser::Options::first_model;
  const auto with_hydrogens =
      hydrogens ? molib::Molecules::Parser::Options::hydrogens : 0;

  molib::Molecules::Parser(qpdb_file, which_models | with_hydrogens)
      .parse()
      .first()
      .generate_bio(which_bio, true)
      .write(bio_file);
}

} // namespace insilab::probisligands
