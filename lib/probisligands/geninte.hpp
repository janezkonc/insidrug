#pragma once

#include "common.hpp"
#include "nlohmann/json.hpp"

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisligands {

/**
 * For every ligand atom, calculate the closest receptor atom within a cutoff
 * radius. These are said to interact.
 *
 * @param receptor a Molecule, receptor protein
 * @param scored a map, in which keys are scores, values are vectors of
 * (equivalently scored) ligand clusters as Json arrays
 * @param inte_dist a cutoff for distance between ligand and receptor
 * @param bio_dir a top directory of bio files with ligand structures
 * @return a vector, in which each element represent one binding site and is a
 * Json array in which elements are receptor-ligand atom pairs (with residue
 * info) that are close together in space
 */
std::vector<nlohmann::json> generate_ligand_interactions(
    const molib::Molecule &receptor, const ScoredClusters &scored,
    const double &inte_dist, const std::string &bio_dir);
} // namespace insilab::probisligands
