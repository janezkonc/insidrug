#pragma once

#include "molib/molecule.hpp"
#include "nlohmann/json.hpp"
#include <iostream>

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisligands {

/**
 * Calculate binding site residues on the receptor from centroids (points with
 * radiuses) that cover a binding site.
 *
 * @param receptor a Molecule, receptor protein
 * @param centroids  a vector, in which each element is a 2-element Json array,
 * with first element a vector of centroid centers, and second element a vector
 * of their radiuses
 * @param bresi_dist the distance cutoff from a point to a receptor atom
 * @return a vector, in which each element represents one binding
 * site, and each element is a Json array of binding site residues identified by
 * residue number (resi), insertion code (ins_code), etc.
 */
std::vector<nlohmann::json>
generate_binding_site_residues(const molib::Molecule &receptor,
                               const std::vector<nlohmann::json> &centroids);

/**
 * Calculate binding site residues on the receptor from gridpoints that cover
 * a binding site.
 *
 * @param receptor a Molecule, receptor protein
 * @param gpoints a vector, in which each element corresponds to a binding
 * site, and each element is a Json array of points
 * @param bresi_dist the distance cutoff from a point to a receptor atom
 * @return a vector, in which each element represents one binding
 * site, and each element is a Json array of binding site residues identified by
 * residue number (resi), insertion code (ins_code), etc.
 */
std::vector<nlohmann::json>
generate_binding_site_residues(const molib::Molecule &receptor,
                               const std::vector<nlohmann::json> &gpoints,
                               const double &bresi_dist);
/**
 * Calculate the residues that are near ligand (or any molecule) and their
 * distances to the nearest ligand atom.
 *
 * @param receptor_file a protein receptor
 * @param ligand_file a ligand bound to the receptor
 * @param d maximum distance to consider between ligand and receptor
 * @param receptor_type {all, protein} consider all residue types that are
 * part of receptor (including ions, etc.) or only protein residues
 * @return json array with neighboring residues
 */
nlohmann::json from_ligand(const std::string &receptor_file,
                           const std::string &ligand_file, const double d = 3.0,
                           const std::string &receptor_type = "protein");

/**
 * Calculate the residues that are near given sets of centroids representing
 * one or multiple binding sites.
 *
 * @param receptor_file a protein receptor
 * @param centroids_file a file with centroids that define a binding site on
 * the receptor
 * @param receptor_type {all, protein} consider all residue types that are
 * part of receptor (including ions, etc.) or only protein residues
 * @return json array with neighboring residues, closest distances and binding
 * site identifiers
 */
nlohmann::json from_centroids(const std::string &receptor_file,
                              const std::string &centroids_file,
                              const std::string &receptor_type = "protein");

/**
 * Write binding site residues file either as a text file (.txt) or as a
 * .json file.
 *
 * @param bresi_file a string, a file name ending with .txt or .json (can be
 * gzipped)
 * @param bresi_j a Json array, in which each element has key 'data', an array
 * containing binding site residues
 */
void write_bresi_file(const std::string &bresi_file,
                      const nlohmann::json &bresi_j);

/**
 * Parse binding site residues from a text file (.txt) or from a Json file.
 *
 * @param bresi_file a string, a file name ending with .txt or .json (can be
 * gzipped)
 * @param bresi_j a Json array, in which each element has key 'data', an array
 * containing binding site residues
 */
nlohmann::json parse_bresi_file(const std::string &bresi_file);

} // namespace insilab::probisligands
