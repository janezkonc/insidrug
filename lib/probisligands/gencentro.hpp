#pragma once

#include "common.hpp"
#include "nlohmann/json.hpp"

namespace insilab::probisligands {

/**
 * Generate centroids, a simplified representation of a binding site as a set of
 * coordinates with radiuses, using clustered ligands.
 *
 * @param scored a map, in which first element is a tuple of scores, and second
 * element is a vector of Json arrays, each such array representing one ligand
 * cluster (each ligand must have 'centro' key whose val holds a simplified
 * representation of this ligand)
 * @param centro_clus_rad cluster radius, representing centroid radiuses
 * approximately
 * @return a vector, in which each element is a Json array with two elements,
 * with first element a vector of centroid centers, and second element a vector
 * of their radiuses (the order of the outer vector corresponds to the order in
 * the input map)
 */
std::vector<nlohmann::json> generate_centroids(const ScoredClusters &scored,
                                               const double centro_clus_rad);

/**
 * Leave just centroids that are less than N Angstroms from receptor.
 *
 * @param centroids a vector of 2-element Json arrays, in which first element is
 * a vector of centroid centers, and second element is a vector of their
 * radiuses
 * @param receptor a Molecule, a receptor protein
 * @param max_interatomic_distance maximum distasnce a centroid center is
 * allowed from any receptor atom
 * @return centroids that passed this filter
 */
std::vector<nlohmann::json>
trim_centroids(const std::vector<nlohmann::json> &centroids,
               const molib::Molecule &receptor,
               const double max_interatomic_distance);

} // namespace insilab::probisligands
