#include "genevo.hpp"
#include "common.hpp"
#include "geom3d/matrix.hpp"
#include "helper/debug.hpp"
#include "helper/help.hpp"
#include "inout/inout.hpp"
#include "molib/molecule.hpp"
#include "path/path.hpp"
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

nlohmann::json
generate_evolutionary_conservation(const Molecule &receptor,
                                   const nlohmann::json &probis_j,
                                   const double conservation_min_z_score) {

  nlohmann::json evo;
  int max_cons = 0;
  // go over receptor residues
  for (auto &presidue : receptor.get_residues()) {
    const int resi = presidue->resi();
    const char ins_code = presidue->ins_code();
    const std::string chain_id = presidue->chain().chain_id();
    dbgmsg("resi = " << resi << " ins_code = " << ins_code
                     << " chain_id = " << chain_id);
    int conservation_counter = 0;
    // calculate conservation score for this residue
    for (const auto &d : probis_j) {
      const std::string pdb_id = d.at("pdb_id");
      auto &alignments = d.at("alignment");
      dbgmsg("pdb_id = " << pdb_id);
      bool conserved = false;
      for (const auto &e : alignments) {
        const int alignment_no = e.at("scores").at("alignment_no");
        const double z_score = e.at("scores").at("z_score");
        dbgmsg("alignment_no = " << alignment_no << " z_score = " << z_score);
        if (z_score > conservation_min_z_score) {
          auto &aligned_residues = e.at("aligned_residues");
          for (auto &f : aligned_residues) {
            const std::string &rrc = f.at("a");
            const auto &vec = helper::split(rrc, "=:");

            assert(vec.size() == 6);

            const char qresn = vec[0].at(0);
            const int qresi = stoi(vec[1]);
            const std::string qchain_id = vec[2];

            dbgmsg("qresn = " << qresn << " qresi = " << qresi
                              << " qchain_id = " << qchain_id << " resi = "
                              << resi << " ins_code = " << ins_code
                              << " chain_id = " << chain_id);

            // if this residue is conserved, increase its counter
            if (resi == qresi && ins_code == ' ' && chain_id == qchain_id) {

              ++conservation_counter;
              conserved = true;
              break;
            }
          }
        }
        if (conserved)
          break;
      }
    }
    max_cons = std::max(conservation_counter, max_cons);
    // add to json
    evo.push_back({
        {"resi", resi},
        {"ins_code", ' '}, // for now this is blank space, until I make probis
                           // recongnize and use insertion codes...
        {"chain_id", chain_id},
        {"cons", conservation_counter},
    });
  }
  // normalize the conservation
  if (max_cons > 0) {
    for (auto &d : evo) {
      const double cons = d.at("cons");
      d["cons"] = (int)floor(10 * cons / (max_cons + 1));
    }
  }
  return evo;
}

} // namespace insilab::probisligands
