#pragma once

#include "common.hpp"
#include "nlohmann/json.hpp"

namespace insilab::molib::details {
enum class Rest : std::size_t;
}

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisligands {

/**
 * Split biological assembly (i.e., receptor protein) into biochemical
 * components (i.e., protein chains, small molecules, water, ion,...). Annotate
 * water and ion components with conservation (water) and number of occurences
 * (both) based on their overlap with predicted 'conserved' water and ion
 * clusters.
 *
 * @param ligands a map, in which key is binding site type and value are
 * corresponding binding sites (ligand clusters)
 * @param bio_assembly a Molecule, receptor biological unit (or asymmetric unit)
 * @return a Json array, in which each element is a components of the receptor
 * (bio_assembly), where water and ions are annotated with conservation
 */
nlohmann::json determine_components(
    const std::map<molib::details::Rest, ScoredClustersFlat> &ligands,
    const molib::Molecule &bio_assembly);

/**
 * Determine substrate-competitive binding sites (contain ligands such as
 * substrates (agonists), substrate (agonist)-competitive ligands) and
 * cofactor-competitive binding sites (these are for cofactors and
 * cofactor-competitive ligands).
 *
 * @param compounds a vector, in which each element is a Json array representing
 * one ligand cluster where all ligands are compounds
 * @param cofactors a vector, in which each element is a Json array representing
 * one ligand cluster where all ligands are cofactors
 * @return an array of size 2, in which the two elements are vectors of Json
 * arrays; first element represents substrate-competitive binding sites and
 * second cofactor-competitive binding sites
 */
std::array<std::vector<nlohmann::json>, 2>
determine_substrate_cofactor_competitive(const ScoredClustersFlat &compounds,
                                         const ScoredClustersFlat &cofactors);

/**
 * Determine which accessory ligands are compatible, i.e., are near, do not
 * overlap, with each binding site.
 *
 * @param centroids a vector, in which each element is a 2-element Json array,
 * where the first element is a Json array of centroid centers, and the second
 * element is a Json array of centroid radiuses
 * @param components a Json array, in which each element is a components of the
 * receptor (bio_assembly), and where water and ions are annotated with
 * conservation
 * @param min_num_occ a lower threshold for the number of predicted waters and
 * ions in a cluster
 * @param min_water_cons a lower threshold for the conservation of predicted water
 * cluster
 * @param max_overlap an upper threshold for how much an accessory ligand is
 * allowed to overlap with a binding site (atoms of its predicted ligands)
 * @return a pair, in which the first element is a vector, in which an element
 * represents one binding site, and is a Json array of compatible receptor
 * components for that binding site; the second element is also a vector, in
 * which each element represents one binding site, and is a Json array of
 * compatible accessory ligands for that binding site
 */
std::pair<std::vector<nlohmann::json>, std::vector<nlohmann::json>>
determine_compatible_accessory_ligands(
    const std::vector<nlohmann::json> &centroids,
    const nlohmann::json &components, const int min_num_occ,
    const double min_water_cons, const double max_overlap,
    const molib::details::Rest &bsite_rest);

} // namespace insilab::probisligands
