#pragma once

#include "molib/residue.hpp"
#include "nlohmann/json.hpp"
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace insilab::molib {
class Residue;
class Molecules;
} // namespace insilab::molib

namespace insilab::probisligands {

typedef std::set<std::pair<molib::Residue::Id, molib::Residue::Id>> ResPairSet;
typedef std::map<molib::Residue::Id, molib::Residue::Id> ResMap;
typedef std::set<molib::Residue::Id> ResSet;
typedef std::vector<molib::Residue::Id> ResVec;
typedef std::set<molib::Residue *> ResidueSet;

// Custom comparator that sorts binding sites according to their scores: by
// compound score for small binding sites, and by conservation otherwise.
struct by_scores {
  bool operator()(const auto &i, const auto &j) const {
    const auto compound_score_i = std::get<0>(i);
    const auto compound_score_j = std::get<0>(j);
    const auto cons_i = std::get<4>(i);
    const auto cons_j = std::get<4>(j);
    const auto result = compound_score_i == -1
                            ? cons_i > cons_j
                            : compound_score_i > compound_score_j;
    return result;
  }
};

/// A tuple: compound_score, complex_score, num_occ, num_occ_nr, cons
using Scores = std::tuple<int, int, int, int, double>;

/// A map with key = scores, value = vector of ligand clusters with same scores
/// (each cluster = Json array of ligands)
using ScoredClusters = std::map<Scores, std::vector<nlohmann::json>, by_scores>;

/// A vector of pairs, first element = scores, second element = Json array of
/// ligands
using ScoredClustersFlat = std::vector<std::pair<Scores, nlohmann::json>>;

std::ostream &operator<<(std::ostream &stream, const ResidueSet &residues);
void output_file(molib::Molecules &mols, const std::string &filename);
ResMap json_to_map(const nlohmann::json &aligned_residues);
std::pair<ResSet, ResSet> json_to_set(const nlohmann::json &aligned_residues);
ResMap json_to_map_reverse(const nlohmann::json &aligned_residues);

/**
 * Filter a json array using provided function.
 */
nlohmann::json filter(const nlohmann::json &j,
                      std::function<bool(const nlohmann::json &item)> f);

/**
 * Read a ligand from bio_dir and rotate it according to rotation-translation
 * matrix. Waters are exceptional as they are cached into memory. This is
 * because all water ligands for each representative are saved in one big file
 * named water.json.gz. Saving waters in individual files (as other ligands are)
 * swamped disk with too many files.
 *
 * @param d a Json object that MUST contain 'comments', 'rep' keys from which
 * the file to read is determined and 'mx', a rotation-translation matrix
 * obtained by ProBiS, which is used to rotate ligand
 * @param bio_dir a string, top directory for bio files (files with ligands
 * superimposed onto their representative protein in a sequence identity
 * cluster)
 * @param all_atoms_protein_nucleic if true, protein and nucleic acids ligands
 * are read as all atom, if false they are read as CA or P atoms
 * @return a Molecule, a rotated and translated ligand (if it's a protein or
 * nucleic, then it contains just Calpha or Phosphorous atoms)
 */
molib::Molecule read_rotate_ligand(const nlohmann::json &d,
                                   const std::string &bio_dir,
                                   bool all_atoms_protein_nucleic = false);

/**
 * Convert from vector representation, in which each element of a vector
 * represents one binding site, to flat Json array representation, in which
 * binding sites are distinguished by their type (bsite_rest) and binding site
 * ID (bs_id).
 *
 * @param[in,out] ligands_flat_j a Json array that is extended with new ligands
 * so that each element will have keys: 'data' - contains wrapped binding site
 * data, 'bsite_rest' - binding site type, 'bs_id' - binding site number, as
 * well as keys for scores
 * @param scored  a map, in which keys are scores and values are vectors of
 * equivalent ligand clusters (it is expected that all are of the same
 * bsite_rest)
 * @param bsite_rest binding site type that is attributed to each element
 */
void add_bs_score_info(auto &ligands_flat_j, const auto &scored,
                       const auto &bsite_rest) {
  for (auto bs_id = 1uz; const auto &[scores, equivalent_clusters] : scored) {
    for (const auto &cluster : equivalent_clusters) {
      ligands_flat_j.push_back({
          {"data", cluster},
          {"bsite_rest", bsite_rest},
          {"bs_id", bs_id++},
          {"compound_score", std::get<0>(scores)},
          {"complex_score", std::get<1>(scores)},
          {"num_occ", std::get<2>(scores)},
          {"num_occ_nr", std::get<3>(scores)},
          {"cons", std::get<4>(scores)},
      });
      // erase some keys that are not needed anymore if they exist
      for (auto &d : ligands_flat_j.back().at("data")) {
        d.erase("centro");
        d.erase("ligand");
      }
    }
  }
}

/**
 * Reverse of 'add_bs_score_info'. Split into vector representation for each
 * binding site type (bsite_rest).
 *
 * @param ligands_flat_j a Json array, in which each element has keys: 'data' -
 * contains wrapped binding site data, 'bsite_rest' - binding site type, 'bs_id'
 * - binding site number, as well as keys for scores
 * @return a map, in which a key is a binding site type (bsite_rest) and a value
 * is a vector (corresponding to 'scored_flat'),in which each element is a pair
 * with first element a tuple of scores, and second element a Json array
 * representing a binding site (it is expected that all are of the same
 * bsite_rest)
 */
std::map<molib::Residue::Type, ScoredClustersFlat>
vectorize_ligands(const nlohmann::json &ligands_flat_j);

/**
 * Adds two new keys 'ligand' and 'centro' to ligands, by reading, for each
 * ligand, a corresponding bio file with ligand's coordinates.
 *
 * @param vectorized_ligands a map, in which a key is a binding site type
 * (bsite_rest) and a value is a vector (corresponding to 'scored_flat'),in
 * which each element is a pair with first element a tuple of scores, and second
 * element a Json array representing a binding site (it is expected that all are
 * of the same bsite_rest)
 * @param bio_dir a string, top directory for bio files (files with ligands
 * superimposed onto their representative protein in a sequence identity
 * cluster)
 * @return 'vectorized_ligands' with added ligand PDB coordinates and a
 * simplified representation by centroids
 *
 *
 */
std::map<molib::Residue::Type, ScoredClustersFlat> add_ligand_centro(
    std::map<molib::Residue::Type, ScoredClustersFlat> vectorized_ligands,
    const std::string &bio_dir);

/**
 * Convert from vector representation, in which each element of a vector
 * represents one binding site, to flat Json array representation, in which
 * binding sites are distinguished by their type (bsite_rest) and binding site
 * ID (bs_id).
 *
 * @param[in,out] bsite_entities_flat_j a Json array that is extended in this
 * function with new ligands(each element will have keys: 'data' - contains
 * wrapped binding site data, 'bsite_rest' - binding site type, 'bs_id' -
 * binding site number)
 * @param vec a vector of Json objects, each representing a binding site
 * @param bsite_rest binding site type that is given to each element of
 * bsite_entities_flat_j
 */
void add_bs_info(auto &bsite_entities_flat_j, const auto &vec,
                 const auto &bsite_rest) {
  for (auto j = 1uz; const auto &d : vec) {
    bsite_entities_flat_j.push_back(
        {{"data", d}, {"bsite_rest", bsite_rest}, {"bs_id", j++}});
  }
}

/**
 * Erase vector positions that are found in indexes.
 *
 * @param vec a vector
 * @param indexes a set of indexes to delete from vector
 * @return a new vector with remaining positions
 */
auto erase_empty(const auto &vec, const auto &indexes) {
  std::remove_cvref_t<decltype(vec)> filtered;
  for (auto i = 0uz; i < vec.size(); ++i) {
    if (!indexes.contains(i)) {
      filtered.push_back(vec[i]);
    }
  }
  return filtered;
}

/**
 * Erase defect clusters at indexed positions (version for ScoredClusters).
 *
 * @param scored a map, in which keys are scores and values are vectors of
 * equivalent ligand clusters
 * @param indexes a set of indexes to delete from vector
 * @return a new ScoredClusters map with filtered ligand clusters
 */
auto erase_empty(const ScoredClusters &scored, const auto &indexes) {
  ScoredClusters filtered;
  for (auto i{0uz}; const auto &[scores, equivalent_ligand_clusters] : scored) {
    std::vector<nlohmann::json> filtered_equivalent;
    for (const auto &ligand_cluster : equivalent_ligand_clusters) {
      if (!indexes.contains(i++)) {
        filtered_equivalent.push_back(ligand_cluster);
      }
    }
    filtered[scores] = filtered_equivalent;
  }
  return filtered;
}

} // namespace insilab::probisligands
