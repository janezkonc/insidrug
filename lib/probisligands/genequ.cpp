#include "common.hpp"
#include "geninte.hpp"
#include "molib/molecules.hpp"
#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;

std::vector<std::vector<std::size_t>>
generate_equivalent_binding_sites(const ScoredClusters &scored) {

  std::vector<std::set<std::string>> resns;

  for (const auto &[scores, equivalent_clusters] : scored) {
    for (const auto &cluster : equivalent_clusters) {
      resns.push_back({});
      for (const auto &d : cluster) {
        resns.back().insert(
            d.at("ligand").get<Molecule>().get_residues().front()->resn());
      }
    }
  }
  std::vector<std::vector<std::size_t>> result(resns.size());

  for (auto i{0uz}; i < resns.size(); ++i) {
    for (auto j = i + 1; j < resns.size(); ++j) {
      std::vector<std::string> intersection;
      std::set_intersection(resns[i].cbegin(), resns[i].cend(),
                            resns[j].cbegin(), resns[j].cend(),
                            std::back_inserter(intersection));
      if (!intersection.empty()) {
        result[i].push_back(j + 1);
        result[j].push_back(i + 1);
      }
    }
  }
  return result;
}

} // namespace insilab::probisligands
