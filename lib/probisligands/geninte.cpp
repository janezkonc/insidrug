#include "geninte.hpp"
#include "common.hpp"
#include "molib/molecules.hpp"
#include "path/path.hpp"
#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

namespace insilab::probisligands {

using namespace molib;
using namespace molib::algorithms;

std::vector<nlohmann::json> generate_ligand_interactions(
    const Molecule &receptor, const ScoredClusters &scored,
    const double &inte_dist, const std::string &bio_dir) {

  std::vector<nlohmann::json> result;

  for (const auto &[_unused, clusters] : scored) {
    for (const auto &cluster : clusters) {
      result.push_back(nlohmann::json::array());
      for (const auto &d : cluster) {
        const auto &comments = d.at("comments");
        const auto ligand = probisligands::read_rotate_ligand(d, bio_dir);
        const auto receptor_close_to_ligand =
            receptor.filter({Key::ALL, Key::WITHIN, inte_dist, Key::OF,
                             Key::CONTAINER, ligand});

        std::map<Atom *, std::pair<Atom *, double>> closest;
        for (const auto &pligand_atom : ligand.get_atoms()) {
          closest[pligand_atom] = {nullptr, std::numeric_limits<double>::max()};
          for (const auto &preceptor_atom :
               receptor_close_to_ligand.get_atoms()) {
            const auto d = pligand_atom->crd().distance(preceptor_atom->crd());
            if (d < closest[pligand_atom].second) {
              closest[pligand_atom] = {preceptor_atom, d};
            }
          }
        }
        for (const auto &[pligand_atom, pair_atom_distance] : closest) {
          const auto &[preceptor_atom, distance] = pair_atom_distance;
          if (!preceptor_atom || distance >= inte_dist)
            continue;
          result.back().push_back({
              {"receptor",
               {
                   {"resi", preceptor_atom->residue().resi()},
                   {"ins_code", preceptor_atom->residue().ins_code()},
                   {"resn", preceptor_atom->residue().resn()},
                   {"seg_id", preceptor_atom->segment().seg_id()},
                   {"chain_id", preceptor_atom->chain().chain_id()},
                   {"model_id", preceptor_atom->model().model_id()},
                   {"atom_name", preceptor_atom->atom_name()},
               }},
              {"ligand",
               {
                   {"comments", comments},
                   {"resi", pligand_atom->residue().resi()},
                   {"ins_code", pligand_atom->residue().ins_code()},
                   {"resn", pligand_atom->residue().resn()},
                   {"seg_id", pligand_atom->segment().seg_id()},
                   {"chain_id", pligand_atom->chain().chain_id()},
                   {"model_id", pligand_atom->model().model_id()},
                   {"atom_name", pligand_atom->atom_name()},
               }},
              {"dist", distance},
          });
        }
      }
    }
  }

  return result;
}

} // namespace insilab::probisligands
