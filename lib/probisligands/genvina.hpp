#pragma once

#include "molib/molecule.hpp"
#include "nlohmann/json.hpp"
#include <iostream>

namespace insilab::molib {
class Molecule;
}

namespace insilab::probisligands {

/**
 * Generate boxes around binding site centroids that can be used directly as
 * inputs for AutoDock Vina.
 *
 * @param centroids a vector, in which each element is a 2-element Json array,
 * in which first element is a Json array of centroids, and second element is a
 * Json array of radiuses
 * @return a vector, in which each element is a Json object representing one
 * Vina box
 */
std::vector<nlohmann::json>
generate_vina_boxes(const std::vector<nlohmann::json> &centroids);

/**
 * Write AutoDock Vina binding site file either as a text file (.txt) or as a
 * .json file.
 *
 * @param vina_file a string, a file name ending with .txt or .json (can be
 * gzipped)
 * @param vina_j a Json array, in which each element has key 'data', an object
 * with the following keys: center_x, center_y, center_z, size_x, size_y and
 * size_z
 */
void write_vina_file(const std::string &vina_file,
                     const nlohmann::json &vina_j);

/**
 * Parse AutoDock Vina binding site from a text file (.txt) or from a Json file.
 *
 * @param vina_file a string, a file name ending with .txt or .json (can be
 * gzipped)
 * @return a Json array, in which each element has key 'data', an object
 * with the following keys: center_x, center_y, center_z, size_x, size_y and
 * size_z
 */
nlohmann::json parse_vina_file(const std::string &vina_file);

} // namespace insilab::probisligands
