#pragma once

#include <iomanip>
#include <openssl/md5.h>
#include <sstream>
#include <string>
#include <unistd.h>

namespace insilab::helper {

class MD5 {
public:
  static std::string get_exe_full_path();
  static std::string md5sum_file(const std::string &filename);

  template <typename T> static std::string md5sum(const T &t) {

    unsigned char result[MD5_DIGEST_LENGTH];

    std::stringstream ss;
    ss << t;

    const std::string &str = ss.str();

    ::MD5((unsigned char *)str.c_str(), str.size(), result);

    std::ostringstream sout;
    sout << std::hex << std::setfill('0');

    for (long long c : result) {
      sout << std::setw(2) << (long long)c;
    }
    return sout.str();
  }
};

} // namespace insilab::helper
