#pragma once

#include <exception>
#include <sstream>
#include <string>

namespace insilab::helper {

class Error : public std::exception {
  const std::string __msg;

public:
  Error(const std::string &msg) : __msg(msg) {}
  ~Error() throw() {}
  const char *what() const noexcept { return __msg.c_str(); }
};

} // namespace insilab::helper
