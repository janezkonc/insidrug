#pragma once

#include <cassert>
#include <cstring>
#include <iostream>

namespace insilab::helper {

template <typename T> struct Array1d {
  T *data;
  int sz;

  Array1d operator-() const {
    Array1d response(*this);
    for (int i = 0; i < this->sz; i++) {
      response.data[i] = -response.data[i];
    }
    return response;
  }

  Array1d operator/(const Array1d &other) {
    assert(this->sz == other.sz);
    Array1d response(*this);
    for (int i = 0; i < this->sz; i++) {
      if (other.data[i] != 0) {
        response.data[i] /= other.data[i];
      }
    }
    return response;
  }

  Array1d operator+(const Array1d &other) {
    assert(this->sz == other.sz);
    Array1d response(other);
    for (int i = 0; i < this->sz; i++) {
      response.data[i] += this->data[i];
    }
    return response;
  }

  Array1d operator*(const double &right) const {
    Array1d response(*this);
    for (int i = 0; i < this->sz; i++) {
      response.data[i] *= right;
    }
    return response;
  }

  void init(int SZ) {
    sz = SZ;
    data = new T[sz];
    std::memset(data, 0, sz * sizeof(T));
  }

  Array1d(int SZ) : data(nullptr) { init(SZ); }

  Array1d() : data(nullptr), sz(0) {}

  Array1d(const Array1d &other) { // copy
    sz = other.sz;
    data = new T[sz];
    std::memcpy(data, other.data, sz * sizeof(T));
  }

  ~Array1d() {
    if (data) {
      delete[] data;
      data = nullptr;
    }
  }

  void reset() { std::memset(data, 0, sz * sizeof(T)); }
};

template <typename T>
std::ostream &operator<<(std::ostream &stream, const Array1d<T> &s) {
  stream << "(";
  for (int i = 0; i < s.sz; ++i) {
    stream << s.data[i] << (i + 1 == s.sz ? "" : ",");
  }
  stream << ")" << std::endl;
  return stream;
}

} // namespace insilab::helper
