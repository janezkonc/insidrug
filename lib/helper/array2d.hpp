#pragma once

#include <cassert>
#include <cstdint>
#include <memory>
#include <ostream>
#include <vector>

namespace insilab::helper {

template <typename T> struct Array2d {
  std::unique_ptr<std::unique_ptr<T[]>[]> data;
  std::size_t szi{}, szj{};

public:
  Array2d() = default;
  explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
      : data(std::make_unique<std::unique_ptr<T[]>[]>(SZI)), szi(SZI),
        szj(SZJ) {
    for (std::size_t i{}; i < szi; ++i) {
      data[i] = std::make_unique<std::uint64_t[]>(szj);
    }
  }
  explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

  constexpr const auto get_szi() const { return szi; }
  constexpr const auto get_szj() const { return szj; }
  constexpr const auto empty() const { return !szi; }

  constexpr T &operator[](const std::size_t i, const std::size_t j) {
    return data[i][j];
  }
  constexpr const T &operator[](const std::size_t i,
                                const std::size_t j) const {
    return data[i][j];
  }
  constexpr bool operator==(const Array2d<T> &r) const {
    if (szi != r.szi || szj != r.szj)
      return false;
    for (std::size_t i{}; i < szi; ++i) {
      for (std::size_t j{}; j < szj; ++j) {
        if (data[i][j] != r.data[i][j])
          return false;
      }
    }
    return true;
  }
  template <typename P>
  friend std::ostream &operator<<(std::ostream &stream, const Array2d<P> &s);
};

/**
 * Variant 2 & 4 are the fastest, they are almost equal. V4 is only slightly
 * faster (cumulative times over random and docking graphs for mcq(d)w and
 * kcq(d)w v2: 72282 s, v4: 72238 s). However, v2 is much simpler by design
 * reducing possible bugs, that's why it's the default version for now.
 */

/**
 * Variant 2: dynamically allocated array of arrays, efficient access to
 * elements.
 *
 * @note this class can only be moved (move assignment constructor and operator
 * are implicitly defaulted) but not copied (due to unique_ptr data member)
 */
template <> struct Array2d<bool> {
private:
  std::unique_ptr<std::unique_ptr<std::uint64_t[]>[]> data;
  std::size_t szi{}, szj{};

public:
  Array2d() = default;
  explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
      : data(std::make_unique<std::unique_ptr<std::uint64_t[]>[]>(SZI)),
        szi(SZI), szj(SZJ) {
    const auto byszj = 1 + szj / 64;
    for (std::size_t i{}; i < szi; ++i) {
      data[i] = std::make_unique<std::uint64_t[]>(byszj);
    }
  }
  explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

  constexpr auto get_szi() const { return szi; }
  constexpr auto get_szj() const { return szj; }
  constexpr auto empty() const { return !szi; }

  constexpr bool get(const std::size_t i, const std::size_t j) const {
    return (data[i][j / 64] & (std::uint64_t{1} << (j % 64)));
  }

  constexpr void set(const std::size_t i, const std::size_t j) {
    data[i][j / 64] |= std::uint64_t{1} << (j % 64);
  }

  constexpr void unset(const std::size_t i, const std::size_t j) {
    data[i][j / 64] &= ~(std::uint64_t{1} << (j % 64));
  }

  friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool> &s);
};

////////////////////////////////////////////////////////////////////////////////

// /**
//  * Variant 1: a single block of memory for the matrix, calculate index on
//  * each access.
//  */
// template <> struct Array2d<bool> {
// private:
//   std::unique_ptr<std::uint64_t[]> data;
//   std::size_t szi{}, szj{};

//   constexpr auto get_idx(const std::size_t i, const std::size_t j) const {
//     assert(i < szi && j < szj);
//     return i * szj + j;
//   }

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(std::make_unique<std::uint64_t[]>(1 + SZI * SZJ / 64)),
//       szi(SZI),
//         szj(SZJ) {}
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     const auto idx = get_idx(i, j);
//     return (data[idx / 64] & (std::uint64_t{1} << (idx % 64)));
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     const auto idx = get_idx(i, j);
//     data[idx / 64] |= std::uint64_t{1} << (idx % 64);
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     const auto idx = get_idx(i, j);
//     data[idx / 64] &= ~(std::uint64_t{1} << (idx % 64));
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

// /**
//  * Variant 3: old version from Tanja's time, dynamically allocated array of
//  * arrays, but with inversed i,j indexes, so that j - dimension is longer
//  * than i-dimension.
//  */
// template <> struct Array2d<bool> {
// private:
//   std::unique_ptr<std::unique_ptr<std::uint64_t[]>[]> data;
//   std::size_t szi{}, szj{};

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(
//             std::make_unique<std::unique_ptr<std::uint64_t[]>[]>(1 + SZI /
//             64)),
//         szi(SZI), szj(SZJ) {
//     const auto byszi = 1 + szi / 64;
//     for (std::size_t i{}; i < byszi; ++i) {
//       data[i] = std::make_unique<std::uint64_t[]>(szj);
//     }
//   }
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     return (data[i / 64][j] & (std::uint64_t{1} << (i % 64)));
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     data[i / 64][j] |= std::uint64_t{1} << (i % 64);
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     data[i / 64][j] &= ~(std::uint64_t{1} << (i % 64));
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

// /**
//  * Variant 4: best of both worlds, single block of memory for the matrix and
//  * efficient access to elements.
//  */
// template <> struct Array2d<bool> {
// private:
//   std::unique_ptr<std::uint64_t[]> data;
//   std::unique_ptr<std::uint64_t *[]> index;
//   std::size_t szi{}, szj{};

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(std::make_unique<std::uint64_t[]>(SZI * (1 + SZJ / 64))),
//         index(std::make_unique<std::uint64_t *[]>(SZI)), szi(SZI), szj(SZJ) {
//     const auto byszj = 1 + SZJ / 64;
//     for (std::size_t i{}; i < szi; ++i) {
//       index[i] = data.get() + i * byszj;
//     }
//   }
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     // assert(i < szi && j < szj);
//     return (index[i][j / 64] & (std::uint64_t{1} << (j % 64)));
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     index[i][j / 64] |= std::uint64_t{1} << (j % 64);
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     index[i][j / 64] &= ~(std::uint64_t{1} << (j % 64));
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

// /**
//  * Variant 5: best of both worlds with Tanja's spin, single block of memory
//  for
//  * the matrix and efficient access to elements.
//  */
// template <> struct Array2d<bool> {
// private:
//   std::unique_ptr<std::uint64_t[]> data;
//   std::unique_ptr<std::uint64_t *[]> index;
//   std::size_t szi{}, szj{};

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(std::make_unique<std::uint64_t[]>(SZJ * (1 + SZI / 64))),
//         index(std::make_unique<std::uint64_t *[]>(1 + SZI / 64)), szi(SZI),
//         szj(SZJ) {
//     const auto byszi = 1 + SZI / 64;
//     for (std::size_t i{}; i < byszi; ++i) {
//       index[i] = data.get() + i * szj;
//     }
//   }
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     // assert(i < szi && j < szj);
//     return (index[i / 64][j] & (std::uint64_t{1} << (i % 64)));
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     index[i / 64][j] |= std::uint64_t{1} << (i % 64);
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     index[i / 64][j] &= ~(std::uint64_t{1} << (i % 64));
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

// /**
//  * Variant 6: vector of vectors of bool.
//  *
//  */
// template <> struct Array2d<bool> {
// private:
//   std::vector<std::vector<bool>> data;
//   std::size_t szi{}, szj{};

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(SZI, std::vector<bool>(SZJ)), szi(SZI), szj(SZJ) {}
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     return data[i][j];
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     data[i][j] = true;
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     data[i][j] = false;
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

// /**
//  * Variant 7: a single vector of bool for the matrix, calculate index on
//  * each access.
//  */
// template <> struct Array2d<bool> {
// private:
//   std::vector<bool> data;
//   std::size_t szi{}, szj{};

//   constexpr auto get_idx(const std::size_t i, const std::size_t j) const {
//     assert(i < szi && j < szj);
//     return i * szj + j;
//   }

// public:
//   Array2d() = default;
//   explicit Array2d(const std::size_t SZI, const std::size_t SZJ)
//       : data(std::vector<bool>(SZI * SZJ)), szi(SZI), szj(SZJ) {}
//   explicit Array2d(const std::size_t SZ) : Array2d(SZ, SZ) {}

//   constexpr auto get_szi() const { return szi; }
//   constexpr auto get_szj() const { return szj; }
//   constexpr auto empty() const { return !szi; }

//   constexpr bool get(const std::size_t i, const std::size_t j) const {
//     return data[get_idx(i, j)];
//   }

//   constexpr void set(const std::size_t i, const std::size_t j) {
//     data[get_idx(i, j)] = true;
//   }

//   constexpr void unset(const std::size_t i, const std::size_t j) {
//     data[get_idx(i, j)] = false;
//   }

//   friend std::ostream &operator<<(std::ostream &stream, const Array2d<bool>
//   &s);
// };

} // namespace insilab::helper
