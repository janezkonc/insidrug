#include "md5.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace insilab::helper {

std::string MD5::get_exe_full_path() {
  std::stringstream ss;
  ss << "/proc/" << getpid() << "/exe";
  std::string lnk = ss.str();

  char buf[1024];
  ssize_t len;
  if ((len = readlink(lnk.c_str(), buf, sizeof(buf) - 1)) != -1)
    buf[len] = '\0';
  return std::string(buf);
}

std::string MD5::md5sum_file(const std::string &filename) {

  unsigned char c[MD5_DIGEST_LENGTH];
  FILE *inFile = fopen(filename.c_str(), "rb");

  if (inFile == NULL) {
    return "";
  }

  MD5_CTX mdContext;
  int bytes;
  unsigned char data[1024];

  MD5_Init(&mdContext);
  while ((bytes = fread(data, 1, 1024, inFile)) != 0)
    MD5_Update(&mdContext, data, bytes);
  MD5_Final(c, &mdContext);

  std::ostringstream sout;
  sout << std::hex << std::setfill('0');

  for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
    sout << std::setw(2) << (long long)c[i];
  }

  fclose(inFile);
  return sout.str();
}

} // namespace insilab::helper
