#pragma once

#include <map>
#include <memory>
#include <mutex>
#include <queue>

namespace insilab::helper {

template <class A> struct is_unique_pointer : std::false_type {};
template <class A, class B>
struct is_unique_pointer<std::unique_ptr<A, B>> : std::true_type {};

template <class A> struct is_const_unique_pointer : std::false_type {};
template <class A, class B>
struct is_const_unique_pointer<const std::unique_ptr<A, B>> : std::true_type {};

template <class A> struct is_shared_pointer : std::false_type {};
template <class A>
struct is_shared_pointer<std::shared_ptr<A>> : std::true_type {};

template <class A> struct is_const_shared_pointer : std::false_type {};
template <class A>
struct is_const_shared_pointer<const std::shared_ptr<A>> : std::true_type {};

/**
 * This is a priority queue based on std::priority_queue that enables map-like
 * access to its elements using std::map and enables updating the priorities of
 * its elements on the fly. Values can be various pointers or objects and should
 * have a comparison operator that orders them by priority.
 */
template <class Key, class Val> class PriorityQueueMap {

  static bool __less(const Val &i, const Val &j) {
    if constexpr (is_shared_pointer<Val>::value ||
                  is_unique_pointer<Val>::value ||
                  is_const_shared_pointer<Val>::value ||
                  is_const_unique_pointer<Val>::value ||
                  std::is_pointer_v<Val> || std::is_member_pointer_v<Val>) {
      return *i < *j;
    }
    return i < j;
  };
  using InternalPriorityQueue =
      std::priority_queue<Val, std::vector<Val>, decltype(__less) *>;

  std::map<Key, Val> __map;
  std::map<Val, Key> __rev_map;
  InternalPriorityQueue __queue{__less};

public:
  /**
   * Push element val to the priority queue and insert it at key in the
   * map. Duplicate elements (with same keys) are not allowed.
   *
   * @param key key of element val by which we get access to it
   * @param val element to push
   */
  void push(const Key &key, const Val &val) {
    if (__map.contains(key))
      return;
    __map.emplace(key, val);
    __rev_map.emplace(val, key);
    __queue.push(val);
  }

  /**
   * Pop element val from priority queue and erase it from the both maps.
   */
  void pop() {
    const auto val = __queue.top();
    const auto key = __rev_map.at(val);
    __map.erase(key);
    __rev_map.erase(val);
    __queue.pop();
  }

  /**
   * @return top value in the priority queue
   */
  const auto top() const {
    const auto val = __queue.top();
    const auto key = __rev_map.at(val);
    return std::make_pair(key, val);
  }

  /**
   * @return true if prioritity queue is empty
   */
  auto empty() const { return __queue.empty(); }

  /**
   * @return number of elements in priority queue
   */
  auto size() const { return __queue.size(); }

  /**
   * @return true if queue contains key
   */
  auto contains(const Key &key) const { return __map.contains(key); }

  /**
   * Access element val by using key (this is the main difference to a normal
   * priority queue that does not enable such access).
   *
   * @param key key of element val by which we gain access to it
   * @return reference to element val in the priority queue.
   */
  auto &at(const Key &key) const { return __map.at(key); }

  /**
   * Update the queue ordering, for example when an element's priority changes.
   */
  void update_order() {
    InternalPriorityQueue updated{__less};
    // std::cout << "QUEUE" << std::endl;
    while (!__queue.empty()) {
      const auto &val = __queue.top();
      // std::cout << "key = " << __rev_map.at(val)
      //           << " priority = " << val->popularity << std::endl;
      updated.push(val);
      __queue.pop();
    }
    // std::cout << "END QUEUE" << std::endl;
    __queue.swap(updated);
  }
};

} // namespace insilab::helper
