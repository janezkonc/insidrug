#pragma once

#include <chrono>

namespace insilab::helper {

/**
 * Calculate the duration of execution of some piece of code (thread-correct).
 * The default duration is expressed in seconds but that can be changed using
 * the template parameter, e.g., std::chrono::milliseconds, to get duration in
 * milliseconds.
 */
template <typename T = std::chrono::seconds> class Benchmark {
  std::chrono::time_point<std::chrono::high_resolution_clock> start;

public:
  Benchmark() : start{std::chrono::high_resolution_clock::now()} {}
  void reset() { start = std::chrono::high_resolution_clock::now(); }
  // Get duration from  start (by default in seconds)
  auto duration() const {
    // duration_cast is needed from higher to lower precision (eg. ns -> s) for
    // integer times (default std::chrono::milliseconds == std::chrono::duration<integer_type, std:milli>),
    // but not when we use double times (e.g., std::chrono::duration<double, std::milli>)
    const auto diff = std::chrono::duration_cast<T>(
        std::chrono::high_resolution_clock::now() - start);
    return diff.count();
  }
};

} // namespace insilab::helper
