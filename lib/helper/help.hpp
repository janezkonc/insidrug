#pragma once

#include "debug.hpp"
#include "error.hpp"
#include <algorithm>
#include <iomanip>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

namespace insilab::helper {

std::string memusage(const std::string &);
std::tuple<double, double, double> gnuplot(const double &x1, const double &x2,
                                           const std::string &datapoints);

struct edge {
  std::string atom_property1;
  std::string atom_property2;
  std::string bond_property;
};

using smiles = std::vector<edge>;

std::ostream &operator<<(std::ostream &os, const smiles &edges);

struct rename_rule {
  smiles pattern;
  std::vector<std::string> rule;
};

using rename_rules = std::vector<rename_rule>;

std::ostream &operator<<(std::ostream &os, const rename_rule &rule);

enum IdatmGeometry {
  Ion = 0,
  Single = 1,
  Linear = 2,
  Planar = 3,
  Tetrahedral = 4,
  TrigonalBipyramidal = 5,
  TetragonalBipyramidal = 6,
  High = 9
};

struct IdatmEntry {
  IdatmGeometry geometry;
  int substituents;
  std::string description;
};
typedef std::map<const std::string, const IdatmEntry> IdatmInfoMap;

/**
 * Remove all leading characters from a string.
 *
 * @param str a string to trim
 * @param trim_char a character that we want to remove
 * @return a trimmed copy of the input string
 *
 */
std::string trim_left(const std::string &str, char trim_char = ' ');

/**
 * Remove all trailing characters from a string.
 *
 * @param str a string to trim
 * @param trim_char a character that we want to remove
 * @return a trimmed copy of the input string
 *
 */
std::string trim_right(const std::string &str, char trim_char = ' ');

/**
 * Remove all leading and trailing characters from a string.
 *
 * @param str a string to trim
 * @param trim_char a character that we want to remove
 * @return a trimmed copy of the input string
 *
 */
std::string trim(const std::string &str, char trim_char = ' ');

/// Determine if two double values are approximately equal.
bool approx_equal(const double a, const double b, const double epsilon = 1e-4);

/**
 * Determine if two sorted containers have an element in common. This function
 * is more efficient for testing if two sets have element in common than e.g.
 * using std::set_intersection.
 *
 * @param first1 begin - first set
 * @param last1 end - first set
 * @param first2 begin - second set
 * @param last2 end - second set
 * @param comp comparison function (optional)
 * @return true if sets have non-empty intersection
 *
 */
template <typename I1, typename I2, typename Comp = std::less<>>
bool has_element_in_common(I1 first1, I1 last1, I2 first2, I2 last2,
                           Comp &&comp = Comp()) {
  while (first1 != last1 && first2 != last2) {
    if (comp(*first1, *first2)) {
      ++first1;
    } else if (comp(*first2, *first1)) {
      ++first2;
    } else {
      return true;
    }
  }
  return false;
}

/**
 * Calculate a difference between vectors vec1 and vec2 (the vectors do not need
 * to be sorted).
 *
 * @param vec1 a first vector
 * @param vec2 a second vector
 * @param comp an optional comparison function for elements within each of the
 * two vectors that will be used for sorting the two vectors
 * @return a vector containing elements that are in vec1 but not in vec2 (vec1
 * \ vec2)
 */
template <typename T, class Comp = std::ranges::less>
std::vector<T> vector_difference(const std::vector<T> &vec1,
                                 const std::vector<T> &vec2, Comp comp = {}) {
  std::vector<T> result;
  // sort the input vectors to prepare for set_difference
  std::vector<T> sorted_vec1{vec1};
  std::vector<T> sorted_vec2{vec2};
  std::ranges::sort(sorted_vec1, comp);
  std::ranges::sort(sorted_vec2, comp);
  // find the difference of the sorted vectors
  std::ranges::set_difference(sorted_vec1, sorted_vec2,
                              std::back_inserter(result), comp);
  return result;
}

// Convert anything that has ostream << operator to string.
template <typename T> std::string to_string(T num) {
  std::ostringstream ss;
  ss << num;
  const auto result = ss.str();
  return result;
}

std::string dtos(double d,
                 int precision = 3); // double to std::string of precision

std::string replace_str_char(std::string str, const std::string &replace,
                             char ch);

/**
 * Split a string into tokens on a delimiter. Tokens can optionally be
 * converted to another type by defining the convert_to function.
 *
 * @param source a string to split
 * @param delimiter a delimiter string on which to split
 * @param keep_empty allow empty tokens in the results
 * @param convert_to a function that allows converting tokens (strings) to a new
 * type
 *
 * @return a vector of tokens converted to the desired type
 */
template <typename T = std::string>
auto split(
    const std::string &source, const char *delimiter = " ",
    bool keep_empty = false,
    T convert_to(const std::string &) = [](const std::string &s) {
      return s;
    }) {
  std::vector<T> results;
  std::size_t prev = 0;
  std::size_t next = 0;
  while ((next = source.find_first_of(delimiter, prev)) != std::string::npos) {
    if (keep_empty || (next - prev != 0)) {
      results.push_back(convert_to(source.substr(prev, next - prev)));
    }
    prev = next + 1;
  }
  if (prev < source.size()) {
    results.push_back(convert_to(source.substr(prev)));
  }
  return results;
}

/**
 * Squeeze a list of elements (eg. a std::string) by removing duplicate
 * consecutive elements (eg. white-spaces) and replacing them with only one such
 * element.
 *
 * @param source a std::string or a vector of elements of any type
 * @param check_equality a function that compares two consecutive elements and
 * returns true if they are equal
 * @return a list of elements in which duplicate elements are replaced with only
 * one copy
 */
template <typename T = std::string>
T squeeze(
    const T &source, bool check_equality(const typename T::value_type &a,
                                         const typename T::value_type &b) =
                         [](const char &a, const char &b) {
                           return std::isspace(a) && std::isspace(b);
                         }) {

  T result;
  std::unique_copy(source.begin(), source.end(), std::back_inserter(result),
                   check_equality);
  return result;
}

/**
 * Create a hash string of specified length from a string given as input.
 *
 * @param s a string to convert to hash
 * @param length a length of the returned hash string
 * @return a hash of specified length (if length > size of string given by
 * std::hash, then the hash will contain zeroes on the right)
 */
std::string hash(const std::string &s, const size_t length = 2);

std::optional<char> get_one_letter(const std::string &resn);
std::string get_three_letter(const char c);

const IdatmEntry &get_info_map(const std::string &name);
const std::vector<std::string> &get_gaff_replacement(const std::string &name);
std::vector<std::vector<std::string>>
get_replacement(const std::vector<std::string> &initial);

const std::string EW = "^N|^O|F|Cl|Br"; // electron withdrawing atoms
const std::string XX = "^C|^N|^O|^S|^P";
const std::string XA = "^O|^S";
const std::string XB = "^N|^P";
const std::string XC = "F|Cl|Br|I";
const std::string XD = "^S|^P";

extern const std::map<const std::string, const std::string> gaff_flip;
extern const std::set<std::string> gaff_group_1;
extern const std::set<std::string> gaff_group_2;

extern const std::vector<std::string> idatm_unmask;
extern const std::vector<std::string> idatm_element_unmask;
extern const std::map<const std::string, const std::string> idatm_to_sybyl;

extern const std::map<std::pair<std::string, std::string>, double>
    repulsion_idx;
extern const std::map<const std::string, const int> idatm_mask;

extern const std::set<std::string> amino_acids;
extern const std::set<std::string> nucleic_acids_DNA, nucleic_acids_RNA;
extern const std::set<std::string> ions;
extern const std::map<const std::string, const std::string> heavy_atoms;
extern const std::map<const std::string, const std::string>
    protein_hydrogen_atoms;
extern const std::map<const std::string, const std::string>
    nucleic_hydrogen_atoms;
extern const std::map<const std::string, const std::string> buffer_ions;
extern const std::map<const std::string,
                      const std::pair<std::string, std::string>>
    non_specific_binders;
extern const std::map<const std::string,
                      const std::pair<std::string, std::string>>
    non_specific_ion_binders;
extern const std::map<const std::string,
                      const std::map<std::string, std::string>>
    standard_residues;
extern const IdatmInfoMap infoMap;

extern const std::map<const char, const std::string> three_letter;
extern const std::map<const std::string, std::string> sybyl;
extern const std::map<std::string, const char> one_letter;

extern const rename_rules special;
extern const rename_rules refine;
extern const rename_rules bond_gaff_type;

extern const rename_rules rotatable;
extern const std::map<std::string, std::vector<std::string>> gaff_replacement;
extern const rename_rules gaff;
extern const rename_rules atomic_penalty_scores;

} // namespace insilab::helper
