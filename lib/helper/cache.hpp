#pragma once

#include "priorityqueuewithmapaccess.hpp"
#include <cassert>
#include <concepts>
#include <map>
#include <memory>
#include <mutex>
#include <queue>

namespace insilab::helper {

template <typename T>
concept HasLessOperator = requires(T a, T b) {
  { a < b } -> std::same_as<bool>;
};

template <typename T>
concept HasPopularity = requires(T a, T b) {
  { a < b } -> std::same_as<bool>;
  {a.set_popularity(3)};
  {T()};
};

/**
 * A generic class that allows caching various values (objects) that are
 * accessible through their keys in a cache with a fixed maximum size.
 * Internally, a priority queue is used that can update the order of elements on
 * the fly. In this queue, values are sorted by user-defined 'popularity'; a
 * value is 'unused' if no external object is referencing it. If cache is
 * full and a new value needs to be inserted, the least 'popular' AND 'unused'
 * value is removed.
 */
template <HasLessOperator Key, HasPopularity T> class Cache {

  using Val = std::shared_ptr<T>;

  helper::PriorityQueueMap<Key, Val> __priority_queue;
  mutable std::mutex __mtx{};
  const int __max_cache_size;
  std::map<Key, int> __popularity;

public:
  Cache(const int max_cache_size) : __max_cache_size(max_cache_size) {}

  /**
   * Get shared pointers to values from cache:
   *
   *   if value is in cache, return shared_ptr to it;
   *   if NOT in cache, create 'empty' value, return shared_ptr to it.
   *
   * Increase running popularity for this key.
   *
   * @param key a key that uniquely identifies a value
   * @return a shared pointer to existing or created value
   * @notes
   */
  Val get_or_create_value_pointer(const Key &key) {
    const std::lock_guard<std::mutex> guard(__mtx);
    if (__priority_queue.contains(key)) { // get val from cache
      const auto val = __priority_queue.at(key);
      val->set_popularity(++__popularity[key]);
      __priority_queue
          .update_order(); // needed since priority was changed in-place
      return val;
    }
    const auto val = std::make_shared<T>(); // create new val
    val->set_popularity(++__popularity[key]);
    __priority_queue.push(key, val);
    return val;
  }

  /**
   * Check if cache size is above limit and try to remove a value from cache,
   * which is unused and has highest priority (lowest popularity) if such a
   * value exists. Value is unused if its use_count equals 4 (it is referenced
   * only from the inside of priority queue).
   */
  void maybe_remove() {
    const std::lock_guard<std::mutex> guard(__mtx);
    if (__priority_queue.size() < __max_cache_size)
      return;
    std::vector<std::pair<Key, Val>> tmp_store{};
    while (!__priority_queue.empty()) {
      const auto [key, val] = __priority_queue.top();
      if (val.use_count() == 4) {
        break;
      }
      tmp_store.emplace_back(key, val);
      __priority_queue.pop();
    }
    if (!__priority_queue.empty()) {
      __priority_queue.pop();
    }
    for (const auto &[key, val] : tmp_store) {
      __priority_queue.push(key, val);
    }
  }
};

} // namespace insilab::helper
