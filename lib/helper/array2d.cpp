#include "array2d.hpp"
#include <ostream>

namespace insilab::helper {

template <typename T>
std::ostream &operator<<(std::ostream &stream, const Array2d<T> &s) {
  for (auto i{0uz}; i < s.get_szi(); ++i) {
    for (auto j{0uz}; j < s.get_szj(); ++j) {
      stream << s[i, j];
    }
    stream << std::endl;
  }
  return stream;
}

template std::ostream &operator<<(std::ostream &stream,
                                  const Array2d<unsigned short> &s);
template std::ostream &operator<<(std::ostream &stream,
                                  const Array2d<unsigned> &s);
template std::ostream &operator<<(std::ostream &stream,
                                  const Array2d<std::size_t> &s);
template std::ostream &operator<<(std::ostream &stream, const Array2d<int> &s);

std::ostream &operator<<(std::ostream &stream, const Array2d<bool> &s) {
  for (auto i{0uz}; i < s.szi; ++i) {
    for (auto j{0uz}; j < s.szj; ++j) {
      stream << s.get(i, j);
    }
    stream << std::endl;
  }
  return stream;
}

} // namespace insilab::helper
