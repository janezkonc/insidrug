#pragma once

#include <string>
#include <vector>

namespace insilab::helper {

/**
 * A template class that serves as a base class for different parsers (eg. to
 * read various molecule formats, probis surface files, etc.). It must be a
 * template class because 'parse' function may return different things (eg. for
 * probis srf format it returns molecules, probes, descriptors, whereas for
 * molecule parser it just returns the molecules read... It allows reading a
 * file only to a certain position (pos) and then continuing from that position.
 * It also enables reading up to num_occur of blocks in the file at once (eg.
 * ten molecules at a time) for efficiency.
 */
template <typename T> class ParserBase {
protected:
  const std::string __file;
  const int __hm;
  const int __num_occur;
  std::streampos __pos;

public:
  ParserBase(const std::string &file, int hm, const int num_occur = -1)
      : __file(file), __hm(hm), __num_occur(num_occur), __pos(0) {}
  virtual T parse(const std::vector<std::string> &) = 0;
  virtual std::vector<std::string> read_file() = 0;
  void set_pos(std::streampos pos) { __pos = pos; };
  std::streampos get_pos() const { return __pos; };
  const std::string get_file() const { return __file; }
};

} // namespace insilab::helper