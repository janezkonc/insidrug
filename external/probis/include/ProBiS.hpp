#pragma once

#include <string>

namespace ProBiS {
void compare_against_bslib(int argc, char *argv[],
                           const std::string &receptor_file,
                           const std::string &receptor_chain_id,
                           const std::string &bslib_file, const int ncpu,
                           const std::string &nosql_file,
                           const std::string &json_file);
}
