#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;

TEST_CASE("Try to determine element symbols from atom names (and fail)") {

  using namespace molib;

  const auto ligand = Molecules::Parser("files/cyc.pdb.gz",
                                        Molecules::Parser::Options::hydrogens)
                          .parse();
  std::vector<Element> determined, actual;
  auto fail{0u};
  for (const auto &patom : ligand.get_atoms()) {
    try {
      determined.emplace_back(patom->atom_name());
      actual.emplace_back(patom->element());
      std::cout << "Determining element symbol from " << patom->atom_name()
                << " gives " << determined.back()
                << " (actual element = " << actual.back() << ")" << std::endl;
    } catch (const std::exception &e) {
      std::cerr << e.what() << std::endl;
      ++fail;
    }
  }
  REQUIRE(fail == 11);
}

TEST_CASE("Renumber atoms after deleting hydrogens") {

  using namespace molib;

  for (auto &molecule : Molecules::Parser("files/decoys-mmp13-chunk38.mol2.gz",
                                          Molecules::Parser::Options::hydrogens)
                            .parse()) {
    if (molecule.name().find("ZINC63372157") != std::string::npos) {
      algorithms::erase_hydrogen(molecule);
      algorithms::renumber_atoms(molecule);
      REQUIRE(molecule.get_atoms().back()->atom_number() == 31);
      break;
    }
  }
}

TEST_CASE("Mapping of coordinates to Grid for easy neighbors calculation") {

  using namespace molib;

  const auto decoys =
      Molecules::Parser("files/decoys-mmp13-chunk38.json.gz",
                        Molecules::Parser::Options::all_models, 1)
          .parse();
  SECTION("Grid assignment operator") {
    const auto coordinates = decoys.get_crds();
    geom3d::Point::Grid g(coordinates);
    geom3d::Point::Grid h;
    h = g;
    REQUIRE(h == g);
    const auto crd = decoys.get_crds().front();
    REQUIRE(g.get_closest_including_self(crd, 15.0).size() ==
            h.get_closest_including_self(crd, 15.0).size());
    REQUIRE(h == g);

    // Grid does not own coordinates, using it like this is UNDEFINED BEHAVIOR:
    // geom3d::Point::Grid g(decoys.get_crds()); // Wrong!
  }
  SECTION("Grid wrapper") {
    // Molecule doesn't have .crd() interface
    const Molecule::PVec vec_of_mols = decoys.get_molecules();
    // ... so let's wrap it with adapter
    const Molecule::GridWrapped wrapped =
        grid::GridWrapper(vec_of_mols, [](const Molecule *m) -> geom3d::Point {
          return algorithms::compute_geometric_center(*m);
        }).get_wrapped();
    // we can now create a Grid with wrapped molecules
    const Molecule::Grid g(wrapped);
    const auto crd = decoys.get_crds().front();
    REQUIRE(g.get_neighbors(crd, 20.0).size() == decoys.size());
    // ... and use neighbors search on molecules
    for (const auto &wrapee : g.get_neighbors(crd, 5.0)) {
      std::cout << "Neighbor: " << std::endl;
      std::cout << *wrapee->orig() << std::endl; // unwrap
    }
  }
}

TEST_CASE("Back references") {

  using namespace molib;

  const auto decoys =
      Molecules::Parser("files/decoys-mmp13-chunk38.json.gz",
                        Molecules::Parser::Options::all_models, 1)
          .parse();
  const auto &atom = *decoys.get_atoms().front();
  const auto &residue = *decoys.get_residues().front();
  atom.residue().set_resn("MIA");
  REQUIRE(residue.resn() == atom.residue().resn());
  REQUIRE(residue.resn() == "MIA");
}

TEST_CASE("Read gzipped file in chunks") {

  using namespace molib;

  const auto &[contents, new_position] = inout::read_file_to_str(
      "files/1all.pdb.gz", 0, inout::FileNotFound::panic, 1, "$");
  const auto [contents2, new_position2] = inout::read_file_to_str(
      "files/1all.pdb.gz", new_position, inout::FileNotFound::panic, 1, "$");
  const auto [contents3, new_position3] =
      inout::read_file_to_str("files/1all.pdb.gz", new_position2,
                              inout::FileNotFound::panic, 2, "^SOURCE");
  const auto [contents4, new_position4] =
      inout::read_file_to_str("files/1all.pdb.gz", new_position3,
                              inout::FileNotFound::panic, 1, "^KEYWDS");
  const auto [contents5, new_position5] =
      inout::read_file_to_str("files/1all.pdb.gz", new_position4,
                              inout::FileNotFound::panic, 1, "^EXPDTA");
  REQUIRE(
      contents ==
      R"(HEADER    LIGHT-HARVESTING PROTEIN                01-MAR-95   1ALL              
)");

  REQUIRE(
      contents4 ==
      R"(SOURCE   2 ORGANISM_SCIENTIFIC: ARTHROSPIRA PLATENSIS;                          
SOURCE   3 ORGANISM_TAXID: 118562;                                              
SOURCE   4 MOL_ID: 2;                                                           
SOURCE   5 ORGANISM_SCIENTIFIC: ARTHROSPIRA PLATENSIS;                          
SOURCE   6 ORGANISM_TAXID: 118562                                               
)");

  REQUIRE(
      contents5 ==
      R"(KEYWDS    LIGHT-HARVESTING PROTEIN, PHYCOBILIPROTEIN                            
)");
}

TEST_CASE("Molecules move constructor must correct back references") {

  using namespace molib;

  auto m = Molecules::Parser("files/decoys-mmp13-chunk38.mol2.gz").parse();
  REQUIRE(m.get_atoms().front()->molecules().get_atoms().size() ==
          m.get_atoms().size());
}

TEST_CASE("Test molecule copy constructor") {

  using namespace molib;

  auto decoy = Molecules::Parser("files/decoys-mmp13-chunk38.json.gz",
                                 Molecules::Parser::Options::all_models, 1)
                   .parse()
                   .first();
  Molecule copy{decoy};
  std::cout << "First atom's bonds' size = "
            << decoy.get_atoms().front()->get_bonds().size() << std::endl;
  REQUIRE(decoy.get_atoms().front()->get_bonds().size() ==
          copy.get_atoms().front()->get_bonds().size());
}
