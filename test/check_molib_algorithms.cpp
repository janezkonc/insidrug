#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include "insilab/ommiface.hpp"
#include <algorithm>
#include <filesystem>
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
namespace fs = std::filesystem;

TEST_CASE("Calculate atoms on the molecular surface of a protein") {

  using namespace molib;

  Molecules::Parser m("files/3bkl.pdb.gz",
                      Molecules::Parser::Options::skip_hetatm);
  Molecule molecule = m.parse().first();

  const auto [surface_atoms, surface_probes] =
      molib::algorithms::compute_surface_atoms(molecule.get_atoms());

  const auto &largest_surface_atoms = surface_atoms.at(0);
  Molecule surf(largest_surface_atoms);
  surf.write(fs::temp_directory_path() / fs::path("surf1.pdb.gz"));

  const auto &largest_surface_probes = surface_probes.at(0);
  Molecule psurf;
  for (int i = 1; const auto &probe : largest_surface_probes) {
    const auto &[crd, atoms] = *probe;
    psurf.add({}).add({}).add({}).add({}).add({}).add(
        Atom({.atom_number = i++, .crd = crd}));
  }
  psurf.write(fs::temp_directory_path() / fs::path("probes1.pdb.gz"));

  std::vector<std::size_t> sizes;
  std::ranges::transform(surface_probes, std::back_inserter(sizes),
                         [](const auto &vec) { return vec.size(); });
  auto sorted_sizes = sizes;
  std::ranges::stable_sort(sorted_sizes, std::ranges::greater());

  REQUIRE(largest_surface_atoms.size() == 4071);
  REQUIRE(largest_surface_probes.size() == 5309);
  REQUIRE(surface_atoms.size() == surface_probes.size());
  REQUIRE(surface_atoms.size() == 60);
  REQUIRE(sizes == sorted_sizes);
}

TEST_CASE("Search for rotatable bonds in multiple molecules and check that "
          "they do not change between versions") {
  using namespace molib;

  const auto decoy = Molecules::Parser("files/decoys-mmp13-chunk38.mol2.gz",
                                       Molecules::Parser::Options::all_models)
                         .parse()
                         .first()
                         .compute_atom_bond_types()
                         .build();
  // read "ground truth" - rotatable bonds calculated before the
  // overhaul of search_replace
  const auto ground = Molecules::Parser("files/decoys-mmp13-chunk38.json.gz",
                                        Molecules::Parser::Options::all_models)
                          .parse()
                          .first();
  const auto bonds = details::get_bonds_from<details::get_bonds_t::in>(decoy);
  const auto ground_bonds =
      details::get_bonds_from<details::get_bonds_t::in>(ground);
  std::vector<std::string> a, b;
  std::ranges::transform(bonds, std::back_inserter(a), &Bond::get_rotatable);
  std::ranges::transform(ground_bonds, std::back_inserter(b),
                         &Bond::get_rotatable);
  std::ranges::sort(a);
  std::ranges::sort(b);

  REQUIRE(a == b);
}

TEST_CASE("Find and label different substructures in a protein") {

  using namespace molib;

  const auto protein = Molecules::Parser("files/1all.pdb.gz")
                           .parse()
                           .first()
                           .compute_atom_bond_types()
                           .build();
  // connect atoms in standard residues (protein, nucleic)
  ommiface::connect_bonds_standard_residues(protein);

  auto sz{0u};

  // function that adds a property (based on rule) to each atom in the set of
  // matched atoms
  const auto apply_rule = [&sz](const auto &matched_rules) {
    for (const auto &[matched_atoms, rule] : matched_rules) {
      const auto &r = helper::split(rule, "=");
      assert(r.size() == 2);
      ++sz;
      for (const auto &patom : matched_atoms) {
        patom->add_property(r[1]);
      }
    }
  };

  const auto output_atoms = [](const auto &protein, const std::string &prop) {
    for (const auto &patom : protein.get_atoms()) {
      if (patom->has_property(prop))
        std::cout << *patom << std::endl;
    }
  };

  SECTION("Find and label atoms that form peptide bonds") {
    algorithms::replace_substructure(
        protein,
        {{{
              // a graph describing the substructure
              {"^N$#1", "^C$#2", ""},
          },
          {
              // a rule to apply to the matched atoms
              {"1,2:type=pept"},
          }}},
        apply_rule, [](const auto &atom, const auto &pseudo_atom) {
          return std::regex_search(atom.atom_name(),
                                   std::regex(pseudo_atom.get_smiles_label()));
        });
    output_atoms(protein, "pept");
    std::cout << "Number of peptide bonds = " << sz << std::endl;
    REQUIRE(sz == 319);
  }

  SECTION("Find and label atoms in six-membered rings") {
    algorithms::replace_substructure(
        protein,
        {{{
              {"#1", "#2", ""},
              {"#2", "#3", ""},
              {"#3", "#4", ""},
              {"#4", "#5", ""},
              {"#5", "#6", ""},
              {"#6", "#1", ""},
          },
          {
              {"1,2,3,4,5,6:type=6ring"},
          }}},
        apply_rule,
        // compare atoms function always returns true since there are no atom
        // labels in the graph
        [](const auto &atom, const auto &pseudo_atom) { return true; });
    output_atoms(protein, "6ring");
    std::cout << "Number of six-membered rings = " << sz << std::endl;
    REQUIRE(sz == 25);
  }

  SECTION("Find and label double bonds in CYC ligand") {
    // The result requires explanation, since it differs from what 2D picture of
    // CYC on RCSB PDB shows (I believe our method is right here). We find 15
    // double bonds, i.e., two COOH groups => 4 double bonds (we count both
    // delocalized bonds in each COO as double) and 11 double bonds in rings and
    // between rings. CHIMERA (taking as input CYC A from PDB), assigns IDATM
    // types, that there would be 15 double bonds too in the same places as we
    // find them (unfortunately CHIMERA doesn't show double bonds). Wikipedia
    // and CAS picture of PHYCOCYANOBILIN also show 15 double bonds (taking into
    // account the COO twist I mentioned) with one bond at a different location
    // from where we predict it. However, PDB and PubChem show 14 double bonds.
    // So, I measured in PyMOL the C-N distance where our method says it's a
    // double bond and PDB says it's a single bond, and the distance is 1.3 A,
    // which is consistent with a double bond.
    algorithms::replace_substructure(
        protein,
        {{{
              {"#1", "#2", "bo=2"},
          },
          {
              {"1,2:type=double"},
          }}},
        apply_rule,
        // compare atoms function always returns true since there are no
        // atom labels in the graph
        [](const auto &atom, const auto &pseudo_atom) {
          return atom.chain().chain_id() == "A" &&
                 atom.residue().resn() == "CYC";
        });
    output_atoms(protein, "double");
    std::cout << "Number of double bonds = " << sz << std::endl;
    REQUIRE(sz == 15);
  }
}
