#include "insilab/probisligands.hpp"
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;

TEST_CASE("Generate binding site residues from centroids (.json)") {
  std::cout << probisligands::from_centroids(
      "files/receptor_1allA_compound_1.pdb.gz", "files/centro_1allA.json.gz");
}

TEST_CASE("Generate binding site residues from centroids (.cen)") {
  std::cout << probisligands::from_centroids(
      "files/receptor_1allA_compound_1.pdb.gz",
      "files/probisdock_centro_1allA_compound_1.cen.gz");
}

TEST_CASE("Generate binding site residues from ligands (.pdb)") {
  std::cout << probisligands::from_ligand(
      "files/receptor_1allA_compound_1.pdb.gz", "files/cyc.pdb.gz", 4.0);
}
