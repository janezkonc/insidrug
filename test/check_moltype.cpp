#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;

TEST_CASE("Determine different biochemical components (check if correct number "
          "of each is detected)") {

  using namespace molib;

  for (const auto &[file, rest, num, is_of_type] :
       std::initializer_list<std::tuple<std::string, Residue::Type, std::size_t,
                                        std::function<bool(Residue::Type)>>>{
           {"files/1rmm.pdb.gz",
            Residue::Type::protein | Residue::Type::polypeptide |
                Residue::Type::nonstandard,
            2,
            [](Residue::Type rest) -> bool {
              return details::is_polypeptide(rest) &&
                     details::is_nonstandard(rest);
            }}, // modified protein (CWO, 32S)
           {"files/1all.pdb.gz",
            Residue::Type::small | Residue::Type::compound |
                Residue::Type::covalent,
            2,
            [](Residue::Type rest) {
              return details::is_compound(rest) && details::is_covalent(rest);
            }}, // covalently linked compound (CYC)
           {"files/4m8t.pdb.gz",
            Residue::Type::small | Residue::Type::compound |
                Residue::Type::covalent,
            1,
            [](Residue::Type rest) {
              return details::is_compound(rest) && details::is_covalent(rest);
            }}, // covalent inhibitor (RMM)
           {"files/1m8t.pdb.gz", Residue::Type::buffer, 10,
            [](Residue::Type rest) {
              return details::is_buffer(rest);
            }}, // nonspecific binders (HEZ)
           {"files/3sgj.pdb.gz", Residue::Type::glycan, 4,
            [](Residue::Type rest) {
              return details::is_glycan(rest);
            }}, // glycans
           {"files/2pcu.pdb.gz", Residue::Type::small | Residue::Type::compound,
            1,
            [](Residue::Type rest) {
              return details::is_compound(rest);
            }}, // amino acid ligand (ASP), peptide
           {"files/2p2n.pdb.gz",
            Residue::Type::small | Residue::Type::compound |
                Residue::Type::covalent,
            1,
            [](Residue::Type rest) {
              return details::is_compound(rest) && details::is_covalent(rest);
            }}, // amino acid covalently linked (ASP 7001) not
                // part of polymer
           {"files/351c.pdb.gz",
            Residue::Type::small | Residue::Type::cofactor |
                Residue::Type::covalent,
            1,
            [](Residue::Type rest) {
              return details::is_cofactor(rest) && details::is_covalent(rest);
            }}, // heme (HEM) covalently linked to protein
           {"files/4env.pdb.gz",
            Residue::Type::small | Residue::Type::cofactor |
                Residue::Type::covalent,
            4,
            [](Residue::Type rest) {
              return details::is_cofactor(rest) && details::is_covalent(rest);
            }}, // heme (HEM and HDD - alternate locations)
                // linked to protein through Fe
           {"files/2pmv.pdb.gz", Residue::Type::small | Residue::Type::cofactor,
            2,
            [](Residue::Type rest) {
              return details::is_cofactor(rest);
            }}, // cobalamin cofactor
           {"files/1ytf.pdb.gz", Residue::Type::nucleic | Residue::Type::DNA, 2,
            [](Residue::Type rest) {
              return details::is_DNA(rest);
            }}, // TATA box (DNA ligand)
       }) {
    std::cout << "........................... " << file
              << " ..........................." << std::endl;
    const auto molecule =
        Molecules::Parser(file).parse().first().compute_residue_types();
    // std::cout << IOmanip::pdb << IOmanip::extended << molecule << std::endl;
    std::size_t found_rest{0}, found_rest2{0};
    for (const auto &component_atoms :
         algorithms::split_biochemical_components(molecule)) {
      const auto component{Molecule(component_atoms)};
      if (rest == (Residue::Type::protein | Residue::Type::polypeptide |
                   Residue::Type::nonstandard)) {
        for (const auto &presidue : component.get_residues()) {
          if (presidue->rest() == rest)
            found_rest++;
          if (is_of_type(presidue->rest()))
            found_rest2++;
        }
      } else {
        if (component.get_residues()[0]->rest() == rest)
          found_rest++;
        if (is_of_type(component.get_residues()[0]->rest()))
          found_rest2++;
      }
    }
    REQUIRE(found_rest == num);
    REQUIRE(found_rest2 == num);
  }
}
