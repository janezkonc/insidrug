#include "insilab/program.hpp"
#include <filesystem>
#include <string>

using namespace insilab;
namespace fs = std::filesystem;

program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  /** Test parsing of command line arguments **/
  return program::ArgumentParser("This is test_argument_parser")
      .add_config({"config"}, "config.ini", "Read a configuration file [.ini]")
      .add_argument<int>({"i", "int"}, "Integer value", 1, false)
      .add_argument<bool>({"v", "verbose"}, "Control the verbosity", false,
                          false)
      .add_argument<double>({"n", "number"}, "Value allowed", 0.0, false,
                            {0.0, 100.0}) // check if value is allowed
      .add_argument<int>({"p", "probis"}, "Value range", 23, false, {},
                         {-1, 30}) // check if value is in range
      .add_argument<std::string>(
          {"probisdock_db_dir"},
          "A directory that contains a binding sites dataset (receptors, \n"
          "template ligands, centroids) obtained from the ProBiS-Dock \n"
          "Database (http://probis-dock-database.insilab.org). File names must "
          "\n"
          "follow the standard (this is default in ProBiS-Dock Database): \n"
          "\n"
          "  * receptor (protein): receptor_PDBID_CHAINID_*.pdb.gz\n"
          "  * template ligands:   predicted_ligands_PDBID_CHAINID_*.pdb.gz\n"
          "  * centroids:          probisdock_centro_PDBID_CHAINID_*.cen.gz\n",
          "")
      .add_argument<std::string>(
          {"r", "receptor_file"},
          "The protein to which small molecules will be \n"
          "docked ({.pdb|.cif|.json}[.gz])\n",
          "")
      .add_argument<std::string>(
          {"template_ligands_file"},
          "The template ligands for ProBiS-Score - provide when h > 0.0 \n"
          "(.pdb[.gz]|.mol2[.gz]|.json[.gz])\n",
          "")

      .add_argument<std::string>(
          {"centro_file"}, "Binding site centroids (.cen|.json[.gz]) ", "")
      .add_argument<std::string>({"tmp_dir"},
                                 "Directory in which intermediary files will \n"
                                 "be saved (use with --debug)\n",
                                 fs::temp_directory_path())
      .add_argument<std::vector<std::string>>(
          {"vector_arg"}, "A list of strings",
          {}) // default value can be an empty vector {}, or a vector of any
              // size {"a", "b",...}, but it cannot be a vector with empty
              // elements e.g., {""}
      .set_needed({"probis", "int"})
      .set_excluded({"probis", "int"}, {"number"})
      .add_option_group(
          "multi_receptor", {"probisdock_db_dir"},
          "Inverse docking of multiple ligands against multiple receptors")
      .add_option_group(
          "single_receptor",
          {"receptor_file", "template_ligands_file", "centro_file"},
          "Allows docking of multiple ligands against a single receptor")
      .set_excluded_option_group("multi_receptor", "single_receptor")
      .set_required_option_group("single_receptor")
      .parse(argc, argv);
}

int run(const program::ArgumentParser &args) {
  std::cout << "State of verbose flag = " << args.get<bool>("verbose")
            << std::endl;
  std::cout << "Value of n option = " << args.get<double>("number")
            << std::endl;
  std::cout << "Value of p option = " << args.get<int>("probis") << std::endl;
  std::cout << "Value of receptor option = "
            << args.get<std::string>("receptor_file") << std::endl;

  std::cout << "\nConfig file:\n\n" << args.print_config(true) << std::endl;

  return 0;
}

int main(int argc, char *argv[]) {
  return program::create(argc, argv, parse_arguments, run);
}
