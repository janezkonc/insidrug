#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/probis.hpp"
#include <algorithm>
#include <filesystem>
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
namespace fs = std::filesystem;
using namespace molib;

TEST_CASE("New ProBiS protein local structural similarity search algorithm") {

  Molecule protein =
      Molecules::Parser("files/3bkl.pdb.gz", io::Parser::Options::skip_hetatm)
          .parse()
          .first();

  Molecule DNA_RNA_hybrid =
      Molecules::Parser("files/124d.pdb.gz", io::Parser::Options::skip_hetatm)
          .parse()
          .first();

  SECTION("Calculate descriptors on (or near) the solvent accessible surface "
          "of a protein") {

    const auto &descs_all_surfaces = probis::generate_descriptors(protein);

    std::vector<std::string> mnsps;
    std::ranges::transform(
        descs_all_surfaces.at(0), std::back_inserter(mnsps),
        [](const auto &d) { return probis::details::from_mnsp(d.mnsp()); });

    REQUIRE(descs_all_surfaces.size() == 60);
    REQUIRE(descs_all_surfaces.at(0).size() == 1774);

    REQUIRE(std::ranges::count(mnsps, "acceptor") == 679);
    REQUIRE(std::ranges::count(mnsps, "donor") == 687);
    REQUIRE(std::ranges::count(mnsps, "acceptor_donor") == 115);
    REQUIRE(std::ranges::count(mnsps, "aliphatic") == 215);
    REQUIRE(std::ranges::count(mnsps, "pi") == 78);

    // atom_name=NE2, resn=HIS, resi=513
    REQUIRE(std::ranges::find_if(descs_all_surfaces.at(0), [](const auto &d) {
              return d.crd() == geom3d::Point{49.554, 46.228, 44.547} &&
                     d.mnsp() == (probis::details::mnsp_t::acceptor |
                                  probis::details::mnsp_t::donor);
            }) != descs_all_surfaces.at(0).end());
    // atom_name=ND1, resn=HIS, resi=513
    REQUIRE(std::ranges::find_if(descs_all_surfaces.at(0), [](const auto &d) {
              return d.crd() == geom3d::Point{51.189, 47.495, 43.97} &&
                     d.mnsp() == (probis::details::mnsp_t::acceptor |
                                  probis::details::mnsp_t::donor);
            }) != descs_all_surfaces.at(0).end());
    // atom_name=CG, resn=TRP, resi=574
    REQUIRE(std::ranges::find_if(descs_all_surfaces.at(0), [](const auto &d) {
              return d.crd() == geom3d::Point{44.828, 27.202, 39.544} &&
                     d.mnsp() == probis::details::mnsp_t::pi;
            }) != descs_all_surfaces.at(0).end());
    // atom_name=NE1, resn=TRP, resi=574
    REQUIRE(std::ranges::find_if(descs_all_surfaces.at(0), [](const auto &d) {
              return d.crd() == geom3d::Point{46.445, 27.045, 40.575} &&
                     d.mnsp() == probis::details::mnsp_t::donor;
            }) != descs_all_surfaces.at(0).end());
    // atom_name=SD, resn=MET, resi=86
    REQUIRE(std::ranges::find_if(descs_all_surfaces.at(0), [](const auto &d) {
              return d.crd() == geom3d::Point{59.495, 41.450, 66.661} &&
                     d.mnsp() == probis::details::mnsp_t::aliphatic;
            }) != descs_all_surfaces.at(0).end());
  }

  SECTION("Calculate descriptors on (or near) the solvent accessible surface "
          "of an DNA-RNA hybrid duplex") {

    const auto &descs_all_surfaces = probis::generate_descriptors(
        DNA_RNA_hybrid, probis::details::descriptor_rules_nucleic);

    std::vector<std::string> mnsps;
    std::ranges::transform(
        descs_all_surfaces.at(0), std::back_inserter(mnsps),
        [](const auto &d) { return probis::details::from_mnsp(d.mnsp()); });

    REQUIRE(descs_all_surfaces.size() == 1);
    REQUIRE(descs_all_surfaces.at(0).size() == 184);

    REQUIRE(std::ranges::count(mnsps, "acceptor") == 118);
    REQUIRE(std::ranges::count(mnsps, "donor") == 20);
    REQUIRE(std::ranges::count(mnsps, "acceptor_donor") == 6);
    REQUIRE(std::ranges::count(mnsps, "aliphatic") == 16);
    REQUIRE(std::ranges::count(mnsps, "pi") == 24);
  }

  SECTION("Calculate product graphs and find a set of matching descriptors "
          "using maximum clique algorithm") {

    const auto &descs_all_surfaces = probis::generate_descriptors(protein);

    const auto &product_vertices =
        probis::compute_tetrahedron_product_graph_vertices(
            descs_all_surfaces.at(0), descs_all_surfaces.at(0), 4.0, 6.0, 0.5);
    const auto &product_graphs =
        probis::compute_product_graphs(product_vertices, 12.0, 2.0);

    auto max_max_clique_sz{0uz};
    for (const auto &product_graph : product_graphs) {
      const auto &conn = glib::generate_adjacency_matrix(product_graph);
      const auto &max_clique = glib::algorithms::find_maximum_clique(conn);
      max_max_clique_sz = std::max(max_clique.size(), max_max_clique_sz);
      //   for (const auto &idx : max_clique) {
      //     std::cout << "Matched tetrahedrons:\n";
      //     const auto &pvertex = product_graph[idx];
      //     const auto &[pth1, pth2] = pvertex->similar_entities;
      //     for (auto i{0uz}; i < pth1->descriptor.size(); ++i) {
      //       const auto &d1 = *pth1->descriptor[i];
      //       const auto &d2 = *pth2->descriptor[i];
      //       std::cout << d1 << " <=> " << d2 << std::endl;
      //     }
      //   }
    }
    REQUIRE(product_vertices.first.size() == 728);
    REQUIRE(product_graphs.size() == product_vertices.first.size());
    REQUIRE(max_max_clique_sz == 63);
  }
}