#include "insilab/helper.hpp"
#include "insilab/parallel.hpp"
#include <atomic>
#include <cstdlib>
#include <ctime>
#include <filesystem>
#include <iostream>
#include <memory>
#include <numeric>
#include <string>
#include <thread>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
namespace fs = std::filesystem;
using namespace std::chrono_literals;

constexpr int workload = 1000000000;

struct Element {
  int popularity{};
  std::atomic_bool ready{};
  Element() {}
  void set_popularity(const int p) { popularity = p; }
  bool operator<(const Element &rhs) const {
    return popularity > rhs.popularity;
  }
  bool is_ready() { return ready; }
  void set_ready() { ready = true; }
};

struct ExampleJob : parallel::Job {
  bool is_executable() const override { return true; }
  std::shared_ptr<parallel::Result> execute() const override {
    int sum{};
    for (int i = 0; i < workload; ++i) {
      sum += i;
    }
    std::cout << "Hello from 'example job'! I will simulate some "
                 "work... sum = "
              << sum << std::endl;
    return nullptr;
  }
};

class CountJobsListener : public parallel::JobListener {
  std::atomic_int __jobs_count{};

public:
  void notify(const std::shared_ptr<parallel::Job> job,
              const std::shared_ptr<parallel::Result> result) {
    ++__jobs_count;
    std::cout << "Notify called! Running job count = " << __jobs_count
              << std::endl;
    if (this->jobs_done()) {
      // execute this only when the last subscribed job for this listener is
      // done
      std::cout << "Total number of jobs executed = " << __jobs_count
                << std::endl;
      std::cout << "Goodbye happy beppie!" << std::endl;
    }
  }
};

struct SpawnJob : parallel::Job {
  parallel::Jobs &jobs;
  SpawnJob(parallel::Jobs &jobs) : jobs(jobs) {}
  bool is_executable() const override { return true; }
  std::shared_ptr<parallel::Result> execute() const override {
    std::cout << "Hello from 'spawn job'. I will spawn three more jobs!"
              << std::endl;
    // listener that will count number of spawned jobs
    const auto count_jobs = std::make_shared<CountJobsListener>();
    for (int i = 0; i < 3; ++i) {
      jobs.push(std::make_shared<ExampleJob>(), count_jobs);
    }
    count_jobs->start_listening();
    return nullptr;
  }
};

struct WorkerJob : parallel::Job {
  std::map<int, std::shared_ptr<Element>> elements;
  helper::Cache<int, Element> &cache;
  WorkerJob(auto elements, auto &cache) : elements(elements), cache(cache) {}

  bool is_executable() const override {
    for (const auto &[_unused, val] : elements) {
      if (!val->is_ready())
        return false;
    }
    return true;
  }
  std::shared_ptr<parallel::Result> execute() const override {
    std::cout << "Hello from 'worker job'. I will sleep for random time and "
                 "then use some data from cache!"
              << std::endl;
    const int random_time = std::rand() % 100;
    std::this_thread::sleep_for(std::chrono::milliseconds(random_time));
    for (const auto [key, val] : elements) {
      std::cout << "popularity of element " << key << " is " << val->popularity
                << std::endl;
      cache.maybe_remove();
    }
    return nullptr;
  }
};

struct CacheJob : parallel::Job {
  parallel::Jobs &jobs;
  helper::Cache<int, Element> &cache;
  CacheJob(parallel::Jobs &jobs, auto &cache) : jobs(jobs), cache(cache) {}
  bool is_executable() const override { return true; }
  std::shared_ptr<parallel::Result> execute() const override {
    std::cout << "Hello from 'cache job'. I will spawn three more jobs!"
              << std::endl;
    // get from cache or create on cache 5 elements
    std::map<int, std::shared_ptr<Element>> elements;
    for (int i = 0; i < 5; ++i) {
      int random_key = std::rand() % 100;
      elements.emplace(random_key,
                       cache.get_or_create_value_pointer(random_key));
    }
    for (int i = 0; i < 3; ++i) {
      jobs.push(std::make_shared<WorkerJob>(elements, cache));
    }
    // set each element's ready state
    for (auto &[key, val] : elements) {
      std::this_thread::sleep_for(std::chrono::milliseconds(std::rand() % 10));
      val->set_ready();
    }

    return nullptr;
  }
};

struct DataExpensiveWorkerJob : parallel::Job {
  std::shared_ptr<std::vector<int>>
      p_data; // very BIG data (for example a file that was read from disk)
  DataExpensiveWorkerJob(const auto &p_data) : p_data(p_data) {}
  bool is_executable() const override { return true; }
  std::shared_ptr<parallel::Result> execute() const override {
    std::cout << "Hello from 'data expensive worker job'! I contain big data "
                 "and will stay around for some time"
              << std::endl;
    std::this_thread::sleep_for(2s);

    return nullptr;
  }
};

struct DataExpensiveJob : parallel::Job {
  parallel::Jobs &jobs;
  std::shared_ptr<std::vector<int>>
      p_data; // very BIG data (for example a file that was read from disk)
  DataExpensiveJob(parallel::Jobs &jobs)
      : jobs(jobs), p_data(std::make_shared<std::vector<int>>()) {}
  bool is_executable() const override { return true; }
  std::shared_ptr<parallel::Result> execute() const override {
    std::cout
        << "Hello from 'data expensive job'. I will spawn three more jobs "
           "and stay around for some time!"
        << std::endl;
    // initialize data
    for (int i = 0; i < 10000000; ++i) {
      p_data->push_back(i);
    }
    std::cout << "Data initialized in thread " << std::this_thread::get_id()
              << " Size of data = " << (p_data->size() / 1000000.0) << "M"
              << std::endl;

    for (int i = 0; i < 3; ++i) {
      jobs.push(std::make_shared<DataExpensiveWorkerJob>(p_data));
    }
    std::this_thread::sleep_for(100ms);
    return nullptr;
  }
};

TEST_CASE("Execute jobs in parallel") {
  constexpr int ncpu = 24;
  constexpr int njobs = 100;
  parallel::Jobs jobs(ncpu);

  SECTION("Simple parallel jobs") {
    // initialize worker threads
    std::vector<std::jthread> threads;
    for (int thread_id = 0; thread_id < ncpu; ++thread_id) {
      threads.emplace_back([&jobs, thread_id]() { jobs.start(thread_id); });
    }
    // place some jobs on the queue to be executed in parallel
    for (int i = 0; i < njobs; ++i) {
      jobs.push(std::make_shared<ExampleJob>());
    }
    // ensures worker threads don't exit before jobs are 'placed'
    jobs.set_no_more_jobs();
  }
  SECTION("Listener showing total number of jobs at the end") {
    // initialize worker threads
    std::vector<std::jthread> threads;
    for (int thread_id = 0; thread_id < ncpu; ++thread_id) {
      threads.emplace_back([&jobs, thread_id]() { jobs.start(thread_id); });
    }
    // initialize a listener that will count total number of jobs
    const auto count_jobs = std::make_shared<CountJobsListener>();
    // place some jobs on the queue to be executed in parallel
    for (int i = 0; i < njobs; ++i) {
      jobs.push(std::make_shared<ExampleJob>(), count_jobs);
    }
    // ensures that number of subscribed jobs is correct
    count_jobs->start_listening();
    // ensures worker threads don't exit before jobs are 'placed'
    jobs.set_no_more_jobs();
  }
  SECTION("Each job spawns three more jobs and registers a listener") {
    // initialize worker threads
    std::vector<std::jthread> threads;
    for (int thread_id = 0; thread_id < ncpu; ++thread_id) {
      threads.emplace_back([&jobs, thread_id]() { jobs.start(thread_id); });
    }
    // place some jobs on the queue to be executed in parallel
    for (int i = 0; i < njobs / 3; ++i) {
      jobs.push(std::make_shared<SpawnJob>(jobs));
      std::this_thread::sleep_for(100ms); // simulate slow reading
    }
    // ensures worker threads don't exit before jobs are 'placed'
    jobs.set_no_more_jobs();
  }
  SECTION("Parallel with cached intermediary results") {

    constexpr int max_cache_size = 20;

    // initialize worker threads
    std::vector<std::jthread> threads;
    for (int thread_id = 0; thread_id < ncpu; ++thread_id) {
      threads.emplace_back([&jobs, thread_id]() { jobs.start(thread_id); });
    }
    // cache for some intermediary results
    helper::Cache<int, Element> cache(max_cache_size);
    std::srand(std::time(nullptr));

    // place some jobs on the queue to be executed in parallel
    for (int i = 0; i < njobs / 3; ++i) {
      jobs.push(std::make_shared<CacheJob>(jobs, cache));
      std::this_thread::sleep_for(100ms); // simulate slow reading
    }
    // ensures worker threads don't exit before jobs are 'placed'
    jobs.set_no_more_jobs();
  }
  SECTION("Limit number of jobs that consume a lot of memory") {

    constexpr int max_jobs =
        10; // set this to 100 to see the difference in memory consumption
    parallel::Jobs limited_jobs(ncpu, max_jobs);

    // initialize worker threads
    std::vector<std::jthread> threads;
    for (int thread_id = 0; thread_id < ncpu; ++thread_id) {
      threads.emplace_back(
          [&limited_jobs, thread_id]() { limited_jobs.start(thread_id); });
    }
    // place some jobs on the queue to be executed in parallel
    for (int i = 0; i < njobs; ++i) {
      limited_jobs.push_wait(std::make_shared<DataExpensiveJob>(limited_jobs));
    }
    std::cout << "DONE INSERTING NEW JOBS" << std::endl;
    // ensures worker threads don't exit before jobs are 'placed'
    limited_jobs.set_no_more_jobs();
  }
}
