#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include <filesystem>
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
namespace fs = std::filesystem;

TEST_CASE("Write protein sequence in Fasta format") {

  using namespace molib;

  Molecule molecule = Molecules::Parser("files/1all.pdb.gz").parse().first();
  const auto &filename = fs::temp_directory_path() / fs::path("1all.fasta.gz");
  molecule.write(filename);
  const auto &str = inout::read_file_to_str(filename).first;
  REQUIRE(
      str.find(R"(SIVTKSIVNADAEARYLSPGELDRIKSFVTSGERRVRIAETMTGARERIIKQAGDQLF)"
               R"(GKRPDVVSPGGNAYGADMTATCLRDLDYYLRLITYGIVAGDVTPIEEIGVVGVREMYKSL)"
               R"(GTPIEAIAEGVRAMKSVATSLLSGADAAEAGSYFDYLIGAMS)") !=
      std::string::npos);
  REQUIRE(
      str.find(R"(MQDAITSVINSSDVQGKYLDASAIQKLKAYFATGELRVRAATTISANAANIVKE)"
               R"(AVAKSLLYSDVTRPGGNMYTTRRYAACIRDLDYYLRYATYAMLAGDPSILDERV)"
               R"(LNGLKETYNSLGVPIGATVQAIQAMKEVTAGLVGGGAGKEMGIYFDYICSGLS)") !=
      std::string::npos);
}

TEST_CASE("Write a small molecule as Json with extended information") {

  using namespace molib;

  Molecule molecule = Molecules::Parser("files/decoys-mmp13-chunk38.mol2.gz")
                          .parse()
                          .first()
                          .compute_atom_bond_types()
                          .build(); // here, build is not strictly necessary,
                                    // but it's good practice anyway
  const auto tmpfile = fs::temp_directory_path() / fs::path("onedecoy.json.gz");
  molecule.write(tmpfile.string());
}

TEST_CASE("Write small molecules as Json with extended information") {

  using namespace molib;

  const auto decoys =
      Molecules::Parser("files/decoys-mmp13-chunk38.mol2.gz").parse();
  for (auto &decoy : decoys) {
    decoy.compute_atom_bond_types();
  }
  SECTION(
      "Molecules in a single go as one Molecule per line (Molecules.name not "
      "written to file)") {
    const auto tmpfile =
        fs::temp_directory_path() / fs::path("decoys-1.json.gz");
    decoys.write(tmpfile.string());
    // parse it back and check that the number of molecules read matches those
    // written
    REQUIRE(Molecules::Parser(tmpfile.string(),
                              Molecules::Parser::Options::all_models)
                .parse()
                .size() == decoys.size());
  }
  SECTION("Each Molecule appended on a new line (no Molecules in Json)") {
    const auto tmpfile =
        fs::temp_directory_path() / fs::path("decoys-2.json.gz");
    fs::remove(tmpfile);
    for (const auto &decoy : decoys) {
      decoy.write(tmpfile.string(), Molecule::Writer::Options::append);
    }
    // parse it back and check that the number of molecules read matches those
    // written
    REQUIRE(Molecules::Parser(tmpfile.string(),
                              Molecules::Parser::Options::all_models)
                .parse()
                .size() == decoys.size());
  }
}

TEST_CASE("Write protein with small ligand in our Json format with extended "
          "information") {

  using namespace molib;

  Molecule molecule = Molecules::Parser("files/1all.pdb.gz")
                          .parse()
                          .first()
                          .generate_bio(Molecule::BioHowMany::all_bio)
                          .compute_residue_types()
                          .compute_atom_bond_types()
                          .build(); // here, build is required, since
                                    // generate_bio generates new molecule
  molecule.write(fs::temp_directory_path() / fs::path("1all.json.gz"));
}

TEST_CASE("Write a PDB file") {

  using namespace molib;

  Molecule molecule;

  molecule.add(Assembly())
      .add(Model())
      .add(Chain())
      .add(molib::Segment())
      .add(Residue({.resn = "ABC", .resi = 1}))
      .add(Atom({.atom_number = 1, .atom_name = "CA"}));
  molecule.add(Assembly())
      .add(Model())
      .add(Chain())
      .add(molib::Segment())
      .add(Residue({.resn = "ABC", .resi = 1}))
      .add(Atom({.atom_number = 2, .atom_name = "CB"}));

  SECTION("the shorter way - showcasing append") {
    const auto tmpfile = fs::temp_directory_path() / fs::path("test1.pdb.gz");
    fs::remove(tmpfile);
    molecule.write(tmpfile.string(), Molecule::Writer::Options::append);
  }
  SECTION("and the slightly longer way") {
    const auto tmpfile = fs::temp_directory_path() / fs::path("test2.pdb.gz");
    Molecules::Writer m(tmpfile.string());
    m.write(molecule);
  }
}
