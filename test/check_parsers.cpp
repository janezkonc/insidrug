#include "insilab/molib.hpp"
#include "insilab/probis.hpp"
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
using namespace molib;

TEST_CASE("Parse our Json file format that contains several molecules") {
  SECTION("One by one, each Molecule from its own line") {
    std::vector<int> v;
    Molecules::Parser m("files/decoys-mmp13-chunk38.json.gz",
                        Molecules::Parser::Options::all_models, 1);
    while (const auto decoys = m.parse()) {
      std::cout << io::IOmanip::pdb << decoys << std::endl;
      std::cout << "-------------------" << std::endl;
      v.push_back(decoys.size());
    }
    REQUIRE(v == std::vector{1, 1, 1, 1, 1, 1, 1, 1, 1});
  }
  SECTION("Two at a time") {
    std::vector<int> v;
    Molecules::Parser m("files/decoys-mmp13-chunk38.json.gz",
                        Molecules::Parser::Options::all_models, 2);
    while (const auto decoys = m.parse()) {
      std::cout << io::IOmanip::pdb << decoys << std::endl;
      std::cout << "-------------------" << std::endl;
      v.push_back(decoys.size());
    }
    REQUIRE(v == std::vector{2, 2, 2, 2, 1});
  }
  SECTION("All in one go") {
    Molecules::Parser m("files/decoys-mmp13-chunk38.json.gz",
                        Molecules::Parser::Options::all_models);
    const auto decoys = m.parse();
    REQUIRE(decoys.size() == 9);
  }
}

TEST_CASE("Parse ProBiS srf file (old format)") {
  auto surface = probis::Surface::Parser("files/1allA.srf.gz").parse();

  std::cout << surface.molecule() << std::endl;
  std::cout << surface.descriptors() << std::endl;
  std::cout << surface.probes() << std::endl;

  for (auto &psegment : surface.molecule().get_segments()) {
    std::cout << "Segment = " << psegment->seg_id() << std::endl;
  }

  for (auto idatm_type : surface.molecule().get_idatm_types()) {
    std::cout << "Idatm = " << idatm_type << std::endl;
  }
}

TEST_CASE("Parse ProBiS srf file (old format) with coordinates that cannot be "
          "split by whitespace") {
  auto surface = probis::Surface::Parser("files/1f5rA.srf.gz").parse();

  std::cout << surface.molecule() << std::endl;
  std::cout << surface.descriptors() << std::endl;
  std::cout << surface.probes() << std::endl;

  for (auto &psegment : surface.molecule().get_segments()) {
    std::cout << "Segment = " << psegment->seg_id() << std::endl;
  }

  for (auto idatm_type : surface.molecule().get_idatm_types()) {
    std::cout << "Idatm = " << idatm_type << std::endl;
  }
}

TEST_CASE("Parse PDB file") {
  Molecules::Parser m("files/1all.pdb.gz");
  Molecule molecule = m.parse().first();
  std::cout << molecule << std::endl;
}

TEST_CASE("Parse PDB file with waters and ions") {
  Molecules::Parser m("files/3bkl.pdb.gz");
  Molecule molecule = m.parse().first();
  std::cout << molecule << std::endl;
}

TEST_CASE("Parse PDB file with 107 molecules") {
  Molecules::Parser m("files/predicted_ligands_3max_A_compound_1.pdb.gz");
  Molecules mols = m.parse();
  std::cout << mols << std::endl;
  REQUIRE(mols.size() == 107);
}

TEST_CASE("Parse PDB file with 107 molecules in chunks of about 10") {
  Molecules::Parser m("files/predicted_ligands_3max_A_compound_1.pdb.gz",
                      Molecules::Parser::Options::all_models, 10);
  int total{};
  while (const Molecules mols = m.parse()) {
    total += mols.size();
    std::cout << "chunk size = " << mols.size() << std::endl;
  }
  REQUIRE(total == 107);
}

TEST_CASE("Parse SDF file") {
  Molecules::Parser m("files/65064.sdf.gz");
  Molecule molecule = m.parse().first();
  std::cout << molecule << std::endl;
}
