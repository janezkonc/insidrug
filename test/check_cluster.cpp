#include "insilab/cluster.hpp"
#include <iostream>
#include <random>
#include <string>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;

TEST_CASE("OPTICS density-based clustering") {
  const std::vector<std::string> vertices{"a", "b", "c", "d"};
  const std::map<std::pair<std::size_t, std::size_t>, double>
      pairwise_distances{{{0, 1}, 10.6}, {{0, 2}, 10.2}, {{0, 3}, 10.3},
                         {{1, 2}, 0.1},  {{1, 3}, 0.2},  {{2, 3}, 0.5}};
  const std::vector<double> scores{1.0, 2.0, 3.0, 4.0};

  const auto clusters = cluster::compute_density_based_clusters(
      pairwise_distances, scores, 1.0, 0.6, 2);
  for (auto id = 1uz; const auto &cluster : clusters) {
    for (const auto &v : cluster) {
      std::cout << "Cluster = " << id << " vertex = " << vertices[v]
                << std::endl;
    }
    ++id;
  }
  REQUIRE(clusters.size() == 2);
}

TEST_CASE("OPTICS density-based clustering with large random data") {
  constexpr std::size_t sz = 2000;     // number of vertices
  constexpr std::size_t n_part = 3;    // number of clusters
  constexpr double mean = 20.0;        // mean distance
  constexpr double sd = 10.0;          // standard deviation of distances
  constexpr double f = 100.0;          // inter-cluster dist. are f * d_intra
  constexpr double p_intra = 0.8;      // probability of edge within cluster
  constexpr double p_inter = 0.1;      // probability of edge between clusters
  constexpr double clus_rad = 10.0;    // radius of generated clusters
  constexpr std::size_t min_pts = 200; // minimium number of vertices in cluster

  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<> normal_distrib(mean, sd);
  std::uniform_real_distribution<> distrib01(0.0, 1.0);

  // let's create some clusters...
  std::map<std::pair<std::size_t, std::size_t>, double> pairwise_distances;
  for (auto i = 0uz; i < sz; ++i) {
    for (auto j = i + 1; j < sz; ++j) {
      if (i % n_part == j % n_part) {
        // both in the same cluster
        if (distrib01(gen) < p_intra)
          pairwise_distances[{i, j}] = normal_distrib(gen);
      } else {
        // both from different clusters
        if (distrib01(gen) < p_inter)
          pairwise_distances[{i, j}] = f * normal_distrib(gen);
      }
    }
  }
  std::vector<double> scores(sz);
  for (auto i = 0uz; i < sz; ++i) {
    scores[i] = distrib01(gen);
  }
  const auto clusters = cluster::compute_density_based_clusters(
      pairwise_distances, scores, clus_rad, clus_rad - 0.1, min_pts);

  REQUIRE(clusters.size() == 3);
}
