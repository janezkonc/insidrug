#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include <filesystem>
#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;
namespace fs = std::filesystem;

TEST_CASE("Filter molecule atoms by different selection expressions") {

  using namespace molib;
  using namespace molib::algorithms;

  Molecules::Parser m("files/1all.json.gz",
                      Molecules::Parser::Options::all_models, 1);
  Molecule molecule = m.parse().first();

  Molecule molecule_w_DNA = Molecules::Parser("files/1ytf.pdb.gz")
                                .parse()
                                .first()
                                .compute_residue_types();

  SECTION("Empty filter expression should return unchanged molecule") {
    const auto filtered = molecule.filter({});
    filtered.write(fs::temp_directory_path() / fs::path("empty.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 2586);
  }
  SECTION("Water but not protein 5.0 Angstroms around compound") {
    const auto filtered = molecule.filter({Key::REST, Residue::Type::compound,
                                           Key::AROUND, 5.0, Key::AND, Key::NOT,
                                           Key::REST, Residue::Type::protein});
    filtered.write(fs::temp_directory_path() /
                   fs::path("water_5_around_compound.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 6);
  }
  SECTION("Compound within 5.0 Angstroms of any protein atom") {
    const auto filtered =
        molecule.filter({Key::REST, Residue::Type::compound, Key::WITHIN, 5.0,
                         Key::OF, Key::REST, Residue::Type::protein});
    filtered.write(fs::temp_directory_path() /
                   fs::path("compound_within_5_of_protein.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 86);
  }
  SECTION("Grid version (faster) for compound within 5.0 Angstroms of any "
          "protein atom") {
    const auto protein = molecule.filter({Key::REST, Residue::Type::protein});
    const Atom::Grid g(protein.get_atoms());
    const auto filtered =
        molecule.filter({Key::REST, Residue::Type::compound, Key::WITHIN, 5.0,
                         Key::OF, Key::GRID, g});
    filtered.write(fs::temp_directory_path() /
                   fs::path("grid_compound_within_5_of_protein.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 86);
  }
  SECTION("Calpha atoms of protein and compound") {
    const auto filtered = molecule.filter(
        {Key::REST, Residue::Type::compound, Key::OR, Key::REST,
         Residue::Type::protein, Key::AND, Key::ATOM_NAME, "CA"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("calpha_and_compound.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 407);
  }
  SECTION("Calpha atoms of protein or phosphorous atoms of nucleic acid") {
    const auto filtered = molecule_w_DNA.filter(
        {Key::REST, Residue::Type::protein, Key::AND, Key::ATOM_NAME, "CA",
         Key::OR, Key::REST, Residue::Type::nucleic, Key::AND, Key::ATOM_NAME,
         "P"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("sparse_protein_nucleic.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 402);
  }
  SECTION("Tyrosine residues' Calphas of chain A") {
    const auto filtered = molecule.filter(
        {Key::REST, Residue::Type::protein, Key::AND, Key::CHAIN, "A", Key::AND,
         Key::ATOM_NAME, "CA", Key::AND, Key::RESN, "TYR"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("tyr_chain_A_calpha.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 8);
  }
  SECTION("Tyrosine and alanine residues' Calphas of chain A") {
    const auto filtered = molecule.filter({Key::REST,
                                           Residue::Type::protein,
                                           Key::AND,
                                           Key::CHAIN,
                                           "A",
                                           Key::AND,
                                           Key::ATOM_NAME,
                                           "CA",
                                           Key::AND,
                                           Key::RESN,
                                           "TYR",
                                           Key::OR,
                                           Key::REST,
                                           Residue::Type::protein,
                                           Key::AND,
                                           Key::CHAIN,
                                           "A",
                                           Key::AND,
                                           Key::ATOM_NAME,
                                           "CA",
                                           Key::AND,
                                           Key::RESN,
                                           "ALA"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("ala_tyr_chain_A_calpha.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 27);
  }
  SECTION("Parentheses protein begin end") {
    const auto filtered = molecule.filter(
        {Key::P_OPEN, Key::REST, Residue::Type::protein, Key::P_CLOSE});
    filtered.write(fs::temp_directory_path() /
                   fs::path("parentheses_protein_begin_end.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 2408);
  }
  SECTION("Parentheses Calpha protein begin end") {
    const auto filtered = molecule.filter(
        {Key::P_OPEN, Key::REST, Residue::Type::protein, Key::AND,
         Key::ATOM_NAME, "CA", Key::AND, Key::CHAIN, "A", Key::P_CLOSE});
    filtered.write(
        fs::temp_directory_path() /
        fs::path("parentheses_calpha_chainA_protein_begin_end.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 160);
  }
  SECTION("Byres parentheses Calpha protein begin end") {
    const auto filtered = molecule.filter(
        {Key::BYRES, Key::P_OPEN, Key::REST, Residue::Type::protein, Key::AND,
         Key::ATOM_NAME, "CA", Key::AND, Key::CHAIN, "A", Key::P_CLOSE});
    filtered.write(
        fs::temp_directory_path() /
        fs::path("byres_parentheses_calpha_chainA_protein_begin_end.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 1198);
  }
  SECTION("Byres no parentheses Calpha protein begin end") {
    const auto filtered = molecule.filter(
        {Key::BYRES, Key::REST, Residue::Type::protein, Key::AND,
         Key::ATOM_NAME, "CA", Key::AND, Key::CHAIN, "A"});
    filtered.write(
        fs::temp_directory_path() /
        fs::path(
            "byres_no_parentheses_calpha_chainA_protein_begin_end.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 1198);
  }
  SECTION("Residues (whole) 5 Angstroms around residue CYC of chain A with "
          "parentheses") {
    const auto filtered =
        molecule.filter({Key::BYRES, Key::P_OPEN, Key::RESN, "CYC", Key::AND,
                         Key::CHAIN, "A", Key::P_CLOSE, Key::AROUND,
                         5.0}); // 'around' can be used with or without
                                // parentheses on the left side
    filtered.write(fs::temp_directory_path() /
                   fs::path("binding_site_chain_A.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 194);
  }
  SECTION("Residues (whole) 5 Angstroms around residue CYC of chain A no "
          "parentheses") {
    const auto filtered = molecule.filter(
        {Key::BYRES, Key::RESN, "CYC", Key::AND, Key::CHAIN, "A", Key::AROUND,
         5.0}); // 'around' can be used with or without parentheses on the
                // left side
    filtered.write(fs::temp_directory_path() /
                   fs::path("binding_site_chain_A_no_parentheses.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 194);
  }
  SECTION("Binding site with ligand") {
    const auto filtered = molecule.filter(
        {Key::RESN, "CYC", Key::AND, Key::CHAIN, "A", Key::OR, Key::BYRES,
         Key::RESN, "CYC", Key::AND, Key::CHAIN, "A", Key::AROUND,
         5.0}); // expression with parentheses gives the same result, eg.,
                // resn CYC and chain A or byres (resn CYC and chain A)
                // around 5.0, but writing it without is more concise
    filtered.write(fs::temp_directory_path() /
                   fs::path("ligand_binding_site_chain_A.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 237);
  }
  SECTION("Everything within 5.0 Angstroms of CYC ligand in chain B") {
    const auto filtered = molecule.filter(
        {Key::BYRES, Key::ALL, Key::WITHIN, 5.0, Key::OF, Key::CHAIN, "B",
         Key::AND, Key::RESN,
         "CYC"}); // right side (after 'of') can be without parentheses
    filtered.write(fs::temp_directory_path() /
                   fs::path("all_within_5_of_cyc_chain_B.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 269);
  }
  SECTION("Just TYRs 5 Angstroms around residue CYC of chain A") {
    const auto filtered =
        molecule.filter({Key::BYRES, Key::RESN, "CYC", Key::AND, Key::CHAIN,
                         "A", Key::AROUND, 5.0, Key::AND, Key::RESN, "TYR"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("tyr_in_binding_site_chain_A.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 36);
  }
  SECTION("Calpha atoms of protein or CMA atom of CYC or everything but "
          "protein") {
    const auto filtered = molecule.filter({Key::REST,
                                           Residue::Type::protein,
                                           Key::AND,
                                           Key::ATOM_NAME,
                                           "CA",
                                           Key::OR,
                                           Key::REST,
                                           Residue::Type::compound,
                                           Key::AND,
                                           Key::ATOM_NAME,
                                           "CMA",
                                           Key::OR,
                                           Key::NOT,
                                           Key::P_OPEN,
                                           Key::REST,
                                           Residue::Type::protein,
                                           Key::OR,
                                           Key::REST,
                                           Residue::Type::compound,
                                           Key::P_CLOSE});
    filtered.write(
        fs::temp_directory_path() /
        fs::path("protein_calpha_or_CMA_of_CYC_or_all_not_protein.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 415);
  }
  SECTION("Everything within 5.0 Angstroms of ligand that is a separate object "
          "from receptor") {
    const auto receptor = molecule.filter({Key::REST, Residue::Type::protein});
    const auto ligand = Molecules::Parser("files/cyc.pdb.gz").parse().first();
    const auto filtered = receptor.filter(
        {Key::ALL, Key::WITHIN, 5.0, Key::OF, Key::CONTAINER, ligand});
    filtered.write(fs::temp_directory_path() /
                   fs::path("all_within_5_of_container_ligand.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 98);
  }
  SECTION("Just TYRs 5 Angstroms around ligand CYC where ligand is parsed "
          "separately from protein") {

    const auto receptor = molecule.filter({Key::REST, Residue::Type::protein});
    const auto ligand = Molecules::Parser("files/cyc.pdb.gz").parse().first();

    const auto filtered =
        receptor.filter({Key::BYRES, Key::CONTAINER, ligand, Key::AROUND, 5.0,
                         Key::AND, Key::RESN, "TYR"});
    filtered.write(fs::temp_directory_path() /
                   fs::path("container_ligand_around_5_and_tyr.pdb.gz"));
    REQUIRE(filtered.get_atoms().size() == 36);
  }
  SECTION("Filter entities by entity_id and compare filtered to the expected") {
    const auto components = algorithms::split_biochemical_components(molecule);
    std::vector<std::size_t> filtered_sz, expected_sz;
    for (auto entity_id{0uz}; entity_id < components.size(); ++entity_id) {
      const auto filtered =
          molecule.filter({Key::ENTITY, static_cast<int>(entity_id)});
      filtered_sz.push_back(filtered.get_atoms().size());
      expected_sz.push_back(components[entity_id].size());
    }
    REQUIRE(filtered_sz == expected_sz);
  }
}
