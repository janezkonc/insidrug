#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include <iostream>
#include <memory>
#include <queue>
#include <variant>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

using namespace insilab;

bool operator==(const std::vector<double> &lhs,
                const std::vector<double> &rhs) {
  if (lhs.size() != rhs.size()) {
    return false;
  }
  for (std::size_t i = 0; i < lhs.size(); ++i) {
    if (!helper::approx_equal(lhs[i], rhs[i], 0.01)) {
      return false;
    }
  }
  return true;
}

bool operator==(const std::vector<std::vector<double>> &lhs,
                const std::vector<std::vector<double>> &rhs) {
  for (std::size_t i = 0; i < lhs.size(); ++i)
    if (!operator==(lhs[i], rhs[i]))
      return false;
  return true;
}

template <auto Start, auto End, auto Inc, class F>
constexpr void constexpr_for(F &&f) {
  if constexpr (Start < End) {
    f(std::integral_constant<decltype(Start), Start>());
    constexpr_for<Start + Inc, End, Inc>(f);
  }
}

TEST_CASE("Maximum clique search and maximum weighted clique search") {
  const auto &[adjacency_matrix, _unused] = glib::io::read_dimacs_graph(
      "files/unweighted_graph_clique_size_500.clq"); /* test.clq from mcqd */

  using Policy = std::variant<glib::algorithms::LowestWeightPolicy,
                              glib::algorithms::HighestWeightPolicy>;
  const auto weighted_graphs = std::initializer_list{
      std::tuple{glib::io::read_dimacs_graph(
                     "files/docking_weighted_graph.clq"), /* docking 3max */
                 Policy{glib::algorithms::LowestWeightPolicy{}}, 3},
      {glib::io::read_dimacs_graph("files/random_int_weighted_70_0.8_10_2.clq"),
       Policy{glib::algorithms::HighestWeightPolicy{}}, 17},
      {glib::io::read_dimacs_graph(
           "files/random_double_weighted_70_0.8_10_2.clq"),
       Policy{glib::algorithms::HighestWeightPolicy{}}, 16},
      {glib::io::read_dimacs_graph(
           "files/random_double_negative_weighted_100_0.8_100_20.clq"),
       Policy{glib::algorithms::LowestWeightPolicy{}}, 18},
  };

  const auto size_weighted_graphs = 4;

  SECTION("A maximum clique in an undirected, unweighted graph") {
    helper::Benchmark<std::chrono::milliseconds> b;
    const auto &clq = glib::algorithms::find_maximum_clique(adjacency_matrix);
    std::cout << "Maximum clique calculation took " << b.duration() << " ms"
              << std::endl;
    REQUIRE(clq.size() == 500);
  }
  SECTION("A maximum weight clique in an undirected, weighted graph") {
    helper::Benchmark<std::chrono::milliseconds> b;
    std::vector<std::pair<std::size_t, std::string>> results;

    constexpr_for<0, size_weighted_graphs, 1>([&weighted_graphs,
                                               &results](auto i) {
      auto it = weighted_graphs.begin();
      std::advance(it, i);
      const auto &[graph, weight_policy, _unused] = *it;

      const auto &[adjacency_matrix_w, weights] = graph;
      auto add_max_weight_clique_to_results = [&](auto weight_policy) {
        const auto &[clq, w] = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQDWStrategy, decltype(weight_policy)>(
            adjacency_matrix_w, weights);
        results.push_back({clq.size(), helper::dtos(w, 3)});
        std::cout << "Found maximum weight clique with size of " << clq.size()
                  << " and weight of " << helper::dtos(w, 3) << std::endl;
      };
      if (std::holds_alternative<glib::algorithms::LowestWeightPolicy>(
              weight_policy)) {
        add_max_weight_clique_to_results(
            glib::algorithms::LowestWeightPolicy{});
      } else {
        add_max_weight_clique_to_results(
            glib::algorithms::HighestWeightPolicy{});
      }
    });
    std::cout << "Maximum weight clique calculation took " << b.duration()
              << " ms" << std::endl;
    REQUIRE(results ==
            std::vector{std::pair{std::size_t{3}, std::string{"-19.513"}},
                        {std::size_t{17}, std::string{"200.000"}},
                        {std::size_t{16}, std::string{"184.115"}},
                        {std::size_t{18}, std::string{"-1943.484"}}});
  }
  SECTION("Top N highest weight cliques with size equal K in an undirected, "
          "weighted graph") {
    constexpr auto N = 10;
    helper::Benchmark<std::chrono::milliseconds> b;
    std::vector<std::vector<double>> results;

    constexpr_for<0, size_weighted_graphs, 1>(
        [&weighted_graphs, &results, N](auto i) {
          auto it = weighted_graphs.begin();
          std::advance(it, i);
          const auto &[graph, weight_policy, K] = *it;
          const auto &[adjacency_matrix_w, weights] = graph;
          auto add_max_weight_clique_to_results = [&](auto weight_policy) {
            const auto &[cliques, calculated_weights] =
                glib::algorithms::find_n_highest_weight_k_cliques<
                    glib::algorithms::KCQDWStrategy, decltype(weight_policy)>(
                    adjacency_matrix_w, weights, K, N);
            results.push_back(calculated_weights);
            std::cout << "Found top " << calculated_weights.size()
                      << " highest weight cliques with size equal " << K
                      << std::endl;
          };
          if (std::holds_alternative<glib::algorithms::LowestWeightPolicy>(
                  weight_policy)) {
            add_max_weight_clique_to_results(
                glib::algorithms::LowestWeightPolicy{});
          } else {
            add_max_weight_clique_to_results(
                glib::algorithms::HighestWeightPolicy{});
          }
        });

    std::cout << "K-weighted clique calculation took " << b.duration() << " ms"
              << std::endl;
    // calculated by exhaustive brute-force algorithm
    REQUIRE(operator==(
        results,
        std::vector{std::vector{-19.5127, -19.0698, -18.9594, -18.8382,
                                -18.7833, -18.7726, -18.7676, -18.7543,
                                -18.7425, -18.7399},
                    {200, 197, 196, 196, 194, 193, 192, 191, 191, 191},
                    {184.115, 183.379, 183, 182.726, 182.641, 182.51, 182.044,
                     181.581, 181.533, 181.342},
                    {-1943.48, -1922.56, -1907.68, -1899.14, -1887.43, -1886.76,
                     -1878.8, -1872.04, -1866.49, -1865.81}}));
  }
}

TEST_CASE("Shortest path matrix algorithms on random graph") {

  constexpr auto N{500};   // number of vertices of a graph
  constexpr auto P{0.001}; // probability of edge

  using namespace molib;
  class Vertex : public helper::template_vector_container<Vertex> {};
  std::vector<std::unique_ptr<Vertex>> graph;
  for (auto i{0uz}; i < N; ++i) {
    graph.emplace_back(std::make_unique<Vertex>());
  }

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> distrib01(0.0, 1.0);

  for (auto &pu : graph) {
    for (auto &pv : graph) {
      if (distrib01(gen) < P && pu != pv) {
        pu->add(&*pv);
        pv->add(&*pu);
      }
    }
  }
  helper::Benchmark<std::chrono::milliseconds> b;
  auto sp1 = glib::algorithms::compute_shortest_path_matrix(graph);
  std::cout << "Shortest path calculation (Floyd-Warshall) took "
            << b.duration() << " ms" << std::endl;
  b.reset();
  const auto sp2 =
      glib::algorithms::compute_shortest_path_matrix_sparse_graph(graph);
  std::cout << "Shortest path calculation (BFS) took " << b.duration() << " ms"
            << std::endl;

  REQUIRE(sp1 == sp2);
}

TEST_CASE("Number of rings in a molecule - CWO has 30+ rings, our method does "
          "not find all") {

  using namespace molib;
  // there are more rings for CWO in reality and it is expected that this test
  // (for CWO) fails sometimes
  for (const auto &[file, n_rings] : {
           std::pair{"files/CWO.mol2.gz", 28},
           {"files/CYC.mol2.gz", 4},
           {"files/HEM.mol2.gz", 8},
           {"files/TES.mol2.gz", 4},
           {"files/ADM.mol2.gz", 3}, // should be 4, but ok...
       }) {
    const auto molecule = Molecules::Parser(file).parse().first().build();
    const auto rings = glib::algorithms::find_rings(molecule.get_atoms());
    std::cout << "number of rings = " << rings.size() << std::endl;
    REQUIRE(rings.size() >= n_rings);
  }
}
