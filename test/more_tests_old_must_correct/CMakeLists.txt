include_directories($CMAKE_CURRENT_SOURCE_DIR)
include_directories( ${CMAKE_CURRENT_BINARY_DIR} )

set(MAJOR_VERSION 1)
set(MINOR_VERSION 1)
set(TWEAK_VERSION 1)

configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/version.hpp.in
    ${CMAKE_CURRENT_BINARY_DIR}/version.hpp
)

set( test_libraries
  molecule
  fragmenter
  liblisica
  graph
  geom3d
  helper
  data
)

set( additional_libraries
  -Wl,--whole-archive pthread -Wl,--no-whole-archive
  dl
  ssl
  crypto
  z
)

add_executable( test_version test_version.cpp)

set( all_libraries
  ${test_libraries}
  ${Boost_LIBRARIES}
  ${Boost_date_time_LIBRARY}
  ${Boost_filesystem_LIBRARY}
  ${Boost_regex_LIBRARY}
  ${Boost_system_LIBRARY}
  ${GSL_LIBRARIES}
  ${additional_libraries}
  )

target_link_libraries( test_version ${all_libraries} )
