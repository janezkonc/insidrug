#include "program/program.hpp"
#include "version.hpp"
#include <string>

int program::run() {
  /** Test version and banner **/
  std::cout << version::get_banner() << std::endl
            << version::get_run_info(1) << std::endl
            << version::get_version() << std::endl;

  return 0;
}

int main(int argc, char *argv[]) { return program::run(); }
