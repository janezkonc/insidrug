- Install required libraries:

    `sudo apt-get install g++ cmake libgsl-dev libboost-all-dev libssl-dev`

    > Required compiler: gcc12 (support for C++23 standard)

- Download the latest source code:

    `git clone https://gitlab.com/janezkonc/insidrug.git`

- Move to the top directory:

    `cd insidrug`

- Create the build directory:

    `cmake . -Brelease -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++`

- Move to the build directory:

    `cd release`

- To see different compile targets:

    `make help`

- Compile one of the possible compile targets (e.g., on 8 CPUs):

    ```
    make -j8 probisdock
    make -j8 maxcliquedynweight
    ...
    ```
