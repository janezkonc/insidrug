#pragma once

#include "genprobis/clinvar.hpp"
#include "genprobis/mapping.hpp"
#include "genprobis/pdbanno.hpp"
#include "genprobis/pharmgkb.hpp"
#include "genprobis/sifts.hpp"
#include "genprobis/uni.hpp"
#include "genprobis/uniprotvariants.hpp"
