#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "convert_bresi_format: Convert between .json and .txt format")
      .add_argument<std::string>(
          {"ifile", "i"}, "Input bresi file [{.txt,.json}[.gz]", {}, true)
      .add_argument<std::string>(
          {"ofile", "o"}, "Output bresi file [{.txt,.json}[.gz]", {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  const auto bresi_j =
      probisligands::parse_bresi_file(args.get<std::string>("ifile"));
  probisligands::write_bresi_file(args.get<std::string>("ofile"), bresi_j);

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
