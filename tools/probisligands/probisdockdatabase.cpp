#include "insilab/centro.hpp"
#include "insilab/geom3d.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>
#include <thread>
#include <typeinfo>
#include <vector>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "ProBiS-Dock Database: Prepare docking-ready binding site for "
             "small molecules (divided into substrate- and cofactor- "
             "competitive binding sites) in receptor (biological assembly) "
             "for a given PDB protein.")
      .add_argument<std::string>({"receptor_file"},
                                 "Receptor filename (.pdb|.cif|.json[.gz])", "",
                                 true)
      .add_argument<std::string>(
          {"lig_clus_file"},
          "File with ligand clusters for a given PDB ID,Chain ID detected by "
          "probisligands (.json[.gz])",
          "", true)
      .add_argument<std::string>(
          {"predicted_ligands_file"},
          "Will contain generated substrate- and cofactor-competitive ligand "
          "clusters (.json[.gz])",
          "predicted_ligands_file.json.gz")
      .add_argument<std::string>(
          {"centroids_file"},
          "Will contain generated centroids for substrate- and "
          "cofactor-competitive binding sites (.json[.gz])",
          "centroids_file.json.gz")
      .add_argument<std::string>(
          {"bresi_file"},
          "Will contain generated binding site residues on the receptor for "
          "substrate- and cofactor-competitive binding sites (.json[.gz])",
          "bresi_file.json.gz")
      .add_argument<std::string>(
          {"vina_file"},
          "Will contain generated AutoDock Vina boxes for substrate- and "
          "cofactor-competitive binding sites (.json[.gz])",
          "vina_file.json.gz")
      .add_argument<std::string>(
          {"equivalent_file"},
          "Will contain equivalent, i.e., bind same ligands, substrate- and "
          "cofactor-competitive binding sites (.json[.gz])",
          "equivalent_file.json.gz")
      .add_argument<std::string>(
          {"accessory_file"},
          "Will contain accessory ligands for each determined substrate- and "
          "cofactor-competitive binding site (.json[.gz])",
          "accessory_file.json.gz")
      .add_argument<std::string>(
          {"bio_dir"}, "Directory with ProBiS-ligands bio database", "bio")
      .add_argument<double>({"centro_clus_rad"},
                            "Cluster radius for centroid centers", 3.0)
      .add_argument<double>({"moltype_tanimoto_cutoff"},
                            "Minimium tanimoto to consider a ligand to be a "
                            "cofactor or glycan",
                            0.8)
      .add_argument<unsigned>({"small"},
                              "A cutoff to consider a protein as a peptide", 20)
      .add_argument<int>(
          {"min_num_occ"},
          "Minimum number of occurences of water or ions in a binding site "
          "to count this a conserved binding site (applies to water and ion "
          "accessory ligands)",
          10)
      .add_argument<double>(
          {"min_water_cons"},
          "Minimum conservation score of waters in a binding "
          "site to count this a conserved binding site (applies to water "
          "accessory ligands)",
          0.6) // changed 0.7 -> 0.6 on JUL/9/2021 (to agree with JCIM paper)
      .add_argument<double>(
          {"max_overlap"},
          "Maximum overlap percentage to determine if a binding site is "
          "engulfed in e.g., a protein ligand",
          0.9)
      .add_argument<double>({"max_interatomic_distance"},
                            "Maximum distance that a binding site can extend "
                            "from protein surface into solvent",
                            8.0)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;

  // read ligands file first, check if exists, and exit early if not found
  // (see issue #30 in bslib@GitLab)
  const auto ligands_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("lig_clus_file")).first);

  // leave just the rest types we'll need
  const auto filtered_ligands_j =
      probisligands::filter(ligands_j, [](const nlohmann::json &item) -> bool {
        const auto &bsite_rest = item.at("bsite_rest").get<Residue::Type>();
        return details::is_compound(bsite_rest) ||
               details::is_cofactor(bsite_rest) ||
               details::is_water(bsite_rest) || details::is_ion(bsite_rest);
      });

  const auto ligands_vec = probisligands::vectorize_ligands(filtered_ligands_j);
  auto ligands_3d = probisligands::add_ligand_centro(
      ligands_vec, args.get<std::string>("bio_dir"));

  // initialize parsers for receptor (and ligands) and read the receptor
  // molecule(s)
  const molib::Molecule bio_assembly =
      molib::Molecules::Parser(args.get<std::string>("receptor_file"),
                               molib::Molecules::Parser::Options::first_model)
          .parse()
          .first()
          .generate_bio(molib::Molecule::BioHowMany::first_bio,
                        true) // output asymmetric unit if no bio
          .compute_residue_types(
              args.get<double>("moltype_tanimoto_cutoff"),
              args.get<unsigned>("small")) // find cofactors, glycans,
                                           // buffer in the PDB file
          .build();

  const auto substrate_cofactor_competitive =
      probisligands::determine_substrate_cofactor_competitive(
          ligands_3d[details::compound_v], ligands_3d[details::cofactor_v]);

  const auto components =
      probisligands::determine_components(ligands_3d, bio_assembly);

  nlohmann::json scored_j, centroids_j, bresi_j, vina_j, equivalent_j,
      accessory_j;

  const auto bsite_rest_v = std::vector<Residue::Type>{
      details::substrate_competitive_v, details::cofactor_competitive_v};
  for (auto i{0uz}; i < substrate_cofactor_competitive.size(); ++i) {
    const auto &ligands_same_type = substrate_cofactor_competitive[i];

    const auto scored = probisligands::score_binding_sites(
        ligands_same_type, args.get<int>("min_num_occ"),
        args.get<double>("min_water_cons")); // min_num_occ and min_water_cons
                                             // have no effect here

    const auto centroids = probisligands::generate_centroids(
        scored, args.get<double>("centro_clus_rad"));

    const auto trimmed_centroids = probisligands::trim_centroids(
        centroids, bio_assembly, args.get<double>("max_interatomic_distance"));

    const auto bresi = probisligands::generate_binding_site_residues(
        bio_assembly, trimmed_centroids);

    const auto vina_boxes =
        probisligands::generate_vina_boxes(trimmed_centroids);

    const auto [receptor, accessory] =
        probisligands::determine_compatible_accessory_ligands(
            trimmed_centroids, components, args.get<int>("min_num_occ"),
            args.get<double>("min_water_cons"), args.get<double>("max_overlap"),
            bsite_rest_v[i]);

    // find defect binding sites and discard
    std::set<std::size_t> indexes;
    for (auto j{0uz}; j < trimmed_centroids.size(); ++j) {
      if (trimmed_centroids[j].empty() || bresi[j].empty() ||
          receptor[j].empty()) {
        indexes.insert(j);
      }
    }
    const auto e_scored = probisligands::erase_empty(scored, indexes);
    const auto e_trimmed_centroids =
        probisligands::erase_empty(trimmed_centroids, indexes);
    const auto e_bresi = probisligands::erase_empty(bresi, indexes);
    const auto e_vina_boxes = probisligands::erase_empty(vina_boxes, indexes);
    const auto e_accessory = probisligands::erase_empty(accessory, indexes);

    // after erasing, because bs_id's change
    const auto equivalent =
        probisligands::generate_equivalent_binding_sites(e_scored);

    probisligands::add_bs_score_info(scored_j, e_scored, bsite_rest_v[i]);
    probisligands::add_bs_info(centroids_j, e_trimmed_centroids,
                               bsite_rest_v[i]);
    probisligands::add_bs_info(bresi_j, e_bresi, bsite_rest_v[i]);
    probisligands::add_bs_info(vina_j, e_vina_boxes, bsite_rest_v[i]);
    probisligands::add_bs_info(equivalent_j, equivalent, bsite_rest_v[i]);
    probisligands::add_bs_info(accessory_j, e_accessory, bsite_rest_v[i]);
  }

  // output docking-ready receptors with template ligands and centroids for
  // each binding site
  inout::output_file(scored_j, args.get<std::string>("predicted_ligands_file"));
  inout::output_file(centroids_j, args.get<std::string>("centroids_file"));
  inout::output_file(bresi_j, args.get<std::string>("bresi_file"));
  inout::output_file(vina_j, args.get<std::string>("vina_file"));
  inout::output_file(equivalent_j, args.get<std::string>("equivalent_file"));
  inout::output_file(accessory_j, args.get<std::string>("accessory_file"));

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
