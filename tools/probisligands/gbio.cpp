#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "gbio: Generate biological assemblies for a protein.")

      .add_argument<std::string>(
          {"qpdb_file"}, "Query PDB file (.pdb,.cif,.json[.gz])", "", true)
      .add_argument<std::string>(
          {"bio_file"},
          "Output biological units to this pdb file (.pdb,.cif,.json[.gz])", "",
          true)
      .add_argument<std::string>({"qcid"}, "Query chain id(s) ", "A")
      .add_argument<std::string>({"models"}, "Which models to read", "all",
                                 false, {"first", "all"})
      .add_argument<std::string>({"bio"}, "Generate biological assemblies ",
                                 "none", false, {"none", "first", "all"})
      .add_argument<bool>({"hydrogens"}, "Read hydrogens")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  probisligands::generate_biological_assemblies(
      args.get<std::string>("models"), args.get<bool>("hydrogens"),
      args.get<std::string>("qpdb_file"), args.get<std::string>("qcid"),
      args.get<std::string>("bio"), args.get<std::string>("bio_file"));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
