#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>
#include <mutex>
#include <thread>
#include <typeinfo>

////////////////// MOLTYPE - ASSIGN RESIDUE TYPES ///////////////////////////

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "moltype: Assign ligand types to molecules within a pdb file.\n\n")
      .add_argument<std::string>(
          {"i", "in_file"}, "Input receptor file in pdb format (.pdb[.gz])", "",
          true)
      .add_argument<std::string>({"o", "out_file"},
                                 "Output file with typed receptor (.json[.gz])")
      .add_argument<std::string>(
          {"bio_ass"}, "Generate biological assembly from PDB records", "asym",
          false, {"asym", "first"})
      .add_argument<std::string>({"models"},
                                 "Do you wish to read all models from the PDB "
                                 "file or just the first one?",
                                 "first", false, {"first", "all"})
      .add_argument<bool>({"hydrogens"}, "Read hydrogens", false, false)
      .add_argument<double>({"moltype_tanimoto_cutoff"},
                            "Minimium tanimoto to consider a ligand to be a "
                            "cofactor or glycan",
                            0.8)
      .add_argument<unsigned>({"small"},
                              "A cutoff to consider a protein as a peptide", 20)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  const auto which_bio = args.get<std::string>("bio_ass") == "first"
                             ? molib::Molecule::BioHowMany::first_bio
                             : molib::Molecule::BioHowMany::asym;
  const auto which_models =
      args.get<std::string>("models") == "all"
          ? molib::Molecules::Parser::Options::all_models
          : molib::Molecules::Parser::Options::first_model;
  const auto with_hydrogens = args.get<bool>("hydrogens")
                                  ? molib::Molecules::Parser::Options::hydrogens
                                  : 0;

  molib::Molecules::Parser(args.get<std::string>("in_file"),
                           which_models | with_hydrogens |
                               Molecules::Parser::Options::panic_bad_structure)
      .parse()
      .first()
      .generate_bio(which_bio, true)
      .compute_residue_types(args.get<double>("moltype_tanimoto_cutoff"),
                             args.get<unsigned>("small"))
      .write(args.get<std::string>("out_file"));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
