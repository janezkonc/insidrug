#include "insilab/geom3d.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/path.hpp"
#include "insilab/program.hpp"
#include <limits>
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "gsup: Rotate ligands determined in step20 to become ligands of "
             "the representative protein chain.\n"
             "Expects that protein chains have been clustered by sequence "
             "identity and that each member chain was aligned onto\n"
             "the representative chain with ProBiS algorithm.")

      .add_argument<std::string>(
          {"probis_json_file"},
          "A file with ProBiS alignments - cluster member chains are aligned "
          "onto the representative protein chain (.json[.gz])",
          "", true)
      .add_argument<std::string>(
          {"ligands_dir"}, "A directory with prepared ligands for each PDB ID",
          "", true)
      .add_argument<std::string>(
          {"bio_dir"},
          "A directory to output the ligands (biologically relevant) "
          "superimposed onto the representative protein chain",
          "bio")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  namespace fs = std::filesystem;

  // get representative PDB & Chain ID from ProBiS file name
  const auto fn =
      fs::path(args.get<std::string>("probis_json_file")).filename();
  const auto rep_pdb_chain_id = fn.extension().string() == ".json"
                                    ? fn.stem().string()         // .json
                                    : fn.stem().stem().string(); // .json.gz
  const std::string water_filename{
      path::join(args.get<std::string>("bio_dir"), rep_pdb_chain_id,
                 helper::to_string(Residue::Type::water) + ".json.gz")};

  inout::create_empty_file(water_filename);

  // read the alignments from json
  for (const auto &d : nlohmann::json::parse(
           inout::read_file_to_str(args.get<std::string>("probis_json_file"))
               .first)) {
    try { // if something goes wrong, e.g., a PDB file is not found, don't
          // exit (just complain)
      const std::string pdb_id = d.at("pdb_id").get<std::string>().substr(
          0, 4); // ProBiS outputs PDB ID + CHAIN ID as pdb_id...oh yeah..
      const std::string &chain_id = d.at("chain_id");
      std::string pdb_file =
          path::join(args.get<std::string>("ligands_dir"), pdb_id + ".json");
      if (!path::file_exists(pdb_file)) {
        pdb_file += ".gz"; // try gzipped
      }
      const auto ligands =
          Molecules::Parser(pdb_file, Molecules::Parser::Options::all_models)
              .parse()
              .rotate(geom3d::Matrix( // rotate according to best alignment
                          d.at("alignment").at(0).at("rotation_matrix"),
                          d.at("alignment").at(0).at("translation_vector")),
                      true) // inverse rotation
              .select([chain_id](const Molecule &ligand) {
                return ligand.get_comments()
                           .at("bs_chain_id")
                           .get<std::string>() == chain_id;
              }) // consider just ligands that bind to the rotated protein chain
              .build();
      // output rotated ligands for each cluster member pdb
      for (const auto &ligand : ligands) {
        const auto &comments_j = ligand.get_comments();
        const auto rest = comments_j.at("rest").get<Residue::Type>();
        if (details::is_water(rest)) { // water is special as there are so many
          ligand.write(water_filename, Molecule::Writer::Options::append);
        } else {
          const auto &filename = comments_j.dump() + ".json.gz";
          ligand.write(path::join(args.get<std::string>("bio_dir"),
                                  rep_pdb_chain_id, filename));
        }
      }
    } catch (const std::exception &e) {
      std::cerr
          << "[NOTE] An exception occurred during reading ProBiS's Json's "
             "alignments in "
          << args.get<std::string>("probis_json_file")
          << " reason: " << e.what() << " ... skipping ... " << std::endl;
    }
  }
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
