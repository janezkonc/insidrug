#include "insilab/probisligands.hpp"
#include "ProBiS.hpp"
#include "insilab/docker.hpp"
#include "insilab/geom3d.hpp"
#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include "insilab/path.hpp"
#include "insilab/program.hpp"
#include <iostream>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  const auto args =
      insilab::program::ArgumentParser(
          "ProBiS-ligands: Predict binding sites and their ligands by "
          "local structural similarity.")
          .add_argument<bool>({"noprobis"},
                              "Do not perform comparsion of binding sites "
                              "(disabled by default, meaning that if no switch "
                              "is give, probis comparison will be done)",
                              false, false)
          .add_argument<std::string>({"receptor_file"},
                                     "Receptor filename (.pdb[.gz])", "", true)
          .add_argument<std::vector<std::string>>(
              {"receptor_chain_id"}, "Chain ID(s) of the receptor", {"A"})
          .add_argument<std::string>({"bslib_file"},
                                     "Read binding sites library from this "
                                     "file",
                                     "bslibdb/bslib.txt")
          .add_argument<std::string>({"nosql_file"},
                                     "NoSql-formatted ProBiS alignments output "
                                     "file",
                                     "probis.nosql")
          .add_argument<std::string>(
              {"json_file"},
              "Json-formatted ProBiS alignments input/output file",
              "probis.json.gz")
          .add_argument<std::string>(
              {"filter_file"},
              "Each line contains PDB & Chain ID corresponding to a "
              "representative "
              "protein in the ProBiS-generated Json file of aligned proteins - "
              "these up-to-date representatives will be considered, others "
              "will be filtered out, e.g., 1allA A - note the format with "
              "Chain ID repeated twice!")
          .add_argument<std::vector<std::string>>(
              {"bsite_rest"}, "Binding site types to be determined",
              {"small", "protein", "nucleic", "ion", "water", "glycan",
               "protein.oligopeptide", "small.compound", "small.cofactor"})
          .add_argument<std::vector<double>>(
              {"epsilon"}, "Cluster radius for predicted ligands by probis",
              {5.0, 8.0, 8.0, 2.0, 2.0, 5.0, 6.0, 5.0, 3.0})
          .add_argument<std::vector<int>>(
              {"min_pts"},
              "The minimum number of points (for predicted "
              "ligands) required to form a cluster",
              {10, 10, 10, 10, 10, 10, 10, 10, 10})
          .add_argument<std::vector<double>>(
              {"min_z_score"},
              "Minimium z-score of ligands to be considered  in clustering",
              {2.5, 3.0, 3.0, 2.0, 2.5, 2.5, 2.5, 2.5, 2.5})
          .add_argument<std::vector<int>>({"min_aligned_near"},
                                          "Minimum number of aligned residues "
                                          "near ligand to keep this ligand",
                                          {3, 5, 4, 2, 3, 1, 3, 3, 3})
          .add_argument<double>({"cons_min_z_score"},
                                "Minimium z-score for computing evolutionary "
                                "conservation scores",
                                2.0)
          .add_argument<double>(
              {"align_min_z_score"},
              "Minimium z-score for computing multiple sequence alignment", 1.0)
          .add_argument<double>(
              {"dist_cutoff_aligned"},
              "Distance cutoff for determining if ligand is in the aligned "
              "region of the protein",
              5.0)
          .add_argument<double>(
              {"max_clus_rad"},
              "Maximum size (radius) of clusters for water and ion ligands",
              5.0)
          .add_argument<std::string>(
              {"bio_dir"}, "Directory with ProBiS-ligands bio database",
              "lig/bio")
          .add_argument<std::string>(
              {"lig_clus_file"},
              "Ligand clusters found by ProBiS are outputted to this file",
              "ligand_clusters.json.gz")
          .add_argument<double>(
              {"centro_clus_rad"},
              "Binding sites are composed of centroids that "
              "are clustered with this radius (higher value -> fewer centroids "
              "-> more granular representation of a binding site)",
              3.0)
          .add_argument<double>({"polymer_centro_clus_rad"},
                                "Large ligands are represented as centroids in "
                                "the clustering that determines binding sites",
                                3.0)
          .add_argument<std::string>(
              {"centro_out_file"},
              "Filename for outputting calculated centroids",
              "centroids.json.gz")
          .add_argument<std::string>({"bresi_file"},
                                     "Bresi file for output of binding site "
                                     "residues",
                                     "bresi.json.gz")
          .add_argument<std::string>(
              {"evo_file"},
              "Evo file for output of evolutionary conservation scores for "
              "query protein sequence",
              "evo.json.gz")
          .add_argument<std::string>(
              {"inte_file"},
              "Inte file to output interactions of ligands with repceptor "
              "residues",
              "inte.json.gz")
          .add_argument<std::string>(
              {"msa_file"},
              "Filename for output of generated multiple sequence alignment",
              "msa.json.gz")
          .add_argument<double>({"inte_dist"},
                                "Interaction distance between ligand and "
                                "receptor",
                                5.0)
          .add_argument<int>({"min_num_occ"},
                             "Minimum number of occurences of water or ions "
                             "(predicted ligands) in a binding site "
                             "to count this a conserved binding site",
                             10)
          .add_argument<double>({"min_water_cons"},
                                "Minimum conservation score of waters "
                                "(predicted ligands) in a binding "
                                "site to count this a conserved binding site",
                                0.6)
          .add_argument<double>({"max_interatomic_distance"},
                                "Maximum interatomic distance", 8.0)
          .add_argument<int>({"ncpu"}, "Number of CPUs to use concurrently", 1)
          .add_config({"config"},
                      "Read parameters from the specified parameter file.")
          .parse(argc, argv);

  // Check if binding sites to be determined are well defined (must be of same
  // size)
  const auto sizes = std::vector<std::size_t>{
      args.get<std::vector<std::string>>("bsite_rest").size(),
      args.get<std::vector<double>>("epsilon").size(),
      args.get<std::vector<int>>("min_pts").size(),
      args.get<std::vector<double>>("min_z_score").size(),
      args.get<std::vector<int>>("min_aligned_near").size()};
  if (!std::all_of(sizes.cbegin(), sizes.cend(),
                   [&sizes](const auto i) { return i == sizes.at(0); }))
    throw insilab::helper::Error(
        "[WHOOPS] Binding sites to be determined are ill-defined (i.e., "
        "bsite_rest, epsilon, min_pts, min_z_score, min_aligned_near should "
        "all have the same number of elements)!");

  // If requested, identify potential binding sites using ProBiS algorithm
  if (!args.get<bool>("noprobis")) {
    insilab::inout::output_file(
        "",
        args.get<std::string>(
            "nosql_file")); // probis local structural alignments
    const auto &rcv = args.get<std::vector<std::string>>("receptor_chain_id");
    const auto &receptor_chain_id =
        std::accumulate(rcv.begin(), rcv.end(), std::string{});
    ProBiS::compare_against_bslib(
        argc, argv, args.get<std::string>("receptor_file"), receptor_chain_id,
        args.get<std::string>("bslib_file"), args.get<int>("ncpu"),
        args.get<std::string>("nosql_file"),
        args.get<std::string>("json_file"));
  }
  return args;
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;
  using namespace insilab::molib::algorithms;

  helper::Benchmark b;

  // read the json with alignments obtained with ProBiS
  const auto probis_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("json_file")).first);

  // filter it so that it contains only the 'current' proteins
  const auto filtered_probis_j = [&args, &probis_j]() {
    if (args.get<std::string>("filter_file").empty())
      return probis_j;
    const auto filter_v = helper::split(
        inout::read_file_to_str(args.get<std::string>("filter_file")).first,
        "\n");
    const std::set<std::string> filter{filter_v.begin(), filter_v.end()};
    nlohmann::json result =
        probisligands::filter(probis_j, [&filter](const nlohmann::json &item) {
          return filter.contains(item.at("pdb_id").get<std::string>() + " " +
                                 item.at("chain_id").get<std::string>());
        });
    return result;
  }();

  // initialize parsers for receptor (and ligands) and read the receptor
  // molecule(s)
  const auto receptor =
      Molecules::Parser(args.get<std::string>("receptor_file"),
                        Molecules::Parser::Options::first_model |
                            Molecules::Parser::Options::skip_hetatm)
          .parse()
          .first()
          .filter({Key::REST, Residue::Type::protein})
          .filter(details::chains_to_exprvec(
              args.get<std::vector<std::string>>("receptor_chain_id")));

  algorithms::compute_idatm_type(receptor);
  // define the types of binding sites we want to generate
  const auto &bsite_rest_v = args.get<std::vector<std::string>>("bsite_rest");
  const auto &epsilon = args.get<std::vector<double>>("epsilon");
  const auto &min_pts = args.get<std::vector<int>>("min_pts");
  const auto &min_z_score = args.get<std::vector<double>>("min_z_score");
  const auto &min_aligned_near = args.get<std::vector<int>>("min_aligned_near");

  const auto evo_j = probisligands::generate_evolutionary_conservation(
      receptor, filtered_probis_j, args.get<double>("cons_min_z_score"));

  const auto msa_j = probisligands::generate_multiple_sequence_alignment(
      receptor, filtered_probis_j, args.get<double>("align_min_z_score"));

  inout::output_file(evo_j, args.get<std::string>("evo_file"));
  inout::output_file(msa_j, args.get<std::string>("msa_file"));

  nlohmann::json scored_j, centroids_j, interactions_j, bresi_j;

  for (auto i{0uz}; i < bsite_rest_v.size(); ++i) {
    const auto &bsite_rest = details::to_rest(bsite_rest_v.at(i));

    const auto ligands_j = probisligands::read_filter_transpose_ligands(
        receptor, filtered_probis_j, args.get<std::string>("bio_dir"),
        args.get<double>("dist_cutoff_aligned"), min_z_score.at(i),
        min_aligned_near.at(i), args.get<double>("polymer_centro_clus_rad"),
        details::rest_fn.at(bsite_rest),
        args.get<double>("max_interatomic_distance"));

    const auto clustered = probisligands::generate_ligand_clusters(
        receptor, ligands_j, args.get<double>("dist_cutoff_aligned"),
        epsilon.at(i), min_pts.at(i), args.get<double>("max_clus_rad"));

    const auto scored = probisligands::score_binding_sites(
        clustered, args.get<int>("min_num_occ"),
        args.get<double>("min_water_cons"));

    const auto centroids = probisligands::generate_centroids(
        scored, args.get<double>("centro_clus_rad"));

    const auto trimmed_centroids = probisligands::trim_centroids(
        centroids, receptor, args.get<double>("max_interatomic_distance"));

    const auto interactions = probisligands::generate_ligand_interactions(
        receptor, scored, args.get<double>("inte_dist"),
        args.get<std::string>("bio_dir"));

    const auto bresi = probisligands::generate_binding_site_residues(
        receptor, trimmed_centroids);

    // note: it is ensured that sizes of clustered (and scored), centroids,
    // trimmed_centroids, interactions, bresi, are the same!
    probisligands::add_bs_score_info(scored_j, scored, bsite_rest);
    probisligands::add_bs_info(centroids_j, trimmed_centroids, bsite_rest);
    probisligands::add_bs_info(interactions_j, interactions, bsite_rest);
    probisligands::add_bs_info(bresi_j, bresi, bsite_rest);
  }

  inout::output_file(scored_j, args.get<std::string>("lig_clus_file"));
  inout::output_file(centroids_j, args.get<std::string>("centro_out_file"));
  inout::output_file(interactions_j, args.get<std::string>("inte_file"));
  inout::output_file(bresi_j, args.get<std::string>("bresi_file"));

  std::clog << "ProBiS-ligands took " << b.duration() << " s" << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
