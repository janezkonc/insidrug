#include "insilab/geom3d.hpp"
#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"
#include <limits>
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "step20: Determine to which binding site each ligand \n"
             "(uniquely identified as a biochemical component) belongs.")

      .add_argument<std::string>({"bio_file"},
                                 "Biological assembly with determined "
                                 "biological components (.json[.gz])",
                                 "", true)
      .add_argument<std::string>(
          {"ligands_file"},
          "Output all ligands within biological assembly together with "
          "information to which binding site each ligand is bound (.json[.gz])",
          "", true)
      .add_argument<std::string>({"pdb_id"},
                                 "PDB ID of the biological assembly", "", true)
      .add_argument<double>(
          {"d", "dist"}, "Maximum distance between pairs of atoms to consider",
          5.0)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  using namespace insilab::molib::algorithms;

  const auto receptor =
      Molecules::Parser(args.get<std::string>("bio_file"),
                        Molecules::Parser::Options::all_models |
                            Molecules::Parser::Options::panic_bad_structure)
          .parse()
          .first();
  const auto components = algorithms::split_biochemical_components(receptor);
  std::map<Atom *, std::size_t> atom_to_entity_id;
  for (auto entity_id{0uz}; entity_id < components.size(); ++entity_id) {
    const auto &component_atoms = components[entity_id];
    for (const auto &patom : component_atoms)
      atom_to_entity_id[patom] = entity_id;
  }
  Molecules result;
  for (auto entity_id{0uz}; entity_id < components.size(); ++entity_id) {
    const auto component{Molecule(components[entity_id])};
    const auto &first_residue = *component.get_residues().front();

    if (details::is_notassigned(first_residue.rest()) ||
        details::is_nonstandard(first_residue.rest()) ||
        details::is_buffer(first_residue.rest()))
      continue;

    const auto num_component_atoms = component.get_atoms().size();
    // filter receptor atoms in model 1 that are < dist from ligand

    const auto close_receptor_atoms = receptor.filter<true>(
        {Key::ENTITY, static_cast<int>(entity_id), Key::AROUND,
         args.get<double>("dist"), Key::AND, Key::REST, Residue::Type::protein,
         Key::AND, Key::MODEL, 1});

    // count contacts with receptor by receptor's chain_id and sort
    // decreasingly
    std::map<std::pair<std::string, std::size_t>, int> freq;
    for (const auto &patom : close_receptor_atoms) {
      freq[std::pair{patom->chain().chain_id(), atom_to_entity_id.at(patom)}]++;
    }
    std::vector<std::tuple<std::string, int, int>> close_atom_counts;
    for (const auto &[key, count] : freq) {
      const auto &[bs_chain_id, bs_entity_id] = key;
      close_atom_counts.push_back({bs_chain_id, bs_entity_id, count});
    }
    std::ranges::sort(close_atom_counts, [](const auto &i, const auto &j) {
      return std::get<2>(i) > std::get<2>(j);
    });

    // determine binding site chain(s) for the ligand starting from those that
    // have more contacts
    for (const auto &[bs_chain_id, bs_entity_id, count] : close_atom_counts) {
      // calculate percent of ligand in contact with binding site
      const double coverage =
          static_cast<double>(count) / (count + num_component_atoms);

      // and make some hard-coded decisions which ligands are really bound to
      // receptor
      if (details::is_glycan(first_residue.rest()) ||
          details::is_covalent(first_residue.rest()) ||
          (details::is_cofactor(first_residue.rest()) &&
           (coverage > 0.5 || count > 20)) ||
          (details::is_water(first_residue.rest()) && count > 3) ||
          (details::is_polypeptide(first_residue.rest()) &&
           (coverage > 0.2 || count > 20)) ||
          (details::is_nucleic(first_residue.rest()) &&
           (coverage > 0.2 || count > 20)) ||
          (details::is_compound(first_residue.rest()) &&
           (coverage > 0.5 || count > 20)) ||
          (details::is_ion(first_residue.rest()) && count > 3) ||
          (details::is_oligopeptide(first_residue.rest()) &&
           (coverage > 0.2 || count > 10))) {

        result.add(component).set_comments(nlohmann::json{
            {"rest", first_residue.rest()},
            {"pdb_id", args.get<std::string>("pdb_id")},
            {"bs_chain_id", bs_chain_id},
            {"resn", first_residue.resn()},
            {"resi", first_residue.resi()},
            {"chain_id", component.get_chains().front()->chain_id()},
            {"model_id", component.get_models().front()->model_id()},
            {"assembly_id", component.get_assemblies().front()->assembly_id()},
            {"close_receptor_atoms", count},
            {"coverage", std::stod(helper::dtos(coverage))}});
      }
    }
  }
  result.write(args.get<std::string>("ligands_file"));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
