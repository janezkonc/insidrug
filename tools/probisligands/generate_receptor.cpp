#include "insilab/centro.hpp"
#include "insilab/geom3d.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>
#include <thread>
#include <typeinfo>
#include <vector>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "generate_receptor: Used by the web server to print to stdout the "
             "receptor for a binding site (protein chains that are near, do "
             "not overlap with binding site).")
      .add_argument<std::string>({"receptor_file"},
                                 "Receptor filename (.pdb|.cif|.json[.gz])", "",
                                 true)
      .add_argument<std::string>(
          {"centro_file"},
          "A centroids file that will be read containing substrate- and "
          "cofactor-competitive binding sites (.json[.gz])"
          "centroids_file.json.gz",
          "", true)
      .add_argument<std::size_t>({"bs_id"},
                                 "Binding site identifier to identify "
                                 "particular binding site in centroids file",
                                 {}, true)
      .add_argument<std::string>({"bsite_rest"},
                                 "Binding site residue type to identify "
                                 "particular binding site in centroids file",
                                 "", true)
      .add_argument<std::string>({"bio_assembly"},
                                 "Which assembly to generate from REMARK 350 "
                                 "lines in a header of a PDB file?",
                                 "first_bio", false,
                                 {"first_bio", "all_bio", "asym"})
      .add_argument<double>(
          {"max_overlap"},
          "Maximum overlap percentage to determine if a binding site is "
          "engulfed in e.g., a protein ligand (must be the same value as in "
          "probisdockdatabase)",
          0.9)
      .add_argument<double>({"moltype_tanimoto_cutoff"},
                            "Minimium tanimoto to consider a ligand to be a "
                            "cofactor or glycan",
                            0.8)
      .add_argument<unsigned>({"small"},
                              "A cutoff to consider a protein as a peptide", 20)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;

  const auto &what_to_generate = args.get<std::string>("bio_assembly");

  // initialize parsers for receptor (and ligands) and read the receptor
  // molecule(s)
  const Molecule bio_assembly =
      Molecules::Parser(args.get<std::string>("receptor_file"),
                        Molecules::Parser::Options::first_model)
          .parse()
          .first()
          .generate_bio(what_to_generate == "first_bio"
                            ? Molecule::BioHowMany::first_bio
                            : (what_to_generate == "all_bio"
                                   ? Molecule::BioHowMany::all_bio
                                   : Molecule::BioHowMany::asym),
                        true) // output asymmetric unit if no bio
          .compute_residue_types(args.get<double>("moltype_tanimoto_cutoff"),
                                 args.get<unsigned>("small"))
          .build();

  const auto components = probisligands::determine_components({}, bio_assembly);

  const auto centroids_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("centro_file")).first);

  const auto &args_bs_id = args.get<std::size_t>("bs_id");
  const auto &args_bsite_rest =
      details::to_rest(args.get<std::string>("bsite_rest"));

  const auto filtered_centroids = probisligands::filter(
      centroids_j,
      [&args_bs_id, &args_bsite_rest](const nlohmann::json &item) -> bool {
        const auto &bs_id = item.at("bs_id").get<std::size_t>();
        const auto &bsite_rest = item.at("bsite_rest").get<Residue::Type>();
        return bs_id == args_bs_id && bsite_rest == args_bsite_rest;
      });

  if (filtered_centroids.size() != 1) {
    throw helper::Error("[WHOOPS] Could not find bsite_rest and bs_id within "
                        "centro_file... Wrong binding site selected?");
  }
  std::vector<nlohmann::json> centro_vec;
  centro_vec.push_back(std::pair{filtered_centroids.at(0).at("data").at(0),
                                 filtered_centroids.at(0).at("data").at(1)});
  const auto [receptor_components, _unused] =
      probisligands::determine_compatible_accessory_ligands(
          centro_vec, components, {}, {}, args.get<double>("max_overlap"),
          args_bsite_rest);

  if (receptor_components.size() != 1)
    throw helper::Error(
        "[WHOOPS] Could not find a compatible receptor for a binding site!");

  // join receptor_components components into one molecule
  Molecules mols;
  for (const auto &component : receptor_components.front()) {
    mols.add(component);
  }
  Molecule receptor{mols.get_atoms()};
  std::cout << nlohmann::json(receptor) << std::endl;

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
