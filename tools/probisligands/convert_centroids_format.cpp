#include "insilab/centro.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "convert_centroids_format: Convert between .json and .cen format")
      .add_argument<std::string>(
          {"ifile", "i"}, "Input centroids file [{.cen,.json}[.gz]", {}, true)
      .add_argument<std::string>(
          {"ofile", "o"}, "Output centroids file [{.cen,.json}[.gz]", {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  const auto centroids_j = centro::parse_file(args.get<std::string>("ifile"));
  centro::write_file(args.get<std::string>("ofile"), centroids_j);

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
