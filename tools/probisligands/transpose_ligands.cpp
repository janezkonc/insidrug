#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "transpose_ligands: Generate rotated (transposed) ligands "
             "from the output of ProBiS-ligands.")
      .add_argument<std::string>(
          {"lig_clus_file"},
          "File with ligand clusters for a given PDB ID,Chain ID detected by "
          "probisligands (.json[.gz])",
          "", true)
      .add_argument<std::string>({"output_format"}, "Output format", "json",
                                 false, {"json", "pdb"})
      .add_argument<bool>({"all_atoms_protein_nucleic"},
                          "Do you want all atoms or only CA or P atoms?")
      .add_argument<bool>({"no_coordinates"},
                          "Don't add ligands' coordinates to the output, just "
                          "plain ligands file")
      .add_argument<bool>({"unique"}, "Make ligands unique by their resn")
      .add_argument<std::size_t>(
          {"bs_id"},
          "Binding site identifier to identify "
          "particular binding site in centroids file "
          "(if omitted consider ligands regardless of bs_id)",
          {})
      .add_argument<std::string>(
          {"bsite_rest"},
          "Binding site residue type to identify "
          "particular binding site in centroids file "
          "(if omitted consider ligands regardless of bsite_rest)",
          "")
      .add_argument<std::string>({"select_ligands"},
                                 "Select specific ligand(s) in a binding site "
                                 "by comparing their comments"
                                 "(select all ligands by default)",
                                 "[]")
      .add_argument<std::string>(
          {"bio_dir"}, "Directory with ProBiS-ligands bio database", "bio")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  auto ligands_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("lig_clus_file")).first);

  if (args.get<bool>("unique")) {
    for (auto &bsite_j : ligands_j) {
      nlohmann::json filtered_data;
      std::set<std::string> unique_resns;
      std::copy_if(bsite_j.at("data").begin(), bsite_j.at("data").end(),
                   std::back_inserter(filtered_data),
                   [&unique_resns](const nlohmann::json &item) {
                     const auto resn =
                         item.at("comments").at("resn").get<std::string>();
                     const auto [_unused, is_unique_resn] =
                         unique_resns.insert(resn);
                     return is_unique_resn;
                   });
      bsite_j.at("data") = filtered_data;
    }
  }

  const auto select_ligands_j =
      nlohmann::json::parse(args.get<std::string>("select_ligands"));

  if (!select_ligands_j.empty()) {
    for (auto &bsite_j : ligands_j) {
      nlohmann::json filtered_data;
      std::copy_if(bsite_j.at("data").begin(), bsite_j.at("data").end(),
                   std::back_inserter(filtered_data),
                   [&select_ligands_j](const nlohmann::json &item) {
                     const auto comments_j = item.at("comments");
                     return std::count(select_ligands_j.begin(),
                                       select_ligands_j.end(), comments_j);
                   });
      bsite_j.at("data") = filtered_data;
    }
  }

  if (!args.get<bool>("no_coordinates")) {
    for (auto &bsite_j : ligands_j) {
      const auto bs_id = bsite_j.at("bs_id").get<std::size_t>();
      if (args.get<std::size_t>("bs_id") &&
          args.get<std::size_t>("bs_id") != bs_id)
        continue;
      const auto bsite_rest = bsite_j.at("bsite_rest").get<std::string>();
      if (!args.get<std::string>("bsite_rest").empty() &&
          args.get<std::string>("bsite_rest") != bsite_rest)
        continue;
      for (auto &ligand_j : bsite_j.at("data")) {
        try {
          const auto ligand = probisligands::read_rotate_ligand(
              ligand_j, args.get<std::string>("bio_dir"),
              args.get<bool>("all_atoms_protein_nucleic"));
          ligand_j["rotated"] = ligand;
        } catch (const std::exception &e) {
          ligand_j["rotated"] = nullptr; // fixes issue #22 (see ProBiS-Fold's
                                         // GitLab) this is: file not found
                                         // due to rounding error in coverage
        }
      }
    }
  }
  if (args.get<std::string>("output_format") == "json")
    std::cout << ligands_j << std::endl;
  else { // pdb format
    for (const auto &bsite_j : ligands_j) {

      if (bsite_j.at("data").empty() ||
          !bsite_j.at("data").at(0).contains("rotated"))
        continue;
      std::cout << ">>> " << bsite_j.at("bsite_rest").get<std::string>() << " "
                << bsite_j.at("bs_id").get<std::size_t>() << std::endl;
      for (const auto &ligand_j : bsite_j.at("data")) {
        std::cout << ligand_j.at("rotated").get<molib::Molecule>();
      }
    }
  }
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
