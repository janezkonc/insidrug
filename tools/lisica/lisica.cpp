#include "insilab/lisica.hpp"
#include "insilab/glib.hpp"
#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/path.hpp"
#include "insilab/program.hpp"
#include <iostream>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             R"(LiSiCA: Ligand Similarity using Clique Algorithm

           __    ____  ___  ____  ___    __   
          (  )  (_  _)/ __)(_  _)/ __)  /__\
           )(__  _)(_ \__ \ _)(_( (__  /(__)\
          (____)(____)(___/(____)\___)(__)(__)

Terms of use:
============
  (1) This program is free for academic users who intend to use it
  solely for not-for-profit academic research activities.

  (2) For-profit organizations can contact us at konc@cmm.ki.si 
  for licensing information.

If you use this software, please cite:
=====================================
  LESNIK, Samo, STULAR, Tanja, BRUS, Boris, KNEZ, Damijan, GOBEC, Stanislav, 
  JANEZIC, Dušanka, KONC, Janez. LiSiCA: A Software for Ligand-based Virtual Screening 
  and its Application for the Discovery of Butyrylcholinesterase Inhibitors. 
  J. Chem. Inf. Model., 2015, 55, 1521-1528.
             )")
      .add_argument<std::string>(
          {"reference", "R"},
          "Path to the reference molecule file [{.mol2,.pdb,.cif,.json}[.gz]",
          {}, true)
      .add_argument<std::string>(
          {"target", "T"},
          "Path to the target molecules file [{.mol2,.pdb,.cif,.json}[.gz]", {},
          true)
      .add_argument<std::string>(
          {"top_ranked", "o"},
          "Path to the top ranked molecules file [{.mol2,.pdb,.cif,.json}[.gz]",
          "top_ranked.mol2", false)
      .add_argument<std::size_t>({"ncpu", "n"},
                                 "Number of CPUs to use concurrently (if not "
                                 "specified, all CPUs are used)",
                                 std::thread::hardware_concurrency(), false)
      .add_argument<std::size_t>({"dimension", "d"},
                                 "Initiate two- or three-dimensional screening",
                                 2, false, {2, 3})
      .add_argument<std::string>({"atom_type", "y"},
                                 "Which atom type to compare", "sybyl", false,
                                 {"sybyl", "idatm", "gaff", "element"})
      .add_argument<std::string>({"screen_type", "e"},
                                 "Search for tanimoto similarities or whether "
                                 "a fragment is present or not",
                                 "tanimoto", false, {"tanimoto", "fragment"})
      .add_argument<double>({"max_allow_dist", "m"},
                            "Maximum allowed atom spatial distance for the 3D "
                            "screening product graph measured in Angstroms",
                            1.0)
      .add_argument<insilab::glib::node_idx_t>(
          {"max_allow_path", "s"},
          "Maximum allowed shortest path size for the 2D screening product "
          "graph measured in the number of covalent bonds",
          1)
      .add_argument<std::size_t>(
          {"num_mols_out", "w"},
          "Number of highest ranked molecules to write to output", 0)
      .add_argument<std::size_t>({"num_conf_out", "c"},
                                 "Maximum allowed number of highest scoring "
                                 "conformations to be outputed",
                                 1)
      .add_argument<bool>({"hydrogens", "g"}, "Enables comparison of hydrogens")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .set_excluded({"max_allow_dist"}, {"max_allow_path"})
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  Molecules::Parser r(args.get<std::string>("reference"),
                      (args.get<bool>("hydrogens")
                           ? Molecules::Parser::Options::first_model |
                                 Molecules::Parser::Options::hydrogens
                           : Molecules::Parser::Options::first_model));

  Molecule reference = r.parse().first();

  Molecules::Parser t(args.get<std::string>("target"),
                      (args.get<bool>("hydrogens")
                           ? Molecules::Parser::Options::all_models |
                                 Molecules::Parser::Options::hydrogens
                           : Molecules::Parser::Options::all_models),
                      10);

  lisica::Lisica lisica(reference.get_atoms());

  lisica.set_num_cpu(args.get<std::size_t>("ncpu"));
  lisica.set_num_mols_out(args.get<std::size_t>("num_mols_out"));
  lisica.set_dimension(args.get<std::size_t>("dimension"));
  lisica.set_max_allow_dist(args.get<double>("max_allow_dist"));
  lisica.set_max_allow_path(args.get<glib::node_idx_t>("max_allow_path"));

  lisica::Lisica::CompareAtomTypes ctype;
  if (args.get<std::string>("atom_type") == "sybyl")
    ctype = lisica::Lisica::CompareAtomTypes::sybyl;
  else if (args.get<std::string>("atom_type") == "idatm")
    ctype = lisica::Lisica::CompareAtomTypes::idatm;
  else if (args.get<std::string>("atom_type") == "gaff")
    ctype = lisica::Lisica::CompareAtomTypes::gaff;
  else if (args.get<std::string>("atom_type") == "element")
    ctype = lisica::Lisica::CompareAtomTypes::element;
  lisica.set_compare_atom_types(ctype);

  lisica::Lisica::ScreenType stype;
  if (args.get<std::string>("screen_type") == "tanimoto")
    stype = lisica::Lisica::ScreenType::tanimoto;
  else if (args.get<std::string>("screen_type") == "fragment")
    stype = lisica::Lisica::ScreenType::fragment;

  lisica.set_screen_type(stype);

  dbgmsg("before running lisica");

  lisica.run(t);

  lisica::Lisica::TopScreened top = lisica.get_top();
  std::vector<std::pair<double, std::string>> tanimoto_list =
      lisica.get_tanimoto_list();

  if (!top.empty()) {
    inout::output_file(top, args.get<std::string>("top_ranked"));
  }

  std::cout << "_________________________LiSiCA Results "
               "Summary_________________________"
            << std::endl
            << std::endl;
  std::cout << "Rank\tTanimoto\tCompound" << std::endl;
  std::cout
      << "------------------------------------------------------------------"
         "---------"
      << std::endl;
  for (auto i{0uz}; i < tanimoto_list.size(); ++i)
    std::cout << i + 1 << "\t" << std::fixed << std::setprecision(6)
              << tanimoto_list[i].first << "\t" << tanimoto_list[i].second
              << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
