LiSiCA
========
LiSiCA (Ligand Similarity using Clique Algorithm) is a ligand-based virtual screening software that searches for 2D and 3D similarities between reference compound and a database of target compounds in Mol2 format. The similarities are expressed using the Tanimoto coefficients and the target compounds are ranked accordingly.

Terms of use
========
(1) This program is free for academic users who intend to use it solely for not-for-profit academic research activities.

(2) For-profit organizations can contact us at [konc@cmm.ki.si](mailto:konc@cmm.ki.si) for licensing information.

Installation
========

	Windows: No installation is required.

	Linux: Mark the file as executable: chmod +x lisica

	Source: Unzip the source code, cd to the 'lisica' folder and compile LiSiCA by typing 'make'. Remove compiled files by typing 'make clean'.

Usage
========
General use of LiSiCA:

	In Linux: ./lisica -R <path to reference molecule> -T <path to target molecules> [parameters];
	
	In Windows: LiSiCA{x86|x64}.exe -R <path to reference molecule> -T <path to target molecules> [parameters];

Parameters
========
LiSiCA uses the following optional parameters:

	-n <number of CPU threads> Default value: the default is to try to detect the number of CPUs or, failing that, use 1

	-d <product graph dimension> Possible input: 2, 3 or b; Default value: 2

	-m <maximum allowed atom spatial distance for the 3D product graph measured in angstroms> Default value: 1.0

	-s <maximum allowed shortest path size for the 2D product graph measured in the number of covalent bonds> Default value: 1
	
	-w <number of highest ranked molecules to write to output> Default value: 0
	
	-c <maximum allowed number of highest scoring conformations to be outputed> Default value: 1

	-sf <seeds file>

	-h Enables comparison of hydrogens Default value: disabled

	--help <print LiSiCA parameters>
	
Notes
========
Both the reference and the target compound files can be in the Tripos mol2 format. Alternatively, target database can be compressed mol2 file [.gz] using gzip program in linux. The mol2 files must contain the column with SYBYL atom types (e.g., C.2, C.3, O.3), since lisica depends on them. The reference file should contain only one (reference) compound; the target file may contain many compounds. If the reference file contains more than one compound, the first molecule is used as a reference. The molecules in the target file having the same name are considered as different conformers of the same molecule. Only the best-scoring (by Tanimoto coefficient) conformer will be shown by default in the final output for the 3D screening option. To save the final output (ranked list by Tanimoto coefficient) to a file in Linux, use the bash standard output redirection function (see examples below).

The -m option corresponds to the maximum allowed difference in distances between atoms of the two compared product graph vertices. Lesser values correspond to a more rigorous screening. This option can only be used in combination with the -d 3 option.

The -s option corresponds to the maximum allowed difference in shortest-path length between atoms of the two compared product graph vertices. Lesser values correspond to a more rigorous screening. This option can only be used in combination with the -d 2 option.

If the -w option is set to a value higher than 0, LiSiCA will create mol2 files of the highest scoring target molecules with a comment section at the end of the file where the matching atom pairs are displayed.

The -c option corresponds to the maximum number of outputted files of one molecule in different conformations.

Examples
========
2D screening with default options where the final results are saved to the results_2D.dat file:

	./lisica -R reference.mol2 -T database.mol2[.gz] > results_2D.dat
	
3D screening with default options where the final results are saved to the resultd_3D.dat:

	./lisica -R reference.mol2 -T database.mol2[.gz] -d 3 > results_3D.dat
	
Rigorous 3D screening:

	./lisica -R reference.mol2 -T database.mol2[.gz] -d 3 -m 0.5
	
3D screening where 30 highest scoring target molecules are written in mol2 files with their matching atoms (to the ones in the reference molecule) displayed in the comment section of the mol2 file. Two conformers for each molecule are allowed to be written in the mol2 files.

	./lisica -R reference.mol2 -T database.mol2[.gz] -d 3 -w 30 -c 2

References
========
	LEŠNIK, Samo, ŠTULAR, Tanja, BRUS, Boris, KNEZ, Damijan, GOBEC, Stanislav, JANEŽIČ, Dušanka, KONC, Janez. LiSiCA: A Software for Ligand-based Virtual Screening and its Application for the Discovery of Butyrylcholinesterase Inhibitors. J. Chem. Inf. Model., 2015, 55, 1521-1528.
