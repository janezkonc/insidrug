#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <cassert>
#include <numeric>
#include <string>
#include <vector>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "convert_format: convert between supported molecular formats")
      .add_argument<std::string>(
          {"ifile", "i"},
          "Input protein structure file [{.pdb,.cif,.mol2,.json}[.gz]", {},
          true)
      .add_argument<std::string>(
          {"ofile", "o"},
          "Output protein structure file [{.pdb,.cif,.mol2,.json,.fasta}[.gz]",
          {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  const auto molecules =
      Molecules::Parser(args.get<std::string>("ifile"),
                        Molecules::Parser::Options::all_models |
                            Molecules::Parser::Options::hydrogens)
          .parse();
  molecules.write(args.get<std::string>("ofile"));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
