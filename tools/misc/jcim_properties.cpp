#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/path.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"

/**
 * This program calculates the average structure complexity score for selected
 * small molecule binding sites that was requested by JCIM reviewers
 * (ProBiS-Fold, 2022).
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "remove_spaces_from_json: does what the name says :-)")
      .add_argument<std::string>({"probisdockdb_json_file"},
                                 "ProBiS-Dock DB Json file [.json[.gz]]", {},
                                 true)
      .add_argument<std::string>({"probisdockdb_dir"},
                                 "A directory with ProBiS-Dock DB ligand files",
                                 {}, true)
      .add_argument<int>({"min_compound_score"},
                         "Minimum compound score (Sbsite) to consider "
                         "binding sites",
                         90)
      .add_argument<double>({"max_min_beta_factor"},
                            "Minimum beta factor (AlphaFold's confidence) a "
                            "binding site must have to be considered",
                            std::numeric_limits<double>::max())
      .add_argument<double>({"min_min_beta_factor"},
                            "Minimum beta factor (AlphaFold's confidence) a "
                            "binding site must have to be considered",
                            90.0)
      .add_argument<bool>({"in_pdb"},
                          "True, if AlphaFold protein has "
                          "equivalent in PDB ID, false otherwise",
                          false)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  const auto probisdockdb_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("probisdockdb_json_file"))
          .first);


  const auto in_pdb = args.get<bool>("in_pdb");
  const auto min_compound_score = args.get<int>("min_compound_score");
  const auto min_min_beta_factor = args.get<double>("min_min_beta_factor");
  const auto max_min_beta_factor = args.get<double>("max_min_beta_factor");

  // leave just the rest types we'll need
  const auto filtered_probisdockdb_j = probisligands::filter(
      probisdockdb_j,
      [&in_pdb, &min_compound_score, &min_min_beta_factor,
       &max_min_beta_factor](const nlohmann::json &item) -> bool {
        const auto &bsite_rest = item.at("bsite_rest").get<Residue::Type>();
        const auto &pdb_id_j = item.at("pdb_id");
        const auto &compound_score = item.at("compound_score").get<int>();
        const auto &min_beta_factor = item.at("min_beta_factor").get<int>();
        return details::is_small(bsite_rest) &&
               (in_pdb && !pdb_id_j.is_null() ||
                !in_pdb && pdb_id_j.is_null()) &&
               compound_score > min_compound_score &&
               min_beta_factor > min_min_beta_factor &&
               min_beta_factor <= max_min_beta_factor;
      });
  std::cout << "number of binding sites = " << filtered_probisdockdb_j.size()
            << std::endl;
  // count the number of all small ligands in binding sites
  auto count_all{0uz};
  auto acc_score{0.0};
  for (const auto &bsite_j : filtered_probisdockdb_j) {
    try {
      const auto &bsite_rest = bsite_j.at("bsite_rest").get<Residue::Type>();
      const auto &bs_id = bsite_j.at("bs_id").get<int>();
      const auto &alphafold_id = bsite_j.at("alphafold_id").get<std::string>();

      const auto ligands_j = nlohmann::json::parse(
          inout::read_file_to_str(
              path::join(args.get<std::string>("probisdockdb_dir"),
                         "ligands_" + alphafold_id + ".json.gz"))
              .first);
      std::cout << "number of ligands = " << ligands_j.size() << std::endl;
      const auto filtered_bsites_j = probisligands::filter(
          ligands_j, [&bsite_rest, &bs_id](const nlohmann::json &item) {
            return item.at("bsite_rest").get<Residue::Type>() == bsite_rest &&
                   item.at("bs_id").get<int>() == bs_id;
          });
      std::cout << "filtered bsites = " << filtered_bsites_j.size()
                << std::endl;
      for (const auto &item : filtered_bsites_j) {
        for (const auto &ligand_j : item.at("data")) {
          const auto &complex_score =
              ligand_j.at("complex_score").get<double>();
          acc_score += complex_score;
        }
        count_all += item.at("data").size();
      }
    } catch (const std::exception &e) {
      std::cerr << e.what() << std::endl;
    }
  }

  std::cout << "Number of all ligands that fulfill input criteria: "
            << count_all << std::endl;
  std::cout << "Accumulated complex score: " << acc_score << std::endl;
  std::cout << "Average complex score: " << acc_score / count_all << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
