#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/glib/test.hpp"
#include "insilab/helper.hpp"
#include "insilab/program.hpp"

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "randomize_graph: Randomize vertices of a (un)weighted graph.")
      .add_argument<std::string>(
          {"input_graph_file", "i"},
          "Input (un)weighted graph file in DIMACS format", {}, true)
      .add_argument<std::string>(
          {"output_graph_file", "o"},
          "Output randomized (un)weighted graph file in DIMACS format", {},
          true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  const auto &[adjacency_matrix, weights] =
      glib::io::read_dimacs_graph(args.get<std::string>("input_graph_file"));
  const auto &[shuff_adjacency_matrix, shuff_weights] =
      glib::test::shuffle_vertices(adjacency_matrix, weights);
  glib::io::write_dimacs_graph(args.get<std::string>("output_graph_file"),
                               shuff_adjacency_matrix, shuff_weights);
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
