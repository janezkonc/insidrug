#include "insilab/glib.hpp"
#include "insilab/glib/test.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>

/**
 * Generate various random weighted graphs.
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "add_weights_to_graph: Assign positive (or negative) weights to "
             "unweighted graph's vertices according to normal distribution.")
      .add_argument<std::string>({"ifile", "i"},
                                 "Input unweighted graph file in DIMACS format",
                                 {}, true)
      .add_argument<std::string>({"ofile", "o"},
                                 "Output weighted graph file in DIMACS format",
                                 {}, true)
      .add_argument<std::string>(
          {"method"},
          "Vertex-weights can be assigned based on different "
          "methods",
          {"random"}, false, {"random", "mod200"})
      .add_argument<double>({"mean"}, "Mean (for random weights only)", 100.0 * 10000)
      .add_argument<double>(
          {"sd"}, "Standard deviation (for random weights only)", 20.0 * 10000)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  const auto &[adjacency_matrix, _unused] =
      glib::io::read_dimacs_graph(args.get<std::string>("ifile"));
  const auto &weights =
      args.get<std::string>("method") == "random"
          ? glib::test::assign_random_weights(adjacency_matrix,
                                              args.get<double>("mean"),
                                              args.get<double>("sd"))
          : glib::test::assign_mod200_weights(adjacency_matrix);
  glib::io::write_dimacs_graph(args.get<std::string>("ofile"), adjacency_matrix,
                               weights);
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
