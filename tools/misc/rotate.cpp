#include "insilab/geom3d.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "rotate: Rotate and translate a structure according to "
             "rotational-translational matrix.")

      .add_argument<std::string>(
          {"i", "ifile"},
          "An input molecular structure file that we wish to "
          "rotate (.pdb,.cif,.mol2,.json[.gz])",
          "", true)
      .add_argument<std::string>(
          {"chain_id"},
          "A chain identifier in the molecule that we wish to rotate")
      .add_argument<std::string>({"rota"}, "A rotational matrix (3 x 3)", "",
                                 true)
      .add_argument<std::string>({"trans"}, "A translational vector (3)", "",
                                 true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  namespace fs = std::filesystem;

  const auto rota = nlohmann::json::parse(args.get<std::string>("rota"));
  const auto trans = nlohmann::json::parse(args.get<std::string>("trans"));

  geom3d::Matrix rm{rota, trans};

  const auto rotated = Molecules::Parser(args.get<std::string>("ifile"),
                                         Molecules::Parser::Options::all_models)
                           .parse()
                           .rotate(rm, true) // inverse rotation
                           .first()
                           .build();
                           
  std::cout << nlohmann::json(rotated) << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
