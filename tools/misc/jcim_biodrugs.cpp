#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/path.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"

/**
 * This program calculates the number of biological drugs (protein and peptide)
 * within binding sites that was requested by JCIM reviewers (ProBiS-Fold,
 * 2022).
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "remove_spaces_from_json: does what the name says :-)")
      .add_argument<std::string>({"probisdockdb_json_file"},
                                 "ProBiS-Dock DB Json file [.json[.gz]]", {},
                                 true)
      .add_argument<std::string>(
          {"pdb_drugbank_file"},
          "A text file linking PDB & Chain IDs to DrugBank IDs of  [.txt[.gz]]",
          {}, true)
      .add_argument<std::string>(
          {"probisligandsdb_dir"},
          "A directory with ProBiS-ligands DB ligand files", {}, true)
      .add_argument<double>({"min_min_beta_factor"},
                            "Minimum beta factor (AlphaFold's confidence) a "
                            "binding site must have to be considered",
                            90.0)
      .add_argument<bool>({"in_pdb"},
                          "True, if AlphaFold protein has "
                          "equivalent in PDB ID, false otherwise",
                          false)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  const auto probisdockdb_j = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("probisdockdb_json_file"))
          .first);

  const auto pdb_drugbank_str =
      inout::read_file_to_str(args.get<std::string>("pdb_drugbank_file")).first;

  const auto in_pdb = args.get<bool>("in_pdb");
  const auto min_min_beta_factor = args.get<double>("min_min_beta_factor");

  std::map<std::string, std::string> pdb_drugbank;
  for (const auto &line : helper::split(pdb_drugbank_str, "\n")) {
    const auto &mapping = helper::split(line, ",");
    assert(mapping.size() == 3);
    const auto &pdb_chain_id = mapping[0];
    const auto &seq_id = mapping[1];
    const auto &drugbank_id = mapping[2];
    pdb_drugbank[pdb_chain_id] = drugbank_id;
  }

  // leave just the rest types we'll need
  const auto filtered_probisdockdb_j = probisligands::filter(
      probisdockdb_j,
      [&in_pdb, &min_min_beta_factor](const nlohmann::json &item) -> bool {
        const auto &bsite_rest = item.at("bsite_rest").get<Residue::Type>();
        const auto &pdb_id_j = item.at("pdb_id");
        const auto &min_beta_factor = item.at("min_beta_factor").get<int>();
        return details::is_protein(bsite_rest) &&
               (in_pdb && !pdb_id_j.is_null() ||
                !in_pdb && pdb_id_j.is_null()) &&
               min_beta_factor > min_min_beta_factor;
      });
  std::cout << "number of binding sites = " << filtered_probisdockdb_j.size()
            << std::endl;
  // count the number of drugs in binding sites
  auto count_all{0uz}, count_drug{0uz};
  for (const auto &bsite_j : filtered_probisdockdb_j) {
    try {
      const auto &bsite_rest = bsite_j.at("bsite_rest").get<Residue::Type>();
      const auto &bs_id = bsite_j.at("bs_id").get<int>();
      const auto &alphafold_id = bsite_j.at("alphafold_id").get<std::string>();

      const auto ligands_j = nlohmann::json::parse(
          inout::read_file_to_str(
              path::join(args.get<std::string>("probisligandsdb_dir"),
                         "ligands_" + alphafold_id + ".json.gz"))
              .first);
      std::cout << "number of ligands = " << ligands_j.size() << std::endl;
      const auto filtered_bsites_j = probisligands::filter(
          ligands_j, [&bsite_rest, &bs_id](const nlohmann::json &item) {
            return item.at("bsite_rest").get<Residue::Type>() == bsite_rest &&
                   item.at("bs_id").get<int>() == bs_id;
          });
      std::cout << "filtered bsites = " << filtered_bsites_j.size()
                << std::endl;
      for (const auto &item : filtered_bsites_j) {
        const auto &num_drugs = std::ranges::count_if(
            item.at("data"),
            [&pdb_drugbank, &alphafold_id](const nlohmann::json &ligand_j) {
              const auto &pdb_id =
                  ligand_j.at("comments").at("pdb_id").get<std::string>();
              const auto &chain_id =
                  ligand_j.at("comments").at("chain_id").get<std::string>();
              if (pdb_drugbank.contains(pdb_id + chain_id)) {
                std::cout << "alphafold_id = " << alphafold_id
                          << " biologic drug PDB = " << pdb_id + chain_id
                          << " DrugBank = "
                          << pdb_drugbank.at(pdb_id + chain_id) << std::endl;
              }
              return pdb_drugbank.contains(pdb_id + chain_id);
            });
        std::cout << "number of drugs = " << num_drugs << std::endl;
        if (num_drugs > 0)
          count_drug++;
        count_all++;
      }
    } catch (const std::exception &e) {
      std::cerr << e.what() << std::endl;
    }
  }

  std::cout << "All binding sites that fulfill input criteria: " << count_all
            << std::endl;
  std::cout << "Binding sites that contain drugs among predicted ligands: "
            << count_drug << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
