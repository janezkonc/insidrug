#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>

/**
 * This program computes a RMSD between two identical molecules.
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "compute_rmsd: compute Root Mean Square Deviation between two "
             "identical molecules")
      .add_argument<std::string>({"first_molecule_file", "f1"},
                                 "First molecule [{.mol2,.pdb,.cif,.json}[.gz]",
                                 {}, true)
      .add_argument<std::string>(
          {"second_molecule_file", "f2"},
          "Second molecule [{.mol2,.pdb,.cif,.json}[.gz]", {}, true)
      .add_argument<bool>(
          {"symmetry"}, "Account for symmetry of molecules in RMSD calculation")
      .add_argument<bool>({"superimpose"},
                          "Superimpose the molecules prior to the RMSD "
                          "calculation. This has effect only if symmetry=true")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  const auto first_molecule =
      Molecules::Parser(args.get<std::string>("first_molecule_file"),
                        Molecules::Parser::Options::first_model |
                            Molecules::Parser::Options::hydrogens)
          .parse()
          .first();
  const auto second_molecule =
      Molecules::Parser(args.get<std::string>("second_molecule_file"),
                        Molecules::Parser::Options::first_model |
                            Molecules::Parser::Options::hydrogens)
          .parse()
          .first();

  // if superimpose is true, try to superimpose the molecules
  const auto superimpose = args.get<bool>("superimpose");
  const auto symmetry = args.get<bool>("symmetry");
  std::cout << "RMSD = "
            << (symmetry ? algorithms::compute_rmsd(
                               first_molecule, second_molecule, superimpose)
                         : algorithms::compute_rmsd_ord(first_molecule,
                                                        second_molecule))
            << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
