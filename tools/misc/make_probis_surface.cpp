#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <cassert>
#include <numeric>
#include <string>
#include <vector>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "make_probis_surface: generate a ProBiS surface file")
      .add_argument<std::string>(
          {"ifile", "i"}, "A protein structure file ({.pdb,.cif,.json}[.gz])",
          {}, true)
      .add_argument<std::string>(
          {"chain_id", "c"},
          "A chain id for one of the chains in the structure file", "A")
      .add_argument<std::string>({"ofile", "o"},
                                 "ProBiS surface file (.srf[.gz])", {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  const auto molecules =
      Molecules::Parser(args.get<std::string>("ifile")).parse();
  const auto descriptors = probis::create_descriptors(molecules.get_atoms());

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
