#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <limits>
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "find_within: find residues on the receptor protein that are "
             "near a ligand")

      .add_argument<std::string>({"r", "receptor"},
                                 "First molecule file e.g., a receptor protein "
                                 "(may be gzipped) [.pdb,.mol2,.cif]",
                                 "", true)
      .add_argument<std::string>({"l", "ligand"},
                                 "Second molecule file e.g., a ligand (may be "
                                 "gzipped) [.pdb,.mol2,.cif]")
      .add_argument<double>(
          {"d", "distance"},
          "Maximum distance between pairs of atoms to consider", 3.0)
      .add_argument<bool>({"byres"}, "Output whole residues")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;
  using namespace insilab::molib::algorithms;

  Molecule receptor =
      Molecules::Parser(args.get<std::string>("receptor")).parse().first();
  Molecule ligand =
      Molecules::Parser(args.get<std::string>("ligand")).parse().first();

  for (auto &presidue : ligand.get_residues()) {
    presidue->set_resn("LIGUNIQ");
  }
  Molecules complex;
  complex.add(ligand);
  complex.add(receptor);
  const auto exprvec = args.get<bool>("byres")
                           ? ExprVec{Key::BYRES, Key::RESN, "LIGUNIQ",
                                     Key::AROUND, args.get<double>("distance")}
                           : ExprVec{Key::RESN, "LIGUNIQ", Key::AROUND,
                                     args.get<double>("distance")};

  const auto filtered = complex.filter(exprvec);
  std::cout << filtered << std::endl;

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
