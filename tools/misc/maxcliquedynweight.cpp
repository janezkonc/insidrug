#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/glib/test.hpp"
#include "insilab/helper.hpp"
#include "insilab/program.hpp"
#include <iostream>
#include <variant>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "maxcliquedynweight: Find maximum cliques and k-cliques in a "
             "weighted or unweighted graph.")
      .add_argument<std::string>({"graph_file"},
                                 "Input weighted graph file in DIMACS format",
                                 {}, true)
      .add_argument<int>({"K"}, "Number of vertices in k-clique to find", 1)
      .add_argument<int>({"N"}, "Maximum number of k-clique to find", 1)
      .add_argument<bool>(
          {"dynamic"},
          "Use dynamic versions of the clique algorithms that use dynamically "
          "varying bounds to speed up the clique search")
      .add_argument<std::string>({"weight_policy"},
                                 "Search for cliques with highest weights or "
                                 "cliques with lowest weights",
                                 {"highest"}, false, {"lowest", "highest"})
      .add_argument<std::string>({"strategy"},
                                 "Search for cliques with highest weights or "
                                 "cliques with lowest weights",
                                 {"maximum_clique"}, false,
                                 {"maximum_clique", "maximum_weight_clique",
                                  "n_k_cliques", "n_highest_weight_k_cliques",
                                  "maximum_weight_clique_experimental"})
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;

  const auto &graph_file = args.get<std::string>("graph_file");
  const auto &K = args.get<int>("K");
  const auto &N = args.get<int>("N");
  const auto &strategy = args.get<std::string>("strategy");
  const auto &weight_policy = args.get<std::string>("weight_policy");
  const auto &dynamic = args.get<bool>("dynamic");

  std::cout << args.print_config(true);
  std::cout << "K=" << K << std::endl;
  std::cout << "N=" << N << std::endl;

  auto print_clique = [](const auto &clique) {
    std::cout << "Size = " << clique.size() << " Vertices = [";
    auto sorted = clique;
    std::ranges::sort(sorted);
    for (auto i{0uz}; i < sorted.size(); ++i)
      std::cout << sorted[i] + 1 << (i < sorted.size() - 1 ? " " : "");
    std::cout << "] ";
  };

  auto print_weight = [](const auto &weight) {
    std::cout << "Weight = " << weight << " ";
  };
  auto print_check = [](const auto &clique, const auto &weight,
                        const auto &adjacency_matrix, const auto &weights) {
    std::cout << "Test = "
              << (glib::test::check_clique(clique, weight, adjacency_matrix,
                                           weights)
                      ? "Passed"
                      : "Failed");
  };

  std::cout << "Reading graph file " << graph_file << std::endl;
  const auto &[adjacency_matrix, weights] =
      glib::io::read_dimacs_graph(graph_file);

  helper::Benchmark<std::chrono::milliseconds> b;

  if (strategy == "maximum_clique") {
    std::cout << "Searching for a maximum clique..." << std::endl;
    std::vector<glib::node_idx_t> clique;
    if (dynamic)
      clique = glib::algorithms::find_maximum_clique(adjacency_matrix);
    else
      clique =
          glib::algorithms::find_maximum_clique<glib::algorithms::MCQStrategy>(
              adjacency_matrix);
    print_clique(clique);
    print_check(clique, 0, adjacency_matrix,
                std::vector<glib::weight_t>(adjacency_matrix.get_szi(), 0));

  }

  else if (strategy == "maximum_weight_clique") {
    std::cout << "Searching for a maximum weight clique..." << std::endl;
    std::vector<glib::node_idx_t> clique;
    glib::weight_t weight;
    if (dynamic) {
      if (weight_policy == "highest") {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique(
            adjacency_matrix, weights);
      } else {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQDWStrategy,
            glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights);
      }
    } else {
      if (weight_policy == "highest") {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQWStrategy>(adjacency_matrix, weights);
      } else {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQWStrategy,
            glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights);
      }
    }
    print_clique(clique);
    print_weight(weight);
    print_check(clique, weight, adjacency_matrix, weights);
  } else if (strategy == "maximum_weight_clique_experimental") {
    std::cout << "Searching for a maximum weight clique (experimental) ..."
              << std::endl;
    std::vector<glib::node_idx_t> clique;
    glib::weight_t weight;
    if (dynamic) {
      if (weight_policy == "highest") {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQDWExpStrategy>(adjacency_matrix, weights);
      } else {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQDWExpStrategy,
            glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights);
      }
    } else {
      if (weight_policy == "highest") {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQWExpStrategy>(adjacency_matrix, weights);
      } else {
        std::tie(clique, weight) = glib::algorithms::find_maximum_weight_clique<
            glib::algorithms::MCQWExpStrategy,
            glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights);
      }
    }
    print_clique(clique);
    print_weight(weight);
    print_check(clique, weight, adjacency_matrix, weights);
  } else if (strategy == "n_highest_weight_k_cliques") {
    std::cout << "Searching for N highest weight k-cliques..." << std::endl;
    std::vector<std::vector<glib::node_idx_t>> cliques;
    std::vector<glib::weight_t> clique_weights;

    if (dynamic) {
      if (weight_policy == "highest") {
        std::tie(cliques, clique_weights) =
            glib::algorithms::find_n_highest_weight_k_cliques(adjacency_matrix,
                                                              weights, K, N);
      } else {
        std::tie(cliques, clique_weights) =
            glib::algorithms::find_n_highest_weight_k_cliques<
                glib::algorithms::KCQDWStrategy,
                glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights,
                                                      K, N);
      }
    } else {
      if (weight_policy == "highest") {
        std::tie(cliques, clique_weights) =
            glib::algorithms::find_n_highest_weight_k_cliques<
                glib::algorithms::KCQWStrategy>(adjacency_matrix, weights, K,
                                                N);
      } else {
        std::tie(cliques, clique_weights) =
            glib::algorithms::find_n_highest_weight_k_cliques<
                glib::algorithms::KCQWStrategy,
                glib::algorithms::LowestWeightPolicy>(adjacency_matrix, weights,
                                                      K, N);
      }
    }

    std::cout << "N_found = " << cliques.size() << std::endl;
    for (auto i{0uz}; i < cliques.size(); ++i) {
      print_clique(cliques[i]);
      print_weight(clique_weights[i]);
      print_check(cliques[i], clique_weights[i], adjacency_matrix, weights);
      std::cout << std::endl;
    }
  }
  std::cout << "\nTime = " << b.duration() << " ms" << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
