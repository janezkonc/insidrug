#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/glib/test.hpp"
#include "insilab/helper.hpp"
#include "insilab/program.hpp"
#include <iostream>
#include <variant>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "szeged_strategy: See by how much a new coloring strategy "
             "improves weight upper bounds.")
      .add_argument<std::string>({"graph_file"},
                                 "Input weighted graph file in DIMACS format",
                                 {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

bool cut1(const int pi, const std::vector<int> &A,
          const insilab::helper::Array2d<bool> &adjacency_matrix) {
  for (int i = 0; i < A.size(); ++i)
    if (adjacency_matrix.get(pi, A[i]))
      return true;
  return false;
}

std::string
check_color_classes(const std::vector<std::vector<int>> &color_classes,
                    const insilab::helper::Array2d<bool> &adjacency_matrix) {
  std::string result = "failed";
  for (const auto &color_class : color_classes) {
    for (const auto &v : color_class) {
      if (cut1(v, color_class, adjacency_matrix))
        return "failed";
    }
  }
  return "success";
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;

  const auto &graph_file = args.get<std::string>("graph_file");

  std::cout << "Reading graph file " << graph_file << std::endl;
  const auto &[adjacency_matrix, weights] =
      glib::io::read_dimacs_graph(graph_file);

  std::vector<int> vertices(adjacency_matrix.get_szi());
  for (auto i = 0; i < adjacency_matrix.get_szi(); ++i) {
    vertices[i] = i;
  }

  // put vertices in color classes
  std::vector<std::vector<int>> color_classes(vertices.size() + 1);
  std::vector<int> max_weights(vertices.size() + 1);
  std::vector<int> max_vertex(vertices.size() + 1);
  int maxno = 1;
  for (auto i = 0; i < vertices.size(); ++i) {
    const auto p = vertices[i];
    int k = 1;
    while (cut1(p, color_classes[k], adjacency_matrix))
      ++k;
    color_classes[k].push_back(p);
    // update maximum weight of k-th color class
    if (weights[p] > max_weights[k]) {
      max_weights[k] = weights[p];
      max_vertex[k] = p;
    }
    maxno = std::max(maxno, k);
  }
  std::cout << "Max color = " << maxno << std::endl;

  const auto sum_max_weights =
      std::accumulate(max_weights.begin(), max_weights.end(), 0);
  std::cout << "Upper bound = " << sum_max_weights << std::endl;

  // "calculate" the color below which we can use our strategy
  const int min_k = maxno;

  // try to put as many as possible vertices with the maximum weight in a color
  // class to color classes with even higher maximum weights
  bool possible = true;
  int iteration = 1;
  while (possible) {
    possible = false;
    for (auto k = min_k; k >= 1; --k) {
      for (auto k2 = k - 1; k2 >= 1; --k2) {
        if (color_classes[k2].empty())
          continue;
        // can we hide the k2-th color class's max weight in k-th color class?
        if (max_weights[k2] < max_weights[k]) {
          // try move the vertex from k2 -> k
          const auto p = max_vertex[k2];
          if (!cut1(p, color_classes[k], adjacency_matrix)) {
            possible = true;
            // p is not adjacent to any vertex in color class k
            // so push p to the k-th color class
            color_classes[k].push_back(p);
            // and remove p from k2-th color class
            std::erase(color_classes[k2], p);
            // update k2-th color class's max weight
            max_weights[k2] = 0; // if k2-th color class is empty
            for (auto i = 0; i < color_classes[k2].size(); ++i) {
              const auto v = color_classes[k2][i];
              if (weights[v] > max_weights[k2]) {
                max_weights[k2] = weights[v];
                max_vertex[k2] = color_classes[k2][i];
              }
            }
            // k-th color class's max weight needs NO updating
          }
        }
      }
    }
    const auto sum_max_weights_new =
        std::accumulate(max_weights.begin(), max_weights.end(), 0);
    std::cout << "Iteration " << iteration++
              << " new upper bound = " << sum_max_weights_new << std::endl;
  }
  const auto sum_max_weights_new =
      std::accumulate(max_weights.begin(), max_weights.end(), 0);
  std::cout << "New upper bound = " << sum_max_weights_new << std::endl;

  std::cout << "Testing for integrity of color classes: "
            << check_color_classes(color_classes, adjacency_matrix)
            << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
