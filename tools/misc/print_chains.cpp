#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <set>
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "print_chains: output protein chain ids")
      .add_argument<std::string>(
          {"ifile", "i"},
          "Input protein structure file [{.pdb,.cif,.mol2,.json}[.gz]", {},
          true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  const auto molecule =
      Molecules::Parser(args.get<std::string>("ifile"),
                        Molecules::Parser::Options::first_model)
          .parse()
          .first();
  std::set<std::string> chains;
  for (const auto &presidue : molecule.get_residues()) {
    if (details::is_protein(presidue->rest())) {
      chains.insert(presidue->chain().chain_id());
    }
  }
  std::cout << nlohmann::json(chains) << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
