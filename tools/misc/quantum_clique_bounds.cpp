#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>

/**
 * This program prints the first bounds on the root node of a graph, time to
 * calculated it, and number of steps until clique is found.
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "quantum_clique_bounds: prints the first bounds on the root node "
             "of a graph, time to calculated it, and number of steps until "
             "clique is found.")
      .add_argument<std::string>({"graph_file"}, "Graph file in DIMACS format",
                                 {}, true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace glib::algorithms;
  const auto [conn, energies] =
      glib::io::read_dimacs_graph(args.get<std::string>("graph_file"));
  helper::Benchmark<std::chrono::milliseconds> b;
  const auto qmax = glib::algorithms::find_maximum_clique(conn);
  std::cout << "Finding max clique of size = " << qmax.size() << " took "
            << b.duration() << "ms" << std::endl;

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
