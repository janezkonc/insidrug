#include "graph/fdlayout.hpp"
#include "helper/debug.hpp"
#include "helper/error.hpp"
#include "helper/inout.hpp"
#include "helper/path.hpp"
#include "nlohmann/json.hpp"
#include "opts_fdlayout.hpp"
#include <atomic>
#include <exception>
#include <iostream>
#include <mutex>
#include <thread>
#include <typeinfo>

void init(int argc, char *argv[]) {
  try {
    TCLAP::CmdLine cmd("Command description message", ' ', __version);

    strargs["network_file"] = {
        "", U<S>(new S("", "network",
                       "File with the network to be optimized, expected x,y "
                       "(,z) coordinates (.json[.gz])",
                       true, "default", "string", cmd))};
    strargs["opti_network_file"] = {
        "",
        U<S>(new S("", "opti_network", "Optimized network file (.json[.gz])",
                   true, "default", "string", cmd))};

    intargs["nsteps"] = {
        "", U<I>(new I("n", "nsteps",
                       "Number of steps for optimization (default: 1000)",
                       false, 1000, "int", cmd))};
    dblargs["dist"] = {
        "", U<D>(new D("d", "dist", "Connection distance (default is 1.0)",
                       false, 1.0, "double", cmd))};
    dblargs["scale"] = {
        "", U<D>(new D("s", "scale",
                       "Scaling factor for coordinates (default is 100.0)",
                       false, 100.0, "double", cmd))};

    cmd.parse(argc, argv);

  } catch (TCLAP::ArgException &e) {
    cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    throw helper::Error("[WHOOPS] arguments error\n");
  }
}

int main(int argc, char *argv[]) {

  try {

    CmdLnOpts cmdl(argc, argv);

    nlohmann::json network = nlohmann::json::parse(
        inout::read_file_to_str(cmdl.args("network_file"))
            .first); // expect x, y (optionally z) in json entry

    const double scale = cmdl.argd("scale");

    geom3d::Point::Vec vertices;
    for (auto &d : network) {
      if (helper::to_string(d.at("group")) == "nodes") {
        vertices.push_back(geom3d::Point(scale * d.at("position").at("x"),
                                         scale * d.at("position").at("y"), 0));
        dbgmsg(d["group"].asString() << " " << d.at("position").at("x") << " "
                                     << d.at("position").at("y"));
      }
    }

    glib::FdLayout fdl(vertices, cmdl.argd("dist"), cmdl.argi("nsteps"));

    geom3d::Point::Vec optimized = fdl.run();

    assert(j.size() == optimized.size());

    int i = 0;
    for (auto &d : network) {
      if (helper::to_string(d.at("group")) == "nodes") {
        d["position"]["x"] = optimized[i].x() / scale;
        d["position"]["y"] = optimized[i].y() / scale;
        ++i;
      }
    }

    inout::output_file(network, cmdl.args("opti_network_file"));

  } catch (exception &e) {
    cerr << e.what() << std::endl;
  }

  return 0;
}
