#include "insilab/helper.hpp"
#include "insilab/molib.hpp"
#include "insilab/program.hpp"
#include <cassert>
#include <numeric>
#include <string>
#include <vector>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "pick_representative: given a set of protein "
             "structures, find a representative structure\n")
      .add_argument<std::vector<std::string>>(
          {"protein_files"}, "A list of protein files [{.pdb,.cif}[.gz]")
      .add_argument<std::vector<std::string>>(
          {"chain_ids"},
          "A list of chain ids, where each chain id corresponds to one in the "
          "list of protein files")
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;
  const auto &protein_files =
      args.get<std::vector<std::string>>("protein_files");
  const auto &chain_ids = args.get<std::vector<std::string>>("chain_ids");
  if (protein_files.empty() || chain_ids.empty())
    throw helper::Error(
        "[WHOOPS] Protein files or chain ids shouldn't be empty!");
  if (protein_files.size() != chain_ids.size())
    throw helper::Error(
        "[WHOOPS] Numbers of protein files and chain ids should be the same!");
  constexpr std::tuple null_repr{0, -1000, 1000.0};
  std::tuple repr = null_repr;

  // return true if new chain size is greater than current representative's
  // chain within tolerance defined by factor
  auto is_greater_within_tolerance =
      [](const auto chain_size, const auto repr_chain_size, const auto factor) {
        return chain_size > repr_chain_size - factor * repr_chain_size;
      };

  auto output_representative = [&](const auto repr) {
    std::cout << protein_files[std::get<0>(repr)] << " "
              << chain_ids[std::get<0>(repr)]
              << " [NO.ATOMS CHAIN = " << std::get<1>(repr)
              << " RESOLUTION = " << std::get<2>(repr) << "]" << std::endl;
  };
  std::map<std::string, Molecule> cache;
  const auto max_cache_size = 5;
  for (int i = 0; i < protein_files.size(); ++i) {
    try {
      for (int j = 0; j < i - max_cache_size; ++j) {
        cache.erase(protein_files[j]);
      }
      if (!cache.contains(protein_files[i])) {
        cache.emplace(protein_files[i],
                      Molecules::Parser(protein_files[i]).parse().first());
      }
      const auto &m = cache.at(protein_files[i]);
      // count the number of protein atoms in chain
      auto chain_size = 0;
      const auto pmodel = m.get_models().front();
      for (const auto &pchain : pmodel->get_chains()) {
        if (pchain->chain_id() != chain_ids[i])
          continue;
        for (const auto &presidue : pchain->get_residues()) {
          if (!(presidue->rest() & Residue::Type::protein))
            continue;
          chain_size += presidue->size();
        }
      }
      // treat NMR models (no resolution) as somewhat 'worse' X-RAY structures
      const auto resolution =
          m.get_resolution() == -1.0 ? 4.0 : m.get_resolution();
      // output_representative(std::make_tuple(i, chain_size, resolution));
      // maybe select new representative
      if (is_greater_within_tolerance(chain_size, std::get<1>(repr), 0.05) &&
          resolution <= std::get<2>(repr)) {
        repr = std::make_tuple(i, chain_size, resolution);
      }
    } catch (const std::exception &e) {
    }
  }
  if (repr == null_repr)
    throw helper::Error("[WHOOPS] Could not determine representative!");
  output_representative(repr);
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
