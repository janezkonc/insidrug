#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"
#include <exception>
#include <iostream>

/**
 * This program removes spaces from a JSON file to reduce its size without
 * changing its content.
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "remove_spaces_from_json: does what the name says :-)")
      .add_argument<std::string>({"json_file"}, "Json file [.json[.gz]]", {},
                                 true)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  const auto json_file_with_spaces = nlohmann::json::parse(
      inout::read_file_to_str(args.get<std::string>("json_file")).first);
  inout::output_file(json_file_with_spaces.dump(),
                     args.get<std::string>("json_file"));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
