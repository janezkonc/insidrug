#include "charmm_hydrogens.hpp"
#include "helper/benchmark.hpp"
#include "helper/error.hpp"
#include "helper/inout.hpp"
#include "helper/iomanip.hpp"
#include "helper/path.hpp"
#include "molecule/molecules.hpp"
#include "molecule/parser.hpp"
#include <atomic>
#include <exception>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_statistics_double.h>
#include <iostream>
#include <mutex>
#include <thread>
#include <typeinfo>

void init(int argc, char *argv[]) {
  try {
    TCLAP::CmdLine cmd("Command description message", ' ', __version);

    strargs["input"] = {
        "", U<S>(new S("i", "input",
                       "Input molecule in which hydrogens should be "
                       "reordered ({.mol2}[.gz])",
                       true, "default", "string", cmd))};

    strargs["output"] = {
        "", U<S>(new S("o", "output",
                       "Output molecule with hydrogens are adjacent to their "
                       "heavy atoms ({.mol2}[.gz])",
                       true, "default", "string", cmd))};

    cmd.parse(argc, argv);
  } catch (TCLAP::ArgException &e) {
    cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    throw helper::Error("[WHOOPS] arguments error\n");
  }
}

CharmmHydrogens::CharmmHydrogens(CmdLnOpts &cmdl) : __cmdl(cmdl) {
  cout << __cmdl.display_message("Started");
}

// reorder hydroges so that they are adjacent to heavy atom
void CharmmHydrogens::run() {
  try {
    molib::Molecules::Parser molecule_reader(
        __cmdl.args("input"), molib::Molecules::Parser::Options::first_model |
                                  molib::Molecules::Parser::Options::hydrogens);
    molib::Molecule molecule = molecule_reader.parse().first();

    molib::Molecules mols;
    molib::Residue &residue =
        mols.add(molib::Molecule(molecule.name()))
            .add(molib::Assembly(0, "ASYMMETRIC UNIT"))
            .add(molib::Model(1))
            .add(molib::Chain('A'))
            .add(molib::Residue("DOC", 1, ' ', molib::Residue::Type::compound));

    // rearrange hydrogens so that they are adjacent to their heavy atoms
    int an = 1;
    for (auto &patom : molecule.get_atoms()) {
      auto &atom = *patom;
      if (atom.element() == molib::Element::H)
        continue;
      atom.set_atom_number(an++);
      auto &heavy = residue.add(atom);
      for (auto &neighb : atom) {
        if (neighb.element() == molib::Element::H) {
          neighb.set_atom_number(an++);
          auto &hydrogen = residue.add(neighb);
          const std::string &sybyl_type =
              atom.get_bond(neighb).get_sybyl_type();
          heavy.connect(hydrogen).set_sybyl_type(sybyl_type);
        }
      }
    }
    // connect heavy atoms
    for (auto &patom : molecule.get_atoms()) {
      auto &atom = *patom;
      if (atom.element() == molib::Element::H)
        continue;
      auto &heavy1 = residue.atom(atom.atom_number());
      for (auto &neighb : atom) {
        if (neighb.element() != molib::Element::H) {
          auto &heavy2 = residue.atom(neighb.atom_number());
          const std::string &sybyl_type =
              atom.get_bond(neighb).get_sybyl_type();
          heavy1.connect(heavy2).set_sybyl_type(sybyl_type);
        }
      }
    }
    inout::output_file(mols.last(), __cmdl.args("output"),
                       {molib::IOmanip::mol2});
  } catch (exception &e) {
    cerr << e.what() << std::endl;
  }
}
