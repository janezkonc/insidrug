#include "insilab/glib.hpp"
#include "insilab/glib/test.hpp"
#include "insilab/program.hpp"
#include <exception>
#include <iostream>

/**
 * Generate various random weighted graphs.
 */

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "generate_random_weighted_graphs: do what the name says.")
      .add_argument<std::string>({"ofile", "o"},
                                 "Output weighted graph file in DIMACS format",
                                 {}, true)
      .add_argument<std::string>(
          {"method"},
          "Vertex-weights can be assigned based on different "
          "methods",
          {"random"}, false, {"random", "mod200"})
      .add_argument<std::size_t>(
          {"size"}, "Number of vertices in a graph to be generated")
      .add_argument<double>(
          {"density"},
          "Edge density (probability of an edge) between zero and one")
      .add_argument<double>({"mean"}, "Mean (for random weights only)",
                            100.0 * 10000)
      .add_argument<double>(
          {"sd"}, "Standard deviation (for random weights only)", 20.0 * 10000)
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;

  glib::test::generate_random_weighted_graph(
      args.get<std::string>("ofile"), args.get<std::string>("method"),
      args.get<std::size_t>("size"), args.get<double>("density"),
      args.get<double>("mean"), args.get<double>("sd"));

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
