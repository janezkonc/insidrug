#include "insilab/molib.hpp"
#include "insilab/probisligands.hpp"
#include "insilab/program.hpp"
#include <limits>
#include <string>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "neighb2: find residues on the receptor protein that are "
             "near ligand or centroids.\n\n"
             "Brief description:\n"
             "  * If using the ligand file as input, then the first molecule "
             "within this file will be considered.\n The distance option has "
             "effect here.\n"
             "  * If using centroids option, then the radius of each centroid "
             "is used to determine if a residue is near.\n"
             "Multiple binding sites can be given identified by their bs_id "
             "and ltype.\n")

      .add_argument<std::string>({"r", "receptor"},
                                 "First molecule file e.g., a receptor protein "
                                 "(may be gzipped) [.pdb,.mol2,.cif]",
                                 "", true)
      .add_argument<std::string>({"l", "ligand"},
                                 "Second molecule file e.g., a ligand (may be "
                                 "gzipped) [.pdb,.mol2,.cif]")
      .add_argument<std::string>(
          {"c", "centro"}, "Centroids file (may be gzipped) [.cen, .json]")
      .add_argument<double>(
          {"d", "distance"},
          "Maximum distance between pairs of atoms to consider", 3.0)
      .add_argument<std::string>(
          {"m", "receptor_type"},
          "The type of residue to consider for the receptor", "protein", false,
          {"protein", "all"})
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .add_option_group(
          "input_type", {"ligand", "centro"},
          "Either a ligand file or centroids file should be provided.")
      .set_required_option_group("input_type", 1)
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  // output json with neighboring residues
  std::cout << (args.is_set("ligand")
                    ? probisligands::from_ligand(
                          args.get<std::string>("receptor"),
                          args.get<std::string>("ligand"),
                          args.get<double>("distance"),
                          args.get<std::string>("receptor_type"))
                    : probisligands::from_centroids(
                          args.get<std::string>("receptor"),
                          args.get<std::string>("centro"),
                          args.get<std::string>("receptor_type")));
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
