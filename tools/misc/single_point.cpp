#include "amber10.hpp"
#include "insilab/docker.hpp"
#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/ommiface.hpp"
#include "insilab/parallel.hpp"
#include "insilab/probisdock.hpp"
#include "insilab/program.hpp"
#include "insilab/score.hpp"
#include "tip3p.hpp"
#include <atomic>
#include <exception>
#include <filesystem>
#include <iostream>
#include <mutex>
#include <thread>
#include <typeinfo>

namespace fs = std::filesystem;

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "single_point: calculate single-point energy of a "
             "fragment (or any small molecule)) in a protein")

      .add_argument<std::string>(
          {"f", "fragment_file"},
          "One small molecule or fragment for which we wish to calculate "
          "single-point energy in the receptor binding site (the molecule MUST "
          "have IDATM types already calculated, so the only acceptable format "
          "is Json) ({.json}[.gz])",
          "", true)
      .add_argument<std::string>(
          {"r", "receptor_file"},
          "The protein in which small molecule binds ({.pdb|.cif|.json}[.gz])")
      .add_argument<std::string>(
          {"template_ligands_file"},
          "The template ligands for ProBiS-Score - provide when probis_level > "
          "0.0 (.pdb[.gz]|.mol2[.gz]|.json[.gz])")
      .add_argument<std::string>({"centro_file"},
                                 "Binding site centroids (.cen|.json[.gz]) ")
      .add_argument<double>(
          {"probis_level"},
          "The combined scoring function is a combination of two score "
          "terms, ProBiS-Score that relies on template ligands and GSscore, a "
          "general statistical scoring function: Combined-Score = h * "
          "ProBiS-Score + (1 - h) * GSscore",
          0.9, false, {}, {0.0, 1.0})
      .add_argument<double>(
          {"max_seq_id"},
          "Upper bound for the sequence identity (%) of the proteins "
          "from which predicted (template) ligands are transposed by the "
          "ProBiS-Dock Database method - enables testing with excluded "
          "template ligands above certain seq. id.",
          100.0)
      .add_argument<std::string>(
          {"ref_state"},
          "Normalization method for the reference state of the GSscore scoring "
          "function - 'mean' is averaged over all atom type pairs, whereas "
          "'cumulative' is a summation for atom type pairs",
          "mean", false, {"mean", "cumulative"})
      .add_argument<std::string>(
          {"comp"},
          "Atom types used in calculating reference state 'reduced' or "
          "'complete' in GSscore scoring function ('reduced' includes "
          "only those atom types present in the specified receptor and small "
          "molecule, whereas	'complete' includes all atom types)",
          "reduced", false, {"reduced", "complete"})
      .add_argument<std::string>(
          {"rad_or_raw"},
          "Function for calculating scores 'radial' or 'normalized_frequency'",
          "radial", false, {"radial", "normalized_frequency"})
      .add_argument<double>({"dist_cutoff"},
                            "Cutoff distance for the scoring function", 6.0,
                            false, {}, {4.0, 15.0})
      .add_argument<int>(
          {"num_read_template_ligands"},
          "Maximum number of template ligands to read in one chunk", 10)
      .add_argument<std::string>(
          {"fftype"},
          "Forcefield to use 'kb' (knowledge-based) or 'phy' (physics-based)",
          "kb", false, {"kb", "phy"})
      .add_argument<double>({"grid_spacing"},
                            "Binding site grid resolution in Angstroms", 0.75)
      .add_argument<double>({"excluded_radius"}, "Excluded radius in Angstroms",
                            0.8)
      .add_argument<double>({"max_interatomic_distance"},
                            "Maximum interatomic distance in Angstroms", 8.0)
      .add_argument<double>({"dist_cutoff_probis_template"},
                            "Distance cutoff for ProBiS-Score scoring "
                            "function in Angstroms",
                            1.0)
      .add_argument<double>({"pow_probis_template"},
                            "How fast the template ProBiS-Score decays with "
                            "distance from template atoms 1/(d+1)^pow",
                            1.0)
      .add_argument<double>({"step_non_bond"},
                            "Step for spline generation of non-bonded "
                            "knowledge-based potential",
                            0.01, false, {}, {0.0, 1.0})
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .add_argument<bool>(
          {"debug"}, "Output intermediary files to a temporary directory "
                     "(location depends on the TMPDIR environment variable)")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  // ligand
  const auto fragment =
      Molecules::Parser(args.get<std::string>("fragment_file"))
          .parse()
          .first()
          .erase_properties(); // erase atom properties since they make the
                               // graph matching incorrect

  // receptors
  const auto receptor =
      Molecules::Parser(args.get<std::string>("receptor_file"),
                        Molecules::Parser::Options::all_models)
          .parse()
          .first()
          .compute_atom_bond_types();

  // template ligands
  const auto template_ligands =
      Molecules::Parser(args.get<std::string>("template_ligands_file"),
                        Molecules::Parser::Options::all_models |
                            Molecules::Parser::Options::hydrogens)
          .parse();

  // centroids
  const auto centroids =
      centro::parse_file(args.get<std::string>("centro_file"));

  // prepare template ligands
  for (auto &template_ligand : template_ligands) {
    try {
      if (template_ligand.get_comments().contains("seq_id") &&
          template_ligand.get_comments().at("seq_id").get<double>() >
              args.get<double>("max_seq_id"))
        continue;
      template_ligand.compute_atom_bond_types();
    } catch (const std::exception &e) {
      std::cerr << "[NOTE] Skipping preparing a template ligand " +
                       template_ligand.get_residues().front()->resn() +
                       " due to "
                << e.what() << std::endl;
    }
  }

  // scoring function is individually compiled for each docked
  // fragment, such that it reflects its atom composition
  score::ScoringFunction score = score::create_scoring_function(
      receptor, fragment, template_ligands, args.get<double>("probis_level"),
      args.get<std::string>("ref_state"), args.get<std::string>("comp"),
      args.get<std::string>("rad_or_raw"), args.get<double>("dist_cutoff"),
      args.get<double>("step_non_bond"),
      args.get<double>("dist_cutoff_probis_template"),
      args.get<double>("pow_probis_template"));

  // create gridpoints for all binding site centroids
  docker::Gpoints gpoints;
  gpoints.identify_gridpoints(
      receptor, template_ligands, centroids, score, fragment.get_idatm_types(),
      args.get<double>("grid_spacing"), args.get<double>("dist_cutoff"),
      args.get<double>("excluded_radius"),
      args.get<double>("max_interatomic_distance"),
      args.get<double>("dist_cutoff_probis_template"));

  docker::Gpoints::PGpointVec pgvec;
  for (auto &point : gpoints.get_gridpoints0()) {
    pgvec.push_back(const_cast<docker::Gpoints::Gpoint *>(&point));
  }

  grid::Grid<docker::Gpoints::Gpoint> grid(pgvec);

  double energy_sum{};
  // go over all cavity points
  for (auto &[bsite_id, cavity_points] : gpoints.get_gridpoints()) {
    const auto &gmap = gpoints.get_gmap(bsite_id);
    for (const auto &patom : fragment.get_atoms()) {
      const auto confijk = docker::Gpoints::crd_to_ijk(
          grid, patom->crd(), args.get<double>("grid_spacing") * 1.33);
      const docker::Gpoints::Gpoint *pgpoint =
          gmap.data[confijk.i][confijk.j][confijk.k];
      energy_sum += pgpoint->energy(patom->idatm_type());
    }
  }
  std::cout << "Single-point energy = " << energy_sum << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
