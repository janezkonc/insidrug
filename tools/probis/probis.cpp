#include "insilab/probis.hpp"
#include "insilab/program.hpp"

std::string get_banner() { return; }

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             R"(*******************************************************************
*                                                                 *
*     _______                      ______      _     ______       *
*    |_   __ \                    |_   _ \    (_)  .' ____ \      *
*      | |__) |  _ .--.    .--.     | |_) |   __   | (___ \_|     *
*      |  ___/  [ `/'`\] / .'`\ \   |  __'.  [  |   _.____`.      *
*     _| |_      | |     | \__. |  _| |__) |  | |  | \____) |     *
*    |_____|    [___]     '.__.'  |_______/  [___]  \______.'     *
*                                                                 *     
*                                                                 *
*                  Member of the INSILAB suite.                   *
*                                                                 *
*              Copyright (c) Janez Konc, 2010-2022                *
*                                                                 *
*          If you used ProBiS in your work, please cite:          *
*                                                                 *
*   Janez Konc, Dušanka Janežič. ProBiS algorithm for detection   *
*    of structurally similar protein binding sites by local       *
*    structural alignment. Bioinformatics, 2010, 26, 1160-1168.   *
*                                                                 *
*    doi: https://doi.org/10.1093/bioinformatics/btq100           *
*                                                                 *
*******************************************************************)")
      .add_argument<bool>(
          {"align"},
          "Read a rotational matrix of an alignment from an .nosql file and "
          "superimpose the two given proteins accordingly (first run "
          "-compare or -surfdb). Output the superimposed proteins' "
          "coordinates in a .pdb file. You need to provide both .pdb files "
          "that you want to superimpose (see -f1, -c1, -f2, c2 modifiers) "
          "and an alignment number (see -alno modifier).")
      .add_argument<bool>(
          {"compare"},
          "Compare two protein surfaces (.pdb or .srf files). (If you use "
          ".pdb files, surfaces will be computed first.) Output their local "
          "structural alignments in an .nosql file. Each alignment consists "
          "of a rotational matrix, alignment scores, and aligned residues of "
          "the compared proteins.")
      .add_argument<bool>(
          {"extract"},
          "Calculate surface of a protein. Redirect the output (which is the "
          "surface) to a surface (.srf) file. Surface files can be used "
          "instead of .pdb files together with -compare or -surfdb options, "
          "which improves performance when doing repetitive comparisons "
          "(because surface does not need to be recalculated for each "
          "comparison). Option -surfdb works with .srf files exclusively!")
      .add_argument<bool>(
          {"results"},
          "Read alignments from an .nosql file, filter them according to "
          "their scores, and calculate fingerprint residues (which can also "
          "be used as filter). Output results in Json format. In addition, "
          "replace B-factors in the query protein’s PDB file with the "
          "degrees of structural conservation. If used with -ligdir "
          "modifier, output ligands in Json format as well.")
      .add_argument<bool>(
          {"surfdb"},
          "Compare the query protein surface (.srf) with other protein "
          "surfaces listed in the SURF_FILE (see -sfile modifier). This does "
          "the same calculation as the -compare option, but faster, because "
          "protein surfaces are precalculated (see -extract option). This "
          "options also supports parallel computation on multiple CPUs. "
          "Output is the same as with -compare.")
      // .add_argument<bool>("-h",
      //                 "Show list of all parameters and their current
      //                 values. " "You can copy/paste the parameters into a
      //                 separate " "file and change their values (see -param
      //                 modifier).")

      .add_argument<int>(
          // "-alno ALIGNMENT_NO",
          {"alno"},
          "Each comparison of a pair of proteins may result in many "
          "different local structural alignments.The alignment number can be "
          "0 to 4 (see default CLUS_TO_OUTPUT parameter).")
#ifndef MPI_ENABLED
      .add_argument<int>({"ncpu"}, /* "-ncpu NCPU" */
                         "Use this number of concurrent threads.")
#endif
      .add_argument<std::string>(
          // "-bsite BSITE, -bsite1 BSITE, or -bsite2 BSITE",
          {"bsite"},
          "This selects protein residues in a certain radius (set by -dist) "
          "around the given ligand, and takes these residues as input (use "
          "with -extract or -compare options). If used with -compare, it "
          "only works with .pdb files (not .srf).For example: ‘-bsite "
          "ATP.305.A’ - ATP (residue name), 305 (residue number), A (chain "
          "id) or ‘-bsite *.*.B’ - chain B is the ligand (so you can select "
          "protein-protein binding sites as input)")
      .add_argument<std::string>({"bsite1"}, "See --bsite for description")
      .add_argument<std::string>({"bsite2"}, "See --bsite for description")
      .add_argument<std::vector<std::string>>(
          {"c1"}, "Chain identifiers of the first compared protein. One- or "
                  "two-letter (new) chain identifiers are accepted. You may "
                  "give multiple chains, e.g., '-c1 A B C'.")
      .add_argument<std::vector<std::string>>(
          {"c2"}, "Chain identifiers of the second compared protein. One- and "
                  "two-letter (new) chain identifiers are accepted. You may "
                  "give multiple chains, e.g., '-c1 A B C'.")
      .add_argument<bool>(
          {"database"}, "Used by the web server (with -surfdb and -compare "
                        "options). It will output an .nosql file as usual, and "
                        "additional .nosql file with inversed rotational "
                        "matrices, whose lines are marked with asterisks.")
      .add_argument<double>(
          // "-dist INTER_CHAIN_DIST",
          {"dist"},
          "The distance between protein chains or between ligand and "
          "protein. Use with -bsite modifier or -mark and -results options. ")
      .add_argument<std::string>({"srffile"},
                                 "Surface file (.srf) for input or output.")
      .add_argument<std::string>({"f1"},
                                 "First compared protein (.pdb, .cif or .srf).")
      .add_argument<std::string>(
          {"f2"}, "Second compared protein (.pdb, .cif or .srf).")
      .add_argument<std::string>({"in"}, "Directory where input files are.")
      .add_argument<bool>(
          {"local"},
          "Use this to perform local alignments search (with -compare or "
          "-surfdb options). By default, after the local alignment is found "
          "(with maximum clique algorithm), an attempt is made to extend "
          "this alignment along the backbones of the compared proteins. In "
          "this way, parts of proteins that adopt different conformations "
          "(e.g., loops) can be aligned (these aligned residues are marked "
          "with 'flx' in alignments.json).")
      .add_argument<bool>({"longnames"},
                          "Use this to allow long file names. By default the "
                          "protein names are trimmed down to 4 letters. ")
      .add_argument<std::string>(
          {"motif"},
          "This selects residues to be used as a query instead of the whole "
          "protein structure (use with -extract or -compare options). This "
          "will generate a .srf file with only the selected residues. To "
          "select some residues on chains A and B of the input protein, use "
          "-motif \"[:A and (14,57,69-71) or :B and (33,34,50)]\". Note that "
          "chain Iids are case sensitive. Square brackets are mandatory!")
      .add_argument<std::string>({"motif1"}, "See --motif for description")
      .add_argument<std::string>({"motif2"}, "See --motif for description")
      .add_argument<bool>(
          {"nobb"},
          "Do not include descriptors originating from backbone atoms.")
      .add_argument<bool>(
          {"nomarkbb"},
          "Turns off the default action, which is to mark (not delete) "
          "backbone descriptors. In the first step of filtering, only "
          "non-backbone descriptors are used, while in the maximum clique "
          "step, all descriptors are used.")
      .add_argument<bool>({"noclus"}, "Local structural alignments found "
                                      "(maximum cliques) are not clustered.")
      .add_argument<bool>(
          {"nofp"}, "Do not calculate fingerprint residues. Do not filter "
                    "by fingerprint residues (use with -results option).")
      .add_argument<bool>({"noprune"},
                          "Alignments are not pruned. By default bad scoring "
                          "cliques are deleted (see SURF_VECTOR_ANGLE, "
                          "BLOSUM_SCORE and CALPHA_RMSD parameters).")
      .add_argument<std::string>({"out"}, "Directory to write output files to.")
      .add_config({"param", "config"},
                  "Read parameters from the specified parameter file.")
      .add_argument<std::string>(
          {"sfile"},
          "Specify file that contains names of .srf files to be compared "
          "with the query protein (see -surfdb option). Each line must "
          "contain one .srf file-name.Example:{newline}protein1.srf "
          "A{newline}protein2.srf B{newline}protein3.srf A{newline}etc.")
      .add_argument<bool>(
          {"super"},
          "Find local structural alignments between two proteins (use with "
          "-compare option) and superimpose the two proteins according to "
          "all alignments found.  For each alignment, output the '.rota.pdb' "
          "file with the proteins superimposed according to this alignment.")
      .add_argument<bool>({"bkeep"},
                          "Keep beta factors of superimposed proteins intact "
                          "when doing alignment with -align option.")
      .add_argument<bool>(
          {"verbose"},
          "Output debugging information. Use when testing the program.")
      .add_argument<double>(
          {"z_score"},
          "The cutoff value for z_score. Low z_score (<2) means that more "
          "insignificant alignments will be outputted (these can also occur "
          "by chance), higher z_score (>2) means only significant alignments "
          "will be outputted (use with -results option).")
      .add_argument<std::string>(
          {"ligdir"}, "Directory where the names of proteins as files: each "
                      "file is PdbIdChainId, e.g., 1allA.")
      .add_argument<std::string>(
          {"nosql"},
          "Output / read nosql alignments to / from this file.")
      .add_argument<std::string>(
          {"json"}, "Output json alignments to this file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;

  std::cout << "\nConfig file:\n\n" << args.print_config(true) << std::endl;

  probis::Alignments alignments;
  probis::Scores scores;

  if (args.get<bool>("compare")) {
    // compare two proteins
    std::tie(alignments, scores) =
        probis::align_local({.f1 = args.get<std::string>("f1"),
                             .f2 = args.get<std::string>("f2"),
                             .c1 = args.get<std::vector<std::string>>("c1"),
                             .c2 = args.get<std::vector<std::string>>("c2"),
                             .bsite1 = args.get<std::string>("bsite1"),
                             .bsite2 = args.get<std::string>("bsite2"),
                             .ncpu = args.get<int>("ncpu"),
                             .local = probis::local});
  }

  // else if (args.get<bool>("extract")) {
  //   // output surface file
  // } else if (args.get<bool>("surfdb")) {
  //   // compare a protein against a protein database
  //   auto [list_of_aligned_proteins, list_of_matrices, scores] =
  //       molib::probis::compare(protein1, list_of_proteins_srfs_or_pdbs,
  //                              molib::probis::method::probis);
  // }

  // compare two binding sites (here we generate just one product graph per
  // comparison, no tetrahedrons (NEW))
  // auto [aligned2, matrices, scores] =
  //     molib::probis::compare(molib::probis::filter::bsite(protein1, bsite1),
  //                            molib::probis::filter::bsite(protein2, bsite2),
  //                            molib::probis::method::probis);

  // compare a binding site against a protein database

  // generate structure-based multi sequence alignment

  // superimpose two proteins based on matrix

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
