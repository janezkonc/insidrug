#include "insilab/geom3d.hpp"
#include "insilab/glib.hpp"
#include "insilab/glib/algorithms.hpp"
#include "insilab/molib.hpp"
#include "insilab/molib/algorithms.hpp"
#include "insilab/probis.hpp"
#include "insilab/program.hpp"
#include <thread>

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             R"(*******************************************************************
*                                                                 *
*           ______ _   _   ___   _                 ___  _         *
*           | ___ \ \ | | / _ \ | |               / _ \| |        *
*           | |_/ /  \| |/ /_\ \| |     ___   ___/ /_\ \ |        *
*           |    /| . ` ||  _  || |    / _ \ / __|  _  | |        *
*           | |\ \| |\  || | | || |___| (_) | (__| | | | |        *
*           \_| \_\_| \_/\_| |_/\_____/\___/ \___\_| |_/_|        *
*                                                                 *
*                                                                 *
*                  Member of the INSILAB suite.                   *
*                                                                 *
*              Copyright (c) Janez Konc, 2023-2024                *
*                                                                 *
*          If you use RnaLocAl in your work, please cite:         *
*                                                                 *
*   Janez Konc, authors. RNALocAl: an algorithm for detecting     *
*    local 3D similarities in RNA structures.                     *
*                                                                 *
*                        Terms of use:                            *
*                                                                 *
*    (1) This program is free for academic users who intend to    *
*        use it solely for not-for-profit academic research       * 
*        activities.                                              *
*                                                                 *
*    (2) For-profit organizations can contact konc@cmm.ki.si      *
*        for licensing information.                               *
*                                                                 *
*******************************************************************)")
      .add_argument<std::string>({"reference", "r"},
                                 "Path to the reference RNA structure file "
                                 "[{.mol2,.pdb,.cif,.json}[.gz]",
                                 "", true)
      .add_argument<std::string>({"target", "t"},
                                 "Path to the target RNA structure file "
                                 "[{.mol2,.pdb,.cif,.json}[.gz]",
                                 "", true)

      .add_argument<std::vector<std::string>>(
          {"ref_chain", "rc"},
          "Chain ID(s) for the reference compared RNA structure file (if "
          "omitted, all chains within the file are considered)")
      .add_argument<std::vector<std::string>>(
          {"tar_chain", "tc"},
          "Chain ID(s) for the target compared RNA structure file (if "
          "omitted, all chains within the file are considered)")
      .add_config({"param", "config"},
                  "Read parameters from the specified parameter file.")
      .add_argument<double>(
          {"cutoff_product"},
          "Defines the size (radius) of local surface patches "
          "to compare in Angstroms",
          12.0)
      .add_argument<double>(
          {"eps_product"},
          "A distance based on which two product graph vertices are connected.",
          2.0)
      .add_argument<double>(
          {"cutoff_min"},
          "The minimum length (in Angstroms) of a tetrahedron side.", 4.0)
      .add_argument<double>(
          {"cutoff_max"},
          "The maximum length (in Angstroms) of a tetrahedron side.", 6.0)
      .add_argument<double>(
          {"eps"},
          "When comparing two tetrahedrons, if the difference "
          "between all of their corresponding "
          "sides is below this threshold (Angstroms), we "
          "consider them similar.",
          0.5)
      .add_argument<int>({"ncpu"},
                         "Number of CPUs to use concurrently (if not "
                         "specified, all CPUs are used)",
                         std::thread::hardware_concurrency())
      .add_argument<bool>(
          {"verbose"},
          "Output debugging information. Use when testing the program.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {

  using namespace insilab;
  using namespace insilab::molib;
  const auto reference =
      Molecules::Parser(args.get<std::string>("reference"),
                        io::Parser::Options::skip_hetatm)
          .parse()
          .first()
          .filter(details::chains_to_exprvec(
              args.get<std::vector<std::string>>("ref_chain")));

  const auto target = Molecules::Parser(args.get<std::string>("target"),
                                        io::Parser::Options::skip_hetatm)
                          .parse()
                          .first()
                          .filter(details::chains_to_exprvec(
                              args.get<std::vector<std::string>>("tar_chain")));

  const auto &reference_descriptors = probis::generate_descriptors(
      reference, probis::details::descriptor_rules_nucleic);
  const auto &target_descriptors = probis::generate_descriptors(
      target, probis::details::descriptor_rules_nucleic);

  const auto &product_vertices =
      probis::compute_tetrahedron_product_graph_vertices(
          reference_descriptors.at(0), target_descriptors.at(0),
          args.get<double>("cutoff_min"), args.get<double>("cutoff_max"),
          args.get<double>("eps"));

  const auto &product_graphs =
      probis::compute_product_graphs<probis::Tetrahedron>(
          product_vertices, args.get<double>("cutoff_product"),
          args.get<double>("eps_product"));

  auto max_max_clique_sz{0uz};
  std::set<std::pair<const probis::Descriptor, const probis::Descriptor>,
           probis::desc_comparator>
      max_nonredundant;
  for (const auto &product_graph : product_graphs) {
    const auto &conn = glib::generate_adjacency_matrix(product_graph);
    const auto &max_clique = glib::algorithms::find_maximum_clique(conn);
    max_max_clique_sz = std::max(max_clique.size(), max_max_clique_sz);

    std::pair<std::vector<probis::Descriptor>, std::vector<probis::Descriptor>>
        all_descriptor_product_vertices;
    std::cout << "Tetrahedron maximum clique size = " << max_clique.size()
              << std::endl;
    std::cout << "Matched tetrahedrons:\n";
    for (const auto &idx : max_clique) {
      const auto &pvertex = product_graph[idx];
      const auto &[th1, th2] = pvertex->similar_entities;
      for (auto i{0uz}; i < th1.descriptor.size(); ++i) {
        const auto &desc1 = *(th1.descriptor[i]);
        const auto &desc2 = *(th2.descriptor[i]);
        std::cout << desc1 << " <=> " << desc2 << std::endl;
      }
      const auto &descriptor_product_vertices =
          probis::compute_descriptor_product_graph_vertices(th1.descriptor,
                                                            th2.descriptor);
      all_descriptor_product_vertices.first.insert(
          all_descriptor_product_vertices.first.end(),
          std::begin(descriptor_product_vertices.first),
          std::end(descriptor_product_vertices.first));
      all_descriptor_product_vertices.second.insert(
          all_descriptor_product_vertices.second.end(),
          std::begin(descriptor_product_vertices.second),
          std::end(descriptor_product_vertices.second));
    }
    const auto &descriptor_product_graph =
        probis::compute_product_graph<probis::Descriptor>(
            all_descriptor_product_vertices, args.get<double>("eps_product"));
    const auto &desc_conn =
        glib::generate_adjacency_matrix(descriptor_product_graph);
    const auto &desc_max_clique =
        glib::algorithms::find_maximum_clique(desc_conn);
    std::cout << "Descriptor maximum clique size = " << desc_max_clique.size()
              << std::endl;

    // make descriptors non redundant
    std::set<std::pair<const probis::Descriptor, const probis::Descriptor>,
             probis::desc_comparator>
        nonredundant;
    for (const auto &idx : desc_max_clique) {
      const auto &pvertex = descriptor_product_graph[idx];
      const auto &[desc1, desc2] = pvertex->similar_entities;
      nonredundant.insert(std::pair{desc1, desc2});
    }
    // now we can superimpose the two sets of vertices using e.g., Kabsch
    // algorithm
    geom3d::Coordinate::Vec crds1, crds2;
    std::cout << "Matched descriptors:\n";
    for (const auto &[desc1, desc2] : nonredundant) {
      std::cout << desc1 << " <=> " << desc2 << std::endl;
      crds1.push_back(desc1.crd());
      crds2.push_back(desc2.crd());
    }
    const auto super_crds2 = geom3d::Kabsch{}.superimpose(crds1, crds2);
    for (auto i{0uz}; i < crds1.size(); ++i) {
      std::cout << crds1[i].crd() << " " << super_crds2[i].crd() << " "
                << crds2[i].crd() << std::endl;
    }
    // from matching descriptors we get residue-residue correspondences
    if (nonredundant.size() > max_nonredundant.size())
      max_nonredundant = nonredundant;
    for (const auto &[desc1, desc2] : nonredundant) {
      std::cout << reference.get_atom(desc1.atom_number()) << " <=> "
                << target.get_atom(desc2.atom_number()) << std::endl;
    }
  }
  std::cout << "The largest local similarity in terms of a maximum clique "
               "where vertices are tetrahedrons: "
            << max_max_clique_sz << std::endl;
  std::cout
      << "The largest local similarity in terms of nonredundant descriptors: "
      << max_nonredundant.size() << std::endl;
  std::cout << "Similar residues (largest similar patch):" << std::endl;
  for (const auto &[desc1, desc2] : max_nonredundant) {
    const auto &ref = reference.get_atom(desc1.atom_number()).residue();
    const auto &tar = target.get_atom(desc2.atom_number()).residue();
    std::cout << ref.resn() << ref.resi() << " <=> " << tar.resn() << tar.resi()
              << std::endl;
  }
  // cluster (extend) similar patches

  // output score, superimposition matrix, terse/full report

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
