#include "insilab/probisdock.hpp"
#include "amber10.hpp"
#include "insilab/helper.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/ommiface.hpp"
#include "insilab/parallel.hpp"
#include "insilab/program.hpp"
#include "insilab/score.hpp"
#include "tip3p.hpp"
#include <atomic>
#include <exception>
#include <filesystem>
#include <iostream>
#include <mutex>
#include <regex>
#include <thread>
#include <typeinfo>

namespace fs = std::filesystem;

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "ProBiS-Dock: A Hybrid Multi-Template Homology Flexible Docking "
             "Algorithm Enabled by Protein Binding Site Comparison")

      .add_argument<std::string>({"l", "ligands_file"},
                                 "Small molecules that will be docked to the "
                                 "receptor ({.pdb|.mol2|.json}[.gz])",
                                 "", true)
      .add_argument<std::string>(
          {"probisdock_db_dir"},
          "A directory that contains a binding sites dataset (receptors, "
          "template ligands, centroids) obtained from the ProBiS-Dock "
          "Database (http://probis-dock-database.insilab.org). File names must "
          "follow the standard (this is default in ProBiS-Dock Database): "
          ""
          "  * receptor (protein): receptor_PDBID_CHAINID_*.pdb.gz"
          "  * template ligands: predicted_ligands_PDBID_CHAINID_*.pdb.gz"
          "  * centroids: probisdock_centro_PDBID_CHAINID_*.cen.gz")
      .add_argument<std::string>({"r", "receptor_file"},
                                 "The protein to which small molecules will be "
                                 "docked ({.pdb|.cif|.json}[.gz])")
      .add_argument<std::string>({"template_ligands_file"},
                                 "The template ligands for ProBiS-Score "
                                 "- provide when probis_level > 0.0 "
                                 "(.pdb[.gz]|.mol2[.gz]|.json[.gz])")

      .add_argument<std::string>({"centro_file"},
                                 "Binding site centroids (.cen|.json[.gz]) ")
      .add_argument<std::string>(
          {"s", "summary_file"}, "Summary of the ProBiS-Dock docking results",
          fs::current_path() / fs::path("probisdock_summary.txt.gz"))
      .add_argument<std::string>(
          {"docked_complexes_file"}, "Docked ligand-receptor complexes",
          fs::current_path() / fs::path("probisdock_docked_complexes.pdb.gz"))
      .add_argument<double>(
          {"receptor_trim_dist"},
          "Output receptor residues less than this distance from the "
          "docked ligand's pose (use with --output_docked_complexes)",
          5.0)
      .add_argument<double>(
          {"probis_level"},
          "The combined scoring function is a combination of two score "
          "terms, ProBiS-Score that relies on template ligands and GSscore, a "
          "general statistical scoring function: Combined-Score = h * "
          "ProBiS-Score + (1 - h) * GSscore",
          0.9, false, {}, {0.0, 1.0})
      .add_argument<double>(
          {"max_seq_id"},
          "Upper bound for the sequence identity (%) of the proteins "
          "from which predicted (template) ligands are transposed by the "
          "ProBiS-Dock Database method - enables testing with excluded "
          "template ligands above certain seq. id.",
          100.0)
      .add_argument<double>(
          {"max_frag_radius"},
          "Maximum fragment radius for creating the initial rotamers", 16.0)
      .add_argument<std::string>(
          {"ref_state"},
          "Normalization method for the reference state of the GSscore scoring "
          "function - 'mean' is averaged over all atom type pairs, whereas "
          "'cumulative' is a summation for atom type pairs",
          "mean", false, {"mean", "cumulative"})
      .add_argument<std::string>(
          {"comp"},
          "Atom types used in calculating reference state 'reduced' or "
          "'complete' in GSscore scoring function ('reduced' includes "
          "only "
          "those atom types present in the "
          "specified receptor and small molecule, whereas	'complete' "
          "includes all atom types)",
          "reduced", false, {"reduced", "complete"})
      .add_argument<std::string>({"rad_or_raw"},
                                 "Function for calculating scores "
                                 "'radial' or 'normalized_frequency'",
                                 "radial", false,
                                 {"radial", "normalized_frequency"})
      .add_argument<double>({"dist_cutoff"},
                            "Cutoff distance for the scoring function", 6.0,
                            false, {}, {4.0, 15.0})
      .add_argument<double>({"step_non_bond"},
                            "Step for spline generation of non-bonded "
                            "knowledge-based potential",
                            0.01, false, {}, {0.0, 1.0})
      .add_argument<double>({"scale_non_bond"},
                            "Scale non-bonded forces and energy for "
                            "knowledge-based potential",
                            10.0, false, {}, {0.0, 1000.0})
      .add_argument<int>({"num_read_ligands"},
                         "Maximum number of ligands to read in one chunk - set "
                         "to higher values when reading from network disks",
                         10)
      .add_argument<int>(
          {"num_read_template_ligands"},
          "Maximum number of template ligands to read in one chunk", 10)
      .add_argument<double>({"spin_degrees"}, "Spin degrees to rotate ligand",
                            30, false, {5, 10, 15, 20, 30, 60, 90})
      .add_argument<double>({"clash_coeff"},
                            "Clash coefficient for determining whether two "
                            "atoms clash by eq. dist12 s< C * (vdw1 + vdw2)",
                            0.75)
      .add_argument<double>({"tol_seed_dist"},
                            "Tolerance on seed distance in-between linking",
                            2.0)
      .add_argument<double>({"lower_tol_seed_dist"},
                            "Lower tolerance on seed distance for getting "
                            "initial conformations of docked fragments",
                            2.0)
      .add_argument<double>({"upper_tol_seed_dist"},
                            "Upper tolerance on seed distance for getting "
                            "initial conformations of docked fragments",
                            2.0)
      .add_argument<int>({"max_possible_conf"},
                         "Maximum number of possible docked conformations of "
                         "compounds to generate (-1 = unlimited)",
                         200)
      .add_argument<int>({"link_iter"},
                         "Maximum number of iterations for A-star linking of "
                         "fragments back into molecules",
                         10000)
      .add_argument<double>(
          {"docked_clus_rad"},
          "Cluster radius between docked ligand conformations", 0.75)
      .add_argument<double>(
          {"max_allow_energy"},
          "Maximum allowed energy for docked seed fragment conformations", 0.0)
      .add_argument<bool>({"iterative"},
                          "Enable iterative minimization during linking", false)
      .add_argument<int>({"n_cliques"},
                         "The number of maximum weight cliques with "
                         "lowest energies to output",
                         100000)
      .add_argument<int>({"max_steps"},
                         "Maximum number of steps the MCQDW algorithm is "
                         "allowed (-1 = unlimited)",
                         100000000)
      .add_argument<std::string>(
          {"fftype"},
          "Forcefield to use 'kb' (knowledge-based) or 'phy' (physics-based)",
          "kb", false, {"kb", "phy"})
      .add_argument<double>({"mini_tol"}, "Minimization tolerance", 0.0001)
      .add_argument<int>({"max_iterations"},
                         "Maximum iterations for minimization during linking",
                         100)
      .add_argument<int>({"max_iterations_final"},
                         "Maximum iterations for final minimization", 1000)
      .add_argument<int>({"update_freq"},
                         "Update non-bond list frequency in steps", 200)
      .add_argument<double>({"position_tolerance"},
                            "Position tolerance which tells when to stop "
                            "minimization in Angstroms - only for kb",
                            0.00000000001)
      .add_argument<double>(
          {"force_tol"},
          "Force convergence tolerance, conformations with unusually "
          "high "
          "forces after minimization will be discarded - only for kb",
          10000.0)
      .add_argument<double>({"flex_radius"},
                            "Protein residues within this distance (in "
                            "Angstroms) around the "
                            "ligand will be flexible during minimization",
                            8.0)
      .add_argument<double>({"top_percent"},
                            "Percent of top-scored docked seed "
                            "fragment's poses to consider "
                            "further for linking back into full molecule",
                            0.15)
      .add_argument<int>(
          {"k_clique_size"},
          "To generate partial conformations of a docked molecule, this value "
          "represents the number of docked fragments required to be positioned "
          "in the binding site at distances with respect to each "
          "other, so that it is possible to reconstruct the original small "
          "molecule in the docked conformation. As a docked molecule typically "
          "consists of several fragments, the default large value indicates "
          "that the k-clique size to be searched will always be equal to the "
          "number of fragments in the docked molecule",
          1000)
      .add_argument<double>({"grid_spacing"},
                            "Binding site grid resolution in Angstroms", 0.75)
      .add_argument<int>({"num_univec"},
                         "Number of unit vectors evenly distributed on a "
                         "sphere for fragment conformation generation",
                         256)
      .add_argument<double>({"conf_spin"},
                            "Spin degrees for fragment conformation generation",
                            10.0)
      .add_argument<double>({"excluded_radius"}, "Excluded radius in Angstroms",
                            0.8)
      .add_argument<double>({"max_interatomic_distance"},
                            "Maximum interatomic distance in Angstroms", 8.0)
      .add_argument<double>({"dist_cutoff_probis_template"},
                            "Distance cutoff for ProBiS-Score scoring "
                            "function in Angstroms",
                            1.0)
      .add_argument<double>({"pow_probis_template"},
                            "How fast the template ProBiS-Score decays with "
                            "distance from template atoms 1/(d+1)^pow",
                            1.0)
      .add_argument<double>(
          {"clus_rad"}, "Cluster radius for rigidly docked seeds in Angstroms",
          2.0)
      .add_argument<int>({"max_jobs"},
                         "Limit for the number of jobs on the queue before "
                         "another one can be pushed on the queue",
                         std::thread::hardware_concurrency())
      .add_argument<int>(
          {"max_cache_size"},
          "Maximum size of cache's priority queue to store docked seeds "
          "- "
          "should be larger than number of threads (see --ncpu)",
          1000)
      .add_argument<int>({"ncpu"},
                         "Number of CPUs to use concurrently (if not "
                         "specified, all CPUs are used)",
                         std::thread::hardware_concurrency())
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .add_argument<bool>(
          {"debug"}, "Output intermediary files to a temporary directory "
                     "(location depends on the TMPDIR environment variable)")
      .add_argument<bool>({"output_docked_complexes"},
                          "Output docked ligand-receptor complexes "
                          "(receptor is trimmed > N A "
                          "around ligand, see --receptor_trim_dist)")
      .add_option_group("multi_receptor", {"probisdock_db_dir"},
                        "Input for inverse docking of multiple ligands against "
                        "multiple receptors")
      .add_argument<bool>(
          {"output_top_scored_only"},
          "Output only one top-scored pose of each docked ligand")
      .add_argument<bool>({"only_ligand_flexible"},
                          "Only ligand is flexible (by default both ligand and "
                          "receptor are flexible)")
      .add_option_group(
          "single_receptor", {"receptor_file", "centro_file"},
          "Input for docking of multiple ligands against a single "
          "receptor")
      .set_excluded_option_group("multi_receptor", "single_receptor")
      .set_required_option_group("single_receptor")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  // ligand reader
  Molecules::Parser lpdb(args.get<std::string>("ligands_file"),
                         Molecules::Parser::Options::all_models |
                             Molecules::Parser::Options::hydrogens,
                         args.get<int>("num_read_ligands"));
  // receptors and template ligands readers
  std::vector<Molecules::Parser> tpdb;
  std::vector<std::pair<Molecules::Parser, std::string>> rpdb;

  // centroid file names
  std::vector<std::string> centroid_files;

  // however, if entire directory of binding sites is provided, read this
  // directory and identify files that belong to each binding site
  if (args.get<std::string>("probisdock_db_dir").empty()) {
    rpdb.emplace_back(
        Molecules::Parser(args.get<std::string>("receptor_file"),
                          Molecules::Parser::Options::all_models),
        fs::path(args.get<std::string>("receptor_file")).filename().string());
    tpdb.emplace_back(args.get<std::string>("template_ligands_file"),
                      Molecules::Parser::Options::all_models |
                          Molecules::Parser::Options::hydrogens,
                      args.get<int>("num_read_template_ligands"));
    centroid_files.emplace_back(args.get<std::string>("centro_file"));
  } else {
    // map PDB & Chain ID to receptor, template ligands and centro file names
    std::map<std::string, std::array<std::string, 3>> bsite;
    for (auto const &dir_entry : fs::directory_iterator{
             fs::path{args.get<std::string>("probisdock_db_dir")}}) {
      const auto file_with_path = dir_entry.path().string();
      const auto filename = dir_entry.path().filename().string();
      const auto id = std::regex_replace(
          filename,
          std::regex{"receptor_|predicted_ligands_|"
                     "probisdock_centro_|.cen.gz|.pdb.gz|.cen|.pdb"},
          "");
      if (filename.starts_with("receptor"))
        bsite[id][0] = file_with_path;
      else if (filename.starts_with("predicted_ligands"))
        bsite[id][1] = file_with_path;
      else if (filename.starts_with("probisdock_centro")) {
        bsite[id][2] = file_with_path;
      }
    }
    // create molecular parsers and the list of centroid files
    for (const auto &[_unused, bsite_files] : bsite) {
      if (bsite_files[0].empty() || bsite_files[1].empty() ||
          bsite_files[2].empty()) {
        std::cerr << "[NOTE] Missing some files in ProBiS-Dock DB for a "
                     "specific receptor... skipping..."
                  << std::endl;
        continue;
      }
      rpdb.emplace_back(
          Molecules::Parser(bsite_files[0],
                            Molecules::Parser::Options::all_models),
          fs::path(bsite_files[0]).filename().string());
      tpdb.emplace_back(bsite_files[1],
                        Molecules::Parser::Options::all_models |
                            Molecules::Parser::Options::hydrogens,
                        args.get<int>("num_read_template_ligands"));
      centroid_files.emplace_back(bsite_files[2]);
    }
  }

  score::ObjectiveFunction objective(
      std::set<int>(), std::set<int>(), args.get<std::string>("ref_state"),
      args.get<std::string>("comp"), args.get<std::string>("rad_or_raw"),
      args.get<double>("dist_cutoff"), args.get<double>("step_non_bond"));
  objective.parse_objective_function(args.get<double>("scale_non_bond"));

  // forcefield stuff : create forcefield for small molecules (and KB
  // non-bonded with receptor) and read receptor's forcefield xml file(s) into
  // forcefield object
  ommiface::ForceField ffield(args.get<double>("step_non_bond"));
  ffield.parse_gaff_dat_file()
      .parse_forcefield_file(data::amber10_dat)
      .parse_forcefield_file(data::tip3p_dat);

  ommiface::SystemTopology::loadPlugins();
  inout::create_empty_file(args.get<std::string>("summary_file"));
  inout::create_empty_file(args.get<std::string>("docked_complexes_file"));

  // list of tasks shared between threads
  parallel::Jobs jobs(args.get<int>("ncpu"), args.get<int>("max_jobs"));
  // cache of docked seeds
  helper::Cache<probisdock::DockedSeedKey, probisdock::MoleculesDecorator>
      cache(args.get<int>("max_cache_size"));

  // initialize worker threads
  std::vector<std::jthread> threads;
  for (int thread_id = 0; thread_id < args.get<int>("ncpu"); ++thread_id) {
    threads.emplace_back(probisdock::parallel_fun, std::ref(jobs), thread_id);
  }
  // start reading files
  probisdock::reader_fun(jobs, args, cache, lpdb, rpdb, tpdb, centroid_files,
                         ffield, objective);
  for (auto &thread : threads) {
    thread.join();
  }
  std::clog << "************ PROBIS-DOCK HAS FINISHED **************"
            << std::endl;
  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
