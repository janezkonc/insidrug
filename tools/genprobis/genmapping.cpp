#include "insilab/genprobis.hpp"
#include "insilab/inout.hpp"
#include "insilab/molib.hpp"
#include "insilab/path.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "genmapping: This program is part of GenProBiS and maps sequence "
             "variants onto protein structures.")
      .add_argument<std::vector<std::string>>(
          {"pdb_files"},
          "A protein file for PDB proteins; or a list of chunks for AlphaFold "
          "proteins, where chunks represent a single protein and are ordered "
          "F[1-N]")
      .add_argument<std::string>(
          {"separated_uniprot_variants_dir"},
          "Directory with calculated files (generated with genexdb), one for "
          "each protein (UniProt_ID) holding variants and clinical information",
          {}, true)
      .add_argument<std::string>(
          {"sifts_file"},
          "An XML file with SIFTS information (UniProt => PDB mapping) for a "
          "given PDB protein structure")
      .add_argument<std::string>({"mapping_file"},
                                 "An output Json file with sequence variants "
                                 "mapped to a protein structure",
                                 "variants_structure_mapping.json.gz")
      .add_config({"config"},
                  "Read parameters from the specified parameter file")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  // read protein file(s), for AlphaFold a protein can be divided into chunks
  // which must be sorted consecutively F1, F2, etc.
  const auto &pdb_files = args.get<std::vector<std::string>>("pdb_files");
  Molecules receptors;
  for (const auto &pdb_file : pdb_files) {
    const auto receptor =
        Molecules::Parser(pdb_file, Molecules::Parser::Options::first_model)
            .parse()
            .first();
    receptors.add(receptor).set_name(pdb_file);
  }

  // read SIFTS file that corresponds to the protein (PDB ID) with PDB <=>
  // UniProt mapping or generate a 'sifts' info for AlphaFold structure (can
  // be multiple chunks)
  const auto sifts =
      args.get<std::string>("sifts_file").empty()
          ? genprobis::generate_sifts_alphafold(receptors)
          : genprobis::generate_sifts(args.get<std::string>("sifts_file"),
                                      receptors.first());

  // make a mapping of PDB residues to sequence variants
  const auto mapping = genprobis::generate_unipdbseq_mapping(
      args.get<std::string>("separated_uniprot_variants_dir"), sifts);

  inout::output_file(mapping, args.get<std::string>("mapping_file"));

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
