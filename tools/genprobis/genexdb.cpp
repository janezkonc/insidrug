#include "insilab/genprobis.hpp"
#include "insilab/inout.hpp"
#include "insilab/program.hpp"
#include "nlohmann/json.hpp"
#include <filesystem>
#include <limits>
#include <string>

namespace fs = std::filesystem;

insilab::program::ArgumentParser parse_arguments(int argc, char *argv[]) {
  return insilab::program::ArgumentParser(
             "genexdb: This program is part of GenProBiS and prepares the "
             "external databases and puts them in a Json format that can be "
             "further processed.")
      .add_argument<std::string>(
          {"submission_summary_file"},
          "ClinVar database tab delimited file with submission summary",
          "clinvar/submission_summary.txt.gz")
      .add_argument<std::string>(
          {"variant_summary_file"},
          "ClinVar database tab delimited file with variant summary",
          "clinvar/variant_summary.txt.gz")
      .add_argument<std::string>(
          {"pharmgkb_file"}, "PharmGKB database XML file", "pharmgkb/rsid.tsv")
      .add_argument<std::string>({"clinvar_json_file"},
                                 "ClinVar Json file generated",
                                 "clinvar.json.gz")
      .add_argument<std::string>({"pharmgkb_json_file"},
                                 "PharmGKB Json file generated",
                                 "pharmgkb.json.gz")
      .add_argument<std::string>({"uniprot_json_file"},
                                 "UniProt Json file generated",
                                 "uniprot.json.gz")
      .add_argument<std::string>(
          {"uniprot_variants_dir"},
          "Directory where the UniProt Variants database files are located",
          "uniprot/variants")
      .add_argument<std::string>({"out_dir"},
                                 "Output directory for UniProt Variants",
                                 fs::current_path())
      .add_config({"config"},
                  "Read parameters from the specified parameter file.")
      .parse(argc, argv);
}

int run(const insilab::program::ArgumentParser &args) {
  using namespace insilab;
  using namespace insilab::molib;

  // read PharmGKB dataset
  const auto pharmgkb =
      genprobis::generate_pharmgkb(args.get<std::string>("pharmgkb_file"));

  inout::output_file(pharmgkb, args.get<std::string>("pharmgkb_json_file"));

  // read the ClinVar dataset
  const auto clinvar = genprobis::generate_clinvar(
      args.get<std::string>("submission_summary_file"),
      args.get<std::string>("variant_summary_file"));

  inout::output_file(clinvar, args.get<std::string>("clinvar_json_file"));

  // read the UniProt Variants dataset and make a Json file, one for each
  // UniProt ID (with added ClinVar and PharmGKB annotations)
  genprobis::generate_uniprot_variants(
      args.get<std::string>("uniprot_variants_dir"),
      args.get<std::string>("out_dir"), clinvar, pharmgkb);

  return 0;
}

int main(int argc, char *argv[]) {
  return insilab::program::create(argc, argv, parse_arguments, run);
}
